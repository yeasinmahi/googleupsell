﻿//using System;
//using System.IO;
//using System.Threading.Tasks;
//using BusinessEntity.Log;
//using BusinessEntity.Logging;
//using BusinessObject;

//namespace LogService
//{
//    public class Log
//    {
//        private bool isWriteLogFile = false;
//        private bool isWriteLogDb = false;
//        private bool isForceStopWriteLogDb = false;
//        public async Task Error(Exception ex, string msisdn, bool isWriteFile, bool isWriteDb)
//        {
//            isWriteLogFile = isWriteFile;
//           await Error(ex, msisdn, isWriteDb);
//        }
//        public async Task Error(Exception ex, bool isWriteDb)
//        {
//           await Error(ex, "", isWriteDb);
//        }
//        public async Task Error(Exception ex)
//        {
//           await  Error(ex, "");
//        }
//        public async Task Error(Exception ex, string msisdn)
//        {
//            await Error(ex, "Error", msisdn);
//        }
//        public async Task Error(Exception ex, string title,string msisdn)
//        {
//            BEAppErrorLog entities = new LogHelper().GetErrorEntity(ex,msisdn);

//            if (!isForceStopWriteLogDb && (isWriteLogDb || LogConfig.IsWriteErrorLogDb))
//            {
//              await  WriteLogDb(entities);
//            }
//            string s = new LogHelper().ConvertString(entities);
//            Info(s, title);
//        }
//        public async Task Error(Exception ex, string msisdn, bool isWriteDb)
//        {
//            if (!isWriteDb)
//            {
//                isForceStopWriteLogDb = true;
//            }
//            isWriteLogDb = isWriteDb;
//           await Error(ex, msisdn);
//        }
//        public void Info(string str)
//        {
//           Info("Info", str);
//        }
//        public void Info(string str, string title)
//        {
//            if (isWriteLogFile || LogConfig.IsWriteErrorLogFile)
//               WriteLogFile(title + "\t" + str);
//        }
//        public void Info(string str, string title, bool isWriteFile)
//        {
//            isWriteLogFile = isWriteFile;
//            Info(title, str);
//        }
//        public async Task RequestResponse(BEAppRequestResponseLog data)
//        {
//            await RequestResponse(data, "Request/Response");

//        }
//        public async Task RequestResponse(BEAppRequestResponseLog data, string title)
//        {
//            if (LogConfig.IsWriteRequestResponseFile)
//            {
//                string s = new LogHelper().ConvertString(data);
//                WriteLogFile(title + "\t" + s);
//            }
//            if (LogConfig.IsWriteRequestResponseDb)
//            {
//                await WriteLogDb(data);
//            }

//        }
//        public async Task AuditTrail(BEAppAudiTtrail data)
//        {
//            //if (LogConfig.IsWriteAuditTrailDb)
//            {
//                //WriteLogDb(new Entities(data));
//               await WriteLogDb(data);
//            }

//        }
//        private void WriteLogFile(string msg)
//        {
//            try
//            {
//                string strFilePath = LogConfig.BaseLogPath;
//                string fileName = LogConfig.ErrorLogFileName;
//                FileStream file;
//                StreamWriter sw;
//                if (!File.Exists(LogConfig.ErrorLogFullPath))
//                {
//                    if (!Directory.Exists(strFilePath))
//                    {
//                        Directory.CreateDirectory(strFilePath);
//                    }
//                    try
//                    {
//                        file = new FileStream(LogConfig.ErrorLogFullPath, FileMode.CreateNew, FileAccess.Write, FileShare.Read);
//                        sw = new StreamWriter(file);

//                        sw.WriteLine("Date \t\tTime  \t\tTitle \t\tMessage");
//                        sw.WriteLine("=====================================================================================================");
//                        sw.Close();
//                        file.Close();
//                    }
//                    catch (Exception e)
//                    {
//                    }
//                }

//                try
//                {
//                    file = new FileStream(LogConfig.ErrorLogFullPath, FileMode.Append, FileAccess.Write, FileShare.Read);
//                    sw = new StreamWriter(file);

//                    string strMsg = DateTime.Now.ToString("dd:MM:yyyy") + "\t" + DateTime.Now.ToString("HH:mm:ss:fff") + "\t" + msg;
//                    sw.WriteLine(strMsg);
//                    sw.Close();
//                    file.Close();
//                }
//                catch (Exception e) { }
//            }
//            catch (Exception e)
//            {
//            }
//        }

//        //private async Task WriteLogDb(Entities entities)
//        //{
//        //    try
//        //    {
//        //        if (entities.Error != null)
//        //        {
//        //            new LogDataAccess().InsertAppErrorLog(entities.Error);

//        //        }
//        //        if (entities.RequestResponseMeta != null)
//        //        {
//        //            new LogDataAccess().InsertAppRequestResponse(entities.RequestResponseMeta);

//        //        }
//        //        if (entities.AuditTrailMeta != null)
//        //        {
//        //            new BOAppAudiTtrail().Save()
//        //            //new LogDataAccess().InsertAuditTrail(entities.AuditTrailMeta);

//        //        }
//        //    }
//        //    catch (Exception ex)
//        //    {

//        //    }

//        //}

//        private async Task WriteLogDb(BEAppErrorLog error)
//        {
//           await new BOAppErrorLog().Save(error);
//        }

//        private async Task WriteLogDb(BEAppRequestResponseLog appRequestResponseLog)
//        {
//           await new BOAppRequestResponseLog().Save(appRequestResponseLog);
//        }
//        private async Task WriteLogDb(BEAppAudiTtrail appAudiTtrail)
//        {
//           await new BOAppAudiTtrail().Save(appAudiTtrail);
//        }
//    }
//}

