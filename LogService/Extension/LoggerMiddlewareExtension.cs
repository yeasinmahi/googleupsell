﻿using LogService.Middleware;
using Microsoft.AspNetCore.Builder;

namespace LogService.Extension
{
    public static class LoggerMiddlewareExtension
    {
        public static IApplicationBuilder UseLoggerMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<LoggerMiddleware>();
        }
    }
}