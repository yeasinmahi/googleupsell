﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using BusinessEntity.Logging;
using Microsoft.AspNetCore.Http;

namespace LogService
{
    public class LogHelper
    {
        public BEAppErrorLog GetErrorEntity(Exception ex, string msisdn,double rtt,string request)
        {
            try
            {
                var error = new BEAppErrorLog
                {
                    Message = ex.Message,
                    Stacktrace = ex.StackTrace,
                    //LineNumber = (new StackTrace(ex, true)).GetFrame(0).GetFileLineNumber(),
                    //ColumnNumber = (new StackTrace(ex, true)).GetFrame(0).GetFileColumnNumber()
                    Msisdn = msisdn,
                    Rtt=rtt,
                    Request=request
                };
                return error;
            }
            catch (Exception)
            {
            }

            return null;
        }

        public string ConvertString(Error error)
        {
            try
            {
                var s = GetProperty(error);
                return s;
            }
            catch (Exception e)
            {
                return e.Message;
            }

            //return "Message:" + error.Message + " -- StackTrace:" + ex.StackTrace + " -- LineNumber:" + (new StackTrace(ex, true)).GetFrame(0).GetFileLineNumber() + " -- Column:" + (new StackTrace(ex, true)).GetFrame(0).GetFileColumnNumber())
        }

        public string ConvertString<T>(T data)
        {
            try
            {
                var s = GetProperty(data);
                return s;
            }
            catch (Exception e)
            {
                return e.Message;
            }

            //return "Message:" + error.Message + " -- StackTrace:" + ex.StackTrace + " -- LineNumber:" + (new StackTrace(ex, true)).GetFrame(0).GetFileLineNumber() + " -- Column:" + (new StackTrace(ex, true)).GetFrame(0).GetFileColumnNumber())
        }

        public string GetProperty<T>(T obj)
        {
            var s = string.Empty;
            var t = obj.GetType();
            foreach (var pi in t.GetProperties()) s += "  " + pi.Name + ":" + pi.GetValue(obj, null);
            return s;
        }

        public string CovertString(Stream stream)
        {
            using (var reader = new StreamReader(stream, Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }

        public string CovertString(IHeaderDictionary headers)
        {
            var s = string.Empty;
            foreach (var header in headers) s += header.Key + ":" + header.Value;
            return s;
        }

        public string CovertString(IPAddress[] addresses)
        {
            var s = string.Empty;
            if (addresses != null)
            {
                foreach (var address in addresses) s += address + ",";
                s = s.Substring(0, s.Length - 1);
            }
            
            return s;
        }

        public string CovertString(object[] array)
        {
            var s = string.Empty;
            if (array != null && array.Length > 0)
            {
                foreach (string st in array) s += st + "|";
                s = s.Substring(0, s.Length - 1);
            }

            return s;
        }
        public async Task<string> ReadRequestBody(HttpRequest request)
        {
            request.EnableBuffering();

            var buffer = new byte[Convert.ToInt32(request.ContentLength)];
            await request.Body.ReadAsync(buffer, 0, buffer.Length);
            var bodyAsText = Encoding.UTF8.GetString(buffer);
            request.Body.Seek(0, SeekOrigin.Begin);

            return bodyAsText;
        }

        public async Task<BEAppAudiTtrail> BuildAuditTrailtMetadata(HttpContext context, string msisdn,
            //RouteValue route, 
            string action, string controller, string version, BEServiceUser serviceUser,
            int rtt, string errorMessage, params object[] paramiters)
        {
            var request = context.Request;
            var audit = new BEAppAudiTtrail
            {
                Msisdn = msisdn,
                //RequestUrl = $"{request.Scheme}://{request.Host}{request.Path}",
                RequestUrl = $"{request.Path}",
                RequestQueryString = $"{request.QueryString}".Length > 0 ? $"{request.QueryString}".Substring(1) : null,
                IpAddress = $"{request.HttpContext.Connection.RemoteIpAddress}",
                Method = action,
                //Controller =controller,
                ApiVersion = version,
                Params = CovertString(paramiters),
                RTT = rtt,
                ServiceUserId = serviceUser.UserID,
                ErrorMessage = errorMessage,
                Provider = AppSettings.Provider
            };

            return audit;
        }
    }
}