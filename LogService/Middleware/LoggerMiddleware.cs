﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using BusinessEntity.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace LogService.Middleware
{
    public class LoggerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly BEAppAudiTtrail auditLog = new BEAppAudiTtrail();
        private readonly Log log = new Log(string.Empty);
        private long executeTime;
        private Stopwatch watch;

        public LoggerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            watch= Stopwatch.StartNew();
            try
            {
                BEAppRequestResponseLog appRequestResponseLog = await BuildRequestMetadata(context);
                //await ReadRequestBody(context.Request);

                //context.Response.Headers.Add("RTT", executeTime.ToString());

                appRequestResponseLog.RTT = await ReadResponseBody(context);
                appRequestResponseLog = BuildResponseMetadata(appRequestResponseLog, context.Response, "");
                //auditLog = BuildAuditTrailtMetadata(context);
                //throw new Exception("Test");
                try
                {
                    SendToLog(appRequestResponseLog);
                }
                catch (Exception)
                {
                }
            }
            catch (Exception ex)
            {
                try
                {
                    log.Msisdn = GetMsisdn(context.Request);
                    await log.Error(ex, "Middleware", watch.ElapsedMilliseconds, await new LogHelper().ReadRequestBody(context.Request));
                    auditLog.ErrorMessage = ex.Message;
                }
                catch (Exception)
                {
                }

                //    throw ex;
            }
            finally
            {
                watch.Stop();
                //try
                //{
                //    //auditLog.RTT = (int) executeTime;
                //    //SendToAuditTrail(auditLog);
                //}
                //catch (Exception e)
                //{
                //    log.Msisdn = GetMsisdn(context.Request);
                //    log.Error(e, "AuditTrail");
                //}
            }
        }

        //private async Task<string> ReadRequestBody(HttpRequest request)
        //{
        //    request.EnableBuffering();

        //    var buffer = new byte[Convert.ToInt32(request.ContentLength)];
        //    await request.Body.ReadAsync(buffer, 0, buffer.Length);
        //    var bodyAsText = Encoding.UTF8.GetString(buffer);
        //    request.Body.Seek(0, SeekOrigin.Begin);

        //    return bodyAsText;
        //}

        private async Task<long> ReadResponseBody(HttpContext context)
        {
            //var bodyAsText = string.Empty;
            //var originalBodyStream = context.Response.Body;
            //using (var responseBody = new MemoryStream())
            //{
            //    context.Response.Body = responseBody;

            watch = Stopwatch.StartNew();
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                watch.Stop();
                
            }



            //    //responseBodyContent = await ReadResponseBody(context);
            //    context.Response.Body.Seek(0, SeekOrigin.Begin);
            //    bodyAsText = await new StreamReader(context.Response.Body).ReadToEndAsync();
            //    context.Response.Body.Seek(0, SeekOrigin.Begin);

            //    await responseBody.CopyToAsync(originalBodyStream);


            //}
            //return bodyAsText;
            return watch.ElapsedMilliseconds;
        }

        private async Task<BEAppRequestResponseLog> BuildRequestMetadata(HttpContext context)
        {
            var request = context.Request;
            StringValues msisdn = GetMsisdn(request);


            IPHostEntry host = null;
            try
            {
                host = Dns.GetHostEntry(request.Host.Host);
            }
            catch
            {
            }

            var logHelper = new LogHelper();
            var log = new BEAppRequestResponseLog
            {
                Msisdn = msisdn,
                //RequestUri = $"{request.Scheme}://{request.Host}{request.Path}{request.QueryString}",
                RequestUri = $"{request.Host}{request.Path}{request.QueryString}",
                RequestBody = await new LogHelper().ReadRequestBody(request),
                ClienIipAddress = $"{context.Connection.RemoteIpAddress}",
                RequestHeaders = logHelper.CovertString(request.Headers),
                //RequestProtocol = request.Protocol,
                ServerName = host?.HostName,
                ServerIp = logHelper.CovertString(host?.AddressList)
            };
            request.Body.Position = 0;

            return log;
        }

        private BEAppRequestResponseLog BuildResponseMetadata(BEAppRequestResponseLog logMetadata,
            HttpResponse response, string responseBody)
        {
            //Stream stream = new MemoryStream();
            //response.Body.CopyTo(stream);

            logMetadata.ResponseBody = responseBody;
            logMetadata.ResponSestatusCode = response.StatusCode.ToString();
            logMetadata.ResponseHeaders = new LogHelper().CovertString(response.Headers);

            return logMetadata;
        }

        public string GetMsisdn(HttpRequest request)
        {
            StringValues msisdn;
            if (!string.IsNullOrEmpty(request.Query["msisdn"]))
                msisdn = request.Query["msisdn"];
            else
                request.Headers.TryGetValue("msisdn", out msisdn);
            return msisdn;
        }

        private bool SendToLog(BEAppRequestResponseLog data)
        {
            log.RequestResponse(data);
            // TODO: Write code here to store the logMetadata instance to a pre-configured log store...
            return true;
        }

        //private async Task<bool> SendToAuditTrail(BEAppAudiTtrail data)
        //{
        //    log.AuditTrail(data);
        //    return true;
        //}
    }
}