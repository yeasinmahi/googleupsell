﻿using System;
using System.IO;
using System.Reflection;

namespace LogService
{
    public static class LogConfig
    {
        private static string BasePath { get; set; }
        private static string ErrorFileName { get; set; }

        public static string BaseLogPath
        {
            get
            {
                if (string.IsNullOrWhiteSpace(BasePath))
                {
                    if (Assembly.GetEntryAssembly() != null)
                        return Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Log");
                    else
                    {
                        return "C:\\Log\\ApiHub2";
                    }
                }
                return BasePath;
            }
            set => BasePath = value;
        }

        public static string ErrorLogFileName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(ErrorFileName))
                    return DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
                return ErrorFileName;
            }
            set => ErrorFileName = value;
        }

        public static string ErrorLogFullPath
        {
            get => Path.Combine(BaseLogPath, ErrorLogFileName);
            private set { }
        }

        public static bool IsWriteErrorLogFile { get; set; }
        public static bool IsWriteErrorLogDb { get; set; }
        public static bool IsWriteRequestResponseFile { get; set; }
        public static bool IsWriteRequestResponseDb { get; set; }
        public static bool IsWriteRequestBodyDb { get; set; }
        public static bool IsWriteResponseBodyDb { get; set; }
        public static bool IsWriteTPSRequestLog { get; set; }
        public static bool EnableWriteDBBSErrorLog { get; set; }


        // public static bool IsWriteAuditTrailDb { get; set; } = true;
    }
}