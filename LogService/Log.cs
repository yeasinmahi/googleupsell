﻿using System;
using System.IO;
using System.Threading.Tasks;
using BusinessEntity.Logging;
using BusinessObject;

namespace LogService
{
    public class Log
    {
        private bool _isForceStopWriteLogDb;
        private bool _isWriteLogDb;
        private bool _isWriteLogFile;

        public Log()
        {
        }

        public Log(string msisdn)
        {
            this.Msisdn = msisdn;
        }

        public string Msisdn { get; set; }

        public async Task Error(Exception ex, bool isWriteFile, bool isWriteDb)
        {
            _isWriteLogFile = isWriteFile;
            await Error(ex, isWriteDb);
        }

        public async Task Error(Exception ex)
        {
            await Error(ex, "Error");
        }

        public async Task Error(Exception ex, string title)
        {
            await Error(ex, title, 0,string.Empty);
        }
        public async Task Error(Exception ex, double rtt, string request)
        {
            await Error(ex, "Error", rtt, request);
        }
        public async Task Error(Exception ex, string title,double rtt,string request)
        {
            var entities = new LogHelper().GetErrorEntity(ex, Msisdn, rtt, request);

            if (!_isForceStopWriteLogDb && (_isWriteLogDb || LogConfig.IsWriteErrorLogDb)) await WriteLogDb(entities);
            var s = new LogHelper().ConvertString(entities);
            await Info(s, title);
        }

        public async Task Error(Exception ex, bool isWriteDb)
        {
            if (!isWriteDb) _isForceStopWriteLogDb = true;
            _isWriteLogDb = isWriteDb;
            await Error(ex);
        }

        public async Task Info(string str)
        {
            await Info(str, "Info");
        }

        public async Task Info(string str, string title)
        {
            if (_isWriteLogFile || LogConfig.IsWriteErrorLogFile)
                await WriteLogFile(title + "\t" + str);
        }

        public async Task Info(string str, string title, bool isWriteFile)
        {
            _isWriteLogFile = isWriteFile;
            await Info(str, title);
        }

        public async Task RequestResponse(BEAppRequestResponseLog data)
        {
            await RequestResponse(data, "Request/Response");
        }

        public async Task RequestResponse(BEAppRequestResponseLog data, string title)
        {
            if (LogConfig.IsWriteRequestResponseFile)
            {
                var s = new LogHelper().ConvertString(data);
                await WriteLogFile(title + "\t" + s);
            }

            if (LogConfig.IsWriteRequestResponseDb) await WriteLogDb(data);
        }

        public async Task AuditTrail(BEAppAudiTtrail data)
        {
            //if (LogConfig.IsWriteAuditTrailDb)
            {
                //WriteLogDb(new Entities(data));
                await WriteLogDb(data);
            }
        }

        private async Task WriteLogFile(string msg)
        {
            try
            {
                var strFilePath = LogConfig.BaseLogPath;
                //var fileName = LogConfig.ErrorLogFileName;
                FileStream file;
                StreamWriter sw;
                if (!File.Exists(LogConfig.ErrorLogFullPath))
                {
                    if (!Directory.Exists(strFilePath)) Directory.CreateDirectory(strFilePath);
                    try
                    {
                        file = new FileStream(LogConfig.ErrorLogFullPath, FileMode.CreateNew, FileAccess.Write,
                            FileShare.Read);
                        sw = new StreamWriter(file);

                        await sw.WriteLineAsync("Date \t\tTime  \t\tTitle \t\tMessage");
                        await sw.WriteLineAsync(
                            "=====================================================================================================");
                        sw.Close();
                        file.Close();
                    }
                    catch (Exception)
                    {
                        // ignored
                    }
                }

                try
                {
                    file = new FileStream(LogConfig.ErrorLogFullPath, FileMode.Append, FileAccess.Write,
                        FileShare.Read);
                    sw = new StreamWriter(file);

                    var strMsg = DateTime.Now.ToString("dd:MM:yyyy") + "\t" + DateTime.Now.ToString("HH:mm:ss:fff") +
                                 "\t" + msg;
                    await sw.WriteLineAsync(strMsg);
                    sw.Close();
                    file.Close();
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        //private async Task WriteLogDb(Entities entities)
        //{
        //    try
        //    {
        //        if (entities.Error != null)
        //        {
        //            new LogDataAccess().InsertAppErrorLog(entities.Error);

        //        }
        //        if (entities.RequestResponseMeta != null)
        //        {
        //            new LogDataAccess().InsertAppRequestResponse(entities.RequestResponseMeta);

        //        }
        //        if (entities.AuditTrailMeta != null)
        //        {
        //            new BOAppAudiTtrail().Save()
        //            //new LogDataAccess().InsertAuditTrail(entities.AuditTrailMeta);

        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }

        //}

        private async Task WriteLogDb(BEAppErrorLog error)
        {
            await new BOAppErrorLog().Save(error);
        }

        private async Task WriteLogDb(BEAppRequestResponseLog appRequestResponseLog)
        {
            await new BOAppRequestResponseLog().Save(appRequestResponseLog);
        }

        private async Task WriteLogDb(BEAppAudiTtrail appAudiTtrail)
        {
            await new BOAppAudiTtrail().Save(appAudiTtrail);
        }
    }
}