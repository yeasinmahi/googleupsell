﻿namespace LogService
{
    public class Entities
    {
        public Entities(Error data)
        {
            Error = data;
        }

        public Entities(RequestResponseMeta data)
        {
            RequestResponseMeta = data;
        }

        public Entities(AuditTrailMeta data)
        {
            AuditTrailMeta = data;
        }

        public Error Error { get; set; }
        public RequestResponseMeta RequestResponseMeta { get; set; }
        public AuditTrailMeta AuditTrailMeta { get; set; }
    }

    public class Error
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public int LineNumber { get; set; }
        public int ColumnNumber { get; set; }
    }

    public class RequestResponseMeta
    {
        public int Id { get; set; }
        public string Msisdn { get; set; }
        public string RequestUri { get; set; }
        public string RequestBody { get; set; }
        public string RequestProtocol { get; set; }
        public string RequestHaders { get; set; }
        public string ClientIpAddress { get; set; }
        public string ResponseBody { get; set; }
        public int ResponseStatusCode { get; set; }
        public string ResponseHaders { get; set; }
        public string ServerName { get; set; }
        public string ServerIp { get; set; }
    }

    public class AuditTrailMeta
    {
        public int Id { get; set; }
        public string Msisdn { get; set; }
        public string RequestUrl { get; set; }
        public string RequestQueryString { get; set; }
        public string Method { get; set; }
        public string Controller { get; set; }
        public string Param { get; set; }
        public long Rtt { get; set; }
        public string ClientIpAddress { get; set; }
        public string ApiVersion { get; set; }
        public string ErrorMessage { get; set; }
        public string ServiceUserId { get; set; }
    }

    public class RouteValue
    {
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Version { get; set; }
    }
}