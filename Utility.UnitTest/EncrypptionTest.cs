using NUnit.Framework;

namespace Utility.UnitTest
{
    public class EncrypptionTest
    {
        private string secret;

        [SetUp]
        public void Setup()
        {
            secret = "b14ca5898a4e4133bbce2ea2315a1916";
        }


        [TestCase("234c@#Nn%43v05n9654", "MjM0Y0AjTm4lNDN2MDVuOTY1NA==")]
        [TestCase("x23x940587@$#&)(_*(v nu~fg KD", "eDIzeDk0MDU4N0AkIyYpKF8qKHYgbnV+ZmcgS0Q=")]
        public void EncryptBase64Test(string value, string result)
        {
            Assert.That(result == new Encryption().EncryptBase64(value));
        }

        [TestCase("MjM0Y0AjTm4lNDN2MDVuOTY1NA==", "234c@#Nn%43v05n9654")]
        [TestCase("eDIzeDk0MDU4N0AkIyYpKF8qKHYgbnV+ZmcgS0Q=", "x23x940587@$#&)(_*(v nu~fg KD")]
        public void DecryptBase64Test(string value, string result)
        {
            Assert.That(result == new Encryption().DecryptBase64(value));
        }

        [TestCase("MjM0Y0AjTm4lNDN2MDVuOTY1NA==", "1t/kevvdyP5c9DF+/sY4uMznnY+JjpFRJtjIU7C85kM=")]
        [TestCase("eDIzeDk0MDU4N0AkIyYpKF8qKHYgbnV+ZmcgS0Q=",
            "uUjIQ482S89ijhIul3ndJriUmQ69aTlAnRAVIhrO4AkemknVwacsNnD7HHxBm/Ub")]
        public void EncryptAesTest(string value, string result)
        {
            var r = new Encryption().EncryptAes(value, secret);
            Assert.That(result == r);
        }

        [TestCase("1t/kevvdyP5c9DF+/sY4uMznnY+JjpFRJtjIU7C85kM=", "MjM0Y0AjTm4lNDN2MDVuOTY1NA==")]
        [TestCase("uUjIQ482S89ijhIul3ndJriUmQ69aTlAnRAVIhrO4AkemknVwacsNnD7HHxBm/Ub",
            "eDIzeDk0MDU4N0AkIyYpKF8qKHYgbnV+ZmcgS0Q=")]
        public void DecryptAesTest(string value, string result)
        {
            var r = new Encryption().DecryptAes(value, secret);
            Assert.That(result == r);
        }

        [TestCase("MjM0Y0AjTm4lNDN2MDVuOTY1NA==", "88dbfac9a5d01325432ef8592b58e0bf5d0473d5")]
        [TestCase("eDIzeDk0MDU4N0AkIyYpKF8qKHYgbnV+ZmcgS0Q=", "9c47f50eeda240d01ee2cf30ad50526dc9f3e16f")]
        public void EncryptSha1Test(string value, string result)
        {
            var r = new Encryption().EncryptSha1(value);
            Assert.That(result == r);
        }

        [TestCase("MjM0Y0AjTm4lNDN2MDVuOTY1NA==",
            "e860f5f5a517db2d56f50d466c59dabcfe89e7a4872657a0b5a8bb47f2ee4711d48ffca72ab97f3810eecd4b44e5c50ad58273aedcb731aba18be490b06f08cd")]
        [TestCase("eDIzeDk0MDU4N0AkIyYpKF8qKHYgbnV+ZmcgS0Q=",
            "ab8e10754c8418ab419592f788b3f8a0b50eaec5560c758ddd72549c5e0a5dd7b3038187cf89dc9cf251bb95c4ebe10b5ed11566f8c63b6008e34f4198e16c67")]
        public void EncryptSha3Test(string value, string result)
        {
            var r = new Encryption().EncryptSha3(value);
            Assert.That(result == r);
        }

        [TestCase("MjM0Y0AjTm4lNDN2MDVuOTY1NA==", "88dbfac9a5d01325432ef8592b58e0bf5d0473d5", true)]
        [TestCase("eDIzeDk0MDU4N0AkIyYpKF8qKHYgbnV+ZmcgS0Q=", "9c47f50eeda240d01ee2cf30ad50526dc9f3e16f", true)]
        [TestCase("eDIzeDk0MDU4N0AkIyYpKF8qKHYgbnV+ZmcgS0Q=", "9e47f50eeda240d01ee2cf30ad50526dc9f3e16f", false)]
        public void IsSHA1HashEqualTest(string plaintext, string hashValue, bool expectedOutput)
        {
            var r = new Encryption().IsSHA1HashEqual(hashValue, plaintext);
            Assert.That(expectedOutput == r);
        }

        [TestCase("MjM0Y0AjTm4lNDN2MDVuOTY1NA==",
            "e860f5f5a517db2d56f50d466c59dabcfe89e7a4872657a0b5a8bb47f2ee4711d48ffca72ab97f3810eecd4b44e5c50ad58273aedcb731aba18be490b06f08cd",
            true)]
        [TestCase("eDIzeDk0MDU4N0AkIyYpKF8qKHYgbnV+ZmcgS0Q=",
            "ab8e10754c8418ab419592f788b3f8a0b50eaec5560c758ddd72549c5e0a5dd7b3038187cf89dc9cf251bb95c4ebe10b5ed11566f8c63b6008e34f4198e16c67",
            true)]
        [TestCase("eDIzeDk0MDU4N0AkIyYpKF8qKHYgbnV+ZmcgS0Q=",
            "ab8c10754c8418ab419592f788b3f8a0b50eaec5560c758ddd72549c5e0a5dd7b3038187cf89dc9cf251bb95c4ebe10b5ed11566f8c63b6008e34f4198e16c67",
            false)]
        public void IsSHA3HashEqualTest(string plaintext, string hashValue, bool expectedOutput)
        {
            var r = new Encryption().IsSHA3HashEqual(hashValue, plaintext);
            Assert.That(expectedOutput == r);
        }
    }
}