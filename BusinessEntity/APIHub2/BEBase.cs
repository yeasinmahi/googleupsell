﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BusinessEntity.APIHub2
{
    public class BEBaseSearch
    {
        private DateTime createDateRangeEndDate = DateTime.Today;


        private DateTime createDateRangeStartDate = DateTime.Today.AddDays(-30);
        private DateTime tranDateRangeEndDate = DateTime.Today;

        private DateTime tranDateRangeStartDate = DateTime.Today.AddDays(-30);
        public DateTime FromDate { set; get; } = DateTime.Today.AddDays(1);

        public DateTime ToDate { set; get; } = DateTime.Today.AddDays(-30);

        //public int noOfRecord { set; get; } = 20;

        public string SortColumnName { get; set; }
        public string SortDirection { get; set; }
        public string TranDateRange { get; set; }
        public string CreateDateRange { get; set; }

        public DateTime TranDateRangeStartDate
        {
            get
            {
                if (!string.IsNullOrEmpty(TranDateRange))
                {
                    var str = TranDateRange.Split('-');

                    tranDateRangeStartDate = Convert.ToDateTime(str[0]);
                }

                return tranDateRangeStartDate;
            }
        }

        public DateTime TranDateRangeEndDate
        {
            get
            {
                if (!string.IsNullOrEmpty(TranDateRange))
                {
                    var str = TranDateRange.Split('-');

                    tranDateRangeEndDate = Convert.ToDateTime(str[1]);
                }

                return tranDateRangeEndDate;
            }
        }

        public DateTime CreateDateRangeStartDate
        {
            get
            {
                if (!string.IsNullOrEmpty(CreateDateRange))
                {
                    var str = CreateDateRange.Split('-');

                    createDateRangeStartDate = Convert.ToDateTime(str[0]);
                }

                return createDateRangeStartDate;
            }
        }

        public DateTime CreateDateRangeEndDate
        {
            get
            {
                if (!string.IsNullOrEmpty(CreateDateRange))
                {
                    var str = CreateDateRange.Split('-');

                    createDateRangeEndDate = Convert.ToDateTime(str[1]);
                }

                return createDateRangeEndDate;
            }
        }

        public int RecordCount { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; } = 1;

        public void SetTranDateRange()
        {
            SetTranDateRange(FromDate, ToDate);
        }

        public void SetTranDateRange(DateTime fromDate, DateTime toDate)
        {
            //Mar 29, 19 - Apr 28, 19
            TranDateRange = string.Format("{0} - {1}", fromDate.ToString("MMM dd, yy"), toDate.ToString("MMM dd, yy"));
        }
    }

    public class BEBase
    {
        [JsonIgnore] public string ID { set; get; } = string.Empty;

        [JsonIgnore] public bool IsNew { get; set; } = true;

        [JsonIgnore] public bool IsActive { get; set; } = false;

        [JsonIgnore] public bool IsChecked { get; set; } = false;

        [JsonIgnore] public bool IsDeleted { get; set; } = false;

        [JsonIgnore] public string CreatedBy { set; get; } = string.Empty;

        [JsonIgnore] public DateTime CreatedDate { set; get; } = DateTime.Now;

        [JsonIgnore] public string UpdatedBy { set; get; } = string.Empty;

        [JsonIgnore] public DateTime UpdatedDate { set; get; } = DateTime.MinValue;

        [JsonIgnore] public int SerialNo { set; get; } = 0;

        [JsonIgnore] public string DeletedBy { set; get; } = string.Empty;

        [JsonIgnore] public DateTime DeletedDate { set; get; } = DateTime.MinValue;

        [JsonIgnore] public string ObjectSource { set; get; } = string.Empty;

        [JsonIgnore] public int TotalRows { set; get; } = 0;
    }

    /// <summary>
    ///     Object of BEBaseSearch
    /// </summary>
    public class BEBaseList<T> : List<T>
    {
        public long TotalRecords { get; set; }
    }

    public class BEPagination
    {
        public int PageIndex;

        public BEPagination(int pageSize, int totalRecords, int pageNumber)
        {
            PageIndex = pageNumber;
            PageSize = pageSize;
            RecordCount = totalRecords;
            //SetTotalPage(pageSize, totalRecords);
        }

        public int PageSize { get; } = 20;

        public int RecordCount { get; }

        //private int totalPage = 0;
        //public int TotalPage
        //{
        //    get
        //    {
        //        return totalPage;
        //    }
        //}

        public int TotalPage
        {
            get
            {
                //this.pageSize = pageSize;
                //this.totalRecords = totalNoOfRecord;

                var totalPage = 0;
                double pageCount = 0;

                if (RecordCount > 0)
                    pageCount = (double) (RecordCount / Convert.ToDecimal(PageSize));
                totalPage = (int) Math.Ceiling(pageCount);
                return totalPage;
            }
        }

        public BEBaseList<int> PageList
        {
            get
            {
                var pageList = new BEBaseList<int>();
                if (TotalPage <= 1) return pageList;

                if (TotalPage <= 15)
                {
                    for (var i = 1; i <= TotalPage; i++) pageList.Add(i);
                }
                else
                {
                    pageList.Add(1);
                    pageList.Add(2);

                    //if (TotalPage < 15)
                    //{
                    //for current page
                    var fromPage = PageIndex - 3;
                    if (fromPage < 3) fromPage = 3;
                    var toPage = PageIndex + 3;
                    if (toPage > TotalPage - 2) toPage = TotalPage - 2;
                    for (var i = fromPage; i <= toPage; i++) pageList.Add(i);
                    //}

                    //int pageDiff = TotalPage - toPage;

                    pageList.Add(TotalPage - 1);
                    pageList.Add(TotalPage);
                }

                return pageList;
            }
        }

        ~BEPagination()
        {
        }

        //private void SetTotalPage(int pageSize, int totalRecords) {

        //    this.pageSize = pageSize;
        //    this.totalRecords = totalRecords;

        //        int totalPage = 0;
        //        double pageCount = 0;

        //        if (totalNoOfRecord > 0)
        //            pageCount = (double)((decimal)totalRecords / Convert.ToDecimal(pageSize));
        //        totalPage = (int)Math.Ceiling(pageCount);           
        //}
    }
}