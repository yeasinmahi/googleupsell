using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BusinessEntity.APIHub2
{
    #region Object of BENew_App_Bonus

    public class BENew_App_Bonus // : BEBase
    {
        #region Destructor

        ~BENew_App_Bonus()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Property

        [Display(Name = "Id")] public int Id { get; set; }

        [Display(Name = "Msisdn")] public string Msisdn { get; set; }

        [Display(Name = "Browsedt")] public DateTime Browsedt { get; set; }

        [Display(Name = "Create Date")] public DateTime CreateDate { get; set; }

        [Display(Name = "Browse Source")] public int BrowseSource { get; set; }

        [Display(Name = "Bonus Type")] public string BonusType { get; set; }

        [Display(Name = "Is Processed")] public int IsProcessed { get; set; }

        [Display(Name = "Processe Date")] public DateTime ProcesseDate { get; set; }

        #endregion
    }

    #endregion

    #region Collectin Object of BENew_App_Bonus

    public class BENew_App_Bonuss : List<BENew_App_Bonus>
    {
        #region Destructor

        ~BENew_App_Bonuss()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Functions

        #endregion
    }

    #endregion
}