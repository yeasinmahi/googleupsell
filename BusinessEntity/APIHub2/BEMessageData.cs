﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace BusinessEntity.APIHub2
{
    public class BEMessageData<T> : BEMessageError
    {
        public int Status { get; set; } = BEMessageCodes.SuccessCode.Status;

        public string Message { get; set; }
        public DateTime MessageTime { get; set; } = DateTime.Now;
        public string Provider { get; } = AppSettings.Provider;
        public T Data { set; get; }

        public double RTT
        {
            get
            {
                var span = DateTime.Now - MessageTime;
                return Math.Round(span.TotalMilliseconds, 4);
            }
        }

        //public bool CheckArguments(string msisdn, string subcriptionId)
        //{
        //    //response = new BEMessageData<T>();
        //    if (string.IsNullOrEmpty(msisdn) && string.IsNullOrEmpty(subcriptionId))
        //    {
        //        this.Status = BEMessageCodes.InvalidArguments.Status;
        //        this.Error = BEMessageCodes.InvalidArguments.error;
        //        this.Message = "Please provide msisdn or subcription Id";
        //        this.Errors.Add("ErrorDescription", new[]{ "Please provide at least one between msisdn or subcription Id" });
        //        //this.ErrorDescription = "Please provide at least one between msisdn or subcription Id";
        //        return false;
        //    }
        //    return true;
        //}


        public void MapBEHandledException(BeHandledException handledException)
        {
            Status = handledException.Status;
            Error = handledException.Error;
            var errorMessages = new List<string>();
            if (!string.IsNullOrEmpty(handledException.ErrorDescription))
                errorMessages.Add(handledException.ErrorDescription);

            if (!string.IsNullOrEmpty(handledException.CustomerMessage))
                errorMessages.Add(handledException.CustomerMessage);
            //this.Errors.Add("ErrorDescription", new[] { handledException.ErrorDescription, handledException.CustomerMessage });
            Errors.Add("ErrorDescription", errorMessages.ToArray());
            //this.ErrorDescription = handledException.ErrorDescription;
            Message = handledException.CustomerMessage;
        }

        public void MapBEHandledException(BeHandledException handledException, string customerMessage)
        {
            MapBEHandledException(handledException);
            Message = customerMessage;
        }


        public void MapException(Exception ex)
        {
            Status = BEMessageCodes.InternaError.Status;
            Error = BEMessageCodes.InternaError.error;
            var errorMessages = new List<string>();
            if (!string.IsNullOrEmpty(ex.Message))
                errorMessages.Add(ex.Message);

            //this.Errors.Add("ErrorDescription", new[] { handledException.ErrorDescription, handledException.CustomerMessage });
            Errors.Add("ErrorDescription", errorMessages.ToArray());
            //this.ErrorDescription = ex.Message;
            Message = "Request could not be processed due to technical error";
        }

        public void MapException(Exception ex, string customerMessage)
        {
            MapException(ex);
            Message = customerMessage;
        }


        public void AddModelErrorMessages(ModelStateDictionary modelState, string customerMessage = null)
        {
            //BEMessageData<string> message = new BEMessageData<string>();
            if (!modelState.IsValid)
            {
                foreach (var keyModelStatePair in modelState)
                {
                    var key = keyModelStatePair.Key;
                    var errors = keyModelStatePair.Value.Errors;
                    if (errors != null && errors.Count > 0)
                    {
                        if (errors.Count == 1)
                        {
                            var errorMessage = GetErrorMessage(errors[0]);
                            //Errors.Add(key, new[] { errorMessage });
                            Errors.Add(key, new[] {errorMessage});
                        }
                        else
                        {
                            var errorMessages = new string[errors.Count];
                            for (var i = 0; i < errors.Count; i++) errorMessages[i] = GetErrorMessage(errors[i]);

                            //Errors.Add(key, errorMessages);
                            Errors.Add(key, errorMessages);
                        }
                    }
                }

                if (string.IsNullOrEmpty(customerMessage))
                    customerMessage = "The input was not valid.";
                throw new BeHandledException(BEMessageCodes.BadRequest.Status, BEMessageCodes.BadRequest.error,
                    customerMessage);
            }
        }

        private string GetErrorMessage(ModelError error)
        {
            return string.IsNullOrEmpty(error.ErrorMessage) ? "The input was not valid." : error.ErrorMessage;
        }
    }


    public class BEMessageError
    {
        public string Error { get; set; } = string.Empty;

        public IDictionary<string, string[]> Errors { get; set; } = new Dictionary<string, string[]>();
        //public string ErrorDescription { get; set; } = string.Empty;
    }
}