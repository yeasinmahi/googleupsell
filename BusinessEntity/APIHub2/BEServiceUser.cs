using System.Collections.Generic;
using System.Security.Claims;

namespace BusinessEntity.APIHub2
{
    #region Object of BEServiceUserInfo

    public class BEServiceUser : BEBase
    {
        #region Destructor

        ~BEServiceUser()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Property

        public int UserID { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string InVasUserName { get; set; }

        public string InVasPassword { get; set; }

        public string Description { get; set; }

        public string LMSWebServiceUser { get; set; }

        public string LMSWebServicePassword { get; set; }
        public string DBSSUserName { get; set; }
        public string DBSSPassword { get; set; }
        public string LoxySoftUserName { get; set; }
        public string LoxySoftPassword { get; set; }
        public string LoxySoftChannelName { get; set; }

        public int CMSChanel { get; set; }

        public int CMSSalesChannel { get; set; }

        public string MotherMobileNo { get; set; }
        public string MotherSubscriptionId { get; set; }
        public string MotherCompanyName { get; set; }

        public int LMSChanel { get; set; }

        public int LMSSalesChannel { get; set; }

        public string LMSUSER { get; set; }
        public string LMSPASSWORD { get; set; }

        public string ClientID { get; set; }
        public string ClientSecret { get; set; }

        public bool IsLocked { get; set; }
        public int Serial { get; set; }

        public List<Claim> Claims { get; set; } = new List<Claim>();
        public Dictionary<string, string> APIPermissions = new Dictionary<string, string>();

        #endregion
    }

    #endregion

    #region Collectin Object of BEServiceUserInfo

    /// <exclude />
    public class BEServiceUsers : BEBaseList<BEServiceUser>
    {
        #region Destructor

        ~BEServiceUsers()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Functions

        #endregion
    }

    #endregion
}