using System;
using System.ComponentModel.DataAnnotations;

namespace BusinessEntity.APIHub2
{
    //public enum SourceEnum
    //{

    //	baggShare = 1,
    //	baggShareAdmin = 2,
    //	Facebook = 3,
    //	BaggShareApp = 4,
    //	BaggShareLiteApp = 5
    //}

    #region Object of BEAccessLog

    public class BEAccessLog : BEBase
    {
        #region Destructor

        ~BEAccessLog()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Property

        [Required]
        [Display(Name = "Access Log ID")]
        public int AccessLogID { get; set; }

        [Required] [Display(Name = "MSISDN")] public string MSISDN { get; set; }

        [Required]
        [Display(Name = "Browse DT")]
        public DateTime BrowseDT { get; set; }

        [Required]
        [Display(Name = "Browse URL")]
        public string BrowseURL { get; set; }

        [Display(Name = "RootPath")] public string RootPath { get; set; }


        [Display(Name = "QueryString")] public string QueryString { get; set; }

        [Display(Name = "IPAddress")] public string IPAddress { get; set; }

        [Display(Name = "Browser Info Device ID")]
        public string BrowserInfoDeviceID { get; set; }

        [Display(Name = "Session ID")] public string SessionID { get; set; }

        [Display(Name = "ServiceUserID")] public int ServiceUserID { get; set; }

        [Display(Name = "API Version")] public string APIVersion { get; set; }

        [Display(Name = "Token Number")] public string TokenNumber { get; set; }

        [Display(Name = "Admin User ID")] public string AdminUserID { get; set; }

        [Display(Name = "Error Message")] public string ErrorMessage { get; set; }

        [Display(Name = "Http Method")] public string HttpMethod { get; set; }

        [Display(Name = "MethodName")] public string MethodName { get; set; }

        [Display(Name = "Version")] public string Version { get; set; }

        #endregion
    }

    #endregion

    #region Collectin Object of BEAccessLog

    public class BEAccessLogs : BEBaseList<BEAccessLog>
    {
        #region Destructor

        ~BEAccessLogs()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Functions

        #endregion
    }

    #endregion
}