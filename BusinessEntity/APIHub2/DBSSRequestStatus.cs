﻿namespace BusinessEntity.APIHub2
{
    public class BEProductPurchaseStatus
    {
        public string Id { get; set; }


        public string ScheduledAt { get; set; }


        public string Status { get; set; }
    }
}