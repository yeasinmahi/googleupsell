﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BusinessEntity.APIHub2
{
    #region Object of BEDataProduct

    public class BEDataProduct : BEBase
    {
        #region Destructor

        ~BEDataProduct()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Property

        [Display(Name = "Data Product Name Display")]
        public string DataProductNameDisplay { get; set; }

        [Display(Name = "Data Product Id")] public int DataProductId { get; set; }

        //[Display(Name = "Data Product Type Id")]
        //public int DataProductTypeId { get; set; }

        [Display(Name = "Data Product Name")] public string DataProductName { get; set; }

        [Display(Name = "Description")] public string Description { get; set; }

        [Display(Name = "Related Url")] public string RelatedUrl { get; set; }

        [Display(Name = "Data Code")] public string DataCode { get; set; }

        [Display(Name = "Auto Renew Code")] public string AutoRenewCode { get; set; }

        [Display(Name = "Short Code")] public string ShortCode { get; set; }

        [Display(Name = "Display Order")] public int DisplayOrder { get; set; }

        [Display(Name = "Price")] public string Price { get; set; }

        [Display(Name = "Volume")] public string Volume { get; set; }

        [Display(Name = "Validity")] public string Validity { get; set; }

        [Display(Name = "Product Family Id")] public int ProductFamilyId { get; set; }

        [Display(Name = "Activation Message")] public string ActivationMessage { get; set; }

        [Display(Name = "Deactivation Message")]
        public string DeactivationMessage { get; set; }

        #endregion
    }

    #endregion

    #region Collectin Object of BEDataProduct

    public class BEDataProducts : BEBaseList<BEDataProduct>
    {
        #region Constructor

        public BEDataProducts()
        {
            RefreshStartTime = DateTime.Now;
        }

        #endregion

        public DateTime RefreshStartTime { get; set; }
        public DateTime RefreshEndTime { get; set; }

        public double ExecutionTime
        {
            get
            {
                var span = RefreshEndTime - RefreshStartTime;
                return span.TotalMilliseconds;
            }
        }

        #region Destructor

        ~BEDataProducts()
        {
        }

        #endregion

        #region Functions

        #endregion
    }

    #endregion
}