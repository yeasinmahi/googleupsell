﻿using System;

namespace BusinessEntity.APIHub2
{
    public class BECommon
    {
        public string BrowserDetails(string service, string method, string[] param)
        {
            var output = string.Empty;

            try
            {
                output = "Service:" + service + "|| Method:" + method + "|| Param:";
                foreach (var p in param)
                    output += "||" + p;
                //   output += output + "||" + p;
                //output += output + "," + p.Trim();

                if (!string.IsNullOrEmpty(output) && output.Length > 300)
                    output = output.Substring(0, 299);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }
    }
}