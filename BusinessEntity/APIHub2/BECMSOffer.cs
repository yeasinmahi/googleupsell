﻿namespace BusinessEntity.APIHub2
{

    public class BEMyOffer
    {
        //private int p1;
        //private string p2;
        //private string p3;
        //private int p4;
        //private object p5;
        //private string p6;

        public BEMyOffer()
        {

        }

        public BEMyOffer(long offerId, string offer, string description, string offerType)
        {
            OfferId = offerId;
            Offer = offer;
            Description = description;
            OfferType = offerType;
        }

        public BEMyOffer(long offer_id, string offer_type_name, string offer_name,
            int position, string offer_category_name, string action_type_name
            )
        {
            OfferId = offer_id;
            OfferType = offer_type_name;
            Offer = offer_name;
            this.position = position;
            this.offer_category_name = offer_category_name;
            this.action_type_name = action_type_name;
        }

        public BEMyOffer(long offer_id, string offer_type_name, string offer_name,
           int position, string offer_short_description, string offer_code, string offer_long_description, string url_01, string url_02, string url_03, string url_04,
           string extra_info,
           string offer_category_name, string action_type_name
           )
        {
            OfferId = offer_id;
            OfferType = offer_type_name;
            Offer = offer_name;
            this.position = position;
            Description = offer_long_description;

            this.offer_category_name = offer_category_name;
            this.action_type_name = action_type_name;
        }

        public BEMyOffer(long offer_id, string offer_type_name, string offer_name,
          int position, string offer_long_description,
          string offer_category_name, string action_type_name, double price
          )
        {
            OfferId = offer_id;
            OfferType = offer_type_name;
            Offer = offer_name;
            this.position = position;
            Description = offer_long_description;
            this.offer_category_name = offer_category_name;
            this.offer_category_name = offer_category_name;
            Price = price;
        }


        public long OfferId { get; set; }

        public double Price { get; set; }


        public string Offer { get; set; }


        public string Description { get; set; }


        public string OfferType { get; set; }


        public int position { get; set; }


        public string offer_category_name { get; set; }

        public string action_type_name { get; set; }

        //end
    }

    #region Collectin Object of BEAlert
    /// <exclude />
    /// 

    //public class BEMyOffers
    //{
    //    public BEMyOffers()
    //    {
    //        OfferItems = new List<BEMyOffer>();
    //    }
    //    //[XmlElement("BEMyOffer")]
    //    public List<BEMyOffer> OfferItems { get; set; }
    //    public string ErrorMessage { get; set; }
    //    public string ErrorCode { get; set; }

    //}

    public class BEMyOfferDetail
    {
        public BEMyOfferDetail()
        {
            OfferItem = new BEMyOffer();
        }

        //[XmlElement("BEMyOfferDetail")]
        public BEMyOffer OfferItem { get; set; }
    }




    //public class BEAcceptMyOfferApiRequest
    //{
    //    public BEAcceptMyOfferApiRequest(ulong Msisdn, int channelId, long offerId)
    //    {
    //        offer_id = offerId;
    //        channel_id = channelId;
    //        msisdn = Msisdn;
    //    }
    //    public ulong msisdn { get; set; }
    //    public int channel_id { get; set; }
    //    public long offer_id { get; set; }
    //    public long? user_id { get; set; }
    //}

    //public class BEAcceptMyOfferApiResponse
    //{
    //    public BEAcceptMyOfferApiResponse()
    //    {
    //        // this.MessageTime = DateTime.Now;
    //        //this.IsSuccess = false;
    //    }
    //    //public bool IsSuccess { get; set; }
    //    // public DateTime MessageTime { get; set; }
    //    //public string Provider { get; set; }
    //    public string TransectionID { get; set; }
    //    public string ErrorCode { get; set; }
    //    public string ErrorType { get; set; }
    //    public string ErrorMessage { get; set; }
    //    public string Message { get; set; }
    //    public string MessageCode { get; set; }

    //}

    
    public class BEPurchaseOffer
    {
        //public bool IsSuccess { get; set; }
        //public string msisdn { get; set; }
        //public string offerId { get; set; }

        //public DateTimeOffset MessageTime { get; set; }
        //public string Provider { get; set; }

        public string TransectionID { get; set; }
        //public string Message { get; set; }
        //public int Status { get; set; }
    }



   
}
#endregion

