﻿using System;

namespace BusinessEntity.APIHub2
{
    public class BeHandledException : Exception
    {
        public BeHandledException()
        {
        }

        public BeHandledException(int status, string error)
            : this(status, error, string.Empty)
        {
        }

        public BeHandledException(int status, string error, string customerMessage)
            : this(status, error, string.Empty,customerMessage)
        {
            //this.Status = status;
            //this.Error = error;
            //this.ErrorDescription = errorDescription;
        }

        public BeHandledException(int status, string error, string errorDescription, string customerMessage)
            : this(status, error, errorDescription, customerMessage, null)
        {
            //this.Status = status;
            //this.Error = error;
            //this.ErrorDescription = errorDescription;
            //this.CustomerMessage = customerMessage;
        }

        public BeHandledException(int status, string error, string errorDescription, string customerMessage,
            Exception inner)
            : base(error, inner)
        {
            Status = status;
            Error = error;
            ErrorDescription = errorDescription;
            CustomerMessage = customerMessage;
        }

        public int Status { get; set; }
        public string Error { get; set; }
        public string ErrorDescription { get; set; }
        public string CustomerMessage { get; set; }

        //public BEHandledException(int errorCode, string message)
        //    : base(message)
        //{
        //    this.ErrorCode = errorCode;
        //}
        //public BEHandledException(int errorCode, string message,string errorDescription)
        //    : base(message)
        //{
        //    this.ErrorCode = errorCode;
        //    this.ErrorDescription = errorDescription;
        //}

        //public BEHandledException(int errorCode, string message, Exception inner)
        //    : base(message, inner)
        //{
        //    this.ErrorCode = errorCode;
        //}


        ////////////////
        ///
        public string GetMessage
        {
            get
            {
                if (string.IsNullOrEmpty(CustomerMessage))
                    return Message;
                return $"{Message}|{CustomerMessage}";
            }
        }
    }
}