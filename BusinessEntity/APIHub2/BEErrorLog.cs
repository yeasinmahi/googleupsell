﻿//using System;
//using System.Collections.Generic;

//namespace BusinessEntity.APIHub2
//{
//    #region Object of BEErrorLog
//    /// <summary>
//    /// Object of BEErrorLog
//    /// </summary>
//    public class BEErrorLog
//    {
//        #region Constructor
//        public BEErrorLog()
//        {
//        }
//        #endregion

//        #region Destructor
//        ~BEErrorLog() { }
//        #endregion

//        #region Property

//        public int ID { get; set; }
//        public string MSISDN { get; set; }
//        public DateTime LogTime { get; set; }
//        public string Message { get; set; }
//        public string StackTrace { get; set; }
//        public string RequestURL { get; set; }
//        public string RequestMethod { get; set; }
//        public string RequestFrom { get; set; }
//        public string Response { get; set; }
//        public string Request { get; set; }
//        public double RTT { get; set; }
//        public bool IsActive { get; set; }

//        public string LogDateTimeStr { get { return LogTime.ToString("dd MMM, yyyy hh:mm:ss tt"); } }


//        #endregion


//    }
//    #endregion

//    #region Collectin Object of BEErrorLog
//    /// <exclude />
//    public class BEErrorLogs : BEBaseList<BEErrorLog>
//    {
//        #region Constructor
//        public BEErrorLogs() { }
//        #endregion

//        #region Destructor
//        ~BEErrorLogs() { }
//        #endregion

//        #region Functions
//        #endregion
//    }
//    #endregion

//}

