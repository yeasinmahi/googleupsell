﻿namespace BusinessEntity.APIHub2
{
    public class BEInterest : BEBase
    {
        public int InterestId { set; get; }
        public string InterestName { set; get; }

        ~BEInterest()
        {
        }
    }

    /// <exclude />
    public class BEInterests : BEBaseList<BEInterest>
    {
        ~BEInterests()
        {
        }
    }
}