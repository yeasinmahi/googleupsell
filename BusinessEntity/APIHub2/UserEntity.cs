﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BusinessEntity.RequestResponseModels;

namespace BusinessEntity.APIHub2
{
    public class UserEntity
    {
    }

    public enum OTPForEnum
    {
        [Description("ForgetPassword")] ForgetPassword = 1,
        [Description("Registration")] Registration = 2,
        [Description("OneTimeLogin")] OneTimeLogin = 3,

        [Description("OneTimeActivity")] // For data pack purchase or any other things
        OneTimeActivity = 4
    }


    public class SendOTPRequest
    {
        [Required] public OTPForEnum OTPFor { get; set; }

        [Required] public bool AllOperator { get; set; }

        //  [Required]
        //  [BindProperty]
        ////  [BindProperty(SupportsGet = true)]
        //  [StringLength(10, MinimumLength = 10, ErrorMessage = "MSISDN must be 10 char")]
        // public string MSISDN { get; set; }
    }

    public class IsValidOTPRequest : BaseRequest
    {
        public OTPForEnum OTPFor { get; set; }

        public bool IsOtherOperator { get; set; }

        //public string Msisdn { get; set; }
        public string OTP { get; set; }
    }
}