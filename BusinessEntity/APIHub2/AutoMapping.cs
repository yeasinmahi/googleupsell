﻿using AutoMapper;
using BusinessEntity.RequestResponseModels;

namespace BusinessEntity.APIHub2
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<PurchaseOffer, BEMyOffer>();
            //CreateMap<BEMessageCodes.InvalidClientIDSecret, BEMessageData<string>>(); // means you want to map from InvalidRole to BEMessageData
            //CreateMap<BEMessageCodes.InvalidCustomerPassword, BEMessageData<string>>();
            //CreateMap<BEMessageCodes.InvalidToken, BEMessageData<string>>();
            //CreateMap<BEMessageCodes.InvalidGrandType, BEMessageData<string>>();
            //CreateMap<BEMessageCodes.InvalidRefreshToken, BEMessageData<string>>();
            //CreateMap<BEMessageCodes.InvalidRole, BEMessageData<string>>();

            //foreach (Type item in typeof(BEMessageCodes).GetNestedTypes())
            //{

            //    CreateMap<BEMessageCodes.InvalidRole, BEMessageData<string>>();
            //}

            //         CreateMap<BEMessageCodes, BEMessageData<string>>()
            //.ForMember(pts => pts.GetType, BEMessageData<string>());
        }
    }
}