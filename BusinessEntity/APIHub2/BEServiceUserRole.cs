﻿namespace BusinessEntity.APIHub2
{
    #region Object of BEServiceUserInfo

    public class BEServiceUserRole : BEBase
    {
        #region Destructor

        ~BEServiceUserRole()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Property

        public int RoleID { get; set; }

        public string RoleName { get; set; }

        public int UserID { get; set; }

        public int ServiceUserRoleID { get; set; }

        #endregion
    }

    #endregion

    #region Collectin Object of BEServiceUserInfo

    /// <exclude />
    public class BEServiceUserRoles : BEBaseList<BEServiceUserRole>
    {
        #region Destructor

        ~BEServiceUserRoles()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Functions

        #endregion
    }

    #endregion
}