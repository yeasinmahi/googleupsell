﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace BusinessEntity.APIHub2
{
    public class BEMessageCodes
    {
   
        // Standard HTTP Status
        public static class SuccessCode
        {
            public static int Status = 200;
            public static string error = "The request executed successfully";
        }

        public static class BadRequest
        {
            public static int Status = 400;
            public static string error = "BadRequest";
        }

        public static class UnauthorizedError
        {
            public static int Status = 401;
            public static string error = "Your not authorized to access this API";
        }

        public static class InvalidCustomerPassword
        {
            public static int Status = 401;
            public static string error = "Invalid customer MSISDN or password";
        }

        public static class InvalidClientIDSecret
        {
            public static int Status = 403;
            public static string error = "Invalid client id or secret";
        }

        public static class InvalidOTP
        {
            public static int Status = 403;
            public static string error = "InvalidOTP";
        }

        public static class InvalidGrandType
        {
            public static int Status = 403;
            public static string error = "Invalid Grand Type";
        }

        public static class InvalidToken
        {
            public static int Status = 403;
            public static string error = "Invalid Token";
        }

        public static class InvalidRefreshToken
        {
            public static int Status = 403;
            public static string error = "Invalid Refresh Token";
        }

        // can show customer
        public static class TokenExpired
        {
            public static int Status = 5002;
            public static string error = "Token Expired";
        }

        public static class InvalidRole
        {
            public static int Status = 5003;
            public static string error = "Token Role";
        }

        public static class InvalidMSISDN
        {
            public static int Status = 5004;
            public static string error = "Invalid MSISDN";
        }

        public static class InvalidCustomer
        {
            public static int Status = 5005;
            public static string error = "InvalidCustomer";
        }

        public static class DbConnectionError
        {
            public static int Status = 5006;
            public static string error = "DB Error";
        }

        public static class TooMuchQuickly
        {
            public static int Status = 5007;
            public static string error = "Too Much Quickly";
        }

        public static class Quota
        {
            public static int Status = 5008;
            public static string error = "Quota Limit";
        }


        // can not show customer
        public static class BonusErrorCode
        {
            public static int Status = 6001;
            public static string error = "Data Insert Error";
        }

        public static class BonusDateErrorCode
        {
            public static int Status = 6002;
            public static string error = "Date Format Error/Date is before 15 days";
        }

        //public static class MsisdnFormatErrorCode { public static int Status = 6003; public static string error = "MSISDN Format Error/Inactive msisdn/Not a valid BL msisdn"; }

        public static class InputFormatErrorCode
        {
            public static int Status = 6004;
            public static string error = "Check all input parameter/Input valid parameter according to documentation";
        }

        public static class DataNotFound
        {
            public static int Status = 6005;
            public static string error = "No Data Found";
        }

        public static class InternaError
        {
            public static int Status = 6006;
            public static string error = "InternaError";
        }

        public static class DBSSError
        {
            public static int Status = 6007;
            public static string error = "Error From DBSS";
        }

        public static class InvalidArguments
        {
            public static int Status = 6008;
            public static string error = "MSISDN or SubcriptionId missing";
        }

        public static class AlreadyExist
        {
            public static int Status = 6009;
            public static string error = "Already Exist";
        }
        public static class AlreadyExistAndModified
        {
            public static int Status = 6010;
            public static string error = "Already Exist And Modified";
        }
        public static class GoogleTokenProblem
        {
            public static int Status = 6011;
            public static string error = "Google token fetching problem";
        }
        public static class CMSResponseError
        {
            public static int Status = 6012;
            public static string error = "CMS response error";
        }


    }

    public class MessageCodesList
    {
        public static List<KeyValuePair<string, string>> GetProperties(object data)
        {
            List<KeyValuePair<string, string>> _dict = new List<KeyValuePair<string, string>>();

            Type type = data.GetType();           

            PropertyInfo[] props = type.GetProperties();
            //Type[] types = type.GetNestedTypes(BindingFlags.Public | BindingFlags.Instance);
            //Type[] types1 = type.GetNestedTypes(BindingFlags.NonPublic | BindingFlags.Instance);

            Type[] types = type.GetNestedTypes();

            //BEMessageCodes messageCodes = new BEMessageCodes();
            //myDict[pi.Name] = pi.GetValue(data, null)?.ToString();

            foreach (PropertyInfo prop in props)
            {
                string propName = prop.Name;
                string propValue = prop.GetValue(data, null)?.ToString();

                _dict.Add(new KeyValuePair<string, string>(propName, propValue));             
            }

            foreach (Type item in types)
            {
                string status = string.Empty;
                string error = string.Empty;

                FieldInfo[] props1 = item.GetFields();

                foreach (FieldInfo prop in props1)
                {
                    string propName = prop.Name;
                    if(prop.Name=="Status")
                        status = prop.GetValue(prop)?.ToString();
                    else if (prop.Name == "error")
                        error = prop.GetValue(prop)?.ToString();
                }
                _dict.Add(new KeyValuePair<string, string>(status, error));
            }

            return _dict;
        }
    }
}