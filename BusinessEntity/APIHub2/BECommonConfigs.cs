﻿namespace BusinessEntity.APIHub2
{
    public class BECommonConfig : BEBase
    {
        public int CommonConfigId { set; get; }
        public string CommonConfigName { set; get; }
        public string GroupName { set; get; }

        ~BECommonConfig()
        {
        }
    }

    /// <exclude />
    public class BECommonConfigs : BEBaseList<BECommonConfig>
    {
        ~BECommonConfigs()
        {
        }
    }
}