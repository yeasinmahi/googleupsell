﻿using System;

namespace BusinessEntity.APIHub2
{
    #region Object of BERequestSummary

    /// <summary>
    ///     Object of BERequestSummary
    /// </summary>
    public class BERequestSummary
    {
        #region Constructor

        public BERequestSummary(string requestName)
        {
            RecordDate = DateTime.Now;
            RequestName = requestName;
            MinimumTime = 9999999;
        }

        #endregion

        #region Destructor

        ~BERequestSummary()
        {
        }

        #endregion

        #region Property

        public string RequestName { get; set; }

        public long TotalRequest { get; set; }
        public double TotalRtt { get; set; }
        public long ErrorCount { get; set; } = 0;

        public double MinimumTime { get; set; }
        public double MaxTime { get; set; }
        public double AverageTime { get; set; }
        
        public DateTime RecordDate { get; set; }

        //public DateTime recordDate { get; set; }
        //public DateTime RecordDate
        //{
        //    get { return recordDate; }
        //    set { recordDate = value; }
        //}

        #endregion
    }

    #endregion

    #region Collectin Object of BERequestSummary

    /// <exclude />
    public class BERequestSummarys // : BEBaseList<BERequestSummary>
    {
        #region Destructor

        ~BERequestSummarys()
        {
        }

        #endregion

        #region Constructor

        #endregion


        #region Functions

        #endregion
    }

    #endregion
}