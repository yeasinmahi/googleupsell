//using Newtonsoft.Json;

using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace BusinessEntity.APIHub2
{
    #region Object of BECustomer

    //[Serializable]
    //public enum CustomerTypeEnum
    //{
    //    BOSCustomer = 1,
    //    Icon = 2,
    //    ProffessionalCustomer = 3,
    //    BLMela = 4
    //}


    /// <summary>
    ///     Object of BECustomerView
    /// </summary>
    [DataContract]
    [Serializable]
    [XmlRoot("BECustomer")]
    public class BECustomerView : BEBase
    {
        public BECustomerView()
        {
            //CurrentCustomerName = string.Empty;
            IsCorporate = false;
            //prepaidPostPaidType = string.Empty;
            //IsOTPPass = false;
        }

        public int CustomerId { set; get; }
        public string ScurityAnswersList { set; get; }

        public string UserName { set; get; }

        //public string CompanyName { set; get; }
        public bool IsEmailVerified { set; get; }
        public bool IsBlocked { set; get; }
        public string EmailAddress { set; get; }
        public string AlternativeContactNumber { set; get; }
        public string AlternativeContactNumberHome { set; get; }
        public string AlternativeContactNumberWork { set; get; }
        [XmlIgnoreAttribute] public string Address { get; set; }
        public DateTime BirthDate { set; get; }

        public string BirthDateText
        {
            set
            {
                try
                {
                    BirthDate = value == "" ? DateTime.MinValue : Convert.ToDateTime(value);
                }
                catch (Exception)
                {
                }
            }
            get => BirthDate == DateTime.MinValue ? "" : BirthDate.ToString("dd-MM-yyyy");
        }

        [XmlIgnoreAttribute] public int BillCycle { get; set; }

        public int Gender { set; get; }
        public int MaritalStatus { set; get; }
        public int Salutation { set; get; }
        [XmlIgnoreAttribute] public string MoreAboutYou { set; get; }
        [XmlIgnoreAttribute] public DateTime SpouseBirthDay { set; get; }
        [XmlIgnoreAttribute] public bool IsCorporate { get; set; }

        [XmlIgnoreAttribute]
        public string SpouseBirthDayText
        {
            set
            {
                try
                {
                    SpouseBirthDay = value == "" ? DateTime.MinValue : Convert.ToDateTime(value);
                }
                catch (Exception)
                {
                }
            }
            get => SpouseBirthDay == DateTime.MinValue ? "" : SpouseBirthDay.ToString("dd-MM-yyyy");
        }

        [XmlIgnoreAttribute] public string SpouseContactNumber { set; get; }
        [XmlIgnoreAttribute] public string SpouseEmail { set; get; }
        [XmlIgnoreAttribute] public DateTime MarriageAnniversary { set; get; }

        [XmlIgnoreAttribute]
        public string MarriageAnniversaryText
        {
            set
            {
                try
                {
                    MarriageAnniversary = value == "" ? DateTime.MinValue : Convert.ToDateTime(value);
                }
                catch (Exception)
                {
                }
            }
            get =>
                MarriageAnniversary == DateTime.MinValue
                    ? ""
                    : MarriageAnniversary.ToString("dd-MM-yyyy");
        }

        //private BEBaseList<BEPackage> _packages = null;
        //public BEBaseList<BEPackage> Packages
        //{
        //    set
        //    {
        //        this._packages = value;
        //    }
        //    get
        //    {
        //        return this._packages != null ? this._packages : new BEBaseList<BEPackage>();
        //    }
        //}

        //private BEBaseList<BEActiveService> _activePackages = null;
        //public BEBaseList<BEActiveService> ActivePackages
        //{
        //    set
        //    {
        //        this._activePackages = value;
        //    }
        //    get
        //    {
        //        return this._activePackages != null ? this._activePackages : new BEBaseList<BEActiveService>();
        //    }
        //}


        //public string EquipIDnServiceID { get; set; }
        //public string CallnControlFlag { get; set; }


        //private BEBaseList<BEImageGallery> imageGalary = null;
        //public BEBaseList<BEImageGallery> ImageGalary
        //{
        //    get { return imageGalary; }
        //    set { imageGalary = value; }
        //}

        //public bool CanViewItemizedBill { get; set; }

        //[NonSerialized]
        //private BEInterests ints;

        //public BEInterests Interests
        //{
        //    set
        //    {
        //        this.ints = value;
        //    }
        //    get
        //    {
        //        return this.ints != null ? this.ints : new BEInterests();
        //    }
        //}
        [XmlIgnoreAttribute] public int Religion { set; get; }
        [XmlIgnoreAttribute] public bool FreeEmailAlertService { set; get; }
        [XmlIgnoreAttribute] public bool FreeENewsLetter { set; get; }
        [XmlIgnoreAttribute] public bool IsMustChangePassword { set; get; }

        //public string InterestList
        //{
        //    get
        //    {
        //        StringBuilder strBld = new StringBuilder();
        //        foreach (BEInterest interest in this.Interests)
        //        {
        //            strBld.Append(interest.InterestName);
        //            strBld.Append(", ");
        //        }
        //        if (strBld.Length > 1) strBld.Remove(strBld.Length - 2, 2);

        //        return strBld.ToString();
        //    }
        //}

        public string GenderText { set; get; }
        public string MaritalStatusText { set; get; }
        [XmlIgnoreAttribute] public string SalutationText { set; get; }
        [XmlIgnoreAttribute] public string ReligionText { set; get; }

        public string MSISDN { set; get; }

        public string MSISDNWith0 => "0" + MSISDN;

        public string MSISDNWith880 => "880" + MSISDN;

        //public string CustomerPassword { set; get; }
        public string ContractNumber { set; get; }
        [XmlIgnoreAttribute] public bool CustomerRegisteredOnWeb { set; get; }

        [XmlIgnoreAttribute] public string VirtualContactNo { set; get; }

        //public string CurrentMSISDN { set; get; }
        //public string CorporateDailMSISDN { set; get; }
        //public Int32 CurrentCustomerId { set; get; }
        //public string CurrentCustomerPrepaidPostPaidType { set; get; }
        //public string CorporateDailCustomerPrepaidPostPaidType { set; get; }
        //public string CurrentCustomerName { set; get; }
        //public bool IsHVC { set; get; }
        [XmlIgnoreAttribute] public string ConnectionType { set; get; }
        //private string prepaidPostPaidType = string.Empty;
        //public string LastEmailText { set; get; }
        //public string PrepaidPostPaidType
        //{
        //    get;
        //    set;
        //    //get
        //    //{
        //    //    if (prepaidPostPaidType.Length > 0) return prepaidPostPaidType;

        //    //    if (this.ConnectionType.ToUpper() == "PREP")
        //    //    {
        //    //        prepaidPostPaidType = "PREP";
        //    //    }
        //    //    else
        //    //    {
        //    //        var pkg = this.ActivePackages.Where(p => p.ServiceId.Contains("CC") == true);
        //    //        if (pkg.Count<BEActiveService>() > 0)
        //    //        {
        //    //            prepaidPostPaidType = "PREP";
        //    //        }
        //    //        else
        //    //        {
        //    //            prepaidPostPaidType = "POST";
        //    //        }
        //    //    }

        //    //    return prepaidPostPaidType;
        //    //}
        //}

        //public bool IsEmailVerified { set; get; }
        //public string VerificationCode { set; get; }
        //public string Profession { set; get; }
        //public bool HasOtherInterest { set; get; }
        //public string OtherInterest { set; get; }
        //public int CustomerType { get; set; }
        [XmlIgnoreAttribute] public DateTime LastLogin { get; set; }

        //added for coupon referral
        public string MyReferralCode { set; get; }
        public string ReferrerCode { set; get; }

        ~BECustomerView()
        {
        }

        //[XmlIgnoreAttribute]
        public DateTime BillCycleDateMonthAndYearWise(int year, int month)
        {
            var billCycleDate = BillCycleDateMonthAndYearWise(year, month, 0);

            return billCycleDate;
        }

        //[XmlIgnoreAttribute]
        public DateTime BillCycleDateMonthAndYearWise(int year, int month, int addMonths)
        {
            var billCycleDate = DateTime.MinValue;
            if (BillCycle == 21)
            {
                billCycleDate = new DateTime(year, month, 21);
                billCycleDate = billCycleDate.AddMonths(addMonths);
            }
            else
            {
                var tempDate = new DateTime(year, month, 1).AddMonths(addMonths);
                var lastDayofMonth = DateTime.DaysInMonth(tempDate.Year, tempDate.Month);
                billCycleDate = new DateTime(tempDate.Year, tempDate.Month, lastDayofMonth);
            }

            return billCycleDate;
        }

        //public int HomeContentIndex { get; set; }
        //public TimeSpan HomeContentTime { get; set; }
        //public bool IsBlocked { get; set; }

        //public string Department { get; set; }
        //public string LockedReason { get; set; }
        //public string SecretAnswer1 { get; set; }
        //public string SecretAnswer2 { get; set; }
        //public string SecretAnswer3 { get; set; }
        //public string SecretAnswer4 { get; set; }
        //public string LastSecurityKey { get; set; }

        //private bool _isBackOfficeUser = false;
        //public bool IsBackOfficeUser
        //{
        //    get
        //    {
        //        return _isBackOfficeUser;
        //    }
        //    set
        //    {
        //        _isBackOfficeUser = value;
        //    }
        //}

        //public bool IsOTPPass { get; set; }

        //public int AdminUserID { get; set; }
    }

    /// <exclude />
    [DataContract]
    [Serializable]
    [XmlRoot("BECustomer")]
    public class BECustomer : BEBase
    {
        private bool _isBackOfficeUser;

        [NonSerialized] private BEInterests ints;

        public BECustomer()
        {
            CurrentCustomerName = string.Empty;
            IsCorporate = false;
            //prepaidPostPaidType = string.Empty;
            IsOTPPass = false;
            EmailAddress = "";
        }

        public int CustomerId { set; get; }
        public string UserName { set; get; }
        public string CompanyName { set; get; }
        public string EmailAddress { set; get; }
        public string AlternativeContactNumber { set; get; }
        public string AlternativeContactNumberHome { set; get; }
        public string AlternativeContactNumberWork { set; get; }
        public string Address { get; set; }

        public string SecretQuestionList
        {
            get
            {
                var secretQuestions = string.Empty;

                if (!string.IsNullOrEmpty(SecretAnswer1))
                    secretQuestions += "1||";

                if (!string.IsNullOrEmpty(SecretAnswer2))
                    secretQuestions += "2||";

                if (!string.IsNullOrEmpty(SecretAnswer3))
                    secretQuestions += "3||";

                if (!string.IsNullOrEmpty(SecretAnswer4))
                    secretQuestions += "4||";

                if (secretQuestions.Length > 0)
                    secretQuestions = secretQuestions.Substring(0, secretQuestions.Length - 2);
                return secretQuestions;
            }
        }

        public DateTime BirthDate { set; get; }

        public string BirthDateText
        {
            set
            {
                try
                {
                    BirthDate = value == "" ? DateTime.MinValue : Convert.ToDateTime(value);
                }
                catch (Exception)
                {
                }
            }
            get =>
                BirthDate == DateTime.MinValue
                    ? ""
                    : BirthDate.ToString("yyyy-MM-dd"); //this.BirthDate.ToString("dd-MM-yyyy");
        }

        public int BillCycle { get; set; }

        public int Gender { set; get; }
        public int MaritalStatus { set; get; }
        public int Salutation { set; get; }
        public string MoreAboutYou { set; get; }
        public DateTime SpouseBirthDay { set; get; }
        public bool IsCorporate { get; set; }

        public string SpouseBirthDayText
        {
            set
            {
                try
                {
                    SpouseBirthDay = value == "" ? DateTime.MinValue : Convert.ToDateTime(value);
                }
                catch (Exception)
                {
                }
            }
            get => SpouseBirthDay == DateTime.MinValue ? "" : SpouseBirthDay.ToString("dd-MM-yyyy");
        }

        public string SpouseContactNumber { set; get; }
        public string SpouseEmail { set; get; }
        public DateTime MarriageAnniversary { set; get; }

        public string MarriageAnniversaryText
        {
            set
            {
                try
                {
                    MarriageAnniversary = value == "" ? DateTime.MinValue : Convert.ToDateTime(value);
                }
                catch (Exception)
                {
                }
            }
            get =>
                MarriageAnniversary == DateTime.MinValue
                    ? ""
                    : MarriageAnniversary.ToString("dd-MM-yyyy");
        }


        //private BEBaseList<BEActiveService> _activePackages = null;
        //public BEBaseList<BEActiveService> ActivePackages
        //{
        //    set
        //    {
        //        this._activePackages = value;
        //    }
        //    get
        //    {
        //        return this._activePackages != null ? this._activePackages : new BEBaseList<BEActiveService>();
        //    }
        //}


        public string EquipIDnServiceID { get; set; }
        public string CallnControlFlag { get; set; }


        public bool CanViewItemizedBill { get; set; }

        public BEInterests Interests
        {
            set => ints = value;
            get => ints != null ? ints : new BEInterests();
        }

        public int Religion { set; get; }
        public bool FreeEmailAlertService { set; get; }
        public bool FreeENewsLetter { set; get; }

        public bool IsMustChangePassword { set; get; }

        public string InterestList
        {
            get
            {
                var strBld = new StringBuilder();
                foreach (var interest in Interests)
                {
                    strBld.Append(interest.InterestName);
                    strBld.Append(", ");
                }

                if (strBld.Length > 1) strBld.Remove(strBld.Length - 2, 2);

                return strBld.ToString();
            }
        }

        public string GenderText { set; get; }
        public string MaritalStatusText { set; get; }
        public string SalutationText { set; get; }
        public string ReligionText { set; get; }
        public string MSISDN { set; get; }

        public string MSISDNWith0 => "0" + MSISDN;

        public string MSISDNWith880 => "880" + MSISDN;

        public string CustomerPassword { set; get; }
        public string ContractNumber { set; get; }
        public bool CustomerRegisteredOnWeb { set; get; }
        public string VirtualContactNo { set; get; }
        public string CurrentMSISDN { set; get; }
        public string CorporateDailMSISDN { set; get; }
        public int CurrentCustomerId { set; get; }
        public string CurrentCustomerPrepaidPostPaidType { set; get; }
        public string CorporateDailCustomerPrepaidPostPaidType { set; get; }
        public string CurrentCustomerName { set; get; }
        public bool IsHVC { set; get; }
        public string ConnectionType { set; get; }

        public DateTime ConnectionDate { set; get; }

        //private string prepaidPostPaidType = string.Empty;
        public string LastEmailText { set; get; }

        public string PrepaidPostPaidType
        {
            get;
            set;
            //get
            //{
            //    if (prepaidPostPaidType.Length > 0) return prepaidPostPaidType;

            //    if (this.ConnectionType.ToUpper() == "PREP")
            //    {
            //        prepaidPostPaidType = "PREP";
            //    }
            //    else
            //    {
            //        var pkg = this.ActivePackages.Where(p => p.ServiceId.Contains("CC") == true);
            //        if (pkg.Count<BEActiveService>() > 0)
            //        {
            //            prepaidPostPaidType = "PREP";
            //        }
            //        else
            //        {
            //            prepaidPostPaidType = "POST";
            //        }
            //    }

            //    return prepaidPostPaidType;
            //}
        }

        public bool IsEmailVerified { set; get; }
        public string VerificationCode { set; get; }
        public string Profession { set; get; }
        public bool HasOtherInterest { set; get; }
        public string OtherInterest { set; get; }
        public int CustomerType { get; set; }
        public DateTime LastLogin { get; set; }
        public int HomeContentIndex { get; set; }
        public TimeSpan HomeContentTime { get; set; }
        public bool IsBlocked { get; set; }

        public string Department { get; set; }
        public string LockedReason { get; set; }
        public string SecretAnswer1 { get; set; }
        public string SecretAnswer2 { get; set; }
        public string SecretAnswer3 { get; set; }
        public string SecretAnswer4 { get; set; }
        public string LastSecurityKey { get; set; }

        public bool IsBackOfficeUser
        {
            get => _isBackOfficeUser;
            set => _isBackOfficeUser = value;
        }

        public bool IsOTPPass { get; set; }

        public int AdminUserID { get; set; }
        public int WrongAttempt { get; set; }
        public string FacebookID { set; get; }
        public string GooglePlusID { set; get; }

        public string PrepaidPostPaidTypeConsideringCallnControll
        {
            get
            {
                var prepaidPostPaidType = PrepaidPostPaidType;

                if (prepaidPostPaidType == "POST" && CallnControlFlag == "Call & Control") prepaidPostPaidType = "PREP";

                return prepaidPostPaidType;
            }
        }

        //added for coupon reffereal

        public string MyReferralCode { set; get; }
        public string ReferrerCode { set; get; }
        public string SubscriptionID { get; set; }
        public string ContractID { get; set; }
        public string PaymentType { get; set; }
        public string CustomerStatus { get; set; }
        public string CustomerID { get; set; }

        ~BECustomer()
        {
        }

        public DateTime BillCycleDateMonthAndYearWise(int year, int month)
        {
            var billCycleDate = BillCycleDateMonthAndYearWise(year, month, 0);

            return billCycleDate;
        }

        public DateTime BillCycleDateMonthAndYearWise(int year, int month, int addMonths)
        {
            var billCycleDate = DateTime.MinValue;
            if (BillCycle == 21)
            {
                billCycleDate = new DateTime(year, month, 21);
                billCycleDate = billCycleDate.AddMonths(addMonths);
            }
            else
            {
                var tempDate = new DateTime(year, month, 1).AddMonths(addMonths);
                var lastDayofMonth = DateTime.DaysInMonth(tempDate.Year, tempDate.Month);
                billCycleDate = new DateTime(tempDate.Year, tempDate.Month, lastDayofMonth);
            }

            return billCycleDate;
        }
    }

    /// <exclude />
    public class BECustomers : BEBaseList<BECustomer>
    {
        ~BECustomers()
        {
        }
    }

    #region Object of BEExternalLogin

    public class BEExternalLogin : BEBase
    {
        #region Destructor

        ~BEExternalLogin()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Property

        //[Required]
        [Display(Name = "ExternalLogin ID")] public string ExternalLoginID { get; set; }

        //[Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string EmailAddress { get; set; }


        [Display(Name = "LoginProvider")] public string LoginProvider { get; set; }

        [Display(Name = "Provider Key")] public string ProviderKey { get; set; }

        #endregion
    }

    #endregion

    #endregion

    //public class BECustomerSearchCriteria : BEBaseSearch
    //{
    //    [Display(Name = "Customer ID")]
    //    public string CustomerID { get; set; }

    //    public string EmailAddress { get; set; }

    //    public string MSISDN { get; set; }

    //    public BECustomers Customers
    //    {
    //        get; set;
    //    }
    //}
}