﻿using System;

namespace BusinessEntity.APIHub2
{
    public class AppSettings
    {
        public static bool IsTest = false;
        public static bool ClientValidationEnabled { get; set; }
        public static bool UnobtrusiveJavaScriptEnabled { get; set; }
        public static bool IsLoadSettings { get; set; } = false;

        public static string MoneyUnitText { get; set; }

        //public static int OTPLength { get; set; }
        public static int TrackingNumberLength { get; set; }
        public static string RandomNumberGeneratingString { get; set; } = "123456789";
        public static int NumberOfShipperForSendingParcel { get; set; }
        public static string ErrorLogFilePath { get; set; }
        public static int TemporaryPasswordValidity { get; set; }
        public static int TravelDaysGap { get; set; }
        public static int RecordsPerPage { get; set; }

        public static bool IsTestSite { get; set; }
        public static string FacebookAppID { get; set; }
        public static string FacebookAppSecret { get; set; }

        public static string SiteURL { get; set; }
        public static string Provider { get; set; } = "Not Defined";
        public static string ServiceName { get; set; } = "Not Defined";


        public static int ImageResizeTo { get; set; } = 800;

        public static int HttpRequestTimeOut { get; set; } = 3000;
        public static string ServiceUserID { get; set; }
        public static string CpidSecrtKey { get; } = "b14ca5898a4e4133";
        public static string GoogleTokenSecretKey { get; } = "notasecret";


        public static string LOGMSISDNS { get; set; }
        public static string DBBSErrorLogPath { get; set; }

        public static int MinimumRTTToLog { get; set; } = 3000;

        public static int SmallReponse { get; set; } = 1;
        public static int MediumResponse { get; set; } = 3;
        public static int LargeResponse { get; set; } = 1;
        public static string OTPSMS { get; set; }
        public static string SMSsender { get; set; }
        public static int OTPLength { get; set; }
        public static int OTPIntervalTime { get; set; }
        public static int MaximumOTPInADay { get; set; }
        public static int OTPValidityDuration { get; set; }
        public static int OTPWrongAttempLimit { get; set; }

        public static string temporaryLockWrongAttemptLimit { get; set; }
        public static string temporaryLockRemovalTimeMin { get; set; }


        public static int TotalRequestSummary { get; set; } = 100;
        public static int TtlSeconds { get; set; } = 86400;

        public static int NoOfRecordsPerBatch { get; set; } = 10;
        public static int SleepAtIdle { get; set; } = 3000;
        public static int MaxSleepAtIdle { get; set; } = 600000;
        public static int GoogleHttpRequestTimeOut { get; set; } = 10000;

        public static bool IsCompleteAllTaskPerBatchGoogleNotification { get; set; } = true;
        public static bool IsEarlyKafkaAutoOffsetReset { get; set; } = true;

        public static bool IsStartT24 { get; set; } = true;
        public static bool IsStartT02 { get; set; } = true;
        public static bool IsStartT09 { get; set; } = true;
        public static bool IsStartKafkaReader { get; set; } = true;
        public static string GoogleNotificationTableName { get; set; } = "Google_notification";


        /// <summary>
        /// ////////////////////DBSS Configuration Start////////////////////////////////////////
        /// </summary>
        /// 
        public static string DBBSApiBaseURL { get; set; }

        public static string CMSApiBaseURL { get; set; }
        public static bool EnableDemoAccess { get; set; }
        public static string RequestLogPath { get; set; }
        public static int DBSSHttpRequestTimeOut { get; set; }
        public static int CMSHttpRequestTimeOut { get; set; }
        public static bool EnableWriteDBBSErrorLog { get; set; }
        public static string DemoPrePostType { get; set; }

        public static string LMSApiBaseURL { get; set; }
        public static string DNBOLMSApiBaseURL { get; set; }

        public static bool IsLoadConfig { get; set; }
        public static string LanguageCode { get; set; }


        /// <summary>
        /// /////////////////////DBSS Configuration End/////////////////////////
        /// </summary>


        /// <summary>
        /// ////////////////////DBSS Configuration Start////////////////////////////////////////
        /// </summary>
        public static string OpenCodeBaseURL { get; set; }

        public static string OpenCodeUserName { get; set; }
        public static string OpenCodePassword { get; set; }

        /// <summary>
        /// /////////////////////DBSS Configuration End/////////////////////////
        /// </summary>

        public static long ApplicationLoadingTime { get; set; }

        public static DateTime ApplicationStartTime { get; set; } = DateTime.Now;

        /// <summary>
        /// ////////////////////AUTH Configuration Start////////////////////////////////////////
        /// </summary>
        public static string Audience { get; set; }

        public static string Scope { get; set; }
        public static string Issuer { get; set; }
        public static string Sub { get; set; }
        public static string SecretKey { get; set; }
        public static string Alg { get; set; }
        public static string AuthenticationScheme { get; set; }
        public static string TokenType { get; set; }

        /// <summary>
        /// /////////////////////AUTH Configuration End/////////////////////////
        /// </summary>


        /// <summary>
        /// ////////////////////Kafka Configuration Start////////////////////////////////////////
        /// </summary>
        public static string Topic { get; set; }

        public static string GroupId { get; set; }
        public static string BootstrapServers { get; set; }


        public static int NoOfKafkaThread { get; set; }

        public static int NoOfGooglePushNotificationThread { get; set; }


        /// <summary>
        /// /////////////////////AUTH Configuration End/////////////////////////
        /// </summary>
        /// 
        public static string GooglePushNotificationURL { get; set; }
        

        public static class WindowsService
        {
            public static int NoOfRecords { get; set; }

            //httpRequestTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["HttpRequestTimeOut"]);
            public static int SleepWhenIdle { get; set; }
            public static int PerBatchDelay { get; set; }
            public static bool IsWriteLog { get; set; }
            public static bool IsWriteServiceStartEndLog { get; set; }
        }
    }

}