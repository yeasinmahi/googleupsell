﻿//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Collections;

namespace BusinessEntity.APIHub2
{
    public enum SMSChannelEnum
    {
        Default = 0,
        APIHub = 1
    }

    /// <summary>
    ///     Object of BOTP
    /// </summary>
    public class BEOTP
    {
        //public Int32 AreaId { set; get; }
        public string Sender { set; get; }
        public string MSISDN { set; get; }
        public string otpCode { set; get; }
        public string CustomerOTP { set; get; }
        public bool isSendToMobile { set; get; }
        public string PageSourceTitle { set; get; }
        public string DestinationPageAction { set; get; }
        public string DestinationPageController { set; get; }
        public string otpMsg { set; get; }
        public int PageType { set; get; }

        ~BEOTP()
        {
        }

        //       public string DistrictName { set; get; }
    }


    public class OTPResponse : OtpError
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }

    public class OtpError
    {
        public string ErrorCode { get; set; }
        public string Error { get; set; }
    }
}