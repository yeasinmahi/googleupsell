﻿using System;
using System.Collections.Generic;

namespace BusinessEntity.APIHub2
{
    public class BEDataServicePack
    {
        #region Destructor

        ~BEDataServicePack()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Property

        public int DataServicePackID { get; set; }


        public string DataServicePackName { get; set; }

        public string Description { get; set; }

        public string RelatedURL { get; set; }
        public string VASServieActivationCode { get; set; }

        public string PackName { get; set; }
        public string Price { get; set; }
        public string Volume { get; set; }
        public string Validity { get; set; }
        public int DataPackTypeID { get; set; }

        public string PriceText { get; set; }
        public bool IsAutoRenewal { get; set; }
    }

    #endregion

    #region Collection Object of BEDataServicePack

    public class BEDataServicePacks : List<BEDataServicePack>
    {
        #region Constructor

        public BEDataServicePacks()
        {
            RefreshStartTime = DateTime.Now;
        }

        #endregion

        public DateTime RefreshStartTime { get; set; }
        public DateTime RefreshEndTime { get; set; }

        public double ExecutionTime
        {
            get
            {
                var span = RefreshEndTime - RefreshStartTime;
                return span.TotalMilliseconds;
            }
        }

        #region Destructor

        ~BEDataServicePacks()
        {
        }

        #endregion
    }

    #endregion
}