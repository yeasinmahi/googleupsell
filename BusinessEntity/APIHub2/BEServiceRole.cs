﻿namespace BusinessEntity.APIHub2
{
    #region Object of BEServiceRole

    public class BEServiceRole : BEBase
    {
        #region Destructor

        ~BEServiceRole()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Property

        public int RoleID { get; set; }

        public string RoleName { get; set; }

        #endregion
    }

    #endregion

    #region Collectin Object of BEServiceUserInfo

    /// <exclude />
    public class BEServiceRoles : BEBaseList<BEServiceRole>
    {
        #region Destructor

        ~BEServiceRoles()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Functions

        #endregion
    }

    #endregion
}