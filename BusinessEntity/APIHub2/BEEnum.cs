﻿namespace BusinessEntity.APIHub2
{
    public enum CustomerTypeEnum
    {
        Prepaid = 1,
        Postpaid = 2
    }
}