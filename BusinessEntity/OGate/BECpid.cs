﻿namespace BusinessEntity.OGate
{
    public class CpidError
    {
        //  [ApiExplorerSettings(IgnoreApi = true)]
        public CpidError(string errorMessage, string cause)
        {
            ErrorMessage = errorMessage;
            Cause = cause;
        }

        public string ErrorMessage { get; set; }
        public string Cause { get; set; }
    }

    public class CpidResponse
    {
        public string Cpid { get; set; }

        public int TtlSeconds { get; set; }
        //public string Provider { get; set; }
        //public DateTime CreateDate { get; set; } = DateTime.UtcNow;
    }

    public class CpidDecryptResponse
    {
        //public bool IsSuccess { get; set; }
        public int Status { get; set; }
        public string ErrorMessage { get; set; }
        public string Msisdn { get; set; }
        public string CreateDate { get; set; }
        public string CreateTime { get; set; }
        public string Language { get; set; }
        public string Provider { get; set; }
    }
}