using System;
using System.ComponentModel.DataAnnotations;
using BusinessEntity.APIHub2;

namespace BusinessEntity.OGate
{
    #region Object of BECpidHistory

    

    public class BECpid : BEBase
    {
        #region Destructor

        ~BECpid()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Property

        [Display(Name = "id")] public int Id { get; set; }
        [Display(Name = "id")] public bool isRegisterd { get; set; } = false;

        [Display(Name = "msisdn")] public string Msisdn { get; set; }

        [Display(Name = "createDate")] public DateTime CreateDate { get; set; }

        [Display(Name = "updateDate")] public DateTime UpdateDate { get; set; }
        [Display(Name = "cpidExpireDate")] public DateTime CpidExpireDate { get; set; }

        [Display(Name = "cpid")] public string Cpid { get; set; }

        [Display(Name = "provider")] public string Provider { get; set; }
        [Display(Name = "appId")] public string AppId { get; set; }
        [Display(Name = "ttl")] public int Ttl { get; set; }
        [Display(Name = "registerCpid")] public string RegisterCpid { get; set; }
        [Display(Name = "staleTime")] public DateTime StaleTime { get; set; }
        [Display(Name = "updateDateRegister")] public DateTime UpdateDateRegister { get; set; }
        #endregion
    }

    #endregion

    
    #region Collection Object of BECpidHistory

    public class BECpids : BEBaseList<BECpid>
    {
    }

    #endregion
}