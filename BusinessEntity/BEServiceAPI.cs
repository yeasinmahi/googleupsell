using System.ComponentModel.DataAnnotations;
using BusinessEntity.APIHub2;

namespace BusinessEntity
{
    #region Object of BEServiceAPI

    public class BEServiceAPI : BEBase
    {
        #region Destructor

        ~BEServiceAPI()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Property

        [Display(Name = "ID")] public int ID { get; set; }

        [Display(Name = "Api Name")] public string ApiName { get; set; }

        [Display(Name = "Service ID")] public int ServiceID { get; set; }

        #endregion
    }

    #endregion

    #region Collectin Object of BEServiceAPI

    public class BEServiceAPIs : BEBaseList<BEServiceAPI>
    {
        #region Destructor

        ~BEServiceAPIs()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Functions

        #endregion
    }

    #endregion
}