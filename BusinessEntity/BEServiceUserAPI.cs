using System.ComponentModel.DataAnnotations;
using BusinessEntity.APIHub2;

namespace BusinessEntity
{
    #region Object of BEServiceUserAPI

    public class BEServiceUserAPI : BEBase
    {
        #region Destructor

        ~BEServiceUserAPI()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Property

        [Display(Name = "Service User APIService APIID")]
        public int ServiceUserServiceAPIID { get; set; }

        [Display(Name = "User ID")] public int UserID { get; set; }

        [Display(Name = "Service APIID")] public int ServiceAPIID { get; set; }

        [Display(Name = "APIName")] public string APIName { get; set; }

        #endregion
    }

    #endregion

    #region Collectin Object of BEServiceUserAPI

    public class BEServiceUserAPIs : BEBaseList<BEServiceUserAPI>
    {
        #region Destructor

        ~BEServiceUserAPIs()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Functions

        #endregion
    }

    #endregion
}