﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BusinessEntity.ProductConfig
{

    #region Object of BEDataProduct
    public class BEDataProduct : BEDataProductDetails
    {
        #region Constructor
        public BEDataProduct() { }
        #endregion

        #region Destructor
        ~BEDataProduct() { }
        #endregion

        #region Property

      

        [Display(Name = "Data Product Id")]
        public int DataProductId { get; set; }

        [Display(Name = "Data Product Name")]
        public string DataProductName { get; set; }


        [Display(Name = "Data Product Id")]
        public int Data_ProductTypeId { get; set; }

       

        [Display(Name = "Initial Amount")]
        public int InitialAmount { get; set; }

        [Display(Name = "Amount Unit")]
        public string AmountUnit { get; set; }

        [Display(Name = "Product Type")]
        public string ProductType { get; set; }


        #endregion
    }
    #endregion

    public class BEDataProductDetails
    {
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Related Url")]
        public string RelatedUrl { get; set; }

        [Display(Name = "Data Code")]
        public string DataCode { get; set; }

        [Display(Name = "Auto Renew Code")]
        public string AutoRenewCode { get; set; }

        [Display(Name = "Short Code")]
        public string ShortCode { get; set; }

        public int IsActive { get; set; }
        public int IsDeleted { get; set; }
        public int CreatedBy { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public int UpdatedBy { get; set; }
        public DateTimeOffset UpdatedDate { get; set; }

        public int DeletedBy { get; set; }
        public DateTimeOffset DeletedDate { get; set; }

        [Display(Name = "Display Order")]
        public int DisplayOrder { get; set; }

        [Display(Name = "Price")]
        public string Price { get; set; }

        [Display(Name = "Volume")]
        public string Volume { get; set; }

        [Display(Name = "Validity")]
        public string Validity { get; set; }

        [Display(Name = "Product Family Id")]
        public string ProductFamilyId { get; set; }

        [Display(Name = "Activation Message")]
        public string ActivationMessage { get; set; }

        [Display(Name = "Deactivation Message")]
        public string DeactivationMessage { get; set; }
    }
        #region Collectin Object of BEDataProduct
        public class BEDataProducts : List<BEDataProduct>
    {
        public BEDataProducts()
        {
           
        }
        ~BEDataProducts() { }
       
    }
    #endregion
}
