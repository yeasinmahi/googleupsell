﻿using Newtonsoft.Json;

namespace BusinessEntity.ProductConfig
{
    public class BEProductConfig
    {
    }

    public class Product
    {
        public Product()
        {
        }

        public Product(int ID, string Name, string TotalVolume, string Unit, int IsActive)
        {
            this.ID = ID;
            this.Name = Name;
            this.TotalVolume = TotalVolume;
            this.Unit = Unit;
            this.IsActive = IsActive;
        }

        [JsonProperty("ID")] public int ID { get; set; }

        [JsonProperty("Name")] public string Name { get; set; }


        [JsonProperty("TotalVoulme")] public string TotalVolume { get; set; }

        [JsonProperty("Unit")] public string Unit { get; set; }

        [JsonProperty("IsActive")] public int IsActive { get; set; }
    }


    public class PlatForm
    {
        public PlatForm()
        {
        }

        public PlatForm(int ID, string Name, int IsActive)
        {
            this.ID = ID;
            this.Name = Name;

            this.IsActive = IsActive;
        }

        [JsonProperty("ID")] public int ID { get; set; }

        [JsonProperty("Name")] public string Name { get; set; }

        [JsonProperty("IsActive")] public int IsActive { get; set; }
    }

    public class ProductPlatForm
    {
        public ProductPlatForm()
        {
        }

        public ProductPlatForm(int ID, int ProductID, int PlatformID)
        {
            this.ID = ID;
            this.ProductID = ProductID;
            this.PlatformID = PlatformID;
        }

        [JsonProperty("ID")] public int ID { get; set; }

        [JsonProperty("ProductID")] public int ProductID { get; set; }

        [JsonProperty("PlatformID")] public int PlatformID { get; set; }
    }
}