﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.Case
{
    public class TicketResponse
    {
        public TicketResponse()
        {
            Tickets = new List<TicketViewModel>();
        }

        [JsonProperty("tickets")] public List<TicketViewModel> Tickets { get; set; }
    }

    public class TicketViewModel
    {
        [JsonProperty("ticketId")] public long TicketId { get; set; }

        [JsonProperty("categoryId")] public int CategoryId { get; set; }

        [JsonProperty("title")] public string Title { get; set; }

        [JsonProperty("categoryName")] public string CategoryName { get; set; }

        [JsonProperty("categoryFullName")] public string CategoryFullName { get; set; }

        [JsonProperty("createdAt")] public DateTime CreatedAt { get; set; }

        [JsonProperty("serviceDate")] public DateTimeOffset? ServiceDate { get; set; }

        [JsonProperty("lastModified")] public DateTimeOffset? LastModified { get; set; }

        [JsonProperty("channelName")] public string ChannelName { get; set; }

        [JsonProperty("currentStatus")] public string CurrentStatus { get; set; }

        [JsonProperty("messages")] public TicketMessage[] Messages { get; set; }
    }

    public class TicketMessage
    {
        [JsonProperty("messageId")] public long MessageId { get; set; }

        [JsonProperty("body")] public string Body { get; set; }
    }


    public class TicketDummyResponse
    {
        public static string Data =
            "{\n  \"tickets\": [\n    {\n      \"ticketId\": 10,\n      \"categoryId\": 292,\n      \"categoryName\": \"CALL DROP\",\n      \"categoryFullName\": \"Technical Complaint/RADIO/CALL DROP\",\n      \"createdAt\": \"2019-08-20 07:49:26\",\n      \"serviceDate\": \"\",\n      \"lastModified\": \"2019-08-20 08:03:31\",\n      \"channelName\": \"\",\n      \"currentStatus\": \"\",\n      \"messages\": []\n    }\n  ]\n}";
    }
}