﻿using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.CMS
{
    public class MyOfferListResponse : CMSErrorResponse
    {
        public string offerID { get; set; }

        [JsonProperty("offerDescriptionWeb")] public string offerName { get; set; }

        public string offerDescription { get; set; }
        public string imageURL { get; set; }
        public string offerRank { get; set; }
        public string offerType { get; set; }
        public double offerPrice { get; set; }
        public string offerScore { get; set; }
        public string offerLongDescription { get; set; }
        public string offerShortDescription { get; set; }
        public string offerCategoryName { get; set; }
    }
}