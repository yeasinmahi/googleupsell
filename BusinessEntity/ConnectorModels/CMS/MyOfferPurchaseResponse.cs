﻿namespace BusinessEntity.ConnectorModels.CMS
{
    public class MyOfferPurchaseResponse : CMSErrorResponse
    {
        public string ID { get; set; }
        public string Status { get; set; }
    }
}