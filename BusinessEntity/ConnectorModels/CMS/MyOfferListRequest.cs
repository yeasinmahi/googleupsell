﻿namespace BusinessEntity.ConnectorModels.CMS
{
    public class MyOfferListRequest
    {
        public long msisdn { get; set; }
        //public string language { get; set; } = "EN";

        public int channelID { get; set; }

        //public int userID { get; set; }
        public int serviceTypeID { get; set; }

        public int salesChannelID { get; set; }

        //public long retailerMsisdn { get; set; }
        // public double rechargeAmount { get; set; }
        public double balance { get; set; }
    }
}