﻿namespace BusinessEntity.ConnectorModels.CMS
{
    public class MyOfferDetailRequest
    {
        public int msisdn { get; set; }
        public string language { get; set; }
        public int channelID { get; set; }
        public int serviceTypeID { get; set; }
        public string offerID { get; set; }
        public int salesChannelID { get; set; }
    }
}