﻿namespace BusinessEntity.ConnectorModels.CMS
{
    public class MyOfferPurchaseRequest
    {
        public long msisdn { get; set; }
        public string language { get; set; }
        public string offerID { get; set; }
        public int channelID { get; set; }

        public int salesChannelID { get; set; }
        public int actionCall { get; set; }
    }
}