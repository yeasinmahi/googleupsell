﻿using System;

namespace BusinessEntity.ConnectorModels.CMS
{
    public class CMSErrorResponse
    {
        public int PlatformID { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorType { get; set; }
        public string ErrorMessage { get; set; }
    }
    public class CmsRequestLog
    {
        public string Name { get; set; }
        public string Msisdn { get; set; }
        public double Rtt { get; set; }
        public DateTime RequestDate { get; set; }
        public DateTime ResponseDate { get; set; }
        public string Error { get; set; }
        public string Url { get; set; }
    }
}