﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    #region FNF Request & Response

    #region FNF Request

    public class FNFRquest
    {
        [JsonProperty("data")] public FNFRquestData Data { get; set; }

        public class FNFRquestData
        {
            [JsonProperty("type")] public string Type { get; set; }

            [JsonProperty("id")] public string Id { get; set; }

            [JsonProperty("meta")] public Meta Meta { get; set; }
        }

        public class Meta
        {
            [JsonProperty("services")]
            //public FNFServices Services { get; set; }
            public Dictionary<string, object> ServiceParameters { get; set; }

            [JsonProperty("channel")] public string Channel { get; set; }
        }

        public class FNFServices
        {
            //[JsonProperty("FAF3")]
            public Dictionary<string, string> jsonModel { get; set; }
            //public Dictionary<string, object> Faf3 { get; set; }
        }
    }

    #endregion

    #endregion

    #region Bos Camping Registration Request

    public class DeleteDataPackRquest
    {
        public DeleteDataPackRquest()
        {
            Data = new List<DataPackRquestData>();
        }

        [JsonProperty("data")] public List<DataPackRquestData> Data { get; set; }

        public class DataPackRquestData
        {
            [JsonProperty("type")] public string Type { get; set; }

            [JsonProperty("id")] public string Id { get; set; }

            [JsonProperty("meta")] public Meta Meta { get; set; }
        }

        public class Meta
        {
            [JsonProperty("services")] public Services Services { get; set; }

            [JsonProperty("channel")] public string Channel { get; set; }
        }

        public class Services
        {
        }
    }

    #endregion
}