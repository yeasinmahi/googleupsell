﻿using System;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class DBSSRequestResponse
    {
        [JsonProperty("data")] public RequestStatusData Data { get; set; }

        public class RequestStatusData
        {
            [JsonProperty("type")] public string Type { get; set; }

            [JsonProperty("attributes")] public RequestStatusAttributes Attributes { get; set; }

            [JsonProperty("id")] public string Id { get; set; }
        }

        public class RequestStatusAttributes
        {
            [JsonProperty("scheduled-at")] public DateTimeOffset ScheduledAt { get; set; }

            [JsonProperty("status")] public string Status { get; set; }
        }
    }
}