﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class FnfSummaryDbssResponse : ErrorResponse
    {
        public FnfSummaryDbssResponse()
        {
            //this.Data = new List<FnfSummaryDbssDatum>();
            Included = new List<FnfSummaryDbssIncluded>();
        }
        //[JsonProperty("data")]
        //public List<FnfSummaryDbssDatum> Data { get; set; }

        [JsonProperty("included")] public List<FnfSummaryDbssIncluded> Included { get; set; }
    }

    //public class FnfSummaryDbssDatum
    //{
    //[JsonProperty("attributes")]
    //public FnfSummaryDbssDatumAttributes Attributes { get; set; }

    //[JsonProperty("relationships")]
    //public FnfSummaryDbssDatumRelationships Relationships { get; set; }

    //[JsonProperty("links")]
    //public FnfSummaryDbssDatumLinks Links { get; set; }

    //[JsonProperty("id")]
    //public long Id { get; set; }

    //[JsonProperty("type")]
    //public string Type { get; set; }
    //}

    //public class FnfSummaryDbssDatumAttributes
    //{
    //    [JsonProperty("monthly-costs")]
    //    public long MonthlyCosts { get; set; }

    //    [JsonProperty("contract-status")]
    //    public string ContractStatus { get; set; }

    //    [JsonProperty("first-call-date")]
    //    public object FirstCallDate { get; set; }

    //    [JsonProperty("termination-time")]
    //    public object TerminationTime { get; set; }

    //    [JsonProperty("contract-id")]
    //    public long ContractId { get; set; }

    //    [JsonProperty("msisdn")]
    //    public string Msisdn { get; set; }

    //    [JsonProperty("activation-time")]
    //    public DateTimeOffset ActivationTime { get; set; }

    //    [JsonProperty("status")]
    //    public string Status { get; set; }

    //    [JsonProperty("latest-contract-termination-time")]
    //    public DateTimeOffset LatestContractTerminationTime { get; set; }

    //    [JsonProperty("directory-listing")]
    //    public string DirectoryListing { get; set; }

    //    [JsonProperty("payment-type")]
    //    public string PaymentType { get; set; }

    //    [JsonProperty("original-contract-confirmation-code")]
    //    public string OriginalContractConfirmationCode { get; set; }

    //    [JsonProperty("credit-limit")]
    //    public long CreditLimit { get; set; }
    //}

    //public partial class FnfSummaryDbssDatumLinks
    //{
    //    [JsonProperty("self")]
    //    public string Self { get; set; }
    //}

    //public class FnfSummaryDbssDatumRelationships
    //{
    //[JsonProperty("sim-cards")]
    //public FnfSummaryDbssAvailableChildProducts SimCards { get; set; }

    //[JsonProperty("services")]
    //public FnfSummaryDbssServices Services { get; set; }

    //[JsonProperty("subscription-discounts")]
    //public FnfSummaryDbssAvailableChildProducts SubscriptionDiscounts { get; set; }

    //[JsonProperty("network-services")]
    //public FnfSummaryDbssAvailableChildProducts NetworkServices { get; set; }

    //[JsonProperty("owner-customer")]
    //public FnfSummaryDbssBillingRatePlan OwnerCustomer { get; set; }

    //[JsonProperty("products")]
    //public FnfSummaryDbssAvailableChildProducts Products { get; set; }

    //[JsonProperty("payer-customer")]
    //public FnfSummaryDbssBillingRatePlan PayerCustomer { get; set; }

    //[JsonProperty("available-subscription-types")]
    //public FnfSummaryDbssAvailableChildProducts AvailableSubscriptionTypes { get; set; }

    //[JsonProperty("document-validations")]
    //public FnfSummaryDbssAvailableChildProducts DocumentValidations { get; set; }

    //[JsonProperty("product-usages")]
    //public FnfSummaryDbssAvailableChildProducts ProductUsages { get; set; }

    //[JsonProperty("porting-requests")]
    //public FnfSummaryDbssAvailableChildProducts PortingRequests { get; set; }

    //[JsonProperty("billing-rate-plan")]
    //public FnfSummaryDbssBillingRatePlan BillingRatePlan { get; set; }

    //[JsonProperty("combined-usage-report")]
    //public FnfSummaryDbssBillingRatePlan CombinedUsageReport { get; set; }

    //[JsonProperty("user-customer")]
    //public FnfSummaryDbssBillingRatePlan UserCustomer { get; set; }

    //[JsonProperty("gsm-service-usages")]
    //public FnfSummaryDbssAvailableChildProducts GsmServiceUsages { get; set; }

    //[JsonProperty("balances")]
    //public FnfSummaryDbssAvailableChildProducts Balances { get; set; }

    //[JsonProperty("billing-usages")]
    //public FnfSummaryDbssAvailableChildProducts BillingUsages { get; set; }

    //[JsonProperty("barrings")]
    //public FnfSummaryDbssAvailableChildProducts Barrings { get; set; }

    //[JsonProperty("subscription-type")]
    //public FnfSummaryDbssBillingRatePlan SubscriptionType { get; set; }

    //[JsonProperty("available-products")]
    //public FnfSummaryDbssAvailableChildProducts AvailableProducts { get; set; }

    //[JsonProperty("catalog-sim-cards")]
    //public FnfSummaryDbssAvailableChildProducts CatalogSimCards { get; set; }

    //[JsonProperty("connected-products")]
    //public FnfSummaryDbssAvailableChildProducts ConnectedProducts { get; set; }

    //[JsonProperty("connection-type")]
    //public FnfSummaryDbssBillingRatePlan ConnectionType { get; set; }

    //[JsonProperty("available-child-products")]
    //public FnfSummaryDbssAvailableChildProducts AvailableChildProducts { get; set; }

    //[JsonProperty("sim-card-orders")]
    //public FnfSummaryDbssAvailableChildProducts SimCardOrders { get; set; }
    //}

    //public class FnfSummaryDbssAvailableChildProducts
    //{
    //    [JsonProperty("links")]
    //    public FnfSummaryDbssPurpleLinks Links { get; set; }
    //}

    //public partial class FnfSummaryDbssPurpleLinks
    //{
    //    [JsonProperty("related")]
    //    public string Related { get; set; }
    //}

    public class FnfSummaryDbssBillingRatePlan
    {
        [JsonProperty("data")] public FnfSummaryDbssDat Data { get; set; }

        //[JsonProperty("links")]
        //public FnfSummaryDbssPurpleLinks Links { get; set; }
    }

    public class FnfSummaryDbssDat
    {
        //[JsonProperty("type")]
        //public string Type { get; set; }

        [JsonProperty("id")] public string Id { get; set; }
    }

    //public class FnfSummaryDbssServices
    //{
    //[JsonProperty("data")]
    //public Dat[] Data { get; set; }

    //[JsonProperty("links")]
    //public FnfSummaryDbssPurpleLinks Links { get; set; }
    //}

    public class FnfSummaryDbssIncluded
    {
        [JsonProperty("attributes")] public FnfSummaryDbssIncludedAttributes Attributes { get; set; }

        [JsonProperty("relationships")] public FnfSummaryDbssIncludedRelationships Relationships { get; set; }

        //[JsonProperty("links")]
        //public FnfSummaryDbssDatumLinks Links { get; set; }

        //[JsonProperty("id")]
        //public long Id { get; set; }

        [JsonProperty("type")] public string Type { get; set; }
    }

    public class FnfSummaryDbssIncludedAttributes
    {
        //[JsonProperty("service-parameters")]
        //public FnfSummaryDbssServiceParameters ServiceParameters { get; set; }

        [JsonProperty("service-parameters")] public Dictionary<string, string> ServiceParameters { get; set; }
    }

    //public class FnfSummaryDbssServiceParameters
    //{
    //    //[JsonProperty("faf_msisdn1")]
    //    //public string FafMsisdn1 { get; set; }

    //    //[JsonProperty("faf_msisdn2")]
    //    //public string FafMsisdn2 { get; set; }
    //    [JsonProperty("service-parameters")]
    //    public Dictionary<string, string> ServiceParameters { get; set; }
    //}

    public class FnfSummaryDbssIncludedRelationships
    {
        //[JsonProperty("product")]
        //public FnfSummaryDbssBillingRatePlan Product { get; set; }

        [JsonProperty("service")] public FnfSummaryDbssBillingRatePlan Service { get; set; }

        //[JsonProperty("subscription")]
        //public FnfSummaryDbssBillingRatePlan Subscription { get; set; }
    }
}