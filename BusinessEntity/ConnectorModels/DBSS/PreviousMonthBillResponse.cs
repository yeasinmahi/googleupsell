﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class PreviousMonthBillResponse : ErrorResponse
    {
        public PreviousMonthBillResponse()
        {
            Data = new List<PreviousMonthDatum>();
        }

        [JsonProperty("data")] public List<PreviousMonthDatum> Data { get; set; }
    }

    public class PreviousMonthDatum
    {
        [JsonProperty("attributes")] public PreviousMonthAttributes Attributes { get; set; }

        //[JsonProperty("relationships")]
        //public PreviousMonthRelationships Relationships { get; set; }

        //[JsonProperty("links")]
        //public DatumLinks Links { get; set; }

        //[JsonProperty("id")]
        //public string Id { get; set; }

        //[JsonProperty("type")]
        //public string Type { get; set; }
    }

    public class PreviousMonthAttributes
    {
        [JsonProperty("international-airtime-charges")]
        public long InternationalAirtimeCharges { get; set; }

        [JsonProperty("unbilled-single-fees")] public long UnbilledSingleFees { get; set; }

        [JsonProperty("call-forwarding-while-roaming")]
        public long CallForwardingWhileRoaming { get; set; }

        [JsonProperty("local-airtime-charges")]
        public long LocalAirtimeCharges { get; set; }

        [JsonProperty("monthly-subscription-fee")]
        public long MonthlySubscriptionFee { get; set; }

        [JsonProperty("monthly-line-rent")] public long MonthlyLineRent { get; set; }

        [JsonProperty("unbilled-total")] public long UnbilledTotal { get; set; }

        [JsonProperty("other-charges")] public long OtherCharges { get; set; }

        [JsonProperty("unbilled-fixed-fees")] public long UnbilledFixedFees { get; set; }

        [JsonProperty("total-current-charges")]
        public long TotalCurrentCharges { get; set; }

        [JsonProperty("unbilled-calls")] public long UnbilledCalls { get; set; }

        [JsonProperty("discounts")] public long Discounts { get; set; }

        [JsonProperty("previous-balance")] public long PreviousBalance { get; set; }

        [JsonProperty("adjustments")] public long Adjustments { get; set; }

        [JsonProperty("value-added-services")] public long ValueAddedServices { get; set; }

        [JsonProperty("billing-account-type")] public string BillingAccountType { get; set; }

        //[JsonProperty("outside-plan")]
        //public object[] OutsidePlan { get; set; }

        //[JsonProperty("product-usage")]
        //public object[] ProductUsage { get; set; }

        [JsonProperty("total-without-tax")] public long TotalWithoutTax { get; set; }

        [JsonProperty("total-with-tax")] public long TotalWithTax { get; set; }

        [JsonProperty("VAT")] public long Vat { get; set; }
    }

    //public class PreviousMonthRelationships
    //{
    //    [JsonProperty("billing-account")]
    //    public PreviousMonthBillingAccount BillingAccount { get; set; }
    //}

    //public class PreviousMonthBillingAccount
    //{
    //    //[JsonProperty("data")]
    //    //public Data Data { get; set; }

    //    [JsonProperty("links")]
    //    public PreviousMonthBillingAccountLinks Links { get; set; }
    //}

    //public class PreviousMonthBillingAccountLinks
    //{
    //    [JsonProperty("related")]
    //    public string Related { get; set; }
    //}
}