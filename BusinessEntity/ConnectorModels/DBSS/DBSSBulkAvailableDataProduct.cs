﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class DBSSBulkAvailableDataProduct : ErrorResponse
    {
        public DBSSBulkAvailableDataProduct()
        {
            BulkDatums = new List<BulkDatum>();
            Includeds = new List<Included>();
        }

        [JsonProperty("data")] public List<BulkDatum> BulkDatums { get; set; }

        [JsonProperty("included")] public List<Included> Includeds { get; set; }


        public class BulkDatum
        {
            //[JsonProperty("attributes")]
            //public DatumAttributes Attributes { get; set; }

            //[JsonProperty("relationships")]
            //public DatumRelationships Relationships { get; set; }

            //[JsonProperty("links")]
            //public DatumLinks Links { get; set; }

            //[JsonProperty("id")]
            //public long Id { get; set; }

            //[JsonProperty("type")]
            //public string Type { get; set; }
        }

        public class DatumAttributes
        {
            //[JsonProperty("monthly-costs")]
            //public long MonthlyCosts { get; set; }

            //[JsonProperty("allow-reactivation")]
            //public bool AllowReactivation { get; set; }

            //[JsonProperty("contract-status")]
            //public string ContractStatus { get; set; }

            //[JsonProperty("first-call-date")]
            //public DateTimeOffset? FirstCallDate { get; set; }

            //[JsonProperty("termination-time")]
            //public DateTimeOffset? TerminationTime { get; set; }

            //[JsonProperty("contract-id")]
            //public long ContractId { get; set; }

            //[JsonProperty("msisdn")]
            //public string Msisdn { get; set; }

            //[JsonProperty("activation-time")]
            //public DateTimeOffset? ActivationTime { get; set; }

            //[JsonProperty("status")]
            //public string Status { get; set; }

            //[JsonProperty("latest-contract-termination-time")]
            //public DateTimeOffset? LatestContractTerminationTime { get; set; }

            //[JsonProperty("directory-listing")]
            //public string DirectoryListing { get; set; }

            //[JsonProperty("payment-type")]
            //public string PaymentType { get; set; }

            //[JsonProperty("original-contract-confirmation-code")]
            //public string OriginalContractConfirmationCode { get; set; }
        }

        //public class DatumLinks
        //{
        //    [JsonProperty("self")]
        //    public string Self { get; set; }
        //}

        public class DatumRelationships
        {
            //[JsonProperty("sim-cards")]
            //public TartuGecko SimCards { get; set; }

            //[JsonProperty("billing-accounts")]
            //public AvailableProducts BillingAccounts { get; set; }

            //[JsonProperty("services")]
            //public TartuGecko Services { get; set; }

            //[JsonProperty("combined-usage-reports")]
            //public TartuGecko CombinedUsageReports { get; set; }

            //[JsonProperty("subscription-discounts")]
            //public TartuGecko SubscriptionDiscounts { get; set; }

            //[JsonProperty("network-services")]
            //public TartuGecko NetworkServices { get; set; }

            //[JsonProperty("available-loan-products")]
            //public TartuGecko AvailableLoanProducts { get; set; }

            //[JsonProperty("owner-customer")]
            //public BillingRatePlan OwnerCustomer { get; set; }

            //[JsonProperty("products")]
            //public TartuGecko Products { get; set; }

            //[JsonProperty("payer-customer")]
            //public BillingRatePlan PayerCustomer { get; set; }

            //[JsonProperty("available-subscription-types")]
            //public TartuGecko AvailableSubscriptionTypes { get; set; }

            //[JsonProperty("document-validations")]
            //public TartuGecko DocumentValidations { get; set; }

            //[JsonProperty("coordinator-customer")]
            //public TartuGecko CoordinatorCustomer { get; set; }

            //[JsonProperty("product-usages")]
            //public TartuGecko ProductUsages { get; set; }

            //[JsonProperty("porting-requests")]
            //public TartuGecko PortingRequests { get; set; }

            //[JsonProperty("billing-rate-plan")]
            //public BillingRatePlan BillingRatePlan { get; set; }

            //[JsonProperty("user-customer")]
            //public BillingRatePlan UserCustomer { get; set; }

            //[JsonProperty("gsm-service-usages")]
            //public TartuGecko GsmServiceUsages { get; set; }

            //[JsonProperty("balances")]
            //public TartuGecko Balances { get; set; }

            //[JsonProperty("billing-usages")]
            //public TartuGecko BillingUsages { get; set; }

            //[JsonProperty("barrings")]
            //public TartuGecko Barrings { get; set; }

            //[JsonProperty("subscription-type")]
            //public BillingRatePlan SubscriptionType { get; set; }

            //[JsonProperty("available-products")]
            //public AvailableProducts AvailableProducts { get; set; }

            //[JsonProperty("catalog-sim-cards")]
            //public TartuGecko CatalogSimCards { get; set; }

            //[JsonProperty("connected-products")]
            //public TartuGecko ConnectedProducts { get; set; }

            //[JsonProperty("connection-type")]
            //public BillingRatePlan ConnectionType { get; set; }

            //[JsonProperty("available-child-products")]
            //public TartuGecko AvailableChildProducts { get; set; }

            //[JsonProperty("sim-card-orders")]
            //public TartuGecko SimCardOrders { get; set; }
        }

        //public class TartuGecko
        //{
        //    //[JsonProperty("links")]
        //    //public PurpleLinks Links { get; set; }
        //}

        //public class PurpleLinks
        //{
        //    //[JsonProperty("related")]
        //    //public string Related { get; set; }
        //}

        //public class AvailableProducts
        //{
        //    //[JsonProperty("data")]
        //    //public Dat[] Data { get; set; }

        //    //[JsonProperty("links")]
        //    //public PurpleLinks Links { get; set; }
        //}

        //public class Dat
        //{
        //    //[JsonProperty("type")]
        //    //public string Type { get; set; }

        //    //[JsonProperty("id")]
        //    //public string Id { get; set; }
        //}

        public class BillingRatePlan
        {
            [JsonProperty("data")] public Dat Data { get; set; }

            //[JsonProperty("links")]
            //public PurpleLinks Links { get; set; }
        }

        public class Included
        {
            [JsonProperty("attributes")] public IncludedAttributes Attributes { get; set; }

            [JsonProperty("relationships")] public IncludedRelationships Relationships { get; set; }

            //[JsonProperty("links")]
            //public DatumLinks Links { get; set; }

            [JsonProperty("id")] public long Id { get; set; }

            //[JsonProperty("type")]
            //public string Type { get; set; }
        }

        public class IncludedAttributes
        {
            //[JsonProperty("activation-template-id", NullValueHandling = NullValueHandling.Ignore)]
            //public string[] ActivationTemplateId { get; set; }

            [JsonProperty("periodic-amount", NullValueHandling = NullValueHandling.Ignore)]
            public long? PeriodicAmount { get; set; }

            [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
            public Name Name { get; set; }

            //[JsonProperty("ussd-code")]
            //public object UssdCode { get; set; }

            //[JsonProperty("deactivation-template-id", NullValueHandling = NullValueHandling.Ignore)]
            //public string[] DeactivationTemplateId { get; set; }

            [JsonProperty("is-configurable", NullValueHandling = NullValueHandling.Ignore)]
            public bool? IsConfigurable { get; set; }

            [JsonProperty("long-info-text", NullValueHandling = NullValueHandling.Ignore)]
            public Name LongInfoText { get; set; }

            [JsonProperty("periodic-unit", NullValueHandling = NullValueHandling.Ignore)]
            public string PeriodicUnit { get; set; }

            //[JsonProperty("tags", NullValueHandling = NullValueHandling.Ignore)]
            //public string[] Tags { get; set; }

            //[JsonProperty("advice-on-charge", NullValueHandling = NullValueHandling.Ignore)]
            //public AdviceOnCharge AdviceOnCharge { get; set; }

            [JsonProperty("short-description", NullValueHandling = NullValueHandling.Ignore)]
            public Name ShortDescription { get; set; }

            //[JsonProperty("activation-renewal-template-id", NullValueHandling = NullValueHandling.Ignore)]
            //public string[] ActivationRenewalTemplateId { get; set; }

            [JsonProperty("code", NullValueHandling = NullValueHandling.Ignore)]
            public string Code { get; set; }

            //[JsonProperty("renewal-warning-template-id", NullValueHandling = NullValueHandling.Ignore)]
            //public string[] RenewalWarningTemplateId { get; set; }

            [JsonProperty("allow-re-activation", NullValueHandling = NullValueHandling.Ignore)]
            public bool? AllowReActivation { get; set; }

            //[JsonProperty("long-description", NullValueHandling = NullValueHandling.Ignore)]
            //public LongDescription LongDescription { get; set; }

            //[JsonProperty("info-text", NullValueHandling = NullValueHandling.Ignore)]
            //public AdviceOnCharge InfoText { get; set; }

            [JsonProperty("charge-type", NullValueHandling = NullValueHandling.Ignore)]
            public string ChargeType { get; set; }

            //[JsonProperty("modifications-template-id", NullValueHandling = NullValueHandling.Ignore)]
            //public string[] ModificationsTemplateId { get; set; }

            //[JsonProperty("medium-description", NullValueHandling = NullValueHandling.Ignore)]
            //public AdviceOnCharge MediumDescription { get; set; }

            [JsonProperty("display-order", NullValueHandling = NullValueHandling.Ignore)]
            public long? DisplayOrder { get; set; }

            //[JsonProperty("fee", NullValueHandling = NullValueHandling.Ignore)]
            //public long? Fee { get; set; }

            //[JsonProperty("fee-type", NullValueHandling = NullValueHandling.Ignore)]
            //public string FeeType { get; set; }

            //[JsonProperty("tax-inclusive", NullValueHandling = NullValueHandling.Ignore)]
            //public bool? TaxInclusive { get; set; }
        }

        //public class LongDescription
        //{
        //    [JsonProperty("en")]
        //    public string En { get; set; }

        //    [JsonProperty("bn")]
        //    public string Bn { get; set; }
        //}

        public class IncludedRelationships
        {
            //[JsonProperty("services", NullValueHandling = NullValueHandling.Ignore)]
            //public TartuGecko Services { get; set; }

            //[JsonProperty("traffic-bundles", NullValueHandling = NullValueHandling.Ignore)]
            //public TartuGecko TrafficBundles { get; set; }

            //[JsonProperty("option-group", NullValueHandling = NullValueHandling.Ignore)]
            //public TartuGecko OptionGroup { get; set; }

            [JsonProperty("product-family", NullValueHandling = NullValueHandling.Ignore)]
            public BillingRatePlan ProductFamily { get; set; }

            //[JsonProperty("fees", NullValueHandling = NullValueHandling.Ignore)]
            //public AvailableProducts Fees { get; set; }

            //[JsonProperty("product-relationships", NullValueHandling = NullValueHandling.Ignore)]
            //public TartuGecko ProductRelationships { get; set; }

            //[JsonProperty("billing-item", NullValueHandling = NullValueHandling.Ignore)]
            //public BillingRatePlan BillingItem { get; set; }
        }
    }
}