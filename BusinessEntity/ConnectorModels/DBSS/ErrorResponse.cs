﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class ErrorResponse
    {
        public ErrorResponse()
        {
            ResponseCodes = ErrorResponseCodes();
        }

        [JsonIgnore] public Dictionary<string, string> ResponseCodes { get; set; }

        [JsonProperty("errors")] public List<Error> Errors { get; set; }

        [JsonIgnore]
        public Error GetDefaultError
        {
            get
            {
                var error = Errors.Any()
                    ? Errors.FirstOrDefault() != null
                        ? Errors.FirstOrDefault()
                        : new Error("Operation cannot be done due to technical problem!!!")
                    : new Error("Operation cannot be done due to technical problem!!!");

                return error;
            }
        }

        public void HandleError()
        {
            var error = Errors.Any()
                ? Errors.FirstOrDefault() != null
                    ? Errors.FirstOrDefault()
                    : new Error("Operation cannot be done due to technical problem!!!")
                : new Error("Operation cannot be done due to technical problem!!!");

            var message = error.Status > 0
                ? GetErrorMessage(error.Status.ToString())
                : "Exception occurred-[" + error.Status + "] " + "-" + error.Detail;

            throw new Exception(message);
            //return message;
        }

        public string GetErrorMessage(string code)
        {
            var message = string.Empty;
            try
            {
                message = ResponseCodes[code];
            }
            catch
            {
            }

            if (string.IsNullOrEmpty(message))
                message = "Operation cannot be done due to technical problem";

            return message;
        }


        private static Dictionary<string, string> ErrorResponseCodes()
        {
            var errorCodes = new Dictionary<string, string>
            {
                {"0", "OK"},
                {"1", "Unknown Error"},
                {"2", "Invalid Operation"},
                {"3", "Database Error"},
                {"10", "Invalid Action"},
                {"11", "Error Updating Status"},
                {"12", "Error Retrieving requesterMSISDN Information"},
                {"13", "Error Retrieving listMSISDN Information"},
                {"14", "List is not configured (white, black, etc.)"},
                {"15", "requesterMSISDN Already Activated/Deactivated"},
                {"21", "Msisdn Is already in the list"},
                {"22", "Error While adding requesterMSISDN"},
                {"23", "Error Deleting requesterMSISDN"},
                {"24", "List Limit Reached"},
                {"25", "This number is not in your FnF list."},
                {"31", "No such bundle"},
                {"32", "Already subscribed"},
                {"33", "Not subscribed"},
                {"34", "Incorrect Password"},
                {"35", "Incorrect Input Parameters"},
                {"36", "Subscriber is locked"},
                {"37", "Your provided number is incorrect. Please enter a valid number."},
                {"38", "No SMS is configured for either party"},
                {"39", "No password is configured"},
                {"40", "No such status for this service"},
                {"50", "Sorry, your request cannot be executed"},
                {"51", "Your balance is low. Please recharge and try again"},
                {"52", "Charging failed"},
                {"53", "Missing UCIP connector"},
                {"54", "Postpaid subscriber"},
                {"55", "Missing default lang"},
                {"56", "Service missing"},
                {"57", "Service class missing"},
                {"58", "No permission"},
                {"59", "Missing lang"},
                {"60", "Invalid configuration of grace and retry periods"},
                {"61", "Subscription of service is not active"},
                {"62", "no such configuration"},
                {"63", "Refund fail"},
                {"64", "Refund OK"},
                {"65", "Reach limit subscription"},
                {"66", "Service dependence error"},
                {"67", "Not exist reason"},
                {"68", "Not exist message"},
                {"69", "Sorry. You cannot replace this number from your list."},
                {"70", "No menu for profile"},
                {"71", "Usage limit exceeded"},
                {"72", "No possible migrations"},
                {"73", "Incorrect choice"},
                {"74", "Not enough time passed since last operation"},
                {"75", "Incorrect new service class"},
                {"80", "No possible coupons to be purchased"},
                {"81", "Limit gift exceeded"},
                {"82", "Purchase/gift not allowed"},
                {"83", "Incorrect entered requesterMSISDN"},
                {"84", "Not allowed B number"},
                {"85", "Incorrect coupon"},
                {"102", "Subscriber not found (UCIP)"},
                {"107", "Voucher used (UCIP)"},
                {"108", "Voucher used  by other (UCIP)"},
                {"119", "Invalid Voucher"},
                {"201", "Canceled confirm AOC (need for message only)"},
                {"202", "Canceled charge AOC (need for message only)"},
                {"1000", "Max tries reached"},
                {"1001", "Already in FPL"},
                {"1002", "You already have this number in your Super FNF"},
                {"1003", "You already have this number in your FNF list"},
                {"1004", "You cannot enter your own number."},
                {"1005", "Your provided number is incorrect. Please enter a valid number."},
                {"1006", "Numbers with this prefix are not allowed."},
                {"1007", "You cannot add this particular number as FnF or Super FnF."},
                {"1008", "Sorry your FnF list is full; please delete or replace a number before adding a new one"},
                {"1009", "Charged admin exceeded"},
                {"1010", "Move from FnF to SFnF"},
                {"1011", "Empty list"},
                {"1012", "Missing requesterMSISDN to del"},
                {"1013", "Unable to make the change right now"},
                {"1014", "Invalid MSISDN_ID to delete"},
                {"2001", "Not Valid Event configuration (Not Allowed Command)"},
                {"2002", "Not Valid Command"},
                {"2003", "Not Valid Action (Allowed: Activation/Deactivation)"},
                {"2011", "Fail to connect to DB"},
                {"2012", "Error get Bundle Configuration"},
                {"2013", "No Bundle Configured with Provided ID"},
                {"3002", "PSO Connector Error"},
                {"3003", "PSO Request Error"},
                {"3004", "Accumulator Connector Error"},
                {"3005", "Accumulator Request Error"},
                {"3006", "PAM Connector Error"},
                {"3007", "PAM Request Error"},
                {"3008", "Offer Connector Error"},
                {"3009", "Offer Request Error"},
                {"3011", "Refill Connector Error"},
                {"3012", "Refill Request Error"},
                {"5000", "Connector`s error"},
                {"6000", "Your input is invalid. Please try with a valid number"},
                {"6001", "No such keyword"},
                {"6002", "invalid configuration"}
            };

            return errorCodes;
        }
    }

    public class Error
    {
        public Error()
        {
        }

        public Error(string details)
        {
            Detail = details;
        }

        [JsonProperty("detail")] public string Detail { get; set; }

        [JsonProperty("source")] public Source Source { get; set; }

        [JsonProperty("status")] public int Status { get; set; }

        [JsonProperty("meta")] public Meta Meta { get; set; }
    }

    public class Meta
    {
        [JsonProperty("error_code")] public string ErrorCode { get; set; }
    }

    public class Source
    {
        [JsonProperty("pointer")] public string Pointer { get; set; }
    }
}