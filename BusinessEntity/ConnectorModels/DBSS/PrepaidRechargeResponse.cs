﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class PrepaidRechargeResponse : ErrorResponse
    {
        public PrepaidRechargeResponse()
        {
            Data = new List<PrepaidRechargeDatum>();
        }

        [JsonProperty("data")] public List<PrepaidRechargeDatum> Data { get; set; }
    }

    public class PrepaidRechargeDatum
    {
        [JsonProperty("type")] public string Type { get; set; }

        [JsonProperty("attributes")] public PrepaidRechargeAttributes Attributes { get; set; }
    }

    public class PrepaidRechargeAttributes
    {
        //[JsonProperty("transaction-id")]
        //public string TransactionId { get; set; }

        //[JsonProperty("record-id")]
        //public string RecordId { get; set; }

        [JsonProperty("transaction-sub-type")] public string TransactionSubType { get; set; }

        [JsonProperty("edr-type")] public string EdrType { get; set; }

        [JsonProperty("edr-type-label")] public string EdrTypeLabel { get; set; }

        //[JsonProperty("subscription-id")]
        //public long SubscriptionId { get; set; }

        //[JsonProperty("created-at")]
        //public DateTimeOffset CreatedAt { get; set; }

        //[JsonProperty("msisdn")]
        //public string Msisdn { get; set; }

        [JsonProperty("event-at")] public DateTimeOffset EventAt { get; set; }

        [JsonProperty("balances")] public PrepaidRechargeBalance[] Balances { get; set; }

        //[JsonProperty("comment")]
        //public string Comment { get; set; }

        //[JsonProperty("transaction-type")]
        //public long TransactionType { get; set; }

        //[JsonProperty("event-source")]
        //public long EventSource { get; set; }

        //[JsonProperty("customer-id")]
        //public long CustomerId { get; set; }
    }

    public class PrepaidRechargeBalance
    {
        [JsonProperty("transaction-amount")] public double? TransactionAmount { get; set; }

        //[JsonProperty("balance-before")]
        //public double? BalanceBefore { get; set; }

        //[JsonProperty("product-id")]
        //public string ProductId { get; set; }

        //[JsonProperty("offer-id")]
        //public string OfferId { get; set; }

        //[JsonProperty("balance-id")]
        //public string BalanceId { get; set; }

        //[JsonProperty("unit-type")]
        //public long? UnitType { get; set; }

        [JsonProperty("balance-after")] public double? BalanceAfter { get; set; }
    }
}