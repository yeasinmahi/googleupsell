﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class BillingReportResponseNew : ErrorResponse
    {
        public BillingReportResponseNew()
        {
            Data = new List<BillingReportResponseNewDatum>();
        }

        [JsonProperty("data")] public List<BillingReportResponseNewDatum> Data { get; set; }
    }


    public class BillingReportResponseNewDatum
    {
        //[JsonProperty("type")]
        //public string Type { get; set; }

        [JsonProperty("attributes")] public BillingReportNewAttributes Attributes { get; set; }

        //[JsonProperty("relationships")]
        //public BillingReportNewRelationships Relationships { get; set; }

        //[JsonProperty("id")]
        //public string Id { get; set; }
    }

    public class BillingReportNewAttributes
    {
        //[JsonProperty("debt")]
        //public double Debt { get; set; }

        [JsonProperty("deposits")] public BillingReportNewDeposits Deposits { get; set; }

        [JsonProperty("advance-payment")] public double AdvancePayment { get; set; }

        //[JsonProperty("unbilled")]
        //public double Unbilled { get; set; }

        [JsonProperty("over-payment")] public double OverPayment { get; set; }

        //[JsonProperty("credit-limit-usage")]
        //public double CreditLimitUsage { get; set; }

        //[JsonProperty("credit-limit")]
        //public double CreditLimit { get; set; }
    }

    public class BillingReportNewDeposits
    {
        [JsonProperty("default")] public long Default { get; set; }
    }

    //public class BillingReportNewRelationships
    //{
    //    [JsonProperty("billing-account")]
    //    public BillingReportNewBillingAccount BillingAccount { get; set; }
    //}

    //public class BillingReportNewBillingAccount
    //{
    //    [JsonProperty("data")]
    //    public BillingReportNewData Data { get; set; }
    //}

    //public class BillingReportNewData
    //{
    //    [JsonProperty("type")]
    //    public string Type { get; set; }

    //    [JsonProperty("id")]
    //    public long Id { get; set; }
    //}
}