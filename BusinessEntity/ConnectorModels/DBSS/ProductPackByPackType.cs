﻿namespace BusinessEntity.ConnectorModels.DBSS
{
    public class ProductPackByPackType
    {
        public string ProductName { get; set; }
        public string ProductLongInfoText { get; set; }
        public string ProductShortDescription { get; set; }
        public string ProductLongDescription { get; set; }
        public string ProductInfoText { get; set; }
        public string ProductMediumDescription { get; set; }
        public string ProductCode { get; set; }
        public double PeriodicAmount { get; set; }
        public bool IsConfigurable { get; set; }
        public string PeriodicUnit { get; set; }
        public bool AllowReActivation { get; set; }
        public string ChargeType { get; set; }
        public long DisplayOrder { get; set; }
        public string ProductFamilyID { get; set; }
        public string ProductFamilyName { get; set; }
    }
}