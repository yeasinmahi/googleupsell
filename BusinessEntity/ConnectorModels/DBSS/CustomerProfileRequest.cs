﻿using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class CustomerProfileRquest
    {
        [JsonProperty("data")] public CustomerProfileRquestData Data { get; set; }
    }

    public class CustomerProfileRquestData
    {
        [JsonProperty("type")] public string Type { get; set; }

        [JsonProperty("id")] public long Id { get; set; }

        [JsonProperty("attributes")] public CustomerProfileRquestAttributes Attributes { get; set; }
    }

    public class CustomerProfileRquestAttributes
    {
        [JsonProperty("email")] public string Email { get; set; }

        [JsonProperty("alt_contact_phone")] public string AlternativeContactNumber { get; set; }

        [JsonProperty("date-of-birth")]
        // public DateTimeOffset DateOfBirth { get; set; }
        public string DateOfBirth { get; set; }
    }
}