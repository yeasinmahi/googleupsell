﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class BarringResponse : ErrorResponse
    {
        public BarringResponse()
        {
            Data = new List<BarringDatum>();
        }

        [JsonProperty("data")] public List<BarringDatum> Data { get; set; }
    }

    public class BarringDatum
    {
        [JsonProperty("attributes")] public BarringAttributes Attributes { get; set; }

        //[JsonProperty("relationships")]
        //public BarringRelationships Relationships { get; set; }

        //[JsonProperty("links")]
        //public BarringDatumLinks Links { get; set; }

        [JsonProperty("id")] public string Id { get; set; }

        [JsonProperty("type")] public string Type { get; set; }
    }

    public class BarringAttributes
    {
        [JsonProperty("change-ongoing")] public bool ChangeOngoing { get; set; }

        [JsonProperty("upcoming-level")] public object UpcomingLevel { get; set; }

        [JsonProperty("subscription-id")] public long SubscriptionId { get; set; }

        [JsonProperty("barring-level-ids")] public string[] BarringLevelIds { get; set; }

        [JsonProperty("upcoming-barring-level-ids")]
        public object[] UpcomingBarringLevelIds { get; set; }

        [JsonProperty("level")] public string Level { get; set; }
    }

    //public partial class BarringDatumLinks
    //{
    //    [JsonProperty("self")]
    //    public string Self { get; set; }
    //}

    //public class BarringRelationships
    //{
    //    [JsonProperty("barring")]
    //    public Barring Barring { get; set; }

    //    [JsonProperty("barring-levels")]
    //    public BarringLevels BarringLevels { get; set; }
    //}

    //public class Barring
    //{
    //    [JsonProperty("data")]
    //    public BarringData Data { get; set; }

    //    [JsonProperty("links")]
    //    public BarringLinks Links { get; set; }
    //}

    //public class BarringData
    //{
    //    [JsonProperty("type")]
    //    public string Type { get; set; }

    //    [JsonProperty("id")]
    //    public string Id { get; set; }
    //}

    //public class BarringLinks
    //{
    //    [JsonProperty("related")]
    //    public string Related { get; set; }
    //}

    //public partial class BarringLevels
    //{
    //    [JsonProperty("links")]
    //    public BarringLinks Links { get; set; }
    //}
}