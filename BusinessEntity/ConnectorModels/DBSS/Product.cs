﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class Product : ErrorResponse
    {
        public Product()
        {
            Datas = new List<Datum>();
        }

        [JsonProperty("data")] public List<Datum> Datas { get; set; }


        public class Datum
        {
            [JsonProperty("attributes")] public Attributes Attributes { get; set; }

            [JsonProperty("relationships")] public Relationships Relationships { get; set; }

            //[JsonProperty("links")]
            //public DatumLinks Links { get; set; }

            [JsonProperty("id")]
            //[JsonConverter(typeof(ParseStringConverter))]
            public string Id { get; set; }

            //[JsonProperty("type")]
            //public DatumType Type { get; set; }
        }

        public class Attributes
        {
            //public Attributes()
            //{
            //    Tags = new string[] { };
            //}
            [JsonProperty("periodic-amount")] public long PeriodicAmount { get; set; }

            [JsonProperty("name")] public Name Name { get; set; }

            [JsonProperty("is-configurable")] public bool IsConfigurable { get; set; }

            [JsonProperty("long-info-text")] public Name LongInfoText { get; set; }

            [JsonProperty("periodic-unit")] public string PeriodicUnit { get; set; }

            [JsonProperty("short-description")] public Name ShortDescription { get; set; }

            [JsonProperty("code")] public string Code { get; set; }

            [JsonProperty("allow-re-activation")] public bool AllowReActivation { get; set; }

            [JsonProperty("long-description")] public Name LongDescription { get; set; }

            //[JsonProperty("info-text")]
            //public InfoText InfoText { get; set; }

            [JsonProperty("charge-type")] public string ChargeType { get; set; }

            //[JsonProperty("medium-description")]
            //public InfoText MediumDescription { get; set; }

            [JsonProperty("display-order")] public long DisplayOrder { get; set; }

            //[JsonProperty("ussd-code")]
            //public string UssdCode { get; set; }


            //[JsonProperty("tags")]
            //public string[] Tags { get; set; }
        }

        //public class LongDescription
        //{
        //}


        public class Relationships
        {
            //[JsonProperty("services")]
            //public Fees Services { get; set; }

            //[JsonProperty("traffic-bundles")]
            //public Fees TrafficBundles { get; set; }

            //[JsonProperty("option-group")]
            //public Fees OptionGroup { get; set; }

            [JsonProperty("product-family")] public ProductFamily ProductFamily { get; set; }

            //[JsonProperty("fees")]
            //public Fees Fees { get; set; }
        }

        //public class Fees
        //{
        //    [JsonProperty("links")]
        //    public FeesLinks Links { get; set; }
        //}

        //public class FeesLinks
        //{
        //    [JsonProperty("related")]
        //    public string Related { get; set; }
        //}

        public class ProductFamily
        {
            [JsonProperty("data", NullValueHandling = NullValueHandling.Ignore)]
            public Data Data { get; set; }

            //[JsonProperty("links")]
            //public FeesLinks Links { get; set; }
        }

        public class Data
        {
            [JsonProperty("type")] public string Type { get; set; }

            [JsonProperty("id")] public string Id { get; set; }
        }

        //public enum DatumType { Products };
    }
}