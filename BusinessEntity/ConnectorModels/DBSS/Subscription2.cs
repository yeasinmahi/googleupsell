﻿using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class Subscription2
    {
        [JsonProperty("data")] public Subscription2Data Data { get; set; }
    }

    public class Subscription2Data
    {
        [JsonProperty("attributes")] public Subscription2Attributes Attributes { get; set; }
    }

    public class Subscription2Attributes
    {
        [JsonProperty("msisdn")] public string Msisdn { get; set; }
    }
}