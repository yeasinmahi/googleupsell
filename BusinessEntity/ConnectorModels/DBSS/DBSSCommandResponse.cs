﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class DBSSCommandResponse : ErrorResponse
    {
        public DBSSCommandResponse()
        {
            Data = new List<DBSSCommandData>();
        }

        [JsonProperty("data")] public List<DBSSCommandData> Data { get; set; }

        public class DBSSCommandData
        {
            [JsonProperty("type")] public string Type { get; set; }

            [JsonProperty("id")] public string Id { get; set; }

            [JsonProperty("attributes")] public DBSSCommandAttributes Attributes { get; set; }
        }

        public class DBSSCommandAttributes
        {
            [JsonProperty("request-id")] public string RequestId { get; set; }

            [JsonProperty("href")] public string Href { get; set; }

            [JsonProperty("scheduled-at")] public DateTimeOffset ScheduledAt { get; set; }

            [JsonProperty("status")] public string Status { get; set; }
        }
    }
}