﻿using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class GiftVolumeRequestBody
    {
        [JsonProperty("data")] public GiftVolumeRequestBodyData Data { get; set; }
    }

    public class GiftVolumeRequestBodyData
    {
        [JsonProperty("type")] public string Type { get; set; }

        [JsonProperty("id")] public string Id { get; set; }

        [JsonProperty("meta")] public GiftVolumeRequestBodyMeta Meta { get; set; }
    }

    public class GiftVolumeRequestBodyMeta
    {
        [JsonProperty("services")] public GiftVolumeRequestBodyServices Services { get; set; }

        [JsonProperty("channel")] public string Channel { get; set; }
    }

    public class GiftVolumeRequestBodyServices
    {
        [JsonProperty("TRANSFER")] public Transfer Transfer { get; set; }
    }

    public class Transfer
    {
        [JsonProperty("b_msisdn")] public string GiftTo { get; set; }
    }
}