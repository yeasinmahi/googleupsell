﻿using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class DBSSCommonRequestBody : ErrorResponse
    {
        [JsonProperty("data")] public DBSSCommonRequestBodyData Data { get; set; }

        public string GetJson(string productId, string type, string channel)
        {
            var model = new DBSSCommonRequestBody
            {
                Data = new DBSSCommonRequestBodyData
                {
                    Id = productId,
                    Type = type,
                    Meta = new DBSSCommonRequestBodyMeta
                    {
                        Channel = channel
                    }
                }
            };
            return JsonConvert.SerializeObject(model);
        }
    }

    public class DBSSCommonRequestBodyData
    {
        [JsonProperty("type")] public string Type { get; set; }

        [JsonProperty("id")] public string Id { get; set; }

        [JsonProperty("meta")] public DBSSCommonRequestBodyMeta Meta { get; set; }
    }

    public class DBSSCommonRequestBodyMeta
    {
        public DBSSCommonRequestBodyMeta()
        {
            Services = new DBSSCommonRequestBodyServices();
        }

        [JsonProperty("services")] public DBSSCommonRequestBodyServices Services { get; set; }

        [JsonProperty("channel")] public string Channel { get; set; }
    }

    public class DBSSCommonRequestBodyServices
    {
    }
}