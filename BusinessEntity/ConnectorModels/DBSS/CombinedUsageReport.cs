﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class CombinedUsageReport : ErrorResponse
    {
        //public class Relationships
        //{
        //    [JsonProperty("billing-account")]
        //    public BillingAccount BillingAccount { get; set; }
        //}

        //public class BillingAccountLinks
        //{
        //    [JsonProperty("related")]
        //    public string Related { get; set; }
        //}

        //public class BillingAccount
        //{
        //    [JsonProperty("data")]
        //    public Data Data { get; set; }

        //    [JsonProperty("links")]
        //    public BillingAccountLinks Links { get; set; }
        //}

        //public class Data
        //{
        //    [JsonProperty("type")]
        //    public string Type { get; set; }

        //    [JsonProperty("id")]
        //    public long Id { get; set; }
        //}

        public enum BillingAccountType
        {
            local,
            roaming
        }

        [JsonProperty("data")] public List<ResponseData> ResponseDatas { get; set; }

        public class ResponseData
        {
            [JsonProperty("type")] public string Type { get; set; }

            [JsonProperty("attributes")] public Attributes Attributes { get; set; }

            //[JsonProperty("relationships")]
            //public object Relationships { get; set; }

            //[JsonProperty("id")]
            //public long Id { get; set; }

            //[JsonProperty("links")]
            //public DatumLinks Links { get; set; }
        }

        public class Attributes
        {
            [JsonProperty("international-airtime-charges")]
            public long InternationalAirtimeCharges { get; set; }

            [JsonProperty("unbilled-single-fees")] public long UnbilledSingleFees { get; set; }

            [JsonProperty("call-forwarding-while-roaming")]
            public long CallForwardingWhileRoaming { get; set; }

            [JsonProperty("local-airtime-charges")]
            public long LocalAirtimeCharges { get; set; }

            [JsonProperty("monthly-subscription-fee")]
            public long MonthlySubscriptionFee { get; set; }

            [JsonProperty("monthly-line-rent")] public long MonthlyLineRent { get; set; }

            [JsonProperty("unbilled-total")] public long UnbilledTotal { get; set; }

            [JsonProperty("other-charges")] public long OtherCharges { get; set; }

            [JsonProperty("unbilled-fixed-fees")] public long UnbilledFixedFees { get; set; }

            [JsonProperty("total-current-charges")]
            public long TotalCurrentCharges { get; set; }

            [JsonProperty("unbilled-calls")] public long UnbilledCalls { get; set; }

            [JsonProperty("discounts")] public long Discounts { get; set; }

            [JsonProperty("previous-balance")] public long PreviousBalance { get; set; }

            [JsonProperty("adjustments")] public long Adjustments { get; set; }

            [JsonProperty("over-payment")] public long OverPayment { get; set; }

            [JsonProperty("value-added-services")] public long ValueAddedServices { get; set; }

            [JsonProperty("billing-account-type")] public string BillingAccountType { get; set; }

            //[JsonProperty("outside-plan")]
            //public object[] OutsidePlan { get; set; }

            [JsonProperty("product-usage")] public CombinedProductUsage[] ProductUsage { get; set; }

            [JsonProperty("credit-limit")] public double CreditLimit { get; set; }
        }

        public class CombinedProductUsage
        {
            [JsonProperty("name")] public string Name { get; set; }

            [JsonProperty("usages")] public CombinedUsage[] Usages { get; set; }

            [JsonProperty("activated-at")] public DateTimeOffset? ActivatedAt { get; set; }

            [JsonProperty("code")] public string Code { get; set; }

            [JsonProperty("deactivated-at")] public DateTimeOffset? DeactivatedAt { get; set; }

            [JsonProperty("commercial-name")] public Name CommercialName { get; set; }
        }

        public class CombinedUsage
        {
            [JsonProperty("used")] public long? Used { get; set; }

            [JsonProperty("name")] public Name Name { get; set; }

            [JsonProperty("dedicated-account-id")] public long? DedicatedAccountId { get; set; }

            [JsonProperty("left")] public double? Left { get; set; }

            [JsonProperty("traffic-location-category")]
            public object TrafficLocationCategory { get; set; }

            [JsonProperty("total")] public double? Total { get; set; }

            [JsonProperty("gsm-service-type")] public string GsmServiceType { get; set; }

            [JsonProperty("unlimited-usage")] public bool UnlimitedUsage { get; set; }
        }
    }
}