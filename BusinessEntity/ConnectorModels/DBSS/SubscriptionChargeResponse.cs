﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class SubscriptionChargeResponse : ErrorResponse
    {
        public SubscriptionChargeResponse()
        {
            Data = new List<SubscriptionChargeData>();
        }

        [JsonProperty("data")] public List<SubscriptionChargeData> Data { get; set; }

        //[JsonProperty("included")]
        //public List<SubscriptionChargeIncluded> Included { get; set; }
    }

    public class SubscriptionChargeData
    {
        [JsonProperty("attributes")] public SubscriptionChargeDataAttributes Attributes { get; set; }

        //[JsonProperty("relationships")]
        //public SubscriptionChargeDataRelationships Relationships { get; set; }

        //[JsonProperty("links")]
        //public SubscriptionChargeDataLinks Links { get; set; }

        //[JsonProperty("id")]
        //public long Id { get; set; }

        [JsonProperty("type")] public string Type { get; set; }
    }

    public class SubscriptionChargeDataAttributes
    {
        [JsonProperty("activated-at")] public DateTimeOffset ActivatedAt { get; set; }

        [JsonProperty("product-commercial-name")]
        public Name ProductCommercialName { get; set; }

        //[JsonProperty("deactivated-at")]
        //public DateTimeOffset? DeactivatedAt { get; set; }

        [JsonProperty("product-code")] public string ProductCode { get; set; }

        //[JsonProperty("product-name")]
        //public string ProductName { get; set; }

        [JsonProperty("fee")] public double Fee { get; set; }

        [JsonProperty("active")] public bool Active { get; set; }
    }


    //public class SubscriptionChargeDataLinks
    //{
    //    [JsonProperty("self")]
    //    public string Self { get; set; }
    //}

    //public class SubscriptionChargeDataRelationships
    //{
    //    [JsonProperty("product")]
    //    public SubscriptionChargeProduct Product { get; set; }
    //}

    //public class SubscriptionChargeProduct
    //{
    //[JsonProperty("data")]
    //public Dat Data { get; set; }

    //[JsonProperty("links")]
    //public ProductLinks Links { get; set; }
    //}

    public class Dat
    {
        [JsonProperty("type")] public string Type { get; set; }

        [JsonProperty("id")] public string Id { get; set; }
    }

    //public class ProductLinks
    //{
    //    [JsonProperty("related")]
    //    public string Related { get; set; }
    //}

    //public class SubscriptionChargeIncluded
    //{
    //    [JsonProperty("attributes")]
    //    public SubscriptionChargeIncludedAttributes Attributes { get; set; }

    //    [JsonProperty("relationships")]
    //    public SubscriptionChargeIncludedRelationships Relationships { get; set; }

    //    //[JsonProperty("links")]
    //    //public SubscriptionChargeDataLinks Links { get; set; }

    //    [JsonProperty("id")]
    //    public long Id { get; set; }

    //    [JsonProperty("type")]
    //    public string Type { get; set; }
    //}

    //public class SubscriptionChargeIncludedAttributes
    //{
    //    [JsonProperty("periodic-amount")]
    //    public long? PeriodicAmount { get; set; }

    //    [JsonProperty("name")]
    //    public SubscriptionChargeProductCommercialName Name { get; set; }

    //    [JsonProperty("is-configurable")]
    //    public bool? IsConfigurable { get; set; }

    //    [JsonProperty("long-info-text")]
    //    public SubscriptionChargeProductCommercialName LongInfoText { get; set; }

    //    [JsonProperty("periodic-unit")]
    //    public string PeriodicUnit { get; set; }

    //    [JsonProperty("short-description")]
    //    public SubscriptionChargeProductCommercialName ShortDescription { get; set; }

    //    [JsonProperty("code")]
    //    public string Code { get; set; }

    //    [JsonProperty("allow-re-activation")]
    //    public bool? AllowReActivation { get; set; }

    //    [JsonProperty("long-description")]
    //    public LongDescription LongDescription { get; set; }

    //    [JsonProperty("info-text")]
    //    public SubscriptionChargeProductCommercialName InfoText { get; set; }

    //    [JsonProperty("charge-type")]
    //    public string ChargeType { get; set; }

    //    [JsonProperty("medium-description")]
    //    public SubscriptionChargeProductCommercialName MediumDescription { get; set; }

    //    [JsonProperty("display-order")]
    //    public long? DisplayOrder { get; set; }

    //    [JsonProperty("fee")]
    //    public long? Fee { get; set; }

    //    [JsonProperty("fee-type")]
    //    public string FeeType { get; set; }

    //    [JsonProperty("tax-inclusive")]
    //    public bool? TaxInclusive { get; set; }
    //}

    //public class LongDescription
    //{
    //}

    //public class SubscriptionChargeIncludedRelationships
    //{
    //    [JsonProperty("services")]
    //    public SubscriptionChargeProductRelationships Services { get; set; }

    //    [JsonProperty("traffic-bundles")]
    //    public SubscriptionChargeProductRelationships TrafficBundles { get; set; }

    //    [JsonProperty("option-group")]
    //    public SubscriptionChargeProduct OptionGroup { get; set; }

    //    [JsonProperty("product-family")]
    //    public SubscriptionChargeProduct ProductFamily { get; set; }

    //    [JsonProperty("fees")]
    //    public SubscriptionChargeFees Fees { get; set; }

    //    [JsonProperty("product-relationships")]
    //    public SubscriptionChargeProductRelationships ProductRelationships { get; set; }

    //    [JsonProperty("billing-item")]
    //    public SubscriptionChargeProduct BillingItem { get; set; }
    //}

    //public class SubscriptionChargeFees
    //{
    //    [JsonProperty("data")]
    //    public List<Dat> Data { get; set; }

    //    [JsonProperty("links")]
    //    public ProductLinks Links { get; set; }
    //}

    //public class SubscriptionChargeProductRelationships
    //{
    //    [JsonProperty("links")]
    //    public ProductLinks Links { get; set; }
    //}
}