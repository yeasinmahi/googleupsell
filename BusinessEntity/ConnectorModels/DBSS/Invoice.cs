﻿using System;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class Invoice : ErrorResponse
    {
        [JsonProperty("data")] public Datum[] Data { get; set; }


        public class Datum
        {
            [JsonProperty("id")] public string Id { get; set; }

            [JsonProperty("type")] public string Type { get; set; }

            [JsonProperty("attributes")] public Attributes Attributes { get; set; }

            [JsonProperty("relationships")] public Relationships Relationships { get; set; }
        }

        public class Attributes
        {
            [JsonProperty("vat")] public double Vat { get; set; }

            [JsonProperty("invoice-external-id")] public string InvoiceExternalId { get; set; }

            [JsonProperty("claim-state")] public string ClaimState { get; set; }

            [JsonProperty("published")] public bool Published { get; set; }

            [JsonProperty("billing-period-to")] public DateTimeOffset? BillingPeriodTo { get; set; }

            [JsonProperty("total-without-vat")] public double TotalWithoutVat { get; set; }

            [JsonProperty("total")] public double Total { get; set; }

            [JsonProperty("invoice-id")] public string InvoiceId { get; set; }

            [JsonProperty("delivery-type")] public string DeliveryType { get; set; }

            [JsonProperty("due-date")] public DateTimeOffset? DueDate { get; set; }

            [JsonProperty("billing-period-from")] public DateTimeOffset? BillingPeriodFrom { get; set; }

            [JsonProperty("payment-status")] public string PaymentStatus { get; set; }

            [JsonProperty("paid-amount")] public double PaidAmount { get; set; }

            [JsonProperty("customer-can-view-details")]
            public bool CustomerCanViewDetails { get; set; }

            [JsonProperty("invoice-date")] public DateTimeOffset? InvoiceDate { get; set; }
        }


        public class Relationships
        {
            [JsonProperty("billing-account-id")] public string BillingAccountId { get; set; }

            [JsonProperty("invoice-items")] public string InvoiceItems { get; set; }

            [JsonProperty("payments")] public string Payments { get; set; }
        }
    }
}