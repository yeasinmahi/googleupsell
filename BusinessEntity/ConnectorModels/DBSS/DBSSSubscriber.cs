﻿using System;
using System.Collections.Generic;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class DBSSSubscriber
    {
        public DBSSSubscriber()
        {
            Barrings = new List<string>();
        }

        public string SubscriptionID { get; set; }

        //Customer Information
        public string CustomerID { get; set; }
        public string Category { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public bool IsCompany { get; set; }
        public string InvoiceDeliveryType { get; set; }
        public bool IsFleetManager { get; set; }
        public string PrepaidCurrentBalance { get; set; }

        ////SIM Information

        //public string PINCode1 { get; set; }
        //public string PINCode2 { get; set; }
        //public string PUK1 { get; set; }
        //public string PUK2 { get; set; }
        //public string ICC { get; set; }

        //Subscription Type
        public string PrepaidPostpaidType { get; set; }
        public string CurrentPackage { get; set; }
        public double CreditLimit { get; set; }
        public DateTime ConnectionDate { get; set; }
        public DateTime ConnectionEffectiveDate { get; set; }
        public string ConnectionStatus { get; set; }
        public string ConnectionStatusText { get; set; }
        public string PackageID { get; set; }
        public string PackageName { get; set; }
        public string PackageDescription { get; set; }
        public string EQuipIDorServiceID { get; set; }
        public string ContractNo { get; set; }
        public string MSISDN { get; set; }
        public int BillCycle { get; set; }
        public bool IsCallnControll { get; set; }
        public bool IsRoam { get; set; }

        public string VirtualContactNo { get; set; }
        // public string ConnectionTypeCode { get; set; }

        //public List<AvailableProduct> ConnectedProducts { get; set; }
        public List<string> Barrings { get; set; }
    }
}