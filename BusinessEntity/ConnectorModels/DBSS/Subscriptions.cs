﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class Subscription : ErrorResponse
    {
        public Subscription()
        {
            Datas = new List<Datum>();
            Includeds = new List<Included>();
        }

        [JsonProperty("data")] public List<Datum> Datas { get; set; }

        [JsonProperty("included")] public List<Included> Includeds { get; set; }


        public class Datum
        {
            [JsonProperty("attributes")] public DatumAttributes Attributes { get; set; }

            //[JsonProperty("relationships")]
            //public DatumRelationships Relationships { get; set; }

            //[JsonProperty("links")]
            //public DatumLinks Links { get; set; }

            [JsonProperty("id")]
            //[JsonConverter(typeof(ParseStringConverter))]
            public string Id { get; set; }

            //[JsonProperty("type")]
            //public string Type { get; set; }
        }

        //public class Barrings
        //{
        //    [JsonProperty("data")]
        //    public Dat[] Data { get; set; }

        //    [JsonProperty("links")]
        //    public PurpleLinks Links { get; set; }
        //}

        public class DatumAttributes
        {
            //[JsonProperty("monthly-costs")]
            //public long MonthlyCosts { get; set; }

            //[JsonProperty("contract-status")]
            //public string ContractStatus { get; set; }

            //[JsonProperty("first-call-date")]
            //public object FirstCallDate { get; set; }

            //[JsonProperty("termination-time")]
            //public object TerminationTime { get; set; }

            [JsonProperty("contract-id")] public string ContractId { get; set; }

            [JsonProperty("msisdn")] public string Msisdn { get; set; }

            [JsonProperty("activation-time")] public DateTimeOffset ActivationTime { get; set; }

            [JsonProperty("status")] public string Status { get; set; }

            //[JsonProperty("latest-contract-termination-time")]
            //public DateTimeOffset LatestContractTerminationTime { get; set; }

            //[JsonProperty("directory-listing")]
            //public string DirectoryListing { get; set; }

            [JsonProperty("payment-type")] public string PaymentType { get; set; }

            //[JsonProperty("original-contract-confirmation-code")]
            //public string OriginalContractConfirmationCode { get; set; }

            //[JsonProperty("credit-limit")]
            //public long CreditLimit { get; set; }


            [JsonProperty("puk-1")] public string Puk1 { get; set; }

            //[JsonProperty("is-multi-surf")]
            //public bool IsMultiSurf { get; set; }

            [JsonProperty("pin-1")] public string Pin1 { get; set; }

            //[JsonProperty("icc")]
            //public string Icc { get; set; }

            [JsonProperty("puk-2")] public string Puk2 { get; set; }

            [JsonProperty("pin-2")] public string Pin2 { get; set; }

            //[JsonProperty("sim-type")]
            //public string SimType { get; set; }

            //[JsonProperty("status")]
            //public string Status { get; set; }
        }

        //public class DatumLinks
        //{
        //    [JsonProperty("self")]
        //    public string Self { get; set; }
        //}

        public class DatumRelationships
        {
            //[JsonProperty("barrings")]
            //public Barrings Barrings { get; set; }

            //[JsonProperty("sim-cards")]
            //public SimCards SimCards { get; set; }

            //[JsonProperty("services")]
            //public TartuGecko Services { get; set; }

            //[JsonProperty("subscription-discounts")]
            //public TartuGecko SubscriptionDiscounts { get; set; }

            //[JsonProperty("network-services")]
            //public TartuGecko NetworkServices { get; set; }

            [JsonProperty("owner-customer")] public BillingRatePlan OwnerCustomer { get; set; }

            //[JsonProperty("products")]
            //public TartuGecko Products { get; set; }

            //[JsonProperty("payer-customer")]
            //public BillingRatePlan PayerCustomer { get; set; }

            //[JsonProperty("available-subscription-types")]
            //public TartuGecko AvailableSubscriptionTypes { get; set; }

            //[JsonProperty("document-validations")]
            //public TartuGecko DocumentValidations { get; set; }

            //[JsonProperty("product-usages")]
            //public TartuGecko ProductUsages { get; set; }

            //[JsonProperty("porting-requests")]
            //public TartuGecko PortingRequests { get; set; }

            //[JsonProperty("billing-rate-plan")]
            //public BillingRatePlan BillingRatePlan { get; set; }

            //[JsonProperty("combined-usage-report")]
            //public BillingRatePlan CombinedUsageReport { get; set; }

            //[JsonProperty("user-customer")]
            //public BillingRatePlan UserCustomer { get; set; }

            //[JsonProperty("gsm-service-usages")]
            //public TartuGecko GsmServiceUsages { get; set; }

            //[JsonProperty("balances")]
            //public TartuGecko Balances { get; set; }

            //[JsonProperty("billing-usages")]
            //public TartuGecko BillingUsages { get; set; }

            //[JsonProperty("subscription-type")]
            //public BillingRatePlan SubscriptionType { get; set; }

            //[JsonProperty("available-products")]
            //public TartuGecko AvailableProducts { get; set; }

            //[JsonProperty("catalog-sim-cards")]
            //public TartuGecko CatalogSimCards { get; set; }

            //[JsonProperty("connected-products")]
            //public TartuGecko ConnectedProducts { get; set; }

            //[JsonProperty("connection-type")]
            //public BillingRatePlan ConnectionType { get; set; }

            //[JsonProperty("available-child-products")]
            //public TartuGecko AvailableChildProducts { get; set; }

            //[JsonProperty("sim-card-orders")]
            //public TartuGecko SimCardOrders { get; set; }
        }

        //public class TartuGecko
        //{
        //    [JsonProperty("links")]
        //    public PurpleLinks Links { get; set; }
        //}

        public class PurpleLinks
        {
            [JsonProperty("related")] public string Related { get; set; }
        }

        public class BillingRatePlan
        {
            [JsonProperty("data")] public Dat Data { get; set; }

            [JsonProperty("links")] public PurpleLinks Links { get; set; }
        }

        //public class Dat
        //{
        //    [JsonProperty("type")]
        //    public string Type { get; set; }

        //    [JsonProperty("id")]
        //    public string Id { get; set; }
        //}

        //public class SimCards
        //{
        //    [JsonProperty("data")]
        //    public Dat[] Data { get; set; }

        //    [JsonProperty("links")]
        //    public PurpleLinks Links { get; set; }
        //}

        public class Included
        {
            [JsonProperty("attributes")] public IncludedAttributes Attributes { get; set; }

            //[JsonProperty("relationships")]
            //public IncludedRelationships Relationships { get; set; }

            //[JsonProperty("links")]
            //public DatumLinks Links { get; set; }

            [JsonProperty("id")] public string Id { get; set; }

            [JsonProperty("type")] public string Type { get; set; }
        }

        public class IncludedAttributes
        {
            //[JsonProperty("id-expiry")]
            //public object IdExpiry { get; set; }

            //[JsonProperty("charge-type")]
            //public string ChargeType { get; set; }

            //[JsonProperty("periodic-unit")]
            //public string PeriodicUnit { get; set; }

            //[JsonProperty("alternate-contact-phone", NullValueHandling = NullValueHandling.Ignore)]
            //public string AlternateContactPhone { get; set; }

            [JsonProperty("email", NullValueHandling = NullValueHandling.Ignore)]
            public string Email { get; set; }

            //[JsonProperty("bank-account-number")]
            //public object BankAccountNumber { get; set; }

            //[JsonProperty("account-type")]
            //public object AccountType { get; set; }

            //[JsonProperty("date-of-birth", NullValueHandling = NullValueHandling.Ignore)]
            //public DateTimeOffset? DateOfBirth { get; set; }

            //[JsonProperty("ban")]
            //public object Ban { get; set; }

            //[JsonProperty("id-document-type", NullValueHandling = NullValueHandling.Ignore)]
            //public string IdDocumentType { get; set; }

            [JsonProperty("is-company", NullValueHandling = NullValueHandling.Ignore)]
            public bool IsCompany { get; set; }


            //[JsonProperty("online-id")]
            //public object OnlineId { get; set; }

            //[JsonProperty("frame-agreement-ended-at")]
            //public object FrameAgreementEndedAt { get; set; }

            //[JsonProperty("payment-method", NullValueHandling = NullValueHandling.Ignore)]
            //public string PaymentMethod { get; set; }

            //[JsonProperty("customer-credit-limit", NullValueHandling = NullValueHandling.Ignore)]
            //public long CustomerCreditLimit { get; set; }

            //[JsonProperty("agreement-start-date")]
            //public object AgreementStartDate { get; set; }

            //[JsonProperty("language", NullValueHandling = NullValueHandling.Ignore)]
            //public string Language { get; set; }

            //[JsonProperty("is-loyalty-manager", NullValueHandling = NullValueHandling.Ignore)]
            //public bool? IsLoyaltyManager { get; set; }

            //[JsonProperty("id-document-number", NullValueHandling = NullValueHandling.Ignore)]
            //public string IdDocumentNumber { get; set; }

            [JsonProperty("invoice-delivery-type", NullValueHandling = NullValueHandling.Ignore)]
            public string InvoiceDeliveryType { get; set; }

            //[JsonProperty("frame-agreement-started-at")]
            //public object FrameAgreementStartedAt { get; set; }

            //[JsonProperty("nationality", NullValueHandling = NullValueHandling.Ignore)]
            //public string Nationality { get; set; }

            [JsonProperty("trade-register-id")] public string TradeRegisterId { get; set; }

            //[JsonProperty("business-uid")]
            //public object BusinessUid { get; set; }

            //[JsonProperty("legal-address", NullValueHandling = NullValueHandling.Ignore)]
            //public LegalAddress LegalAddress { get; set; }

            //[JsonProperty("marketing-own", NullValueHandling = NullValueHandling.Ignore)]
            //public bool MarketingOwn { get; set; }

            [JsonProperty("category", NullValueHandling = NullValueHandling.Ignore)]
            public string Category { get; set; }

            [JsonProperty("first-name", NullValueHandling = NullValueHandling.Ignore)]
            public string FirstName { get; set; }

            //[JsonProperty("occupation")]
            //public object Occupation { get; set; }

            //[JsonProperty("segmentation-category", NullValueHandling = NullValueHandling.Ignore)]
            //public string SegmentationCategory { get; set; }

            [JsonProperty("is-fleet-manager", NullValueHandling = NullValueHandling.Ignore)]
            public bool IsFleetManager { get; set; }

            //[JsonProperty("marketing-third-party", NullValueHandling = NullValueHandling.Ignore)]
            //public bool? MarketingThirdParty { get; set; }

            [JsonProperty("last-name", NullValueHandling = NullValueHandling.Ignore)]
            public string LastName { get; set; }

            //[JsonProperty("contact-phone", NullValueHandling = NullValueHandling.Ignore)]
            //public string ContactPhone { get; set; }

            [JsonProperty("gender", NullValueHandling = NullValueHandling.Ignore)]
            public string Gender { get; set; }

            [JsonProperty("puk-1", NullValueHandling = NullValueHandling.Ignore)]
            public string Puk1 { get; set; }

            //[JsonProperty("is-multi-surf", NullValueHandling = NullValueHandling.Ignore)]
            //public bool IsMultiSurf { get; set; }

            [JsonProperty("pin-1", NullValueHandling = NullValueHandling.Ignore)]
            //[JsonConverter(typeof(ParseStringConverter))]
            public string Pin1 { get; set; }

            [JsonProperty("icc", NullValueHandling = NullValueHandling.Ignore)]
            public string Icc { get; set; }

            [JsonProperty("puk-2", NullValueHandling = NullValueHandling.Ignore)]
            public string Puk2 { get; set; }

            [JsonProperty("pin-2", NullValueHandling = NullValueHandling.Ignore)]
            public string Pin2 { get; set; }

            [JsonProperty("sim-type", NullValueHandling = NullValueHandling.Ignore)]
            public string SimType { get; set; }

            [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
            public string Status { get; set; }

            [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
            public Name Name { get; set; }

            //[JsonProperty("long-info-text", NullValueHandling = NullValueHandling.Ignore)]
            //public Name LongInfoText { get; set; }

            //[JsonProperty("info-text", NullValueHandling = NullValueHandling.Ignore)]
            //public Name InfoText { get; set; }

            //[JsonProperty("short-description", NullValueHandling = NullValueHandling.Ignore)]
            //public Name ShortDescription { get; set; }

            //[JsonProperty("medium-description", NullValueHandling = NullValueHandling.Ignore)]
            //public Name MediumDescription { get; set; }

            //[JsonProperty("long-description", NullValueHandling = NullValueHandling.Ignore)]
            //public Name LongDescription { get; set; }

            //[JsonProperty("advice-on-charge", NullValueHandling = NullValueHandling.Ignore)]
            //public Name AdviceOnCharge { get; set; }

            //[JsonProperty("brand", NullValueHandling = NullValueHandling.Ignore)]
            //public string Brand { get; set; }

            [JsonProperty("code", NullValueHandling = NullValueHandling.Ignore)]
            public string Code { get; set; }

            //[JsonProperty("business-model-type", NullValueHandling = NullValueHandling.Ignore)]
            //public string BusinessModelType { get; set; }

            [JsonProperty("payment-type", NullValueHandling = NullValueHandling.Ignore)]
            public string PaymentType { get; set; }

            //[JsonProperty("change-price", NullValueHandling = NullValueHandling.Ignore)]
            //public long ChangePrice { get; set; }

            //[JsonProperty("display-order", NullValueHandling = NullValueHandling.Ignore)]
            //public int DisplayOrder { get; set; }

            //[JsonProperty("change-price-currency")]
            //public object ChangePriceCurrency { get; set; }

            [JsonProperty("credit-limit", NullValueHandling = NullValueHandling.Ignore)]
            public double CreditLimit { get; set; }

            //[JsonProperty("service-parameters")]
            //public ServiceParameters ServiceParameters { get; set; }

            //[JsonProperty("allow-re-activation", NullValueHandling = NullValueHandling.Ignore)]
            //public bool AllowReActivation { get; set; }

            //[JsonProperty("barring-level-ids", NullValueHandling = NullValueHandling.Ignore)]
            //public string[] BarringLevelIds { get; set; }

            //[JsonProperty("upcoming-barring-level-ids", NullValueHandling = NullValueHandling.Ignore)]
            //public object[] UpcomingBarringLevelIds { get; set; }


            //[JsonProperty("is-fleet-manager", NullValueHandling = NullValueHandling.Ignore)]
            //public bool IsFleetManager { get; set; }
        }

        //public class ServiceParameters
        //{
        //    [JsonProperty("faf_msisdn1")]
        //    public string FafMsisdn1 { get; set; }

        //    [JsonProperty("faf_msisdn2")]
        //    public string FafMsisdn2 { get; set; }

        //    [JsonProperty("faf_msisdn3")]
        //    public string FafMsisdn3 { get; set; }

        //    [JsonProperty("faf_msisdn4")]
        //    public string FafMsisdn4 { get; set; }

        //    [JsonProperty("faf_msisdn5")]
        //    public string FafMsisdn5 { get; set; }

        //    [JsonProperty("faf_msisdn6")]
        //    public string FafMsisdn6 { get; set; }

        //    [JsonProperty("faf_msisdn7")]
        //    public string FafMsisdn7 { get; set; }

        //    [JsonProperty("faf_msisdn8")]
        //    public string FafMsisdn8 { get; set; }

        //    [JsonProperty("faf_msisdn9")]
        //    public string FafMsisdn9 { get; set; }

        //    [JsonProperty("faf_msisdn10")]
        //    public string FafMsisdn10 { get; set; }
        //}

        //public class LegalAddress
        //{
        //    [JsonProperty("type")]
        //    public string Type { get; set; }

        //    [JsonProperty("attributes")]
        //    public LegalAddressAttributes Attributes { get; set; }
        //}

        //public class LegalAddressAttributes
        //{
        //    [JsonProperty("area")]
        //    //[JsonConverter(typeof(ParseStringConverter))]
        //    public long Area { get; set; }

        //    [JsonProperty("flat-number")]
        //    //[JsonConverter(typeof(ParseStringConverter))]
        //    public long FlatNumber { get; set; }

        //    [JsonProperty("thana")]
        //    public string Thana { get; set; }

        //    [JsonProperty("co")]
        //    public string Co { get; set; }

        //    [JsonProperty("country")]
        //    public string Country { get; set; }

        //    [JsonProperty("division")]
        //    public string Division { get; set; }

        //    [JsonProperty("country-name")]
        //    public CountryName CountryName { get; set; }

        //    [JsonProperty("road")]
        //    //[JsonConverter(typeof(ParseStringConverter))]
        //    public long Road { get; set; }

        //    [JsonProperty("house-number")]
        //    //[JsonConverter(typeof(ParseStringConverter))]
        //    public long HouseNumber { get; set; }

        //    [JsonProperty("district")]
        //    public string District { get; set; }

        //    [JsonProperty("post-code")]
        //    //[JsonConverter(typeof(ParseStringConverter))]
        //    public long PostCode { get; set; }
        //}

        //public class CountryName
        //{
        //    [JsonProperty("fr")]
        //    public string Fr { get; set; }

        //    [JsonProperty("en")]
        //    public string En { get; set; }

        //    [JsonProperty("de")]
        //    public string De { get; set; }

        //    [JsonProperty("it")]
        //    public string It { get; set; }
        //}


        //public class IncludedRelationships
        //{
        //[JsonProperty("barring", NullValueHandling = NullValueHandling.Ignore)]
        //public BillingRatePlan Barring { get; set; }

        //[JsonProperty("barring-levels", NullValueHandling = NullValueHandling.Ignore)]
        //public TartuGecko BarringLevels { get; set; }

        //[JsonProperty("company-people", NullValueHandling = NullValueHandling.Ignore)]
        //public TartuGecko CompanyPeople { get; set; }

        //[JsonProperty("customer-edit-permission", NullValueHandling = NullValueHandling.Ignore)]
        //public BillingRatePlan CustomerEditPermission { get; set; }

        //[JsonProperty("inventory", NullValueHandling = NullValueHandling.Ignore)]
        //public BillingRatePlan Inventory { get; set; }

        //[JsonProperty("orders", NullValueHandling = NullValueHandling.Ignore)]
        //public TartuGecko Orders { get; set; }

        //[JsonProperty("subscription", NullValueHandling = NullValueHandling.Ignore)]
        //public BillingRatePlan Subscription { get; set; }

        //[JsonProperty("included-products", NullValueHandling = NullValueHandling.Ignore)]
        //public TartuGecko IncludedProducts { get; set; }

        //[JsonProperty("billing-rate-plans", NullValueHandling = NullValueHandling.Ignore)]
        //public TartuGecko BillingRatePlans { get; set; }

        //[JsonProperty("mandatory-products", NullValueHandling = NullValueHandling.Ignore)]
        //public TartuGecko MandatoryProducts { get; set; }

        //[JsonProperty("option-group-products", NullValueHandling = NullValueHandling.Ignore)]
        //public TartuGecko OptionGroupProducts { get; set; }

        //[JsonProperty("option-groups", NullValueHandling = NullValueHandling.Ignore)]
        //public TartuGecko OptionGroups { get; set; }

        //[JsonProperty("fees", NullValueHandling = NullValueHandling.Ignore)]
        //public TartuGecko Fees { get; set; }

        //[JsonProperty("optional-products", NullValueHandling = NullValueHandling.Ignore)]
        //public TartuGecko OptionalProducts { get; set; }

        //[JsonProperty("available-connection-types", NullValueHandling = NullValueHandling.Ignore)]
        //public TartuGecko AvailableConnectionTypes { get; set; }

        //[JsonProperty("service")]
        //public BillingRatePlan Service { get; set; }

        //[JsonProperty("product-family")]
        //public BillingRatePlan ProductFamily { get; set; }

        //[JsonProperty("subscription")]
        //public BillingRatePlan SubscriptionBillingRatePlan { get; set; }
        //}
    }
}