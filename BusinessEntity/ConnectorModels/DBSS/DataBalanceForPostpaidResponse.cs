﻿using System;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class DataBalanceForPostpaidResponse : ErrorResponse
    {
        [JsonProperty("data")] public Data DataResponse { get; set; }


        public class Data
        {
            [JsonProperty("type")] public string Type { get; set; }

            [JsonProperty("attributes")] public Attributes Attributes { get; set; }

            [JsonProperty("id")]
            //[JsonConverter(typeof(ParseStringConverter))]
            public long Id { get; set; }

            //[JsonProperty("links")]
            //public Links Links { get; set; }
        }

        public class Attributes
        {
            //[JsonProperty("outside-plan")]
            //public object[] OutsidePlan { get; set; }

            [JsonProperty("product-usage")] public ProductUsage[] ProductUsage { get; set; }

            //[JsonProperty("unbilled-usage")]
            //public long UnbilledUsage { get; set; }
        }

        public class ProductUsage
        {
            [JsonProperty("name")] public string Name { get; set; }

            [JsonProperty("usages")] public Usage[] Usages { get; set; }

            //[JsonProperty("activated-at")]
            //public DateTimeOffset ActivatedAt { get; set; }

            [JsonProperty("code")] public string Code { get; set; }

            [JsonProperty("deactivated-at")] public DateTimeOffset? DeactivatedAt { get; set; }

            [JsonProperty("commercial-name")] public Name CommercialName { get; set; }
        }


        public class Usage
        {
            [JsonProperty("used")] public long Used { get; set; }

            //[JsonProperty("name")]
            //public Name Name { get; set; }

            [JsonProperty("left")] public long? Left { get; set; }

            //[JsonProperty("traffic-location-category")]
            //public string TrafficLocationCategory { get; set; }

            //[JsonProperty("total")]
            //public long? Total { get; set; }

            [JsonProperty("gsm-service-type")] public string GsmServiceType { get; set; }

            //[JsonProperty("unlimited-usage")]
            //public bool UnlimitedUsage { get; set; }
        }

        //public class Links
        //{
        //    [JsonProperty("self")]
        //    public string Self { get; set; }
        //}
    }
}