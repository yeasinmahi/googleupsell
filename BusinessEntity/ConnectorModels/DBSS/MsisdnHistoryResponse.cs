﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class MsisdnHistoryResponse : ErrorResponse
    {
        public MsisdnHistoryResponse()
        {
            Data = new List<MsisdnHistoryResponseData>();
        }

        [JsonProperty("data")] public List<MsisdnHistoryResponseData> Data { get; set; }


        public class MsisdnHistoryResponseData
        {
            [JsonProperty("type")] public string Type { get; set; }

            [JsonProperty("attributes")] public MsisdnHistoryResponseAttributes Attributes { get; set; }
        }

        public class MsisdnHistoryResponseAttributes
        {
            //[JsonProperty("cell-id")]
            //public object CellId { get; set; }
            public MsisdnHistoryResponseAttributes()
            {
                Balances = new List<Balance>();
            }

            [JsonProperty("duration")] public int? Duration { get; set; }

            //[JsonProperty("call-direction")]
            //public object CallDirection { get; set; }

            //[JsonProperty("record-id")]
            //public string RecordId { get; set; }

            //[JsonProperty("bnr")]
            //public string Bnr { get; set; }

            //[JsonProperty("call-case")]
            //public long? CallCase { get; set; }

            //[JsonProperty("basic-service-code")]
            //public long? BasicServiceCode { get; set; }

            //[JsonProperty("service-type")]
            //public object ServiceType { get; set; }

            //[JsonProperty("dc-event")]
            //public object DcEvent { get; set; }

            //[JsonProperty("call-destination-type")]
            //public object CallDestinationType { get; set; }

            [JsonProperty("calling-number")] public string CallingNumber { get; set; }

            //[JsonProperty("roaming-indicator")]
            //public long? RoamingIndicator { get; set; }

            //[JsonProperty("subscription-id")]
            //public long? SubscriptionId { get; set; }

            //[JsonProperty("created-at")]
            //public DateTimeOffset CreatedAt { get; set; }

            //[JsonProperty("invoice-sequence-id")]
            //public long? InvoiceSequenceId { get; set; }

            [JsonProperty("data-amount")] public long? DataAmount { get; set; }

            [JsonProperty("destination-name")] public Name DestinationName { get; set; }

            [JsonProperty("event-at")] public DateTimeOffset EventAt { get; set; }

            [JsonProperty("called-number")] public string CalledNumber { get; set; }

            [JsonProperty("event-type")] public string EventType { get; set; }

            [JsonProperty("balances")] public List<Balance> Balances { get; set; }

            //[JsonProperty("destination-country-code")]
            //public object DestinationCountryCode { get; set; }

            //[JsonProperty("origin-country-code")]
            //public object OriginCountryCode { get; set; }

            //[JsonProperty("call-destination")]
            //public string CallDestination { get; set; }

            //[JsonProperty("billable-amount")]
            //public string BillableAmount { get; set; }

            //[JsonProperty("mcc")]
            //public object Mcc { get; set; }

            //[JsonProperty("product-code")]
            //public object ProductCode { get; set; }

            //[JsonProperty("origin-name")]
            //public object OriginName { get; set; }

            //[JsonProperty("service-category")]
            //public object ServiceCategory { get; set; }

            [JsonProperty("country-name")] public string CountryName { get; set; }

            [JsonProperty("transaction-type")] public string TransactionType { get; set; }

            //[JsonProperty("direction-type")]
            //public string DirectionType { get; set; }

            //[JsonProperty("reference-price")]
            //public double? ReferencePrice { get; set; }

            //[JsonProperty("technical-call-case")]
            //public long? TechnicalCallCase { get; set; }

            //[JsonProperty("product-name")]
            //public object ProductName { get; set; }

            //[JsonProperty("lac")]
            //public object Lac { get; set; }

            //[JsonProperty("billing-item-id")]
            //public string BillingItemId { get; set; }

            //[JsonProperty("plmn")]
            //public string Plmn { get; set; }

            [JsonProperty("total-with-vat")] public double? TotalWithVat { get; set; }

            //[JsonProperty("customer-id")]
            //public long? CustomerId { get; set; }

            //[JsonProperty("subscription-type-id")]
            //public string SubscriptionTypeId { get; set; }
        }

        public class Balance
        {
            [JsonProperty("transaction-amount")] public double? TransactionAmount { get; set; }

            //[JsonProperty("balance-before")]
            //public double? BalanceBefore { get; set; }

            //[JsonProperty("product-id")]
            //public string ProductId { get; set; }

            //[JsonProperty("offer-id")]
            //public string OfferId { get; set; }

            [JsonProperty("balance-id")] public string BalanceId { get; set; }

            //[JsonProperty("unit-type")]
            //public long? UnitType { get; set; }

            //[JsonProperty("balance-after")]
            //public double? BalanceAfter { get; set; }
        }
    }
}