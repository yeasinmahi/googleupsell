﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using BusinessEntity.ConnectorModels.OpenCode;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class AccountBalance
    {
        public AccountBalance()
        {
            DadicatedAccountList = new List<DadicatedAccount>();
        }

        public string Currency { get; set; }
        public double MainAccountBalance { get; set; }
        public double PostpaidCurrentBill { get; set; }
        public double PostpaidTotalDue { get; set; }
        public double CreditLimit { get; set; }
        public double LimitUses { get; set; }
        public DateTime? MainAccountBalanceValidity { get; set; }


        public string SubscriptionId { get; set; }
        public string PaymentType { get; set; }
        public List<DadicatedAccount> DadicatedAccountList { get; set; }


        public AccountBalance MapFromUcip(BalanceInfo info)
        {
            var daBalance = new AccountBalance
            {
                MainAccountBalance = Convert.ToDouble(info.accountValue1) / 100,
                //ServiceClass = info.serviceClassCurrent,
                Currency = info.currency1,
                MainAccountBalanceValidity = DateTime.ParseExact(info.serviceRemovalDate
                    , "yyyyMMddThh:mm:ss+0000", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal),


                DadicatedAccountList = info.dedicatedAccountInformation.Select(dedicatedAccount => new DadicatedAccount
                {
                    DadicatedAccountId = dedicatedAccount.dedicatedAccountID,
                    ProductId = dedicatedAccount.dedicatedAccountID, //TODO need to check
                    ProductName = "", //Fill from Memory
                    Unit = dedicatedAccount.dedicatedAccountUnitType.ToString(),
                    DadicatedAccountValue = Convert.ToDouble(dedicatedAccount.dedicatedAccountActiveValue1),
                    InitialAmount = Convert.ToDouble(dedicatedAccount.dedicatedAccountActiveValue1),
                    RemainingAmount = Convert.ToDouble(dedicatedAccount.dedicatedAccountActiveValue1),
                    TotalAmount = Convert.ToDouble(dedicatedAccount.dedicatedAccountActiveValue1),
                    ExpieryDate = TimeZone.CurrentTimeZone.ToLocalTime(DateTime.ParseExact(dedicatedAccount.expiryDate
                        , "yyyyMMddThh:mm:ss+0000", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal))
                }).ToList()
            };
            //if (info.dedicatedAccountInformation != null)
            //{
            //    AccountInformation genericLoan = info.dedicatedAccountInformation.FirstOrDefault(a => a.dedicatedAccountID == 255);
            //    AccountInformation dataLoan = info.dedicatedAccountInformation.FirstOrDefault(a => a.dedicatedAccountID == 245);
            //}

            return daBalance;
        }

        public AccountBalance MapFromDbssBalanceForPripaid(PrepaidSubscriptionBalanceResponse dataBalance,
            bool loadSubscriptionInfo = true)
        {
            if (dataBalance?.Includeds == null)
                return new AccountBalance
                {
                    DadicatedAccountList = new List<DadicatedAccount>()
                };
            var mainAccount = dataBalance.Includeds.FirstOrDefault(x => x.Attributes.IsMainBalance);

            AccountBalance daBalance = null;
            if (mainAccount != null)
                daBalance = new AccountBalance
                {
                    MainAccountBalance = mainAccount.Attributes?.Amount ?? 0,
                    MainAccountBalanceValidity =
                        mainAccount.Attributes?.Lifecycle?.SupervisionExpiryDate?.DateTime,
                    DadicatedAccountList =
                        dataBalance.Includeds != null && dataBalance.Includeds.Any()
                            ? dataBalance.Includeds
                                .Where(s => s.Attributes != null && s.Attributes.DedicatedAccountId != null)
                                .Select(item => new DadicatedAccount
                                {
                                    DadicatedAccountId = item.Attributes.DedicatedAccountId != null
                                        ? (int)item.Attributes.DedicatedAccountId
                                        : 0,
                                    ProductName = item.Attributes.ProductName != null
                                        ? item.Attributes.ProductName.En ?? string.Empty
                                        : "Bonus" + (!string.IsNullOrWhiteSpace(item.Attributes.Unit)
                                            ? $" {(item.Attributes.Unit.ToLower() == "mb" ? "Data" : item.Attributes.Unit)}"
                                            : string.Empty),
                                    Unit = item.Attributes.Unit ?? string.Empty,
                                    //  GsmServiceType = item.Attributes.GsmServiceType != null ? item.Attributes.GsmServiceType : item.Attributes.Unit != null ? item.Attributes.Unit.ToLower() == "mb" ? "data" : item.Attributes.Unit.ToLower() == "min" ? "voice" : item.Attributes.Unit.ToLower() == "sms" ? "sms" : item.Attributes.Unit.ToLower() == "mms" ? "mms" : string.Empty : string.Empty,
                                    GsmServiceType = item.Attributes.Unit != null
                                        ? item.Attributes.Unit.ToLower() == "mb"
                                            ? "data"
                                            : item.Attributes.Unit.ToLower() == "min"
                                                ? "voice"
                                                : item.Attributes.Unit.ToLower() == "sms"
                                                    ? "sms"
                                                    : item.Attributes.Unit.ToLower() == "mms"
                                                        ? "mms"
                                                        : string.Empty
                                        : item.Attributes.GsmServiceType,

                                    ProductId = item.Attributes.ProductId,
                                    ProductCode = item.Attributes.ProductCode,
                                    RemainingAmount = item.Attributes.Amount,
                                    TotalAmount = item.Attributes.TotalAmount,
                                    InitialAmount = item.Attributes.TotalAmount,

                                    //ExpieryDate = item.Attributes.ExpiryAt?.DateTime.Date ?? DateTime.MinValue
                                    ExpieryDate = item.Attributes.ExpiryAt ?? DateTime.MinValue

                                }).ToList()
                            : new List<DadicatedAccount>()
                };
            foreach (var a in daBalance.DadicatedAccountList)
            {
                if(a.ExpieryDate.TimeOfDay == new TimeSpan(12, 0, 0))
                {
                    a.ExpieryDate = a.ExpieryDate.Add(new TimeSpan(5, 59, 59));
                }
                
            }
            if (loadSubscriptionInfo)
                if (dataBalance.Data != null && dataBalance.Data.Length > 0)
                {
                    //var datumAttributes = dataBalance.Data[0].Attributes;
                    if (dataBalance.Data[0].Attributes != null)
                        if (daBalance != null)
                            daBalance.PaymentType = dataBalance.Data[0].Attributes.PaymentType;
                    if (daBalance != null) daBalance.SubscriptionId = dataBalance.Data[0].Id;
                }

            return daBalance;
        }

        public AccountBalance MapFromDbssBalanceForPostpaid(CombinedUsageReport dataBalance)
        {
            var daBalance = new AccountBalance
            {
                DadicatedAccountList = new List<DadicatedAccount>()
            };

            if (dataBalance == null || dataBalance.ResponseDatas == null) return daBalance;


            var local = dataBalance.ResponseDatas.FirstOrDefault(s => s.Attributes.BillingAccountType == "local")
                        ?? new CombinedUsageReport.ResponseData
                        {
                            Attributes = new CombinedUsageReport.Attributes
                            { ProductUsage = new CombinedUsageReport.CombinedProductUsage[0] }
                        };


            double unbilled = local.Attributes.TotalCurrentCharges;
            //unbilled += unbilled * (AppSetting.VATnSD / 100);
            var previousDue = local.Attributes.PreviousBalance;
            daBalance.PostpaidCurrentBill = unbilled;
            daBalance.PostpaidTotalDue = previousDue;
            daBalance.CreditLimit = local.Attributes.CreditLimit;

            foreach (var usage in local.Attributes.ProductUsage)
            {
                if (usage.Usages.FirstOrDefault() == null) continue;
                foreach (var item in usage.Usages)
                {
                    var daAccount = new DadicatedAccount
                    {
                        DadicatedAccountId = 0,
                        ProductName = usage.Name,
                        Unit = item.GsmServiceType == "data" ? "MB" :
                            item.GsmServiceType == "taka" || item.GsmServiceType == "voice" ||
                            item.GsmServiceType == "money" ? "Min" :
                            item.GsmServiceType == "sms" ? "SMS" :
                            item.GsmServiceType == "mms" ? "MMS" : string.Empty,
                        GsmServiceType = item.GsmServiceType ?? string.Empty,
                        ProductId = 0,

                        RemainingAmount = item.Left,
                        TotalAmount = item.Total,
                        ExpieryDate = usage.DeactivatedAt?.DateTime.Date ?? DateTime.MinValue
                    };
                    daBalance.DadicatedAccountList.Add(daAccount);
                }
            }

            return daBalance;
        }


        public class DadicatedAccount
        {
            public int DadicatedAccountId { get; set; }
            public long? ProductId { get; set; }
            public double DadicatedAccountValue { get; set; }
            public double? InitialAmount { get; set; }
            public double? RemainingAmount { get; set; }
            public double? TotalAmount { get; set; }
            public string ProductName { get; set; }
            public string ProductCode { get; set; }
            public string GsmServiceType { get; set; }
            public string Unit { get; set; }
            public DateTime ExpieryDate { get; set; }
        }
    }
}