﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class PaymentResponse : ErrorResponse
    {
        public PaymentResponse()
        {
            Data = new List<PaymentData>();
        }

        [JsonProperty("data")] public List<PaymentData> Data { get; set; }
    }

    public class PaymentData
    {
        [JsonProperty("attributes")] public PaymentAttributes Attributes { get; set; }

        //[JsonProperty("relationships")]
        //public Relationships Relationships { get; set; }

        //[JsonProperty("links")]
        //public PaymentDataLinks Links { get; set; }

        //[JsonProperty("id")]
        //public string Id { get; set; }

        //[JsonProperty("type")]
        //public string Type { get; set; }
    }

    public class PaymentAttributes
    {
        //[JsonProperty("received-at")]
        //public DateTimeOffset ReceivedAt { get; set; }

        //[JsonProperty("invoice-external-id")]
        //public string InvoiceExternalId { get; set; }

        [JsonProperty("payment-amount")] public long PaymentAmount { get; set; }

        //[JsonProperty("payment-archive-id")]
        //public string PaymentArchiveId { get; set; }

        //[JsonProperty("invoice-id")]
        //public long? InvoiceId { get; set; }

        [JsonProperty("payment-type")] public string PaymentType { get; set; }

        //[JsonProperty("payment-id")]
        //public long PaymentId { get; set; }

        //[JsonProperty("invoice-date")]
        //public DateTimeOffset? InvoiceDate { get; set; }

        [JsonProperty("payment-date")] public DateTimeOffset PaymentDate { get; set; }
    }

    //public class PaymentDataLinks
    //{
    //    [JsonProperty("self")]
    //    public string Self { get; set; }
    //}

    //public class Relationships
    //{
    //    [JsonProperty("customer")]
    //    public PaymentCustomer Customer { get; set; }
    //}

    //public class PaymentCustomer
    //{
    //    [JsonProperty("data")]
    //    public Data Data { get; set; }

    //    [JsonProperty("links")]
    //    public CustomerLinks Links { get; set; }
    //}

    //public class Data
    //{
    //    [JsonProperty("type")]
    //    public string Type { get; set; }

    //    [JsonProperty("id")]
    //    public long Id { get; set; }
    //}

    //public class CustomerLinks
    //{
    //    [JsonProperty("related")]
    //    public string Related { get; set; }
    //}
}