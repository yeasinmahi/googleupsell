﻿using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class RevenueStatus : ErrorResponse
    {
        [JsonProperty("data")] public Data Datas { get; set; }

        public class Data
        {
            [JsonProperty("attributes")] public Attributes Attributes { get; set; }

            //[JsonProperty("relationships")]
            //public Relationships Relationships { get; set; }

            //[JsonProperty("links")]
            //public DataLinks Links { get; set; }

            //[JsonProperty("id")]
            //public string Id { get; set; }

            //[JsonProperty("type")]
            //public string Type { get; set; }
        }

        public class Attributes
        {
            [JsonProperty("is-in-collection-process")]
            public bool IsInCollectionProcess { get; set; }

            [JsonProperty("unpaid-amount")] public long UnpaidAmount { get; set; }

            [JsonProperty("farthest-active-collection-step")]
            public object FarthestActiveCollectionStep { get; set; }

            [JsonProperty("unbilled-amount-timestamp")]
            public object UnbilledAmountTimestamp { get; set; }

            [JsonProperty("has-active-payment-plan")]
            public bool HasActivePaymentPlan { get; set; }

            [JsonProperty("overdue-unpaid-amount")]
            public long OverdueUnpaidAmount { get; set; }

            //[JsonProperty("currently-active-collection-step")]
            //public object CurrentlyActiveCollectionStep { get; set; }

            [JsonProperty("unbilled-amount")] public long UnbilledAmount { get; set; }
        }

        //public class DataLinks
        //{
        //    [JsonProperty("self")]
        //    public string Self { get; set; }
        //}

        //public class Relationships
        //{
        //    [JsonProperty("customer")]
        //    public Customer Customer { get; set; }
        //}

        //public class Customer
        //{
        //    [JsonProperty("data")]
        //    public CustomerData Data { get; set; }

        //    [JsonProperty("links")]
        //    public CustomerLinks Links { get; set; }
        //}

        //public class CustomerData
        //{
        //    [JsonProperty("type")]
        //    public string Type { get; set; }

        //    [JsonProperty("id")]
        //    //[JsonConverter(typeof(ParseStringConverter))]
        //    public long Id { get; set; }
        //}

        //public class CustomerLinks
        //{
        //    [JsonProperty("related")]
        //    public string Related { get; set; }
        //}
    }
}