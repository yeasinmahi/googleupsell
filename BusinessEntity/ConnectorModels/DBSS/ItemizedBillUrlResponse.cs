﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class ItemizedBillUrlResponse : ErrorResponse
    {
        public ItemizedBillUrlResponse()
        {
            Data = new List<ItemizedBillUrlDatum>();
        }

        [JsonProperty("data")] public List<ItemizedBillUrlDatum> Data { get; set; }
    }

    public class ItemizedBillUrlDatum
    {
        [JsonProperty("attributes")] public ItemizedBillUrlAttributes Attributes { get; set; }

        //[JsonProperty("relationships")]
        //public ItemizedBillUrlRelationships Relationships { get; set; }

        //[JsonProperty("links")]
        //public ItemizedBillUrlDatumLinks Links { get; set; }

        [JsonProperty("id")] public string Id { get; set; }

        //[JsonProperty("type")]
        //public string Type { get; set; }
    }

    public class ItemizedBillUrlAttributes
    {
        //[JsonProperty("vat")]
        //public long Vat { get; set; }

        //[JsonProperty("invoice-external-id")]
        //public long InvoiceExternalId { get; set; }

        //[JsonProperty("claim-state")]
        //public string ClaimState { get; set; }

        //[JsonProperty("published")]
        //public bool Published { get; set; }

        //[JsonProperty("billing-period-to")]
        //public DateTimeOffset BillingPeriodTo { get; set; }

        //[JsonProperty("total-without-vat")]
        //public long TotalWithoutVat { get; set; }

        //[JsonProperty("total")]
        //public long Total { get; set; }

        //[JsonProperty("invoice-id")]
        //public string InvoiceId { get; set; }

        //[JsonProperty("delivery-type")]
        //public string DeliveryType { get; set; }

        [JsonProperty("due-date")] public DateTimeOffset DueDate { get; set; }

        //[JsonProperty("billing-period-from")]
        //public DateTimeOffset BillingPeriodFrom { get; set; }

        //[JsonProperty("payment-status")]
        //public string PaymentStatus { get; set; }

        //[JsonProperty("paid-amount")]
        //public long PaidAmount { get; set; }

        //[JsonProperty("customer-can-view-details")]
        //public bool CustomerCanViewDetails { get; set; }
        //[JsonProperty("invoice-date")]
        //public DateTimeOffset InvoiceDate { get; set; }

        [JsonProperty("pdf-url")] public string PdfUrl { get; set; }
    }

    //public partial class ItemizedBillUrlDatumLinks
    //{
    //    [JsonProperty("self")]
    //    public string Self { get; set; }
    //}

    //public class ItemizedBillUrlRelationships
    //{
    //    [JsonProperty("payments")]
    //    public ItemizedBillUrlInvoiceItems Payments { get; set; }

    //    [JsonProperty("owner-customer")]
    //    public ItemizedBillUrlInvoicePdf OwnerCustomer { get; set; }

    //    [JsonProperty("payer-customer")]
    //    public ItemizedBillUrlInvoicePdf PayerCustomer { get; set; }

    //    [JsonProperty("invoice-items")]
    //    public ItemizedBillUrlInvoiceItems InvoiceItems { get; set; }

    //    [JsonProperty("invoice-pdf")]
    //    public ItemizedBillUrlInvoicePdf InvoicePdf { get; set; }
    //}

    //public class ItemizedBillUrlInvoiceItems
    //{
    //    [JsonProperty("links")]
    //    public ItemizedBillUrlInvoiceItemsLinks Links { get; set; }
    //}

    //public partial class ItemizedBillUrlInvoiceItemsLinks
    //{
    //    [JsonProperty("related")]
    //    public string Related { get; set; }
    //}

    //public partial class ItemizedBillUrlInvoicePdf
    //{
    //    [JsonProperty("data")]
    //    public ItemizedBillUrlData Data { get; set; }

    //    [JsonProperty("links")]
    //    public ItemizedBillUrlInvoiceItemsLinks Links { get; set; }
    //}

    //public class ItemizedBillUrlData
    //{
    //    [JsonProperty("type")]
    //    public string Type { get; set; }

    //    [JsonProperty("id")]
    //    public string Id { get; set; }
    //}
}