﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class DataBalanceResponse : ErrorResponse
    {
        public DataBalanceResponse()
        {
            DataResponse = new List<DataBalanceResponseData>();
        }

        [JsonProperty("data")] public List<DataBalanceResponseData> DataResponse { get; set; }

        public class DataBalanceResponseData
        {
            [JsonProperty("attributes")] public Attributes Attributes { get; set; }

            //[JsonProperty("relationships")]
            //public Relationships Relationships { get; set; }

            //[JsonProperty("links")]
            //public DatumLinks Links { get; set; }

            [JsonProperty("id")] public string Id { get; set; }

            [JsonProperty("type")] public string Type { get; set; }
        }

        public class Attributes
        {
            [JsonProperty("expiry-at")] public DateTimeOffset? ExpiryAt { get; set; }

            [JsonProperty("dedicated-account-id")] public long? DedicatedAccountId { get; set; }

            [JsonProperty("balance-name")] public Name BalanceName { get; set; }

            [JsonProperty("available-amount")] public double? AvailableAmount { get; set; }

            [JsonProperty("product-id")] public long? ProductId { get; set; }

            [JsonProperty("amount")] public double? Amount { get; set; }

            //[JsonProperty("total-amount")]
            //public double? TotalAmount { get; set; }

            //[JsonProperty("unit")]
            //public string Unit { get; set; }

            [JsonProperty("product-name")] public Name ProductName { get; set; }
        }


        //public partial class DatumLinks
        //{
        //    [JsonProperty("self")]
        //    public string Self { get; set; }
        //}

        //public partial class Relationships
        //{
        //    [JsonProperty("subscription")]
        //    public Subscription Subscription { get; set; }
        //}

        //public partial class Subscription
        //{
        //    [JsonProperty("data")]
        //    public Data Data { get; set; }

        //    [JsonProperty("links")]
        //    public SubscriptionLinks Links { get; set; }
        //}

        //public partial class Data
        //{
        //    [JsonProperty("type")]
        //    public string Type { get; set; }

        //    [JsonProperty("id")]
        //    public long Id { get; set; }
        //}

        //public partial class SubscriptionLinks
        //{
        //    [JsonProperty("related")]
        //    public string Related { get; set; }
        //}
    }
}