﻿using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class GiftDataPackRequestBody
    {
        [JsonProperty("data")] public GiftDataPackData Data { get; set; }
    }

    public class GiftDataPackData
    {
        [JsonProperty("type")] public string Type { get; set; }

        [JsonProperty("id")] public string Id { get; set; }

        [JsonProperty("meta")] public GiftDataPackMeta Meta { get; set; }
    }

    public class GiftDataPackMeta
    {
        [JsonProperty("services")] public GiftDataPackServices Services { get; set; }

        [JsonProperty("channel")] public string Channel { get; set; }
    }

    public class GiftDataPackServices
    {
        [JsonProperty("GIFTING")] public Gifting Gifting { get; set; }
    }

    public class Gifting
    {
        [JsonProperty("gift_to")] public string GiftTo { get; set; }
    }
}