﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class PrepaidSubscriptionBalanceResponse : ErrorResponse
    {
        //[JsonProperty("data")]
        //public List<DataBalanceResponseData> DataResponse { get; set; }
        public PrepaidSubscriptionBalanceResponse()
        {
            Includeds = new List<Included>();
        }

        [JsonProperty("included")] public List<Included> Includeds { get; set; }

        [JsonProperty("data")] public Datum[] Data { get; set; }


        //public class DataBalanceResponseData
        //{
        //[JsonProperty("attributes")]
        //public DatumAttributes Attributes { get; set; }

        //[JsonProperty("relationships")]
        //public DatumRelationships Relationships { get; set; }

        //[JsonProperty("links")]
        //public DatumLinks Links { get; set; }

        //[JsonProperty("id")]
        //public long Id { get; set; }

        //[JsonProperty("type")]
        //public string Type { get; set; }
        //}


        //public class DatumLinks
        //{
        //    [JsonProperty("self")]
        //    public string Self { get; set; }
        //}

        //public class DatumRelationships
        //{
        //    [JsonProperty("sim-cards")]
        //    public PuneHedgehog SimCards { get; set; }

        //    [JsonProperty("billing-accounts")]
        //    public PuneHedgehog BillingAccounts { get; set; }

        //    [JsonProperty("services")]
        //    public PuneHedgehog Services { get; set; }

        //    [JsonProperty("subscription-discounts")]
        //    public PuneHedgehog SubscriptionDiscounts { get; set; }

        //    [JsonProperty("network-services")]
        //    public PuneHedgehog NetworkServices { get; set; }

        //    [JsonProperty("available-loan-products")]
        //    public PuneHedgehog AvailableLoanProducts { get; set; }

        //    [JsonProperty("owner-customer")]
        //    public BillingRatePlan OwnerCustomer { get; set; }

        //    [JsonProperty("products")]
        //    public PuneHedgehog Products { get; set; }

        //    [JsonProperty("payer-customer")]
        //    public BillingRatePlan PayerCustomer { get; set; }

        //    [JsonProperty("available-subscription-types")]
        //    public PuneHedgehog AvailableSubscriptionTypes { get; set; }

        //    [JsonProperty("document-validations")]
        //    public PuneHedgehog DocumentValidations { get; set; }

        //    [JsonProperty("coordinator-customer")]
        //    public PuneHedgehog CoordinatorCustomer { get; set; }

        //    [JsonProperty("product-usages")]
        //    public PuneHedgehog ProductUsages { get; set; }

        //    [JsonProperty("porting-requests")]
        //    public PuneHedgehog PortingRequests { get; set; }

        //    [JsonProperty("billing-rate-plan")]
        //    public BillingRatePlan BillingRatePlan { get; set; }

        //    [JsonProperty("combined-usage-report")]
        //    public BillingRatePlan CombinedUsageReport { get; set; }

        //    [JsonProperty("user-customer")]
        //    public BillingRatePlan UserCustomer { get; set; }

        //    [JsonProperty("gsm-service-usages")]
        //    public PuneHedgehog GsmServiceUsages { get; set; }

        //    [JsonProperty("balances")]
        //    public Balances Balances { get; set; }

        //    [JsonProperty("billing-usages")]
        //    public PuneHedgehog BillingUsages { get; set; }

        //    [JsonProperty("barrings")]
        //    public PuneHedgehog Barrings { get; set; }

        //    [JsonProperty("subscription-type")]
        //    public BillingRatePlan SubscriptionType { get; set; }

        //    [JsonProperty("available-products")]
        //    public PuneHedgehog AvailableProducts { get; set; }

        //    [JsonProperty("catalog-sim-cards")]
        //    public PuneHedgehog CatalogSimCards { get; set; }

        //    [JsonProperty("connected-products")]
        //    public PuneHedgehog ConnectedProducts { get; set; }

        //    [JsonProperty("connection-type")]
        //    public BillingRatePlan ConnectionType { get; set; }

        //    [JsonProperty("available-child-products")]
        //    public PuneHedgehog AvailableChildProducts { get; set; }

        //    [JsonProperty("sim-card-orders")]
        //    public PuneHedgehog SimCardOrders { get; set; }
        //}

        //public class PurpleLinks
        //{
        //    [JsonProperty("related")]
        //    public string Related { get; set; }
        //}

        //public class Balances
        //{
        //    [JsonProperty("data")]
        //    public Dat[] Data { get; set; }

        //    [JsonProperty("links")]
        //    public PurpleLinks Links { get; set; }
        //}

        //public class Dat
        //{
        //    [JsonProperty("type")]
        //    public string Type { get; set; }

        //    [JsonProperty("id")]
        //    public string Id { get; set; }
        //}

        //public class BillingRatePlan
        //{
        //[JsonProperty("data")]
        //public Dat Data { get; set; }

        //[JsonProperty("links")]
        //public PurpleLinks Links { get; set; }
        // }

        public class Included
        {
            [JsonProperty("attributes")] public IncludedAttributes Attributes { get; set; }

            //[JsonProperty("relationships")]
            //public IncludedRelationships Relationships { get; set; }

            //[JsonProperty("links")]
            //public DatumLinks Links { get; set; }

            //[JsonProperty("id")]
            //public string Id { get; set; }

            //[JsonProperty("type")]
            //public string Type { get; set; }
        }

        public class Datum
        {
            [JsonProperty("attributes")] public DatumAttributes Attributes { get; set; }

            //[JsonProperty("relationships")]
            //public DatumRelationships Relationships { get; set; }

            //[JsonProperty("links")]
            //public DatumLinks Links { get; set; }

            [JsonProperty("id")]
            //[JsonConverter(typeof(ParseStringConverter))]
            public string Id { get; set; }

            [JsonProperty("type")] public string Type { get; set; }
        }

        public class DatumAttributes
        {
            [JsonProperty("monthly-costs")] public long MonthlyCosts { get; set; }

            [JsonProperty("allow-reactivation")] public bool AllowReactivation { get; set; }

            [JsonProperty("contract-status")] public string ContractStatus { get; set; }

            [JsonProperty("first-call-date")] public object FirstCallDate { get; set; }

            [JsonProperty("termination-time")] public object TerminationTime { get; set; }

            //[JsonProperty("contract-id")]
            //[JsonConverter(typeof(ParseStringConverter))]
            public string ContractId { get; set; }

            [JsonProperty("msisdn")] public string Msisdn { get; set; }

            [JsonProperty("activation-time")] public DateTimeOffset ActivationTime { get; set; }

            [JsonProperty("status")] public string Status { get; set; }

            [JsonProperty("latest-contract-termination-time")]
            public DateTimeOffset LatestContractTerminationTime { get; set; }

            [JsonProperty("directory-listing")] public string DirectoryListing { get; set; }

            [JsonProperty("payment-type")] public string PaymentType { get; set; }

            [JsonProperty("original-contract-confirmation-code")]
            public string OriginalContractConfirmationCode { get; set; }
        }

        public class IncludedAttributes
        {
            [JsonProperty("expiry-at")] public DateTime? ExpiryAt { get; set; }

            [JsonProperty("dedicated-account-id")] public long? DedicatedAccountId { get; set; }

            //[JsonProperty("balance-name")]
            //public string BalanceName { get; set; }

            [JsonProperty("lifecycle")] public Lifecycle Lifecycle { get; set; }

            [JsonProperty("is-main-balance")] public bool IsMainBalance { get; set; }

            [JsonProperty("product-id")] public long? ProductId { get; set; }

            [JsonProperty("amount")] public double? Amount { get; set; }

            [JsonProperty("total-amount")] public double? TotalAmount { get; set; }

            [JsonProperty("gsm-service-type")] public string GsmServiceType { get; set; }

            [JsonProperty("product-code")] public string ProductCode { get; set; }

            [JsonProperty("unit")] public string Unit { get; set; }

            [JsonProperty("product-name")] public Name ProductName { get; set; }
        }

        public class Lifecycle
        {
            //[JsonProperty("service-removal-date")]
            //public DateTimeOffset? ServiceRemovalDate { get; set; }

            //[JsonProperty("credit-clearance-date")]
            //public DateTimeOffset? CreditClearanceDate { get; set; }

            [JsonProperty("supervision-expiry-date")]
            public DateTimeOffset? SupervisionExpiryDate { get; set; }

            //[JsonProperty("service-fee-expiry-date")]
            //public DateTimeOffset? ServiceFeeExpiryDate { get; set; }
        }

        //public class IncludedRelationships
        //{
        //    [JsonProperty("subscription")]
        //    public BillingRatePlan Subscription { get; set; }
        //}
    }
}