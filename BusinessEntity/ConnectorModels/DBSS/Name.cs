﻿using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class Name
    {
        [JsonProperty("en")] public string En { get; set; }

        [JsonProperty("bn")] public string Bn { get; set; }
    }
}