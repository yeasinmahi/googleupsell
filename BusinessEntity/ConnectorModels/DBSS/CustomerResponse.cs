﻿using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class CustomerResponse : ErrorResponse
    {
        [JsonProperty("data")] public CustomerData Data { get; set; }
    }

    public class CustomerData
    {
        [JsonProperty("attributes")] public CustomerAttributes Attributes { get; set; }

        [JsonProperty("id")] public string Id { get; set; }
    }

    public class CustomerAttributes
    {
        //[JsonProperty("id-expiry")]
        //public DateTimeOffset? IdExpiry { get; set; }

        //[JsonProperty("email")]
        //public string Email { get; set; }

        //[JsonProperty("bank-account-number")]
        //public object BankAccountNumber { get; set; }

        //[JsonProperty("account-type")]
        //public object AccountType { get; set; }

        //[JsonProperty("date-of-birth")]
        //public DateTimeOffset? DateOfBirth { get; set; }

        //[JsonProperty("ban")]
        //public object Ban { get; set; }

        //[JsonProperty("id-document-type")]
        //public string IdDocumentType { get; set; }

        //[JsonProperty("is-company")]
        //public bool IsCompany { get; set; }

        //[JsonProperty("online-id")]
        //public object OnlineId { get; set; }

        //[JsonProperty("frame-agreement-ended-at")]
        //public object FrameAgreementEndedAt { get; set; }

        //[JsonProperty("payment-method")]
        //public string PaymentMethod { get; set; }

        //[JsonProperty("agreement-start-date")]
        //public DateTimeOffset? AgreementStartDate { get; set; }

        //[JsonProperty("language")]
        //public string Language { get; set; }

        //[JsonProperty("is-loyalty-manager")]
        //public bool IsLoyaltyManager { get; set; }

        //[JsonProperty("id-document-number")]
        //public string IdDocumentNumber { get; set; }

        //[JsonProperty("invoice-delivery-type")]
        //public string InvoiceDeliveryType { get; set; }

        //[JsonProperty("frame-agreement-started-at")]
        //public object FrameAgreementStartedAt { get; set; }

        //[JsonProperty("nationality")]
        //public string Nationality { get; set; }

        //[JsonProperty("trade-register-id")]
        //public object TradeRegisterId { get; set; }

        //[JsonProperty("business-uid")]
        //public object BusinessUid { get; set; }

        //[JsonProperty("marketing-own")]
        //public bool MarketingOwn { get; set; }

        //[JsonProperty("alt-contact-phone")]
        //public string AltContactPhone { get; set; }

        //[JsonProperty("category")]
        //public string Category { get; set; }

        [JsonProperty("first-name")] public string FirstName { get; set; }

        //[JsonProperty("is-coordinator")]
        //public bool IsCoordinator { get; set; }

        //[JsonProperty("occupation")]
        //public string Occupation { get; set; }

        [JsonProperty("middle-name")] public string MiddleName { get; set; }

        //[JsonProperty("segmentation-category")]
        //public string SegmentationCategory { get; set; }

        //[JsonProperty("is-fleet-manager")]
        //public bool IsFleetManager { get; set; }

        //[JsonProperty("marketing-third-party")]
        //public bool MarketingThirdParty { get; set; }

        [JsonProperty("last-name")] public string LastName { get; set; }

        //[JsonProperty("contact-phone")]
        //public string ContactPhone { get; set; }

        //[JsonProperty("gender")]
        //public string Gender { get; set; }
    }
}