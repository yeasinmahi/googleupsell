﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class PackageMigrationResponse : ErrorResponse
    {
        public PackageMigrationResponse()
        {
            Data = new List<Datum>();
            IncludedList = new List<Included>();
        }

        [JsonProperty("data")] public List<Datum> Data { get; set; }

        [JsonProperty("included")] public List<Included> IncludedList { get; set; }

        public class Datum
        {
            //[JsonProperty("attributes")]
            //public DatumAttributes Attributes { get; set; }

            //[JsonProperty("relationships")]
            //public DatumRelationships Relationships { get; set; }

            //[JsonProperty("links")]
            //public DatumLinks Links { get; set; }

            //[JsonProperty("id")]
            //public long Id { get; set; }

            //[JsonProperty("type")]
            //public string Type { get; set; }
        }

        //public class DatumAttributes
        //{
        //    [JsonProperty("monthly-costs")]
        //    public long MonthlyCosts { get; set; }

        //    [JsonProperty("contract-status")]
        //    public string ContractStatus { get; set; }

        //    [JsonProperty("first-call-date")]
        //    public object FirstCallDate { get; set; }

        //    [JsonProperty("termination-time")]
        //    public object TerminationTime { get; set; }

        //    [JsonProperty("contract-id")]
        //    public long ContractId { get; set; }

        //    [JsonProperty("msisdn")]
        //    public string Msisdn { get; set; }

        //    [JsonProperty("activation-time")]
        //    public DateTimeOffset ActivationTime { get; set; }

        //    [JsonProperty("status")]
        //    public string Status { get; set; }

        //    [JsonProperty("latest-contract-termination-time")]
        //    public DateTimeOffset LatestContractTerminationTime { get; set; }

        //    [JsonProperty("directory-listing")]
        //    public string DirectoryListing { get; set; }

        //    [JsonProperty("payment-type")]
        //    public string PaymentType { get; set; }

        //    [JsonProperty("original-contract-confirmation-code")]
        //    public string OriginalContractConfirmationCode { get; set; }

        //    [JsonProperty("credit-limit")]
        //    public long CreditLimit { get; set; }
        //}

        //public class DatumLinks
        //{
        //    [JsonProperty("self")]
        //    public string Self { get; set; }
        //}

        //public class DatumRelationships
        //{
        //    [JsonProperty("sim-cards")]
        //    public PuneHedgehog SimCards { get; set; }

        //    [JsonProperty("services")]
        //    public PuneHedgehog Services { get; set; }

        //    [JsonProperty("subscription-discounts")]
        //    public PuneHedgehog SubscriptionDiscounts { get; set; }

        //    [JsonProperty("network-services")]
        //    public PuneHedgehog NetworkServices { get; set; }

        //    [JsonProperty("owner-customer")]
        //    public BillingRatePlan OwnerCustomer { get; set; }

        //    [JsonProperty("products")]
        //    public PuneHedgehog Products { get; set; }

        //    [JsonProperty("payer-customer")]
        //    public BillingRatePlan PayerCustomer { get; set; }

        //    [JsonProperty("available-subscription-types")]
        //    public AvailableSubscriptionTypes AvailableSubscriptionTypes { get; set; }

        //    [JsonProperty("document-validations")]
        //    public PuneHedgehog DocumentValidations { get; set; }

        //    [JsonProperty("product-usages")]
        //    public PuneHedgehog ProductUsages { get; set; }

        //    [JsonProperty("porting-requests")]
        //    public PuneHedgehog PortingRequests { get; set; }

        //    [JsonProperty("billing-rate-plan")]
        //    public BillingRatePlan BillingRatePlan { get; set; }

        //    [JsonProperty("combined-usage-report")]
        //    public BillingRatePlan CombinedUsageReport { get; set; }

        //    [JsonProperty("user-customer")]
        //    public BillingRatePlan UserCustomer { get; set; }

        //    [JsonProperty("gsm-service-usages")]
        //    public PuneHedgehog GsmServiceUsages { get; set; }

        //    [JsonProperty("balances")]
        //    public PuneHedgehog Balances { get; set; }

        //    [JsonProperty("billing-usages")]
        //    public PuneHedgehog BillingUsages { get; set; }

        //    [JsonProperty("barrings")]
        //    public PuneHedgehog Barrings { get; set; }

        //    [JsonProperty("subscription-type")]
        //    public BillingRatePlan SubscriptionType { get; set; }

        //    [JsonProperty("available-products")]
        //    public PuneHedgehog AvailableProducts { get; set; }

        //    [JsonProperty("catalog-sim-cards")]
        //    public PuneHedgehog CatalogSimCards { get; set; }

        //    [JsonProperty("connected-products")]
        //    public PuneHedgehog ConnectedProducts { get; set; }

        //    [JsonProperty("connection-type")]
        //    public BillingRatePlan ConnectionType { get; set; }

        //    [JsonProperty("available-child-products")]
        //    public PuneHedgehog AvailableChildProducts { get; set; }

        //    [JsonProperty("sim-card-orders")]
        //    public PuneHedgehog SimCardOrders { get; set; }
        //}

        //public class PuneHedgehog
        //{
        //    [JsonProperty("links")]
        //    public PurpleLinks Links { get; set; }
        //}

        //public class PurpleLinks
        //{
        //    [JsonProperty("related")]
        //    public string Related { get; set; }
        //}

        //public class AvailableSubscriptionTypes
        //{
        //    [JsonProperty("data")]
        //    public List<Dat> Data { get; set; }

        //    [JsonProperty("links")]
        //    public PurpleLinks Links { get; set; }
        //}

        //public class Dat
        //{
        //    [JsonProperty("type")]
        //    public string Type { get; set; }

        //    [JsonProperty("id")]
        //    public long Id { get; set; }
        //}

        //public class BillingRatePlan
        //{
        //    [JsonProperty("data")]
        //    public Dat Data { get; set; }

        //    [JsonProperty("links")]
        //    public PurpleLinks Links { get; set; }
        //}

        public class Included
        {
            [JsonProperty("attributes")] public IncludedAttributes Attributes { get; set; }

            //[JsonProperty("relationships")]
            //public IncludedRelationships Relationships { get; set; }

            //[JsonProperty("links")]
            //public DatumLinks Links { get; set; }

            [JsonProperty("id")] public long Id { get; set; }

            [JsonProperty("type")] public string Type { get; set; }
        }

        public class IncludedAttributes
        {
            [JsonProperty("name")] public Name Name { get; set; }

            //[JsonProperty("brand")]
            //public string Brand { get; set; }

            [JsonProperty("code")] public string Code { get; set; }

            //[JsonProperty("business-model-type")]
            //public string BusinessModelType { get; set; }

            //[JsonProperty("payment-type")]
            //public string PaymentType { get; set; }

            //[JsonProperty("change-price")]
            //public long ChangePrice { get; set; }

            //[JsonProperty("change-price-currency")]
            //public object ChangePriceCurrency { get; set; }

            //[JsonProperty("credit-limit")]
            //public long CreditLimit { get; set; }
        }

        //public class IncludedRelationships
        //{
        //    [JsonProperty("included-products")]
        //    public PuneHedgehog IncludedProducts { get; set; }

        //    [JsonProperty("billing-rate-plans")]
        //    public PuneHedgehog BillingRatePlans { get; set; }

        //    [JsonProperty("mandatory-products")]
        //    public PuneHedgehog MandatoryProducts { get; set; }

        //    [JsonProperty("option-group-products")]
        //    public PuneHedgehog OptionGroupProducts { get; set; }

        //    [JsonProperty("option-groups")]
        //    public PuneHedgehog OptionGroups { get; set; }

        //    [JsonProperty("fees")]
        //    public PuneHedgehog Fees { get; set; }

        //    [JsonProperty("optional-products")]
        //    public PuneHedgehog OptionalProducts { get; set; }
        //}
    }
}