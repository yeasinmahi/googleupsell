﻿//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace BusinessEntity.ConnectorModels.DBSS
//{
//    public class RechargeResponseV2
//    {
//        [JsonProperty("data")]
//        public RechargeDatum[] Data { get; set; }
//    }

//    public class RechargeDatum
//    {
//        [JsonProperty("type")]
//        public RechargeTypeEnum Type { get; set; }

//        [JsonProperty("attributes")]
//        public Attributes Attributes { get; set; }
//    }

//    public class RechargeAttributes
//    {
//        [JsonProperty("transaction-id")]
//        public string TransactionId { get; set; }

//        [JsonProperty("record-id")]
//        public string RecordId { get; set; }

//        [JsonProperty("transaction-sub-type")]
//        public RechargeTransactionSubType TransactionSubType { get; set; }

//        [JsonProperty("edr-type")]
//        public RechargeEdrType EdrType { get; set; }

//        [JsonProperty("edr-type-label")]
//        public RechargeEdrTypeLabel EdrTypeLabel { get; set; }

//        [JsonProperty("subscription-id")]
//        public long SubscriptionId { get; set; }

//        [JsonProperty("created-at")]
//        public DateTimeOffset CreatedAt { get; set; }

//        [JsonProperty("msisdn")]
//        public string Msisdn { get; set; }

//        [JsonProperty("event-at")]
//        public DateTimeOffset EventAt { get; set; }

//        [JsonProperty("balances")]
//        public RechargeBalance[] Balances { get; set; }

//        [JsonProperty("comment")]
//        public object Comment { get; set; }

//        [JsonProperty("transaction-type")]
//        public long TransactionType { get; set; }

//        [JsonProperty("event-source")]
//        public long EventSource { get; set; }

//        [JsonProperty("customer-id")]
//        public long CustomerId { get; set; }
//    }

//    public class RechargeBalance
//    {
//        [JsonProperty("transaction-amount")]
//        public object TransactionAmount { get; set; }

//        [JsonProperty("balance-before")]
//        public long? BalanceBefore { get; set; }

//        [JsonProperty("product-id")]
//        public object ProductId { get; set; }

//        [JsonProperty("offer-id")]
//        public object OfferId { get; set; }

//        [JsonProperty("balance-id")]
//        public string BalanceId { get; set; }

//        [JsonProperty("unit-type")]
//        public long? UnitType { get; set; }

//        [JsonProperty("balance-after")]
//        public object BalanceAfter { get; set; }
//    }

//    public enum RechargeEdrType { Adj, Rch };

//    public enum RechargeEdrTypeLabel { Adjustment, Recharge };

//    public enum RechargeTransactionSubType { Empty, The1Qinspirepost };

//    public enum RechargeTypeEnum { EventDetailRecords };
//}

