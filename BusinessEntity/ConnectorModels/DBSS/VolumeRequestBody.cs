﻿using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.DBSS
{
    public class VolumeRequestBody
    {
        [JsonProperty("data")] public VolumeRequestData Data { get; set; }

        public class VolumeRequestData
        {
            [JsonProperty("type")] public string Type { get; set; }

            [JsonProperty("id")] public string Id { get; set; }

            [JsonProperty("meta")] public VolumeRequestMeta Meta { get; set; }
        }

        public class VolumeRequestMeta
        {
            [JsonProperty("services")] public VolumeRequestServices Services { get; set; }

            [JsonProperty("channel")] public string Channel { get; set; }
        }

        public class VolumeRequestServices
        {
            [JsonProperty("REQUEST")] public VolumeRequestRequest Request { get; set; }
        }

        public class VolumeRequestRequest
        {
            [JsonProperty("b_msisdn")] public string BMsisdn { get; set; }
        }
    }
}