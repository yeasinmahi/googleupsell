﻿namespace BusinessEntity.ConnectorModels.Base
{
    public class NameAttribute
    {
        public string ru { get; set; }
        public string ka { get; set; }
        public string en { get; set; }
    }
}