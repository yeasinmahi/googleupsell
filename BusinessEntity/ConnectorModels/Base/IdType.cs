﻿namespace BusinessEntity.ConnectorModels.Base
{
    public class IdType
    {
        public string type { get; set; }
        public string id { get; set; }
    }
}