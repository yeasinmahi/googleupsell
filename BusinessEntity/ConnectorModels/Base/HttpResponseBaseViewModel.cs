﻿using Newtonsoft.Json;

namespace BusinessEntity.ConnectorModels.Base
{
    public class HttpResponseBaseViewModel
    {
        [JsonProperty("errors")] public HttpError Errors { get; set; }

        public uint StatusCode { get; set; }

        //public bool IsSuccessStatusCode { get; set; }
        public string ReasonPhrase { get; set; }
    }

    public class HttpErrors
    {
        [JsonProperty("detail")] public string Detail { get; set; }

        [JsonProperty("source")] public HttpErrorSource Source { get; set; }

        [JsonProperty("status")] public int Status { get; set; }

        [JsonProperty("meta")] public HttpErrorMeta Meta { get; set; }
    }

    public class HttpErrorMeta
    {
        [JsonProperty("error_code")] public string ErrorCode { get; set; }
    }

    public class HttpErrorSource
    {
        [JsonProperty("pointer")] public string Pointer { get; set; }
    }

    public class HttpError
    {
        [JsonProperty("errors")] public HttpErrors[] Errors { get; set; }
    }
}