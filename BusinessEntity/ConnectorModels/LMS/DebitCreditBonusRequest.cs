﻿namespace BusinessEntity.ConnectorModels.LMS
{
    public class DebitCreditBonusRequest : GetCustomerInfoWithIDRequest
    {
        public string bonusName { get; set; }
        public int quantity { get; set; }
        public string origin { get; set; }
    }
}