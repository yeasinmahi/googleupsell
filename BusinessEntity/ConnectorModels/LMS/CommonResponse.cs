﻿namespace BusinessEntity.ConnectorModels.LMS
{
    public class CommonResponse : ErrorResponse
    {
        public int apiVersion { get; set; }
        public int responseCode { get; set; }
        public string responseMessage { get; set; }
    }
}