﻿namespace BusinessEntity.ConnectorModels.LMS
{
    public class CommonRequest
    {
        public CommonRequest()
        {
            apiVersion = 1;
        }

        public int apiVersion { get; set; }
        public string loginName { get; set; }
        public string password { get; set; }
    }
}