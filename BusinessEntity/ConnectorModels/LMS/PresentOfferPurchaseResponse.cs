﻿namespace BusinessEntity.ConnectorModels.LMS
{
    public class PresentOfferPurchaseResponse : CommonResponse
    {
        public string ID { get; set; }
        public string Status { get; set; }
    }
}