﻿namespace BusinessEntity.ConnectorModels.LMS
{
    public class GetCustomerInfoWithIDRequest : CommonRequest
    {
        public string customerID { get; set; }
    }
}