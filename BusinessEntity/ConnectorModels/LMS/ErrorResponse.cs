﻿namespace BusinessEntity.ConnectorModels.LMS
{
    public class ErrorResponse
    {
        public int PlatformID { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorType { get; set; }
        public string ErrorMessage { get; set; }
    }
}