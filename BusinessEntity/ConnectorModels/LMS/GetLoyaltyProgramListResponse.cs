﻿using System.Collections.Generic;

namespace BusinessEntity.ConnectorModels.LMS
{
    public class GetLoyaltyProgramListResponse : CommonResponse
    {
        public GetLoyaltyProgramListResponse()
        {
            loyaltyPrograms = new List<LoyaltyProgram>();
        }

        public List<LoyaltyProgram> loyaltyPrograms { get; set; }
    }

    public class GetLoyaltyProgramResponse : CommonResponse
    {
        public GetLoyaltyProgramResponse()
        {
            loyaltyProgram = new LoyaltyProgram();
        }

        public LoyaltyProgram loyaltyProgram { get; set; }
    }
}