﻿using System;
using System.Collections.Generic;

namespace BusinessEntity.ConnectorModels.LMS
{
    public class CommonClass
    {
    }

    public class LoyaltyProgram
    {
        public LoyaltyProgram()
        {
            tiers = new List<Tier>();
            characteristics = new List<Characteristic>();
        }

        public List<Characteristic> characteristics { get; set; }

        public string statusPointsID { get; set; }

        public List<Tier> tiers { get; set; }

        public string loyaltyProgramDescription { get; set; }
        public string loyaltyProgramName { get; set; }
        public string loyaltyProgramID { get; set; }

        public string loyaltyProgramType { get; set; }
        public string rewardPointsID { get; set; }
    }

    public class Tier
    {
        public string tierName { get; set; }
        public int statusPointLevel { get; set; }
        public int numberOfRewardPointsPerUnit { get; set; }
        public int numberOfStatusPointsPerUnit { get; set; }
        public string statusEventName { get; set; }

        public string rewardEventName { get; set; }
    }

    public class Characteristic
    {
        public string characteristicID { get; set; }
        public int value { get; set; }
    }

    public class OfferPrice
    {
        public string ID { get; set; }
        public int value { get; set; }
    }

    public class OfferContent
    {
        public string ID { get; set; }
        public int value { get; set; }
    }

    public class OfferInList
    {
        public int offerID { get; set; }
        public string offerName { get; set; }
    }

    public class DataRecord
    {
        public string eventID { get; set; }
        public string moduleName { get; set; }
        public string featureName { get; set; }
        public string origin { get; set; }
        public string deliveryRequestID { get; set; }
        public string eventDatetime { get; set; }
        public string providerName { get; set; }
        public string customerId { get; set; }
        public string deliverableName { get; set; }
        public int deliverableQty { get; set; }
        public string operation { get; set; }
        public string deliveryStatus { get; set; }
        public DateTime? deliverableExpirationDate { get; set; }
        public int apiVersion { get; set; }
        public int responseCode { get; set; }
        public string responseMessage { get; set; }
        public DateTime? deliveryDate { get; set; }
    }
}