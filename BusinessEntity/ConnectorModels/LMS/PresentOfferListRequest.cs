﻿namespace BusinessEntity.ConnectorModels.LMS
{
    public class PresentOfferListRequest
    {
        public long msisdn { get; set; }
        public string language { get; set; }
        public int channelID { get; set; }
        public int userID { get; set; }
        public int serviceTypeID { get; set; }
        public int salesChannelID { get; set; }
    }
}