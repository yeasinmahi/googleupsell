﻿namespace BusinessEntity.ConnectorModels.LMS
{
    public class PresentOfferPurchaseRequest
    {
        public long msisdn { get; set; }
        public string language { get; set; }
        public string offerID { get; set; }
        public int channelID { get; set; }

        public int salesChannelID { get; set; }
        public int actionCall { get; set; }
    }
}