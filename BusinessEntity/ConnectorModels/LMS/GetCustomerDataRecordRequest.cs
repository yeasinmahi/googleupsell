﻿namespace BusinessEntity.ConnectorModels.LMS
{
    public class GetCustomerDataRecordRequest : CommonRequest
    {
        public string customerID { get; set; }
        public string startDate { get; set; }
    }
}