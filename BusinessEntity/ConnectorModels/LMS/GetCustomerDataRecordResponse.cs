﻿using System.Collections.Generic;

namespace BusinessEntity.ConnectorModels.LMS
{
    public class GetCustomerDataRecordResponse : CommonResponse
    {
        public GetCustomerDataRecordResponse()
        {
            records = new List<DataRecord>();
        }

        public List<DataRecord> records { get; set; }
    }

    public class GetCustomerBRDResponse : CommonResponse
    {
        public GetCustomerBRDResponse()
        {
            BDRs = new List<DataRecord>();
        }

        public List<DataRecord> BDRs { get; set; }
    }

    public class GetCustomerORDResponse : CommonResponse
    {
        public GetCustomerORDResponse()
        {
            ODRs = new List<DataRecord>();
        }

        public List<DataRecord> ODRs { get; set; }
    }
}