﻿namespace BusinessEntity.ConnectorModels.LMS
{
    public class PresentOfferDetailRequest
    {
        public int msisdn { get; set; }
        public string language { get; set; }
        public int channelID { get; set; }
        public int userID { get; set; }
        public string offerID { get; set; }
        public int salesChannelID { get; set; }
    }
}