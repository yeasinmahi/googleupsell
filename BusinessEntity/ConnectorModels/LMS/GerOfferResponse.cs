﻿using System;
using System.Collections.Generic;

namespace BusinessEntity.ConnectorModels.LMS
{
    public class GerOfferResponse : CommonResponse
    {
        public GerOfferResponse()
        {
            characteristics = new List<Characteristic>();
            offerPrice = new List<OfferPrice>();
            offerContent = new List<OfferContent>();
        }

        public List<OfferContent> offerContent { get; set; }
        public List<OfferPrice> offerPrice { get; set; }
        public List<Characteristic> characteristics { get; set; }

        public long offerID { get; set; }


        public string offerName { get; set; }
        public string description { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public string objective { get; set; }
    }
}