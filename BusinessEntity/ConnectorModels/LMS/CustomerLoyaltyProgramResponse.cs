﻿using System;
using System.Collections.Generic;

namespace BusinessEntity.ConnectorModels.LMS
{
    public class CustomerLoyaltyProgramResponse : CommonResponse
    {
        public CustomerLoyaltyProgramResponse()
        {
            loyaltyPrograms = new List<CustomerLoyaltyProgram>();
        }

        public List<CustomerLoyaltyProgram> loyaltyPrograms { get; set; }
    }

    public class CustomerLoyaltyProgram
    {
        public CustomerLoyaltyProgram()
        {
            loyaltyProgramHistory = new List<CustomerLoyaltyProgramHistory>();
        }

        public List<CustomerLoyaltyProgramHistory> loyaltyProgramHistory { get; set; }

        public string loyaltyProgramName { get; set; }
        public DateTime? loyaltyProgramEnrollmentDate { get; set; }
        public string tierName { get; set; }
        public DateTime? tierEnrollmentDate { get; set; }
        public int rewardsPointsBalance { get; set; }
        public int rewardsPointsEarned { get; set; }
        public int rewardsPointsConsumed { get; set; }
        public int rewardsPointsExpired { get; set; }
        public string loyaltyProgramType { get; set; }
    }

    public class CustomerLoyaltyProgramHistory
    {
        public string fromTier { get; set; }
        public string toTier { get; set; }
        public DateTime? transitionDate { get; set; }
    }
}