﻿namespace BusinessEntity.ConnectorModels.LMS
{
    public class GetOffersRequest : CommonRequest
    {
        public long offerID { get; set; }
    }
}