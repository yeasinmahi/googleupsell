﻿using System.Collections.Generic;

namespace BusinessEntity.ConnectorModels.LMS
{
    public class GetOfferListResponse : CommonResponse
    {
        public GetOfferListResponse()
        {
            offer = new List<OfferInList>();
        }

        public List<OfferInList> offer { get; set; }
    }
}