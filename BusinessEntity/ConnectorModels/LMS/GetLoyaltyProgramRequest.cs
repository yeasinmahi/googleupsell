﻿namespace BusinessEntity.ConnectorModels.LMS
{
    public class GetLoyaltyProgramRequest : CommonRequest
    {
        public long loyaltyProgramID { get; set; }
    }
}