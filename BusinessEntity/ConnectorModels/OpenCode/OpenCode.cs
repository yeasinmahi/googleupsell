﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Serialization;
using BusinessEntity.APIHub2;

namespace BusinessEntity.ConnectorModels.OpenCode
{
    public class OpenCode
    {
        #region Delete Offer

        public RetMessage DeleteOffer(string msisdn, string offerId)
        {
            var message = new RetMessage();
            try
            {
                //string url = ConfigurationManager.AppSettings["XMLRpcBaseURL"];
                //var userName = ConfigurationManager.AppSettings["XMLRpcUserName"];
                //var password = ConfigurationManager.AppSettings["XMLRpcPassword"];

                var url = AppSettings.OpenCodeBaseURL;
                var userName = AppSettings.OpenCodeUserName;
                var password = AppSettings.OpenCodePassword;

                var content =
                    "<?xml version=\"1.0\"?> <methodCall> <methodName>DeleteOffer</methodName> <params> <param> <value> <struct> <member> <name>originNodeType</name> <value><string>EXT</string></value> </member> <member><name>originHostName</name> <value><string>TEST</string></value> </member> <member><name>originTransactionID</name> <value><string>1494908896782</string></value> </member> <member><name>originTimeStamp</name> <value><dateTime.iso8601>20170516T10:28:16+0600</dateTime.iso8601></value> </member> <member><name>subscriberNumber</name> <value><string>" +
                    msisdn + "</string></value> </member> <member><name>offerID</name> <value><int>" + offerId +
                    "</int></value> </member> <member><name>messageCapabilityFlag</name> <value> <struct> <member><name>accountActivationFlag</name> <value><boolean>1</boolean></value> </member> </struct> </value> </member> </struct> </value> </param> </params> </methodCall>";
                var requestData = Encoding.ASCII.GetBytes(content);
                var request = (HttpWebRequest) WebRequest.Create(url);
                request.Credentials = new NetworkCredential(userName, password);
                //request.Headers.Add("Authorization", "Basic ZmRzdXNlcjpmZHN1c2VyCg==");
                request.Method = "POST";
                request.UserAgent = "UGw Server/4.3/1.0";
                request.ContentType = "text/xml";
                request.Accept = "text/xml";
                request.Timeout = 60000;
                request.ContentLength = requestData.Length;
                request.ServicePoint.ConnectionLimit = 1;
                request.KeepAlive = false;
                var result = "";


                using (var requestStream = request.GetRequestStream())
                {
                    requestStream.Write(requestData, 0, requestData.Length);
                    using (var response = (HttpWebResponse) request.GetResponse())
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            using (var reader = new StreamReader(stream, Encoding.ASCII))
                            {
                                result = reader.ReadToEnd();
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(result))
                {
                    var obj = DeserializeUpdateOfferResponse(result);

                    if (obj != null)
                    {
                        if (obj.responseCode == "0")
                        {
                            message.Data = obj;
                        }
                        else
                        {
                            message.Message = "Response Code :" + obj.responseCode;
                            message.MessageType = MessageType.None;
                            if (!string.IsNullOrEmpty(obj.faultCode))
                                message.Message = "Fault occur. Fault Code :" + obj.faultCode + ", fault String : " +
                                                  obj.faultString;
                        }
                    }
                }
            }
            catch (WebException webex)
            {
                var resp = "";
                var errResp = webex.Response;
                using (var respStream = errResp.GetResponseStream())
                {
                    var reader = new StreamReader(respStream);
                    resp = reader.ReadToEnd();
                }

                message.Message = resp;
                message.MessageType = MessageType.Error;
            }
            catch (Exception ex)
            {
                var a = ex.Message;
                message.Message = ex.Message;
                message.MessageType = MessageType.Error;
            }

            return message;
        }

        #endregion

        #region Check Loan

        public RetMessage GetBalanceAndDate(string msisdn)
        {
            var message = new RetMessage();
            try
            {
                //string url = ConfigurationManager.AppSettings["XMLRpcBaseURL"];
                //var userName = ConfigurationManager.AppSettings["XMLRpcUserName"];
                //var password = ConfigurationManager.AppSettings["XMLRpcPassword"];

                var url = AppSettings.OpenCodeBaseURL;
                var userName = AppSettings.OpenCodeUserName;
                var password = AppSettings.OpenCodePassword;

                var content =
                    "<?xml version=\"1.0\"?> <methodCall> <methodName>GetBalanceAndDate</methodName> <params> <param> <value> <struct> <member> <name>originNodeType</name> <value> <string>EXT</string> </value> </member> <member> <name>originHostName</name> <value> <string>TEST</string> </value> </member> <member> <name>originTransactionID</name> <value> <string>00020180514000000012</string> </value> </member> <member> <name>originTimeStamp</name> <value> <dateTime.iso8601>20121102T10:25:00+0700</dateTime.iso8601> </value> </member> <member> <name>subscriberNumber</name> <value> <string>" +
                    msisdn +
                    "</string> </value> </member> <member> <name>subscriberNumberNAI</name> <value> <i4>1</i4> </value> </member> <member> <name>dedicatedAccountSelection</name> <value> <array> <data> <value> <struct> <member> <name>dedicatedAccountIDFirst</name> <value> <i4>1</i4> </value> </member> <member> <name>dedicatedAccountIDLast</name> <value> <i4>255</i4> </value> </member> </struct> </value> </data> </array> </value> </member> </struct> </value> </param> </params> </methodCall>";
                var requestData = Encoding.ASCII.GetBytes(content);
                var request = (HttpWebRequest) WebRequest.Create(url);
                request.Credentials = new NetworkCredential(userName, password);
                //request.Headers.Add("Authorization", "Basic ZmRzdXNlcjpmZHN1c2VyCg==");
                request.Method = "POST";
                request.UserAgent = "UGw Server/4.3/1.0";
                request.ContentType = "text/xml";
                request.Accept = "text/xml";
                request.Timeout = 60000;
                request.ContentLength = requestData.Length;
                request.ServicePoint.ConnectionLimit = 1;
                request.KeepAlive = false;
                var result = "";


                using (var requestStream = request.GetRequestStream())
                {
                    requestStream.Write(requestData, 0, requestData.Length);
                    using (var response = (HttpWebResponse) request.GetResponse())
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            using (var reader = new StreamReader(stream, Encoding.ASCII))
                            {
                                result = reader.ReadToEnd();
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(result))
                {
                    var obj = DeserializeResponse(result);

                    if (obj != null)
                    {
                        if (obj.responseCode == "0")
                        {
                            message.Data = obj;
                        }
                        else
                        {
                            message.Message = "No Data found. Response Code :" + obj.responseCode;
                            message.MessageType = MessageType.None;
                        }
                    }
                }
            }
            catch (WebException webex)
            {
                var resp = "";
                var errResp = webex.Response;
                if (errResp != null)
                {
                    using (var respStream = errResp.GetResponseStream())
                    {
                        var reader = new StreamReader(respStream);
                        resp = reader.ReadToEnd();
                    }

                    message.Message = resp;
                }
                else
                {
                    resp = webex.Message;
                }

                message.Message = resp;
                message.MessageType = MessageType.Error;
            }
            catch (Exception ex)
            {
                var a = ex.Message;
                message.Message = ex.Message;
                message.MessageType = MessageType.Error;
            }

            return message;
        }

        private BalanceInfo DeserializeResponse(string xml)
        {
            var balance = new BalanceInfo();
            try
            {
                var serializer = new XmlSerializer(typeof(MethodResponse));
                MethodResponse result;
                using (TextReader reader = new StringReader(xml))
                {
                    result = (MethodResponse) serializer.Deserialize(reader);
                }

                var output = result.Params.Param.Value.Struct.Member;

                if (output != null)
                {
                    foreach (var item in output.Where(a => a.Value != null))
                    {
                        var name = item.Name;
                        var value = item.Value;

                        #region String

                        if (value.String != null)
                        {
                            var orgValue = GetObjectValue(value);
                            var info = balance.GetType().GetProperty(name);
                            if (info != null)
                                info.SetValue(balance, Convert.ChangeType(orgValue, info.PropertyType, null));
                        }

                        #endregion

                        #region Int

                        if (value.I4 != null)
                        {
                            var orgValue = GetObjectValue(value);
                            var info = balance.GetType().GetProperty(name);
                            if (info != null)
                                info.SetValue(balance, Convert.ChangeType(orgValue, info.PropertyType, null));
                        }

                        #endregion

                        #region Datetime

                        if (value.DateTime != null)
                        {
                            var orgValue = GetObjectValue(value);
                            var info = balance.GetType().GetProperty(name);
                            if (info != null)
                                info.SetValue(balance, Convert.ChangeType(orgValue, info.PropertyType, null));
                        }

                        #endregion

                        #region Boolean

                        if (value.Boolean != null)
                        {
                            var orgValue = GetObjectValue(value);
                            var info = balance.GetType().GetProperty(name);
                            if (info != null)
                                info.SetValue(balance, Convert.ChangeType(orgValue, info.PropertyType, null));
                        }

                        #endregion

                        #region Array

                        if (value.Array != null)
                        {
                            var orgValue = value.Array;

                            if (name == "dedicatedAccountInformation")
                            {
                                var info = balance.GetType().GetProperty(name);
                                var dedicatedAccountInformationList = new List<AccountInformation>();
                                foreach (var accountInfo in value.Array.Data.Value)
                                {
                                    var dedicatedAccountInformation = new AccountInformation();
                                    foreach (var account in accountInfo.Struct.Member)
                                    {
                                        var iName = account.Name;
                                        var iValue = GetObjectValue(account.Value);

                                        var innerInfo = dedicatedAccountInformation.GetType().GetProperty(iName);
                                        if (innerInfo != null)
                                            innerInfo.SetValue(dedicatedAccountInformation,
                                                Convert.ChangeType(iValue, innerInfo.PropertyType, null));
                                    }

                                    dedicatedAccountInformationList.Add(dedicatedAccountInformation);
                                }

                                info.SetValue(balance,
                                    Convert.ChangeType(dedicatedAccountInformationList, info.PropertyType, null));
                            }

                            if (name == "offerInformationList")
                            {
                                var info = balance.GetType().GetProperty(name);
                                var offerInformationList = new List<OfferInformation>();
                                foreach (var accountInfo in value.Array.Data.Value)
                                {
                                    var offerInformation = new OfferInformation();
                                    foreach (var account in accountInfo.Struct.Member)
                                    {
                                        var iName = account.Name;
                                        var iValue = GetObjectValue(account.Value);

                                        var innerInfo = offerInformation.GetType().GetProperty(iName);
                                        if (innerInfo != null)
                                            innerInfo.SetValue(offerInformation,
                                                Convert.ChangeType(iValue, innerInfo.PropertyType, null));
                                    }

                                    offerInformationList.Add(offerInformation);
                                }

                                info.SetValue(balance,
                                    Convert.ChangeType(offerInformationList, info.PropertyType, null));
                            }
                        }

                        #endregion

                        #region Struct

                        if (value.Struct != null)
                        {
                            var orgValue = value.Struct;
                            var info = balance.GetType().GetProperty(name);
                            if (name == "accountFlagsAfter")
                            {
                                var accountFlagsAfter = new AccountFlag();
                                foreach (var innerItem in value.Struct.Member)
                                {
                                    var iName = innerItem.Name;
                                    var iValue = GetObjectValue(innerItem.Value);
                                    var innerInfo = accountFlagsAfter.GetType().GetProperty(iName);
                                    if (innerInfo != null)
                                        innerInfo.SetValue(accountFlagsAfter,
                                            Convert.ChangeType(iValue, innerInfo.PropertyType, null));
                                }

                                info.SetValue(balance, Convert.ChangeType(accountFlagsAfter, info.PropertyType, null));
                            }

                            if (name == "accountFlagsBefore")
                            {
                                var accountFlagsBefore = new AccountFlag();
                                foreach (var innerItem in value.Struct.Member)
                                {
                                    var iName = innerItem.Name;
                                    var iValue = GetObjectValue(innerItem.Value);
                                    var innerInfo = accountFlagsBefore.GetType().GetProperty(iName);
                                    if (innerInfo != null)
                                        innerInfo.SetValue(accountFlagsBefore,
                                            Convert.ChangeType(iValue, innerInfo.PropertyType, null));
                                }

                                info.SetValue(balance, Convert.ChangeType(accountFlagsBefore, info.PropertyType, null));
                            }
                        }

                        #endregion
                    }

                    var res = balance;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return balance;
        }

        private string GetObjectValue(Value value)
        {
            var res = "";
            try
            {
                if (value.Boolean != null)
                {
                    if (value.Boolean == "1")
                        return "true";
                    return "false";
                }

                if (value.String != null) return value.String;
                if (value.I4 != null) return value.I4;
                if (value.DateTime != null) return value.DateTime;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return res;
        }

        #endregion

        #region Update Offer

        public RetMessage UpdateOffer(string msisdn, string offerId, string startDate = "", string endDate = "")
        {
            var message = new RetMessage();
            try
            {
                if (string.IsNullOrEmpty(startDate))
                    startDate = "20121023T20:00:00+0800";
                if (string.IsNullOrEmpty(endDate))
                    endDate = "20221023T20:00:00+0800";

                //string url = ConfigurationManager.AppSettings["XMLRpcBaseURL"];
                //var userName = ConfigurationManager.AppSettings["XMLRpcUserName"];
                //var password = ConfigurationManager.AppSettings["XMLRpcPassword"];

                var url = AppSettings.OpenCodeBaseURL;
                var userName = AppSettings.OpenCodeUserName;
                var password = AppSettings.OpenCodePassword;

                var content =
                    "<?xml version=\"1.0\"?><methodCall> <methodName>UpdateOffer</methodName> <params> <param> <value> <struct> <member> <name>originNodeType</name> <value><string>EXT</string></value> </member> <member> <name>originHostName</name> <value><string>TEST</string></value> </member> <member> <name>originTransactionID</name> <value><string>0002010100700000002</string></value> </member> <member> <name>originTimeStamp</name> <value><dateTime.iso8601>20121022T16:40:00+0700</dateTime.iso8601></value> </member> <member> <name>subscriberNumber</name> <value><string>" +
                    msisdn + "</string></value> </member> <member> <name>offerID</name> <value><i4>" + offerId +
                    "</i4></value> </member> <member> <name>startDate</name> <value><dateTime.iso8601>" + startDate +
                    "</dateTime.iso8601></value> </member> <member> <name>expiryDate</name> <value><dateTime.iso8601>" +
                    endDate +
                    "</dateTime.iso8601></value> </member> <member> <name>subscriberNumberNAI</name> <value><i4>1</i4></value> </member> </struct> </value> </param> </params> </methodCall>";
                var requestData = Encoding.ASCII.GetBytes(content);
                var request = (HttpWebRequest) WebRequest.Create(url);
                request.Credentials = new NetworkCredential(userName, password);
                //request.Headers.Add("Authorization", "Basic ZmRzdXNlcjpmZHN1c2VyCg==");
                request.Method = "POST";
                request.UserAgent = "UGw Server/4.3/1.0";
                request.ContentType = "text/xml";
                request.Accept = "text/xml";
                request.Timeout = 60000;
                request.ContentLength = requestData.Length;
                request.ServicePoint.ConnectionLimit = 1;
                request.KeepAlive = false;
                var result = "";


                using (var requestStream = request.GetRequestStream())
                {
                    requestStream.Write(requestData, 0, requestData.Length);
                    using (var response = (HttpWebResponse) request.GetResponse())
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            using (var reader = new StreamReader(stream, Encoding.ASCII))
                            {
                                result = reader.ReadToEnd();
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(result))
                {
                    var obj = DeserializeUpdateOfferResponse(result);

                    if (obj != null)
                    {
                        if (obj.responseCode == "0")
                        {
                            message.Data = obj;
                        }
                        else
                        {
                            message.Message = "Response Code :" + obj.responseCode;
                            message.MessageType = MessageType.None;
                            if (!string.IsNullOrEmpty(obj.faultCode))
                                message.Message = "Fault occur. Fault Code :" + obj.faultCode + ", fault String : " +
                                                  obj.faultString;
                        }
                    }
                }
            }
            catch (WebException webex)
            {
                var resp = "";
                var errResp = webex.Response;
                using (var respStream = errResp.GetResponseStream())
                {
                    var reader = new StreamReader(respStream);
                    resp = reader.ReadToEnd();
                }

                message.Message = resp;
                message.MessageType = MessageType.Error;
            }
            catch (Exception ex)
            {
                var a = ex.Message;
                message.Message = ex.Message;
                message.MessageType = MessageType.Error;
            }

            return message;
        }

        private UpdateOfferInfo DeserializeUpdateOfferResponse(string xml)
        {
            var offerInfo = new UpdateOfferInfo();
            try
            {
                var result = new MethodResponse();
                var result2 = new FaultResponse();
                var output = new List<Member>();

                if (!xml.Contains("faultCode"))
                {
                    var serializer = new XmlSerializer(typeof(MethodResponse));
                    using (TextReader reader = new StringReader(xml))
                    {
                        result = (MethodResponse) serializer.Deserialize(reader);
                    }
                }
                else
                {
                    var serializer = new XmlSerializer(typeof(FaultResponse));
                    using (TextReader reader = new StringReader(xml))
                    {
                        result2 = (FaultResponse) serializer.Deserialize(reader);
                    }
                }

                if (!xml.Contains("faultCode"))
                    output = result.Params.Param.Value.Struct.Member;
                else
                    output = result2.Fault.Value.Struct.Member;


                if (output != null)
                {
                    foreach (var item in output.Where(a => a.Value != null))
                    {
                        var name = item.Name;
                        var value = item.Value;

                        #region String

                        if (value.String != null)
                        {
                            var orgValue = GetObjectValue(value);
                            var info = offerInfo.GetType().GetProperty(name);
                            if (info != null)
                                info.SetValue(offerInfo, Convert.ChangeType(orgValue, info.PropertyType, null));
                        }

                        #endregion

                        #region Int

                        if (value.I4 != null)
                        {
                            var orgValue = GetObjectValue(value);
                            var info = offerInfo.GetType().GetProperty(name);
                            if (info != null)
                                info.SetValue(offerInfo, Convert.ChangeType(orgValue, info.PropertyType, null));
                        }

                        #endregion

                        #region Datetime

                        if (value.DateTime != null)
                        {
                            var orgValue = GetObjectValue(value);
                            var info = offerInfo.GetType().GetProperty(name);
                            if (info != null)
                                info.SetValue(offerInfo, Convert.ChangeType(orgValue, info.PropertyType, null));
                        }

                        #endregion

                        #region Boolean

                        if (value.Boolean != null)
                        {
                            var orgValue = GetObjectValue(value);
                            var info = offerInfo.GetType().GetProperty(name);
                            if (info != null)
                                info.SetValue(offerInfo, Convert.ChangeType(orgValue, info.PropertyType, null));
                        }

                        #endregion
                    }

                    var res = offerInfo;
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }

            return offerInfo;
        }

        private UpdateOfferInfo DeserializeUpdateOfferResponseFault(string xml)
        {
            var balance = new UpdateOfferInfo();
            try
            {
                var serializer = new XmlSerializer(typeof(FaultResponse));
                FaultResponse result;
                using (TextReader reader = new StringReader(xml))
                {
                    result = (FaultResponse) serializer.Deserialize(reader);
                }

                var output = result.Fault.Value.Struct.Member;

                if (output != null)
                {
                    foreach (var item in output.Where(a => a.Value != null))
                    {
                        var name = item.Name;
                        var value = item.Value;

                        #region String

                        if (value.String != null)
                        {
                            var orgValue = GetObjectValue(value);
                            var info = balance.GetType().GetProperty(name);
                            if (info != null)
                                info.SetValue(balance, Convert.ChangeType(orgValue, info.PropertyType, null));
                        }

                        #endregion

                        #region Int

                        if (value.I4 != null)
                        {
                            var orgValue = GetObjectValue(value);
                            var info = balance.GetType().GetProperty(name);
                            if (info != null)
                                info.SetValue(balance, Convert.ChangeType(orgValue, info.PropertyType, null));
                        }

                        #endregion

                        #region Datetime

                        if (value.DateTime != null)
                        {
                            var orgValue = GetObjectValue(value);
                            var info = balance.GetType().GetProperty(name);
                            if (info != null)
                                info.SetValue(balance, Convert.ChangeType(orgValue, info.PropertyType, null));
                        }

                        #endregion

                        #region Boolean

                        if (value.Boolean != null)
                        {
                            var orgValue = GetObjectValue(value);
                            var info = balance.GetType().GetProperty(name);
                            if (info != null)
                                info.SetValue(balance, Convert.ChangeType(orgValue, info.PropertyType, null));
                        }

                        #endregion
                    }

                    var res = balance;
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }

            return balance;
        }

        #endregion
    }
}