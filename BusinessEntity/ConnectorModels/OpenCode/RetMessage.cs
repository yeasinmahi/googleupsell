﻿namespace BusinessEntity.ConnectorModels.OpenCode
{
    public enum MessageType
    {
        None = 0,
        Success = 1,
        Error = 2,
        Warning = 3
    }

    public class RetMessage
    {
        public RetMessage()
        {
            MessageType = MessageType.Success;
        }

        public MessageType MessageType { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }

    public class SetMessage
    {
        public RetMessage SetSuccessMessage(string message = "Success !", object data = null)
        {
            return new RetMessage {MessageType = MessageType.Success, Message = message, Data = data};
        }

        public RetMessage SetErrorMessage(string message = "Error !", object data = null)
        {
            return new RetMessage {MessageType = MessageType.Error, Message = message, Data = data};
        }

        public RetMessage SetWarningMessage(string message = "Warning !", object data = null)
        {
            return new RetMessage {MessageType = MessageType.Warning, Message = message, Data = data};
        }
    }
}