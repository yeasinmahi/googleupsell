﻿using System;

namespace BusinessEntity.ConnectorModels.OpenCode
{
    public class LoanInfo
    {
        public bool activationStatusFlag { get; set; }
    }

    public class RequestInfo
    {
        public string originNodeType { get; set; }
        public string originHostName { get; set; }
        public string originTransactionID { get; set; }
        public DateTime originTimeStamp { get; set; }
        public string subscriberNumber { get; set; }
        public int subscriberNumberNAI { get; set; }

        public dedicatedAccountSelection[] dedicatedAccountSelection { get; set; }
    }

    public class dedicatedAccountSelection
    {
        public int dedicatedAccountIDFirst { get; set; }
        public int dedicatedAccountIDLast { get; set; }
    }
}