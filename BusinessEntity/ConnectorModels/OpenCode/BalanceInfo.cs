﻿using System;
using System.Collections.Generic;

namespace BusinessEntity.ConnectorModels.OpenCode
{
    public class AccountFlag
    {
        public bool activationStatusFlag { get; set; }
        public bool negativeBarringStatusFlag { get; set; }
        public bool serviceFeePeriodExpiryFlag { get; set; }
        public bool serviceFeePeriodWarningActiveFlag { get; set; }
        public bool supervisionPeriodExpiryFlag { get; set; }
        public bool supervisionPeriodWarningActiveFlag { get; set; }
    }

    public class AccountInformation
    {
        public string dedicatedAccountActiveValue1 { get; set; }
        public int dedicatedAccountID { get; set; }
        public int dedicatedAccountUnitType { get; set; }
        public string dedicatedAccountValue1 { get; set; }
        public string expiryDate { get; set; }
        public string startDate { get; set; }

        public double dedicatedAccountItemValue
        {
            get
            {
                double value = 0;
                try
                {
                    if (!string.IsNullOrEmpty(dedicatedAccountValue1))
                    {
                        if (dedicatedAccountValue1.Length > 18)
                        {
                            var newValue = dedicatedAccountValue1.Substring(0, 18);
                            value = Convert.ToDouble(newValue) / 100;
                            //need to write log
                        }
                        else
                        {
                            value = Convert.ToDouble(dedicatedAccountValue1) / 100;
                        }
                    }
                }
                catch (Exception)
                {
                    //need to write log
                }

                return value;
            }
        }
    }

    public class OfferInformation
    {
        public string expiryDate { get; set; }
        public int offerID { get; set; }
        public int offerType { get; set; }
        public string startDate { get; set; }
        public string expiryDateTime { get; set; }
    }


    public class UpdateOfferInfo : OfferInformation
    {
        public string responseCode { get; set; }
        public string originTransactionID { get; set; }

        public string faultCode { get; set; }
        public string faultString { get; set; }
    }


    /// <summary>
    ///     For generic loan we have to check dedicatedAccountInformation with dedicatedAccountID is 255,
    ///     dedicatedAccountValue1 will contains the balance information. If dedicatedAccountValue1 is > 0 that means user has
    ///     loan amount.
    ///     For data loan we have to check dedicatedAccountInformation with dedicatedAccountID is 245,
    ///     dedicatedAccountValue1 will contains the balance information. If dedicatedAccountValue1 is > 0 that means user has
    ///     loan amount.
    ///     Amount must be devide by 100
    /// </summary>
    public class BalanceInfo
    {
        public AccountFlag accountFlagsAfter { get; set; }
        public AccountFlag accountFlagsBefore { get; set; }
        public string accountValue1 { get; set; }
        public string creditClearanceDate { get; set; }
        public string currency1 { get; set; }
        public List<AccountInformation> dedicatedAccountInformation { get; set; }
        public int languageIDCurrent { get; set; }
        public List<OfferInformation> offerInformationList { get; set; }

        public string originTransactionID { get; set; }

        public string responseCode { get; set; }

        public string serviceClassCurrent { get; set; }

        public string serviceFeeExpiryDate { get; set; }

        public string serviceRemovalDate { get; set; }

        public string supervisionExpiryDate { get; set; }
    }
}