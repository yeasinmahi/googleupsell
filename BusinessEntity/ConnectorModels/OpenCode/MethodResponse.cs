﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BusinessEntity.ConnectorModels.OpenCode
{
    [XmlRoot(ElementName = "value")]
    public class Value
    {
        [XmlElement(ElementName = "boolean")] public string Boolean { get; set; }

        [XmlElement(ElementName = "struct")] public Struct Struct { get; set; }

        [XmlElement(ElementName = "string")] public string String { get; set; }

        [XmlElement(ElementName = "dateTime.iso8601")]
        public string DateTime { get; set; }

        [XmlElement(ElementName = "array")] public Array Array { get; set; }

        [XmlElement(ElementName = "i4")] public string I4 { get; set; }
    }

    [XmlRoot(ElementName = "member")]
    public class Member
    {
        [XmlElement(ElementName = "name")] public string Name { get; set; }

        [XmlElement(ElementName = "value")] public Value Value { get; set; }
    }

    [XmlRoot(ElementName = "struct")]
    public class Struct
    {
        [XmlElement(ElementName = "member")] public List<Member> Member { get; set; }
    }

    [XmlRoot(ElementName = "data")]
    public class Data
    {
        [XmlElement(ElementName = "value")] public List<Value> Value { get; set; }
    }

    [XmlRoot(ElementName = "array")]
    public class Array
    {
        [XmlElement(ElementName = "data")] public Data Data { get; set; }
    }

    [XmlRoot(ElementName = "param")]
    public class Param
    {
        [XmlElement(ElementName = "value")] public Value Value { get; set; }
    }

    [XmlRoot(ElementName = "params")]
    public class Params
    {
        [XmlElement(ElementName = "param")] public Param Param { get; set; }
    }

    [XmlRoot(ElementName = "methodResponse")]
    public class MethodResponse
    {
        [XmlElement(ElementName = "params")] public Params Params { get; set; }
    }

    [XmlRoot(ElementName = "fault")]
    public class Fault
    {
        [XmlElement(ElementName = "value")] public Value Value { get; set; }
    }

    [XmlRoot(ElementName = "methodResponse")]
    public class FaultResponse
    {
        [XmlElement(ElementName = "fault")] public Fault Fault { get; set; }
    }
}