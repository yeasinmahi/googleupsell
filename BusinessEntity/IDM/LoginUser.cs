﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BusinessEntity.IDM
{
    [Serializable]
    public class LoginUser
    {
        //[Required, MinLength(5), MaxLength(50)]
        public string grant_type { get; set; }

        //[Required, MinLength(5), MaxLength(50)]
        public string client_id { get; set; }

        //[Required, MinLength(5), MaxLength(200)]
        public string client_secret { get; set; }
        public string scope { get; set; }

        [MaxLength(300)] [MinLength(0)] public string MSISDN { get; set; }

        [MaxLength(300)]
        [MinLength(0)]
        //    [DataType(DataType.Password)]
        public string CustomerPassword { get; set; }

        public string OTP { get; set; }


        public string RefreshToken { get; set; }

        public string RedirectURL { get; set; }
    }
}