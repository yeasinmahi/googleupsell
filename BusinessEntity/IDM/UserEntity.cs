﻿using System.ComponentModel;

namespace BusinessEntity.IDM
{
    public class UserEntity
    {
    }

    public enum ValidityReason
    {
        [Description("ForgetPassword")] ForgetPassword = 1,
        [Description("Registration")] Registration = 2,
        [Description("OneTimeLogin")] OneTimeLogin = 3,

        [Description("OneTimeActivity")] // For data pack purchase or any other things
        OneTimeActivity = 4
    }


    public class SendOTPRequest
    {
        public string client_id { get; set; }
        public string client_secret { get; set; }
        public ValidityReason ValidFor { get; set; }
        public bool AllOperator { get; set; }
        public string MSISDN { get; set; }
    }

    public class IsValidOTPRequest
    {
        public ValidityReason ValidFor { get; set; }

        //public bool IsOtherOperator { get; set; }
        public string MSISDN { get; set; }
        public string OTP { get; set; }
    }
}