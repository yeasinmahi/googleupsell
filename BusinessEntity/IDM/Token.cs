﻿using System;
using Newtonsoft.Json;

namespace BusinessEntity.IDM
{
    public class Token
    {
        //public string UserId { get; set; }
        public string token_type { get; set; } = "Bearer";

        [JsonProperty("refresh_token")] public string refresh_token { get; set; }
        //public string OldRefreshToken { get; set; }

        [JsonProperty("access_token")] public string access_token { get; set; }
        //   public bool CustomerToken { get; set; }

        //[JsonProperty("provider")]
        //public string provider { get; set; }
        [JsonProperty("expires_in")] public long expires_in { get; set; }

        //[JsonProperty("access_token_validity")]
        //public DateTime access_token_validity { get; set; }
        //[JsonProperty("refresh_token_validity")]
        //public DateTime refresh_token_validity { get; set; } = DateTime.UtcNow.AddDays(30);

        public DateTime GetOn { get; set; } = DateTime.Now;
        public DateTime ValidTill { get; set; } = DateTime.Now;

        public bool IsValid
        {
            get
            {
                if (ValidTill < DateTime.Now.AddMinutes(-1))
                    return true;
                return false;
            }
        }
    }
}