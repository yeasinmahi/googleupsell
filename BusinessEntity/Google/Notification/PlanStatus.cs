﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;

namespace BusinessEntity.Google.Notification
{

    public class PlanStatus
    {
        //[JsonProperty("name")] public string Name { get; set; }

        [JsonProperty("plans")] public List<Plan> Plans { get; set; }

        //[JsonProperty("languageCode")] public string LanguageCode { get; set; }

        [JsonProperty("expireTime")] 
        public DateTimeOffset ExpireTime { get; set; } = DateTimeOffset.UtcNow.AddDays(5);

        [JsonProperty("updateTime")] 
        public DateTimeOffset UpdateTime { get; set; } = DateTimeOffset.UtcNow.AddDays(-5);

        //[JsonProperty("title")] public string Title { get; set; }

        //[JsonProperty("subscriberId")] public string SubscriberId { get; set; }

        // [JsonProperty("accountInfo")]
        //public AccountInfo AccountInfo { get; set; }

        [JsonProperty("uiCompatibility")]
        public string UiCompatibility { get; set; } = UiCompatibilityEnum.UI_INCOMPATIBLE.ToString("F");

        //  [JsonProperty("planInfoPerClient")]
        //  public PlanInfoPerClient PlanInfoPerClient { get; set; }

        // public BEBaseList<string> notifications { get; set; }
        // //public PlanInfoPerClient planInfoPerClient { get; set; }
        //public string CpidState { get; set; }
    }
    public class PlanStatusResponse : PlanStatus
    {
        [JsonProperty("error")] public Error Error { get; set; }
    }
    public class Error
    {
        [JsonProperty("code")] public string Code { get; set; }
        [JsonProperty("message")] public string Message { get; set; }
        [JsonProperty("status")] public string Status { get; set; }
    }
    public class Plan
    {
        public Plan()
        {
        }

        //public Plan(string planName, string planID, string PlanCategory, DateTime expirationTime,
        //    List<PlanModule> planModules)
        //{
        //    PlanName = planName;
        //    PlanId = planID;
        //    this.PlanCategory = PlanCategory;
        //    ExpirationTime = expirationTime;
        //    PlanModules = planModules;
        //}

        [JsonProperty("planName")] public string PlanName { get; set; }

        //[JsonProperty("planId")] public string PlanId { get; set; }


        //[JsonProperty("planCategory")] public string PlanCategory { get; set; }

        //[JsonProperty("expirationTime")] public DateTime ExpirationTime { get; set; }

        [JsonProperty("planModules")] public List<PlanModule> PlanModules { get; set; }

        //public string planState { get; set; }
    }

    public enum PlanCategory
    {
        PLAN_CATEGORY_UNSPECIFIED,
        PREPAID,
        POSTPAID
    }

    public class PlanModule
    {
        public PlanModule()
        {
        }

        //public PlanModule(string moduleNmae, DateTimeOffset expirationTime, string overUsagePolicy,
        //    string maxRateKbps, string description, string coarseBalanceLevel)
        //{
        //    ModuleName = moduleNmae;
        //    ExpirationTime = expirationTime;
        //    OverUsagePolicy = overUsagePolicy;
        //    MaxRateKbps = maxRateKbps;
        //    Description = description;
        //    this.coarseBalanceLevel = coarseBalanceLevel;
        //}

        [JsonProperty("coarseBalanceLevel")] public string coarseBalanceLevel { get; set; }

        //[JsonProperty("trafficCategories")] public string[] TrafficCategories { get; set; }

        [JsonProperty("expirationTime")] public DateTimeOffset ExpirationTime { get; set; }

        //[JsonProperty("overUsagePolicy")] public string OverUsagePolicy { get; set; }

        //[JsonProperty("maxRateKbps")] public string MaxRateKbps { get; set; }

        //[JsonProperty("description")] public string Description { get; set; }

        //[JsonProperty("moduleName")] public string ModuleName { get; set; }

        //[JsonProperty("usedBytes")] public string UsedBytes { get; set; }

        [JsonProperty("planModuleState")] public string planModuleState { get; set; }


        //[JsonProperty("refreshPeriod")] public string refreshPeriod { get; set; }

        public ByteQuota byteBalance { get; set; }

        // public TimeQuota timeBalance { get; set; }
    }

    public class ByteQuota
    {
        public string quotaBytes { get; set; }
        public string remainingBytes { get; set; }
    }

    public class TimeQuota
    {
        public string quotaMinutes { get; set; }
        public string remainingMinutes { get; set; }
    }

    public enum BalanceLevel
    {
        BALANCE_LEVEL_UNSPECIFIED,
        NO_PLAN,
        OUT_OF_DATA,
        LOW_QUOTA,
        HIGH_QUOTA
    }

    public enum PlanModuleTrafficCategory
    {
        PLAN_MODULE_TRAFFIC_CATEGORY_UNSPECIFIED,
        GENERIC,
        VIDEO,
        VIDEO_BROWSING,
        VIDEO_OFFLINE,
        MUSIC,
        GAMING,
        SOCIAL,
        MESSAGING,
        APP_STORE
    }

    public enum OverUsagePolicy
    {
        OVER_USAGE_POLICY_UNSPECIFIED,
        THROTTLED,
        BLOCKED,
        PAY_AS_YOU_GO
    }

    public enum PlanState
    {
        ACTIVE,
        INACTIVE,
        EXPIRING_SOON,
        NEWLY_ACTIVE,
        EXPIRED
    }

    public enum RefreshPeriod
    {
        REFRESH_PERIOD_NONE,
        DAILY,
        MONTHLY,
        BIWEEKLY,
        WEEKLY
    }

    public class AccountInfo
    {
        [JsonProperty("accountBalance")] public Money AccountBalance { get; set; }

        [JsonProperty("loanBalance")] public Money LoanBalance { get; set; }

        [JsonProperty("unpaidLoan")] public Money UnpaidLoan { get; set; }

        [JsonProperty("accountBalanceStatus")] public AccountBalanceStatus AccountBalanceStatus { get; set; }

        [JsonProperty("validUntil")] public string ValidUntil { get; set; }

        [JsonProperty("payAsYouGoCharge")] public Money PayAsYouGoCharge { get; set; }

        [JsonProperty("accountTopUp")] public Money AccountTopUp { get; set; }
    }

    public enum AccountBalanceStatus
    {
        VALID,
        INVALID
    }

    public class Money
    {
        [JsonProperty("currencyCode")] public string CurrencyCode { get; set; }

        [JsonProperty("units")] public string Units { get; set; }

        [JsonProperty("nanos")] public long Nanos { get; set; }
    }

    public enum UiCompatibilityEnum
    {
        UI_COMPATIBILITY_UNSPECIFIED,
        UI_COMPATIBLE,
        UI_INCOMPATIBLE
    }

    public enum NotificationType
    {
        NOTIFICATION_UNDEFINED,
        NOTIFICATION_LOW_BALANCE_WARNING,
        NOTIFICATION_DATA_EXPIRATION_WARNING,
        NOTIFICATION_OUT_OF_DATA,
        NOTIFICATION_PLAN_ACTIVATION,
        NOTIFICATION_PAY_AS_YOU_GO,
        NOTIFICATION_ACCOUNT_TOP_UP,
        NOTIFICATION_DATA_EXPIRED
    }

    public class PlanInfoPerClient
    {
        public PlanInfoPerClient(Youtube youtube)
        {
            Youtube = youtube;
        }

        [JsonProperty("youtube")] public Youtube Youtube { get; set; }
    }

    public class Youtube
    {
        public Youtube(RateLimitedStreaming rls)
        {
            RateLimitedStreaming = rls;
        }

        [JsonProperty("rateLimitedStreaming")] public RateLimitedStreaming RateLimitedStreaming { get; set; }
    }

    public class RateLimitedStreaming
    {
        public RateLimitedStreaming(long kbps)
        {
            MaxMediaRateKbps = kbps;
        }

        [JsonProperty("maxMediaRateKbps")] public long MaxMediaRateKbps { get; set; }
    }

    public enum CpidState
    {
        [Description("String 1")] CPID_STATE_UNSPECIFIED,

        [Description("String 4")] CPID_INVALIDATED
    }
}