﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace BusinessEntity.Google.Notification
{
    public class PushNotification
    {
        [JsonProperty("id")] public string Id { get; set; }

        [JsonProperty("kafkaId")] public string KafkaId { get; set; }

        [JsonProperty("triggerId")] public string TriggerId { get; set; }

        //[JsonProperty("triggerDescription")]
        // public string TriggerDescription { get; set; }

        [JsonProperty("aggregateId")] public string AggregateId { get; set; }

        // [JsonProperty("incomingTime")]
        //  public string IncomingTime { get; set; }

        [JsonProperty("notificationTime")] public string NotificationTime { get; set; }

        //[JsonProperty("IsSuccess")] public int IsSuccess { get; set; }

        [JsonProperty("MSISDN")] public string Msisdn { get; set; }

        [JsonProperty("TRIGGER_ATTR_02")] public string TRIGGER_ATTR_02 { get; set; }

        [JsonProperty("TRIGGER_ATTR_01")] public string TRIGGER_ATTR_01 { get; set; }

        [JsonIgnore] public int IsProcessed { get; set; }

        //[JsonProperty("isProcessed1")] public int IsProcessed1 { get; set; }

        [JsonProperty("ProcessDate")] public DateTime ProcessDate { get; set; }

        [JsonProperty("BookID")] public string BookID { get; set; }

        [JsonProperty("BookDate")] public DateTime BookDate { get; set; }
        [Display(Name = "TRIGGER_ATTR_03")] public string TRIGGER_ATTR_03 { get; set; }

        [Display(Name = "TRIGGER_ATTR_04")] public string TRIGGER_ATTR_04 { get; set; }
        [Display(Name = "TRIGGER_ATTR_05")] public string TRIGGER_ATTR_05 { get; set; }

        [Display(Name = "TRIGGER_ATTR_06")] public string TRIGGER_ATTR_06 { get; set; }

        [Display(Name = "TRIGGER_ATTR_07")] public string TRIGGER_ATTR_07 { get; set; }

        [Display(Name = "TRIGGER_ATTR_08")] public string TRIGGER_ATTR_08 { get; set; }

        [Display(Name = "TRIGGER_ATTR_09")] public string TRIGGER_ATTR_09 { get; set; }

        [Display(Name = "TRIGGER_ATTR_10")] public string TRIGGER_ATTR_10 { get; set; }

        [Display(Name = "TRIGGER_ATTR_11")] public string TRIGGER_ATTR_11 { get; set; }

        [Display(Name = "TRIGGER_ATTR_12")] public string TRIGGER_ATTR_12 { get; set; }

        [Display(Name = "TRIGGER_ATTR_13")] public string TRIGGER_ATTR_13 { get; set; }

        [Display(Name = "TRIGGER_ATTR_14")] public string TRIGGER_ATTR_14 { get; set; }

        [Display(Name = "TRIGGER_ATTR_15")] public string TRIGGER_ATTR_15 { get; set; }

        [Display(Name = "TRIGGER_ATTR_16")] public string TRIGGER_ATTR_16 { get; set; }

        [Display(Name = "TRIGGER_ATTR_17")] public string TRIGGER_ATTR_17 { get; set; }

        [Display(Name = "TRIGGER_ATTR_18")] public string TRIGGER_ATTR_18 { get; set; }

        [Display(Name = "TRIGGER_ATTR_19")] public string TRIGGER_ATTR_19 { get; set; }

        [Display(Name = "TRIGGER_ATTR_20")] public string TRIGGER_ATTR_20 { get; set; }

        public string ErrorMessage { get; set; }

        public string CPID { get; set; }
        public string PackName { get; set; }
        public string PackCode { get; set; }
        public double PackQuta { get; set; }
        public string PackUnit { get; set; }
        public string PackType { get; set; }

    }

    #region Object of BEkafkanotification

    public class PushNotificationa
    {
        #region Destructor

        ~PushNotificationa()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Property

        [Display(Name = "id")] public int id { get; set; }

        [Display(Name = "triggerid")] public string triggerid { get; set; }

        [Display(Name = "aggregateid")] public string aggregateid { get; set; }

        [Display(Name = "incomingtime")] public DateTime incomingtime { get; set; }

        [Display(Name = "notificationtime")] public DateTime notificationtime { get; set; }

        [Display(Name = "isprocessed")] public int isprocessed { get; set; }

        [Display(Name = "issuccess")] public int issuccess { get; set; }

        [Display(Name = "msisdn")] public string msisdn { get; set; }

        [Display(Name = "trigger_attr_01")] public string trigger_attr_01 { get; set; }

        [Display(Name = "trigger_attr_02")] public string trigger_attr_02 { get; set; }

        [Display(Name = "deleverystatus")] public string deleverystatus { get; set; }

        [Display(Name = "deleverytime")] public DateTime deleverytime { get; set; }

        #endregion
    }

    #endregion

    #region Collectin Object of BEkafkanotification

    public class PushNotifications : List<PushNotification>
    {
        #region Destructor

        ~PushNotifications()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Functions

        #endregion
    }

    #endregion
}