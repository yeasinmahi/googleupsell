﻿using Newtonsoft.Json;

namespace BusinessEntity.Google
{
    public enum DpaStatusEnum
    {
        UNKNOWN = 0,
        OPERATIONAL = 1,
        UNAVAILABLE = 2
    }

    public class DpaStatus
    {
        [JsonProperty("status")] public DpaStatusEnum Status { get; set; }

        [JsonProperty("message")] public string Message { get; set; }
    }
}