﻿using System;
using System.Collections.Generic;

namespace BusinessEntity.Google.DPA
{
    public class Cost
    {
        public string currencyCode { get; set; } = "BDT";
        public string units { get; set; }
        public int nanos { get; set; }
    }

    public class Offer
    {
        public Offer(string planName, string planId, string planDescription,
            string promoMessage, string languageCode, string overusagePolicy, Cost cost, string duration,
            string offerContext, List<string> trafficCategories, string quotaBytes)
        {
            this.planName = planName;
            this.planId = planId;
            this.planDescription = planDescription;
            this.promoMessage = promoMessage;
            this.languageCode = languageCode;
            this.overusagePolicy = overusagePolicy;
            this.cost = cost;
            this.duration = duration;
            this.offerContext = offerContext;
            this.trafficCategories = trafficCategories;
            this.quotaBytes = quotaBytes;
        }

        public string planName { get; set; }
        public string planId { get; set; }
        public string planDescription { get; set; }
        public string promoMessage { get; set; }
        public string languageCode { get; set; }
        public string overusagePolicy { get; set; }
        public Cost cost { get; set; }
        public string duration { get; set; }
        public string offerContext { get; set; }
        public List<string> trafficCategories { get; set; }
        public string quotaBytes { get; set; }
    }

    public class PlanOffer
    {
        public List<Offer> offers { get; set; }
        public DateTime expireTime { get; set; }
    }

    //    {
    //	"offers": [{
    //		"planName": "ACME Red",
    //		"planId": "turbulent1",
    //		"planDescription": "Unlimited Videos for 30 days.",
    //		"promoMessage": "Binge watch videos.",
    //		"languageCode": "en_US",
    //		"overusagePolicy": "BLOCKED",
    //		"cost": {
    //			"currencyCode": "INR",
    //			"units": "300",
    //			"nanos": 0
    //		},
    //		"duration": "2592000s",
    //		"offerContext": "YouTube",
    //		"trafficCategories": ["VIDEO"],
    //		"quotaBytes": "9223372036850"
    //	}],
    //	"expireTime": "2019-03-04T00:06:07Z"
    //}
}