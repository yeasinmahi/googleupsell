﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BusinessEntity.Google.DPA
{
    //  {
    //  "transactionStatus": "SUCCESS",

    //  "purchase": {
    //    "planId": string,               // copied from request. (req.)
    //    "transactionId": string,        // copied from request. (req.)
    //    "transactionMessage": string,   // status message. (opt.)
    //    "confirmationCode": string,     // DPA-generated confirmation code
    //                                    // for successful transaction. (opt.)
    //    "planActivationTime" : string,  // Time when plan will be activated,
    //                                    // in timestamp format. (opt.)
    //  },

    //  // walletInfo is populated with the balance left in the user's account.
    //  "walletBalance": {
    //    "currencyCode": string,       // 3-letter currency code defined in ISO 4217.
    //    "units": string,              // Whole units of the currency amount.
    //    "nanos": number               // Number of nano units of the amount.
    //  }
    //}
    public static class transactionStatusEnum
    {
        public static string success = "SUCCESS";
        public static string error = "ERROR";
    }


    public class Purchase
    {
        public string planId { get; set; }
        public string transactionId { get; set; }
        public string transactionMessage { get; set; }
        public string confirmationCode { get; set; }
        public DateTime planActivationTime { get; set; }
    }

    public class WalletBalance
    {
        public string currencyCode { get; set; }
        public string units { get; set; }
        public int nanos { get; set; }
    }

    public class PurchasePlan
    {
        public PurchasePlan()
        {
            purchase = new Purchase();
            walletBalance = new WalletBalance();
            transactionStatus = transactionStatusEnum.success;
        }

        public string transactionStatus { get; set; }
        public Purchase purchase { get; set; }
        public WalletBalance walletBalance { get; set; }
    }

//    {
//	"planId": "string",
//	"transactionId": "string",
//	"offerContext": "string",
//	"callbackUrl": "string"

//}
//{
//  "planId": string,         // Id of plan to be purchased. Copied from
//                            // offers.planId field returned from a
//                            // Upsell Offer request,
//                            // if available. (req.).
//  "transactionId": string,  // Unique request identifier (req.)
//  "offerContext": string,   // Copied from from the
//                            // offers.offerContext, if available.
//                            // (opt.)
//  "callbackUrl": string     // URL that the DPA can call back with response once
//                            // it has handled the request.
//}
    public class BEpurchasePlanRequest
    {
        [Required] public string planId { get; set; }

        [Required] public string transactionId { get; set; }

        public string offerContext { get; set; }
        public string callbackUrl { get; set; }
    }
}