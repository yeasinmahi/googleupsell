﻿using System;
using System.Collections.Generic;

namespace BusinessEntity.Google.DPA
{
    public class Plan
    {
        public string planName { get; set; }
        public string planId { get; set; }
        public string planCategory { get; set; }

        public DateTime expirationTime { get; set; }

        public List<PlanModule> planModules { get; set; }
        //public PlanState planState { get; set; }

        // public BEBaseList<PlanModule> planModules { get; set; }
    }

    public class RateLimitedStreaming
    {
        public int maxMediaRateKbps { get; set; }
    }

    public class Youtube
    {
        public RateLimitedStreaming rateLimitedStreaming { get; set; }
    }

    public class PlanInfoPerClient
    {
        public Youtube youtube { get; set; }
    }

    public class PlanStatus
    {
        public List<Plan> plans { get; set; }
        public string languageCode { get; set; }
        public DateTime expireTime { get; set; }
        public DateTime updateTime { get; set; }

        public string title { get; set; }
        //public string subscriberId { get; set; }
        //public string accountInfo { get; set; }
        //public UiCompatibility uiCompatibility { get; set; }
        //public NotificationType notifications { get; set; }
        //public PlanInfoPerClient planInfoPerClient { get; set; }
        //public CpidState CpidState { get; set; }

        // public PlanInfoPerClient planInfoPerClient { get; set; }
    }

    public class PlanModule
    {
        public string coarseBalanceLevel { get; set; }

        //public TrafficCategories TrafficCategories { get; set; }
        public DateTime expirationTime { get; set; }

        //public OverUsagePolicy overUsagePolicy { get; set; }
        //public string maxRateKbps { get; set; }
        public string description { get; set; }
        public string moduleName { get; set; }

        public string usedBytes { get; set; }

        //public PlanState planModuleState { get; set; }
        //public RefreshPeriod refreshPeriod { get; set; }
        public ByteQuota byteBalance { get; set; }
        public TimeQuota timeBalance { get; set; }
    }

    public class ByteQuota
    {
        public string quotaBytes { get; set; }
        public string remainingBytes { get; set; }
    }

    public class TimeQuota
    {
        public string quotaMinutes { get; set; }
        public string remainingMinutes { get; set; }
    }

    public enum PlanState
    {
        ACTIVE,
        INACTIVE,
        EXPIRING_SOON,
        NEWLY_ACTIVE,
        EXPIRED
    }

    public enum RefreshPeriod
    {
        REFRESH_PERIOD_NONE,
        DAILY,
        MONTHLY,
        BIWEEKLY,
        WEEKLY
    }

    public enum OverUsagePolicy
    {
        OVER_USAGE_POLICY_UNSPECIFIED,
        THROTTLED,
        BLOCKED,
        PAY_AS_YOU_GO
    }

    public enum TrafficCategories
    {
        PLAN_MODULE_TRAFFIC_CATEGORY_UNSPECIFIED,
        GENERIC,
        VIDEO,
        VIDEO_BROWSING,
        VIDEO_OFFLINE,
        MUSIC,
        GAMING,
        SOCIAL,
        MESSAGING,
        APP_STORE
    }

    public enum BalanceLevel
    {
        BALANCE_LEVEL_UNSPECIFIED,
        NO_PLAN,
        OUT_OF_DATA,
        LOW_QUOTA,
        HIGH_QUOTA
    }

    public enum PlanCategory
    {
        PLAN_CATEGORY_UNSPECIFIED,
        PREPAID,
        POSTPAID
    }

    public enum CpidState
    {
        PLAN_CATEGORY_UNSPECIFIED,
        PREPAID,
        POSTPAID
    }

    public enum NotificationType
    {
        NOTIFICATION_UNDEFINED,
        NOTIFICATION_LOW_BALANCE_WARNING,
        NOTIFICATION_DATA_EXPIRATION_WARNING,
        NOTIFICATION_OUT_OF_DATA,
        NOTIFICATION_PLAN_ACTIVATION,
        NOTIFICATION_PAY_AS_YOU_GO,
        NOTIFICATION_ACCOUNT_TOP_UP,
        NOTIFICATION_DATA_EXPIRED
    }

    public enum UiCompatibility
    {
        UI_COMPATIBILITY_UNSPECIFIED,
        UI_COMPATIBLE,
        UI_INCOMPATIBLE
    }
}