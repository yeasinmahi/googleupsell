﻿using System.Collections.Generic;
using System.ComponentModel;

namespace BusinessEntity.Google.DPA
{
    public class ErrorResponseGoogle
    {
        //      {
        //"errorMessage": string,
        //"cause": enum (ErrorCause)
        //  }

        public ErrorStatusEnum cause { get; set; }
        public string errorMessage { get; set; }
    }

    public enum ErrorStatusEnum
    {
        [Description("ERROR_CAUSE_UNSPECIFIED")]
        ERROR_CAUSE_UNSPECIFIED = 0,
        [Description("INVALID_NUMBER")] INVALID_NUMBER = 1,
        [Description("INCOMPATIBLE_PLAN")] INCOMPATIBLE_PLAN = 2,
        [Description("DUPLICATE_TRANSACTION")] DUPLICATE_TRANSACTION = 3,
        [Description("BAD_REQUEST")] BAD_REQUEST = 4,
        [Description("BAD_CPID")] BAD_CPID = 5,
        [Description("BACKEND_FAILURE")] BACKEND_FAILURE = 6,
        [Description("REQUEST_QUEUED")] REQUEST_QUEUED = 7,
        [Description("USER_ROAMING")] USER_ROAMING = 8,
        [Description("USER_OPT_OUT")] USER_OPT_OUT = 9,
        [Description("SIM_RELOAD_REQUIRED")] SIM_RELOAD_REQUIRED = 10,
        [Description("TOO_MANY_REQUESTS")] TOO_MANY_REQUESTS = 11,
        [Description("PAYMENT_MISSING")] PAYMENT_MISSING = 12,
        [Description("INVALID_IMSI")] INVALID_IMSI = 13,

        [Description("INELIGIBLE_FOR_SERVICE")]
        INELIGIBLE_FOR_SERVICE = 14,
        [Description("SERVICE_UNAVAILABLE")] SERVICE_UNAVAILABLE = 15,

        [Description("SERVICE_NOT_SUBSCRIBED")]
        SERVICE_NOT_SUBSCRIBED = 16,
        [Description("ACCOUNT_SUSPENDED")] ACCOUNT_SUSPENDED = 17
    }

    public static class errorValue
    {
        public static IDictionary<string, string> Error_dict = new Dictionary<string, string>
        {
            {"ERROR_CAUSE_UNSPECIFIED", "The cause of the error is unknown or is not specified below."},
            {
                "INVALID_NUMBER",
                "The phone number provided in the request is invalid.This error cause should be used for all requests that failed due to a bad phone number(e.g., the 404 NOT FOUND error)."
            },
            {
                "INCOMPATIBLE_PLAN",
                "The plan included in the request is incompatible with the user.See purchase plan API call in data plan agent API specification."
            },
            {
                "DUPLICATE_TRANSACTION",
                "The requested transaction is a duplicate of a previous transaction. See purchase plan API call in data plan agent API specification."
            },
            {"BAD_REQUEST", "Original request was malformed."},
            {"BAD_CPID", "The CPID provided in the request has expired or is otherwise unrecognized."},
            {
                "BACKEND_FAILURE",
                "While the DPA itself is operational, the backend necessary to complete the request is not."
            },
            {
                "REQUEST_QUEUED",
                "The DPA has queued a request for processing but the outcome of the request is still pending. The client may retry the request to retrieve the result of the request.The DPA will return REQUEST_QUEUED to requests that arrive before processing of the original request has completed."
            },
            {"USER_ROAMING", "The CPID endpoint or the DPA received a request for a user who is currently roaming."},
            {"USER_OPT_OUT", "User has not opted in the program."},
            {
                "SIM_RELOAD_REQUIRED",
                "The phone number provided in the request is in a \"grace period\" where a subscriber can receive incoming calls but cannot use mobile data. User has to top up credits or buy data packs to get out of this state."
            },
            {"TOO_MANY_REQUESTS", "GTAF is making too many requests to the DPA."},
            {"PAYMENT_MISSING", "The operator does not have a way to charge the user for purchase."},
            {"INVALID_IMSI", "The IMSI provided is invalid or inactive"},
            {"INELIGIBLE_FOR_SERVICE", "User is not eligible for the requested service"},
            {"SERVICE_UNAVAILABLE", "Requested service is unavailable.Please retry later"},
            {"SERVICE_NOT_SUBSCRIBED", "Service is not subscribed by user"},
            {"ACCOUNT_SUSPENDED", "User account is inactive or suspended"}
        };
    }
}