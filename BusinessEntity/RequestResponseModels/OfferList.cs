﻿using System.ComponentModel.DataAnnotations;

namespace BusinessEntity.RequestResponseModels
{
    public class OfferList : BaseRequest
    {
        [Required] public bool ShowBalanceWiseProduct { get; set; }

        [Required] public bool ShowLoanProduct { get; set; }
    }
}