﻿namespace BusinessEntity.RequestResponseModels
{
    public class AvailableDataPackListRequest : BaseRequest
    {
        public int DataPackTypeID { get; set; }
    }
}