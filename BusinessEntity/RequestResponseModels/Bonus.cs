﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BusinessEntity.RequestResponseModels
{
    public class Bonus
    {
    }

    public class BonusError
    {
        public string ErrorCode { get; set; }
        public string Error { get; set; }
    }

    public class BonusResponse : BonusError
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }

    public class BonusRequest : BaseRequest
    {
        //public string msisdn { get; set; }

        [Required] public DateTime date { get; set; }

        [Required] public BonusType bonusType { get; set; }
    }

    public enum BonusType
    {
        SignIn,
        SignUp
    }
}