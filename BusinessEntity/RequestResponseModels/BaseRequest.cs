﻿using System.ComponentModel.DataAnnotations;

namespace BusinessEntity.RequestResponseModels
{
    public class BaseRequest
    {
        //[Required]
        [StringLength(10, MinimumLength = 10, ErrorMessage = "MSISDN must be 10 char")]
        public string MSISDN { get; set; }

        public string SubscriptionID { get; set; }
    }
}