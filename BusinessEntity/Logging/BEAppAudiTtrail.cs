using System.ComponentModel.DataAnnotations;
using BusinessEntity.APIHub2;

namespace BusinessEntity.Logging
{
    #region Object of BEAppAudiTtrail

    public class BEAppAudiTtrail // : BEBase
    {
        #region Destructor

        ~BEAppAudiTtrail()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Property

        [Display(Name = "Id")] public int Id { get; set; }

        [Display(Name = "Msisdn")] public string Msisdn { get; set; }

        [Display(Name = "Request Url")] public string RequestUrl { get; set; }

        [Display(Name = "Params")] public string Params { get; set; }

        [Display(Name = "Ip Address")] public string IpAddress { get; set; }

        [Display(Name = "RTT")] public int RTT { get; set; }

        [Display(Name = "Error Message")] public string ErrorMessage { get; set; }

        [Display(Name = "Request Query String")]
        public string RequestQueryString { get; set; }

        [Display(Name = "Method")] public string Method { get; set; }

        [Display(Name = "Api Version")] public string ApiVersion { get; set; }

        [Display(Name = "Service User Id")] public int ServiceUserId { get; set; }

        [Display(Name = "Provider")] public string Provider { get; set; }

        #endregion
    }

    #endregion

    #region Collectin Object of BEAppAudiTtrail

    public class BEAppAudiTtrails : BEBaseList<BEAppAudiTtrail>
    {
        #region Destructor

        ~BEAppAudiTtrails()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Functions

        #endregion
    }

    #endregion
}