using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BusinessEntity.Logging
{
    #region Object of BEAppErrorLog

    public class BEAppErrorLog //: BEBase
    {
        #region Destructor

        ~BEAppErrorLog()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Property

        [Display(Name = "Id")] public string Id { get; set; }

        [Display(Name = "Msisdn")] public string Msisdn { get; set; }

        [Display(Name = "Log Time")] public DateTime LogTime { get; set; } = DateTime.Now;

        [Display(Name = "Message")] public string Message { get; set; }

        [Display(Name = "Stacktrace")] public string Stacktrace { get; set; }

        [Display(Name = "Request Method")] public string RequestMethod { get; set; }

        [Display(Name = "Request From")] public string RequestFrom { get; set; }

        [Display(Name = "Request Url")] public string RequestUrl { get; set; }

        [Display(Name = "Request")] public string Request { get; set; }

        [Display(Name = "Response")] public string Response { get; set; }

        [Display(Name = "Rtt")] public double Rtt { get; set; }

        #endregion
    }

    #endregion

    #region Collectin Object of BEAppErrorLog

    public class BEAppErrorLogs : List<BEAppErrorLog>
    {
        #region Destructor

        ~BEAppErrorLogs()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Functions

        #endregion
    }

    #endregion
}