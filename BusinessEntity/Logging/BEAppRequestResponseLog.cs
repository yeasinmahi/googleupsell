using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BusinessEntity.Logging
{
    #region Object of BEAppRequestResponseLog

    public class BEAppRequestResponseLog //: BEBase
    {
        #region Destructor

        ~BEAppRequestResponseLog()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Property

        [Display(Name = "Server Name")] public string ServerName { get; set; }

        [Display(Name = "Server Ip")] public string ServerIp { get; set; }

        [Display(Name = "Id")] public int Id { get; set; }

        [Display(Name = "Msisdn")] public string Msisdn { get; set; }

        [Display(Name = "Request Uri")] public string RequestUri { get; set; }

        [Display(Name = "Request Body")] public string RequestBody { get; set; }

        [Display(Name = "Request Headers")] public string RequestHeaders { get; set; }

        [Display(Name = "Clien Iip Address")] public string ClienIipAddress { get; set; }

        [Display(Name = "Response Body")] public string ResponseBody { get; set; }

        [Display(Name = "RequestMethod")] public string RequestMethod { get; set; }

        [Display(Name = "Respon Sestatus Code")]
        public string ResponSestatusCode { get; set; }

        [Display(Name = "Response Headers")] public string ResponseHeaders { get; set; }

        [Display(Name = "RTT")] public double RTT { get; set; }

        #endregion
    }

    #endregion

    #region Collectin Object of BEAppRequestResponseLog

    public class BEAppRequestResponseLogs : List<BEAppRequestResponseLog>
    {
        #region Destructor

        ~BEAppRequestResponseLogs()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Functions

        #endregion
    }

    #endregion
}