﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MessageConsumer
{
    public interface IBookingConsumer
    {
        void Listen(Action<string> message);
    }
}
