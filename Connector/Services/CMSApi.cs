﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using BusinessEntity.ConnectorModels.CMS;
using Connector.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Connector.Services
{
    public class CMSApi
    {
        //DBSSHandler httpClientExtentions = new DBSSHandler();

        private readonly LogWriter logWriter = new LogWriter();

        //public CMSApi(bool writeLog)
        //{
        //    WriteLog = writeLog;
        //}

        //public static bool WriteLog { get; set; } = AppSettings.WriteLog;
        //public static bool WriteLogPath { get; set; }

        //private static readonly string contentTypeJson = "application/json";
        //private static readonly string contentTypeVND_API_JSON = " application/vnd.api+json";

        private readonly IHttpClientFactory clientFactory;
        public CMSApi(IHttpClientFactory clientFactory)
        {
            this.clientFactory = clientFactory;
        }
        public async Task<List<MyOfferListResponse>> GetMyOfferList(MyOfferListRequest request, IHttpClientFactory clientFactory)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var response = new List<MyOfferListResponse>();
            var sresponse = new MyOfferListResponse();
            //var json = CMSResponseHelper.PresentOfferListResponse;
            //var json = CMSResponseHelper.CMSErrorResponse;


            var contentString = JsonConvert.SerializeObject(request);
            // string json = cmdAPI.HTTPPostRequest(string.Concat(CMS.CMSApiBaseURL, CMS.PresentOfferListURLPrefix), contentString, request.msisdn.ToString(), "CMS", MethodBase.GetCurrentMethod().Name);

            //var json = await new CMSHandler().Post(string.Concat(CMSHandler.CMSApiBaseURL, CMSHandler.PresentOfferListURLPrefix),
            //    contentString, request.msisdn.ToString(), "CMS", MethodBase.GetCurrentMethod().Name, clientFactory);

            response = await new CMSHandler(clientFactory).PostHttpClient(string.Concat(CMSHandler.CMSApiBaseURL, CMSHandler.PresentOfferListURLPrefix),
                contentString, request.msisdn.ToString(), "CMS", MethodBase.GetCurrentMethod().Name);



            //if (string.IsNullOrEmpty(json)) throw new Exception("CMS Error||" + "No Response form CMS");

            //try
            //{
            //    var token = JToken.Parse(json);

            //    if (token is JArray)
            //    {
            //        response = token.ToObject<List<MyOfferListResponse>>();
            //    }
            //    else if (token is JObject)
            //    {
            //        sresponse = token.ToObject<MyOfferListResponse>();
            //        response.Add(sresponse);
            //    }

            //    // response = JsonConvert.DeserializeObject<List<MyOfferListResponse>>(json);
            //}
            //catch (JsonReaderException ex)
            //{
            //    throw ex;
            //}


            //rtt = watch.ElapsedMilliseconds;
            return response;
        }


        public async Task<MyOfferDetailResponse> GetMyOfferDetails(MyOfferDetailRequest request,IHttpClientFactory clientFactory)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var response = new List<MyOfferDetailResponse>();
            var sresponse = new MyOfferDetailResponse();
            //string include = "services";

            var URL = string.Concat(CMSHandler.CMSApiBaseURL,
                string.Format(CMSHandler.PresentOfferDetailsURL, request.msisdn, request.offerID, request.language,
                    request.channelID, request.salesChannelID));
            //var json = CMSResponseHelper.PresentOfferDetailsResponse;
            //var json = CMSResponseHelper.CMSErrorResponse;
            //var json = new HttpHelper().GetAsync(URL).GetAwaiter().GetResult();

            var contentString = JsonConvert.SerializeObject(request);
            response = await new CMSHandler(clientFactory).Post<List<MyOfferDetailResponse>>(
                string.Concat(CMSHandler.CMSApiBaseURL, CMSHandler.PresentOfferDetailsURLPrefix), contentString,
                request.msisdn.ToString(), "CMS", MethodBase.GetCurrentMethod().Name);
            sresponse = response.FirstOrDefault();
            //if (CMSHandler.IsValidJson(json))
            //    try
            //    {
            //        var token = JToken.Parse(json);

            //        if (token is JArray)
            //        {
            //            response = token.ToObject<List<MyOfferDetailResponse>>();
            //            sresponse = response.FirstOrDefault();
            //        }
            //        else if (token is JObject)
            //        {
            //            sresponse = token.ToObject<MyOfferDetailResponse>();
            //            response.Add(sresponse);
            //        }

            //        // response = JsonConvert.DeserializeObject<List<MyOfferListResponse>>(json);
            //    }
            //    catch (JsonReaderException ex)
            //    {
            //        rtt = watch.ElapsedMilliseconds;
            //        await logWriter.ToErrorLog(ex, 0, request.msisdn.ToString(), MethodBase.GetCurrentMethod().Name,
            //            "URL: " + URL + "|" + "BODY:  " + contentString, "CMS", json);

            //        throw;
            //    }

            rtt = watch.ElapsedMilliseconds;
            return sresponse;
        }

        public async Task<MyOfferPurchaseResponse> PurchaseMyOffer(MyOfferPurchaseRequest request, IHttpClientFactory clientFactory)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var response = new MyOfferPurchaseResponse();
            //string include = "services";
            try
            {
                var URL = string.Concat(CMSHandler.CMSApiBaseURL,
                    string.Format(CMSHandler.AcceptOfferURLPrefix, request.msisdn, request.offerID, request.channelID,
                        request.salesChannelID));
                // var json = CMSResponseHelper.AcceptOfferResponse;
                // var json = CMSResponseHelper.CMSErrorResponse;
                // var json = new HttpHelper().GetAsync(URL).GetAwaiter().GetResult();
                var contentString = JsonConvert.SerializeObject(request);
                // string json = new CMS().HTTPPostRequest(string.Concat(CMS.CMSApiBaseURL, CMS.AcceptOfferURLPrefix), contentString, request.msisdn.ToString(), "CMS", MethodBase.GetCurrentMethod().Name);

                response = await new CMSHandler(clientFactory).Post<MyOfferPurchaseResponse>(
                    string.Concat(CMSHandler.CMSApiBaseURL, CMSHandler.AcceptOfferURLPrefix), contentString,
                    request.msisdn.ToString(), "CMS", MethodBase.GetCurrentMethod().Name);
                ////if (cmsHelper.IsValidJson(json))
                ////{
                //if (string.IsNullOrEmpty(json)) throw new Exception("CMS Error||" + "No Response form CMS");

                //if (CMSHandler.IsValidJson(json))
                //    try
                //    {
                //        var token = JToken.Parse(json);

                //        if (token is JArray)
                //        {
                //            response = token.ToObject<List<MyOfferPurchaseResponse>>();
                //            sresponse = response.FirstOrDefault();
                //        }
                //        else if (token is JObject)
                //        {
                //            sresponse = token.ToObject<MyOfferPurchaseResponse>();
                //            response.Add(sresponse);
                //        }

                //        // response = JsonConvert.DeserializeObject<List<MyOfferListResponse>>(json);
                //    }
                //    catch (Exception ex)
                //    {
                //        await logWriter.ToErrorLog(ex, 0, request.msisdn.ToString(), MethodBase.GetCurrentMethod().Name,
                //            "URL: " + URL + "|" + "BODY:  " + contentString, "CMS", json);

                //        throw ex;
                //    }
            }

            catch (Exception e)
            {
                throw e;
            }

            rtt = watch.ElapsedMilliseconds;
            return response;
        }
    }
}