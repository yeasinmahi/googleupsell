﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using BusinessEntity.ConnectorModels.LMS;
using Connector.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Connector.Services
{
    public class LMSApi
    {
        private readonly IHttpClientFactory clientFactory;
        public LMSApi(IHttpClientFactory clientFactory)
        {
            this.clientFactory = clientFactory;
        }

        public LMSApi(bool writeLog)
        {
            WriteLog = writeLog;
        }

        public bool WriteLog { get; set; }
        public bool WriteLogPath { get; set; }

        //private readonly string contentTypeJson = "application/json";
        //private readonly string contentTypeVND_API_JSON = " application/vnd.api+json";

        public async Task<CustomerLoyaltyProgramResponse> GetCustomerLoyaltyPrograms(
            GetCustomerInfoWithIDRequest request)
        {
          

            var contentString = JsonConvert.SerializeObject(request);
            List<CustomerLoyaltyProgramResponse> response = await new CMSHandler(clientFactory).Post<List<CustomerLoyaltyProgramResponse>>(
                string.Concat(LMS.LMSApiBaseURL, LMS.getCustomerLoyaltyProgramsURLPrefix), contentString,
                "CTID:" + request.customerID, "LMS", MethodBase.GetCurrentMethod().Name);
            
            CustomerLoyaltyProgramResponse sresponse = response.FirstOrDefault();
            //if (CMSHandler.IsValidJson(json))
            //    try
            //    {
            //        var token = JToken.Parse(json);

            //        if (token is JArray)
            //        {
            //            response = token.ToObject<List<CustomerLoyaltyProgramResponse>>();
            //            sresponse = response.FirstOrDefault();
            //            if (sresponse.loyaltyPrograms.Count < 1) throw new Exception("No Data Found");
            //        }
            //        else if (token is JObject)
            //        {
            //            sresponse = token.ToObject<CustomerLoyaltyProgramResponse>();
            //            response.Add(sresponse);
            //            if (sresponse.loyaltyPrograms.Count < 1) throw new Exception("No Data Found");
            //        }

            //        // response = JsonConvert.DeserializeObject<List<MyOfferListResponse>>(json);
            //    }
            //    catch (JsonReaderException ex)
            //    {
            //        throw ex;
            //    }
            return sresponse;
        }

        public async Task<GetCustomerBRDResponse> GetCustomerBDRs(GetCustomerDataRecordRequest request)
        {
            var contentString = JsonConvert.SerializeObject(request);
            List<GetCustomerBRDResponse> response = await new CMSHandler(clientFactory).Post<List<GetCustomerBRDResponse>>(string.Concat(LMS.LMSApiBaseURL, LMS.getCustomerBDRsURLPrefix),
                contentString, "CTID:" + request.customerID, "LMS", MethodBase.GetCurrentMethod().Name);
            //var json = LMSResponseHelper.GetCustomerBDRsResponse;
            //var json = LMSResponseHelper.ErrorResponse;
            GetCustomerBRDResponse sresponse = response.FirstOrDefault();
            //if (CMSHandler.IsValidJson(json))
            //    try
            //    {
            //        var token = JToken.Parse(json);

            //        if (token is JArray)
            //        {
            //            response = token.ToObject<List<GetCustomerBRDResponse>>();
            //            sresponse = response.FirstOrDefault();
            //        }
            //        else if (token is JObject)
            //        {
            //            sresponse = token.ToObject<GetCustomerBRDResponse>();
            //            response.Add(sresponse);
            //        }

            //        // response = JsonConvert.DeserializeObject<List<MyOfferListResponse>>(json);
            //    }
            //    catch (JsonReaderException ex)
            //    {
            //        throw ex;
            //    }

            return sresponse;
        }

        public async Task<GetCustomerORDResponse> GetCustomerODRs(GetCustomerDataRecordRequest request)
        {

            var contentString = JsonConvert.SerializeObject(request);
            List<GetCustomerORDResponse> response = await new CMSHandler(clientFactory).Post<List<GetCustomerORDResponse>>(string.Concat(LMS.LMSApiBaseURL, LMS.getCustomerODRsURLPrefix),
                contentString, "CTID" + request.customerID, "LMS", MethodBase.GetCurrentMethod().Name);
            GetCustomerORDResponse sresponse = response.FirstOrDefault();
            //var json = LMSResponseHelper.GetCustomerODRsResponse;
            //var json = LMSResponseHelper.ErrorResponse;

            //if (CMSHandler.IsValidJson(json))
            //    try
            //    {
            //        var token = JToken.Parse(json);

            //        if (token is JArray)
            //        {
            //            response = token.ToObject<List<GetCustomerORDResponse>>();
            //            sresponse = response.FirstOrDefault();
            //        }
            //        else if (token is JObject)
            //        {
            //            sresponse = token.ToObject<GetCustomerORDResponse>();
            //            response.Add(sresponse);
            //        }

            //        // response = JsonConvert.DeserializeObject<List<MyOfferListResponse>>(json);
            //    }
            //    catch (JsonReaderException ex)
            //    {
            //        throw ex;
            //    }

            //rtt = watch.ElapsedMilliseconds;
            return sresponse;
        }

        public async Task<GetOfferListResponse> GetOffersList(GetCustomerInfoWithIDRequest request)
        {
                       var contentString = JsonConvert.SerializeObject(request);
            List<GetOfferListResponse> response = await new CMSHandler(clientFactory).Post<List<GetOfferListResponse>>(
                string.Concat(LMS.DNBOApiBaseURL, LMS.getCustomerLoyaltyProgramsURLPrefix), contentString,
                "CTID" + request.customerID, "LMS", MethodBase.GetCurrentMethod().Name);
            //var json = LMSResponseHelper.GetPresentOfferDetailsResponse;
            //var json = LMSResponseHelper.ErrorResponse;
            GetOfferListResponse sresponse = response.FirstOrDefault();
            //if (CMSHandler.IsValidJson(json))
            //    try
            //    {
            //        var token = JToken.Parse(json);

            //        if (token is JArray)
            //        {
            //            response = token.ToObject<List<GetOfferListResponse>>();
            //            sresponse = response.FirstOrDefault();
            //        }
            //        else if (token is JObject)
            //        {
            //            sresponse = token.ToObject<GetOfferListResponse>();
            //            response.Add(sresponse);
            //        }

            //        // response = JsonConvert.DeserializeObject<List<MyOfferListResponse>>(json);
            //    }
            //    catch (JsonReaderException ex)
            //    {
            //        throw ex;
            //    }

            return sresponse;
        }

        public async Task<GerOfferResponse> GetOffers(GetOffersRequest request)
        {
            
            var contentString = JsonConvert.SerializeObject(request);
            List<GerOfferResponse> response = await new CMSHandler(clientFactory).Post<List<GerOfferResponse>>(
                string.Concat(LMS.DNBOApiBaseURL, LMS.getCustomerLoyaltyProgramsURLPrefix), contentString,
                "OFID" + request.offerID, "LMS", MethodBase.GetCurrentMethod().Name);
            GerOfferResponse sresponse = response.FirstOrDefault();
            //var json = LMSResponseHelper.GetPresentOfferDetailsResponse;
            //var json = LMSResponseHelper.ErrorResponse;

            //if (CMSHandler.IsValidJson(json))
            //    try
            //    {
            //        var token = JToken.Parse(json);

            //        if (token is JArray)
            //        {
            //            response = token.ToObject<List<GerOfferResponse>>();
            //            sresponse = response.FirstOrDefault();
            //        }
            //        else if (token is JObject)
            //        {
            //            sresponse = token.ToObject<GerOfferResponse>();
            //            response.Add(sresponse);
            //        }

            //        // response = JsonConvert.DeserializeObject<List<MyOfferListResponse>>(json);
            //    }
            //    catch (JsonReaderException ex)
            //    {
            //        throw ex;
            //    }

            return sresponse;
        }

        public async Task<GetLoyaltyProgramResponse> GetLoyaltyProgram(GetLoyaltyProgramRequest request)
        {
           
            var contentString = JsonConvert.SerializeObject(request);
            List<GetLoyaltyProgramResponse> response = await new CMSHandler(clientFactory).Post<List<GetLoyaltyProgramResponse>>(
                string.Concat(LMS.LMSApiBaseURL, LMS.getLoyaltyProgramsListURLPrefix), contentString,
                "LPID" + request.loyaltyProgramID, "LMS", MethodBase.GetCurrentMethod().Name);
            GetLoyaltyProgramResponse sresponse = response.FirstOrDefault();
            //var json = LMSResponseHelper.GetLoyaltyProgramResponse;
            //var json = LMSResponseHelper.ErrorResponse;

            //if (CMSHandler.IsValidJson(json))
            //    try
            //    {
            //        var token = JToken.Parse(json);

            //        if (token is JArray)
            //        {
            //            response = token.ToObject<List<GetLoyaltyProgramResponse>>();
            //            sresponse = response.FirstOrDefault();
            //        }
            //        else if (token is JObject)
            //        {
            //            sresponse = token.ToObject<GetLoyaltyProgramResponse>();
            //            response.Add(sresponse);
            //        }

            //        // response = JsonConvert.DeserializeObject<List<MyOfferListResponse>>(json);
            //    }
            //    catch (JsonReaderException ex)
            //    {
            //        throw ex;
            //    }

            return sresponse;
        }

        public async Task<GetLoyaltyProgramListResponse> GetLoyaltyProgramsList(CommonRequest request)
        {
           
            var contentString = JsonConvert.SerializeObject(request);
            List<GetLoyaltyProgramListResponse> response = await new CMSHandler(clientFactory).Post<List<GetLoyaltyProgramListResponse>>(
                string.Concat(LMS.LMSApiBaseURL, LMS.getLoyaltyProgramsListURLPrefix), contentString,
                "LMID" + request.loginName, "LMS", MethodBase.GetCurrentMethod().Name);
            GetLoyaltyProgramListResponse sresponse = response.FirstOrDefault();
            // var json = LMSResponseHelper.GetLoyaltyProgramListResponse;
            //var json = LMSResponseHelper.ErrorResponse;

            //if (CMSHandler.IsValidJson(json))
            //    try
            //    {
            //        var token = JToken.Parse(json);

            //        if (token is JArray)
            //        {
            //            response = token.ToObject<List<GetLoyaltyProgramListResponse>>();
            //            sresponse = response.FirstOrDefault();
            //        }
            //        else if (token is JObject)
            //        {
            //            sresponse = token.ToObject<GetLoyaltyProgramListResponse>();
            //            response.Add(sresponse);
            //        }

            //        // response = JsonConvert.DeserializeObject<List<MyOfferListResponse>>(json);
            //    }
            //    catch (JsonReaderException ex)
            //    {
            //        throw ex;
            //    }

            return sresponse;
        }

        public async Task<CommonResponse> DebitBonus(DebitCreditBonusRequest request)
        {
            
            var contentString = JsonConvert.SerializeObject(request);
            List<CommonResponse> response = await new CMSHandler(clientFactory).Post<List<CommonResponse>>(string.Concat(LMS.LMSApiBaseURL, LMS.getCustomerDebitBonusrefix),
                contentString, "CTID" + request.customerID, "LMS", MethodBase.GetCurrentMethod().Name);
            CommonResponse sresponse = response.FirstOrDefault();
            //var json = LMSResponseHelper.CommonResponse;
            //var json = LMSResponseHelper.ErrorResponse;
            //if (CMSHandler.IsValidJson(json))
            //    try
            //    {
            //        var token = JToken.Parse(json);

            //        if (token is JArray)
            //        {
            //            response = token.ToObject<List<CommonResponse>>();
            //            sresponse = response.FirstOrDefault();
            //        }
            //        else if (token is JObject)
            //        {
            //            sresponse = token.ToObject<CommonResponse>();
            //            response.Add(sresponse);
            //        }

            //        // response = JsonConvert.DeserializeObject<List<MyOfferListResponse>>(json);
            //    }
            //    catch (JsonReaderException ex)
            //    {
            //        throw ex;
            //    }

            return sresponse;
        }

        public async Task<CommonResponse> CreditBonus(DebitCreditBonusRequest request)
        {
                        var contentString = JsonConvert.SerializeObject(request);
            List<CommonResponse> response = await new CMSHandler(clientFactory).Post<List<CommonResponse>>(string.Concat(LMS.LMSApiBaseURL, LMS.getCustomerCreditBonusrefix),
                contentString, "CTID" + request.customerID, "LMS", MethodBase.GetCurrentMethod().Name);
            CommonResponse sresponse = response.FirstOrDefault();
            //var json = LMSResponseHelper.CommonResponse;
            //var json = LMSResponseHelper.ErrorResponse;
            //if (CMSHandler.IsValidJson(json))
            //    try
            //    {
            //        var token = JToken.Parse(json);

            //        if (token is JArray)
            //        {
            //            response = token.ToObject<List<CommonResponse>>();
            //            sresponse = response.FirstOrDefault();
            //        }
            //        else if (token is JObject)
            //        {
            //            sresponse = token.ToObject<CommonResponse>();
            //            response.Add(sresponse);
            //        }

            //        // response = JsonConvert.DeserializeObject<List<MyOfferListResponse>>(json);
            //    }
            //    catch (JsonReaderException ex)
            //    {
            //        throw ex;
            //    }
            return sresponse;
        }

        public async Task<List<PresentOfferListResponse>> GetPresentOfferList(PresentOfferListRequest request)
        {
            
            var contentString = JsonConvert.SerializeObject(request);
            List<PresentOfferListResponse> response = await new CMSHandler(clientFactory).Post<List<PresentOfferListResponse>>(string.Concat(LMS.DNBOApiBaseURL, LMS.GetPresentOfferListURLPrefix),
                contentString, request.msisdn.ToString(), "LMS", MethodBase.GetCurrentMethod().Name);
            //var json = LMSResponseHelper.GetPresentOfferListResponseLMS;
            //var json = LMSResponseHelper.ErrorResponse;
            //if (CMSHandler.IsValidJson(json))
            //    try
            //    {
            //        var token = JToken.Parse(json);

            //        if (token is JArray)
            //        {
            //            response = token.ToObject<List<PresentOfferListResponse>>();
            //        }
            //        else if (token is JObject)
            //        {
            //            sresponse = token.ToObject<PresentOfferListResponse>();
            //            response.Add(sresponse);
            //        }

            //        // response = JsonConvert.DeserializeObject<List<MyOfferListResponse>>(json);
            //    }
            //    catch (JsonReaderException ex)
            //    {
            //        throw ex;
            //    }

            return response;
        }

        public async Task<PresentOfferDetailResponse> GetPresentOfferDetail(PresentOfferDetailRequest request)
        {
            
            var contentString = JsonConvert.SerializeObject(request);
            List<PresentOfferDetailResponse> response = await new CMSHandler(clientFactory).Post<List<PresentOfferDetailResponse>>(
                string.Concat(LMS.DNBOApiBaseURL, LMS.GetPresentOfferDetailsURLPrefix), contentString,
                request.msisdn.ToString(), "LMS", MethodBase.GetCurrentMethod().Name);
            PresentOfferDetailResponse sresponse = response.FirstOrDefault();
            //var json = LMSResponseHelper.GetPresentOfferDetailsResponseLMS;
            //var json = LMSResponseHelper.ErrorResponse;

            //if (CMSHandler.IsValidJson(json))
            //    try
            //    {
            //        var token = JToken.Parse(json);

            //        if (token is JArray)
            //        {
            //            response = token.ToObject<List<PresentOfferDetailResponse>>();
            //            sresponse = response.FirstOrDefault();
            //        }
            //        else if (token is JObject)
            //        {
            //            sresponse = token.ToObject<PresentOfferDetailResponse>();
            //            response.Add(sresponse);
            //        }

            //        // response = JsonConvert.DeserializeObject<List<MyOfferListResponse>>(json);
            //    }
            //    catch (JsonReaderException ex)
            //    {
            //        throw ex;
            //    }

            return sresponse;
        }

        public async Task<PresentOfferPurchaseResponse> AcceptOffer(PresentOfferPurchaseRequest request)
        {
            
            var contentString = JsonConvert.SerializeObject(request);
            List<PresentOfferPurchaseResponse> response = await new CMSHandler(clientFactory).Post<List<PresentOfferPurchaseResponse>>(string.Concat(LMS.DNBOApiBaseURL, LMS.DNBOAcceptOfferURLPrefix),
                contentString, request.msisdn.ToString(), "LMS", MethodBase.GetCurrentMethod().Name);
            PresentOfferPurchaseResponse sresponse = response.FirstOrDefault();
            ///var json = LMSResponseHelper.DNBOAcceptOfferResponse;
            // var json = LMSResponseHelper.ErrorResponse;
            //if (CMSHandler.IsValidJson(json))
            //    try
            //    {
            //        var token = JToken.Parse(json);

            //        if (token is JArray)
            //        {
            //            response = token.ToObject<List<PresentOfferPurchaseResponse>>();
            //            sresponse = response.FirstOrDefault();
            //        }
            //        else if (token is JObject)
            //        {
            //            sresponse = token.ToObject<PresentOfferPurchaseResponse>();
            //            response.Add(sresponse);
            //        }

            //        // response = JsonConvert.DeserializeObject<List<MyOfferListResponse>>(json);
            //    }
            //    catch (JsonReaderException)
            //    {
            //    }

            return sresponse;
        }
    }
}