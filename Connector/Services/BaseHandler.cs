﻿using BusinessEntity.APIHub2;
using System.Diagnostics;

namespace Connector.Services
{
    public class BaseHandler
    {
        protected Stopwatch watch = Stopwatch.StartNew();
        protected bool IsError { get; set; } = false;
        protected bool IsCustomerToken { get; set; } = false;

        private string actualErrorMessage = string.Empty;
        protected string ActualErrorMessage
        {
            //get { return ActualErrorMessage; }
            set { actualErrorMessage = $"{actualErrorMessage}{value}|"; }
        }

        protected void CheckError<T>(BEMessageData<T> messageData, bool throwError = false)
        {
            if (messageData.Status != 200)
            {
                IsError = true;
                ActualErrorMessage = messageData.Error;
            }
            switch (messageData.Status)
            {
                case 401:
                    //if (!IsCustomerToken)
                    //    GoogleToken.ClearAccessToken();

                    if (throwError)
                        throw new BeHandledException(BEMessageCodes.UnauthorizedError.Status, BEMessageCodes.UnauthorizedError.error, messageData.Error);
                    break;
                default:
                    break;
            }
        }
    }
}
