﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using BusinessEntity.ConnectorModels.CMS;
using BusinessEntity.ConnectorModels.DBSS;
using BusinessObject;
using Connector.Helpers;
using LogService;
using Newtonsoft.Json;

namespace Connector.Services
{
    public class DBSSWebAPI
    {
        private readonly string contentTypeJson = "application/json";
        private readonly string contentTypeVND_API_JSON = "application/vnd.api+json";
        private readonly DBSSHandler dbbsHandler;
        private readonly LogWriter logWriter = new LogWriter();
        private readonly IHttpClientFactory _clientFactory;
        private readonly static string clientName = "dbssHttpClientFactory";
        public DBSSWebAPI(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
            dbbsHandler = new DBSSHandler(_clientFactory);

        }

        public DBSSWebAPI(bool writeLog)
        {
            dbbsHandler = new DBSSHandler(_clientFactory);
            WriteLog = writeLog;
        }

        public bool WriteLog { get; set; }
        public bool WriteLogPath { get; set; }


        //NEED TO REMOVE
        public async Task<DataBalanceResponse> GetDataBalancePrePaid(string subscriptionId)
        {
            try
            {
                Stopwatch watch = Stopwatch.StartNew();
                double rtt = 0;
                var response = new DataBalanceResponse();

                var URL = string.Format(DBSSAPIUrls.BalancesURL, subscriptionId);
                response = await dbbsHandler.GetJsonObjectFromUrl<DataBalanceResponse>(URL,
                    ResponseHelper.DataBalancePrePaidResponseJson, MethodBase.GetCurrentMethod().Name,
                    "SCID:" + subscriptionId);
                if (response.Errors != null) response.HandleError();

                rtt = watch.ElapsedMilliseconds;
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //NEED TO REMOVE
        public async Task<DataBalanceForPostpaidResponse> GetDataBalancePostPaid(string subscriptionId)
        {
            try
            {
                Stopwatch watch = Stopwatch.StartNew();
                double rtt = 0;
                var response = new DataBalanceForPostpaidResponse();

                var URL = string.Concat(string.Format(DBSSAPIUrls.DataBalancePostPaid, subscriptionId));
                response = await dbbsHandler.GetJsonObjectFromUrl<DataBalanceForPostpaidResponse>(URL,
                    ResponseHelper.DataBalancePostpaidResponseJson, MethodBase.GetCurrentMethod().Name,
                    "SCID:" + subscriptionId);
                if (response.Errors != null) response.HandleError();
                rtt = watch.ElapsedMilliseconds;
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///     GetBalanceInformation
        /// </summary>
        /// <param name="requesterMSISDN"></param>
        /// <param name="subscriptionId"></param>
        /// <param name="perpaidPostpaidType"></param>
        /// <returns></returns>
        public async Task<AccountBalance> GetBalanceInformation(string requesterMSISDN, string subscriptionId,
            string perpaidPostpaidType)
        {
            AccountBalance accountBalance = null;
            Stopwatch watch = Stopwatch.StartNew();
            //double rtt = 0;


            accountBalance = new AccountBalance();
            //accountBalance = SelfCareConnector.Helpers.MapHelper.IsUcip
            //? GetUCIPBalanceInformation(requesterMSISDN)
            accountBalance =
                await GetDBSSBalanceInformation(requesterMSISDN, subscriptionId, perpaidPostpaidType, true);


            return accountBalance;
        }

        public async Task<AccountBalance> GetDBSSBalanceInformation(string requesterMSISDN, string subsriptionID,
            string perpaidPostpaidType, bool loadSubscriptionInfo = false)
        {
            var daBalance = new AccountBalance();
            try
            {
                if (perpaidPostpaidType.Equals("POST", StringComparison.CurrentCultureIgnoreCase))
                {
                    if (!string.IsNullOrEmpty(subsriptionID))
                    {
                        var dataBalance = await GetCurrentMonthUsageReport(subsriptionID);

                        if (dataBalance.Errors != null) dataBalance.HandleError();
                        daBalance = daBalance.MapFromDbssBalanceForPostpaid(dataBalance);
                    }
                }
                else
                {
                    var dataBalance = await GetSubscriptionBalancePrePaid(requesterMSISDN);
                    if (dataBalance.Errors != null) dataBalance.HandleError();

                    daBalance = daBalance.MapFromDbssBalanceForPripaid(dataBalance, loadSubscriptionInfo);
                }
            }
            catch (Exception)
            {
                //SelfCareConnector.Helpers.await logWriter.ToErrorLog(ex, 0, requesterMSISDN, MethodBase.GetCurrentMethod().Name, "DBSSService." + MethodBase.GetCurrentMethod().Name, "BOSGW");
                daBalance = new AccountBalance();
            }

            return daBalance;
        }

        /// <summary>
        /// </summary>
        /// <param name="requesterMSISDN"></param>
        /// <param name="subsriptionID"></param>
        /// <param name="perpaidPostpaidType"></param>
        /// <returns></returns>
        private async Task<AccountBalance> GetDBSSBalanceInformationInternal(string requesterMSISDN,
            string subsriptionID, string perpaidPostpaidType)
        {
            var daBalance = new AccountBalance();
            try
            {
                if (!string.IsNullOrEmpty(subsriptionID))
                {
                    if (perpaidPostpaidType.Equals("POST", StringComparison.CurrentCultureIgnoreCase))
                    {
                        var dataBalance = await GetCurrentMonthUsageReport(subsriptionID);

                        if (dataBalance.Errors != null) dataBalance.HandleError();
                        daBalance = daBalance.MapFromDbssBalanceForPostpaid(dataBalance);
                    }
                    else
                    {
                        var dataBalance = await GetSubscriptionBalancePrePaid(requesterMSISDN);
                        if (dataBalance.Errors != null) dataBalance.HandleError();

                        daBalance = daBalance.MapFromDbssBalanceForPripaid(dataBalance);
                    }
                }
            }
            catch (Exception)
            {
                //SelfCareConnector.Helpers.await logWriter.ToErrorLog(ex, 0, requesterMSISDN, MethodBase.GetCurrentMethod().Name, "DBSSService." + MethodBase.GetCurrentMethod().Name, "BOSGW");
                daBalance = new AccountBalance();
            }

            return daBalance;
        }


        #region Subscriber & Customer

        public async Task<DBSSSubscriber> GetSubscriptionInformation(string requesterMSISDN)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var dbssSubscriber = new DBSSSubscriber();

            Subscription subscription = null;
            var cacheKey = string.Concat(UtilityHelper.CacheKeys.DBSSSubscriber, requesterMSISDN, requesterMSISDN);
            dbssSubscriber = new CachingHelper<DBSSSubscriber>().Get<DBSSSubscriber>(cacheKey);
            if (dbssSubscriber == null)
                try
                {
                    {
                        //subscription = new Subscription();
                        //var includeSubscriptionsAndOwner = string.Empty;
                        //subscription = dbssAPI.GetSubscriptions(requesterMSISDN, "subscription-type,owner-customer,sim-cards,connection-type,connected-products,barrings", out rttDouble);
                        subscription =
                            await GetSubscriptions(requesterMSISDN, "subscription-type,owner-customer,barrings");

                        if (subscription.Errors != null) subscription.HandleError();
                    }

                    if (subscription.Datas != null && subscription.Datas.Any())
                    {
                        dbssSubscriber = new DBSSSubscriber();
                        dbssSubscriber.SubscriptionID = subscription.Datas[0].Id;

                        var subscriptionType = subscription.Includeds.Find(x => x.Type == "subscription-types");
                        var customer = subscription.Includeds.Find(x => x.Type == "customers");
                        //Subscription.Included connectionType = subscription.Includeds.Find(x => x.Type == "connection-types");
                        //Subscription.Included simCards = subscription.Includeds.Find(x => x.Type == "sim-cards");
                        //List<Subscription.Included> connectedProducts = subscription.Includeds.Where(x => x.Type == "products").ToList();
                        var barrings = subscription.Includeds.Where(x => x.Type == "barrings").ToList();
                        if (subscriptionType != null)
                        {

                            //var subscriptionData = subscription.Datas.FirstOrDefault();
                            var subscriptionData = subscription.Datas.FindAll(c => c.Attributes.Status.ToLower() == "active").FirstOrDefault(); ;
                            if (subscriptionData != null)
                            {
                                dbssSubscriber.CurrentPackage = subscriptionType.Attributes.Name != null
                                    ? subscriptionType.Attributes.Name.En ?? string.Empty
                                    : string.Empty;
                                dbssSubscriber.PrepaidPostpaidType =
                                    !string.IsNullOrEmpty(subscriptionType.Attributes.PaymentType)
                                        ? subscriptionType.Attributes.PaymentType
                                        : string.Empty;
                                dbssSubscriber.PrepaidPostpaidType =
                                    !string.IsNullOrEmpty(dbssSubscriber.PrepaidPostpaidType) &&
                                    dbssSubscriber.PrepaidPostpaidType.ToLower() == "prepaid"
                                        ? "PREP"
                                        : "POST";
                                dbssSubscriber.CreditLimit = subscriptionType.Attributes.CreditLimit > 0
                                    ? subscriptionType.Attributes.CreditLimit
                                    : 0;
                                dbssSubscriber.ConnectionDate =
                                    subscriptionData.Attributes.ActivationTime.DateTime
                                        .Date; //Convert.ToDateTime(subscriptionData.Attributes.ActivationTime);
                                dbssSubscriber.ConnectionEffectiveDate =
                                    subscriptionData.Attributes.ActivationTime.DateTime.Date;
                                dbssSubscriber.ConnectionStatus =
                                    !string.IsNullOrEmpty(subscriptionData.Attributes.Status)
                                        ? subscriptionData.Attributes.Status
                                        : string.Empty;
                                dbssSubscriber.ConnectionStatusText =
                                    !string.IsNullOrEmpty(subscriptionData.Attributes.Status)
                                        ? subscriptionData.Attributes.Status
                                        : string.Empty;
                                dbssSubscriber.PackageID = !string.IsNullOrEmpty(subscriptionType.Id)
                                    ? subscriptionType.Id
                                    : string.Empty;
                                if (!(subscriptionType.Attributes == null || subscriptionType.Attributes.Name == null))
                                    dbssSubscriber.PackageName = subscriptionType.Attributes.Name.En ?? string.Empty;
                                dbssSubscriber.PackageDescription = string.Empty;
                                dbssSubscriber.EQuipIDorServiceID =
                                    !string.IsNullOrEmpty(subscriptionType.Attributes.Code)
                                        ? subscriptionType.Attributes.Code
                                        : string.Empty;
                                // dbssSubscriber.ContractNo = subscriptionData.Attributes.ContractId > 0 ? subscriptionData.Attributes.ContractId.ToString() : string.Empty;
                                dbssSubscriber.ContractNo = subscriptionData.Id ?? string.Empty;
                                dbssSubscriber.MSISDN = !string.IsNullOrEmpty(subscriptionData.Attributes.Msisdn)
                                    ? subscriptionData.Attributes.Msisdn
                                    : string.Empty;
                                dbssSubscriber.BillCycle =
                                    0; //!string.IsNullOrEmpty(subscriptionData.Attributes.BillCycle) ? subscriptionData.Attributes.BillCycle : string.Empty;
                                dbssSubscriber.IsCallnControll =
                                    false; //subscriptionData.Attributes.IsCallnControll ? subscriptionData.Attributes.IsCallnControll : false;
                                dbssSubscriber.IsRoam =
                                    false; //subscriptionData.Attributes.IsRoam ? subscriptionData.Attributes.IsRoam : false;
                                dbssSubscriber.VirtualContactNo = subscriptionType.Attributes.TradeRegisterId != null
                                    ? subscriptionType.Attributes.TradeRegisterId
                                    : string.Empty;
                                // dbssSubscriber.ConnectionTypeCode = connectionType.Attributes != null ? !string.IsNullOrEmpty(connectionType.Attributes.Code) ? connectionType.Attributes.Code : string.Empty : string.Empty;
                            }
                            else
                            {
                                dbssSubscriber.CurrentPackage = subscriptionType.Attributes.Name != null
                                    ? subscriptionType.Attributes.Name.En ?? string.Empty
                                    : string.Empty;
                                dbssSubscriber.PrepaidPostpaidType =
                                    !string.IsNullOrEmpty(subscriptionType.Attributes.PaymentType)
                                        ? subscriptionType.Attributes.PaymentType
                                        : string.Empty;
                                dbssSubscriber.CreditLimit = subscriptionType.Attributes.CreditLimit > 0
                                    ? subscriptionType.Attributes.CreditLimit
                                    : 0;
                            }
                        }

                        if (customer != null)
                        {
                            dbssSubscriber.CustomerID = !string.IsNullOrEmpty(customer.Id) ? customer.Id : string.Empty;
                            dbssSubscriber.Category = !string.IsNullOrEmpty(customer.Attributes.Category)
                                ? customer.Attributes.Category
                                : string.Empty;
                            dbssSubscriber.Email = !string.IsNullOrEmpty(customer.Attributes.Email)
                                ? customer.Attributes.Email
                                : string.Empty;
                            dbssSubscriber.FirstName = !string.IsNullOrEmpty(customer.Attributes.FirstName)
                                ? customer.Attributes.FirstName
                                : string.Empty;
                            dbssSubscriber.LastName = !string.IsNullOrEmpty(customer.Attributes.LastName)
                                ? customer.Attributes.LastName
                                : string.Empty;
                            dbssSubscriber.Gender = !string.IsNullOrEmpty(customer.Attributes.Gender)
                                ? customer.Attributes.Gender
                                : string.Empty;
                            dbssSubscriber.IsCompany =
                                customer.Attributes.IsCompany && customer.Attributes.IsCompany;
                            dbssSubscriber.InvoiceDeliveryType =
                                !string.IsNullOrEmpty(customer.Attributes.InvoiceDeliveryType)
                                    ? customer.Attributes.InvoiceDeliveryType
                                    : string.Empty;
                            dbssSubscriber.IsFleetManager = customer.Attributes.IsFleetManager && customer.Attributes.IsFleetManager;
                        }
                        //if (simCards != null)
                        //{
                        //    dbssSubscriber.PINCode1 = !string.IsNullOrEmpty(simCards.Attributes.Pin1) ? simCards.Attributes.Pin2 : string.Empty;
                        //    dbssSubscriber.PINCode2 = !string.IsNullOrEmpty(simCards.Attributes.Pin2) ? simCards.Attributes.Pin2 : string.Empty;
                        //    dbssSubscriber.PUK1 = !string.IsNullOrEmpty(simCards.Attributes.Puk1) ? simCards.Attributes.Puk1 : string.Empty;
                        //    dbssSubscriber.PUK2 = !string.IsNullOrEmpty(simCards.Attributes.Puk2) ? simCards.Attributes.Puk2 : string.Empty;
                        //    dbssSubscriber.ICC = !string.IsNullOrEmpty(simCards.Attributes.Icc) ? simCards.Attributes.Puk2 : string.Empty;
                        //}

                        if (barrings != null) dbssSubscriber.Barrings.AddRange(barrings.Select(b => b.Id));
                        new CachingHelper<DBSSSubscriber>().Put(cacheKey, dbssSubscriber, AppSettings.MediumResponse);
                    }
                }
                catch (Exception ex)
                {
                    rtt = watch.ElapsedMilliseconds;
                    await logWriter.ToErrorLog(ex, rtt, requesterMSISDN, MethodBase.GetCurrentMethod().Name,
                        MethodBase.GetCurrentMethod().Name, "DBSS");
                    throw new Exception("You are not a valid Customer!!!");
                }

            return dbssSubscriber;
        }

        public async Task<Subscription> GetSubscriptions(string msisdn, string include)
        {
            var jsonResponse = string.Empty;
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            Subscription subscription = null;
            var URL = string.Empty;
            try
            {
                //if (subscription != null) return subscription;
                URL = string.Format(DBSSAPIUrls.SubscriptionsURLByMSISDN, msisdn, include);
                var demoJson = AppSettings.DemoPrePostType == "POST"
                    ? ResponseHelper.SubscriptionResponsePOST
                    : ResponseHelper.SubscriptionResponsePRE;

                subscription = await dbbsHandler.GetJsonObjectFromUrl<Subscription>(URL, demoJson, 
                    MethodBase.GetCurrentMethod().Name, msisdn);
                if (subscription != null && subscription.Errors != null) subscription.HandleError();

                rtt = watch.ElapsedMilliseconds;
                return subscription;
            }

            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, msisdn, MethodBase.GetCurrentMethod().Name, URL, "DBSS",
                    request: URL);
                throw new Exception("You are not a valid customer!!!");
            }
        }

        public async Task<SubscriptionsBalance> GetSubscriptionsBalance(string msisdn, string include)
        {
            var jsonResponse = string.Empty;
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            SubscriptionsBalance subscription = null;
            var URL = string.Empty;
            try
            {
                URL = string.Format(DBSSAPIUrls.SubscriptionsURLByMSISDN, msisdn, include);
                var demoJson = AppSettings.DemoPrePostType == "POST"
                    ? ResponseHelper.SubscriptionResponsePOST
                    : ResponseHelper.SubscriptionResponsePRE;

                subscription = await dbbsHandler.GetJsonObjectFromUrl<SubscriptionsBalance>(URL, demoJson, MethodBase.GetCurrentMethod()?.Name, msisdn);
                if (subscription.Errors != null) subscription.HandleError();

                rtt = watch.ElapsedMilliseconds;

                return subscription;
            }

            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, msisdn, MethodBase.GetCurrentMethod()?.Name, URL, "DBSS",
                    request: URL);
                throw new Exception("You are not a valid customer!!!");
            }
        }

        public async Task<CustomerResponse> GetCustomer(string customerId)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var URL = string.Empty;

            try
            {
                URL = string.Format(DBSSAPIUrls.CustomerProfileURL, customerId);
                var customer = await dbbsHandler.GetJsonObjectFromUrl<CustomerResponse>(URL,
                    ResponseHelper.CustomerProfileResponse, MethodBase.GetCurrentMethod()?.Name,
                    "SCID:" + customerId);
                if (customer.Errors != null) customer.HandleError();
                return customer;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "SCID:" + customerId, MethodBase.GetCurrentMethod()?.Name,
                    MethodBase.GetCurrentMethod()?.Name, "DBSS");
                throw new Exception("You are not a valid Customer!!!");
            }
        }

        public async Task<DBSSCommandResponse> SaveCustomerProfile(long customerId, string email, DateTime? dateOfBirth)
        {
            var json = string.Empty;
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var URL = string.Empty;
            var response = new DBSSCommandResponse();
            try
            {
                var dob = dateOfBirth == null
                    ? DateTime.Now.ToString("yyyy-MM-dd")
                    : dateOfBirth.Value.ToString("yyyy-MM-dd");
                var model = new CustomerProfileRquest();
                var data = new CustomerProfileRquestData();
                data.Type = "customers";
                data.Id = customerId;

                var attributes = new CustomerProfileRquestAttributes();
                attributes.Email = email;
                attributes.DateOfBirth = dob;
                data.Attributes = attributes;
                model.Data = data;

                URL = string.Concat(string.Format(DBSSAPIUrls.CustomerProfileURL, customerId));
                var jsonRequest = JsonConvert.SerializeObject(model);

                //json = await new HttpWebRequestHelper().PATCH(URL, jsonRequest, contentTypeVND_API_JSON,
                //    AppSettings.DBSSHttpRequestTimeOut);
                response = await new HttpClientFactoryHandler(_clientFactory,clientName).Post<DBSSCommandResponse>(URL, AppSettings.DBSSHttpRequestTimeOut,jsonRequest, null, contentTypeVND_API_JSON);
                //response = JsonConvert.DeserializeObject<DBSSCommandResponse>(json);
                if (response.Errors != null) response.HandleError();
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "CTID:" + customerId, MethodBase.GetCurrentMethod()?.Name, URL,
                    "DBSS", json);
                // throw new Exception("You are not a valid Customer!!!");
            }

            return response;
        }

        public async Task<BarringResponse> GetBarrings(string subscriptionId)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var URL = string.Empty;

            try
            {
                //return JsonConvert.DeserializeObject<BarringResponse>(ResponseHelper.BarringResponseJson);
                URL = string.Concat(string.Format(DBSSAPIUrls.BarringsUrl, subscriptionId));
                var response = await dbbsHandler.GetJsonObjectFromUrl<BarringResponse>(URL, string.Empty, MethodBase.GetCurrentMethod().Name, "SCID:" + subscriptionId);
                if (response.Errors != null) response.HandleError();
                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "SCID:" + subscriptionId, MethodBase.GetCurrentMethod().Name,
                    MethodBase.GetCurrentMethod().Name, "DBSS");
                throw new Exception("You are not a valid Customer!!!");
            }
        }

        public async Task<string> GetSubscriptionIdByMSISDN(string MSISDN)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var subcriptionId = "";
            try
            {
                var subcription = await GetSubscriptions(MSISDN, "subscription-type");

                if (subcription.Datas.Any())
                {
                    subcriptionId = subcription.Datas.First().Id;
                    subcriptionId = subcriptionId.PadRight(32, 'a');
                }

                return subcriptionId;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, MSISDN, MethodBase.GetCurrentMethod().Name,
                    MethodBase.GetCurrentMethod().Name, "DBSS");
                throw new Exception("You are not a valid Customer!!!");
            }
        }

        public async Task<string> GetCustomerIdByMSISDN(string MSISDN)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var subcriptionId = "";
            try
            {
                var subcription = await GetSubscriptions(MSISDN, "owner-customer");
                if (subcription.Datas.Any())
                {
                    subcriptionId = subcription.Includeds.First().Id;
                    subcriptionId = subcriptionId.PadRight(32, 'a');
                }

                return subcriptionId;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, MSISDN, MethodBase.GetCurrentMethod().Name,
                    MethodBase.GetCurrentMethod().Name, "DBSS");
                throw new Exception("You are not a valid Customer!!!");
            }
        }

        public async Task<string> GetMSISDNBySubscriptionId(string subscriptionId)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var URL = string.Empty;

            try
            {
                var dbssCustomerId = subscriptionId.Replace("a", "");
                URL = string.Format(DBSSAPIUrls.SubscriptionURL, dbssCustomerId);
                var demoJson = AppSettings.DemoPrePostType == "POST"
                    ? ResponseHelper.SubscriptionResponsePOST
                    : ResponseHelper.SubscriptionResponsePRE;

                var subscription = await dbbsHandler.GetJsonObjectFromUrl<Subscription2>(URL, demoJson, 
                    MethodBase.GetCurrentMethod().Name, "CTID:" + subscriptionId);
                var msisdn = string.Empty;
                if (subscription.Data != null) msisdn = subscription.Data.Attributes.Msisdn.Replace("880", "");
                return msisdn;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "SCID:" + subscriptionId, MethodBase.GetCurrentMethod().Name,
                    MethodBase.GetCurrentMethod().Name, "DBSS");
                throw new Exception("You are not a valid Customer!!!");
            }
        }

        #endregion

        #region Product && Package

        public async Task<Product> GetAvailableProduct(string subscriptionId)
        {
            Stopwatch watch = Stopwatch.StartNew();
            var jsonResponse = string.Empty;

            double rtt = 0;
            Product product = null;
            var URL = string.Empty;
            try
            {
                URL = string.Format(DBSSAPIUrls.AvailableProductURL, subscriptionId);
                product = await dbbsHandler.GetJsonObjectFromUrl<Product>(URL, ResponseHelper.AvailableProductResponse,
                    MethodBase.GetCurrentMethod().Name, "SCID:" + subscriptionId);
                if (product.Errors != null) product.HandleError();

                rtt = watch.ElapsedMilliseconds;
                return product;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "SCID:" + subscriptionId, MethodBase.GetCurrentMethod().Name, URL,
                    "DBSS", request: URL);
                throw new Exception("Subscriber not eligible for any packages!!!");
            }
        }

        public async Task<List<ProductPackByPackType>> GetServicePackByPackType(string subscriptionId,
            params string[] filter)
        {
            var availableProductList = new List<ProductPackByPackType>();
            Stopwatch watch = Stopwatch.StartNew();
            var jsonResponse = string.Empty;

            double rtt = 0;
            Product product = null;
            var URL = string.Empty;
            try
            {
                URL = string.Format(DBSSAPIUrls.ConnectedProductURL, subscriptionId);
                product = await dbbsHandler.GetJsonObjectFromUrl<Product>(URL, ResponseHelper.AvailableProductResponse,
                    MethodBase.GetCurrentMethod().Name, "SCID:" + subscriptionId);
                if (product.Errors != null) product.HandleError();

                if (product.Datas.Count() > 0)
                {
                    foreach (var item in product.Datas)
                    {
                        var availableProduct = new ProductPackByPackType();
                        availableProduct.ProductName = item.Attributes.Name != null
                            ? item.Attributes.Name.En ?? string.Empty
                            : string.Empty;
                        //availableProduct.ProductName = item.Attributes.Name.En ?? string.Empty;
                        availableProduct.ProductLongInfoText = item.Attributes.LongInfoText != null
                            ? item.Attributes.LongInfoText.En ?? string.Empty
                            : string.Empty; //item.Attributes.LongInfoText.En ?? string.Empty;
                        availableProduct.ProductShortDescription = item.Attributes.ShortDescription != null
                            ? item.Attributes.ShortDescription.En ?? string.Empty
                            : string.Empty; //item.Attributes.ShortDescription.En ?? string.Empty;
                        availableProduct.ProductLongDescription = item.Attributes.Name != null
                            ? item.Attributes.Name.En ?? string.Empty
                            : string.Empty; //item.Attributes.Name.En ?? string.Empty;
                        availableProduct.ProductInfoText = item.Attributes.Name != null
                            ? item.Attributes.Name.En ?? string.Empty
                            : string.Empty; //item.Attributes.Name.En ?? string.Empty;
                        availableProduct.ProductMediumDescription = item.Attributes.Name != null
                            ? item.Attributes.Name.En ?? string.Empty
                            : string.Empty; //item.Attributes.Name.En ?? string.Empty;
                        availableProduct.ProductCode = !string.IsNullOrEmpty(item.Attributes.Code)
                            ? item.Attributes.Code
                            : string.Empty; //item.Attributes.Code ?? string.Empty;
                        availableProduct.PeriodicAmount = item.Attributes.PeriodicAmount > 0
                            ? Convert.ToDouble(item.Attributes.PeriodicAmount)
                            : 0; //Convert.ToDouble(item.Attributes.PeriodicAmount);
                        availableProduct.IsConfigurable = item.Attributes.IsConfigurable;
                        availableProduct.PeriodicUnit = !string.IsNullOrEmpty(item.Attributes.PeriodicUnit)
                            ? item.Attributes.PeriodicUnit
                            : string.Empty; //item.Attributes.PeriodicUnit ?? string.Empty;
                        availableProduct.AllowReActivation = item.Attributes.AllowReActivation;
                        availableProduct.ChargeType = !string.IsNullOrEmpty(item.Attributes.ChargeType)
                            ? item.Attributes.ChargeType
                            : string.Empty; //item.Attributes.ChargeType ?? string.Empty;
                        availableProduct.DisplayOrder =
                            item.Attributes.DisplayOrder > 0
                                ? item.Attributes.DisplayOrder
                                : 0; //item.Attributes.DisplayOrder;

                        if (item.Relationships.ProductFamily.Data != null)
                        {
                            availableProduct.ProductFamilyID =
                                !string.IsNullOrEmpty(item.Relationships.ProductFamily.Data.Id)
                                    ? item.Relationships.ProductFamily.Data.Id
                                    : string.Empty; //item.Relationships.ProductFamily.Data.Id;
                            availableProduct.ProductFamilyName =
                                !string.IsNullOrEmpty(item.Relationships.ProductFamily.Data.Type)
                                    ? item.Relationships.ProductFamily.Data.Type
                                    : string.Empty; //item.Relationships.ProductFamily.Data.Type ?? string.Empty;
                        }
                        else
                        {
                            availableProduct.ProductFamilyID = string.Empty;
                            availableProduct.ProductFamilyName = string.Empty;
                        }


                        availableProductList.Add(availableProduct);
                    }

                    if (filter.Length > 0)
                    {
                        var filterItemProductList = new List<ProductPackByPackType>();
                        filterItemProductList =
                            availableProductList.Where(x => filter.Contains(x.ProductFamilyID)).ToList();
                        availableProductList = filterItemProductList;
                    }
                }

                rtt = watch.ElapsedMilliseconds;
                return availableProductList;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "SCID:" + subscriptionId, MethodBase.GetCurrentMethod().Name, URL,
                    "DBSS", request: URL);
                throw new Exception("Subscriber not eligible for any packages!!!");
            }
        }

        public async Task<DBSSProducts> GetAvailableProductsByFamilyName(string subscriptionId, string familyName)
        {
            Stopwatch watch = Stopwatch.StartNew();
            var URL = string.Empty;
            double rtt = 0;
            try
            {
                var availableProductList = new DBSSProducts();

                var product = new Product();

                URL = string.Concat(
                    string.Format(DBSSAPIUrls.AvailableProductsByFamilyName, subscriptionId, familyName));
                product = await dbbsHandler.GetJsonObjectFromUrl<Product>(URL, ResponseHelper.BundlePackListResponse,
                    MethodBase.GetCurrentMethod().Name, "SCID:" + subscriptionId);

                foreach (var item in product.Datas)
                {
                    var availableProduct = new AvailableProduct();
                    availableProduct.ProductId = item.Id;
                    availableProduct.ProductName = item.Attributes.Name != null
                        ? item.Attributes.Name.En ?? string.Empty
                        : string.Empty;
                    //availableProduct.ProductName = item.Attributes.Name.En ?? string.Empty;
                    availableProduct.ProductLongInfoText = item.Attributes.LongInfoText != null
                        ? item.Attributes.LongInfoText.En ?? string.Empty
                        : string.Empty; //item.Attributes.LongInfoText.En ?? string.Empty;
                    availableProduct.ProductShortDescription = item.Attributes.ShortDescription != null
                        ? item.Attributes.ShortDescription.En ?? string.Empty
                        : string.Empty; //item.Attributes.ShortDescription.En ?? string.Empty;
                    availableProduct.ProductLongDescription = item.Attributes.Name != null
                        ? item.Attributes.Name.En ?? string.Empty
                        : string.Empty; //item.Attributes.Name.En ?? string.Empty;
                    availableProduct.ProductInfoText = item.Attributes.Name != null
                        ? item.Attributes.Name.En ?? string.Empty
                        : string.Empty; //item.Attributes.Name.En ?? string.Empty;
                    availableProduct.ProductMediumDescription = item.Attributes.Name != null
                        ? item.Attributes.Name.En ?? string.Empty
                        : string.Empty; //item.Attributes.Name.En ?? string.Empty;
                    availableProduct.ProductCode = !string.IsNullOrEmpty(item.Attributes.Code)
                        ? item.Attributes.Code
                        : string.Empty; //item.Attributes.Code ?? string.Empty;
                    availableProduct.PeriodicAmount = item.Attributes.PeriodicAmount > 0
                        ? Convert.ToDouble(item.Attributes.PeriodicAmount)
                        : 0; //Convert.ToDouble(item.Attributes.PeriodicAmount);
                    availableProduct.IsConfigurable = item.Attributes.IsConfigurable;
                    availableProduct.PeriodicUnit = !string.IsNullOrEmpty(item.Attributes.PeriodicUnit)
                        ? item.Attributes.PeriodicUnit
                        : string.Empty; //item.Attributes.PeriodicUnit ?? string.Empty;
                    availableProduct.AllowReActivation = item.Attributes.AllowReActivation;
                    availableProduct.ChargeType = !string.IsNullOrEmpty(item.Attributes.ChargeType)
                        ? item.Attributes.ChargeType
                        : string.Empty; //item.Attributes.ChargeType ?? string.Empty;
                    availableProduct.DisplayOrder =
                        item.Attributes.DisplayOrder > 0
                            ? item.Attributes.DisplayOrder
                            : 0; //item.Attributes.DisplayOrder;

                    if (item.Relationships.ProductFamily.Data != null)
                    {
                        availableProduct.ProductFamilyID =
                            !string.IsNullOrEmpty(item.Relationships.ProductFamily.Data.Id)
                                ? item.Relationships.ProductFamily.Data.Id
                                : string.Empty; //item.Relationships.ProductFamily.Data.Id;
                        availableProduct.ProductFamilyName =
                            !string.IsNullOrEmpty(item.Relationships.ProductFamily.Data.Type)
                                ? item.Relationships.ProductFamily.Data.Type
                                : string.Empty; //item.Relationships.ProductFamily.Data.Type ?? string.Empty;
                    }
                    else
                    {
                        availableProduct.ProductFamilyID = string.Empty;
                        availableProduct.ProductFamilyName = string.Empty;
                    }

                    availableProductList.Add(availableProduct);
                }

                if (product.Errors != null) product.HandleError();

                rtt = watch.ElapsedMilliseconds;
                return availableProductList;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "SCID:" + subscriptionId, MethodBase.GetCurrentMethod().Name, URL,
                    "DBSS", request: URL);
                throw new Exception("Subscriber not eligible for any packages!!!");
            }
        }

        public async Task<Product> GetAllAvailableProducts(string subscriptionId)
        {
            Stopwatch watch = Stopwatch.StartNew();
            var URL = string.Empty;
            double rtt = 0;

            try
            {
                var response = new Product();

                URL = string.Concat(string.Format(DBSSAPIUrls.AvailableProducts, subscriptionId));
                response = await dbbsHandler.GetJsonObjectFromUrl<Product>(URL, ResponseHelper.AvailableProductsJson,
                    MethodBase.GetCurrentMethod().Name, "SCID:" + subscriptionId);
                if (response.Errors != null) response.HandleError();

                rtt = watch.ElapsedMilliseconds;
                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "SCID:" + subscriptionId, MethodBase.GetCurrentMethod().Name, URL,
                    "DBSS", request: URL);
                throw new Exception("Subscriber not eligible for any packages!!!");
            }
        }

        public async Task<Product> GetAllAvailableProductsByChannel(string subscriptionId, string chanelName)
        {
            Stopwatch watch = Stopwatch.StartNew();
            var URL = string.Empty;
            double rtt = 0;

            try
            {
                var response = new Product();
                URL = string.Concat(string.Format(DBSSAPIUrls.AvailableProductsByChanel, subscriptionId, chanelName));
                response = await dbbsHandler.GetJsonObjectFromUrl<Product>(URL, ResponseHelper.AvailableProductsJson,
                    MethodBase.GetCurrentMethod().Name, "SCID:" + subscriptionId);
                if (response.Errors != null) response.HandleError();

                rtt = watch.ElapsedMilliseconds;
                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "SCID:" + subscriptionId, MethodBase.GetCurrentMethod().Name, URL,
                    "DBSS", request: URL);
                throw new Exception("Subscriber not eligible for any packages!!!");
            }
        }

        public async Task<DBSSProducts> GetBulkAvailableProducts(string msisdn, string companyName)
        {
            Stopwatch watch = Stopwatch.StartNew();
            var URL = string.Empty;
            double rtt = 0;

            try
            {
                var availableProductList = new DBSSProducts();
                var response = new DBSSBulkAvailableDataProduct();

                URL = string.Concat(string.Format(DBSSAPIUrls.BulkAvailableDataProduct, msisdn, companyName));
                response = await dbbsHandler.GetJsonObjectFromUrl<DBSSBulkAvailableDataProduct>(URL,
                    ResponseHelper.BulkAvailableDataProductJson, MethodBase.GetCurrentMethod().Name, msisdn);
                if (response != null && response.Includeds != null)
                {
                    foreach (var item in response.Includeds)
                    {
                        var availableProduct = new AvailableProduct();
                        availableProduct.ProductId = item.Id.ToString();
                        availableProduct.ProductName = item.Attributes.Name != null
                            ? item.Attributes.Name.En ?? string.Empty
                            : string.Empty;
                        //availableProduct.ProductName = item.Attributes.Name.En ?? string.Empty;
                        availableProduct.ProductLongInfoText = item.Attributes.LongInfoText != null
                            ? item.Attributes.LongInfoText.En ?? string.Empty
                            : string.Empty; //item.Attributes.LongInfoText.En ?? string.Empty;
                        availableProduct.ProductShortDescription = item.Attributes.ShortDescription != null
                            ? item.Attributes.ShortDescription.En ?? string.Empty
                            : string.Empty; //item.Attributes.ShortDescription.En ?? string.Empty;
                        availableProduct.ProductLongDescription = item.Attributes.Name != null
                            ? item.Attributes.Name.En ?? string.Empty
                            : string.Empty; //item.Attributes.Name.En ?? string.Empty;
                        availableProduct.ProductInfoText = item.Attributes.Name != null
                            ? item.Attributes.Name.En ?? string.Empty
                            : string.Empty; //item.Attributes.Name.En ?? string.Empty;
                        availableProduct.ProductMediumDescription = item.Attributes.Name != null
                            ? item.Attributes.Name.En ?? string.Empty
                            : string.Empty; //item.Attributes.Name.En ?? string.Empty;
                        availableProduct.ProductCode = !string.IsNullOrEmpty(item.Attributes.Code)
                            ? item.Attributes.Code
                            : string.Empty; //item.Attributes.Code ?? string.Empty;
                        availableProduct.PeriodicAmount = item.Attributes.PeriodicAmount > 0
                            ? Convert.ToDouble(item.Attributes.PeriodicAmount)
                            : 0; //Convert.ToDouble(item.Attributes.PeriodicAmount);
                        availableProduct.IsConfigurable = item.Attributes.IsConfigurable ?? false;
                        availableProduct.PeriodicUnit = !string.IsNullOrEmpty(item.Attributes.PeriodicUnit)
                            ? item.Attributes.PeriodicUnit
                            : string.Empty; //item.Attributes.PeriodicUnit ?? string.Empty;
                        availableProduct.AllowReActivation = item.Attributes.AllowReActivation ?? false;
                        availableProduct.ChargeType = !string.IsNullOrEmpty(item.Attributes.ChargeType)
                            ? item.Attributes.ChargeType
                            : string.Empty; //item.Attributes.ChargeType ?? string.Empty;
                        availableProduct.DisplayOrder =
                            item.Attributes.DisplayOrder != null
                                ? item.Attributes.DisplayOrder.Value
                                : 0; //item.Attributes.DisplayOrder;

                        if (item.Relationships.ProductFamily != null && item.Relationships.ProductFamily.Data != null)
                        {
                            availableProduct.ProductFamilyID =
                                !string.IsNullOrEmpty(item.Relationships.ProductFamily.Data.Id)
                                    ? item.Relationships.ProductFamily.Data.Id
                                    : string.Empty; //item.Relationships.ProductFamily.Data.Id;
                            availableProduct.ProductFamilyName =
                                !string.IsNullOrEmpty(item.Relationships.ProductFamily.Data.Type)
                                    ? item.Relationships.ProductFamily.Data.Type
                                    : string.Empty; //item.Relationships.ProductFamily.Data.Type ?? string.Empty;
                        }
                        else
                        {
                            availableProduct.ProductFamilyID = string.Empty;
                            availableProduct.ProductFamilyName = string.Empty;
                        }

                        availableProductList.Add(availableProduct);
                    }

                }

                if (response != null && response.Errors != null) response.HandleError();

                rtt = watch.ElapsedMilliseconds;
                return availableProductList;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, msisdn, MethodBase.GetCurrentMethod().Name, URL, "DBSS",
                    request: URL);
                throw new Exception("Subscriber not eligible for any packages!!!");
            }
        }

        public async Task<Product> GetAvailableLoanProducts(string subscriptionId)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var URL = string.Empty;
            var response = new Product();

            try
            {
                URL = string.Concat(string.Format(DBSSAPIUrls.AvailableLoanProducts, subscriptionId));
                response = await dbbsHandler.GetJsonObjectFromUrl<Product>(URL, ResponseHelper.BundlePackListResponse,
                    MethodBase.GetCurrentMethod().Name, "SCID:" + subscriptionId);
                if (response.Errors != null) response.HandleError();

                rtt = watch.ElapsedMilliseconds;
                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "SCID:" + subscriptionId, MethodBase.GetCurrentMethod().Name, URL,
                    "DBSS");
                throw new Exception("You are not eligible for taking loan!!!");
            }
        }

        public async Task<PackageMigrationResponse> PackageMigrationList(string msisdn, string include)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var URL = string.Empty;

            try
            {
                var response = new PackageMigrationResponse();

                URL = string.Concat(string.Format(DBSSAPIUrls.PackageMigrationInfoListURL, msisdn, include));
                response = await dbbsHandler.GetJsonObjectFromUrl<PackageMigrationResponse>(URL,
                    ResponseHelper.PackageMigrationInfoListResponseJson, MethodBase.GetCurrentMethod().Name,
                    msisdn);
                if (response.Errors != null) response.HandleError();

                rtt = watch.ElapsedMilliseconds;
                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, msisdn, MethodBase.GetCurrentMethod().Name, URL, "DBSS",
                    request: URL);
                throw new Exception("Subscriber not eligible for any package migration!!!");
            }
        }

        public async Task<ConnectedProductsViewModel> GetConnectedProducts(string msisdn)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var URL = string.Empty;

            try
            {
                var response = new ConnectedProductsViewModel();

                URL = string.Format(DBSSAPIUrls.ConnectedProductURL2, msisdn);
                response = await dbbsHandler.GetJsonObjectFromUrl<ConnectedProductsViewModel>(URL,
                    ResponseHelper.ConnectedProductViewModelStrings, MethodBase.GetCurrentMethod().Name,
                    "SCID:" + msisdn);
                if (response.Errors != null) response.HandleError();

                rtt = watch.ElapsedMilliseconds;
                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "SCID:" + msisdn, MethodBase.GetCurrentMethod().Name, URL, "DBSS",
                    request: URL);
                throw new Exception("Subscriber not availed any product yet!!!");
            }
        }

        #endregion

        #region Balance && BILL

        public async Task<PrepaidSubscriptionBalanceResponse> GetSubscriptionBalancePrePaid(string requesterMSISDN)
        {
            //CmsRequestLog requestLog = new CmsRequestLog();
            Stopwatch watch = Stopwatch.StartNew();
            var URL = string.Empty;

            try
            {

                var response = new PrepaidSubscriptionBalanceResponse();

                URL = string.Format(DBSSAPIUrls.SubscriptionBalanceURL, requesterMSISDN);
                //URL = "http://172.16.11.52/home/dbss";

                //requestLog.Name = $"{AppSettings.Provider}.Connector.DBSS.GetSubscriptionBalancePrePaid";
                //requestLog.RequestDate = DateTime.Now;
                //requestLog.Msisdn = requesterMSISDN;
                //requestLog.Url = URL;

                response = await dbbsHandler.GetJsonObjectFromUrl<PrepaidSubscriptionBalanceResponse>(URL,
                    ResponseHelper.SubscriptionBalancePrePaidResponseJson,
                    MethodBase.GetCurrentMethod()?.Name, requesterMSISDN);
                
                if (response.Errors != null) response.HandleError();
                
                return response;
            }
            catch (Exception ex)
            {

                // This error log not to write because already been written in previous/inner Method
                //await logWriter.ToErrorLog(ex, rtt, requesterMSISDN, MethodBase.GetCurrentMethod().Name, URL, "DBSS");

                //if (LogConfig.IsWriteTPSRequestLog)
                //{
                //    requestLog.Rtt = watch.ElapsedMilliseconds;
                //    requestLog.ResponseDate = DateTime.Now;
                //    requestLog.Error = ex.Message;

                //    new BOrequestlog().Save(requestLog);
                //}

                //new LogWriter().ToTextFileLog(ex, watch.ElapsedMilliseconds, requesterMSISDN, URL, "BODY:  " + string.Empty, ex.Message);
                throw ex;
            }
        }

        public async Task<BillingReportResponseNew> GetBillingReport(string subscriptionId, string billingAccountType)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var URL = string.Empty;

            try
            {
                URL = string.Format(DBSSAPIUrls.BillingReportUrlNew, subscriptionId, billingAccountType);
                var customer = await dbbsHandler.GetJsonObjectFromUrl<BillingReportResponseNew>(URL,
                    ResponseHelper.BillingReportResponseJson, MethodBase.GetCurrentMethod().Name,
                    "SCID:" + subscriptionId);
                if (customer.Errors != null) customer.HandleError();
                return customer;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "SCID:" + subscriptionId, MethodBase.GetCurrentMethod().Name, URL,
                    "DBSS");
                throw new Exception("Unable to process your request, please try again!!!");
            }
        }

        public async Task<CombinedUsageReport> GetCurrentMonthUsageReport(string subscriptionId)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var URL = string.Empty;
            var jsonResponse = string.Empty;
            try
            {
                CombinedUsageReport combinedUsageReport = null;
                // if (combinedUsageReport != null) return combinedUsageReport;
                URL = string.Format(DBSSAPIUrls.CurrentMonthUsageReportsURL, subscriptionId);
                combinedUsageReport = await dbbsHandler.GetJsonObjectFromUrl<CombinedUsageReport>(URL,
                    ResponseHelper.CombinedUsageReportResponse, MethodBase.GetCurrentMethod().Name,
                    "SCID:" + subscriptionId);
                if (combinedUsageReport != null && combinedUsageReport.Errors != null)
                    combinedUsageReport.HandleError();

                rtt = watch.ElapsedMilliseconds;
                return combinedUsageReport;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "SCID:" + subscriptionId, MethodBase.GetCurrentMethod()?.Name, URL,
                    "DBSS");
                throw new Exception("Unable to process your request, please try again!!!");
            }
            finally{
                watch.Stop();
            }
        }

        public async Task<CombinedUsageReport> GetPreviousMonthUsageReport(string subscriptionId)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var URL = string.Empty;
            var jsonResponse = string.Empty;

            try
            {
                CombinedUsageReport combinedUsageReport = null;
                //   if (combinedUsageReport != null) return combinedUsageReport;
                URL = string.Format(DBSSAPIUrls.PreviousMonthUsageReportsURL, subscriptionId);
                combinedUsageReport = await dbbsHandler.GetJsonObjectFromUrl<CombinedUsageReport>(URL,
                    ResponseHelper.CombinedUsageReportResponse, MethodBase.GetCurrentMethod()?.Name,
                    "SCID:" + subscriptionId);
                if (combinedUsageReport != null && combinedUsageReport.Errors != null)
                    combinedUsageReport.HandleError();

                rtt = watch.ElapsedMilliseconds;
                return combinedUsageReport;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "SCID:" + subscriptionId, MethodBase.GetCurrentMethod()?.Name, URL,
                    "DBSS");
                throw new Exception("Unable to process your request, please try again!!!");
            }
        }

        public async Task<RevenueStatus> GetRevenueStatus(string customerId)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var URL = string.Empty;
            var jsonResponse = string.Empty;

            try
            {
                RevenueStatus revenueStatus = null;

                //string cacheKey = String.Concat(UtilityHelper.CacheKeys.AvailableProduct, msisdn);
                //revenueStatus = new CachingHelper<RevenueStatus>().Get<RevenueStatus>(cacheKey, out rtt);

                //if (revenueStatus != null) return revenueStatus;
                URL = string.Format(DBSSAPIUrls.RevenueStatusURL, customerId);
                revenueStatus = await dbbsHandler.GetJsonObjectFromUrl<RevenueStatus>(URL,
                    ResponseHelper.RevenueStatusResponse, MethodBase.GetCurrentMethod()?.Name,
                    "CTID:" + customerId);


                // Call Inner API
                if (revenueStatus?.Errors != null) revenueStatus.HandleError();
                //foreach (var item in revenueStatus.Data)
                //{
                //    item.Relationships.ProductFamilies = GetProductFamily(Convert.ToString(item.Id));
                //    item.Relationships.ProductFees = GetProductFees(Convert.ToString(item.Id));
                //    item.Relationships.TrafficBundles = GetProductTrafficBundles(Convert.ToString(item.Id));
                //}

                //new CachingHelper<RevenueStatus>().Put<RevenueStatus>(cacheKey, revenueStatus, UtilityHelper.CacheValidity.SmallReponse);

                //
                rtt = watch.ElapsedMilliseconds;
                return revenueStatus;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "CTID:" + customerId, MethodBase.GetCurrentMethod()?.Name, URL,
                    "DBSS");
                throw new Exception("Unable to process your request, please try again!!!");
            }
        }

        public async Task<Invoice> GetInvoice(string subscriptionId, string startDate, string endDate)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var URL = string.Empty;

            try
            {
                var response = new Invoice();
                URL = string.Concat(string.Format(DBSSAPIUrls.Invoice, subscriptionId, startDate, endDate));
                response = await dbbsHandler.GetJsonObjectFromUrl<Invoice>(URL, ResponseHelper.BillReportResponse,
                    MethodBase.GetCurrentMethod().Name, "SCID:" + subscriptionId);
                if (response.Errors != null) response.HandleError();

                rtt = watch.ElapsedMilliseconds;
                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "SCID:" + subscriptionId, MethodBase.GetCurrentMethod().Name, URL,
                    "DBSS");
                throw new Exception("Unable to process your request, please try again!!!");
            }
        }

        public async Task<ItemizedBillUrlResponse> GetItemizedBillUrl(string subscriptionId, DateTime startDate,
            DateTime endDate, string billingAccountType)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var URL = string.Empty;

            try
            {
                // return JsonConvert.DeserializeObject<ItemizedBillUrlResponse>(ResponseHelper.ItemWiseBillResponseJson);
                if (!string.IsNullOrWhiteSpace(billingAccountType))
                    URL = string.Concat(string.Format(DBSSAPIUrls.ItemizedBillUrl, subscriptionId,
                        startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd"), billingAccountType));
                else
                    URL = string.Concat(string.Format(DBSSAPIUrls.ItemizedBillUrl2, subscriptionId,
                        startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd")));

                var response = await dbbsHandler.GetJsonObjectFromUrl<ItemizedBillUrlResponse>(URL, string.Empty,
                    MethodBase.GetCurrentMethod().Name, "SCID:" + subscriptionId);
                if (response.Errors != null) response.HandleError();
                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "SCID:" + subscriptionId, MethodBase.GetCurrentMethod().Name, URL,
                    "DBSS");
                throw new Exception("Unable to process your request, please try again!!!");
            }
        }

        public async Task<PreviousMonthBillResponse> GetPreviousMonthBill(string subscriptionId, DateTime startDate,
            DateTime endDate)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var URL = string.Empty;

            try
            {
                URL = string.Concat(string.Format(DBSSAPIUrls.PreviousBillUrl, subscriptionId,
                    startDate.ToString("yyyy-MM-dd"), endDate.ToString("yyyy-MM-dd")));

                var response = await dbbsHandler.GetJsonObjectFromUrl<PreviousMonthBillResponse>(URL,
                    ResponseHelper.PreviousMonthBillResponseJson, MethodBase.GetCurrentMethod().Name,
                    "SCID:" + subscriptionId);
                if (response.Errors != null) response.HandleError();
                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "SCID:" + subscriptionId, MethodBase.GetCurrentMethod().Name, URL,
                    "DBSS");
                throw new Exception("Unable to process your request, please try again!!!");
            }
        }
        //public BillingReportResponse GetBillingReport(string subscriptionId, string billingReportId)
        //{
        //    DateTime startTime = DateTime.Now;
        //    double rtt = 0;
        //    string URL = string.Empty;

        //    try
        //    {
        //        //return JsonConvert.DeserializeObject<BillingReportResponse>(ResponseHelper.BillReportResponse);
        //        string url = String.Concat(string.Format(DBSSAPIUrls.BillingReportUrl, subscriptionId, billingReportId));
        //        BillingReportResponse response = url.GetJsonObjectFromUrl<BillingReportResponse>(url, MethodBase.GetCurrentMethod().Name, "SCID:" + subscriptionId);
        //        if (response.Errors != null)
        //        {
        //            response.HandleError();
        //        }
        //        return response;
        //    }
        //    catch (Exception ex)
        //    {
        //        rtt = watch.ElapsedMilliseconds;
        //        await logWriter.ToErrorLog(rtt, "SCID:" + subscriptionId, MethodBase.GetCurrentMethod().Name, URL, "DBSS");
        //        throw new Exception("Unable to process your request, please try again!!!");
        //    }
        //}

        #endregion

        #region USAGE & RECHARGE/PAYMENT

        public async Task<MsisdnHistoryResponse> CallHistory(string subscriptionId, string filter,
            string transactiontype)
        {
            Stopwatch watch = Stopwatch.StartNew();
            var URL = string.Empty;
            double rtt = 0;

            try
            {
                var response = new MsisdnHistoryResponse();
                URL = string.Concat(string.Format(DBSSAPIUrls.CallSmsHistory, subscriptionId, filter, transactiontype));
                response = await dbbsHandler.GetJsonObjectFromUrl<MsisdnHistoryResponse>(URL,
                    ResponseHelper.CallHistoryResponse, MethodBase.GetCurrentMethod().Name,
                    "SCID:" + subscriptionId);
                if (response.Errors != null) response.HandleError();

                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "SCID:" + subscriptionId, MethodBase.GetCurrentMethod().Name, URL,
                    "DBSS", request: URL);
                throw new Exception("No Usage History Found!!!");
            }
        }

        public async Task<MsisdnHistoryResponse> CallHistory(string subscriptionId, string filter)
        {
            Stopwatch watch = Stopwatch.StartNew();
            var URL = string.Empty;
            double rtt = 0;
            try
            {
                var response = new MsisdnHistoryResponse();
                URL = string.Concat(string.Format(DBSSAPIUrls.CallSmsHistory2, subscriptionId, filter));
                response = await dbbsHandler.GetJsonObjectFromUrl<MsisdnHistoryResponse>(URL,
                    ResponseHelper.CallHistoryResponse, MethodBase.GetCurrentMethod().Name,
                    "SCID:" + subscriptionId);
                if (response.Errors != null) response.HandleError();
                rtt = watch.ElapsedMilliseconds;

                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "SCID:" + subscriptionId, MethodBase.GetCurrentMethod().Name, URL,
                    "DBSS", request: URL);
                throw new Exception("No Usage History Found!!!");
            }
        }

        public async Task<SubscriptionChargeResponse> GetSubscriptionCharge(string subscriptionId, string status,
            string includes)
        {
            Stopwatch watch = Stopwatch.StartNew();
            var URL = string.Empty;
            double rtt = 0;

            try
            {
                var response = new SubscriptionChargeResponse();

                URL = string.Concat(string.Format(DBSSAPIUrls.SubscriptionCharge, subscriptionId, status, includes));
                response = await dbbsHandler.GetJsonObjectFromUrl<SubscriptionChargeResponse>(URL,
                    ResponseHelper.SubscriptionChargeResponse, MethodBase.GetCurrentMethod().Name,
                    "SCID:" + subscriptionId);
                if (response.Errors != null) response.HandleError();

                rtt = watch.ElapsedMilliseconds;
                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "SCID:" + subscriptionId, MethodBase.GetCurrentMethod().Name, URL,
                    "DBSS", request: URL);
                throw new Exception("No Charge History Found!!!");
            }
        }

        public async Task<PrepaidRechargeResponse> GetRechargeHistory(DateTime fromDate, DateTime toDate,
            string subscriptionId)
        {
            Stopwatch watch = Stopwatch.StartNew();
            var URL = string.Empty;
            double rtt = 0;

            try
            {
                URL = string.Concat(string.Format(DBSSAPIUrls.RechargeHistoryURL, subscriptionId,
                    fromDate.ToString("yyyy-MM-dd"), toDate.ToString("yyyy-MM-dd")));
                //response = await dbbsHandler.GetJsonObjectFromUrl<RechargeResponse>(ResponseHelper.RechargeHistoryResponseJson);
                var response = await dbbsHandler.GetJsonObjectFromUrl<PrepaidRechargeResponse>(URL,
                    ResponseHelper.RechargeHistoryResponseJsonV2, MethodBase.GetCurrentMethod().Name,
                    "SCID:" + subscriptionId);
                if (response.Errors != null) response.HandleError();

                rtt = watch.ElapsedMilliseconds;
                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "SCID:" + subscriptionId, MethodBase.GetCurrentMethod().Name, URL,
                    "DBSS", request: URL);
                throw new Exception("No Recharge History Found!!!");
            }
        }

        public async Task<PaymentResponse> GetPayment(string customerId, DateTime startDate, DateTime endDate)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var URL = string.Empty;
            try
            {
                var response = new PaymentResponse();
                URL = string.Concat(string.Format(DBSSAPIUrls.PaymentURL, customerId, startDate.ToString("yyyy-MM-dd"),
                    endDate.ToString("yyyy-MM-dd")));
                response = await dbbsHandler.GetJsonObjectFromUrl<PaymentResponse>(URL, ResponseHelper.PaymentResponse,
                    MethodBase.GetCurrentMethod().Name, "CTID:" + customerId);
                if (response.Errors != null) response.HandleError();

                rtt = watch.ElapsedMilliseconds;
                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "CTID:" + customerId, MethodBase.GetCurrentMethod().Name, URL,
                    "DBSS", request: URL);
                throw new Exception("No Payment History Found!!!");
            }
        }

        #endregion

        #region FnF & SFnF

        public async Task<FnfSummaryDbssResponse> GetFNFList(string requesterMSISDN)
        {
            Stopwatch watch = Stopwatch.StartNew();
            var URL = string.Empty;
            double rtt = 0;

            try
            {
                var include = "services";

                URL = string.Format(DBSSAPIUrls.SubscriptionsURLByMSISDN, requesterMSISDN, include);
                var response = await dbbsHandler.GetJsonObjectFromUrl<FnfSummaryDbssResponse>(URL,
                    ResponseHelper.FnfListResponse, MethodBase.GetCurrentMethod().Name, requesterMSISDN);
                if (response.Errors != null) response.HandleError();

                rtt = watch.ElapsedMilliseconds;
                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, requesterMSISDN, MethodBase.GetCurrentMethod().Name, URL, "DBSS",
                    request: URL);
                throw new Exception("No FnF/SFnF Found!!!");
            }
        }

        public async Task<DBSSCommandResponse> PATCHFnfRequest(string requesterMSISDN,
            Dictionary<string, string> fnfNumberList, string productId, string fnfServiceId, string bosUserName)
        {
            Stopwatch watch = Stopwatch.StartNew();
            var URL = string.Empty;
            var jsonRequest = string.Empty;
            double rtt = 0;

            try
            {
                var model = new FNFRquest();
                var data = new FNFRquest.FNFRquestData();
                var meta = new FNFRquest.Meta();
                var service = new FNFRquest.FNFServices();

                var fnfModel = new Dictionary<string, object>();
                fnfModel.Add(fnfServiceId, fnfNumberList);

                meta.Channel = bosUserName;
                meta.ServiceParameters = fnfModel; //fnfNumberList;
                data.Type = "products";
                data.Id = fnfServiceId;
                data.Meta = meta;
                model.Data = data;

                var response = new DBSSCommandResponse();
                URL = string.Concat(string.Format(DBSSAPIUrls.DataPackURL, requesterMSISDN));
                jsonRequest = JsonConvert.SerializeObject(model);

                response = await dbbsHandler.PatchRequest<DBSSCommandResponse>(URL, jsonRequest, contentTypeJson,
                    MethodBase.GetCurrentMethod().Name, requesterMSISDN);

                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, requesterMSISDN, MethodBase.GetCurrentMethod().Name,
                    "URL:" + URL + "Body:" + jsonRequest, "DBSS", request: "URL:" + URL + "Body:" + jsonRequest);
                throw new Exception("Unable to process your operation, please try again!!! ");
            }
        }

        #endregion

        #region DATA/BUNDAL PACK POST & PATCH & DELETE

        [Obsolete("please use DataPackBySubscription instead of DataPack", false)]
        public async Task<DBSSCommandResponse> DataPack(string productId, string msisdn, string bosUserName)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var jsonRequest = string.Empty;
            var URL = string.Empty;

            try
            {
                jsonRequest = new DBSSCommonRequestBody().GetJson(productId, "products", bosUserName);
                var response = new DBSSCommandResponse();
                URL = string.Concat(string.Format(DBSSAPIUrls.DataPackURL, msisdn));
                response = await dbbsHandler.PostRequest<DBSSCommandResponse>(URL, jsonRequest, contentTypeVND_API_JSON,
                    MethodBase.GetCurrentMethod().Name, msisdn);
                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, msisdn, MethodBase.GetCurrentMethod().Name,
                    "URL:" + URL + "Body:" + jsonRequest, "DBSS", request: "URL:" + URL + "Body:" + jsonRequest);
                throw new Exception("Unable to process your operation, please try again!!! ");
            }
        }

        public async Task<DBSSCommandResponse> DataPackBySubscription(string productId, string subscriptionId,
            string bosUserName)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var jsonRequest = string.Empty;
            var URL = string.Empty;

            try
            {
                jsonRequest = new DBSSCommonRequestBody().GetJson(productId, "products", bosUserName);

                var response = new DBSSCommandResponse();
                URL = string.Concat(string.Format(DBSSAPIUrls.DataPackURLBySubscription, subscriptionId));
                response = await dbbsHandler.PostRequest<DBSSCommandResponse>(URL, jsonRequest, contentTypeVND_API_JSON,
                    MethodBase.GetCurrentMethod().Name, "SCID:" + subscriptionId);
                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "SCID:" + subscriptionId, MethodBase.GetCurrentMethod().Name,
                    "URL:" + URL + "Body:" + jsonRequest, "DBSS", request: "URL:" + URL + "Body:" + jsonRequest);
                throw new Exception("Unable to process your operation, please try again!!! ");
            }
        }

        public async Task<DBSSCommandResponse> DataPackDelete(string productId, string msisdn, string channelName)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var jsonRequest = string.Empty;
            var URL = string.Empty;

            try
            {
                var model = new DeleteDataPackRquest();
                var data = new DeleteDataPackRquest.DataPackRquestData();
                var meta = new DeleteDataPackRquest.Meta();
                var service = new DeleteDataPackRquest.Services();

                meta.Channel = channelName;
                meta.Services = service;
                data.Type = "products";
                data.Id = productId;
                data.Meta = meta;
                model.Data.Add(data);

                var response = new DBSSCommandResponse();
                URL = string.Concat(string.Format(DBSSAPIUrls.DataPackURL, msisdn));
                jsonRequest = JsonConvert.SerializeObject(model);

                //var json = ResponseHelper.DataPackResponseJson
                //var json = await new HttpWebRequestHelper().Delete(URL, jsonRequest, contentTypeJson,AppSettings.DBSSHttpRequestTimeOut);
                response = await new HttpClientFactoryHandler(_clientFactory,clientName).Post<DBSSCommandResponse>(URL, AppSettings.DBSSHttpRequestTimeOut, jsonRequest, null, contentTypeVND_API_JSON);
                //var resp = JsonConvert.DeserializeObject<DBSSCommandResponse>(json);
                //response = resp;

                return response;
            }
            catch (Exception ex)
            {
                //rtt = watch.ElapsedMilliseconds;
                //await logWriter.ToErrorLog(ex, rtt, msisdn, MethodBase.GetCurrentMethod().Name,
                //    "URL:" + URL + "Body:" + jsonRequest, "DBSS", request: "URL:" + URL + "Body:" + jsonRequest);
                //throw new Exception("Unable to process your operation, please try again!!! ");
                throw ex;
            }
        }

        public async Task<DBSSCommandResponse> GiftDataPack(string dataPackGiftCode, string requestFrom,
            string requestTo, string channnelName)
        {
            var requestBody
                = new GiftDataPackRequestBody
                {
                    Data = new GiftDataPackData
                    {
                        Id = dataPackGiftCode,
                        Type = "products",
                        Meta = new GiftDataPackMeta
                        {
                            Channel = channnelName,
                            Services = new GiftDataPackServices
                            {
                                Gifting = new Gifting
                                {
                                    GiftTo = requestTo
                                }
                            }
                        }
                    }
                };

            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var jsonRequest = string.Empty;
            var URL = string.Empty;

            try
            {
                var url = string.Concat(string.Format(DBSSAPIUrls.DataPackGiftURL3, requestFrom));
                var requestBodyJson = JsonConvert.SerializeObject(requestBody);

                var response = await dbbsHandler.PostRequest<DBSSCommandResponse>(URL, requestBodyJson,
                    contentTypeVND_API_JSON, MethodBase.GetCurrentMethod().Name, requestFrom);
                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, requestFrom, MethodBase.GetCurrentMethod().Name,
                    "URL:" + URL + "Body:" + jsonRequest, "DBSS", request: "URL:" + URL + "Body:" + jsonRequest);
                throw new Exception("Unable to process your operation, please try again!!! ");
            }
        }

        public async Task<DBSSCommandResponse> TransferDataPack(string dataPackGiftCode, string requestFrom,
            string requestTo, string channelName, string subscriptionId)
        {
            var requestBody
                = new GiftVolumeRequestBody
                {
                    Data = new GiftVolumeRequestBodyData
                    {
                        Id = dataPackGiftCode,
                        Type = "volume",
                        Meta = new GiftVolumeRequestBodyMeta
                        {
                            Channel = channelName,
                            Services = new GiftVolumeRequestBodyServices
                            {
                                Transfer = new Transfer
                                {
                                    GiftTo = requestTo
                                }
                            }
                        }
                    }
                };

            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var jsonRequest = string.Empty;
            var URL = string.Empty;

            try
            {
                URL = string.Concat(string.Format(DBSSAPIUrls.DataPackGiftURL2, subscriptionId));
                jsonRequest = JsonConvert.SerializeObject(requestBody);
                var response = await dbbsHandler.PostRequest<DBSSCommandResponse>(URL, jsonRequest,
                    contentTypeVND_API_JSON, MethodBase.GetCurrentMethod().Name, requestFrom);
                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, requestFrom, MethodBase.GetCurrentMethod().Name,
                    "URL:" + URL + "Body:" + jsonRequest, "DBSS", request: "URL:" + URL + "Body:" + jsonRequest);
                throw new Exception("Unable to process your operation, please try again!!! ");
            }
        }

        public async Task<DBSSCommandResponse> RequestDataPack(string dataPackGiftCode, string requestFrom,
            string requestTo, string channelName, string subscriptionId)
        {
            var requestBody = new VolumeRequestBody
            {
                Data = new VolumeRequestBody.VolumeRequestData
                {
                    Type = "volume",
                    Id = dataPackGiftCode,
                    Meta = new VolumeRequestBody.VolumeRequestMeta
                    {
                        Channel = channelName,
                        Services = new VolumeRequestBody.VolumeRequestServices
                        {
                            Request = new VolumeRequestBody.VolumeRequestRequest
                            {
                                BMsisdn = requestTo
                            }
                        }
                    }
                }
            };

            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var jsonRequest = string.Empty;
            var URL = string.Empty;

            try
            {
                URL = string.Concat(string.Format(DBSSAPIUrls.DataPackGiftURL2, subscriptionId));
                jsonRequest = JsonConvert.SerializeObject(requestBody);
                var response = await dbbsHandler.PostRequest<DBSSCommandResponse>(URL, jsonRequest,
                    contentTypeVND_API_JSON, MethodBase.GetCurrentMethod().Name, requestFrom);
                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, requestFrom, MethodBase.GetCurrentMethod().Name,
                    "URL:" + URL + "Body:" + jsonRequest, "DBSS", request: "URL:" + URL + "Body:" + jsonRequest);
                throw new Exception("Unable to process your operation, please try again!!! ");
            }
        }

        public async Task<DBSSCommandResponse> BOSCampingRegistration(string productId, string msisdn,
            string bosUserName)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var jsonRequest = string.Empty;
            var URL = string.Empty;

            try
            {
                jsonRequest = new DBSSCommonRequestBody().GetJson(productId, "products", bosUserName);

                var response = new DBSSCommandResponse();
                URL = string.Concat(string.Format(DBSSAPIUrls.ProductsURL, msisdn));
                //var json = await new HttpWebRequestHelper().Post(URL, jsonRequest, contentTypeVND_API_JSON,AppSettings.DBSSHttpRequestTimeOut);
                response = await new HttpClientFactoryHandler(_clientFactory,clientName).Post<DBSSCommandResponse>(URL, AppSettings.DBSSHttpRequestTimeOut, jsonRequest, null, contentTypeVND_API_JSON);
                //response = JsonConvert.DeserializeObject<DBSSCommandResponse>(json);
                return response;
            }
            catch (Exception ex)
            {
                //rtt = watch.ElapsedMilliseconds;
                //await logWriter.ToErrorLog(ex, rtt, msisdn, MethodBase.GetCurrentMethod().Name,
                //    "URL:" + URL + "Body:" + jsonRequest, "DBSS", request: "URL:" + URL + "Body:" + jsonRequest);
                //throw new Exception("Unable to process your operation, please try again!!! ");
                throw ex;
            }
        }

        public async Task<DBSSRequestResponse> GetRequestStatus(string requestId)
        {
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var URL = string.Empty;

            try
            {
                URL = string.Format(DBSSAPIUrls.RequestStatusUrl, requestId);
                var response =
                    await dbbsHandler.GetJsonObjectFromUrl<DBSSRequestResponse>(URL,
                        ResponseHelper.CommandRequestResponseJson,  MethodBase.GetCurrentMethod().Name,
                        "RQID:" + requestId);
                return response;
            }
            catch (Exception ex)
            {
                rtt = watch.ElapsedMilliseconds;
                await logWriter.ToErrorLog(ex, rtt, "RQID:" + requestId, MethodBase.GetCurrentMethod().Name, URL,
                    "DBSS", URL);
                throw new Exception("Unable to process your operation, please try again!!! ");
            }
        }

        public async Task<IEnumerable<BEDataProduct>> GetAllSubscriberDataProduct(string clientID,
            string requesterMSISDN)
        {
            IEnumerable<BEDataProduct> products = null;
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;

            var cacheKey =
                string.Concat(UtilityHelper.CacheKeys.AllAvaliableProduct, "channel", requesterMSISDN); //, userName
            products = new CachingHelper<IEnumerable<BEDataProduct>>().Get<IEnumerable<BEDataProduct>>(cacheKey);


            if (products == null)
            {
                products = new List<BEDataProduct>();

                try
                {
                    var dataProductsBOS = BOConfiguationData.DataProducts.Where(s => s.IsActive);
                    var serviceUser = BOConfiguationData.GetServiceUserByClientID(clientID);

                    var dbssSubscriber = await new DBSSWebAPI(_clientFactory).GetSubscriptionInformation(requesterMSISDN);
                    var dbssProducts = await GetAllAvailableProductsByChanel(requesterMSISDN,
                        dbssSubscriber.SubscriptionID, serviceUser.DBSSUserName);
                    var codes = dbssProducts.Datas.Select(s => s.Attributes.Code);

                    products = dataProductsBOS.Where(s =>
                        codes.Contains(s.DataCode) || !string.IsNullOrWhiteSpace(s.AutoRenewCode) &&
                        codes.Contains(s.AutoRenewCode));

                    new CachingHelper<IEnumerable<BEDataProduct>>().Put(cacheKey, products, AppSettings.MediumResponse);
                }
                catch (Exception ex)
                {
                    await logWriter.ToErrorLog(ex, 0, requesterMSISDN, MethodBase.GetCurrentMethod().Name,
                        "DBSSService." + MethodBase.GetCurrentMethod().Name);
                    throw ex;
                }
            }

            return products;
        }

        public async Task<Product> GetAllAvailableProductsByChanel(string requesterMSISDN, string subscriptionId,
            string chanelName)
        {
            //String dateTimeformat = "MMM ddd d HH:mm yyyy";
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            var dbssAvaliableProducts = new Product();

            var cacheKey = string.Concat(UtilityHelper.CacheKeys.AllAvailableProductsByChanel, requesterMSISDN,
                subscriptionId, chanelName);
            dbssAvaliableProducts = new CachingHelper<Product>().Get<Product>(cacheKey);

            if (dbssAvaliableProducts == null)
                try
                {
                    dbssAvaliableProducts =
                        await new DBSSWebAPI(_clientFactory).GetAllAvailableProductsByChannel(subscriptionId, chanelName);
                    if (dbssAvaliableProducts.Errors != null) dbssAvaliableProducts.HandleError();

                    new CachingHelper<Product>().Put(cacheKey, dbssAvaliableProducts, AppSettings.SmallReponse);
                }
                catch (Exception ex)
                {
                    await logWriter.ToErrorLog(ex, 0, requesterMSISDN, MethodBase.GetCurrentMethod().Name,
                        "DBSSService." + MethodBase.GetCurrentMethod().Name);
                    throw ex;
                }

            return dbssAvaliableProducts;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="msisdn"></param>
        /// <returns></returns>
        //private AccountBalance GetUCIPBalanceInformation(string msisdn)
        //{
        //    AccountBalance daBalance = new AccountBalance();
        //    try
        //    {

        //        if (!string.IsNullOrEmpty(msisdn) && daBalance == null)
        //        {
        //            RetMessage value = new OpenCode().GetBalanceAndDate("880" + msisdn);

        //            if (value.MessageType == MessageType.Success)
        //            {
        //                BalanceInfo info = (BalanceInfo)value.Data;
        //                if (info != null)
        //                {

        //                    daBalance = AccountBalance.MapFromUCIP(info);

        //                }
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //       // SelfCareConnector.Helpers.await logWriter.ToErrorLog(ex, 0, msisdn, MethodBase.GetCurrentMethod().Name, "DBSSService." + MethodBase.GetCurrentMethod().Name, "BOSGW");
        //       // throw new Exception(GetOpenCodeMessage("") + "!");
        //    }
        //    return daBalance;
        //}
    }
}