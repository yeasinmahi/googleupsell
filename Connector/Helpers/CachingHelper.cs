﻿//using Microsoft.Extensions.Caching.Memory;

using System;
using System.Diagnostics;
using System.Runtime.Caching;

//using System.Runtime.Caching;


namespace Connector.Helpers
{
    public class CachingHelper<T>
    {
        private readonly MemoryCache _cache = MemoryCache.Default;

        public T Get<T>(string key)
        {
            //rtt = watch.ElapsedMilliseconds;
            return (T)_cache.Get(key);
        }

        public void Put<T>(string key, T value)
        {
            _cache.Set(key, value, GetCacheItemPolicy(ObjectCache.InfiniteAbsoluteExpiration));
        }

        public void Put<T>(string key, T value, int validityInMin)
        {
            _cache.Set(key, value, GetCacheItemPolicy(DateTimeOffset.UtcNow.AddMinutes(validityInMin)));
        }

        public void Remove(string key)
        {
            _cache.Remove(key);
        }

        private CacheItemPolicy GetCacheItemPolicy(DateTimeOffset absoluteExpiration)
        {
            var cacheItemPolicy = new CacheItemPolicy
            {
                AbsoluteExpiration = absoluteExpiration,
                SlidingExpiration = ObjectCache.NoSlidingExpiration
            };
            return cacheItemPolicy;
        }
    }
}