﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using BusinessEntity.Logging;
using BusinessObject;
using Utility.Extensions;

namespace Connector.Helpers
{
    internal class LogWriter
    {
        private static object _tempFileLocker = new object();
        public async Task ToErrorLog(Exception exception, double rtt, string msisdn, string requestMethod,
            string requestURL = "", string requester = "BOSGW", string response = "", string request = "")
        {
            var provider = AppSettings.Provider;


            try
            {
                try
                {
                    exception.ToTextFileLog(msisdn, requestMethod, requestURL, requester, response,
                        request); //to write text file
                }
                catch (Exception)
                {
                }


                if (string.IsNullOrWhiteSpace(request) && requester.Contains("DBSS")) request = requestURL;

                var message = string.Empty;
                var exceptions = exception.GetAllExceptions();
                foreach (var ex in exceptions)
                {
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(st.FrameCount - 1);

                    if (st.FrameCount == 0 || frame == null) continue;
                    var log = new BEAppErrorLog
                    {
                        Msisdn = msisdn,
                        LogTime = DateTime.Now,
                        Message = ex.Message,
                        Stacktrace = ex.StackTrace,
                        RequestFrom = provider + "-" + requester,
                        RequestMethod = requestMethod,
                        RequestUrl = requestURL,
                        Request = request,
                        Response = response,
                        Rtt = rtt
                        //IsActive = true,
                    };
                    await new BOAppErrorLog().Create(log);
                }
            }
            catch (Exception ex)
            {
                ex.ToTextFileLog(msisdn, requestMethod, requestURL, requester, response, request);
            }
        }
        public void ToTextFileLog(Exception exception, double rtt, string msisdn, string url, string request,
            string response)
        {
            var fileName = "ErrorLog";
            var fileType = "txt";
            var message = string.Empty;
            var exceptions = exception.GetAllExceptions();
            //var startupPath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/{0}/", folderName));
            // var startupPath = RequestLogPath;
            //  var startupPath = "E:\\NewPublish\\APIHub\\Log\\CMSPostRequest\\";  
            var startupPath = Path.Combine(AppSettings.ErrorLogFilePath, "CMSPostRequest");
            Directory.CreateDirectory(startupPath);
            var filePath = Path.Combine(startupPath, fileName + "_" + DateTime.Now.ToString("yyyy-MM-dd") + "." + fileType);
            //if (!Directory.Exists(filePath))
            //{
            //    message += "Date|Time|MSISDN|FileName|MethodName|LineNumber|Message|StackTrace" + Environment.NewLine;
            //}


            foreach (var ex in exceptions)
            {
                var msg = string.Empty;
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(st.FrameCount - 1);

                if (st.FrameCount == 0 || frame == null) continue;
                msg = DateTime.Now.ToString("yyyy-MM-dd") + "|"
                                                          + DateTime.Now.ToString("hh:mm:ss") + "|"
                                                          + msisdn + "|"
                                                          + Path.GetFileName(frame.GetFileName()) + "|"
                                                          + frame.GetMethod()?.Name + "|"
                                                          + frame.GetFileLineNumber() + "|"
                                                          + ex.Message + "|"
                                                          + ex.StackTrace;

                msg += Environment.NewLine;
                message += msg;
            }

            message += "======" + msisdn + "==============" + rtt + "==========================" + Environment.NewLine;
            ;
            message += url + Environment.NewLine;
            message += request + Environment.NewLine;
            message += response + Environment.NewLine;
            lock (_tempFileLocker)
            {
                File.AppendAllText(filePath, message);
            }
        }
        public async Task ToRequestLog(double rtt, string msisdn, string request, string response, string requestMethod,
            string requester = "BOSGW")
        {
            var enableLog = AppSettings.EnableWriteDBBSErrorLog;
            // bool enableLog = true;
            var provider = AppSettings.Provider;
            var logMSISDNS = AppSettings.LOGMSISDNS;
            var msisdns = new List<string>();
            string[] msisdnsArray = null;
            if (!string.IsNullOrWhiteSpace(logMSISDNS)) msisdnsArray = logMSISDNS.Split('|');

            if (msisdnsArray != null && msisdnsArray.Length > 0)
                msisdns = msisdnsArray.ToList();

            if (enableLog)
                await WriteLog(rtt, msisdn, request, response, requestMethod, requester, provider);
            else if (msisdns.Contains(msisdn))
                await WriteLog(rtt, msisdn, request, response, requestMethod, requester, provider);
            else if (rtt > AppSettings.MinimumRTTToLog)
                await WriteLog(rtt, msisdn, request, response, requestMethod, requester, provider);
        }

        private async Task WriteLog(double rtt, string msisdn, string request, string response, string requestMethod,
            string requester, string provider)
        {
            try
            {
                //try
                //{
                //    request.ToRequestResponseLog(response, "--FROM: " + requestMethod + "--GW: " + requester + "--MSISDN: " + msisdn);//to write text file
                //}
                //catch (Exception ex)
                //{
                //}

                var log = new BEAppRequestResponseLog
                {
                    Msisdn = msisdn,
                    // LogTime = DateTime.Now,
                    RequestUri = request,
                    ResponseBody = response,
                    ClienIipAddress = provider + "-" + requester,
                    RequestMethod = requestMethod,
                    RTT = rtt
                };
                await new BOAppRequestResponseLog().Create(log);
            }
            catch (Exception ex)
            {
                await ToErrorLog(ex, rtt, msisdn, requestMethod);
            }
        }
    }
}