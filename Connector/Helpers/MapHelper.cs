﻿//using BusinessEntity;
//using BusinessObject;
//using Connector.Entities.DBSS;
//using System;
//using System.Collections.Generic;
//using System.Configuration;
//using System.Linq;
//using System.Web;

//namespace Connector.Helpers
//{
//    public static class MapHelper
//    {
//        public static bool IsUcip
//        {
//            get
//            {
//                return ConfigurationManager.AppSettings["UsingDaApiFrom"] == "UCIP";
//            }
//        }

//        public static BECurrentDataList GetAllPack(this AccountBalance accountBalance)
//        {

//            items pakItems = new items();

//            #region TAKA && VOICE && MONEY
//            var minScale = IsUcip ? 60 : 1;

//            item taka = new item
//            {
//                key = "Bonus Minutes",
//                ItemList = new items(),
//            };

//            var takaList = accountBalance.DadicatedAccountList.Where(da =>
//                (da.GsmServiceType == "taka" || da.GsmServiceType == "voice" || da.GsmServiceType == "money")
//                && da.RemainingAmount > 0 && da.DadicatedAccountID != 255).Select(da => new item
//                {
//                    key = string.Format("{0:0} Minutes", Math.Round((decimal)da.RemainingAmount / (da.Unit.ToLower() != "min" ? minScale : 1), MidpointRounding.AwayFromZero)) ?? string.Empty,
//                    value = string.Format("Your {1} remaining Bought Minutes is: {0:0} min. It will expire on {2}", Math.Round((decimal)da.RemainingAmount / (da.Unit.ToLower() != "min" ? minScale : 1), MidpointRounding.AwayFromZero), da.ProductName, da.ExpieryDate.ToString("dd-MMM-yyyy")) ?? string.Empty,

//                });

//            if (takaList.Any())
//            {
//                taka.value = "Total Minutes " + accountBalance.DadicatedAccountList.Where(da =>
//                              (da.GsmServiceType == "taka" || da.GsmServiceType == "voice" || da.GsmServiceType == "money")
//                              && da.RemainingAmount > 0 && da.DadicatedAccountID != 255).Sum(da => Math.Round((decimal)da.RemainingAmount / (da.Unit.ToLower() != "min" ? minScale : 1), MidpointRounding.AwayFromZero)).ToString();
//                taka.ItemList.AddRange(takaList);

//            }
//            pakItems.Add(taka);
//            #endregion

//            #region SMS

//            item sms = new item
//            {
//                key = "Bonus SMS",
//                ItemList = new items(),
//            };

//            var smsList = accountBalance.DadicatedAccountList.Where(da =>
//                da.GsmServiceType == "sms" && da.RemainingAmount > 0).Select(da => new item
//                {

//                    key = string.Format("{0:0}", Math.Round((decimal)da.RemainingAmount, MidpointRounding.AwayFromZero)) ?? string.Empty,
//                    value = string.Format("Your {1} remaining Bought SMS is: {0:0}. It will expire on {2}", Math.Round((decimal)da.RemainingAmount, MidpointRounding.AwayFromZero), da.ProductName, da.ExpieryDate.AddDays(0).ToString("dd-MMM-yyyy")) ?? string.Empty,

//                });

//            if (smsList.Any())
//            {
//                sms.value = "Total SMS " + accountBalance.DadicatedAccountList.Where(da =>
//                            da.GsmServiceType == "sms"
//                            && da.RemainingAmount > 0).Sum(da => Math.Round((decimal)da.RemainingAmount, MidpointRounding.AwayFromZero)).ToString();

//                sms.ItemList.AddRange(smsList);
//            }
//            pakItems.Add(sms);

//            #endregion

//            #region MMS

//            item mms = new item
//            {
//                key = "Bonus MMS",
//                ItemList = new items(),
//            };

//            var mmsList = accountBalance.DadicatedAccountList.Where(da =>
//                da.GsmServiceType == "mms" && da.RemainingAmount > 0).Select(da => new item
//                {
//                    key = string.Format("{0:0}", Math.Round((decimal)da.RemainingAmount, MidpointRounding.AwayFromZero)) ?? string.Empty,
//                    value = string.Format("Your {1} remaining Bought MMS is: {0:0}. It will expire on {2}", Math.Round((decimal)da.RemainingAmount, MidpointRounding.AwayFromZero), da.ProductName, da.ExpieryDate.AddDays(0).ToString("dd-MMM-yyyy")) ?? string.Empty,

//                });


//            if (mmsList.Any())
//            {
//                mms.value = "Total MMS " + accountBalance.DadicatedAccountList.Where(da =>
//                            da.GsmServiceType == "mms"
//                            && da.RemainingAmount > 0).Sum(da => Math.Round((decimal)da.RemainingAmount, MidpointRounding.AwayFromZero)).ToString();

//                mms.ItemList.AddRange(mmsList);

//            }
//            pakItems.Add(mms);
//            #endregion

//            #region VOICE

//            //item voice = new item
//            //{
//            //    ItemList = new items(),
//            //};

//            //var voiceList = dedicatedAccounts.Where(da =>
//            //    da.CacheDedicatedAccounts.DedicatedaccountCategoryId == DedicatedAccountCategory.voice
//            //    && da.ApiDedicatedAccounts.DadicatedAccountRemainingVolume > 0).Select(da => new item
//            //    {
//            //        key = da.CacheDedicatedAccounts.DedicatedaccountName,
//            //        value = string.Format("{0:0}", Math.Round((decimal)da.ApiDedicatedAccounts.DadicatedAccountRemainingVolume / 60, MidpointRounding.AwayFromZero)) ?? string.Empty,

//            //    });
//            //voice.value = dedicatedAccounts.Where(da =>
//            //    da.CacheDedicatedAccounts.DedicatedaccountCategoryId == DedicatedAccountCategory.voice
//            //    && da.ApiDedicatedAccounts.DadicatedAccountRemainingVolume > 0).Sum(da => Math.Round((decimal)da.ApiDedicatedAccounts.DadicatedAccountRemainingVolume / 60, MidpointRounding.AwayFromZero)).ToString();

//            //if (voiceList.Any())
//            //{
//            //    voice.ItemList.AddRange(voiceList);
//            //    pakItems.Add(voice);
//            //}

//            #endregion

//            #region DATA PACK

//            DataPackItems dataPackList = new DataPackItems();
//            dataPackList = accountBalance.GetDataPack();
//            #endregion

//            BECurrentDataList currentDataList = new BECurrentDataList
//            {
//                HasDataPack = dataPackList.Count > 0,
//                DataPackList = dataPackList,
//                DataItemsList = new items(),

//            };
//            currentDataList.DataItemsList.AddRange(pakItems);

//            return currentDataList;
//        }

//        public static DataPackItems GetDataPack(this AccountBalance accountBalance)
//        {

//            DataPackItems dataPackList = new DataPackItems();
//            var dataScale = IsUcip ? 102400 : 1024;

//            foreach (var item in accountBalance.DadicatedAccountList.Where(da =>
//                da.GsmServiceType == "data" && da.RemainingAmount > 0))
//            {


//                dataPackList.Add(new DataPackItem
//                {
//                    PackName = item.ProductName,
//                    DataVolumeUnit = "MB",// item.Unit,
//                    RemainingVolume = string.Format("{0:0}", Math.Round((decimal)item.RemainingAmount / (item.Unit.ToLower() != "mb" ? dataScale : 1), MidpointRounding.AwayFromZero)) ?? string.Empty,
//                    TotalVolume = string.Format("{0:0}", Math.Round((decimal)item.TotalAmount / (item.Unit.ToLower() != "mb" ? dataScale : 1), MidpointRounding.AwayFromZero)) ?? string.Empty,
//                    validity = item.ExpieryDate.ToString("dd-MMM-yyyy"),
//                    DataCategory = !string.IsNullOrWhiteSpace(item.ProductCode) ? DataCategoryEnum.Purchased : DataCategoryEnum.Bonus
//                });
//            }
//            return dataPackList;
//        }

//        public static BEBonusGroups GetBonusGroup(this AccountBalance accountBalance)
//        {
//            BEBonusGroups pakItems = new BEBonusGroups();


//            #region TAKA && VOICE
//            var minScale = IsUcip ? 60 : 1;

//            BEBonusGroup taka = new BEBonusGroup
//            {
//                GroupName = "Bonus Minutes",
//                GroupItemList = new List<BEBonusItem>(),
//            };

//            var takaList = accountBalance.DadicatedAccountList.Where(da =>
//             (da.GsmServiceType == "taka" || da.GsmServiceType == "voice" || da.GsmServiceType == "money")
//             && da.RemainingAmount > 0 && da.DadicatedAccountID != 255).Select(da => new BEBonusItem
//             {
//                 ItemName = da.ProductName,
//                 itemText = da.ProductName,
//                 RemainingVolume = da.RemainingAmount ?? 0,
//                 TotalVolume = da.TotalAmount ?? 0,
//                 Validity = da.ExpieryDate,


//             });

//            if (takaList.Any())
//            {
//                taka.Total = accountBalance.DadicatedAccountList.Where(da =>
//                              (da.GsmServiceType == "taka" || da.GsmServiceType == "voice" || da.GsmServiceType == "money")
//                              && da.RemainingAmount > 0 && da.DadicatedAccountID != 255).Sum(da => Math.Round(da.RemainingAmount ?? 0 / (da.Unit.ToLower() != "min" ? minScale : 1), MidpointRounding.AwayFromZero));
//                taka.GroupItemList.AddRange(takaList);

//            }
//            pakItems.Add(taka);
//            #endregion

//            #region SMS

//            BEBonusGroup sms = new BEBonusGroup
//            {
//                GroupName = "Bonus SMS",
//                GroupItemList = new List<BEBonusItem>(),
//            };

//            var smsList = accountBalance.DadicatedAccountList.Where(da =>
//             da.GsmServiceType == "sms" && da.RemainingAmount > 0).Select(da => new BEBonusItem
//             {
//                 ItemName = da.ProductName,
//                 itemText = da.ProductName,
//                 RemainingVolume = da.RemainingAmount ?? 0,
//                 TotalVolume = da.TotalAmount ?? 0,
//                 Validity = da.ExpieryDate,

//             });

//            if (smsList.Any())
//            {
//                sms.Total = accountBalance.DadicatedAccountList.Where(da =>
//                            da.GsmServiceType == "sms"
//                            && da.RemainingAmount > 0).Sum(da => Math.Round(da.RemainingAmount ?? 0, MidpointRounding.AwayFromZero));

//                sms.GroupItemList.AddRange(smsList);
//            }
//            pakItems.Add(sms);

//            #endregion

//            #region MMS

//            BEBonusGroup mms = new BEBonusGroup
//            {
//                GroupName = "Bonus MMS",
//                GroupItemList = new List<BEBonusItem>(),
//            };

//            var mmsList = accountBalance.DadicatedAccountList.Where(da =>
//              da.GsmServiceType == "mms" && da.RemainingAmount > 0).Select(da => new BEBonusItem
//              {
//                  ItemName = da.ProductName,
//                  itemText = da.ProductName,
//                  RemainingVolume = da.RemainingAmount ?? 0,
//                  TotalVolume = da.TotalAmount ?? 0,
//                  Validity = da.ExpieryDate,

//              });


//            if (mmsList.Any())
//            {
//                mms.Total = accountBalance.DadicatedAccountList.Where(da =>
//                            da.GsmServiceType == "mms"
//                            && da.RemainingAmount > 0).Sum(da => Math.Round(da.RemainingAmount ?? 0, MidpointRounding.AwayFromZero));

//                mms.GroupItemList.AddRange(mmsList);

//            }
//            pakItems.Add(mms);
//            #endregion

//            return pakItems;
//        }

//        public static double MALoanAmount(this AccountBalance accountBalance)
//        {
//            var maLoan = accountBalance.DadicatedAccountList.FirstOrDefault(a => a.DadicatedAccountID == 255);
//            return maLoan != null ? maLoan.RemainingAmount ?? 0 : 0;
//        }
//        public static double DALoanAmount(this AccountBalance accountBalance)
//        {
//            var maLoan = accountBalance.DadicatedAccountList.FirstOrDefault(a => a.DadicatedAccountID == 245);
//            return maLoan != null ? maLoan.RemainingAmount ?? 0 : 0;
//        }

//    }
//}

