﻿//using System;
//using System.Collections.Generic;
//using System.Net;
//using System.Net.Http;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;
//using Newtonsoft.Json;

//namespace Connector.Helpers
//{
//    public class HttpClientHandler
//    {
//        private LogWriter logWriter = new LogWriter();


//        public async Task<T> Get<T>(string requestUri, int timeout,
//            Dictionary<string, string> header = null)
//        {
//            T response = default;
//            try
//            {
//                var httpClient = new HttpClient();

//                if (header != null)
//                    foreach (var item in header)
//                        httpClient.DefaultRequestHeaders.Add(item.Key, item.Value);
//                httpClient.Timeout = TimeSpan.FromMilliseconds(timeout);

//                var clientResponse = httpClient.GetAsync(requestUri).Result;
//                var responseBody = clientResponse.Content.ReadAsStringAsync().Result;
                
//                if (clientResponse.IsSuccessStatusCode)
//                {
//                    response = JsonConvert.DeserializeObject<T>(responseBody);
//                }
//                else
//                {
//                    if (string.IsNullOrWhiteSpace(responseBody))
//                        responseBody = $"status Code: {clientResponse.StatusCode} Reason:{ clientResponse.ReasonPhrase}";
//                    throw new Exception(responseBody);
//                }
//            }
            
//            catch (Exception e)
//            {
//                throw e;
//            }

//            return response;
//        }

//        public async Task<T> Post<T>(string requestUri, int timeout,
//            string jsonData, Dictionary<string, string> header = null, string mediaType = "application/json")
//        {
//            T responseObj = default;

//            try
//            {
//                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

//                //HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");
//                using (var httpClient = new HttpClient { BaseAddress = new Uri(requestUri), Timeout = TimeSpan.FromMilliseconds(timeout) })
//                {
//                    if (header != null)
//                        foreach (var item in header)
//                            httpClient.DefaultRequestHeaders.Add(item.Key, item.Value);
//                    var data = new StringContent(jsonData, Encoding.UTF8, mediaType);

//                    var clientResponse = httpClient.PostAsync(requestUri, data).Result;

//                    //var clientResponse = httpClient.GetAsync(requestUri).Result;
//                    var responseBody = clientResponse.Content.ReadAsStringAsync().Result;
//                    if (clientResponse.IsSuccessStatusCode)
//                    {
//                        responseObj = JsonConvert.DeserializeObject<T>(responseBody);
//                    }
//                    else
//                    {
//                        if (string.IsNullOrWhiteSpace(responseBody))
//                            responseBody = $"status Code: {clientResponse.StatusCode} Reason:{ clientResponse.ReasonPhrase}";
//                        throw new Exception(responseBody);
//                    }

//                }

//                //Thread.Sleep(timeout);
//                ////throw new Exception("CMS test Error");
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }

//            return responseObj;
//        }


//        //public async Task<string> HTTPPostRequest(string url, string contentString, string msisdn, string token)
//        //{
//        //    string response = string.Empty;

//        //    try
//        //    {
//        //        ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });

//        //        HttpContent content = new StringContent(contentString, Encoding.UTF8, "application/json");
//        //        HttpClient client = new HttpClient { BaseAddress = new Uri(url) };

//        //        client.Timeout = TimeSpan.FromMilliseconds(7000);
//        //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
//        //        // client.Headers.Add("Content-Type", "image/jpeg");


//        //        Uri uri = new Uri(url, UriKind.Absolute);
//        //        HttpResponseMessage httpResponseMessage = client.PostAsync(uri, content).Result;
//        //        var jsonString = httpResponseMessage.Content.ReadAsStringAsync();
//        //        jsonString.Wait();
//        //        response = jsonString.Result;
//        //        //////////////////////////////////////////////////////
//        //        //ServicePointManager.SecurityProtocol = (SecurityProtocolType)768 | (SecurityProtocolType)3072;

//        //        //var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
//        //        //httpWebRequest.ContentType = "application/json; charset=utf-8";
//        //        //httpWebRequest.Method = "POST";
//        //        //httpWebRequest.ProtocolVersion = HttpVersion.Version10;
//        //        //httpWebRequest.Headers();

//        //        //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
//        //        //{
//        //        //    string json = contentString;

//        //        //    streamWriter.Write(json);
//        //        //    streamWriter.Flush();
//        //        //}
//        //        //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
//        //        //using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
//        //        //{
//        //        //    response = streamReader.ReadToEnd();

//        //        //}

//        //        await logWriter.ToRequestLog(0, msisdn, "URL: " + url + "|" + "BODY:  " + contentString, response, MethodBase.GetCurrentMethod().Name, "");

//        //    }
//        //    catch (Exception ex)
//        //    {
//        //        await logWriter.ToErrorLog(ex, 0, msisdn, MethodBase.GetCurrentMethod().Name, "URL: " + url + "|" + "BODY:  " + contentString, "", response);
//        //        throw;
//        //    }

//        //    return response;
//        //}
//    }
//}