﻿//using Newtonsoft.Json;
//using Newtonsoft.Json.Linq;
//using Connector.Entities.Base;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Net.Http.Headers;
//using System.Text;
//using System.Threading.Tasks;

//namespace Connector.Helpers
//{
//    public class DBSSHttpClient
//    {
//        private readonly HttpClient _httpClient;
//        public DBSSHttpClient()
//        {
//            _httpClient = new HttpClient();
//        }

//        public TResponse Post<TResponse>(string url, object obj, string contentType = null)
//            where TResponse : HttpResponseBaseViewModel
//        {
//            var credentialsAsString = JsonConvert.SerializeObject(obj, Formatting.Indented);
//            var requestBody = new StringContent(credentialsAsString, Encoding.UTF8, contentType ?? "application/json");

//            using (var httpResponse = _httpClient.PostAsync(url, requestBody).Result)
//            {
//                return this.HandleResponse<TResponse>(httpResponse);
//            }
//        }

//        public TResponse Patch<TResponse>(string url, object obj, string contentType = null)
//            where TResponse : HttpResponseBaseViewModel
//        {
//            var method = new HttpMethod("PATCH");
//            var requestBody = JsonConvert.SerializeObject(obj, Formatting.Indented);
//            var requestBodyAsString = new StringContent(requestBody, Encoding.UTF8, contentType ?? "application/json");
//            var request = new HttpRequestMessage(method, url)
//            {
//                Content = requestBodyAsString
//            };

//            HttpResponseMessage response = new HttpResponseMessage();
//            using (var httpResponse = _httpClient.SendAsync(request).Result)
//            {
//                return this.HandleResponse<TResponse>(httpResponse);
//            }
//        }

//        public TResponse Put<TResponse>(string url, object obj)
//           where TResponse : HttpResponseBaseViewModel
//        {
//            var credentialsAsString = JsonConvert.SerializeObject(obj, Formatting.Indented);
//            var requestBody = new StringContent(credentialsAsString, Encoding.UTF8, "application/json");

//            using (var httpResponse = _httpClient.PutAsync(url, requestBody).Result)
//            {
//                return this.HandleResponse<TResponse>(httpResponse);
//            }
//        }

//        public TResponse Get<TResponse>(string url)
//          where TResponse : HttpResponseBaseViewModel
//        {
//            using (var httpResponse = _httpClient.GetAsync(url).Result)
//            {
//                return this.HandleResponse<TResponse>(httpResponse);
//            }
//        }

//        public TResponse Delete<TResponse>(string url)
//          where TResponse : HttpResponseBaseViewModel
//        {
//            using (var httpResponse = _httpClient.DeleteAsync(url).Result)
//            {
//                return this.HandleResponse<TResponse>(httpResponse);
//            }
//        }

//        //public TResponse GetAll<TResponse>(string url)
//        //  where TResponse //: ResponseBaseViewModel
//        //{
//        //    _httpClient.DefaultRequestHeaders.Authorization
//        //      = new AuthenticationHeaderValue("Bearer", HttpContext.Current.Session["SumupAccessToken"] + "");

//        //    using (var httpResponse = _httpClient.GetAsync(url).Result)
//        //    {
//        //        if (httpResponse.IsSuccessStatusCode)
//        //        {
//        //            using (var response = httpResponse.Content.ReadAsStringAsync())
//        //            {
//        //                var responsJson = JToken.Parse(response.Result);
//        //                return responsJson.ToObject<List<TResponse>>();
//        //            }
//        //        }

//        //        //    return HandleResponse<TResponse>(httpResponse);
//        //    }
//        //}

//        private TResponse HandleResponse<TResponse>(HttpResponseMessage httpResponse)
//            where TResponse : HttpResponseBaseViewModel
//        {
//            using (var response = httpResponse.Content.ReadAsStringAsync())
//            {
//                if (!httpResponse.IsSuccessStatusCode)
//                {
//                    TResponse error = Activator.CreateInstance<TResponse>();
//                    error.StatusCode = httpResponse.StatusCode;
//                    error.ReasonPhrase = httpResponse.ReasonPhrase;
//                    error.IsSuccessStatusCode = httpResponse.IsSuccessStatusCode;
//                    if (httpResponse.StatusCode != HttpStatusCode.InternalServerError)
//                        error.Errors = response.Result.ToError();
//                    return error;
//                }
//                else
//                {
//                    TResponse responseViewModel;
//                    if (response.Result != null)
//                        responseViewModel = JsonConvert.DeserializeObject<TResponse>(response.Result);
//                    else
//                        responseViewModel = Activator.CreateInstance<TResponse>();

//                    responseViewModel.StatusCode = httpResponse.StatusCode;
//                    responseViewModel.ReasonPhrase = httpResponse.ReasonPhrase;
//                    responseViewModel.IsSuccessStatusCode = httpResponse.IsSuccessStatusCode;
//                    return responseViewModel;
//                }
//            }
//        }
//    }
//}

