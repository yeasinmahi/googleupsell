﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using System.Text.Json;
using Newtonsoft.Json;
using LogService;

namespace Connector.Helpers
{
    public class HttpClientFactoryHandler
    {

        private readonly IHttpClientFactory _clientFactory;
        private readonly string _clientName = string.Empty;
        public HttpClientFactoryHandler(IHttpClientFactory clientFactory, string clientName)
        {
            _clientFactory = clientFactory;
            _clientName = clientName;
        }
        public async Task<T> Get<T>(string requestUri, int timeout, 
            Dictionary<string, string> header = null, string mediaType = "application/json")
        {
            //T response = default;
            T response = default;
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Get, requestUri);
                if (header != null)
                {
                    foreach (var item in header)
                    {
                        request.Headers.Add(item.Key, item.Value);
                    }
                }
                request.Content = new StringContent(string.Empty, Encoding.UTF8, mediaType);

                var client = _clientFactory.CreateClient(_clientName);
                client.Timeout = TimeSpan.FromMilliseconds(timeout);
                HttpClient.DefaultProxy.Credentials = CredentialCache.DefaultCredentials;
                //var clientFactoryResponse =  await client.GetAsync(requestUri);
                var clientFactoryResponse = await client.SendAsync(request);

                var responseBody = await clientFactoryResponse.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<T>(responseBody);
                
            }

            catch (Exception e)
            {
                throw e;
            }

            return response;
        }

        public async Task<T> Post<T>(string requestUri, int timeout, string jsonData, Dictionary<string, string> header = null, string mediaType = "application/json")
        {
            T response = default;
            try
            {
                var request = new HttpRequestMessage(HttpMethod.Post, requestUri);
                if (header != null)
                {
                    foreach (var item in header)
                    {
                        request.Headers.Add(item.Key, item.Value);
                    }
                }
                request.Content = new StringContent(jsonData, Encoding.UTF8, mediaType);

                var client = _clientFactory.CreateClient(_clientName);
                client.Timeout = TimeSpan.FromMilliseconds(timeout);

                var clientFactoryResponse = await client.SendAsync(request);
                var responseBody = await clientFactoryResponse.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<T>(responseBody);

            }

            catch (Exception e)
            {
                throw e;
            }

            return response;
        }
    }
}
