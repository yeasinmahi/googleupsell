﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Connector.Helpers
{
    public static class StringExtenstion
    {
        public static T ToTypeOf<T>(this string source)
        {
            using (var xmlStream = new StringReader(source))
            {
                var serializer = new XmlSerializer(typeof(T), "");
                var type = (T) serializer.Deserialize(XmlReader.Create(xmlStream));
                return type;
            }
        }
    }
}