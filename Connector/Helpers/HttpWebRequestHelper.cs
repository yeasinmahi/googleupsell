﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using LogService;
using Newtonsoft.Json;

namespace Connector.Helpers
{
    public class HttpWebRequestHelper
    {
        //LogWriter logWriter = new LogWriter();
        public async Task<string> Get(string uri, int timeout)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(uri);
                int httpRequestTimeOut = AppSettings.DBSSHttpRequestTimeOut;
                request.Timeout = timeout;
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                using (var response = (HttpWebResponse)request.GetResponse())
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                string message = string.Empty;
                //await logWriter.ToErrorLog(ex, 0, string.Empty, MethodBase.GetCurrentMethod().Name, uri, destination, request: uri);
                if (ex.Response == null)
                {
                    message = ex.Message;
                }
                else
                {
                    using (var stream = ex.Response.GetResponseStream())
                    using (var reader = new StreamReader(stream))
                    {
                        message = await reader.ReadToEndAsync();
                    }
                }


                throw new System.Exception(message);
            }
            //Thread.Sleep(timeout);
            //throw new Exception("DBSS test Error");
           // return "Dbss";
        }

        public async Task<string> GetAsync(string uri, int timeout)
        {
            try
            {
                var request = (HttpWebRequest) WebRequest.Create(uri);
                //int httpRequestTimeOut = AppSettings.DBSSHttpRequestTimeOut;
                request.Timeout = AppSettings.DBSSHttpRequestTimeOut;

                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                using (var response = (HttpWebResponse) await request.GetResponseAsync())
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    return await reader.ReadToEndAsync();
                }
            }
            catch (WebException ex)
            {
                using (var stream = ex.Response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    var message = reader.ReadToEnd();
                }

                throw ex;
            }
        }

        public async Task<string> Post(string uri, string data, string contentType, int timeout)
        {
            string message;
            try
            {
                var dataBytes = Encoding.UTF8.GetBytes(data);
                var request = (HttpWebRequest) WebRequest.Create(uri);
                //int httpRequestTimeOut = AppSettings.DBSSHttpRequestTimeOut;
                //request.Timeout = AppSettings.DBSSHttpRequestTimeOut;
                request.Timeout = timeout;

                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                request.ContentLength = dataBytes.Length;
                request.ContentType = contentType;
                request.Method = "POST";
                using (var requestBody = request.GetRequestStream())
                {
                    requestBody.Write(dataBytes, 0, dataBytes.Length);
                }

                using (var response = (HttpWebResponse) request.GetResponse())
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    message = reader.ReadToEnd();
                }

                return message;
            }
            catch (WebException ex)
            {
                //string message = string.Empty;
                //await logWriter.ToErrorLog(ex, 0, string.Empty, MethodBase.GetCurrentMethod().Name, uri, destination, request: uri);
                if (ex.Response == null)
                {
                    message = ex.Message;
                }
                else
                {
                    using (var stream = ex.Response.GetResponseStream())
                    using (var reader = new StreamReader(stream))
                    {
                        message = await reader.ReadToEndAsync();
                    }
                }


                throw new System.Exception(message);
            }
        }

        public async Task<string> PostAsync(string uri, string data, string contentType, int timeout)
        {
            string message;
            try
            {
                var dataBytes = Encoding.UTF8.GetBytes(data);
                var request = (HttpWebRequest) WebRequest.Create(uri);
                //int httpRequestTimeOut = AppSettings.DBSSHttpRequestTimeOut;
                request.Timeout = AppSettings.DBSSHttpRequestTimeOut;

                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                request.ContentLength = dataBytes.Length;
                request.ContentType = contentType;
                request.Method = "POST";
                using (var requestBody = request.GetRequestStream())
                {
                    await requestBody.WriteAsync(dataBytes, 0, dataBytes.Length);
                }

                using (var response = (HttpWebResponse) await request.GetResponseAsync())
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    message = await reader.ReadToEndAsync();
                }

                return message;
            }
            catch (WebException ex)
            {
                using (var stream = ex.Response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    message = reader.ReadToEnd();
                }

                return message;
            }
        }

        public async Task<string> PATCH(string uri, string data, string contentType, int timeout)
        {
            string message;
            try
            {
                var dataBytes = Encoding.UTF8.GetBytes(data);
                var request = (HttpWebRequest) WebRequest.Create(uri);
                //int httpRequestTimeOut = AppSettings.DBSSHttpRequestTimeOut;
                request.Timeout = AppSettings.DBSSHttpRequestTimeOut;

                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                request.ContentLength = dataBytes.Length;
                request.ContentType = contentType;
                request.Method = "PATCH";
                using (var requestBody = request.GetRequestStream())
                {
                    requestBody.Write(dataBytes, 0, dataBytes.Length);
                }

                using (var response = (HttpWebResponse) request.GetResponse())
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    message = reader.ReadToEnd();
                }

                return message;
            }
            catch (WebException ex)
            {
                using (var stream = ex.Response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    message = reader.ReadToEnd();
                }

                return message;
            }
        }

        public async Task<string> PatchAsync(string uri, string data, string contentType, int timeout)
        {
            try
            {
                var dataBytes = Encoding.UTF8.GetBytes(data);
                var request = (HttpWebRequest) WebRequest.Create(uri);
                //int httpRequestTimeOut = AppSettings.DBSSHttpRequestTimeOut;
                request.Timeout = AppSettings.DBSSHttpRequestTimeOut;

                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                request.ContentLength = dataBytes.Length;
                request.ContentType = contentType;
                request.Method = "PATCH";
                using (var requestBody = request.GetRequestStream())
                {
                    await requestBody.WriteAsync(dataBytes, 0, dataBytes.Length);
                }

                using (var response = (HttpWebResponse) await request.GetResponseAsync())
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    return await reader.ReadToEndAsync();
                }
            }
            catch (WebException ex)
            {
                using (var stream = ex.Response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    var message = reader.ReadToEnd();
                }

                throw ex;
            }
        }

        public async Task<string> Delete(string uri, string data, string contentType, int timeout)
        {
            try
            {
                var dataBytes = Encoding.UTF8.GetBytes(data);
                var request = (HttpWebRequest) WebRequest.Create(uri);
                //int httpRequestTimeOut = AppSettings.DBSSHttpRequestTimeOut;
                request.Timeout = AppSettings.DBSSHttpRequestTimeOut;

                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                request.ContentLength = dataBytes.Length;
                request.ContentType = contentType;
                request.Method = "DELETE";
                using (var requestBody = request.GetRequestStream())
                {
                    requestBody.Write(dataBytes, 0, dataBytes.Length);
                }

                using (var response = (HttpWebResponse) request.GetResponse())
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                using (var stream = ex.Response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    var message = reader.ReadToEnd();
                }

                throw ex;
            }
        }


        public async Task<T> Post<T>(string uri, int timeout,
            string data, Dictionary<string, string> header = null, string mediaType = "application/json")
        {
            T responseObj = default;
            string message;
            try
            {
                
                var dataBytes = Encoding.UTF8.GetBytes(data);
                var request = (HttpWebRequest) WebRequest.Create(uri);

                request.Proxy = WebRequest.DefaultWebProxy;
                request.Credentials = CredentialCache.DefaultCredentials; ;
                request.Proxy.Credentials = CredentialCache.DefaultCredentials;


                //int httpRequestTimeOut = AppSettings.DBSSHttpRequestTimeOut;
                request.Timeout = timeout;

                if (header != null)
                    foreach (var item in header)
                    {
                        request.Headers.Add(item.Key, item.Value);
                    }

                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                request.ContentLength = dataBytes.Length;
                request.ContentType = mediaType;
                request.Method = "POST";
                using (var requestBody = request.GetRequestStream())
                {
                    requestBody.Write(dataBytes, 0, dataBytes.Length);
                }

                using (var response = (HttpWebResponse) request.GetResponse())
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    message = reader.ReadToEnd();
                }
                new Log("").Info("Url: "+uri+ "\nRequest:\n "+ data+ "\nResponse: \n "+message);
                responseObj = JsonConvert.DeserializeObject<T>(message);
                //return responseObj;
            }
            catch (WebException ex)
            {
                //using (var stream = ex.Response.GetResponseStream())
                //using (var reader = new StreamReader(stream))
                //{
                //    message = reader.ReadToEnd();
                //    try
                //    {
                //        responseObj = JsonConvert.DeserializeObject<T>(message);
                //        return responseObj;
                //    }
                //    catch { }
                //}
                if (ex.Response != null)
                {
                    if (ex.Status == WebExceptionStatus.ProtocolError)
                    {
                        using (var response = ex.Response as HttpWebResponse)
                        {
                            if (response != null)
                            {
                                if ((int)response.StatusCode == 400)
                                {
                                    throw new BeHandledException(BEMessageCodes.BadRequest.Status, BEMessageCodes.BadRequest.error, "Bad Request Occurred");
                                }
                            }
                            else
                            {
                                // no http status code available
                            }
                        }
                            
                    }
                    else
                    {
                        // no http status code available
                    }

                    using (var stream = ex.Response.GetResponseStream())
                    using (var reader = new StreamReader(stream))
                    {
                        message = reader.ReadToEnd();
                        try
                        {
                            responseObj = JsonConvert.DeserializeObject<T>(message);
                            return responseObj;
                        }
                        catch { }
                    }
                }

                throw ex;
            }

            return responseObj;
        }
    }
}