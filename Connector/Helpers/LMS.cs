﻿using BusinessEntity.APIHub2;

namespace Connector.Helpers
{
    internal class LMS
    {
        // public static string LMSApiBaseURL = "http://172.16.7.154:7091/nglm-thirdpartymanager";// Will be set on application startup

        //public static string DNBOApiBaseURL = "http://172.16.200.84:6061/dnbo-engine/";// Will be set on application startup
        //public static string LMSApiBaseURL = ConfigurationManager.AppSettings["LMSApiBaseURL"];
        //public static string DNBOApiBaseURL = ConfigurationManager.AppSettings["DNBOLMSApiBaseURL"];

        public static string LMSApiBaseURL = AppSettings.LMSApiBaseURL;
        public static string DNBOApiBaseURL = AppSettings.DNBOLMSApiBaseURL;

        public static string GetPresentOfferListURL =
            "/PresentOffers?msisdn=880{0}&language={1}&channelID={2}&serviceTypeID={3}&salesChannelID={4}";


        public static string GetPresentOfferDetailsURL =
            "/PresentOfferDetails?msisdn=880{0}&offerID={1}&language={2}&channelID={3}&salesChannelID={4}";


        public static string DNBOAcceptOfferURL =
            "/AcceptOffer?msisdn=880{0}&offerID={1}&channelID={2}&salesChannelID={3}";

        public static string GetPresentOfferListURLPrefix = "/PresentOffers";


        public static string GetPresentOfferDetailsURLPrefix = "/PresentOfferDetails";


        public static string DNBOAcceptOfferURLPrefix = "/AcceptOffer";

        public static string getLoyaltyProgramsListURLPrefix = "/getLoyaltyProgramsList";
        public static string getLoyaltyProgramsURLPrefix = "/getLoyaltyProgramsListURL";
        public static string getCustomerLoyaltyProgramsURLPrefix = "/getCustomerLoyaltyPrograms";
        public static string getCustomerBDRsURLPrefix = "/getCustomerBDRs";

        public static string getCustomerODRsURLPrefix = "/getCustomerODRs";

        //has to be changed 
        public static string getCustomerDebitBonusrefix = "/debitBonus";
        public static string getCustomerCreditBonusrefix = "/creditBonus";
    }

    public static class LMSRequestHelper
    {
        public static string DNBOAcceptOfferRequestLMS =
            "{\r\n  \"channel_id\": 4,\r\n  \"sales_channel_id\": 5,\r\n  \"user_id\": 12,\r\n  \"msisdn\": 1234567890,\r\n  \u201CofferID\u201D: \u201C123\u201D,\r\n  \"actionCall\": 1\r\n}\r\n";

        public static string GetPresentOfferDetailRequestLMS =
            "{\r\n\"msisdn\":1234567890,\r\n\"offerID\":\"123\",\r\n\"language\":\"en\",\r\n\"channelID\":4,\r\n\"userID\":12,\r\n\"salesChannelID\":5\r\n}\r\n";

        public static string GetPresentOfferListRequestLMS =
            "{\r\n\"msisdn\":1902796042,\r\n\"language\":\"en\",\r\n\"channelID\":4,\r\n\"userID\":12,\r\n\"salesChannelID\":5\r\n}\r\n";

        public static string DNBOAcceptOfferRequest =
            "{ \"msisdn\":1932899336,\n\"offerID\":\'123\',  \n\"channelID\":5, \n\"salesChannelID\":11 }\n";

        public static string GetPresentOfferListRequest =
            "{\"msisdn\":123456789, \n\"language\":\"EN\", \n\"channelID\":5, \n\"serviceTypeID\":1, \n\"salesChannelID\":11 \n}\n\n\n\n\n\n ";

        public static string GetPresentOfferDetailRequest =
            "{\"msisdn\":123456789, \n\"offerID\":\"123\"\n\"language\":\"EN\", \n\"channelID\":5, \n\"serviceTypeID\":1, \n\"salesChannelID\":11 \n}\n\n\n\n\n\n";
    }

    public static class LMSResponseHelper
    {
        public static string GetCustomerLoyaltyProgramResponse =
            "{\r\n  \"apiVersion\": 1,\r\n  \"loyaltyPrograms\": [\r\n    {\r\n      \"loyaltyProgramExitDate\": null,\r\n      \"loyaltyProgramHistory\": [\r\n        {\r\n          \"fromTier\": null,\r\n          \"transitionDate\": \"2019-09-17T17:23:13+0200\",\r\n          \"toTier\": \"Bronze\"\r\n        }\r\n      ],\r\n      \"tierName\": \"Bronze\",\r\n      \"rewardsPointsConsumed\": 200,\r\n      \"rewardsPointsBalance\": 150,\r\n      \"loyaltyProgramName\": \"Program120190522100000\",\r\n      \"loyaltyProgramEnrollmentDate\": \"2018-09-17T17:23:13+0200\",\r\n      \"loyaltyProgramType\": \"POINTS\",\r\n      \"tierEnrollmentDate\": \"2019-09-17T17:23:13+0200\",\r\n      \"rewardsPointsExpired\": 50,\r\n      \"rewardsPointsEarned\": 400\r\n    }\r\n  ],\r\n  \"responseMessage\": \"SUCCESS\",\r\n  \"responseCode\": 0\r\n}\r\n";

        public static string GetPresentOfferListResponse =
            "[\n    {\n        \"offerID\": \"123\",\n        \"offerName\": \"1GB $10 7Days\",\n        \"offerDescription\": \"Buy a 1GB Data Bundle for 10$, expires in 7 Days from purchase\",\n        \"imageURL\": \"http://resource_link1\",\n        \"offerRank\": 1,\n        \"offerPrice\": 10.5,\n        \"offerScore\": 234,\n        \"offerLongDescription\": \"Buy a 1GB Data Bundle for 10$, expires in 7 Days from purchase to enjoy navigating the internet\",\n        \"offerCategoryName\": \"Bundle\"\n    },\n    {\n        \"offerID\": \"555\",\n        \"offerName\": \"1GB $20 30Days\",\n        \"offerDescription\": \"Buy a 1GB Data Bundle for 10$, expires in 7 Days from purchase\",\n        \"imageURL\": \"http://resource_link1\",\n        \"offerRank\": 2,\n        \"offerPrice\": 20,\n        \"offerScore\": 134,\n        \"offerLongDescription\": \"Buy a 1GB Data Bundle for 10$, expires in 7 Days from purchase to enjoy navigating the internet\",\n        \"offerCategoryName\": \"Bundle\"\n    },\n    {\n        \"offerID\": \"56\",\n        \"offerName\": \"100MinNat $5 7Days\",\n        \"offerDescription\": \"Buy 100 National Minutes for 5$, expires in 7 Days from purchase\",\n        \"imageURL\": \"http://resource_link1\",\n        \"offerRank\": 3,\n        \"offerPrice\": 3,\n        \"offerScore\": 45,\n        \"offerLongDescription\": \"Buy 100 National Minutes for 5$, expires in 7 Days from purchase to be close to all your friends\",\n        \"offerCategoryName\": \"Bundle\"\n    }\n]";

        public static string DNBOAcceptOfferResponse = "{\"ID\":754535354543,\n\"Status\":\"success\"}\n\n\n";

        public static string ErrorResponse =
            "{\"platformID\":7, \n\"errorCode\":101, \n\"errorType\":\"FUNCTIONAL\", \n\"errorMessage\":\"MSISDN not available in IOM\"}\n\n\n";

        public static string CommonResponse =
            "{  \r\n   \"apiVersion\":1,   \r\n   \"responseMessage\":\"SUCCESS\",\r\n   \"responseCode\":0\r\n}\r\n\r\n";

        public static string GetPresentOfferDetailsResponse =
            "{\"offerID\": \"123\", \n\"offerName\": \"1GB $10 7Days\", \n\"offerDescription\": \"Buy a 1GB Data Bundle for 10$, expires in 7 Days from purchase\", \n\"imageURL\": \"http://resource_link1\", \n\"offerRank\": \"1\", \n\"offerPrice\": \"10.5\", \n\"offerScore\": \"-1.75\", \n\"offerLongDescription\": \"Buy a 1GB Data Bundle for 10$, expires in 7 Days from purchase to enjoy navigating the internet\", \n\"offerCategoryName\": \"Bundle\" \n} \n\n\n\n\n\n\n";

        public static string GetCustomerBDRsResponse =
            "{  \r\n   \"apiVersion\":1,\r\n   \"BDRs\":[  \r\n      {  \r\n         \"eventID\":\"00037622144357438487\",\r\n         \"featureName\":null,\r\n         \"origin\":\"\",\r\n         \"moduleName\":\"Unknown\",\r\n         \"deliveryRequestID\":\"00037622144357438487\",\r\n         \"returnCode\":0,\r\n         \"eventDatetime\":\"2019-04-23T03:13:40-0400\",\r\n         \"providerId\":\"2\",\r\n         \"customerId\":\"1593347338169345\",\r\n         \"returnCodeDetails\":null,         \r\n   \"deliverableQty\":11,\r\n         \"moduleId\":null,\r\n         \"operation\":\"Credit\",\r\n         \"deliveryStatus\":\"delivered\",\r\n         \"featureId\":null,\r\n         \"deliverableId\":\"12\"\r\n      }\r\n   ],\r\n   \"responseMessage\":\"SUCCESS\",\r\n   \"responseCode\":0\r\n}\r\n";

        public static string GetCustomerODRsResponse =
            "{  \r\n   \"apiVersion\":1,\r\n   \"ODRs\":[  \r\n      {  \r\n         \"eventID\":\"00037622144357438487\",\r\n         \"origin\":\"\",\r\n         \"moduleName\":\"Unknown\",\r\n         \"deliveryRequestID\":\"00037622144357438487\",\r\n         \"returnCode\":0,\r\n         \"eventDatetime\":\"2019-04-23T03:13:40-0400\",\r\n         \"customerId\":\"1593347338169345\",\r\n         \"purchaseId\":\"yuasffyud\",\r\n         \"offerId\":\"offerId-1\",\r\n         \"offerQty\":1,\r\n         \"salesChannel\":\"Channel\",\r\n         \"voucherCode\":\"\",\r\n         \"voucherPartnerId\":\"\",\r\n         \"returnCodeDetails\":null,         \r\n   \"moduleId\":null,\r\n         \"deliveryStatus\":\"delivered\",\r\n         \"featureId\":null\r\n      }\r\n   ],\r\n   \"responseMessage\":\"SUCCESS\",\r\n   \"responseCode\":0\r\n}\r\n";

        public static string GetLoyaltyProgramResponse =
            "{\r\n  \"apiVersion\": 1,\r\n  \"responseMessage\": \"SUCCESS\",\r\n  \"loyaltyProgram\": {\r\n    \"characteristics\": [\r\n      {\r\n        \"characteristicID\": \"4\",\r\n        \"value\": 1\r\n      },\r\n      {\r\n        \"characteristicID\": \"5\",\r\n        \"value\": 2\r\n      }\r\n    ],\r\n    \"statusPointsID\": \"LP_Status_Point_001\",\r\n    \"tiers\": [\r\n        {\r\n          \"numberOfRewardPointsPerUnit\": 1,\r\n          \"numberOfStatusPointsPerUnit\": 1,\r\n          \"statusEventName\": \"ARPU\",\r\n          \"rewardEventName\": \"recharge\",\r\n          \"tierName\": \"Basic\",\r\n          \"statusPointLevel\": 0\r\n        },\r\n        {\r\n          \"numberOfRewardPointsPerUnit\": 1,\r\n          \"numberOfStatusPointsPerUnit\": 1,\r\n          \"statusEventName\": \"ARPU\",\r\n          \"rewardEventName\": \"recharge\",\r\n          \"tierName\": \"Silver\",\r\n          \"statusPointLevel\": 700\r\n        },\r\n        {\r\n          \"numberOfRewardPointsPerUnit\": 1,\r\n          \"numberOfStatusPointsPerUnit\": 1,\r\n          \"statusEventName\": \"ARPU\",\r\n          \"rewardEventName\": \"recharge\",\r\n          \"tierName\": \"Gold\",\r\n          \"statusPointLevel\": 1200\r\n        },\r\n        {\r\n          \"numberOfRewardPointsPerUnit\": 1,\r\n          \"numberOfStatusPointsPerUnit\": 1,\r\n          \"statusEventName\": \"ARPU\",\r\n          \"rewardEventName\": \"recharge\",\r\n          \"tierName\": \"Platinum\",\r\n          \"statusPointLevel\": 1800\r\n        }\r\n      ],\r\n    \"loyaltyProgramDescription\": null,\r\n    \"loyaltyProgramName\": \"Priyojon Program\",\r\n    \"loyaltyProgramID\": \"loyaltyProgram_POINTS_001\",\r\n    \"loyaltyProgramType\": \"POINTS\",\r\n    \"rewardPointsID\": \"LP_Reward_Point_001\"\r\n  },\r\n  \"responseCode\": 0\r\n}\r\n";

        public static string GetLoyaltyProgramListResponse =
            "{\r\n  \"apiVersion\": 1,\r\n  \"loyaltyPrograms\": [\r\n    {\r\n      \"characteristics\": [\r\n        {\r\n          \"characteristicID\": \"4\",\r\n          \"value\": 1\r\n        },\r\n        {\r\n          \"characteristicID\": \"5\",\r\n          \"value\": 2\r\n        }\r\n      ],\r\n      \"statusPointsID\": \"LP_Status_Point_001\",\r\n      \"tiers\": [\r\n        {\r\n          \"numberOfRewardPointsPerUnit\": 1,\r\n          \"numberOfStatusPointsPerUnit\": 1,\r\n          \"statusEventName\": \"ARPU\",\r\n          \"rewardEventName\": \"recharge\",\r\n          \"tierName\": \"Basic\",\r\n          \"statusPointLevel\": 0\r\n        },\r\n\r\n        {\r\n          \"numberOfRewardPointsPerUnit\": 1,\r\n          \"numberOfStatusPointsPerUnit\": 1,\r\n          \"statusEventName\": \"ARPU\",\r\n          \"rewardEventName\": \"recharge\",\r\n          \"tierName\": \"Silver\",\r\n          \"statusPointLevel\": 700\r\n        },\r\n        {\r\n          \"numberOfRewardPointsPerUnit\": 1,\r\n          \"numberOfStatusPointsPerUnit\": 1,\r\n          \"statusEventName\": \"ARPU\",\r\n          \"rewardEventName\": \"recharge\",\r\n          \"tierName\": \"Gold\",\r\n          \"statusPointLevel\": 1200\r\n        },\r\n        {\r\n          \"numberOfRewardPointsPerUnit\": 1,\r\n          \"numberOfStatusPointsPerUnit\": 1,\r\n          \"statusEventName\": \"ARPU\",\r\n          \"rewardEventName\": \"recharge\",\r\n          \"tierName\": \"Platinum\",\r\n          \"statusPointLevel\": 1800\r\n        }\r\n      ],\r\n      \"loyaltyProgramDescription\": null,\r\n      \"loyaltyProgramName\": \"Priyojon Program\",\r\n      \"loyaltyProgramID\": \"loyaltyProgram_POINTS_001\",\r\n      \"loyaltyProgramType\": \"POINTS\",\r\n      \"rewardPointsID\": \"LP_Reward_Point_001\"\r\n    }\r\n  ],\r\n  \"responseMessage\": \"SUCCESS\",\r\n  \"responseCode\": 0\r\n}\r\n";

        public static string GetPresentOfferListResponseLMS =
            "[\r\n    {\r\n        \"offerID\": \"123\",\r\n        \"offerName\": \"100MB 300points 7Days\",\r\n        \"offerDescription\": \"Buy a 100MB Data Bundle for 300 points, expires in 7 Days from purchase\",\r\n        \"offerRank\": 1,\r\n        \"offerPrice\": 300,\r\n        \"offerScore\": 234,\r\n        \"offerCategoryName\": \"Data Bundles\"\r\n    },\r\n    {\r\n        \"offerID\": \"555\",\r\n        \"offerName\": \"35min onnet 300points 3Days\",\r\n        \"offerDescription\": \"Buy a 35 onnet minutes Bundle for 300 points, expires in 3 Days from purchase\",\r\n        \"offerRank\": 2,\r\n        \"offerPrice\": 20,\r\n        \"offerScore\": 134,\r\n        \"offerCategoryName\": \"Voice Bundles\"\r\n    },\r\n    {\r\n        \"offerID\": \"56\",\r\n        \"offerName\": \"Key ring\",\r\n        \"offerDescription\": \"Buy a Banglalink Key Ring for 200 points\",\r\n        \"offerRank\": 3,\r\n        \"offerPrice\": 3,\r\n        \"offerScore\": 45,\r\n        \"offerCategoryName\": \"Gifts\"\r\n    },\r\n    {\r\n        \"offerID\": \"356\",\r\n        \"offerName\": \"Papers Restaurant Discount\",\r\n        \"offerDescription\": \"12% discount on food item except Biriyanbni & Dessert\",\r\n        \"offerRank\": 4,\r\n        \"offerPrice\": 0,\r\n        \"offerScore\": 76,\r\n        \"offerCategoryName\": \"Food & Beverage\",\r\n        \"partnerName\": \"Papers Restaurant\"\r\n    }\r\n]\r\n";

        public static string GetPresentOfferDetailsResponseLMS =
            "{\r\n    \"offerID\": \"123\",\r\n    \"offerName\": \"100MB 300points 7Days\",\r\n    \"offerDescription\": \"Buy a 100MB Data Bundle for 300 points, expires in 7 Days from purchase\",\r\n    \"imageURL\": \"http:\\/resource_link1\",\r\n    \"offerRank\": 1,\r\n    \"offerPrice\": 300,\r\n    \"offerScore\": 234,\r\n    \"offerLongDescription\": \"Buy a 100MB Data Bundle for 300 points, expires in 7 Days from purchase to enjoy navigating the internet\",\r\n    \"offerCategoryName\": \"Data Bundles\"\r\n}\r\n";
    }
}