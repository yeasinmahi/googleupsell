﻿using System;

namespace Connector.Helpers
{
    public static class UtilityHelper
    {
        //public static double TimeDifference(DateTime startTime)
        //{
        //    var span = DateTime.Now - startTime;
        //    return span.TotalMilliseconds;
        //}


        public static class CacheKeys
        {
            public static string GetDashBoardInformation2 = "GetDashBoardInformation2";
            public static string DBSSSubscriber = "DBSSSubscriber";
            public static string GetDashboardConnectionInfo = "GetDashboardConnectionInfo";
            public static string GetConnectionInfo = "GetConnectionInfo";
            public static string AvaliableBundleProduct = "AvaliableBundleProducts";
            public static string AvaliableProduct = "AvaliableProducts";
            public static string UCPIAccountBalance = "UCPIAccountBalance";
            public static string AllAvaliableProduct = "AllAvaliableProducts";
            public static string AllAvailableProductsByChanel = "AllAvailableProductsByChanel";
            public static string CustomerAccountBalance = "CustomerAccountBalance";
            public static string UsageHistory = "UsageHistory";
            public static string CallHistory = "CallHistory";
            public static string ChargeHistory = "ChargeHistory";
            public static string RoamingHistory = "RoamingHistory";
            public static string PaymentRechargeHistory = "PaymentRechargeHistory";
            public static string SubscriptionChargeHistory = "SubscriptionChargeHistory";
            public static string PackageMigration = "PackageMigration";
            public static string GetPostpaidBillInformation = "GetPostpaidBillInformation";
        }

        public static class IncludeProperties
        {
            public static string OutGoingCallHistory = "outgoing_calls";
            public static string DataUsagesChargeHistory = "data_usage";
            public static string SMS = "sms";

            public static string RoamingCallHistory = "roaming_calls";

            public static string AvailableSubscriptionTypes = "available-subscription-types";
        }

        public static class SubscriptionChargeFilterStatus
        {
            public static string SubscriptionChargeInclude = "product.fees";

            public static string all = "all";
            public static string expired = "expired";
            public static string active = "active"; //by default

            public static string activationFrom = "activation-from";
            public static string activationTo = "activation-to";
            public static string deactivationFrom = "deactivation-from";
            public static string deactivationTo = "deactivation-to";
        }

        [Obsolete("", true)]
        public static class CacheValidity
        {
            //public static int SmallReponse = int.Parse(ConfigurationManager.AppSettings["CacheValiditySmallReponse"]);
            //public static int MediumResponse = int.Parse(ConfigurationManager.AppSettings["CacheValidityMediumResponse"]); // 
            //public static int LargeResponse = int.Parse(ConfigurationManager.AppSettings["CacheValidityLargeResponse"]); //Call HIstory


            //public static int SmallReponse = APPSettings.SmallReponse;
            //public static int MediumResponse = int.Parse(ConfigurationManager.AppSettings["CacheValidityMediumResponse"]); // 
            //public static int LargeResponse = int.Parse(ConfigurationManager.AppSettings["CacheValidityLargeResponse"]); //Call HIstory

            //var url = APPSettings.SmallReponse;
            //var userName = APPSettings.OpenCodeUserName;
            //var password = APPSettings.OpenCodePassword;

            //public static int OneMinute = 1;
            //public static int TwoMinute = 2;
            //public static int ThreeMinute = 3;
            //public static int FiveMinute = 5;
            //public static int TenMinute = 10;
            //public static int ThirtyMinute = 30;
        }
    }
}