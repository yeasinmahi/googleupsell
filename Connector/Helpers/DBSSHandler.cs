﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using BusinessObject;
using Newtonsoft.Json;

namespace Connector.Helpers
{
    public class DBSSHandler
    {
        private readonly LogWriter logWriter = new LogWriter();
        private readonly static string clientName = "dbssHttpClientFactory";
        private readonly IHttpClientFactory _clientFactory;
        public DBSSHandler(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;

        }
        public async Task<T> GetJsonObjectFromUrl<T>(string url, string jsonResponse,
            string methodName, string msisdn)
        {
            //var logPath = AppSettings.RequestLogPath;
            T response = default;
            string message;
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            try
            {
                
                //response = AppSettings.EnableDemoAccess ? JsonConvert.DeserializeObject<T>(jsonResponse) : await new HttpClientHandler().Get<T>(url, AppSettings.DBSSHttpRequestTimeOut);
                //message = AppSettings.EnableDemoAccess ? JsonConvert.DeserializeObject<string>(jsonResponse) : await new HttpWebRequestHelper().Get(url, AppSettings.DBSSHttpRequestTimeOut);
                response = AppSettings.EnableDemoAccess ? JsonConvert.DeserializeObject<T>(jsonResponse) : await new HttpClientFactoryHandler(_clientFactory, clientName).Get<T>(url, AppSettings.DBSSHttpRequestTimeOut,null, "application/vnd.api+json");
                //jsonResponse = "{'data':[{'attributes':{'monthly-costs':0,'allow-reactivation':false,'contract-status':'rollover','first-call-date':null,'termination-time':null,'contract-id':'52863400','msisdn':'8801404850126','activation-time':'2019-04-29T00:00:00.000+06:00','status':'active','latest-contract-termination-time':'2020-01-10T00:00:00.000+06:00','directory-listing':'none','payment-type':'prepaid','original-contract-confirmation-code':'MIG_52863400'},'relationships':{'sim-cards':{'links':{'related':'/api/v1/subscriptions/52863400/sim-cards'}},'billing-accounts':{'links':{'related':'/api/v1/subscriptions/52863400/billing-accounts'}},'services':{'links':{'related':'/api/v1/subscriptions/52863400/services'}},'combined-usage-reports':{'links':{'related':'/api/v1/subscriptions/52863400/combined-usage-reports'}},'subscription-discounts':{'links':{'related':'/api/v1/subscriptions/52863400/subscription-discounts'}},'network-services':{'links':{'related':'/api/v1/subscriptions/52863400/network-services'}},'available-loan-products':{'links':{'related':'/api/v1/subscriptions/52863400/available-loan-products'}},'owner-customer':{'data':{'type':'customers','id':'1009604117'},'links':{'related':'/api/v1/subscriptions/52863400/owner-customer'}},'products':{'links':{'related':'/api/v1/subscriptions/52863400/products'}},'payer-customer':{'data':{'type':'customers','id':'1083433388'},'links':{'related':'/api/v1/subscriptions/52863400/payer-customer'}},'available-subscription-types':{'links':{'related':'/api/v1/subscriptions/52863400/available-subscription-types'}},'document-validations':{'links':{'related':'/api/v1/subscriptions/52863400/document-validations'}},'coordinator-customer':{'links':{'related':'/api/v1/subscriptions/52863400/coordinator-customer'}},'product-usages':{'links':{'related':'/api/v1/subscriptions/52863400/product-usages'}},'porting-requests':{'links':{'related':'/api/v1/subscriptions/52863400/porting-requests'}},'billing-rate-plan':{'data':{'type':'billing-rate-plans','id':'1'},'links':{'related':'/api/v1/subscriptions/52863400/billing-rate-plan'}},'user-customer':{'data':{'type':'customers','id':'1083433388'},'links':{'related':'/api/v1/subscriptions/52863400/user-customer'}},'gsm-service-usages':{'links':{'related':'/api/v1/subscriptions/52863400/gsm-service-usages'}},'balances':{'data':[{'type':'balances','id':'52863400'},{'type':'balances','id':'52863400-143'},{'type':'balances','id':'52863400-245'},{'type':'balances','id':'52863400-255'},{'type':'balances','id':'52863400'},{'type':'balances','id':'52863400'},{'type':'balances','id':'52863400'},{'type':'balances','id':'52863400'},{'type':'balances','id':'52863400'},{'type':'balances','id':'52863400'},{'type':'balances','id':'52863400'},{'type':'balances','id':'52863400'},{'type':'balances','id':'52863400'},{'type':'balances','id':'52863400'},{'type':'balances','id':'52863400'},{'type':'balances','id':'52863400'},{'type':'balances','id':'52863400'},{'type':'balances','id':'52863400'},{'type':'balances','id':'52863400'},{'type':'balances','id':'52863400'},{'type':'balances','id':'52863400'},{'type':'balances','id':'52863400'},{'type':'balances','id':'52863400'},{'type':'balances','id':'52863400'},{'type':'balances','id':'52863400'}],'links':{'related':'/api/v1/subscriptions/52863400/balances'}},'billing-usages':{'links':{'related':'/api/v1/subscriptions/52863400/billing-usages'}},'barrings':{'links':{'related':'/api/v1/subscriptions/52863400/barrings'}},'subscription-type':{'data':{'type':'subscription-types','id':'1'},'links':{'related':'/api/v1/subscriptions/52863400/subscription-type'}},'available-products':{'links':{'related':'/api/v1/subscriptions/52863400/available-products'}},'catalog-sim-cards':{'links':{'related':'/api/v1/subscriptions/52863400/catalog-sim-cards'}},'connected-products':{'links':{'related':'/api/v1/subscriptions/52863400/connected-products'}},'connection-type':{'data':{'type':'connection-types','id':'3'},'links':{'related':'/api/v1/subscriptions/52863400/connection-type'}},'available-child-products':{'links':{'related':'/api/v1/subscriptions/52863400/available-child-products'}},'sim-card-orders':{'links':{'related':'/api/v1/subscriptions/52863400/sim-card-orders'}}},'links':{'self':'/api/v1/subscriptions/52863400'},'id':'52863400','type':'subscriptions'}],'included':[{'attributes':{'expiry-at':'2020-08-20T12:00:00.000Z','dedicated-account-id':null,'balance-name':null,'lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':'3642','amount':0,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':null,'start':'2020-07-13T12:00:00.000Z','product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'},{'attributes':{'expiry-at':'9999-12-31T12:00:00.000Z','dedicated-account-id':null,'balance-name':null,'lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':'349','amount':0,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':null,'start':'2019-10-06T12:00:00.000Z','product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'},{'attributes':{'expiry-at':'9999-12-31T12:00:00.000Z','dedicated-account-id':null,'balance-name':null,'lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':'10102','amount':0,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':null,'start':'2019-05-17T12:00:00.000Z','product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'},{'attributes':{'expiry-at':'9999-12-31T12:00:00.000Z','dedicated-account-id':null,'balance-name':null,'lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':'764','amount':0,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':null,'start':'2019-06-03T12:00:00.000Z','product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'},{'attributes':{'expiry-at':'2021-02-04T12:00:00.000Z','dedicated-account-id':null,'balance-name':null,'lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':'6483','amount':0,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':null,'start':'2020-02-06T12:00:00.000Z','product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'},{'attributes':{'expiry-at':'9999-12-31T12:00:00.000Z','dedicated-account-id':null,'balance-name':null,'lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':'10113','amount':0,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':null,'start':'2019-05-17T12:00:00.000Z','product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'},{'attributes':{'expiry-at':'9999-12-31T12:00:00.000Z','dedicated-account-id':'245','balance-name':'DA LOAN','lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':null,'amount':0,'total-amount':0,'gsm-service-type':null,'product-code':null,'unit':'BDT','start':null,'product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400-245/subscription'}}},'links':{'self':'/api/v1/balances/52863400-245'},'id':'52863400-245','type':'balances'},{'attributes':{'expiry-at':'2020-08-20T12:00:00.000Z','dedicated-account-id':null,'balance-name':null,'lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':'6535','amount':0,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':null,'start':'2020-07-13T12:00:00.000Z','product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'},{'attributes':{'expiry-at':'9999-12-31T12:00:00.000Z','dedicated-account-id':null,'balance-name':null,'lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':'763','amount':0,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':null,'start':'2019-05-22T12:00:00.000Z','product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'},{'attributes':{'expiry-at':null,'dedicated-account-id':null,'balance-name':null,'lifecycle':{'service-removal-date':'2023-03-08T12:00:00.000Z','credit-clearance-date':'2023-06-09T12:00:00.000Z','supervision-expiry-date':'2020-08-20T12:00:00.000Z','service-fee-expiry-date':'2020-09-19T12:00:00.000Z'},'is-main-balance':true,'product-id':null,'offer-id':null,'amount':47.69,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':'BDT','start':null,'product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'},{'attributes':{'expiry-at':'2020-07-26T12:00:00.000Z','dedicated-account-id':'143','balance-name':'143','lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':null,'amount':155.38,'total-amount':155.38,'gsm-service-type':'data','product-code':'25MBLOGINBONUS','unit':'MB','start':null,'product-name':{'en':'25MB Login Bonus-4Days','bn':'২৫এমবি বোনাস - ৪ দিন'}},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400-143/subscription'}}},'links':{'self':'/api/v1/balances/52863400-143'},'id':'52863400-143','type':'balances'},{'attributes':{'expiry-at':'2020-09-01T12:00:00.000Z','dedicated-account-id':null,'balance-name':null,'lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':'208','amount':0,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':null,'start':'2020-05-10T12:00:00.000Z','product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'},{'attributes':{'expiry-at':'9999-12-31T12:00:00.000Z','dedicated-account-id':null,'balance-name':null,'lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':'667','amount':0,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':null,'start':'2019-05-22T12:00:00.000Z','product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'},{'attributes':{'expiry-at':'2020-08-20T12:00:00.000Z','dedicated-account-id':null,'balance-name':null,'lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':'6669','amount':0,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':null,'start':'2020-07-13T12:00:00.000Z','product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'},{'attributes':{'expiry-at':'9999-12-31T12:00:00.000Z','dedicated-account-id':null,'balance-name':null,'lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':'10110','amount':0,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':null,'start':'2019-05-17T12:00:00.000Z','product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'},{'attributes':{'expiry-at':'9999-12-31T12:00:00.000Z','dedicated-account-id':null,'balance-name':null,'lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':'10103','amount':0,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':null,'start':'2019-05-17T12:00:00.000Z','product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'},{'attributes':{'expiry-at':'9999-12-31T12:00:00.000Z','dedicated-account-id':null,'balance-name':null,'lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':'10111','amount':0,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':null,'start':'2019-05-17T12:00:00.000Z','product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'},{'attributes':{'expiry-at':'2020-08-20T12:00:00.000Z','dedicated-account-id':null,'balance-name':null,'lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':'6672','amount':0,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':null,'start':'2020-07-13T12:00:00.000Z','product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'},{'attributes':{'expiry-at':'2020-09-01T12:00:00.000Z','dedicated-account-id':null,'balance-name':null,'lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':'6589','amount':0,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':null,'start':'2020-06-13T12:00:00.000Z','product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'},{'attributes':{'expiry-at':'2020-08-20T12:00:00.000Z','dedicated-account-id':null,'balance-name':null,'lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':'4316','amount':0,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':null,'start':'2020-07-13T12:00:00.000Z','product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'},{'attributes':{'expiry-at':'9999-12-31T12:00:00.000Z','dedicated-account-id':null,'balance-name':null,'lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':'10112','amount':0,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':null,'start':'2019-05-17T12:00:00.000Z','product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'},{'attributes':{'expiry-at':'2020-09-01T12:00:00.000Z','dedicated-account-id':null,'balance-name':null,'lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':'6545','amount':0,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':null,'start':'2020-06-13T12:00:00.000Z','product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'},{'attributes':{'expiry-at':'2020-09-01T12:00:00.000Z','dedicated-account-id':null,'balance-name':null,'lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':'6590','amount':0,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':null,'start':'2020-05-08T12:00:00.000Z','product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'},{'attributes':{'expiry-at':'9999-12-31T12:00:00.000Z','dedicated-account-id':'255','balance-name':'255','lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':null,'amount':0,'total-amount':0,'gsm-service-type':null,'product-code':null,'unit':'BDT','start':null,'product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400-255/subscription'}}},'links':{'self':'/api/v1/balances/52863400-255'},'id':'52863400-255','type':'balances'},{'attributes':{'expiry-at':'9999-12-31T12:00:00.000Z','dedicated-account-id':null,'balance-name':null,'lifecycle':null,'is-main-balance':false,'product-id':null,'offer-id':'10101','amount':0,'total-amount':null,'gsm-service-type':null,'product-code':null,'unit':null,'start':'2019-05-17T12:00:00.000Z','product-name':null},'relationships':{'subscription':{'data':{'type':'subscriptions','id':'52863400'},'links':{'related':'/api/v1/balances/52863400/subscription'}}},'links':{'self':'/api/v1/balances/52863400'},'id':'52863400','type':'balances'}]}";
                
                watch.Stop();
                rtt = watch.ElapsedMilliseconds;
                                
                await new BORequestSummary().Add($"{AppSettings.Provider}.Connector.DBSS.{methodName}", rtt, false);
                return response;
            }
            catch (Exception ex)
            {
                
                rtt = watch.ElapsedMilliseconds;
                await new BORequestSummary().Add($"{AppSettings.Provider}.Connector.DBSS.{methodName}", rtt, true);
                throw;
            }
            finally
            {
                watch.Stop();
            }
        }


        public async Task<T> PostRequest<T>(string url, string jsonRequest, string contentType,
            string methodName, string msisdn, string requestFrom = "DBSS")
        {
            T response = default;
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            //string logPath = ExceptionExtensions.RequestLogPath;
            var logPath = AppSettings.RequestLogPath;
            var json = string.Empty;
            try
            {
                //json = await new HttpWebRequestHelper().Post(url, jsonRequest, contentType,
                //    AppSettings.DBSSHttpRequestTimeOut);

                response = await new HttpClientFactoryHandler(_clientFactory, clientName).Post<T>(url, AppSettings.DBSSHttpRequestTimeOut,jsonRequest, null, contentType);

                //rtt = watch.ElapsedMilliseconds;
                //await logWriter.ToRequestLog(rtt, msisdn, "URL: " + url + "|" + "BODY: " + jsonRequest, json,
                //    methodName, json == "{\n  \"data\": []\n}" ? requestFrom + ".Empty" : requestFrom);

                //var response = JsonConvert.DeserializeObject<T>(json);
                rtt = watch.ElapsedMilliseconds;
                await new BORequestSummary().Add(AppSettings.Provider + ".DBSS.Conector." + methodName, rtt, false);


                return response;
            }
            catch (Exception ex)
            {
                //await logWriter.ToRequestLog(rtt, msisdn, "URL: " + url + "|" + "BODY: " + jsonRequest, json,
                //    methodName, json == "{\n  \"data\": []\n}" ? requestFrom + ".Empty" : requestFrom);

                //await logWriter.ToErrorLog(ex, rtt, msisdn, methodName, "URL: " + url + "|" + "BODY: " + jsonRequest,
                //    requestFrom, json);
                //if (json.Contains("MultipleObjectsReturned"))
                //    throw new Exception("Subscriber not eligible for this package (MOR)!!!");
                //if (json.Contains("account credit limit insufficient"))
                //    throw new Exception(
                //        "Insufficient credit limit. Please Increase your credit limit and try again!!!");
                rtt = watch.ElapsedMilliseconds;
                await new BORequestSummary().Add(AppSettings.Provider + ".DBSS.Conector." + methodName, rtt, true);
                throw ex;
            }
        }

        public async Task<T> PatchRequest<T>(string url, string jsonRequest, string contentType, 
            string methodName, string msisdn)
        {
            T response = default;
            Stopwatch watch = Stopwatch.StartNew();
            double rtt = 0;
            //string logPath = ExceptionExtensions.RequestLogPath;
            //var logPath = AppSettings.RequestLogPath;
            var json = string.Empty;
            try
            {
                response = await new HttpClientFactoryHandler(_clientFactory, clientName).Post<T>(url, AppSettings.DBSSHttpRequestTimeOut, jsonRequest);

                //rtt = watch.ElapsedMilliseconds;
                //await logWriter.ToRequestLog(rtt, msisdn, "URL: " + url + "|" + "BODY: " + jsonRequest, json,
                //    methodName, json == "{\n  \"data\": []\n}" ? "DBSS.Empty" : "DBSS");

                //var response = JsonConvert.DeserializeObject<T>(json);
                //rtt = watch.ElapsedMilliseconds;
                await new BORequestSummary().Add($"{AppSettings.Provider}.Connector.DBSS.{methodName}", rtt, false);

                return response;
            }
            catch (Exception ex)
            {
                //await logWriter.ToRequestLog(rtt, msisdn, "URL: " + url + "|" + "BODY: " + jsonRequest, json,
                //    methodName, json == "{\n  \"data\": []\n}" ? "DBSS.Empty" : "DBSS");

                //await logWriter.ToErrorLog(ex, rtt, msisdn, methodName, "URL: " + url + "|" + "BODY: " + jsonRequest,
                //    "DBSS", json);

                //if (json.Contains("MultipleObjectsReturned"))
                //    throw new Exception("Subscriber not eligible for this package (MOR)!!!");
                //if (json.Contains("account credit limit insufficient"))
                //    throw new Exception(
                //        "Insufficient credit limit. Please Increase your credit limit and try again!!!");
                rtt = watch.ElapsedMilliseconds;
                await new BORequestSummary().Add($"{AppSettings.Provider}.Connector.DBSS.{methodName}", rtt, true);
                throw;
            }
        }

        //public async Task ToErrorLog(Exception exception, double rtt, string msisdn, string requestMethod, string requestURL = "", string requester = "BOSGW", string response = "", string request = "")
        //{
        //    var provider = AppSettings.Provider;


        //    try
        //    {

        //        try
        //        {
        //            exception.ToTextFileLog(msisdn, requestMethod, requestURL, requester, response, request);//to write text file

        //        }
        //        catch (Exception)
        //        {


        //        }


        //        if (string.IsNullOrWhiteSpace(request) && requester.Contains("DBSS"))
        //        {
        //            request = requestURL;
        //        }

        //        string message = string.Empty;
        //        var exceptions = exception.GetAllExceptions();
        //        foreach (var ex in exceptions)
        //        {
        //            StackTrace st = new StackTrace(ex, true);
        //            StackFrame frame = st.GetFrame(st.FrameCount - 1);

        //            if (st.FrameCount == 0 || frame == null) continue;
        //            BEAppErrorLog log = new BEAppErrorLog
        //            {
        //                Msisdn = msisdn,
        //                LogTime = DateTime.Now,
        //                Message = ex.Message,
        //                Stacktrace = ex.StackTrace,
        //                RequestFrom = provider + "-" + requester,
        //                RequestMethod = requestMethod,
        //                RequestUrl = requestURL,
        //                Request = request,
        //                Response = response,
        //                Rtt = rtt,
        //                //IsActive = true,

        //            };
        //           await new BOAppErrorLog().Create(log);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToTextFileLog(msisdn, requestMethod, requestURL, requester, response, request);
        //    }
        //}

        //public async Task ToRequestLog(double rtt, string msisdn, string request, string response, string requestMethod, string requester = "BOSGW")
        //{
        //     bool enableLog = AppSettings.EnableWriteDBBSErrorLog;
        //   // bool enableLog = true;
        //    var provider = AppSettings.Provider;
        //    var logMSISDNS = AppSettings.LOGMSISDNS;
        //    List<string> msisdns = new List<string>();
        //    if (!string.IsNullOrWhiteSpace(logMSISDNS))
        //    {
        //        msisdns = logMSISDNS.Split('|').ToList();
        //    }

        //    if (enableLog)
        //       await WriteLog(rtt, msisdn, request, response, requestMethod, requester, provider);
        //    else if (msisdns.Contains(msisdn))
        //       await WriteLog(rtt, msisdn, request, response, requestMethod, requester, provider);
        //    else if (rtt > AppSettings.MinimumRTTToLog)
        //       await WriteLog(rtt, msisdn, request, response, requestMethod, requester, provider);
        //}

        //private async Task WriteLog(double rtt, string msisdn, string request, string response, string requestMethod, string requester, string provider)
        //{
        //    try
        //    {
        //        //try
        //        //{
        //        //    request.ToRequestResponseLog(response, "--FROM: " + requestMethod + "--GW: " + requester + "--MSISDN: " + msisdn);//to write text file
        //        //}
        //        //catch (Exception ex)
        //        //{
        //        //}

        //        BEAppRequestResponseLog log = new BEAppRequestResponseLog
        //        {
        //            Msisdn = msisdn,
        //           // LogTime = DateTime.Now,
        //            RequestUri = request,
        //            ResponseBody = response,
        //            ClienIipAddress = provider + "-" + requester,
        //            RequestMethod = requestMethod,
        //            RTT = rtt,

        //        };
        //        await new BOAppRequestResponseLog().Create(log);
        //    }
        //    catch (Exception ex)
        //    {
        //       await ToErrorLog(ex, rtt, msisdn, requestMethod);
        //    }
        //}
    }
}