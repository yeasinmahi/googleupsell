﻿//using Newtonsoft.Json;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using Utility;
//using BusinessObject;
//using System.Diagnostics;
//using BusinessEntity.APIHub2;
//using Utility.Extensions;
//using System.Threading.Tasks;
//using BusinessEntity.Log;

//namespace Connector.Helpers
//{
//    public class DBSSHttpHandler
//    {
//        public async Task<T> GetJsonObjectFromUrl<T>( string url, string jsonResponse, DateTime startTime, string methodName, string msisdn)
//        {
//           // bool isUseJson = AppSettings.EnableDemoAccess;

//            string logPath = AppSettings.RequestLogPath;
//            T response = default;

//            double rtt = 0;
//            try
//            {
//                jsonResponse = AppSettings.EnableDemoAccess
//                    ? jsonResponse
//                    : await new HttpWebRequestHelper().Get(url);

//                rtt = watch.ElapsedMilliseconds;

//               await ToRequestLog(rtt, msisdn, url, jsonResponse, methodName, jsonResponse == "{\n  \"data\": []\n}" ? "DBSS.Empty" : "DBSS");

//                if (jsonResponse == "{\n  \"data\": []\n}")
//                {
//                    response = (T)Activator.CreateInstance(typeof(T));
//                }
//                else
//                {
//                    response = JsonConvert.DeserializeObject<T>(jsonResponse);
//                    rtt = watch.ElapsedMilliseconds;
//                   await new BORequestSummary().Add($"{ AppSettings.Provider}.Connector.DBSS.{methodName}", rtt, false);
//                }

//                return response;
//            }
//            catch (Exception ex)
//            {
//                rtt = watch.ElapsedMilliseconds;
//                await ToErrorLog(ex, rtt, msisdn, methodName, url, "DBSS", jsonResponse);
//                await ToRequestLog(rtt, msisdn, url, jsonResponse, methodName, jsonResponse == "{\n  \"data\": []\n}" ? "DBSS.Empty" : "DBSS");

//                rtt = watch.ElapsedMilliseconds;
//                await new BORequestSummary().Add($"{ AppSettings.Provider}.Connector.DBSS.{methodName}", rtt, true);
//                throw;
//            }
//        }


//        public async Task<T> PostRequest<T>(string url, string jsonRequest, string contentType, DateTime startTime, string methodName, string msisdn, string requestFrom = "DBSS")
//        {
//            double rtt = 0;
//            //string logPath = ExceptionExtensions.RequestLogPath;
//            string logPath = AppSettings.RequestLogPath;
//            string json = string.Empty;
//            try
//            {
//                json = await new HttpWebRequestHelper().Post(url, jsonRequest, contentType);

//                rtt = watch.ElapsedMilliseconds;
//                await ToRequestLog(rtt, msisdn, "URL: " + url + "|" + "BODY: " + jsonRequest, json, methodName, json == "{\n  \"data\": []\n}" ? requestFrom + ".Empty" : requestFrom);

//                var response = JsonConvert.DeserializeObject<T>(json);
//                rtt = watch.ElapsedMilliseconds;
//                await new BORequestSummary().Add(AppSettings.Provider+".DBSS.Conector." + methodName, rtt, false);


//                return response;
//            }
//            catch (Exception ex)
//            {
//                await ToRequestLog(rtt, msisdn, "URL: " + url + "|" + "BODY: " + jsonRequest, json, methodName, json == "{\n  \"data\": []\n}" ? requestFrom + ".Empty" : requestFrom);

//                await ToErrorLog(ex, rtt, msisdn, methodName, "URL: " + url + "|" + "BODY: " + jsonRequest, requestFrom, json);
//                if (json.Contains("MultipleObjectsReturned"))
//                {
//                    throw new Exception("Subscriber not eligible for this package (MOR)!!!");
//                }
//                if (json.Contains("account credit limit insufficient"))
//                {
//                    throw new Exception("Insufficient credit limit. Please Increase your credit limit and try again!!!");
//                }
//                rtt = watch.ElapsedMilliseconds;
//                await new BORequestSummary().Add(AppSettings.Provider + ".DBSS.Conector." + methodName, rtt, true);
//                throw ex;
//            }
//        }

//        public async Task<T> PatchRequest<T>(string url, string jsonRequest, string contentType, DateTime startTime, string methodName, string msisdn)
//        {
//            double rtt = 0;
//            //string logPath = ExceptionExtensions.RequestLogPath;
//            string logPath = AppSettings.RequestLogPath;
//            string json = string.Empty;
//            try
//            {
//                json = await new HttpWebRequestHelper().PATCH(url, jsonRequest, contentType);

//                rtt = watch.ElapsedMilliseconds;
//                await ToRequestLog(rtt, msisdn, "URL: " + url + "|" + "BODY: " + jsonRequest, json, methodName, json == "{\n  \"data\": []\n}" ? "DBSS.Empty" : "DBSS");

//                var response = JsonConvert.DeserializeObject<T>(json);
//                rtt = watch.ElapsedMilliseconds;
//                await new BORequestSummary().Add($"{ AppSettings.Provider}.Connector.DBSS.{methodName}", rtt, false);

//                return response;

//            }
//            catch (Exception ex)
//            {
//                await ToRequestLog(rtt, msisdn, "URL: " + url + "|" + "BODY: " + jsonRequest, json, methodName, json == "{\n  \"data\": []\n}" ? "DBSS.Empty" : "DBSS");

//                await ToErrorLog(ex, rtt, msisdn, methodName, "URL: " + url + "|" + "BODY: " + jsonRequest, "DBSS", json);

//                if (json.Contains("MultipleObjectsReturned"))
//                {
//                    throw new Exception("Subscriber not eligible for this package (MOR)!!!");
//                }
//                if (json.Contains("account credit limit insufficient"))
//                {
//                    throw new Exception("Insufficient credit limit. Please Increase your credit limit and try again!!!");
//                }
//                rtt = watch.ElapsedMilliseconds;
//                await new BORequestSummary().Add($"{ AppSettings.Provider}.Connector.DBSS.{methodName}", rtt, true);
//                throw;
//            }
//        }

//        public async Task ToErrorLog(Exception exception, double rtt, string msisdn, string requestMethod, string requestURL = "", string requester = "BOSGW", string response = "", string request = "")
//        {
//            var provider = AppSettings.Provider;


//            try
//            {

//                try
//                {
//                    exception.ToTextFileLog(msisdn, requestMethod, requestURL, requester, response, request);//to write text file

//                }
//                catch (Exception)
//                {


//                }


//                if (string.IsNullOrWhiteSpace(request) && requester.Contains("DBSS"))
//                {
//                    request = requestURL;
//                }

//                string message = string.Empty;
//                var exceptions = exception.GetAllExceptions();
//                foreach (var ex in exceptions)
//                {
//                    StackTrace st = new StackTrace(ex, true);
//                    StackFrame frame = st.GetFrame(st.FrameCount - 1);

//                    if (st.FrameCount == 0 || frame == null) continue;
//                    BEAppErrorLog log = new BEAppErrorLog
//                    {
//                        Msisdn = msisdn,
//                        LogTime = DateTime.Now,
//                        Message = ex.Message,
//                        Stacktrace = ex.StackTrace,
//                        RequestFrom = provider + "-" + requester,
//                        RequestMethod = requestMethod,
//                        RequestUrl = requestURL,
//                        Request = request,
//                        Response = response,
//                        Rtt = rtt,
//                        //IsActive = true,

//                    };
//                   await new BOAppErrorLog().Create(log);
//                }
//            }
//            catch (Exception ex)
//            {
//                ex.ToTextFileLog(msisdn, requestMethod, requestURL, requester, response, request);
//            }
//        }

//        public async Task ToRequestLog(double rtt, string msisdn, string request, string response, string requestMethod, string requester = "BOSGW")
//        {
//             bool enableLog = AppSettings.EnableWriteDBBSErrorLog;
//           // bool enableLog = true;
//            var provider = AppSettings.Provider;
//            var logMSISDNS = AppSettings.LOGMSISDNS;
//            List<string> msisdns = new List<string>();
//            if (!string.IsNullOrWhiteSpace(logMSISDNS))
//            {
//                msisdns = logMSISDNS.Split('|').ToList();
//            }

//            if (enableLog)
//               await WriteLog(rtt, msisdn, request, response, requestMethod, requester, provider);
//            else if (msisdns.Contains(msisdn))
//               await WriteLog(rtt, msisdn, request, response, requestMethod, requester, provider);
//            else if (rtt > AppSettings.MinimumRTTToLog)
//               await WriteLog(rtt, msisdn, request, response, requestMethod, requester, provider);
//        }

//        private async Task WriteLog(double rtt, string msisdn, string request, string response, string requestMethod, string requester, string provider)
//        {
//            try
//            {
//                //try
//                //{
//                //    request.ToRequestResponseLog(response, "--FROM: " + requestMethod + "--GW: " + requester + "--MSISDN: " + msisdn);//to write text file
//                //}
//                //catch (Exception ex)
//                //{
//                //}

//                BEAppRequestResponseLog log = new BEAppRequestResponseLog
//                {
//                    Msisdn = msisdn,
//                   // LogTime = DateTime.Now,
//                    RequestUri = request,
//                    ResponseBody = response,
//                    ClienIipAddress = provider + "-" + requester,
//                    RequestMethod = requestMethod,
//                    RTT = rtt,

//                };
//                await new BOAppRequestResponseLog().Create(log);
//            }
//            catch (Exception ex)
//            {
//               await ToErrorLog(ex, rtt, msisdn, requestMethod);
//            }
//        }
//    }
//}

