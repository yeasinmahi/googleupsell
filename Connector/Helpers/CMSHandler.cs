﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using BusinessEntity.ConnectorModels.CMS;
using BusinessObject;
using LogService;
using Newtonsoft.Json;
using Utility.Extensions;

namespace Connector.Helpers
{
    public class CMSHandler
    {
        //DBSSHandler DBSSHandler = new DBSSHandler();
        //1.	PresentOffers: http://172.16.8.99:7777/api/PresentOffers
        //2.	PresentOfferDetails : http://172.16.8.99:7777/api/PresentOfferDetails
        //3.	AcceptOffer : http://172.16.8.99:7777/api/AcceptOffer

        private readonly static string clientName = "cmsHttpClientFactory";

        public static string CMSApiBaseURLTest = "http://172.16.8.99:7777/api"; // Will be set on application startup

        // public static string CMSApiBaseURL = "http://cmsdnbo.banglalinkgsm.com/dnbo-engine";
        //      public static string CMSApiBaseURL = ConfigurationManager.AppSettings["DNBOCMSApiBaseURL"];
        public static string CMSApiBaseURL = AppSettings.CMSApiBaseURL;

        //public static string CMSApiBaseURL = "https://cmsdnbo.banglalinkgsm.com/dnbo-engine"; 
        // Will be set on application startup
        //https://cmsdnbo.banglalinkgsm.com/dnbo-engine
        public static string PresentOfferListURL =
            "/PresentOffers?msisdn=880{0}&language={1}&channelID={2}&serviceTypeID={3}&salesChannelID={4}";


        public static string PresentOfferDetailsURL =
            "/PresentOfferDetails?msisdn=880{0}&offerID={1}&language={2}&channelID={3}&salesChannelID={4}";


        public static string AcceptOfferURL = "/AcceptOffer?msisdn=880{0}&offerID={1}&channelID={2}&salesChannelID={3}";

        public static string PresentOfferListURLPrefix = "/PresentOffers";


        public static string PresentOfferDetailsURLPrefix = "/PresentOfferDetails";


        public static string AcceptOfferURLPrefix = "/AcceptOffer";
        //public async Task<string> HTTPPostRequest(string url, string contentString, string msisdn, string cmsORlma, string methodName)
        //{
        //    string response = string.Empty;
        //    double rtt = 0;
        //    DateTime startTime = DateTime.Now;

        //    try
        //    {

        //        HttpContent content = new StringContent(contentString, Encoding.UTF8, "application/json");
        //        HttpClient client = new HttpClient { BaseAddress = new Uri(CMSApiBaseURL) };
        //        Uri uri = new Uri(url, UriKind.Absolute);
        //        client.Timeout = TimeSpan.FromMilliseconds(AppSettings.CMSHttpRequestTimeOut);
        //        HttpResponseMessage httpResponseMessage = client.PostAsync(uri, content).Result;
        //        var jsonString = httpResponseMessage.Content.ReadAsStringAsync();
        //        jsonString.Wait();
        //        response = jsonString.Result;

        //        await httpClientExtentions.ToRequestLog(0, msisdn, "URL: " + url + "|" + "BODY:  " + contentString, response, MethodBase.GetCurrentMethod().Name, cmsORlma);

        //        rtt = watch.ElapsedMilliseconds;
        //        await new BORequestSummary().Add($"{ AppSettings.Provider}.Connector.{cmsORlma}.{methodName}", rtt, false);

        //    }
        //    catch (Exception ex)
        //    {
        //        await httpClientExtentions.ToErrorLog(ex, 0, msisdn, MethodBase.GetCurrentMethod().Name, "URL: " + url + "|" + "BODY:  " + contentString, cmsORlma, response);

        //        rtt = watch.ElapsedMilliseconds;
        //        ToTextFileLog(ex, rtt, msisdn, url, "BODY:  "
        //            + contentString, response);
        //        await new BORequestSummary().Add($"{ AppSettings.Provider}.Connector.{cmsORlma}.{methodName}", rtt, true);
        //        throw;
        //    }
        //    finally
        //    {

        //    }

        //    return response;
        //}
        private static readonly object _tempFileLocker = new object();

        private readonly IHttpClientFactory clientFactory;
        public CMSHandler(IHttpClientFactory clientFactory)
        {
            this.clientFactory = clientFactory;
        }
        public static bool IsValidJson(string stringValue)
        {
            if (string.IsNullOrWhiteSpace(stringValue)) return false;

            var value = stringValue.Trim();

            if (value.StartsWith("{") && value.EndsWith("}") || //For object
                value.StartsWith("[") && value.EndsWith("]")) //For array
                try
                {
                    //var obj = JToken.Parse(value);
                    return true;
                }
                catch (JsonReaderException)
                {
                    return false;
                }

            return false;
        }

        public async Task<T> Post<T>(
            string url, string data, string msisdn, string destination, string methodName)
        {
            //string method = "POST";
            T response = default ;
            Stopwatch watch = Stopwatch.StartNew();
            try
            {
                //if (LogConfig.IsWriteTPSRequestLog)
                //    new BOrequestlog().Save($"{AppSettings.Provider}.Connector.{destination}.{methodName}");

                //Stopwatch watch = Stopwatch.StartNew();
                response = await new HttpClientFactoryHandler(clientFactory,clientName).Post<T>(url, AppSettings.CMSHttpRequestTimeOut, data);

                //message = "[{'offerID':'20546','offerName':'Prepaid_6486_BTL_DATA_1GB@21Tk - 72 Hours_DBSS','offerDescription':'1GB@21Tk - 72 Hours(Incl all taxes)','offerScore':'0','offerRank':'1','imageURL':null,'offerPrice':'21.0','offerDescriptionWeb':'VOICE | 0 | MIN; SMS | 0 | SMS; DATA | 1 | GB; TK | 21 | TK; VAL | 3 | DAYS; TARIFF | 0 | P / SEC ON - NET; TARIFF | 0 | P / SEC OFF - NET; CAT | DAT | ','offerShortDescription':'1GB@21Tk - 72hours','offerLongDescription':'1GB@21Tk - 72hours(Incl all taxes)'},{'offerID':'20289','offerName':'Prepaid_ATL_DATA_100MB_FB - 7D@Tk.7_DBSS','offerDescription':'100MB_FB - 7days @TK.7(Incl all taxes)','offerScore':'0','offerRank':'2','imageURL':null,'offerPrice':'7.0','offerDescriptionWeb':'VOICE | 0 | MIN; SMS | 0 | SMS; DATA | 100 | MB; TK | 7 | TK; VAL | 7 | DAYS; TARIFF | 0 | P / SEC ON - NET; TARIFF | 0 | P / SEC OFF - NET; CAT | DAT | ','offerShortDescription':'100MB_FB - 7days @TK.7','offerLongDescription':'100MB_FB - 7days @TK.7(Incl all taxes)'},{'offerID':'20936','offerName':'6706_BTL_DATA_2GB@45Tk - 7D_DBSS','offerDescription':'2GB@45Tk - 7D(Incl all taxes)','offerScore':'0','offerRank':'3','imageURL':null,'offerPrice':'45.0','offerDescriptionWeb':'VOICE | 0 | MIN; SMS | 0 | SMS; DATA | 2 | GB; TK | 45 | TK; VAL | 7 | DAYS; TARIFF | 0 | P / SEC ON - NET; TARIFF | 0 | P / SEC OFF - NET; CAT | DAT | ','offerShortDescription':'2GB@45Tk - 7D','offerLongDescription':'2GB@45Tk - 7D(Incl all taxes)'},{'offerID':'20552','offerName':'Prepaid_BTL_6467_DATA_3GB@58Tk - 7D_DBSS','offerDescription':'3GB@58Tk - 7D(Incl all taxes)','offerScore':'0','offerRank':'4','imageURL':null,'offerPrice':'58.0','offerDescriptionWeb':'VOICE | 0 | MIN; SMS | 0 | SMS; DATA | 3 | GB; TK | 58 | TK; VAL | 7 | DAYS; TARIFF | 0 | P / SEC ON - NET; TARIFF | 0 | P / SEC OFF - NET; CAT | DAT | ','offerShortDescription':'3GB@58Tk - 7D','offerLongDescription':'3GB@58Tk - 7D(Incl all taxes)'},{'offerID':'20331','offerName':'DBSS_14min@9Tk - 2 day','offerDescription':'15min@9Tk - 2 day(including all taxes)','offerScore':'0','offerRank':'5','imageURL':null,'offerPrice':'9.0','offerDescriptionWeb':'VOICE | 14 | MIN; SMS | 0 | SMS; DATA | 0 | MB; TK | 9 | TK; VAL | 2 | DAY; TARIFF | 0 | P / SEC ON - NET; TARIFF | 0 | P / SEC OFF - NET; CAT | VOI | ','offerShortDescription':'14min@9Tk - 2D','offerLongDescription':'14min@9Tk - 2 Days(including all taxes)'},{'offerID':'20278','offerName':'DBSS_9min @6Tk - 1 day','offerDescription':'10min @6Tk - 1 day(including all taxes)','offerScore':'0','offerRank':'6','imageURL':null,'offerPrice':'6.0','offerDescriptionWeb':'VOICE | 9 | MIN; SMS | 0 | SMS; DATA | 0 | MB; TK | 6 | TK; VAL | 1 | DAY; TARIFF | 0 | P / SEC ON - NET; TARIFF | 0 | P / SEC OFF - NET; CAT | VOI | ','offerShortDescription':'9mins@6Tk - 1D','offerLongDescription':'9mins@6Tk - 1 day(including all taxes)'}]";
                //watch.Stop();
                //new Log().Info("Cms " + watch.ElapsedMilliseconds);
                new BORequestSummary().Add($"{AppSettings.Provider}.Connector.{destination}.{methodName}", watch.ElapsedMilliseconds,false);
            }
            catch (WebException ex)
            {
                //  ToTextFileLog(ex, rtt, msisdn, url, "BODY:  "
                //  + data, message);
                try
                {
                    //new LogWriter().ToTextFileLog(ex, watch.ElapsedMilliseconds, msisdn, url, "BODY:  "+ data, message);
                    new BORequestSummary().Add($"{AppSettings.Provider}.Connector.{destination}.{methodName}",
                        watch.ElapsedMilliseconds, true);
                }
                catch (Exception e)
                {
                    throw e;
                }

                ;
                //if (ex.Status == WebExceptionStatus.ProtocolError)
                //{
                //    throw new BEHandledException(Convert.ToInt32(((HttpWebResponse)ex.Response).StatusCode), ((HttpWebResponse)ex.Response).StatusDescription,ex.Message);
                //    //Console.WriteLine("Status Code : {0}", ((HttpWebResponse)e.Response).StatusCode);
                //    //Console.WriteLine("Status Description : {0}", ((HttpWebResponse)e.Response).StatusDescription);
                //}
                //else
                throw ex;
            }

            return response;
        }
        
        public async Task<List<MyOfferListResponse>> PostHttpClient(string url, string data, string msisdn, string destination, string methodName)
        {
            //CmsRequestLog requestLog = new CmsRequestLog();
            //url = "http://172.16.11.52/home/cms";
            List<MyOfferListResponse> myOfferListResponse = null;
            //requestLog.Name = $"{AppSettings.Provider}.Connector.{destination}.{methodName}";
            //requestLog.RequestDate = DateTime.Now;
            //requestLog.Msisdn = msisdn;
            //requestLog.Url = url;

            Stopwatch watch = Stopwatch.StartNew();
            try
            {
                
                //myOfferListResponse = await new HttpClientHandler().Post<List<MyOfferListResponse>>(url, AppSettings.CMSHttpRequestTimeOut, data);
                myOfferListResponse = await new HttpClientFactoryHandler(clientFactory, clientName).Post<List<MyOfferListResponse>>(url, AppSettings.CMSHttpRequestTimeOut,data);
                new BORequestSummary().Add($"{AppSettings.Provider}.Connector.{destination}.{methodName}", watch.ElapsedMilliseconds, false);
            }
            catch (Exception ex)
            {
                
                //if (LogConfig.IsWriteTPSRequestLog)
                //{
                //    requestLog.Rtt = watch.ElapsedMilliseconds;
                //    requestLog.ResponseDate = DateTime.Now;
                //    requestLog.Error = ex.Message;

                //    new BOrequestlog().Save(requestLog);
                //}

                //new LogWriter().ToTextFileLog(ex, watch.ElapsedMilliseconds, msisdn, url, "BODY:  " + data, ex.Message);
                new BORequestSummary().Add($"{AppSettings.Provider}.Connector.{destination}.{methodName}", watch.ElapsedMilliseconds, true);
                throw ex;
            }
            finally
            {

                watch.Stop();
            }

            return myOfferListResponse;
        }

        //public void ToTextFileLog(Exception exception, double rtt, string msisdn, string url, string request,
        //    string response)
        //{
        //    var fileName = "ErrorLog";
        //    var fileType = "txt";
        //    var message = string.Empty;
        //    var exceptions = exception.GetAllExceptions();
        //    //var startupPath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/{0}/", folderName));
        //    // var startupPath = RequestLogPath;
        //    //  var startupPath = "E:\\NewPublish\\APIHub\\Log\\CMSPostRequest\\";  
        //    var startupPath = Path.Combine(AppSettings.ErrorLogFilePath, "CMSPostRequest");
        //    Directory.CreateDirectory(startupPath);
        //    var filePath = Path.Combine(startupPath, fileName + "_" + DateTime.Now.ToString("yyyy-MM-dd") + "." + fileType);
        //    //if (!Directory.Exists(filePath))
        //    //{
        //    //    message += "Date|Time|MSISDN|FileName|MethodName|LineNumber|Message|StackTrace" + Environment.NewLine;
        //    //}


        //    foreach (var ex in exceptions)
        //    {
        //        var msg = string.Empty;
        //        var st = new StackTrace(ex, true);
        //        var frame = st.GetFrame(st.FrameCount - 1);

        //        if (st.FrameCount == 0 || frame == null) continue;
        //        msg = DateTime.Now.ToString("yyyy-MM-dd") + "|"
        //                                                  + DateTime.Now.ToString("hh:mm:ss") + "|"
        //                                                  + msisdn + "|"
        //                                                  + Path.GetFileName(frame.GetFileName()) + "|"
        //                                                  + frame.GetMethod()?.Name + "|"
        //                                                  + frame.GetFileLineNumber() + "|"
        //                                                  + ex.Message + "|"
        //                                                  + ex.StackTrace;

        //        msg += Environment.NewLine;
        //        message += msg;
        //    }

        //    message += "======" + msisdn + "==============" + rtt + "==========================" + Environment.NewLine;
        //    ;
        //    message += url + Environment.NewLine;
        //    message += request + Environment.NewLine;
        //    message += response + Environment.NewLine;
        //    lock (_tempFileLocker)
        //    {
        //        File.AppendAllText(filePath, message);
        //    }
        //}
    }

    //public static class CMSRequestHelper
    //{
    //    public static string AcceptOfferRequest =
    //        "{ \"msisdn\":1932899336,\n\"offerID\":\'123\',  \n\"channelID\":5, \n\"salesChannelID\":11 }\n";

    //    public static string PresentOfferListRequest =
    //        "{\"msisdn\":123456789, \n\"language\":\"EN\", \n\"channelID\":5, \n\"serviceTypeID\":1, \n\"salesChannelID\":11 \n}\n\n\n\n\n\n ";

    //    public static string PresentOfferDetailRequest =
    //        "{\"msisdn\":123456789, \n\"offerID\":\"123\"\n\"language\":\"EN\", \n\"channelID\":5, \n\"serviceTypeID\":1, \n\"salesChannelID\":11 \n}\n\n\n\n\n\n";
    //}


    //public static class CMSResponseHelper
    //{
    //    public static string PresentOfferListResponse =
    //        "[\n    {\n        \"offerID\": \"123\",\n        \"offerName\": \"1GB $10 7Days\",\n        \"offerDescription\": \"Buy a 1GB Data Bundle for 10$, expires in 7 Days from purchase\",\n        \"imageURL\": \"http://resource_link1\",\n        \"offerRank\": 1,\n        \"offerPrice\": 10.5,\n        \"offerScore\": 234,\n        \"offerLongDescription\": \"Buy a 1GB Data Bundle for 10$, expires in 7 Days from purchase to enjoy navigating the internet\",\n        \"offerCategoryName\": \"Bundle\"\n    },\n    {\n        \"offerID\": \"555\",\n        \"offerName\": \"1GB $20 30Days\",\n        \"offerDescription\": \"Buy a 1GB Data Bundle for 10$, expires in 7 Days from purchase\",\n        \"imageURL\": \"http://resource_link1\",\n        \"offerRank\": 2,\n        \"offerPrice\": 20,\n        \"offerScore\": 134,\n        \"offerLongDescription\": \"Buy a 1GB Data Bundle for 10$, expires in 7 Days from purchase to enjoy navigating the internet\",\n        \"offerCategoryName\": \"Bundle\"\n    },\n    {\n        \"offerID\": \"56\",\n        \"offerName\": \"100MinNat $5 7Days\",\n        \"offerDescription\": \"Buy 100 National Minutes for 5$, expires in 7 Days from purchase\",\n        \"imageURL\": \"http://resource_link1\",\n        \"offerRank\": 3,\n        \"offerPrice\": 3,\n        \"offerScore\": 45,\n        \"offerLongDescription\": \"Buy 100 National Minutes for 5$, expires in 7 Days from purchase to be close to all your friends\",\n        \"offerCategoryName\": \"Bundle\"\n    }\n]";

    //    public static string AcceptOfferResponse = "{\"ID\":754535354543,\n\"Status\":\"success\"}\n\n\n";

    //    public static string CMSErrorResponse =
    //        "{\"platformID\":7, \n\"errorCode\":101, \n\"errorType\":\"FUNCTIONAL\", \n\"errorMessage\":\"MSISDN not available in IOM\"}\n\n\n";

    //    //  public static string PresentOfferListResponse = "[{ \"offerID\": \"234\", \n\"offerName\": \"Opt-in to Recharge Campaign\", \n\"offerDescription\": \"Accept this Offer and you will have the chance to get 500NatMin for 30 Days. Details to come\", \n\"imageURL\": \"http://resource_linkn\", \n\"offerRank\": \"5\", \n\"offerPrice\": \"5\", \n\"offerScore\": \"0\", \n\"offerLongDescription\": \"Accept this Offer and you will have the chance to get 500NatMin for 30 Days. Follow campaign rules after opting in\", \n\"offerCategoryName\": \"Top-up\" \n} ,\n{ \"offerID\": \"234\", \n\"offerName\": \"Opt-in to Recharge Campaign\", \n\"offerDescription\": \"Accept this Offer and you will have the chance to get 500NatMin for 30 Days. Details to come\", \n\"imageURL\": \"http://resource_linkn\", \n\"offerRank\": \"5\", \n\"offerPrice\": \"5\", \n\"offerScore\": \"0\", \n\"offerLongDescription\": \"Accept this Offer and you will have the chance to get 500NatMin for 30 Days. Follow campaign rules after opting in\", \n\"offerCategoryName\": \"Top-up\" \n} \n]\n\n\n\n\n";
    //    public static string PresentOfferDetailsResponse =
    //        "{\"offerID\": \"123\", \n\"offerName\": \"1GB $10 7Days\", \n\"offerDescription\": \"Buy a 1GB Data Bundle for 10$, expires in 7 Days from purchase\", \n\"imageURL\": \"http://resource_link1\", \n\"offerRank\": \"1\", \n\"offerPrice\": \"10.5\", \n\"offerScore\": \"-1.75\", \n\"offerLongDescription\": \"Buy a 1GB Data Bundle for 10$, expires in 7 Days from purchase to enjoy navigating the internet\", \n\"offerCategoryName\": \"Bundle\" \n} \n\n\n\n\n\n\n";
    //}
}