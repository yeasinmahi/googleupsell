﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using BusinessEntity.Google.Notification;
using BusinessObject;
using Confluent.Kafka;
using LogService;
using Newtonsoft.Json;
using Utility.Extensions;

namespace Connector.Listener
{
    public class KafkaNotificationConsumer
    {
        public static bool RunService { get; set; } = true;
        public static string[] Triggers = { "T02", "T09", "T24" };

        //private ConsumerConfig GetConsumerProviderConfiguration()
        //{
        //    var conf = new ConsumerConfig
        //    {
        //        GroupId = AppSettings.GroupId,
        //        BootstrapServers = AppSettings.BootstrapServers,
        //        // Note: The AutoOffsetReset property determines the start offset in the event
        //        // there are not yet any committed offsets for the consumer group for the
        //        // topic/partitions of interest. By default, offsets are committed
        //        // automatically, so in this example, consumption will only start from the
        //        // earliest message in the topic 'my-topic' the first time you run the program.
        //        SessionTimeoutMs = 10000,
        //        AutoOffsetReset = AutoOffsetReset.Earliest,
        //        SecurityProtocol = SecurityProtocol.Plaintext,

        //    };
        //    return conf;
        public ConsumerConfig GetConsumerProviderConfiguration(string groupId)
        {
            var conf = new ConsumerConfig
            {
                GroupId = groupId,
                BootstrapServers = AppSettings.BootstrapServers,
                // Note: The AutoOffsetReset property determines the start offset in the event
                // there are not yet any committed offsets for the consumer group for the
                // topic/partitions of interest. By default, offsets are committed
                // automatically, so in this example, consumption will only start from the
                // earliest message in the topic 'my-topic' the first time you run the program.
                SessionTimeoutMs = 10000,
                
                AutoOffsetReset = AppSettings.IsEarlyKafkaAutoOffsetReset?AutoOffsetReset.Earliest:AutoOffsetReset.Latest,
                SecurityProtocol = SecurityProtocol.Plaintext,
                EnableAutoCommit = true,
                AutoCommitIntervalMs = 10000,
            };
            return conf;
        }

        public void MessageListener()
        {
            Console.WriteLine(@"Listener Started: ");

            try
            {
                //int x = 0;

                Console.WriteLine(@"Listener for Single: ");
                try
                {
                    using (var c = new ConsumerBuilder<Ignore, string>(GetConsumerProviderConfiguration(AppSettings.GroupId)).Build())
                    {
                        c.Subscribe(AppSettings.Topic);

                        var cts = new CancellationTokenSource();
                        Console.CancelKeyPress += (_, e) =>
                        {
                            e.Cancel = true; // prevent the process from terminating.
                                RunService = false;
                            cts.Cancel();
                        };

                        try
                        {
                            while (RunService)
                            {
                                ReadMessageFromKafka(c, cts);
                            }
                        }
                        catch (OperationCanceledException)
                        {
                            c.Close();
                           // throw;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Task.Run(() => new Log().Error(ex));
                    Console.WriteLine($@"Error occurred: {ex.Message}");
                }

            }
            catch (Exception ex)
            {
                Task.Run(() => new Log().Error(ex));
                Console.WriteLine($@"Error occurred: {ex.Message}");
            }

        }

        public void MessageListenerSingleBatch()
        {
            Console.WriteLine(@"Listener for Single: ");
            try
            {
                using (var c = new ConsumerBuilder<Ignore, string>(GetConsumerProviderConfiguration(AppSettings.GroupId)).Build())
                {
                    c.Subscribe(AppSettings.Topic);

                    var cts = new CancellationTokenSource();
                    Console.CancelKeyPress += (_, e) =>
                    {
                        e.Cancel = true; // prevent the process from terminating.
                        RunService = false;
                        cts.Cancel();
                    };

                    try
                    {
                        ReadMessageFromKafka(c, cts);
                    }
                    catch (OperationCanceledException)
                    {
                        c.Close();
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                Task.Run(() => new Log().Error(ex));
                Console.WriteLine($@"Error occurred: {ex.Message}");
            }
        }


        //public PushNotification SingleMessageListener()
        //{
        //    PushNotification pushNotification = null;
        //    try
        //    {
        //        using (var c = new ConsumerBuilder<Ignore, string>(GetConsumerProviderConfiguration(AppSettings.GroupId)).Build())
        //        {
        //            c.Subscribe(AppSettings.Topic);

        //            var cts = new CancellationTokenSource();
        //            Console.CancelKeyPress += (_, e) =>
        //            {
        //                e.Cancel = true; // prevent the process from terminating.
        //                RunService = false;
        //                cts.Cancel();
        //            };

        //            try
        //            {
        //                pushNotification = ReadMessageFromKafka(c, cts);
        //            }
        //            catch (OperationCanceledException)
        //            {
        //                c.Close();
        //                throw;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        new Log().Error(ex);
        //        Console.WriteLine($@"Error occurred: {ex.Message}");
        //    }
        //    return pushNotification;
        //}

        public PushNotification ReadMessageFromKafka(IConsumer<Ignore, string> consumer, CancellationTokenSource cts)
        {
            Console.WriteLine(@"Read from Kafka");
            PushNotification pushNotification= null;
            try
            {
                var cr = consumer.Consume(cts.Token);
                Console.WriteLine($@"Consumed message '{cr.Value}' at: '{cr.TopicPartitionOffset}'.");
                
                pushNotification = JsonConvert.DeserializeObject<PushNotification>(cr.Value);
                //if(pushNotification.TriggerId == "T09")
                //{
                //    Task.Run(() => new Log().Info(pushNotification.NotificationTime));
                //}
                if (Triggers.Any(pushNotification.TriggerId.Contains))
                {
                    pushNotification.Msisdn = pushNotification.Msisdn.MySubString(10, false);
                    //if (pushNotification.TRIGGER_ATTR_01.StartsWith("EV01_"))
                    //{
                    //    pushNotification.TRIGGER_ATTR_01= pushNotification.TRIGGER_ATTR_01.Substring(5);
                    //}
                    if (pushNotification.TRIGGER_ATTR_01.StartsWith("EV01_"))
                    {
                        pushNotification.TRIGGER_ATTR_01 = pushNotification.TRIGGER_ATTR_01?.Replace("EV01_","");
                    }


                    new BOPushNotification().SaveKafka(pushNotification).Wait();


                }

            }
            catch (ConsumeException e)
            {
                Task.Run(() => new Log().Error(e));
                Console.WriteLine($@"Error occurred: {e.Error.Reason}");
                //throw e;
            }
            catch (Exception e)
            {
                Task.Run(() => new Log().Error(e));
                Console.WriteLine($@"Error occurred: {e.Message}");
                //throw e;
            }

            return pushNotification;
        }
    }
}