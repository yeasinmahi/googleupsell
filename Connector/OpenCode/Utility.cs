﻿using System;
using System.ComponentModel;
using System.Linq;

namespace Connector.OpenCode
{
    public static class Utility
    {
        private static readonly Random random = new Random();

        public static string GetNewGuid(ProcessingStatus processingStatus)
        {
            var newId = "";
            var guid = new Guid();
            guid = Guid.NewGuid();
            newId = Convert.ToString(guid) + "-" + RandomString(6);
            return newId;
        }

        public static string GetNewGuid(RequestStatus requestStatus)
        {
            var newId = "";
            var guid = new Guid(); //000-0000-00000
            guid = Guid.NewGuid();
            newId = Convert.ToString(guid) + "-" + RandomString(6);
            return newId;
        }

        public static string GetNewGuid(int type)
        {
            var newId = "";
            var guid = new Guid(); //000-0000-00000
            guid = Guid.NewGuid();
            newId = Convert.ToString(guid) + "-" + RandomString(6);
            return newId;
        }


        public static string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string GetEnumDescription(Enum value)
        {
            try
            {
                var fi = value.GetType().GetField(value.ToString());

                var attributes =
                    (DescriptionAttribute[]) fi.GetCustomAttributes(
                        typeof(DescriptionAttribute),
                        false);

                if (attributes != null &&
                    attributes.Length > 0)
                    return attributes[0].Description;
                return value.ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static string ProcessMSISDN(string msisdn, bool isRemoved)
        {
            var res = msisdn;
            try
            {
                if (isRemoved)
                {
                    if (msisdn.Length > 10) res = msisdn.Substring(msisdn.Length - 10);
                }
                else
                {
                    if (msisdn.Length > 10) res = msisdn.Substring(msisdn.Length - 10);

                    res = "880" + res;
                }
            }
            catch (Exception)
            {
            }

            return res;
        }

        public static string ConvertDateToYYYYMMDD(string date)
        {
            var fDate = "";
            try
            {
                var day = Convert.ToString(Convert.ToDateTime(date).Day);
                var month = Convert.ToString(Convert.ToDateTime(date).Month);
                var year = Convert.ToString(Convert.ToDateTime(date).Year);

                if (day.Length == 1) day = "0" + day;
                if (month.Length == 1) month = "0" + month;

                fDate = year + "-" + month + "-" + day;
            }
            catch (Exception)
            {
            }

            return fDate;
        }

        public static DateTime GetEligibleDate(int billDay, int additionDay)
        {
            var date = DateTime.Now;
            try
            {
                if (DateTime.Now.Day > billDay)
                {
                    date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, billDay).AddDays(additionDay);
                }
                else
                {
                    if (billDay > 28)
                    {
                        billDay = new DateTime(date.Year, date.Month, 1).AddMonths(1).AddDays(-1).Day;
                        additionDay = additionDay + 1;
                    }

                    date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, billDay).AddMonths(-1)
                        .AddDays(additionDay);
                }
            }
            catch (Exception)
            {
            }

            return date;
        }
    }

    public enum GenericProcessingStatus
    {
        Initial = 1,
        Processed = 2,
        Error = -1
    }

    public enum ProcessingStatus
    {
        [Description("Subscriber Existence")] Initial = 2,

        //Status Code = 400 - 403
        [Description("Subscriber Existence")] PreValidation = 2,
        PreValidationFail = 3,
        PreValidationError = -2,
        [Description("Ninety Days")] PreValidationNextStep = 12,

        //Status Code = 415
        [Description("Ninety Days")] NinetyDays = 12,
        NinetyDaysFail = 13,
        NinetyDaysError = -12,

        [Description("Contractual Obligation")]
        NinetyDaysNextStep = 22,


        //MFS Manage Code
        [Description("MFS Stage")] MFSStage = 21,
        MFSStageFail = 27,


        //Status Code = 414
        [Description("Contractual Obligation")]
        ContractualObligation = 22,
        ContractualObligationFail = 23,
        ContractualObligationError = -22,
        [Description("Msisdn Type")] ContractualObligationNextStep = 24,

        //Status Code = 416
        [Description("Msisdn Type")] MsisdnType = 24,
        MsisdnTypeFail = 25,
        MsisdnTypeError = -24,
        [Description("Tabs Status")] MsisdnTypeNextStep = 32,


        //Status Code = 404
        [Description("Tabs Status")] TabsStatus = 32,
        TabsStatusFail = 33,
        TabsStatusInActiveFail = 35,
        TabsStatusError = -32,
        [Description("Loan")] TabsStatusPrepaidNextStep = 42,
        [Description("Bill")] TabsStatusPostpaidNextStep = 52,

        [Description("Loan")] Loan = 42,
        LoanFail = 43,
        LoanError = -42,
        [Description("NID Check")] LoanNextStep = 92,

        [Description("Bill")] Bill = 52,
        BillFail = 53,
        BillError = -52,
        [Description("NID Check")] BillNextStep = 92,

        [Description("NID Check")] NIDCheck = 92,
        NIDCheckFail = 93,
        NIDCheckError = -92,
        NIDCheckNextStep = 100,

        [Description("XML Validation")] XMLValidationFail = 99,

        Completed = 100
    }

    public enum RequestStatus
    {
        Initial = 1,
        ProcessValidationResponse = 20,
        ProcessValidationResponseError = -1,
        ProcessCanceled = 50,
        Completed = 100
    }

    public enum ResponseStatus
    {
        OK = 1,
        ERROR = 2
    }

    public enum LogType
    {
        Success,
        Fail,
        Error
    }

    public enum SuspensionType
    {
        SuspensionIn = 1,
        SuspensionOut = 2,
        Resolved = 3
    }

    public enum SMSChannelEnum
    {
        Default = 0,
        CFL = 1
    }

    public enum BrowseSourceEnum
    {
        NotDefined = 0,
        MNPService = 40
    }

    public enum SMSQueueStatus
    {
        Pending = 20,
        Success = 40,
        Error = 50
    }

    public enum SuspensionRequestStatus
    {
        Initial = 1,
        OGBAR = 2,
        OGBARFail = -1,
        FullOGBARStage = 3, // Permanent Suspension Stage
        FullOGBAR = 4,
        FullOGBARFail = -3
    }

    public static class DBConnectionName
    {
        public static string BLDW = "bldw";
        public static string HVCDB = "HVCDB";
        public static string TABSPROD = "tabsprod";
        public static string DWHPULL = "DWH_Pull";
    }
}