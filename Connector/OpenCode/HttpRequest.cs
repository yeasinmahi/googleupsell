﻿using System;
using System.IO;
using System.Net;
using BusinessEntity.ConnectorModels.OpenCode;

namespace Connector.OpenCode
{
    public class HttpRequest
    {
        public RetMessage HttpPost(string url, string json, int httpRequestTimeOut = 5000)
        {
            RetMessage message;
            var result = "";
            try
            {
                var httpWebRequest = (HttpWebRequest) WebRequest.Create(url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                if (httpRequestTimeOut > 0) httpWebRequest.Timeout = httpRequestTimeOut;

                ServicePointManager.SecurityProtocol =
                    SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                using(var httpResponse = (HttpWebResponse) httpWebRequest.GetResponse())
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                message = new SetMessage().SetSuccessMessage("Success", result);
                
            }
            catch (Exception ex)
            {
                message = new SetMessage().SetErrorMessage("Error:" + ex.Message, ex.Message);
            }

            return message;
        }

        public string HttpSMSRequest(string URL, int httpRequestTimeOut)
        {
            var httpResponse = string.Empty;
            try
            {
                var request = WebRequest.Create(URL);
                request.Timeout = httpRequestTimeOut;

                request.ContentType = "application/x-www-form-urlencoded";

                using (var response = (HttpWebResponse) request.GetResponse())
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    httpResponse = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return httpResponse;
        }
    }


    public class TokenObject
    {
        public string SessionToken { get; set; }
        public bool ISAuthenticate { get; set; }
        public string AuthenticationMessage { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DeviceId { get; set; }
        public bool HasUpdate { get; set; }
        public string MinimumScore { get; set; }
        public string OptionalMinimumScore { get; set; }
        public string MaximumRetry { get; set; }
        public string RoleAccess { get; set; }
    }

    public class DeactivateResponse
    {
        public bool Result { get; set; }
        public string Message { get; set; }
    }

    public class NIDCheckResponse
    {
        public short success { get; set; }
        public string errordesc { get; set; }
        public string biometricdate { get; set; }
    }
}