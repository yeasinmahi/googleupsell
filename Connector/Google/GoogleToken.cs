﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using BusinessEntity.Google;
using Connector.Helpers;
using JWT.Algorithms;
using JWT.Builder;
using LogService;
using Newtonsoft.Json;

namespace Connector.Google
{
    public class GoogleToken
    {
        private static string accessToken = string.Empty;
        private readonly IHttpClientFactory _clientFactory;
        public GoogleToken(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        //public static string AccessToken
        //{
        //    get
        //    {
        //        if (AccessTokenValidTill.AddMinutes(-1) < DateTime.Now
        //                || string.IsNullOrEmpty(accessToken))
        //            _ = new GoogleToken().GetGoogleAccessToken();
        //        return accessToken;
        //    }

        //    //set; 
        //}

        public async Task<string> AccessToken()
        {

            try
            {
                if (string.IsNullOrEmpty(accessToken) || AccessTokenValidTill.AddMinutes(-1) < DateTime.Now)
                {
                    await new GoogleToken(_clientFactory).GetGoogleAccessToken();
                }
            }
            catch (BeHandledException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return accessToken;

        }

        public static void ClearAccessToken()
        {
            accessToken = null;
        }

        public static DateTime AccessTokenValidTill { get; set; } = DateTime.Now;

        private object _locker = new object();
        
        private string GetTokenJWT()
        {
            lock (_locker)
            {
                var utc0 = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                var issueTime = DateTime.UtcNow;

                var iat = (int)issueTime.Subtract(utc0).TotalSeconds;
                var exp = (int)issueTime.AddMinutes(1).Subtract(utc0)
                    .TotalSeconds; // Expiration time is up to 1 hour, but lets play on safe side
                //new Log().Info();
                var path = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "key.p12");
                //var path = "E:\\Yeasin\\Publish\\Selfcare.Service\\key.p12";
                if (!File.Exists(path))
                {
                    new Log().Info("Certificate file not found. "+ path);
                }
                else
                {
                    new Log().Info("Certificate file found. " + path);
                }
                RS256Algorithm algorithm = new RS256Algorithm(new X509Certificate2(path, AppSettings.GoogleTokenSecretKey, X509KeyStorageFlags.Exportable));


                var builder = new JwtBuilder();
                var token = builder.WithAlgorithm(algorithm)
                    .AddClaim("iss", AppSettings.Issuer)
                    .AddClaim("sub", AppSettings.Sub)
                    .AddClaim("scope", AppSettings.Scope)
                    .AddClaim("aud", AppSettings.Audience)
                    .AddClaim("exp", exp)
                    .AddClaim("iat", iat)
                    .Encode();

                return token;
            }
        }

        //public async Task<GoogleTokenResponse> GetGoogleAccessToken()
        //{
        //    GoogleTokenResponse googleTokenResponse;
        //    try
        //    {
        //        string googleTokenJWT = GetToken();
        //        HttpClient client = new HttpClient();
        //        Dictionary<string, string> values = new Dictionary<string, string>{
        //        { "grant_type", "urn:ietf:params:oauth:grant-type:jwt-bearer" },
        //        { "assertion", googleTokenJWT }};
        //        FormUrlEncodedContent content = new FormUrlEncodedContent(values);
        //        var response = await client.PostAsync("https://oauth2.googleapis.com/token", content);
        //        var responseString = await response.Content.ReadAsStringAsync();
        //        googleTokenResponse = JsonConvert.DeserializeObject<GoogleTokenResponse>(responseString);

        //        //request to Google for access token
        //        AccessToken = googleTokenResponse.access_token;
        //        AccessTokenValidTill = DateTime.UtcNow.AddSeconds(googleTokenResponse.expires_in);
        //    }
        //    catch (ArgumentNullException ex)
        //    {
        //        return null;
        //    }
        //    catch (HttpRequestException ex)
        //    {
        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }

        //    return googleTokenResponse;
        //}
        //public async Task GetGoogleAccessToken()
        //{
        //    try
        //    {

        //        string googleTokenJwt = GetTokenJWT();
        //        Dictionary<string, string> values = new Dictionary<string, string>{
        //        { "grant_type", "urn:ietf:params:oauth:grant-type:jwt-bearer" },
        //        { "assertion", googleTokenJwt }};
        //        FormUrlEncodedContent content = new FormUrlEncodedContent(values);

        //        string data = JsonConvert.SerializeObject(values);
        //        byte[] dataBytes = Encoding.UTF8.GetBytes(data);
        //        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://oauth2.googleapis.com/token");
        //        //int httpRequestTimeOut = AppSettings.CMSHttpRequestTimeOut;
        //        //request.Timeout = AppSettings.CMSHttpRequestTimeOut;

        //        request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
        //        request.ContentLength = dataBytes.Length;
        //        request.ContentType = "application/json";
        //        request.Method = "POST";
        //        using (Stream requestBody = request.GetRequestStream())
        //        {
        //            requestBody.Write(dataBytes, 0, dataBytes.Length);
        //        }

        //        string responseString;
        //        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        //        using (Stream stream = response.GetResponseStream())
        //        using (StreamReader reader = new StreamReader(stream))
        //        {
        //            responseString = reader.ReadToEnd();
        //        }

        //        //var response = await client.PostAsync("https://oauth2.googleapis.com/token", content);
        //        //var responseString = await response.Content.ReadAsStringAsync();
        //        var googleTokenResponse = JsonConvert.DeserializeObject<GoogleTokenResponse>(responseString);

        //        //request to Google for access token
        //        AccessToken = googleTokenResponse.access_token;
        //        AccessTokenValidTill = DateTime.UtcNow.AddSeconds(googleTokenResponse.expires_in);
        //    }
        //    catch (Exception ex)
        //    {
        //        await new Log(string.Empty).Error(ex);
        //    }

        //    //return googleTokenResponse;
        //}

        public async Task GetGoogleAccessToken()
        {
            try
            {
                var googleTokenJwt = GetTokenJWT();
                new Log(string.Empty).Info(googleTokenJwt);
                var values = new Dictionary<string, string>
                {
                    {"grant_type", "urn:ietf:params:oauth:grant-type:jwt-bearer"},
                    {"assertion", googleTokenJwt}
                };
                //var content = new FormUrlEncodedContent(values);

                var data = JsonConvert.SerializeObject(values);


                //var googleTokenResponse = await new HttpWebRequestHelper().Post<GoogleTokenResponse>(
                //    "https://oauth2.googleapis.com/token", AppSettings.HttpRequestTimeOut, data);

                var googleTokenResponse = await new HttpClientFactoryHandler(_clientFactory, "googleAuthHttpClientFactory").Post<GoogleTokenResponse>(
                    "https://oauth2.googleapis.com/token", AppSettings.HttpRequestTimeOut, data);

                //request to Google for access token
                accessToken = googleTokenResponse.access_token;
                AccessTokenValidTill = DateTime.Now.AddSeconds(googleTokenResponse.expires_in);
                new Log().Info("Access Token: "+accessToken+" and valid till:"+AccessTokenValidTill);
                if (accessToken == null)
                {
                    throw new BeHandledException(BEMessageCodes.GoogleTokenProblem.Status, BEMessageCodes.GoogleTokenProblem.error, "A problem occurred on Google token fetching");
                }
                //if (token.Status == 401)
                //    throw new BEHandledException(BEMessageCodes.InvalidCustomer.Status, BEMessageCodes.InvalidCustomer.error, "Invalid email or password");
                //else if (token.Status != 200)
                //    throw new BEHandledException(BEMessageCodes.InvalidCustomer.Status, BEMessageCodes.InvalidCustomer.error, token.Error);

            }
            catch (BeHandledException ex)
            {
                ClearAccessToken();
                //await new Log(string.Empty).Error(ex);
                throw ex;
            }
            catch (Exception ex)
            {
                ClearAccessToken();
                await new Log(string.Empty).Error(ex);
                throw ex;
            }

            //return googleTokenResponse;
        }

        public bool RefreshToken()
        {
            try
            {
                if (AccessTokenValidTill < DateTime.UtcNow.AddMinutes(-1)) _ = new GoogleToken(_clientFactory).GetGoogleAccessToken();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}