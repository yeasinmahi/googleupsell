﻿using BusinessEntity.APIHub2;
using BusinessEntity.Google.Notification;
using BusinessObject;
using Connector.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Utility;

namespace Connector.Google
{
    public class GoogleNotification: BaseHandler
    {
        private readonly IHttpClientFactory _clientFactory;
        public GoogleNotification(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }
        public async Task SentPlanStatusNotification(PlanStatus planStatus, string cpid)
        {
            try
            {
                var url = string.Format(AppSettings.GooglePushNotificationURL, cpid);

                var httphelper = new HttpWebRequestHelper();

                //var planstatus = await new HttpWebRequestHelper().Post<PlanStatusResponse>(url, AppSettings.GoogleHttpRequestTimeOut,
                //    JsonConvert.SerializeObject(planStatus), new Dictionary<string, string>
                //    {
                //        {"Authorization", $"Bearer {await new GoogleToken(_clientFactory).AccessToken()}"}
                //    });
                var planstatus = await new HttpClientFactoryHandler(_clientFactory, "googleMDPHttpClientFactory").Post<PlanStatusResponse>(url, AppSettings.GoogleHttpRequestTimeOut,
                    JsonConvert.SerializeObject(planStatus), new Dictionary<string, string>
                    {
                        {"Authorization", $"Bearer {await new GoogleToken(_clientFactory).AccessToken()}"}
                    });


                //if (planstatus.Plans == null)
                //{
                //    throw new Exception(planstatus.Error.Message);
                //}

                if (planstatus.Plans == null)
                {
                    IsError = true;
                    ActualErrorMessage = planstatus.Error.Message;

                    //switch (messageData.Status)
                    //{
                    //    case 401:
                    //        if (!IsCustomerToken)
                    //            GoogleToken.ClearAccessToken();

                    //        if (throwError)
                    //            throw new BEHandledException(BEMessageCodes.UnauthorizedError.Status, BEMessageCodes.UnauthorizedError.error, messageData.Error);
                    //        break;
                    //    default:
                    //        break;
                    //}

                    throw new Exception(planstatus.Error.Message);
                }
            }
            catch (BeHandledException ex)
            {
                IsError = true;
                ActualErrorMessage = ex.CustomerMessage;
                throw ex;
            }
            catch (Exception ex)
            {
                IsError = true;
                ActualErrorMessage = ex.Message;
                throw ex;
            }
            finally
            {
                await new BORequestSummary().Add($"{ AppSettings.Provider}.Connector.{this.GetType().Name}.{CommonFunction.GetAsyncMethodName()}", (int)watch.ElapsedMilliseconds, IsError);
            }
        }



    }
}
