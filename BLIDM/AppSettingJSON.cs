﻿namespace BLIDM
{
    public class AppSettingJSON
    {
        //public class Rootobject
        //{
        //public Logging Logging { get; set; }
        public string AllowedHosts { get; set; }
        public Jwtconfiguration JwtConfiguration { get; set; }

        public Databaseconnection DataBaseConnection { get; set; }
        //public Appsettings AppSettings { get; set; }
        //}

        public class Logging
        {
            public Loglevel LogLevel { get; set; }
        }

        public class Loglevel
        {
            public string Default { get; set; }
            public string Microsoft { get; set; }
            public string MicrosoftHostingLifetime { get; set; }
        }

        public class Jwtconfiguration
        {
            public string Audience { get; set; }
            public string Issuer { get; set; }
            public string SecretKey { get; set; }
            public string Alg { get; set; }
            public int ValidForInMinitues { get; set; }
            public bool IsValidateIssuer { get; set; }
            public bool IsValidateAudience { get; set; }
            public bool IsValidateIssuerSigningKey { get; set; }
            public bool IsRequireExpirationTime { get; set; }
            public bool IsValidateLifetime { get; set; }
            public string AuthenticationScheme { get; set; }
            public string TokenType { get; set; }
        }

        public class Databaseconnection
        {
            public string DefaultConnection { get; set; }
        }

        //public class Appsettings
        //{
        //    public int OTPLength { get; set; }
        //    public int RecordsPerPage { get; set; }
        //}
    }
}