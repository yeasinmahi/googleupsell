﻿using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BLIDM.Helper;
using BusinessEntity.APIHub2;
using BusinessEntity.IDM;
using BusinessObject;
using Connector.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace BLIDM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : BaseAPIController
    {
        private readonly IConfiguration _configuration;

        public TokenController(IHttpContextAccessor accessor, IConfiguration configuration, IMapper mapper, IHttpClientFactory clientFactory) : base(
            accessor, mapper,clientFactory)
        {
            _configuration = configuration;
        }
        //private readonly ISignInRepository _signInRepository;
        //private readonly IUserRepository _userRepository;


        //{
        //    "grant_type": "password",
        //    "client_id":"AreenaPhoneBSPAndroid",
        //    "client_secret":"Phone1$@BSPAreenaAndr0id",
        //    "MSISDN": "1926662002",
        //    "CustomerPassword": "D0D64932DB655FEEABADD14C9C74E3E19BAC2EAD"
        //}
        [HttpPost]
        //      [Consumes("application/x-www-form-urlencoded")]
        public async Task<IActionResult> CreateToken( //FormDataCollection  formData, 
            [FromForm] LoginUser loginUser
            //, LoginUser loginUser
        )
        {
            var messageData = new BEMessageData<string>();

            try
            {
                string authHeader = HttpContext.Request.Headers["Authorization"];
                if (!string.IsNullOrEmpty(authHeader))
                {
                    var authHeaderSplit = authHeader.Split(" ");
                    if (authHeaderSplit != null && authHeaderSplit.Length > 1)
                        // authHeader = authHeader.Split(" ")[1];
                        if (authHeaderSplit[0].ToLower() == "basic")
                            if (authHeaderSplit[1] != "Og==")
                            {
                                var encoding = Encoding.GetEncoding("iso-8859-1");
                                authHeader = encoding.GetString(Convert.FromBase64String(authHeaderSplit[1]));
                                var seperatorIndex = authHeader.IndexOf(':');

                                var username = authHeader.Substring(0, seperatorIndex);
                                var password = authHeader.Substring(seperatorIndex + 1);

                                if (!string.IsNullOrEmpty(username)) loginUser.client_id = username;
                                if (!string.IsNullOrEmpty(password)) loginUser.client_secret = password;
                            }
                }


                if (!string.IsNullOrEmpty(HttpContext.Request.Headers["MSISDN"]))
                    loginUser.MSISDN = HttpContext.Request.Headers["MSISDN"];
                if (!string.IsNullOrEmpty(HttpContext.Request.Headers["CustomerPassword"]))
                    loginUser.CustomerPassword = HttpContext.Request.Headers["CustomerPassword"];
                if (!string.IsNullOrEmpty(HttpContext.Request.Headers["Otp"]))
                    loginUser.OTP = HttpContext.Request.Headers["Otp"];
                if (!string.IsNullOrEmpty(HttpContext.Request.Headers["scope"]))
                    loginUser.scope = HttpContext.Request.Headers["scope"];


                //  BEServiceUser serviceUserInfo = null;
                var customer = new BECustomer();

                switch (loginUser.grant_type)
                {
                    case "client_credentials":
                    case "password":
                    case "otp":


                        CheckPermission(loginUser.client_id, loginUser.client_secret, false);


                        if (!string.IsNullOrEmpty(loginUser.MSISDN))
                            // return Error.BadRequest("Invalid user name or password");
                        {
                            var bOCustomer = new BOCustomer();
                            //customer = bOCustomer.GetCustomer(loginUser.CustomerEmailAddress, loginUser.CustomerPassword);

                            //bool isValidCustomer = false;
                            switch (loginUser.grant_type)
                            {
                                case "client_credentials":
                                case "password":
                                    var isValidPassword =
                                        await bOCustomer.IsValidPassword(loginUser.MSISDN, loginUser.CustomerPassword);
                                    if (!isValidPassword)
                                        throw new BeHandledException(BEMessageCodes.InvalidCustomerPassword.Status,
                                            BEMessageCodes.InvalidCustomerPassword.error);
                                    break;
                                case "otp":
                                    var bOOTP = new BOOTP();
                                    var isValidOTP = await bOOTP.IsValidOTP(loginUser.MSISDN,
                                        AppSettings.OTPValidityDuration, loginUser.OTP,
                                        (int) ValidityReason.OneTimeActivity, AppSettings.OTPWrongAttempLimit);
                                    if (!isValidOTP)
                                        throw new BeHandledException(BEMessageCodes.InvalidOTP.Status,
                                            BEMessageCodes.InvalidOTP.error, "OTP is not valid");


                                    break;
                            }


                            var dBSSWebAPI = new DBSSWebAPI(_clientFactory);
                            var subscription = await dBSSWebAPI.GetSubscriptions(loginUser.MSISDN, "subscription-type");

                            //subscription.Datas.FindAll(c => c.Attributes.Status.ToLower() == "active").Count > 0
                            var subscriber = subscription.Datas.FindAll(c => c.Attributes.Status.ToLower() == "active").FirstOrDefault();


                            //customer.CustomerID = subscriber.Id;
                            customer.SubscriptionID = subscriber.Id;
                            customer.ContractID = subscriber.Attributes.ContractId;
                            customer.PaymentType = subscriber.Attributes.PaymentType;
                            customer.CustomerStatus = subscriber.Attributes.Status;
                            //customer.CustomerStatus = subscriber.Relationships.OwnerCustomer.Data.Id; ;
                            customer.MSISDN = loginUser.MSISDN;

                            //}
                        }

                        break;
                    case "refresh_token":
                        // var isValidRefreshToken = await _signInRepository.IsValidRefreshTokenAsync(user.Id, loginUser.RefreshToken);
                        if (string.IsNullOrEmpty(loginUser.client_id))
                            // return Error.BadRequest("Invalid user name or password");
                            //return BadRequest(messageData);
                            throw new BeHandledException(BEMessageCodes.InvalidClientIDSecret.Status,
                                BEMessageCodes.InvalidClientIDSecret.error);

                        break;
                    default:

                        //return Unauthorized(messageData);
                        ////  break;
                        throw new BeHandledException(BEMessageCodes.InvalidGrandType.Status,
                            BEMessageCodes.InvalidGrandType.error);
                }

                var tokenDto = new TokenGenerator().GenerateToken(loginUser, _configuration, ServiceUser, customer);
                ////Save Token
                //await _signInRepository.SignInAsync(tokenDto);
                //await _signInRepository.CommitChangesAsync();

                Response.Headers.Add("Authorization", $"bearer {tokenDto.access_token}");

                return await WriteAuditTrail(messageData.Status, tokenDto, string.Empty, false,
                    $"ClientID:{loginUser.client_id}");
                //    return CreatedAtRoute("GetLoggedInUserValue", "", tokenDto);
            }
            catch (BeHandledException ex)
            {
                messageData.MapBEHandledException(ex);

                await log.Error(ex);
            }
            catch (Exception ex)
            {
                messageData.MapException(ex);
                await log.Error(ex);
            }

            // return Ok(tokenDto);
            return await WriteAuditTrail(messageData.Status, messageData, string.Empty, false,
                $"ClientID:{loginUser.client_id}");
        }

        [Authorize]
        [HttpGet(Name = "GetLoggedInUserValue")]
        public IActionResult Get()
        {
            return Ok(User.Claims.Select(s => new
            {
                s.Type,
                s.Value
            }));
        }
    }
}