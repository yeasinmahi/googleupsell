﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLIDM;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Utility;

namespace IDM4.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {

        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly LoadAPPSettings loadAPPSettings;

        private readonly AppSettingJSON.Jwtconfiguration jwtconfiguration;

        public WeatherForecastController(ILogger<WeatherForecastController> logger,
            LoadAPPSettings loadAPPSettings, IOptions<AppSettingJSON.Jwtconfiguration> jwtconfiguration)
        {
            _logger = logger;
            this.loadAPPSettings = loadAPPSettings;
            this.jwtconfiguration = jwtconfiguration.Value;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();

            int xys = AppSettings.OTPLength;

            string asdfsd = jwtconfiguration.SecretKey;


            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.UtcNow.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}
