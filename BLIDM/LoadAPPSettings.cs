﻿using System;
using BusinessEntity.APIHub2;
using LogService;
using Microsoft.Extensions.Configuration;

namespace BLIDM
{
    public class LoadAPPSettings
    {
        private readonly IConfiguration _configuration;
        //static LoadAPPSettings()
        //{
        //    //AppSettings.RecordsPerPage = Convert.ToInt16(ConfigurationManager.AppSettings["RecordsPerPage"]);

        //}

        public LoadAPPSettings(IConfiguration configuration)
        {
            _configuration = configuration;

            var section = configuration.GetSection("AppSettings");

            AppSettings.IsLoadSettings = true;
            AppSettings.IsLoadConfig = true;

            AppSettings.RecordsPerPage = Convert.ToInt16(_configuration["AppSettings:RecordsPerPage"]);
            AppSettings.Provider = _configuration["AppSettings:Provider"];
            AppSettings.ServiceName = _configuration["AppSettings:ServiceName"];
            AppSettings.DBBSApiBaseURL = _configuration["AppSettings:DBBSApiBaseURL"];
            AppSettings.RequestLogPath = _configuration["AppSettings:RequestLogPath"];
            AppSettings.DBBSErrorLogPath = _configuration["AppSettings:DBBSErrorLogPath"];
            AppSettings.EnableDemoAccess = Convert.ToBoolean(_configuration["AppSettings:EnableDemoAccess"]);
            AppSettings.DBSSHttpRequestTimeOut = Convert.ToInt16(_configuration["AppSettings:DBSSHttpRequestTimeOut"]);
            AppSettings.CMSApiBaseURL = _configuration["AppSettings:CMSApiBaseURL"];
            AppSettings.OTPLength = Convert.ToInt16(_configuration["AppSettings:OTPLength"]);
            AppSettings.OTPValidityDuration = Convert.ToInt16(_configuration["AppSettings:OTPValidityDuration"]);
            AppSettings.OTPIntervalTime = Convert.ToInt16(_configuration["AppSettings:OTPIntervalTime"]);
            AppSettings.MaximumOTPInADay = Convert.ToInt16(_configuration["AppSettings:MaximumOTPInADay"]);
            AppSettings.OTPWrongAttempLimit = Convert.ToInt16(_configuration["AppSettings:OTPIntervalTime"]);
            AppSettings.OTPSMS = _configuration["AppSettings:OTPSMS"];

            AppSettings.TotalRequestSummary = GetValue<int>(section, "TotalRequestSummary");
            ConfigLog(section);
        }

        public void ConfigLog(IConfigurationSection section)
        {
            LogConfig.IsWriteErrorLogFile = GetValue<bool>(section, "IsWriteErrorLogFile");
            LogConfig.IsWriteErrorLogDb = GetValue<bool>(section, "IsWriteErrorLogDb");
            LogConfig.IsWriteRequestResponseFile = GetValue<bool>(section, "IsWriteRequestResponseFile");
            LogConfig.IsWriteRequestResponseDb = GetValue<bool>(section, "IsWriteRequestResponseDb");
            LogConfig.IsWriteRequestBodyDb = GetValue<bool>(section, "IsWriteRequestBodyDb");
            LogConfig.IsWriteRequestBodyDb = GetValue<bool>(section, "IsWriteRequestBodyDb");
            LogConfig.IsWriteTPSRequestLog = GetValue<bool>(section, "IsWriteTPSRequestLog");
            //LogConfig.IsWriteAuditTrailDb = GetValue<bool>(section, "IsWriteAuditTrailDb");
        }

        private TConvertType GetValue<TConvertType>(IConfigurationSection section, string sectionName)
        {
            var value = section[sectionName];
            var converted = default(TConvertType);
            try
            {
                converted = (TConvertType)
                    Convert.ChangeType(value, typeof(TConvertType));
            }
            catch
            {
            }

            return converted;
        }
    }
}