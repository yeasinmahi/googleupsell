//using AutoMapper;

using System.Diagnostics;
using AutoMapper;
using BLIDM.Helper;
using BusinessEntity.APIHub2;
using BusinessObject;
using LogService.Extension;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Utility.Extensions;

namespace BLIDM
{
    public class Startup
    {
        private readonly Stopwatch watch = new Stopwatch();

        public Startup(IConfiguration configuration)
        {
            watch.Start();

            Configuration = configuration;
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver
                    {NamingStrategy = new CamelCaseNamingStrategy {OverrideSpecifiedNames = false}}
            };
            new LoadAPPSettings(configuration);
            if (AppSettings.IsLoadConfig)
                BOConfiguationData.LoadAllConfigurationData(true, true);
            // AppConfigSettingsLoader.Bind();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                // Use the default property (Pascal) casing
                options.SerializerSettings.ContractResolver = new DefaultContractResolver
                    {NamingStrategy = new CamelCaseNamingStrategy {OverrideSpecifiedNames = false}};

                //// Configure a custom converter
                //options.SerializerOptions.Converters.Add(new MyCustomJsonConverter());
            });

            BuildHttpClientFactory(services);

            services.AddCustomJwt(Configuration);
            //services.AddSingleton(typeof(LoadAPPSettings));
            services.Configure<AppSettingJSON.Jwtconfiguration>(o =>
                Configuration.GetSection("Jwtconfiguration").Bind(o));
            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(1, 0);
            });
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddAutoMapper(typeof(Startup));

            ExceptionExtensions.DBBSErrorLogPath = AppSettings.DBBSErrorLogPath;
            //services.AddAutoMapper(typeof(Startup));
            //services.AddAutoMapper(c => c.AddProfile<Helper.AutoMapping>(), typeof(Startup));
            //services.AddAutoMapper(c => c.AddProfile<BusinessEntity.APIHub2.AutoMapping>(), typeof(Startup));

            //    services.Configure<AppSettingJSON>(o => Configuration.GetValue<AppSettingJSON>("asdf"));


            //LoadAPPSettings loadAPPSettings = new LoadAPPSettings(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}

            app.UseDeveloperExceptionPage();

            app.UseRouting();

            app.UseLoggerMiddleware();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            watch.Stop();
            AppSettings.ApplicationLoadingTime = watch.ElapsedMilliseconds;
        }
        public void BuildHttpClientFactory(IServiceCollection services)
        {
            services.AddHttpClient("dbssHttpClientFactory", c =>
            {
                c.DefaultRequestHeaders.Add("Accept", "application/vnd.api+json");
            });
            services.AddHttpClient("cmsHttpClientFactory", c =>
            {
                c.DefaultRequestHeaders.Add("Accept", "application/json");
            });
            services.AddHttpClient("googleAuthHttpClientFactory", c =>
            {
                c.DefaultRequestHeaders.Add("Accept", "application/json");
            });
            services.AddHttpClient("googleMDPHttpClientFactory", c =>
            {
                c.DefaultRequestHeaders.Add("Accept", "application/json");
            });
            services.AddHttpClient("googleTokenHttpClientFactory", c =>
            {
                c.DefaultRequestHeaders.Add("Accept", "application/json");
            });
        }
    }
}