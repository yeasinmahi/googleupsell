﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using BusinessEntity.APIHub2;
using BusinessEntity.IDM;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace BLIDM.Helper
{
    public class TokenGenerator
    {
        public //async 
            Token GenerateToken(LoginUser loginData, IConfiguration configuration, BEServiceUser serviceUserInfo,
                BECustomer customer)
        {
            //BEServiceUserInfo serviceUserInfo = new BEServiceUserInfo();
            //serviceUserInfo.UserID = Guid.NewGuid().ToString("N");
            //serviceUserInfo.UserName = "ClientXYZ";
            //serviceUserInfo.Password = "ClientSecretXYZ";

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.GivenName, serviceUserInfo.ClientID),
                //new Claim(ClaimTypes.Email, serviceUserInfo.Email),
                //new Claim(ClaimTypes.Email, serviceUserInfo.UserID),
                new Claim(ClaimTypes.Name, serviceUserInfo.ClientID)
            };

            //For Role
            //claims.Add(new Claim(ClaimTypes.Role, "SuperAdmin"));
            //  claims.Add(new Claim(ClaimTypes.Role, "dpa"));
            // claims.Add(new Claim(ClaimTypes.Role, "BasicData"));

            if (serviceUserInfo.UserID != 0 && serviceUserInfo.Claims.Count > 0)
                foreach (var item in serviceUserInfo.Claims)
                    claims.Add(new Claim(ClaimTypes.Role, item.Value));
            if (!string.IsNullOrEmpty(loginData.MSISDN))
                claims.Add(new Claim(ClaimTypes.Role, "CustomerToken"));

            //user.UserRoles
            //    .Distinct()
            //    .ToList()
            //    .ForEach(role =>
            //    {
            //        claims.Add(new Claim(ClaimTypes.Role, role.Roles.Name.ToLower()));
            //    });

            //claims.Add(new Claim("Channel", serviceUserInfo.UserID));

            if (!(customer == null))
            {
                if (!string.IsNullOrEmpty(customer.CustomerID))
                    claims.Add(new Claim("CustomerID", customer.CustomerID));
                if (!string.IsNullOrEmpty(customer.MSISDN)) claims.Add(new Claim("MSISDN", customer.MSISDN));
                if (!string.IsNullOrEmpty(customer.ContractID))
                    claims.Add(new Claim("ContractID", customer.ContractID));
                if (!string.IsNullOrEmpty(customer.SubscriptionID))
                    claims.Add(new Claim("SubscriptionID", customer.SubscriptionID));
                if (!string.IsNullOrEmpty(customer.CustomerStatus))
                    claims.Add(new Claim("CustomerStatus", customer.CustomerStatus));
                //if (!string.IsNullOrEmpty(customer.EmailAddress)) claims.Add(new Claim("CustomerEmail", customer.EmailAddress));
                if (!string.IsNullOrEmpty(customer.PaymentType))
                    claims.Add(new Claim("PaymentType", customer.PaymentType));
            }

            var token = new JwtTokenGenerator().GenerateToken(claims, configuration);

            token.refresh_token = Base64UrlEncoder.Encode(Guid.NewGuid().ToString("N") + loginData.client_id);
            //loginData.RefreshToken = tokens.RefreshToken;
            //loginData.CustomerEmailAddress = serviceUserInfo.UserID;
            //var tokenDto = new Token
            //{
            //    AccessToken = tokens.AccessToken,
            //    RefreshToken = tokens.RefreshToken,
            //    //UserId = serviceUserInfo.UserID,
            //    Provider = "BOS1",
            //    AccessTokenValidity = tokens.AccessTokenExpiration,
            //    RefreshTokenValidity = tokens.RefreshTokenExpiration
            //};
            //token.provider = AppSettings.Provider;

            ////Save Token
            //await _signInRepository.SignInAsync(tokenDto);
            //await _signInRepository.CommitChangesAsync();

            //   Response.Headers.Add("Authorization", $"bearer {tokens.AccessToken}");
            //tokenFromCache = new CachedToken
            //{
            //    Tokens = new CachedTokens
            //    {
            //        AccessToken = tokens.AccessToken,
            //        Expiration = tokens.Expiration,
            //        RefreshToken = tokens.RefreshToken
            //    },
            //    User = new CachedTokenUser
            //    {
            //        Id = user.Id,
            //        Email = user.Email,
            //        PhoneNumber = user.PhoneNumber,
            //        UserFullName = user.UserFullName,
            //        UserRoles = user.UserRoles.Select(s => s.Roles.Name).ToArray(),
            //        PhotoPath = string.IsNullOrEmpty(user.PhotoPath) ? null
            //        : HttpContext.Request.AddServerPath(user.PhotoPath),
            //        Designation = user.Designation
            //    },
            //};

            //await _distributedCache.SetCacheAsStringAsync(tokenCacheKey, tokenFromCache, 10);
            //return CreatedAtRoute("GetLoggedInUserValue", "", tokenFromCache);

            return token;
        }
    }
}