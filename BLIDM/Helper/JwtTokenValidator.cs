﻿using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace BLIDM.Helper
{
    public static class JwtTokenValidator
    {
        //For Central
        public static void AddCustomJwt(this IServiceCollection services, IConfiguration configuration)
        {
            var validationParameters = new TokenValidationParameters
            {
                ValidAudience = configuration["JwtConfiguration:Audience"],
                ValidIssuer = configuration["JwtConfiguration:Issuer"],
                ValidateAudience = bool.Parse(configuration["JwtConfiguration:IsValidateAudience"]),
                ValidateIssuer = bool.Parse(configuration["JwtConfiguration:IsValidateIssuer"]),
                ValidateIssuerSigningKey = bool.Parse(configuration["JwtConfiguration:IsValidateIssuerSigningKey"]),
                ValidateLifetime = bool.Parse(configuration["JwtConfiguration:IsValidateLifetime"]),
                IssuerSigningKey =
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JwtConfiguration:SecretKey"]))
            };

            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options => { options.TokenValidationParameters = validationParameters; });
        }
    }
}