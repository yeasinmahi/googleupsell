﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using BusinessEntity.IDM;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace BLIDM.Helper
{
    public class JwtTokenGenerator
    {
        public Token GenerateToken(IEnumerable<Claim> claims, IConfiguration configuration)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JwtConfiguration:SecretKey"]));
            var credentials = new SigningCredentials(key, configuration["JwtConfiguration:Alg"]);

            var token = new JwtSecurityToken(
                configuration["JwtConfiguration:Issuer"],
                configuration["JwtConfiguration:Audience"],
                claims,
                expires: DateTime.Now.AddMinutes(int.Parse(configuration["JwtConfiguration:ValidForInMinitues"])),
                signingCredentials: credentials
            );
            return new Token
            {
                access_token = new JwtSecurityTokenHandler().WriteToken(token),
                //     AccessTokenExpiration = token.ValidTo,
                //access_token_validity = token.ValidTo,
                expires_in = long.Parse(configuration["JwtConfiguration:ValidForInMinitues"])
            };
        }
    }
}