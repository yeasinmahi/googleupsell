﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestDesktop
{
    public partial class Form1 : Form
    {
        private static HttpClient _httpClient;
        private readonly SynchronizationContext synchronizationContext;
        public Form1(IHttpClientFactory httpClientFactory)
        {
            InitializeComponent();
            synchronizationContext = SynchronizationContext.Current;
            _httpClient = httpClientFactory.CreateClient("dbssHttpClientFactory");
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, "http://10.74.10.10:443/api/v1/subscriptions/63656310/relationships/products");

            var data = "{\r\n   \"data\":{\r\n      \"type\":\"products\",\r\n      \"id\":\"TK36USSD1GB4D1\",\r\n      \"meta\":{\r\n         \"services\":{\r\n\r\n         },\r\n         \"channel\":\"FB\"\r\n      }\r\n   },\r\n   \"errors\":null\r\n}";
            request.Content = new StringContent(data, Encoding.UTF8, "application/vnd.api+json");
           
            //var response = await  _httpClient.SendAsync(request);
            //string s = response.Content.ReadAsStringAsync().Result;


            //var response = _httpClient.GetAsync("http://10.74.10.10:443/api/v1/subscriptions?filter%5Bmsisdn%5D=8801404850126").Result;
            //string s = response.Content.ReadAsStringAsync().Result;

        }
        public void UpdateUI(List<Label> labels)
        {
            synchronizationContext.Post(new SendOrPostCallback(o =>
            {
                foreach (var label in labels)
                {
                    Controls.Add(label);
                }
            }), labels);
        }
    }
}
