using Microsoft.Extensions.DependencyInjection;
using System;
using System.Windows.Forms;

namespace TestDesktop
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            ConfigureServices();
            Application.Run((Form1)ServiceProvider.GetService(typeof(Form1)));
        }
        private static IServiceProvider ServiceProvider { get; set; }
        private static void ConfigureServices()
        {
            var services = new ServiceCollection();
            services.AddHttpClient();
            services.AddHttpClient("dbssHttpClientFactory", c =>
            {
                c.DefaultRequestHeaders.Add("Accept", "application/vnd.api+json");
            });
            services.AddHttpClient("cmsHttpClientFactory", c =>
            {
                c.DefaultRequestHeaders.Add("Accept", "application/json");
            });
            services.AddSingleton<Form1>();
            ServiceProvider = services.BuildServiceProvider();
        }
    }
}
