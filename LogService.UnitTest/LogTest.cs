﻿using System;
using System.Threading.Tasks;
using NUnit.Framework;

namespace LogService.UnitTest
{
    public class LogTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void GetBasePath()
        {
            var s = LogConfig.BaseLogPath;
            Assert.That(!string.IsNullOrEmpty(s));
        }

        [Test]
        public void SetBasePath()
        {
            LogConfig.BaseLogPath = "C\\Log";
            var s = LogConfig.BaseLogPath;
            Assert.That(s == "C\\Log");
        }

        [Test]
        public void GetErrorLogFileName()
        {
            var s = LogConfig.ErrorLogFileName;
            Assert.That(!string.IsNullOrEmpty(s));
        }

        [Test]
        public void SetErrorLogFileName()
        {
            LogConfig.ErrorLogFileName = "Error.txt";
            var s = LogConfig.ErrorLogFileName;
            Assert.That(s == "Error.txt");
        }

        [Test]
        public void GetErrorLogFullPath()
        {
            LogConfig.BaseLogPath = @"C\Log";
            LogConfig.ErrorLogFileName = "Error.txt";

            var s = LogConfig.ErrorLogFullPath;

            Assert.That(s == @"C\Log\Error.txt");
        }

        [Test]
        public async Task WriteTestLog()
        {
            try
            {
                throw new Exception("This is test exception thrown from LogService.UnitTest project");
            }
            catch (Exception ex)
            {
                try
                {
                    var log = new Log();
                    await log.Error(ex);
                    //Log.Error(ex, "Test", true);
                    Assert.That(true);
                }
                catch (Exception)
                {
                    Assert.That(false);
                }
            }
        }

        [Test]
        public void WriteTestLogBatch()
        {
            for (var i = 0; i < 10000; i++) WriteTestLog();
        }
    }
}