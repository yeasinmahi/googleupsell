﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using Connector.Listener;
using LogService;
using Microsoft.Extensions.Hosting;

namespace Selfcare.Service.Services
{
    internal class AllServices : BackgroundService
    {
        private readonly Log log;
        //List<Thread> kafkaThreads = null;
        //List<Thread> googlePushNotificationThreads = null;

        private readonly Thread trKafKaBalanceNotificationReader;
        private readonly Thread trPushNotificationExpiryByAmountT24;
        private readonly Thread trPushNotificationExpiryByDayT09;
        private readonly Thread trPushNotificationExpiryByUsageT02;

        private int noOfKafkaThread = AppSettings.NoOfKafkaThread;
        protected readonly IHttpClientFactory _clientFactory;
        public AllServices(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
            log = new Log("");
            trKafKaBalanceNotificationReader = new Thread(KafKaBalanceNotificationReader);
            trPushNotificationExpiryByDayT09 = new Thread(PushNotificationExpiryByDayT09);
            trPushNotificationExpiryByUsageT02 = new Thread(PushNotificationExpiryByUsageT02);
            trPushNotificationExpiryByAmountT24 = new Thread(PushNotificationExpiryByAmountT24);
        }

        private void PushNotificationExpiryByDayT09()
        {
            log.Info("Windows Service is starting from PushNotificationExpiryByDayT09", "Windows Service", true);

            var notificationProcess = new PushNotificationService(_clientFactory);
            notificationProcess.ListenerForExpiryByDayT09();
        }

        private void PushNotificationExpiryByUsageT02()
        {
            log.Info("Windows Service is starting from PushNotificationExpiryByUsageT02", "Windows Service", true);

            var notificationProcess = new PushNotificationService(_clientFactory);
            notificationProcess.ListenerForExpiryByUsageT02();
        }

        private void PushNotificationExpiryByAmountT24()
        {
            log.Info("Windows Service is starting from PushNotificationExpiryByAmountT24", "Windows Service", true);

            var notificationProcess = new PushNotificationService(_clientFactory);
            notificationProcess.ListenerForExpiryByAmountT24();
        }

        private void KafKaBalanceNotificationReader()
        {
            log.Info("Windows Service is starting from KafkaService", "Windows Service", true);

            var kafkaNotificationConsumer = new KafkaNotificationConsumer();
            kafkaNotificationConsumer.MessageListener();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            //while (!stoppingToken.IsCancellationRequested)
            //{
            //    log.Info("Windows Service is running from My Service", "Windows Service", true);
            //    await Task.Delay(1000, stoppingToken);
            //}

            //kafkaThreads = StartThread(kafkaService, noOfKafkaThread);
            //googlePushNotificationThreads = StartThread(googlePushNotificationService, noOfGooglePushNotificationThreads);
            try
            {
                if (AppSettings.IsStartKafkaReader)
                    trKafKaBalanceNotificationReader.Start();
                //trGooglePlanStatusPushNotification.Start();
                if (AppSettings.IsStartT09)
                    trPushNotificationExpiryByDayT09.Start();
                if (AppSettings.IsStartT02)
                    trPushNotificationExpiryByUsageT02.Start();
                if (AppSettings.IsStartT24)
                    trPushNotificationExpiryByAmountT24.Start();
            }
            catch (Exception ex)
            {
                await log.Error(ex);
            }
        }

        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            log.Info("Windows Service is stopping from All Services", "Windows Service", true);
            KafkaNotificationConsumer.RunService = false;

            trKafKaBalanceNotificationReader.Abort();
            //trGooglePlanStatusPushNotification.Abort();

            trPushNotificationExpiryByDayT09.Abort();
            trPushNotificationExpiryByUsageT02.Abort();
            trPushNotificationExpiryByAmountT24.Abort();


            //StopThread(kafkaThreads);
            //StopThread(googlePushNotificationThreads);

            await Task.CompletedTask;
        }

        public Thread StartThread(ThreadStart threadStart)
        {
            var tr = new Thread(threadStart);
            tr.Start();
            return tr;
        }

        public List<Thread> StartThread(ThreadStart threadStart, int numberOfThread)
        {
            var threads = new List<Thread>();
            for (var i = 0; i < numberOfThread; i++) threads.Add(StartThread(threadStart));
            return threads;
        }

        public void StopThread(Thread thread)
        {
            thread.Abort();
        }

        public void StopThread(List<Thread> threads)
        {
            if (threads != null)
                foreach (var thread in threads)
                    StopThread(thread);
        }
    }
}