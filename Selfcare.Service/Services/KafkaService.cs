﻿using System.Threading;
using System.Threading.Tasks;
using Connector.Listener;
using LogService;
using Microsoft.Extensions.Hosting;

namespace Selfcare.Service.Services
{
    internal class KafkaService : BackgroundService
    {
        private Log log;

        public KafkaService()
        {
            log = new Log(string.Empty);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            //while (!stoppingToken.IsCancellationRequested)
            //{
            //    log.Info("Windows Service is running from My Service", "Windows Service", true);
            //    await Task.Delay(1000, stoppingToken);
            //}

            var kafkaNotificationConsumer = new KafkaNotificationConsumer();
            kafkaNotificationConsumer.MessageListener();
        }
    }
}