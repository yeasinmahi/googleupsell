﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using BusinessEntity.ConnectorModels.DBSS;
using BusinessEntity.Google.Notification;
using BusinessObject;
using Connector.Google;
using Connector.Services;
using LogService;
using static BusinessEntity.ConnectorModels.DBSS.AccountBalance;

namespace Selfcare.Service.Services
{
    public class PushNotificationService
    {
        protected Log log = new Log();
        public static int Counter = 1;
        public static bool RunService { get; set; } = true;
        public static double MbToByte = 1000000;
        protected readonly IHttpClientFactory _clientFactory;

        public PushNotificationService(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
            dbssAPI = new DBSSWebAPI(_clientFactory);
        }
        readonly DBSSWebAPI dbssAPI;
        public void SleepThread(int number)
        {
            if (number < AppSettings.NoOfRecordsPerBatch)
            {
                if (number == 0)
                {
                    int idleTime = AppSettings.SleepAtIdle * Counter;
                    if (idleTime > AppSettings.MaxSleepAtIdle)
                    {
                        idleTime = AppSettings.MaxSleepAtIdle;
                    }
                    else
                    {
                        Counter++;
                    }

                    Thread.Sleep(idleTime);
                }
                else
                {
                    Thread.Sleep(AppSettings.SleepAtIdle);
                    Counter = 1;
                }
            }
        }
        public async Task ListenerForExpiryByDayT09()
        {

            try
            {

                while (RunService)
                {
                    try
                    {
                        await ListenerForExpiryByUsageSingle("T09");
                    }
                    catch (Exception ex)
                    {
                        Task.Run(() => log.Error(ex));
                        Console.WriteLine($"Error occurred: {ex.Message}");
                    }
                }

            }
            catch (Exception ex)
            {
                Task.Run(() => log.Error(ex));
                Console.WriteLine($"Error occurred: {ex.Message}");
            }

        }

        public async Task ListenerForExpiryByUsageT02()
        {
            try
            {
                while (RunService)
                {
                    try
                    {
                        await ListenerForExpiryByUsageSingle("T02");
                    }
                    catch (Exception ex)
                    {
                        Task.Run(() => log.Error(ex));
                        Console.WriteLine($"Error occurred: {ex.Message}");
                    }
                }
            }
            catch (Exception ex)
            {
                Task.Run(() => log.Error(ex));
                Console.WriteLine($@"Error occurred: {ex.Message}");
            }
        }
        public async Task ListenerForExpiryByUsageSingle(string triggerId)
        {

            try
            {
                PushNotifications notification = await new BOPushNotification().GetPushNotificationServiceData(AppSettings.NoOfRecordsPerBatch, Guid.NewGuid().ToString(), triggerId, string.Empty);
                await ProcessNotification(notification);
                SleepThread(notification.Count);
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public async Task ListenerForExpiryByAmountT24()
        {
            try
            {
                while (RunService)
                {
                    try
                    {
                        await ListenerForExpiryByAmountT24Single();
                    }
                    catch (Exception ex)
                    {
                        Task.Run(() => log.Error(ex));
                        Console.WriteLine($"Error occurred: {ex.Message}");
                    }
                }

            }
            catch (Exception ex)
            {
                Task.Run(() => log.Error(ex));
                //Console.WriteLine($"Error occurred: {ex.Message}");
            }

        }
        public async Task ListenerForExpiryByAmountT24Single()
        {
            PushNotifications notification = await new BOPushNotification().GetPushNotificationServiceDataT24(AppSettings.NoOfRecordsPerBatch, Guid.NewGuid().ToString(), "T24", string.Empty);
            await ProcessNotification(notification);
            SleepThread(notification.Count);
        }

        //public async Task ListenerSingleExpiryByDayT09()
        //{
        //    Stopwatch watch = Stopwatch.StartNew();
        //    try
        //    {
        //        PushNotifications notification = new BOPushNotification().GetPushNotificationServiceData(10, Guid.NewGuid().ToString(), "T09", string.Empty).Result;
        //        await ProcessNotification(notification, watch);
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error(ex);
        //    }
        //    finally
        //    {
        //        watch.Stop();
        //    }
        //}

        //public async Task ListenerSingleExpiryByUsageT02()
        //{
        //    Stopwatch watch = Stopwatch.StartNew();
        //    try
        //    {
        //        PushNotifications notification = new BOPushNotification().GetPushNotificationServiceData(10, Guid.NewGuid().ToString(), "T02", string.Empty).Result;
        //        await ProcessNotification(notification);
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error(ex);
        //    }
        //    finally
        //    {
        //        watch.Stop();
        //    }
        //}

        //public async Task ListenerSingleExpiryByAmountT24()
        //{
        //    Stopwatch watch = Stopwatch.StartNew();
        //    try
        //    {
        //        PushNotifications notification = new BOPushNotification().GetPushNotificationServiceDataT24(10, Guid.NewGuid().ToString(), "T24", string.Empty).Result;
        //        await ProcessNotification(notification);
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error(ex);
        //    }
        //    finally
        //    {
        //        watch.Stop();
        //    }
        //}

        //public async Task SentPlanStatusNotificationToGoogle(PushNotification item)
        //{
        //    try
        //    {
        //        var finalURL = string.Format(AppSettings.GooglePushNotificationURL, item?.CPID);
        //        var data = await PreparePlanStatus(item);
        //        await new GoogleNotification().PostRequest(finalURL, JsonConvert.SerializeObject(data), new GoogleToken().AccessToken().Result);

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        //public void SentPlanStatusNotificationToGoogle(PushNotification item)
        //{
        //    try
        //    {
        //        try
        //        {
        //            var googleToken = new GoogleToken();
        //            googleToken.GetGoogleAccessToken();
        //            var
        //                googleAccessToken =
        //                    GoogleToken
        //                        .AccessToken; //"ya29.c.Ko8BxwesGEUP6lkzVeNaGu_6urXvRlpd_sRtXDjJ_7DQKs3NL6fqOS7DlgPYZAT8UddrRN4FsZFwNoNg-YCj5lSa__583BMEe8VN_oYQjjTiMx_D0UkXmn7vwIcyHJqpKq8PAO8GDfNNItc3OiRgOsRCsCvkDhzv2_ee1wT_HoL_Z8CpOh6dyVV5u858AO2ckoU";// GoogleToken.AccessToken;
        //            var httphelper = new HttpWebRequestHelper();
        //            var url = AppSettings.GooglePushNotificationURL;
        //            // CpidResponse cpid = new BOCpid().GetCpid(item.Msisdn, "EN", AppSettings.Provider);
        //            var finalURL = string.Format(url, item.CPID);
        //            // googleAccessToken

        //            var accountBalance = new AccountBalance();
        //            //BECurrentDataList currentDataList = new BECurrentDataList();
        //            var dbssAPI = new DBSSWebAPI();
        //            var data = new PlanStatus();
        //            data.LanguageCode = "en-US";
        //            data.ExpireTime = DateTimeOffset.UtcNow.AddDays(5);
        //            data.UpdateTime = DateTimeOffset.UtcNow.AddDays(-5);
        //            data.Title = "Prepaid Plan";
        //            data.Name = "Prepaid Plan";
        //            data.SubscriberId = item.Msisdn.Length == 10 ? "880" + item.Msisdn : item.Msisdn;
        //            data.uiCompatibility = UiCompatibility.UI_INCOMPATIBLE.ToString("F");
        //            //data.CpidState = CpidState.CPID_STATE_UNSPECIFIED.ToString("F");

        //            // data.PlanInfoPerClient = new PlanInfoPerClient(new Youtube(new RateLimitedStreaming(0)));
        //            data.Plans = new List<Plan>();

        //            try
        //            {
        //                accountBalance = dbssAPI.GetBalanceInformation(item.Msisdn, string.Empty, "PREP").Result;
        //            }
        //            catch (Exception ex)
        //            {
        //                throw ex;
        //            }

        //            var paymentType = accountBalance.PaymentType;

        //            if (paymentType != null && paymentType != "prepaid") data.Title = "Postpaid Plan";

        //            data.Name = data.Title;

        //            var counter = 0;
        //            var plans = new List<Plan>();

        //            var balanceLvl = BalanceLevel.BALANCE_LEVEL_UNSPECIFIED.ToString("F");
        //            var noType = NotificationType.NOTIFICATION_UNDEFINED.ToString("F");

        //            if (item.TRIGGER_ATTR_02.Contains("same") || item.TRIGGER_ATTR_02.Contains("Same"))
        //            {
        //                balanceLvl = BalanceLevel.OUT_OF_DATA.ToString("F");
        //                noType = NotificationType.NOTIFICATION_OUT_OF_DATA.ToString("F");
        //            }
        //            else if (item.TRIGGER_ATTR_02.Contains("early") || item.TRIGGER_ATTR_02.Contains("Early"))
        //            {
        //                balanceLvl = BalanceLevel.LOW_QUOTA.ToString("F");
        //                noType = NotificationType.NOTIFICATION_LOW_BALANCE_WARNING.ToString("F");
        //            }
        //            else if (item.TRIGGER_ATTR_02.Contains("80%"))
        //            {
        //                balanceLvl = BalanceLevel.LOW_QUOTA.ToString("F");
        //                noType = NotificationType.NOTIFICATION_LOW_BALANCE_WARNING.ToString("F");
        //            }
        //            else if (item.TRIGGER_ATTR_02.Contains("100%"))
        //            {
        //                balanceLvl = BalanceLevel.OUT_OF_DATA.ToString("F");
        //                noType = NotificationType.NOTIFICATION_OUT_OF_DATA.ToString("F");
        //            }
        //            else if (item.TRIGGER_ATTR_02.Contains("50MB") || item.TRIGGER_ATTR_02.Contains("50mb"))
        //            {
        //                balanceLvl = BalanceLevel.LOW_QUOTA.ToString("F");
        //                noType = NotificationType.NOTIFICATION_LOW_BALANCE_WARNING.ToString("F");
        //            }


        //            foreach (var itemSingle in accountBalance.DadicatedAccountList.Where(
        //                x => x.GsmServiceType == "data"))
        //            {
        //                counter++;

        //                var module = new PlanModule
        //                {
        //                    coarseBalanceLevel = balanceLvl,
        //                    TrafficCategories = new[] {PlanModuleTrafficCategory.GENERIC.ToString("F")},

        //                    ModuleName = itemSingle.ProductName,
        //                    Description = itemSingle.ProductName,
        //                    ExpirationTime = itemSingle.ExpieryDate.AddHours(6).ToUniversalTime(),
        //                    MaxRateKbps = "1200",

        //                    planModuleState = PlanState.ACTIVE.ToString("F"),

        //                    refreshPeriod = RefreshPeriod.DAILY.ToString("F"),

        //                    OverUsagePolicy = OverUsagePolicy.BLOCKED.ToString("F"),

        //                    byteBalance = new ByteQuota
        //                    {
        //                        quotaBytes = ((long) (itemSingle.TotalAmount * MbToByte)).ToString(),
        //                        remainingBytes = ((long) (itemSingle.RemainingAmount * MbToByte)).ToString()
        //                    },
        //                    //timeBalance = itemSingle.GsmServiceType.ToLower() == "voice" ? new TimeQuota
        //                    //{
        //                    //    quotaMinutes = (itemSingle.TotalAmount).ToString(),

        //                    //    remainingMinutes = (itemSingle.RemainingAmount).ToString()
        //                    //} : null,
        //                    UsedBytes = ((itemSingle.TotalAmount - itemSingle.RemainingAmount) * MbToByte).ToString()
        //                };
        //                var modules = new List<PlanModule>();
        //                modules.Add(module);
        //                var plan = new Plan
        //                {
        //                    PlanId =
        //                        itemSingle.ProductId == null ? counter.ToString() : itemSingle.ProductId.ToString(),
        //                    PlanName = itemSingle.ProductName,
        //                    planState = PlanState.ACTIVE.ToString("F"),
        //                    ExpirationTime = itemSingle.ExpieryDate.AddHours(6).ToUniversalTime(),
        //                    //planCategory = !string.IsNullOrWhiteSpace(item.ProductCode) ?"Purchased" : "Bonus"
        //                    PlanCategory = paymentType == "prepaid"
        //                        ? PlanCategory.PREPAID.ToString("F")
        //                        : PlanCategory.POSTPAID.ToString("F"),
        //                    PlanModules = modules
        //                };
        //                var isShow = false;
        //                if (plan.PlanModules != null && plan.PlanModules.Count() > 0)
        //                    isShow = plan.PlanModules.Where(x =>
        //                        x.byteBalance != null && x.byteBalance.remainingBytes != null &&
        //                        Convert.ToInt64(x.byteBalance.remainingBytes) > 0).Count() > 0;
        //                if (isShow)
        //                    plans.Add(plan);
        //            }

        //            data.Plans = plans;
        //            //data.notifications.Add(noType);
        //            //httphelper.HTTPPostRequest(finalURL,
        //            //    new JavaScriptSerializer().Serialize(data), item.Msisdn, googleAccessToken);
        //            GooglePostRequest(finalURL,
        //                JsonConvert.SerializeObject(data), item.Msisdn, googleAccessToken, 2);
        //        }
        //        catch (Exception)
        //        {
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}


        public async Task ProcessNotification(PushNotifications notification)
        {
            //foreach (var item in notification)
            //{
            //    Task.Run(() => ProcessNotification(item));
            //}

            var uploadTasks = new List<Task>();
            foreach (var item in notification)
            {
                Task obTask = Task.Run(() => ProcessNotification(item));
                uploadTasks.Add(obTask);

            }
            if (AppSettings.IsCompleteAllTaskPerBatchGoogleNotification)
            {
                await Task.WhenAll(uploadTasks);
            }

        }
        public async Task ProcessNotification(PushNotification notification)
        {
            Stopwatch watch = new Stopwatch();
            try
            {

                watch.Start();
                //await SentPlanStatusNotificationToGoogle(item);
                PlanStatus planStatus = await PreparePlanStatus(notification);
                await new GoogleNotification(_clientFactory).SentPlanStatusNotification(planStatus, notification?.CPID);
                watch.Stop();

                if (notification != null)
                    await new BOPushNotification().UpdateNotification(notification.KafkaId, 4, watch.ElapsedMilliseconds);
            }
            catch (Exception e)
            {
                watch.Stop();
                if (notification != null)
                    await new BOPushNotification().UpdateNotification(notification.KafkaId, 5, watch.ElapsedMilliseconds,
                        e.Message);
            }
        }

        public async Task<PushNotification> PreparePushNotificationAsync(string msisdn, string packCode, string notificationType)
        {
            var pushNotification = new PushNotification();
            try
            {
                string triggerId;
                switch (notificationType)
                {
                    case "OUT_OF_DATA":
                        triggerId = "T02";
                        pushNotification.TRIGGER_ATTR_02 = "100%";
                        pushNotification.TRIGGER_ATTR_01 = packCode;
                        break;
                    case "LOW_DATA":
                        triggerId = "T02";
                        pushNotification.TRIGGER_ATTR_02 = "80%";
                        pushNotification.TRIGGER_ATTR_01 = packCode;
                        break;
                    case "EXPIRE_SOON":
                        triggerId = "T09";
                        pushNotification.TRIGGER_ATTR_02 = "Same Day";
                        pushNotification.TRIGGER_ATTR_01 = await new BODataProduct().GetRefeilProfileIdByPackCode(packCode);
                        break;

                    case "50MB":
                        triggerId = "T24";
                        pushNotification.TRIGGER_ATTR_02 = "50MB";
                        pushNotification.TRIGGER_ATTR_01 = "Data";
                        break;
                    default:
                        triggerId = "T24";
                        pushNotification.TRIGGER_ATTR_02 = "50MB";
                        break;
                }

                pushNotification.KafkaId = triggerId + "_" + msisdn;
                pushNotification.TriggerId = triggerId;
                pushNotification.AggregateId = triggerId + "_" + msisdn;
                pushNotification.NotificationTime = DateTime.Now.ToOADate().ToString();
                pushNotification.IsProcessed = 1;
                pushNotification.Msisdn = msisdn;
                pushNotification.CPID = await new BOCpid().GetRegisteredCpid(msisdn);
                pushNotification.PackName = triggerId != "T24" ? "Test Pack" : "All Data";
                pushNotification.PackQuta = 1024;
                pushNotification.PackCode = packCode;
            }
            catch (Exception)
            {
                return null;
            }

            return pushNotification;
        }

        public async Task<PlanStatus> PreparePlanStatus(PushNotification item)
        {

            //data.Plans = await GetPlans(item);
            //data.ExpireTime = DateTimeOffset.UtcNow.AddDays(5);
            //data.UpdateTime = DateTimeOffset.UtcNow.AddDays(-5);
            //data.UiCompatibility = UiCompatibilityEnum.UI_INCOMPATIBLE.ToString("F");


            var plans = new List<Plan>();

            var plan = new Plan
            {
                PlanModules = await GetPlanModules(item),
                PlanName = string.IsNullOrWhiteSpace(item.PackName) ? "Data" : item.PackName
            };

            plans.Add(plan);

            var data = new PlanStatus();
            data.Plans = plans;


            return data;
        }

        public async Task<List<PlanModule>> GetPlanModules(PushNotification item)
        {
            if (item == null)
                throw new Exception("Invalid notification object");
            var balanceLvl = BalanceLevel.BALANCE_LEVEL_UNSPECIFIED.ToString("F");
            var planModuleState = PlanState.ACTIVE.ToString("F");
            var remainingData = item.PackQuta * MbToByte;
            DadicatedAccount dedicatedAccount = null;
            switch (item.TRIGGER_ATTR_02.ToLower())
            {
                case "same day":
                    remainingData = await GetRemainigDataAsByte(item.Msisdn, item.PackCode);
                    planModuleState = PlanState.EXPIRING_SOON.ToString("F");
                    break;
                case "early":
                    remainingData = await GetRemainigDataAsByte(item.Msisdn, item.PackCode);
                    planModuleState = PlanState.EXPIRING_SOON.ToString("F");
                    break;
                case "80%":
                    balanceLvl = BalanceLevel.LOW_QUOTA.ToString("F");
                    remainingData = await GetRemainigDataAsByte(item.Msisdn, item.PackCode); ;
                    break;
                case "100%":
                    balanceLvl = BalanceLevel.OUT_OF_DATA.ToString("F");
                    remainingData = 0;
                    break;
                case "50mb":
                    balanceLvl = BalanceLevel.LOW_QUOTA.ToString("F");
                    remainingData = 50 * MbToByte;
                    break;
            }


            var module = new PlanModule
            {
                coarseBalanceLevel = balanceLvl,
                ExpirationTime = DateTime.UtcNow.AddDays(1),
                planModuleState = planModuleState,
                byteBalance = new ByteQuota
                {
                    remainingBytes = ((long)remainingData).ToString()
                }
            };
            if (dedicatedAccount != null)
                module.ExpirationTime = dedicatedAccount.ExpieryDate.ToUniversalTime();
            var modules = new List<PlanModule>();
            modules.Add(module);
            return modules;
        }
        public async Task<double> GetRemainigDataAsByte(string msisdn, string packcode)
        {
            double remainingData = 0;
            AccountBalance accountBalance = await dbssAPI.GetBalanceInformation(msisdn.Substring(msisdn.Length - 10), string.Empty, "PREP");
            DadicatedAccount dedicatedAccount = accountBalance.DadicatedAccountList.FirstOrDefault(x => x.ProductCode == packcode);
            if (dedicatedAccount == null)
                throw new Exception("Product code not found");
            if (dedicatedAccount.RemainingAmount != null)
                remainingData = (double)(dedicatedAccount.RemainingAmount) * MbToByte;
            return remainingData;
        }
        //public async Task<List<Plan>> GetPlans(PushNotification item)
        //{
        //    var plans = new List<Plan>();

        //    var plan = new Plan
        //    {
        //        PlanModules = await GetPlanModules(item),
        //        PlanName = string.IsNullOrWhiteSpace(item.PackName) ? "All Data" : item.PackName
        //    };

        //    plans.Add(plan);
        //    return plans;
        //}
    }
}