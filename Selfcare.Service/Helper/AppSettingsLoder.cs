﻿using System;
using BusinessEntity.APIHub2;
using LogService;
using Microsoft.Extensions.Configuration;

namespace Selfcare.Service.Helper
{
    public class AppSettingsLoder
    {
        public static void LoadAppSettings(IConfiguration config)
        {
            var jwtConfiguration = config.GetSection("JwtConfiguration");
            var appSettings = config.GetSection("AppSettings");
            var kafkaSettings = config.GetSection("KafkaSettings");

            AppSettings.Audience = jwtConfiguration["Audience"];
            AppSettings.Scope = jwtConfiguration["Scope"];
            AppSettings.Issuer = jwtConfiguration["Issuer"];
            AppSettings.Sub = jwtConfiguration["Sub"];
            AppSettings.SecretKey = jwtConfiguration["SecretKey"];
            AppSettings.Alg = jwtConfiguration["Alg"];
            AppSettings.AuthenticationScheme = jwtConfiguration["AuthenticationScheme"];
            AppSettings.TokenType = jwtConfiguration["TokenType"];

            AppSettings.Topic = kafkaSettings["Topic"];
            AppSettings.GroupId = kafkaSettings["GroupId"];
            AppSettings.BootstrapServers = kafkaSettings["BootstrapServers"];

            AppSettings.NoOfKafkaThread = int.Parse(kafkaSettings["NoOfKafkaThread"]);
            AppSettings.NoOfGooglePushNotificationThread = int.Parse(kafkaSettings["NoOfGooglePushNotificationThread"]);
            AppSettings.GooglePushNotificationURL = appSettings["GooglePushNotificationURL"];

            AppSettings.DBBSApiBaseURL = appSettings["DBBSApiBaseURL"];
            AppSettings.EnableDemoAccess = Convert.ToBoolean(appSettings["EnableDemoAccess"]);

            AppSettings.NoOfRecordsPerBatch = int.Parse(appSettings["NoOfRecordsPerBatch"]);
            AppSettings.SleepAtIdle = int.Parse(appSettings["SleepAtIdle"]);
            AppSettings.MaxSleepAtIdle = int.Parse(appSettings["MaxSleepAtIdle"]);
            AppSettings.GoogleHttpRequestTimeOut = int.Parse(appSettings["GoogleHttpRequestTimeOut"]);
            AppSettings.DBSSHttpRequestTimeOut = int.Parse(appSettings["DBSSHttpRequestTimeOut"]);
            AppSettings.IsCompleteAllTaskPerBatchGoogleNotification = bool.Parse(appSettings["IsCompleteAllTaskPerBatchGoogleNotification"]);
            AppSettings.IsEarlyKafkaAutoOffsetReset = bool.Parse(appSettings["IsEarlyKafkaAutoOffsetReset"]);
            AppSettings.IsStartKafkaReader = bool.Parse(appSettings["IsStartKafkaReader"]);
            AppSettings.IsStartT02 = bool.Parse(appSettings["IsStartT02"]);
            AppSettings.IsStartT09 = bool.Parse(appSettings["IsStartT09"]);
            AppSettings.IsStartT24 = bool.Parse(appSettings["IsStartT24"]);
            AppSettings.GoogleNotificationTableName = appSettings["GoogleNotificationTableName"];


            AppSettings.IsLoadConfig = GetValue<bool>(appSettings, "IsLoadConfig");
            ConfigLog(appSettings);
        }
        public static void ConfigLog(IConfigurationSection section)
        {
            LogConfig.IsWriteErrorLogFile = GetValue<bool>(section, "IsWriteErrorLogFile");
            LogConfig.IsWriteErrorLogDb = GetValue<bool>(section, "IsWriteErrorLogDb");
            LogConfig.IsWriteRequestResponseFile = GetValue<bool>(section, "IsWriteRequestResponseFile");
            LogConfig.IsWriteRequestResponseDb = GetValue<bool>(section, "IsWriteRequestResponseDb");
            LogConfig.IsWriteRequestBodyDb = GetValue<bool>(section, "IsWriteRequestBodyDb");
            LogConfig.IsWriteRequestBodyDb = GetValue<bool>(section, "IsWriteRequestBodyDb");
            LogConfig.IsWriteTPSRequestLog = GetValue<bool>(section, "IsWriteTPSRequestLog");
            LogConfig.EnableWriteDBBSErrorLog = GetValue<bool>(section, "EnableWriteDBBSErrorLog");
            //LogConfig.IsWriteAuditTrailDb = GetValue<bool>(section, "IsWriteAuditTrailDb");
        }
        private static TConvertType GetValue<TConvertType>(IConfigurationSection section, string sectionName)
        {
            var value = section[sectionName];
            var converted = default(TConvertType);
            try
            {
                converted = (TConvertType)
                    Convert.ChangeType(value, typeof(TConvertType));
            }
            catch
            {
            }

            return converted;
        }
    }
}