using System;
using System.Net;
using System.Windows.Forms;
using BusinessEntity.APIHub2;
using BusinessObject;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Selfcare.Service.Forms;
using Selfcare.Service.Helper;
using Selfcare.Service.Services;

namespace Selfcare.Service
{
    internal static class Program
    {
        private static IServiceProvider ServiceProvider { get; set; }
        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main(string[] args)
        {
            IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .Build();

            AppSettingsLoder.LoadAppSettings(config);
            if (AppSettings.IsLoadConfig)
            {
                BOConfiguationData.LoadAllConfigurationData(false, true);
            }

            if (args.Length == 0)
            {

                //var googleToken = new GoogleToken(HttpClientFactory);

                //try
                //{
                //    googleToken.GetGoogleAccessToken().Wait();
                //}
                //catch { }

                CreateHostBuilder(args).Build().Run();

                //var kafkaNotificationConsumer = new KafkaNotificationConsumer();

                //kafkaNotificationConsumer.MessageListener();

                Console.WriteLine(@"Main End");
            }
            else
            {
                RunWindows();
                //CreateHostBuilder(args).Build().Run();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<AllServices>();
                    services.AddHttpClient("dbssHttpClientFactory", c =>
                    {
                        c.DefaultRequestHeaders.Add("Accept", "application/vnd.api+json");
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    });
                    services.AddHttpClient("cmsHttpClientFactory", c =>
                    {
                        c.DefaultRequestHeaders.Add("Accept", "application/json");
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    });
                    services.AddHttpClient("googleAuthHttpClientFactory", c =>
                    {
                        c.DefaultRequestHeaders.Add("Accept", "application/json");
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    });
                    services.AddHttpClient("googleMDPHttpClientFactory", c =>
                    {
                        c.DefaultRequestHeaders.Add("Accept", "application/json");
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    });
                    services.AddHttpClient("googleTokenHttpClientFactory", c =>
                    {
                        c.DefaultRequestHeaders.Add("Accept", "application/json");
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    });
                }).UseWindowsService();
        }
        public static void RunWindows()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Console.WriteLine(@"Configuration Start");
            ConfigureServices();
            Console.WriteLine(@"Configuration End");
            Application.Run(ServiceProvider.GetService<HomePageForm>());
        }
        private static void ConfigureServices()
        {
            var services = new ServiceCollection();
            services.AddHttpClient("dbssHttpClientFactory", c =>
            {
                c.DefaultRequestHeaders.Add("Accept", "application/vnd.api+json");
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            });
            services.AddHttpClient("cmsHttpClientFactory", c =>
            {
                c.DefaultRequestHeaders.Add("Accept", "application/json");
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            });
            services.AddHttpClient("googleAuthHttpClientFactory", c =>
            {
                c.DefaultRequestHeaders.Add("Accept", "application/json");
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            });
            services.AddHttpClient("googleMDPHttpClientFactory", c =>
            {
                c.DefaultRequestHeaders.Add("Accept", "application/json");
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            });
            services.AddHttpClient("googleTokenHttpClientFactory", c =>
            {
                c.DefaultRequestHeaders.Add("Accept", "application/json");
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            });
            services.AddSingleton<HomePageForm>();
            ServiceProvider = services.BuildServiceProvider();
        }

    }
}