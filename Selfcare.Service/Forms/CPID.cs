﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessEntity.OGate;
using BusinessObject;
using Newtonsoft.Json;
using Utility.Extensions;

namespace Selfcare.Service.Forms
{
    public partial class CPID : Form
    {
        public CPID()
        {
            InitializeComponent();
        }
        private async Task<List<string>> GetMSISDNs()
        {
            string[] msisdns = txtMSISDN.Text.Split(new[] {',',';',' '});

            List<string> msisdnList = new List<string>();
            string msisdn = string.Empty;
            foreach (string item in msisdns)
            {
                if (!(item.Length > 9 && item.Length < 14))
                    txtMSISDN.Text += $"{item} is not valid MSISDN" + Environment.NewLine;
                msisdn = item.MySubString(10, false);
                msisdnList.Add(msisdn);
            }
            return msisdnList;
        }
        private async void sendNotificationRb_CheckedChanged(object sender, System.EventArgs e)
        {
            BOCpid bOCpid = new BOCpid();
            List<string> msisdnList = await GetMSISDNs();
            CpidResponse cpid = null;
            List<KeyValuePair<string, string>> _dict = new List<KeyValuePair<string, string>>();

            foreach (var msisdn in msisdnList)
            {
                cpid = await bOCpid.GenerateCpid(msisdn, "EN", "WindowsForm", "WindowsForm", false);
                // txtCPID.Text = cpid.Cpid;
                _dict.Add(new KeyValuePair<string, string>( msisdn, cpid.Cpid));
            }
            lblRefreshAT.Text = DateTime.Now.ToString();

            rdoGenerateCPID.Checked = false;
            if(_dict.Count>0)
                txtCPID.Text = _dict[0].Value;

            dgCPID.DataSource = null;
            dgCPID.DataSource = _dict;
        }

        private async void txtCPID_TextChanged(object sender, EventArgs e)
        {
            BOCpid bOCpid = new BOCpid();
            try
            {
                CpidDecryptResponse cpidDecryptResponse = await bOCpid.GetDycrypt(txtCPID.Text);
               txtCPIDObj.Text = JsonConvert.SerializeObject(cpidDecryptResponse);
                lblRefreshAT.Text = DateTime.Now.ToString();
            }
            catch
            { }
        }
    }
}