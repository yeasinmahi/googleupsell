﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Forms;
using Connector.Google;

namespace Selfcare.Service.Forms
{
    public partial class GoogleTokenForm : Form
    {
        private IHttpClientFactory _httpClientFactory;
        public GoogleTokenForm(IHttpClientFactory httpClientFactory)
        {
            
            InitializeComponent();
            _httpClientFactory = httpClientFactory;
        }

        public async Task<string> GetToken()
        {
            var googleToken = new GoogleToken(_httpClientFactory);
            await googleToken.GetGoogleAccessToken();

            //return await new GoogleToken(_httpClientFactory).AccessToken();
            return await googleToken.AccessToken();
        }

        private async void GoogleTokenForm_Load(object sender, System.EventArgs e)
        {
            googleTokenRtb.Text = await GetToken();
        }
    }
}