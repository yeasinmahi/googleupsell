﻿namespace Selfcare.Service.Forms
{
    partial class KafkaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.msisdnTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.notificationTypeComb = new System.Windows.Forms.ComboBox();
            this.sendNotificationRb = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.insertIntoKafkaRb = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.cboPackCode = new System.Windows.Forms.ComboBox();
            this.insertIntoGoogleRb = new System.Windows.Forms.RadioButton();
            this.sendNotificationByServiceRb = new System.Windows.Forms.RadioButton();
            this.serviceTypeComb = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblSendOn = new System.Windows.Forms.Label();
            this.txtSummary = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.triggerTypeComb = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "MSISDN";
            // 
            // msisdnTxt
            // 
            this.msisdnTxt.Location = new System.Drawing.Point(126, 21);
            this.msisdnTxt.Name = "msisdnTxt";
            this.msisdnTxt.Size = new System.Drawing.Size(465, 23);
            this.msisdnTxt.TabIndex = 1;
            this.msisdnTxt.Text = "1926662002;1404850126";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Notification Type";
            // 
            // notificationTypeComb
            // 
            this.notificationTypeComb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.notificationTypeComb.FormattingEnabled = true;
            this.notificationTypeComb.Items.AddRange(new object[] {
            "OUT_OF_DATA",
            "LOW_DATA",
            "EXPIRE_SOON",
            "50MB"});
            this.notificationTypeComb.Location = new System.Drawing.Point(126, 79);
            this.notificationTypeComb.Name = "notificationTypeComb";
            this.notificationTypeComb.Size = new System.Drawing.Size(207, 23);
            this.notificationTypeComb.TabIndex = 3;
            // 
            // sendNotificationRb
            // 
            this.sendNotificationRb.AutoSize = true;
            this.sendNotificationRb.Location = new System.Drawing.Point(22, 168);
            this.sendNotificationRb.Name = "sendNotificationRb";
            this.sendNotificationRb.Size = new System.Drawing.Size(117, 19);
            this.sendNotificationRb.TabIndex = 4;
            this.sendNotificationRb.TabStop = true;
            this.sendNotificationRb.Text = "Send Notification";
            this.sendNotificationRb.UseVisualStyleBackColor = true;
            this.sendNotificationRb.CheckedChanged += new System.EventHandler(this.sendNotificationRb_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "Pack Code";
            // 
            // insertIntoKafkaRb
            // 
            this.insertIntoKafkaRb.AutoSize = true;
            this.insertIntoKafkaRb.Location = new System.Drawing.Point(156, 168);
            this.insertIntoKafkaRb.Name = "insertIntoKafkaRb";
            this.insertIntoKafkaRb.Size = new System.Drawing.Size(173, 19);
            this.insertIntoKafkaRb.TabIndex = 5;
            this.insertIntoKafkaRb.TabStop = true;
            this.insertIntoKafkaRb.Text = "Insert Into KafkaNotification";
            this.insertIntoKafkaRb.UseVisualStyleBackColor = true;
            this.insertIntoKafkaRb.CheckedChanged += new System.EventHandler(this.insertIntoKafkaRb_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "Time: ";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(65, 146);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(32, 15);
            this.lblTime.TabIndex = 0;
            this.lblTime.Text = "0 ms";
            // 
            // cboPackCode
            // 
            this.cboPackCode.FormattingEnabled = true;
            this.cboPackCode.Items.AddRange(new object[] {
            "OUT_OF_DATA",
            "LOW_DATA",
            "EXPIRE_SOON",
            "50MB"});
            this.cboPackCode.Location = new System.Drawing.Point(126, 50);
            this.cboPackCode.Name = "cboPackCode";
            this.cboPackCode.Size = new System.Drawing.Size(465, 23);
            this.cboPackCode.TabIndex = 3;
            this.cboPackCode.Text = "TK36EV1GB4D";
            // 
            // insertIntoGoogleRb
            // 
            this.insertIntoGoogleRb.AutoSize = true;
            this.insertIntoGoogleRb.Location = new System.Drawing.Point(348, 168);
            this.insertIntoGoogleRb.Name = "insertIntoGoogleRb";
            this.insertIntoGoogleRb.Size = new System.Drawing.Size(187, 19);
            this.insertIntoGoogleRb.TabIndex = 5;
            this.insertIntoGoogleRb.TabStop = true;
            this.insertIntoGoogleRb.Text = "Insert Into Google_Notification";
            this.insertIntoGoogleRb.UseVisualStyleBackColor = true;
            this.insertIntoGoogleRb.CheckedChanged += new System.EventHandler(this.insertIntoGoogleRb_CheckedChanged);
            // 
            // sendNotificationByServiceRb
            // 
            this.sendNotificationByServiceRb.AutoSize = true;
            this.sendNotificationByServiceRb.Location = new System.Drawing.Point(404, 204);
            this.sendNotificationByServiceRb.Name = "sendNotificationByServiceRb";
            this.sendNotificationByServiceRb.Size = new System.Drawing.Size(173, 19);
            this.sendNotificationByServiceRb.TabIndex = 4;
            this.sendNotificationByServiceRb.TabStop = true;
            this.sendNotificationByServiceRb.Text = "Send Notification by Service";
            this.sendNotificationByServiceRb.UseVisualStyleBackColor = true;
            this.sendNotificationByServiceRb.CheckedChanged += new System.EventHandler(this.sendNotificationByServiceRb_CheckedChanged);
            // 
            // serviceTypeComb
            // 
            this.serviceTypeComb.FormattingEnabled = true;
            this.serviceTypeComb.Items.AddRange(new object[] {
            "Single",
            "Continue"});
            this.serviceTypeComb.Location = new System.Drawing.Point(97, 204);
            this.serviceTypeComb.Name = "serviceTypeComb";
            this.serviceTypeComb.Size = new System.Drawing.Size(78, 23);
            this.serviceTypeComb.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 207);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 15);
            this.label5.TabIndex = 0;
            this.label5.Text = "Service Type";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 118);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 15);
            this.label6.TabIndex = 0;
            this.label6.Text = "Send Request on: ";
            // 
            // lblSendOn
            // 
            this.lblSendOn.AutoSize = true;
            this.lblSendOn.Location = new System.Drawing.Point(129, 118);
            this.lblSendOn.Name = "lblSendOn";
            this.lblSendOn.Size = new System.Drawing.Size(32, 15);
            this.lblSendOn.TabIndex = 0;
            this.lblSendOn.Text = "0 ms";
            // 
            // txtSummary
            // 
            this.txtSummary.Location = new System.Drawing.Point(22, 270);
            this.txtSummary.Multiline = true;
            this.txtSummary.Name = "txtSummary";
            this.txtSummary.Size = new System.Drawing.Size(569, 135);
            this.txtSummary.TabIndex = 1;
            this.txtSummary.Text = "1926662002;1404850126";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(209, 207);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 15);
            this.label7.TabIndex = 0;
            this.label7.Text = "Trigger Type";
            // 
            // triggerTypeComb
            // 
            this.triggerTypeComb.FormattingEnabled = true;
            this.triggerTypeComb.Items.AddRange(new object[] {
            "All",
            "T02",
            "T09",
            "T24"});
            this.triggerTypeComb.Location = new System.Drawing.Point(287, 204);
            this.triggerTypeComb.Name = "triggerTypeComb";
            this.triggerTypeComb.Size = new System.Drawing.Size(78, 23);
            this.triggerTypeComb.TabIndex = 3;
            // 
            // KafkaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(613, 431);
            this.Controls.Add(this.insertIntoGoogleRb);
            this.Controls.Add(this.insertIntoKafkaRb);
            this.Controls.Add(this.sendNotificationByServiceRb);
            this.Controls.Add(this.sendNotificationRb);
            this.Controls.Add(this.cboPackCode);
            this.Controls.Add(this.triggerTypeComb);
            this.Controls.Add(this.serviceTypeComb);
            this.Controls.Add(this.notificationTypeComb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtSummary);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.msisdnTxt);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblSendOn);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.label1);
            this.Name = "KafkaForm";
            this.Text = "Kafka";
            this.Load += new System.EventHandler(this.KafkaForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox msisdnTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox notificationTypeComb;
        private System.Windows.Forms.RadioButton insertIntoKafkaRb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton sendNotificationRb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.ComboBox cboPackCode;
        private System.Windows.Forms.RadioButton insertIntoGoogleRb;
        private System.Windows.Forms.RadioButton sendNotificationByServiceRb;
        private System.Windows.Forms.ComboBox serviceTypeComb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblSendOn;
        private System.Windows.Forms.TextBox txtSummary;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox triggerTypeComb;
    }
}