﻿namespace Selfcare.Service.Forms
{
    partial class GoogleTokenForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.googleTokenRtb = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // googleTokenRtb
            // 
            this.googleTokenRtb.Location = new System.Drawing.Point(12, 44);
            this.googleTokenRtb.Name = "googleTokenRtb";
            this.googleTokenRtb.Size = new System.Drawing.Size(710, 184);
            this.googleTokenRtb.TabIndex = 0;
            this.googleTokenRtb.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Google Access Token";
            // 
            // GoogleTokenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 441);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.googleTokenRtb);
            this.Name = "GoogleTokenForm";
            this.Text = "GoogleToken";
            this.Load += new System.EventHandler(this.GoogleTokenForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox googleTokenRtb;
        private System.Windows.Forms.Label label1;
    }
}