﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Selfcare.Service.Forms
{
    public partial class HomePageForm : Form
    {
        private IHttpClientFactory _httpClientFactory;
        public HomePageForm(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
            InitializeComponent();

        }
        private async void HomePageForm_Load(object sender, EventArgs e)
        {
            kafkaToolStripMenuItem.Click += KafkaToolStripMenuItem_Click;
            GoogleTokenToolStripMenuItem.Click += GoogleTokenToolStripMenuItem_Click;
            mnuCPID.Click += mnuCPID_Click;
        }

        private async void GoogleTokenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //new GoogleTokenForm().Show();
            var googleTokenForm = new GoogleTokenForm(_httpClientFactory);
            googleTokenForm.MdiParent = this;
            googleTokenForm.WindowState = FormWindowState.Maximized;
            googleTokenForm.Show();
        }

        private async void KafkaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var kafkaForm = new KafkaForm(_httpClientFactory);
            kafkaForm.MdiParent = this;

            kafkaForm.WindowState = FormWindowState.Maximized;
            kafkaForm.Show();
        }private void mnuCPID_Click(object sender, EventArgs e)
        {
            //new KafkaForm().Show();
            var kafkaForm = new CPID();
            kafkaForm.MdiParent = this;

            kafkaForm.WindowState = FormWindowState.Maximized;
            kafkaForm.Show();
        }
    }
}