﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows.Forms;
using BusinessEntity.APIHub2;
using BusinessEntity.Google.Notification;
using BusinessObject;
using Connector.Google;
using Selfcare.Service.Services;
using Utility.Extensions;

namespace Selfcare.Service.Forms
{
    public partial class KafkaForm : Form
    {
        private IHttpClientFactory _httpClientFactory;
        PushNotificationService pushNotificationService;
        public KafkaForm(IHttpClientFactory httpClientFactory)
        {
            
            InitializeComponent();
            _httpClientFactory = httpClientFactory;
            pushNotificationService = new PushNotificationService(_httpClientFactory);
        }
        private void KafkaForm_Load(object sender, EventArgs e)
        {
            txtSummary.Text = string.Empty;
            BEDataProducts dataProducts = new BEDataProducts();

            foreach (var item in BOConfiguationData.DataProducts)
            {
                if (!string.IsNullOrEmpty(item.DataCode))

                    dataProducts.Add(new BEDataProduct()
                    { DataCode = item.DataCode, DataProductName = $"{item.DataCode}[{item.DataProductName}]" });
                if (!string.IsNullOrEmpty(item.AutoRenewCode))

                    dataProducts.Add(new BEDataProduct()
                    { DataCode = item.AutoRenewCode, DataProductName = $"{item.AutoRenewCode}[{item.DataProductName}]-AutoRenew" });
            }


            cboPackCode.DisplayMember = "DataProductName";
            cboPackCode.ValueMember = "DataCode";
            cboPackCode.DataSource = dataProducts;




            notificationTypeComb.SelectedIndex = 0;
            serviceTypeComb.SelectedIndex = 1;
            //notificationTypeComb.Items.Clear();
            //notificationTypeComb.Items.Add(new 

            //            OUT_OF_DATA
            //LOW_DATA
            //EXPIRE_SOON
            //50MB
        }

        private async void insertIntoKafkaRb_CheckedChanged(object sender, EventArgs e)
        {
            if (!insertIntoKafkaRb.Checked) return;
            await InsertIntoDb("kafkaNotification");
            insertIntoKafkaRb.Checked = false;
        }

        private async void insertIntoGoogleRb_CheckedChanged(object sender, EventArgs e)
        {
            if (!insertIntoGoogleRb.Checked) return;
            await InsertIntoDb("google_notification");
            insertIntoGoogleRb.Checked = false;
        }


        private async Task<List<string>> GetMSISDNs()
        {
            string[] msisdns = msisdnTxt.Text.Split(new[] { ',', ';', ' ' });

            List<string> msisdnList = new List<string>();
            string msisdn = string.Empty;
            foreach (string item in msisdns)
            {
                if (!(item.Length > 9 && item.Length < 14))
                    txtSummary.Text += $"{item} is not valid MSISDN" + Environment.NewLine;
                msisdn = item.MySubString(10, false);
                msisdnList.Add(msisdn);
            }
            return msisdnList;
        }
        public async Task InsertIntoDb(string type)
        {
            txtSummary.Text = string.Empty;
            Stopwatch watch = Stopwatch.StartNew();

            List<string> msisdnList = await GetMSISDNs();
            foreach (var msisdn in msisdnList)
            {


                var packcode = cboPackCode.SelectedValue?.ToString();
                if (string.IsNullOrEmpty(packcode))
                    packcode = cboPackCode.Text?.ToUpper();
                var notificationType = notificationTypeComb.SelectedItem.ToString();

                var pushNotification = await pushNotificationService.PreparePushNotificationAsync(msisdn, packcode, notificationType);

                var boPushNotification = new BOPushNotification();
                try
                {
                    if (type == "google_notification")
                    {

                        await boPushNotification.SaveGoogle(pushNotification);
                    }
                    else if (type == "kafkaNotification")
                    {

                       await  boPushNotification.SaveKafka(pushNotification);
                    }
                    else
                    {
                        txtSummary.Text += $"{msisdn}: Wrong table type" + Environment.NewLine;
                        //MessageBox.Show("Wrong table type");
                    }
                    lblTime.Text = $"{watch.ElapsedMilliseconds}  ms";
                    lblSendOn.Text = DateTime.Now.ToString("HH:mm:ss tt");
                    //MessageBox.Show("Successfully Inserted.");
                    txtSummary.Text += $"{msisdn}: Successfully Inserted." + Environment.NewLine;
                }
                catch (Exception ex)
                {
                    lblTime.Text = $"{watch.ElapsedMilliseconds}  ms";
                    //MessageBox.Show(ex.Message);
                    txtSummary.Text += $"{msisdn}: {ex.Message}" + Environment.NewLine;
                }
            }

        }
        private async void sendNotificationRb_CheckedChanged(object sender, EventArgs e)
        {
            txtSummary.Text = string.Empty;
            Stopwatch watch = Stopwatch.StartNew();

            if (!sendNotificationRb.Checked) return;
       
            List<string> msisdnList = await GetMSISDNs();
            foreach (var msisdn in msisdnList)
            {

                var packcode = cboPackCode.SelectedValue?.ToString();
                if (string.IsNullOrEmpty(packcode))
                    packcode = cboPackCode.Text?.ToUpper();

                var notificationType = notificationTypeComb.SelectedItem.ToString();


                try
                {
                    var pushNotification = await pushNotificationService.PreparePushNotificationAsync(msisdn, packcode, notificationType);
                    //pushNotificationService.SentPlanStatusNotificationToGoogle(pushNotification).Wait();
                    PlanStatus planStatus = await pushNotificationService.PreparePlanStatus(pushNotification);
                    //await SentPlanStatusNotificationToGoogle(item);
                    await new GoogleNotification(_httpClientFactory).SentPlanStatusNotification(planStatus, pushNotification?.CPID);
                    watch.Stop();
                    lblTime.Text = $"{watch.ElapsedMilliseconds}  ms";
                    //MessageBox.Show("Successfully Send Notification.");
                    txtSummary.Text += $"{msisdn}: Successfully Send Notification." + Environment.NewLine;
                }
                catch (Exception ex)
                {
                    watch.Stop();
                    lblTime.Text = $"{watch.ElapsedMilliseconds}  ms";
                    //MessageBox.Show(ex.Message);
                    txtSummary.Text += $"{msisdn}: {ex.Message}" + Environment.NewLine;
                }

            }
            sendNotificationRb.Checked = false;
        }

        private async void sendNotificationByServiceRb_CheckedChanged(object sender, EventArgs e)
        {
            if (!sendNotificationByServiceRb.Checked) return;
            var serviceType = serviceTypeComb.SelectedItem.ToString();
            var triggerType = triggerTypeComb.SelectedItem.ToString();
            if (serviceType == "Single")
            {
                if (triggerType == "T24")
                {
                    Task.Run(() => pushNotificationService.ListenerForExpiryByAmountT24Single());
                } else if (triggerType == "T02")
                {
                    Task.Run(() => pushNotificationService.ListenerForExpiryByUsageSingle("T02"));
                } else if (triggerType == "T09")
                {
                    Task.Run(() => pushNotificationService.ListenerForExpiryByUsageSingle("T09"));
                }
                else
                {
                    Task.Run(() => pushNotificationService.ListenerForExpiryByAmountT24Single());
                    Task.Run(() => pushNotificationService.ListenerForExpiryByUsageSingle("T02"));
                    Task.Run(() => pushNotificationService.ListenerForExpiryByUsageSingle("T09"));
                }
                
            }
            else if (serviceType == "Continue")
            {
                if (triggerType == "T24")
                {
                    Task.Run(() => pushNotificationService.ListenerForExpiryByAmountT24());
                }
                else if (triggerType == "T02")
                {
                    Task.Run(() => pushNotificationService.ListenerForExpiryByUsageT02());
                }
                else if (triggerType == "T09")
                {
                    Task.Run(() => pushNotificationService.ListenerForExpiryByDayT09());
                }
                else
                {
                    Task.Run(() => pushNotificationService.ListenerForExpiryByAmountT24());
                    Task.Run(() => pushNotificationService.ListenerForExpiryByUsageT02());
                    Task.Run(() => pushNotificationService.ListenerForExpiryByDayT09());
                }
            }

            sendNotificationByServiceRb.Checked = false;
        }
    }
}