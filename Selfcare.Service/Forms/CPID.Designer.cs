﻿namespace Selfcare.Service.Forms
{
    partial class CPID
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCPID = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rdoGenerateCPID = new System.Windows.Forms.RadioButton();
            this.txtMSISDN = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblRefreshAT = new System.Windows.Forms.Label();
            this.txtCPIDObj = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dgCPID = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgCPID)).BeginInit();
            this.SuspendLayout();
            // 
            // txtCPID
            // 
            this.txtCPID.Location = new System.Drawing.Point(12, 98);
            this.txtCPID.Name = "txtCPID";
            this.txtCPID.Size = new System.Drawing.Size(264, 101);
            this.txtCPID.TabIndex = 0;
            this.txtCPID.Text = "";
            this.txtCPID.TextChanged += new System.EventHandler(this.txtCPID_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "CPID";
            // 
            // rdoGenerateCPID
            // 
            this.rdoGenerateCPID.AutoSize = true;
            this.rdoGenerateCPID.Location = new System.Drawing.Point(81, 73);
            this.rdoGenerateCPID.Name = "rdoGenerateCPID";
            this.rdoGenerateCPID.Size = new System.Drawing.Size(193, 19);
            this.rdoGenerateCPID.TabIndex = 4;
            this.rdoGenerateCPID.TabStop = true;
            this.rdoGenerateCPID.Text = "Generate CPID (Not Store in DB)";
            this.rdoGenerateCPID.UseVisualStyleBackColor = true;
            this.rdoGenerateCPID.CheckedChanged += new System.EventHandler(this.sendNotificationRb_CheckedChanged);
            // 
            // txtMSISDN
            // 
            this.txtMSISDN.Location = new System.Drawing.Point(81, 18);
            this.txtMSISDN.Multiline = true;
            this.txtMSISDN.Name = "txtMSISDN";
            this.txtMSISDN.Size = new System.Drawing.Size(641, 49);
            this.txtMSISDN.TabIndex = 1;
            this.txtMSISDN.Text = "1926662002";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "MSISDN";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 334);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Refresh At";
            // 
            // lblRefreshAT
            // 
            this.lblRefreshAT.AutoSize = true;
            this.lblRefreshAT.Location = new System.Drawing.Point(79, 334);
            this.lblRefreshAT.Name = "lblRefreshAT";
            this.lblRefreshAT.Size = new System.Drawing.Size(50, 15);
            this.lblRefreshAT.TabIndex = 0;
            this.lblRefreshAT.Text = "MSISDN";
            // 
            // txtCPIDObj
            // 
            this.txtCPIDObj.Location = new System.Drawing.Point(12, 220);
            this.txtCPIDObj.Name = "txtCPIDObj";
            this.txtCPIDObj.Size = new System.Drawing.Size(264, 101);
            this.txtCPIDObj.TabIndex = 0;
            this.txtCPIDObj.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 202);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 15);
            this.label4.TabIndex = 1;
            this.label4.Text = "CPID";
            // 
            // dgCPID
            // 
            this.dgCPID.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCPID.Location = new System.Drawing.Point(308, 98);
            this.dgCPID.Name = "dgCPID";
            this.dgCPID.Size = new System.Drawing.Size(414, 356);
            this.dgCPID.TabIndex = 5;
            this.dgCPID.Text = "dataGridView1";
            // 
            // CPID
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 441);
            this.Controls.Add(this.dgCPID);
            this.Controls.Add(this.lblRefreshAT);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtMSISDN);
            this.Controls.Add(this.rdoGenerateCPID);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCPIDObj);
            this.Controls.Add(this.txtCPID);
            this.Name = "CPID";
            this.Text = "CP ID";
            ((System.ComponentModel.ISupportInitialize)(this.dgCPID)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox txtCPID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rdoGenerateCPID;
        private System.Windows.Forms.TextBox txtMSISDN;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblRefreshAT;
        private System.Windows.Forms.RichTextBox txtCPIDObj;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dgCPID;
    }
}