﻿namespace Selfcare.Service.Forms
{
    partial class HomePageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.formToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kafkaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.GoogleTokenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCPID = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.formToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(784, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip";
            // 
            // formToolStripMenuItem
            // 
            this.formToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kafkaToolStripMenuItem,
            this.GoogleTokenToolStripMenuItem,
            this.mnuCPID});
            this.formToolStripMenuItem.Name = "formToolStripMenuItem";
            this.formToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.formToolStripMenuItem.Text = "Form";
            // 
            // kafkaToolStripMenuItem
            // 
            this.kafkaToolStripMenuItem.Name = "kafkaToolStripMenuItem";
            this.kafkaToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.kafkaToolStripMenuItem.Text = "Kafka";
            // 
            // GoogleTokenToolStripMenuItem
            // 
            this.GoogleTokenToolStripMenuItem.Name = "GoogleTokenToolStripMenuItem";
            this.GoogleTokenToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.GoogleTokenToolStripMenuItem.Text = "Google Token";
            // 
            // mnuCPID
            // 
            this.mnuCPID.Name = "mnuCPID";
            this.mnuCPID.Size = new System.Drawing.Size(147, 22);
            this.mnuCPID.Text = "CP ID";
            // 
            // HomePageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.menuStrip);
            this.IsMdiContainer = true;
            this.Name = "HomePageForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HomePageForm";
            this.Load += new System.EventHandler(this.HomePageForm_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem formToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kafkaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem GoogleTokenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnuCPID;
    }
}