﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace APIHub2.Helper
{
    public class CustomAuthorizeAttribute : TypeFilterAttribute
    {
        public CustomAuthorizeAttribute() : base(typeof(CustomAuthorizeFilter))
        {
            Arguments = new object[] { };
        }

        public CustomAuthorizeAttribute(string claimType, string claimValue) : base(typeof(CustomAuthorizeFilter))
        {
            Arguments = new object[] {new Claim(claimType, claimValue)};
        }
    }

    public class CustomAuthorizeFilter : IAsyncAuthorizationFilter
    {
        //public AuthorizationPolicy Policy { get; }

        //public CustomAuthorizeFilter(AuthorizationPolicy policy)
        //{
        //    Policy = policy ?? throw new ArgumentNullException(nameof(policy));
        //}

        private readonly Claim _claim;

        private readonly IConfiguration _configuration;
        private readonly List<string> _roles;

        public CustomAuthorizeFilter(IConfiguration configuration, Claim claim = null)
        {
            _configuration = configuration;
            _claim = claim;
            var roles = claim.Value.Split(',');

            if (roles == null || roles.Length == 0)
                _roles = new List<string>();
            else
                _roles = roles.ToList();
        }

        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            var message = new BEMessageData<string>();
            var strErrorContent = string.Empty;
            var errorStatusCode = StatusCodes.Status401Unauthorized;

            try
            {
                //// Allow Anonymous skips all authorization
                //if (context.Filters.Any(item => item is IAllowAnonymousFilter))
                //{
                //    return;
                //}

                // if (context.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true)
                //|| context.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true))
                // {
                //     return;
                // }

                if (context == null) throw new ArgumentNullException(nameof(context));

                if (string.IsNullOrEmpty(context.HttpContext.User.Identity.Name))
                {
                    string token = context.HttpContext.Request.Headers["Authorization"];
                    //token = token.Replace("Bearer ", "");

                    token = token?.Split(" ")[1];
                    var signingKey = _configuration["JwtConfiguration:SecretKey"];

                    var claimsPrincipal = new JwtTokenValidator().TokenValidator(token, signingKey);

                    context.HttpContext.User = claimsPrincipal;
                }

                errorStatusCode = StatusCodes.Status200OK;

                if (_claim != null)
                {
                    // var hasClaim = context.HttpContext.User.Claims.Any(c => c.Type == _claim.Type && c.Value == _claim.Value);

                    var myList = context.HttpContext.User.Claims.Where(item =>
                            item.Type == ClaimTypes.Role && _roles.ToList().Contains(item.Value))
                        .Count();


                    // if (!hasClaim)
                    if (myList != _roles.Count())
                    {
                        //throw new Exception
                        errorStatusCode = StatusCodes.Status401Unauthorized;
                        message.Status = BEMessageCodes.InvalidRole.Status;
                        message.Error = BEMessageCodes.InvalidRole.error;

                        //actionContext.Response = new HttpResponseMessage
                        //{
                        //    StatusCode = StatusCodes.Status401Unauthorized,
                        //    //Content = new StringContent("You are not authorized")

                        strErrorContent = JsonConvert.SerializeObject(message);

                        //context.HttpContext.Response.StatusCode = StatusCodes.Status401Unauthorized;
                        //await context.HttpContext.Response.WriteAsync(strErrorContent);
                    }
                }
                //var policyEvaluator = context.HttpContext.RequestServices.GetRequiredService<IPolicyEvaluator>();
                //var authenticateResult = await policyEvaluator.AuthenticateAsync(Policy, context.HttpContext);
                //var authorizeResult = await policyEvaluator.AuthorizeAsync(Policy, authenticateResult, context.HttpContext, context);

                //if (authorizeResult.Challenged)
                //{
                //    // Return custom 401 result
                //    context.Result = new CustomUnauthorizedResult("Authorization failed.");
                //}
                //else if (authorizeResult.Forbidden)
                //{
                //    // Return default 403 result
                //    context.Result = new ForbidResult(Policy.AuthenticationSchemes.ToArray());
                //}

                //      context.Result = new ForbidResult();
            }
            catch (SecurityTokenExpiredException)
            {
                errorStatusCode = StatusCodes.Status401Unauthorized;


                //message.Status = BEMessageCodes.TokenExpired.;
                //message.error = MessageCodes.TokenExpired.Message;
                message.Status = BEMessageCodes.TokenExpired.Status;
                message.Error = BEMessageCodes.TokenExpired.error;

                strErrorContent = JsonConvert.SerializeObject(message);
            }
            catch (SecurityTokenValidationException)
            {
                errorStatusCode = StatusCodes.Status401Unauthorized;


                //message.Status = BEMessageCodes.InvalidToken.;
                //message.error = MessageCodes.InvalidToken.Message;
                message.Status = BEMessageCodes.InvalidRole.Status;
                message.Error = BEMessageCodes.InvalidRole.error;

                strErrorContent = JsonConvert.SerializeObject(message);
            }
            catch (Exception)
            {
                // context.Result = new CustomUnauthorizedResult("Authorization failed.");

                errorStatusCode = StatusCodes.Status401Unauthorized;


                message.Status = errorStatusCode;
                message.Error = "You are not authorized";
                strErrorContent = JsonConvert.SerializeObject(message);
            }

            if (errorStatusCode != StatusCodes.Status200OK)
            {
                context.HttpContext.Response.StatusCode = errorStatusCode;
                //context.HttpContext.Response. = errorStatusCode;
                await context.HttpContext.Response.WriteAsync(strErrorContent);
            }
        }
    }

    public class CustomUnauthorizedResult : JsonResult
    {
        public CustomUnauthorizedResult() : base(new CustomError())
        {
            StatusCode = StatusCodes.Status401Unauthorized;
        }
    }

    public class CustomError : BEMessageData<string>
    {
        // public string Error { get; }

        public CustomError()
        {
            var message = new BEMessageData<string>();


            message.Status = StatusCodes.Status401Unauthorized;
            message.Error = "You are not authorized";

            //actionContext.Response = new HttpResponseMessage
            //{
            //    StatusCode = StatusCodes.Status401Unauthorized,
            //    //Content = new StringContent("You are not authorized")

            //    Content = new ObjectContent(typeof(BusinessEntity.BEMessageData<string>),
            //    message, System.Web.Http.GlobalConfiguration.Configuration.Formatters.JsonFormatter)
            //};
        }
    }
}