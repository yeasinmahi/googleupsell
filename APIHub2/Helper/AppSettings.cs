﻿//using LogService;
//using Microsoft.Extensions.Configuration;
//using System;

//namespace APIHub2.Helper
//{
//    public class AppSettings
//    {
//        public static int OTPLength { get; private set; }
//        public static int RecordsPerPage { get; private set; }
//        public static string Provider { get; private set; }
//        public static string CMSApiBaseURL { get; private set; }
//        public static string DBBSApiBaseURL { get; private set; }
//        public static string RequestLogPath { get; private set; }
//        public static string DBBSErrorLogPath { get; private set; }
//        public static bool EnableDemoAccess { get; private set; }
//        public static int DBSSHttpRequestTimeOut { get; private set; }
//        public static bool IsLoadConfig { get; private set; }
//        public static string LanguageCode { get; private set; }


//        //Log Setting
//        public static bool IsWriteErrorLogFile { get; private set; }
//        public static bool IsWriteErrorLogDb { get; private set; }
//        public static bool IsWriteRequestResponseFile { get; private set; }
//        public static bool IsWriteRequestResponseDb { get; private set; }
//        public static bool IsWriteAuditTrailDb { get; private set; }

//        public static void Load(IConfiguration configuration)
//        {
//            IConfigurationSection section = configuration.GetSection("AppSettings");
//            OTPLength = GetValue<int>(section, "OTPLength");
//            RecordsPerPage = GetValue<int>(section, "RecordsPerPage");
//            Provider = GetValue<string>(section, "Provider");
//            CMSApiBaseURL = GetValue<string>(section, "CMSApiBaseURL");
//            DBBSApiBaseURL = GetValue<string>(section, "DBBSApiBaseURL");
//            RequestLogPath = GetValue<string>(section, "RequestLogPath");
//            DBBSErrorLogPath = GetValue<string>(section, "DBBSErrorLogPath");
//            EnableDemoAccess = GetValue<bool>(section, "EnableDemoAccess");
//            DBSSHttpRequestTimeOut = GetValue<int>(section, "DBSSHttpRequestTimeOut");
//            IsLoadConfig = GetValue<bool>(section, "IsLoadConfig");
//            LanguageCode = GetValue<string>(section, "LanguageCode");


//            //Log Setting
//            IsWriteErrorLogFile = bool.Parse(section["IsWriteErrorLogFile"]);
//            IsWriteErrorLogDb = bool.Parse(section["IsWriteErrorLogDb"]);
//            IsWriteRequestResponseFile = bool.Parse(section["IsWriteRequestResponseFile"]);
//            IsWriteRequestResponseDb = bool.Parse(section["IsWriteRequestResponseDb"]);
//            IsWriteAuditTrailDb = bool.Parse(section["IsWriteAuditTrailDb"]);


//            ConfigLog();
//        }
//        private static void ConfigLog()
//        {
//            LogConfig.IsWriteErrorLogFile = IsWriteErrorLogFile;
//            LogConfig.IsWriteErrorLogDb = IsWriteErrorLogDb;
//            LogConfig.IsWriteRequestResponseFile = IsWriteRequestResponseFile;
//            LogConfig.IsWriteRequestResponseDb = IsWriteRequestResponseDb;
//            LogConfig.IsWriteAuditTrailDb = IsWriteAuditTrailDb;

//        }
//        private static TConvertType GetValue<TConvertType>(IConfigurationSection section, string sectionName)
//        {
//            string value = section[sectionName];
//            var converted = default(TConvertType);
//            try
//            {
//                converted = (TConvertType)
//                    Convert.ChangeType(value, typeof(TConvertType));
//            }
//            catch
//            {

//            }
//            return converted;
//        }

//    }
//}

