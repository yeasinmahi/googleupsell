﻿using BusinessEntity.APIHub2;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;

namespace APIHub2.Helper
{
    public class CustomBadRequest : ValidationProblemDetails

    {
        public CustomBadRequest(ActionContext context)

        {
            Title = "Invalid arguments to the API11111111111";

            Detail = "The inputs supplied to the API are invalid2222222222222222";

            Status = 400;

            ConstructErrorMessages(context);

            Type = context.HttpContext.TraceIdentifier;
        }

        private void ConstructErrorMessages(ActionContext context)

        {
            if (!context.HttpContext.Response.HasStarted)
            {
                var message = new BEMessageData<string>();
                foreach (var keyModelStatePair in context.ModelState)

                {
                    var key = keyModelStatePair.Key;

                    var errors = keyModelStatePair.Value.Errors;

                    if (errors != null && errors.Count > 0)

                    {
                        if (errors.Count == 1)

                        {
                            var errorMessage = GetErrorMessage(errors[0]);

                            Errors.Add(key, new[] {errorMessage});
                            message.Errors.Add(key, new[] {errorMessage});
                        }

                        else

                        {
                            var errorMessages = new string[errors.Count];

                            for (var i = 0; i < errors.Count; i++) errorMessages[i] = GetErrorMessage(errors[i]);

                            Errors.Add(key, errorMessages);
                            message.Errors.Add(key, errorMessages);
                        }
                    }
                }

                var strErrorContent = string.Empty;


                message.Status = BEMessageCodes.BadRequest.Status;
                message.Error = BEMessageCodes.BadRequest.error;

                strErrorContent = JsonConvert.SerializeObject(message);
                context.HttpContext.Response.StatusCode = BEMessageCodes.BadRequest.Status;
                context.HttpContext.Response.WriteAsync(strErrorContent);
            }
        }

        private string GetErrorMessage(ModelError error)

        {
            return string.IsNullOrEmpty(error.ErrorMessage) ? "The input was not valid." : error.ErrorMessage;
        }
    }
}