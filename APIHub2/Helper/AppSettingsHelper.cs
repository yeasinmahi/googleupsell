﻿using System;
using BusinessEntity.APIHub2;
using LogService;
using Microsoft.Extensions.Configuration;

namespace APIHub2.Helper
{
    public class AppSettingsHelper
    {
        public AppSettingsHelper(IConfiguration configuration)
        {
            var section = configuration.GetSection("AppSettings");

            AppSettings.OTPLength = GetValue<int>(section, "OTPLength");
            AppSettings.RecordsPerPage = GetValue<int>(section, "RecordsPerPage");
            AppSettings.Provider = GetValue<string>(section, "Provider");
            AppSettings.ServiceName = GetValue<string>(section, "ServiceName");

            AppSettings.CMSApiBaseURL = GetValue<string>(section, "CMSApiBaseURL");
            AppSettings.DBBSApiBaseURL = GetValue<string>(section, "DBBSApiBaseURL");
            AppSettings.RequestLogPath = GetValue<string>(section, "RequestLogPath");
            AppSettings.DBBSErrorLogPath = GetValue<string>(section, "DBBSErrorLogPath");
            AppSettings.EnableDemoAccess = GetValue<bool>(section, "EnableDemoAccess");
            AppSettings.DBSSHttpRequestTimeOut = GetValue<int>(section, "DBSSHttpRequestTimeOut");
            AppSettings.CMSHttpRequestTimeOut = GetValue<int>(section, "CMSHttpRequestTimeOut");
            AppSettings.IsLoadConfig = GetValue<bool>(section, "IsLoadConfig");
            AppSettings.LanguageCode = GetValue<string>(section, "LanguageCode");

            AppSettings.OTPSMS = GetValue<string>(section, "OTPSMS");
            AppSettings.SMSsender = GetValue<string>(section, "SMSsender");
            AppSettings.OTPLength = GetValue<int>(section, "OTPLength");
            AppSettings.OTPIntervalTime = GetValue<int>(section, "OTPIntervalTime");
            AppSettings.MaximumOTPInADay = GetValue<int>(section, "MaximumOTPInADay");

            AppSettings.temporaryLockWrongAttemptLimit = GetValue<string>(section, "temporaryLockWrongAttemptLimit");
            AppSettings.temporaryLockRemovalTimeMin = GetValue<string>(section, "temporaryLockRemovalTimeMin");


            AppSettings.OTPValidityDuration = GetValue<int>(section, "OTPValidityDuration");
            AppSettings.OTPWrongAttempLimit = GetValue<int>(section, "OTPWrongAttempLimit");

            AppSettings.TotalRequestSummary = GetValue<int>(section, "TotalRequestSummary");
            AppSettings.ErrorLogFilePath = GetValue<string>(section, "ErrorLogFilePath");

            //Log Configuration
            ConfigLog(section);
        }

        public void ConfigLog(IConfigurationSection section)
        {
            LogConfig.IsWriteErrorLogFile = GetValue<bool>(section, "IsWriteErrorLogFile");
            LogConfig.IsWriteErrorLogDb = GetValue<bool>(section, "IsWriteErrorLogDb");
            LogConfig.IsWriteRequestResponseFile = GetValue<bool>(section, "IsWriteRequestResponseFile");
            LogConfig.IsWriteRequestResponseDb = GetValue<bool>(section, "IsWriteRequestResponseDb");
            LogConfig.IsWriteRequestBodyDb = GetValue<bool>(section, "IsWriteRequestBodyDb");
            LogConfig.IsWriteRequestBodyDb = GetValue<bool>(section, "IsWriteRequestBodyDb");
            LogConfig.IsWriteTPSRequestLog = GetValue<bool>(section, "IsWriteTPSRequestLog");
            LogConfig.EnableWriteDBBSErrorLog = GetValue<bool>(section, "EnableWriteDBBSErrorLog");
            //LogConfig.IsWriteAuditTrailDb = GetValue<bool>(section, "IsWriteAuditTrailDb");
        }

        private TConvertType GetValue<TConvertType>(IConfigurationSection section, string sectionName)
        {
            var value = section[sectionName];
            var converted = default(TConvertType);
            try
            {
                converted = (TConvertType)
                    Convert.ChangeType(value, typeof(TConvertType));
            }
            catch
            {
            }

            return converted;
        }
    }
}