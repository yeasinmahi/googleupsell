﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace APIHub2.Helper
{
    public class JwtTokenValidator
    {
        //For Central
        public void TokenValidator(IServiceCollection services, IConfiguration configuration)
        {
            var validationParameters = new TokenValidationParameters
            {
                ValidAudience = configuration["JwtConfiguration:Audience"],
                ValidIssuer = configuration["JwtConfiguration:Issuer"],
                ValidateAudience = bool.Parse(configuration["JwtConfiguration:IsValidateAudience"]),
                ValidateIssuer = bool.Parse(configuration["JwtConfiguration:IsValidateIssuer"]),
                ValidateIssuerSigningKey = bool.Parse(configuration["JwtConfiguration:IsValidateIssuerSigningKey"]),
                ValidateLifetime = bool.Parse(configuration["JwtConfiguration:IsValidateLifetime"]),
                IssuerSigningKey =
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JwtConfiguration:SecretKey"]))
            };

            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options => { options.TokenValidationParameters = validationParameters; });
        }

        public ClaimsPrincipal TokenValidator(string token, string jwtSigningKey
        )
        {
            try
            {
                //if (string.IsNullOrEmpty(token))
                //    return null;
                var tokenHandler = new JwtSecurityTokenHandler();
                //JwtSecurityToken jwtToken = (JwtSecurityToken)tokenHandler.ReadToken(token);
                //if (jwtToken == null)
                //    return null;
                var parameters = new TokenValidationParameters
                {
                    ValidIssuer = "http://api.auth.com",
                    ValidAudience = "http://auth.com",

                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    LifetimeValidator = LifetimeValidator,
                    ValidateIssuerSigningKey = true,

                    // IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSigningKey))
                    // IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("auth#api@lDotNetCoreServer!10#82"))
                    IssuerSigningKey = new SymmetricSecurityKey(
                        Encoding.UTF8.GetBytes(jwtSigningKey))
                };
                var principal = tokenHandler.ValidateToken(token,
                    parameters, out var securityToken);
                return principal;
            }
            //catch (SecurityTokenExpiredException e)
            //{
            //  //  return default;
            //    throw e;

            //}
            //catch (SecurityTokenException e)
            //{
            //    //  return default;
            //    throw e;

            //}
            catch (Exception e)
            {
                //  return default;
                throw e;
            }
        }

        private bool LifetimeValidator(DateTime? notBefore, DateTime? expires, SecurityToken token,
            TokenValidationParameters @params)
        {
            if (expires != null) return expires > DateTime.Now;
            return false;
        }
    }
}