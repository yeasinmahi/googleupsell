﻿using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

//using System.Web.Http;

namespace APIHub2.Helper
{
    public class ClaimRequirementAttribute : TypeFilterAttribute
    {
        public ClaimRequirementAttribute() : base(typeof(ClaimRequirementFilterAsync))
        {
            Arguments = new object[] { };
        }

        public ClaimRequirementAttribute(string claimType, string claimValue) : base(
            typeof(ClaimRequirementFilterAsync))
        {
            Arguments = new object[] {new Claim(claimType, claimValue)};
        }
    }

    //public class ClaimRequirementFilter : IAuthorizationFilter
    //{
    //    readonly Claim _claim;

    //    public ClaimRequirementFilter(Claim claim)
    //    {
    //        _claim = claim;
    //    }

    //    public void OnAuthorization(AuthorizationFilterContext context)
    //    {
    //        if (context.HttpContext.User == null)
    //        {
    //            context.Result = new UnauthorizedResult();
    //        }
    //        else
    //        { 
    //        var hasClaim = context.HttpContext.User.Claims.Any(c => c.Type == _claim.Type && c.Value == _claim.Value);
    //            if (!hasClaim)
    //            {
    //                // context.Result = new ForbidResult();


    //                BusinessEntity.BEMessageData<string> message =
    //                new BusinessEntity.BEMessageData<string>();


    //                message.Status = Convert.ToString((int)HttpStatusCode.Unauthorized);
    //                message.error = "You are not authorized";

    //                //var httpResponseMessage = new HttpResponseMessage
    //                //{
    //                //    StatusCode = HttpStatusCode.Unauthorized,
    //                //    //Content = new StringContent("You are not authorized")

    //                //    Content = new ObjectContent(typeof(BusinessEntity.BEMessageData<string>),
    //                //message, GlobalConfiguration.Configuration.Formatters.JsonFormatter)
    //                //};

    //                  context.Result =  new ForbidResult();


    //               // context.HttpContext.Response = httpResponseMessage;

    //            }
    //        }
    //    }
    //}

    public class ClaimRequirementFilterAsync : IAsyncAuthorizationFilter
    {
        private readonly Claim _claim;

        public ClaimRequirementFilterAsync(Claim claim)
        {
            _claim = claim;
        }

        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            if (context.HttpContext.User == null)
            {
                context.Result = new UnauthorizedResult();
            }
            else
            {
                var hasClaim =context.HttpContext.User.Claims.Any(c => c.Type == _claim.Type && c.Value == _claim.Value);
                if (!hasClaim)
                {
                    // context.Result = new ForbidResult();

                    var message = new BEMessageData<string>();

                    message.Status = (int) HttpStatusCode.Unauthorized;
                    message.Error = "You are not authorized";

                    //var httpResponseMessage = new HttpResponseMessage
                    //{
                    //    StatusCode = HttpStatusCode.Unauthorized,
                    //    //Content = new StringContent("You are not authorized")

                    //    Content = new ObjectContent(typeof(BusinessEntity.BEMessageData<string>),
                    //message, GlobalConfiguration.Configuration.Formatters.JsonFormatter)
                    //};

                    context.Result = new ForbidResult();


                    // context.HttpContext.Response = httpResponseMessage;
                }
            }
        }
    }
}