﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using BusinessEntity.ConnectorModels.CMS;
using Connector.Services;

namespace APIHub2.APIHandler
{
    public class CMSAPIHandler
    {
        private readonly IHttpClientFactory clientFactory;
        public CMSAPIHandler(IHttpClientFactory clientFactory)
        {
            this.clientFactory = clientFactory;
        }
        public async Task<List<MyOfferListResponse>> GetMyOfferList(BEServiceUser serviceUserInfo, string MSISDN,
            bool showBalanceWiseProduct, bool showLoanProduct)
        {
            var responseList = new List<MyOfferListResponse>();

            try
            {
                double pPalance = 0;
                var isEligibleForLoan = true;
                var prepaidPostpaidType = 1;
                
                var dbssAPI = new DBSSWebAPI(clientFactory);

                var accountBalance = await dbssAPI.GetDBSSBalanceInformation(MSISDN, string.Empty, "PREP", true);


                if (accountBalance == null)
                    throw new BeHandledException(BEMessageCodes.InvalidCustomer.Status,
                        BEMessageCodes.InvalidCustomer.error, "Customer balance not found !!!");
                if (string.IsNullOrEmpty(accountBalance.SubscriptionId)
                    || string.IsNullOrEmpty(accountBalance.PaymentType))
                    throw new BeHandledException(BEMessageCodes.InvalidCustomer.Status,
                        BEMessageCodes.InvalidCustomer.error, "Invalid customer!!!");
                if (accountBalance.PaymentType == "prepaid")
                {
                    prepaidPostpaidType = 1;
                    pPalance = accountBalance.MainAccountBalance;
                }
                else
                {
                    prepaidPostpaidType = 2;
                    pPalance = 10000;
                }

                var requestObject = new MyOfferListRequest();
                requestObject.msisdn = Convert.ToInt64(MSISDN);
                //  requestObject.language = "EN";
                requestObject.channelID = serviceUserInfo.CMSChanel;
                requestObject.salesChannelID = serviceUserInfo.CMSSalesChannel;
                requestObject.serviceTypeID = prepaidPostpaidType;
                if (showBalanceWiseProduct)
                    requestObject.balance = pPalance;

                var cmsAPI = new CMSApi(clientFactory);

                responseList = await cmsAPI.GetMyOfferList(requestObject, clientFactory);
                //List<MyOfferListResponse> offers = null;
                if (responseList == null || responseList.Count == 0)
                    // throw new Exception("CMS Error or No offer available for this MSISDN");

                    throw new BeHandledException(BEMessageCodes.CMSResponseError.Status, BEMessageCodes.CMSResponseError.error,
                        "CMS Error||" + "No Response form CMS");
                if (responseList.Count == 1
                    && (!string.IsNullOrEmpty(responseList.FirstOrDefault().ErrorMessage)
                        || responseList.FirstOrDefault().ErrorCode > 0))
                    // throw new Exception("CMS Error or No offer available for this MSISDN");

                    throw new BeHandledException(BEMessageCodes.InternaError.Status, BEMessageCodes.InternaError.error,
                        "CMS Error||" + requestObject.serviceTypeID + "||" +
                        responseList.FirstOrDefault().ErrorMessage);


                if (pPalance <= 20)
                {
                    responseList = responseList.Where(x =>
                        x.offerLongDescription != null && x.offerLongDescription.Contains("Loan")).ToList();

                    if (accountBalance.DadicatedAccountList != null && accountBalance.DadicatedAccountList.Count > 0
                                                                    && accountBalance.DadicatedAccountList.Count > 0)
                        isEligibleForLoan = !(accountBalance.DadicatedAccountList
                            .Where(x => x.DadicatedAccountId == 245 && x.RemainingAmount > 0)
                            .Count() > 0);

                    if (!(showLoanProduct && isEligibleForLoan))
                        responseList.Clear();
                }
                else
                {
                    //    responseList = responseList.Where(x => x.offerPrice <= pPalance
                    //   && x.offerLongDescription != null && !x.offerLongDescription.Contains("Loan")).ToList<MyOfferListResponse>();

                    responseList = responseList.Where(x => !x.offerLongDescription.Contains("Loan")).ToList();

                    if (showBalanceWiseProduct)
                        responseList = responseList.Where(x => x.offerPrice <= pPalance
                                                               && x.offerLongDescription != null).ToList();
                }
            }
            catch (BeHandledException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //WriteLog("M:");
            //   dataList.offers = offerList;
            return responseList;
        }

        public async Task<MyOfferPurchaseResponse> PurchaseMyOffer(BEServiceUser serviceUserInfo, string MSISDN,
            string planId)
        {
            var myOfferListResponse = new List<MyOfferListResponse>();
            var requestObject = new MyOfferPurchaseRequest();
            var responsefromAPI = new MyOfferPurchaseResponse();
            var cmsAPI = new CMSApi(clientFactory);
            var offerCount = 0;
            var isOfferExist = false;

            try
            {
                requestObject.msisdn = Convert.ToInt32(MSISDN);
                requestObject.offerID = planId;
                requestObject.language = "EN";
                requestObject.channelID = Convert.ToInt32(serviceUserInfo.CMSChanel);
                requestObject.salesChannelID = Convert.ToInt32(serviceUserInfo.CMSSalesChannel);
                requestObject.actionCall = 1;


                var amarOfferList = new MyOfferListRequest();
                amarOfferList.msisdn = requestObject.msisdn;
                //amarOfferList.language = requestObject.language;
                amarOfferList.channelID = requestObject.channelID;
                amarOfferList.salesChannelID = requestObject.salesChannelID;
                amarOfferList.serviceTypeID = 1;
                if (serviceUserInfo.UserID == 31) //31-FacebookAPIHub
                {
                    var offerList = await cmsAPI.GetMyOfferList(amarOfferList,clientFactory);
                    if (offerList != null)
                    {
                        offerCount = offerList.Count();
                        isOfferExist = offerList.Where(x => x.offerID == requestObject.offerID).Count() > 0;
                    }
                }

                //isOfferExist = offerList.Where(x => x.offerID == requestObject.offerID).Count() > 0;
                responsefromAPI = await cmsAPI.PurchaseMyOffer(requestObject,clientFactory);
            }
            catch (BeHandledException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //WriteLog("M:");
            //   dataList.offers = offerList;
            return responsefromAPI;
        }
    }
}