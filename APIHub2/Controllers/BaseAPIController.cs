﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using BusinessEntity.APIHub2;
using BusinessEntity.RequestResponseModels;
using BusinessObject;
using Connector.Services;
using LogService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APIHub2.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{v:apiVersion}/[controller]")]
    public class BaseAPIController : ControllerBase
    {
        protected readonly IHttpContextAccessor _accessor;
        protected readonly IHttpClientFactory _clientFactory;
        protected readonly IMapper _mapper;
        protected readonly Stopwatch watch = Stopwatch.StartNew();

        private IEnumerable<Claim> _claims;
        private string action = string.Empty;
        private string actualErrorMessage = string.Empty;
        private string controller = string.Empty;
        protected Log log;
        protected BEServiceUser ServiceUser = new BEServiceUser();


        public BaseAPIController(IHttpContextAccessor accessor, IMapper mapper, IHttpClientFactory clientFactory)
        {
            _accessor = accessor;
            _mapper = mapper;
            _clientFactory = clientFactory;
            log = new Log();
        }

        protected string ActualErrorMessage
        {
            get => actualErrorMessage;
            set => actualErrorMessage = $"{actualErrorMessage}|{value}";
        }


        protected IEnumerable<Claim> Claims
        {
            get
            {
                var context = _accessor.HttpContext;

                if (_claims == null)
                    _claims = context?.User?.Claims?.ToList();
                return _claims;
            }
            private set => _claims = value;
        }
        // private  BEServiceUser serviceUserInfo;
        //protected static Dictionary<string, BEServiceUser> ServiceUserInfo = new Dictionary<string, BEServiceUser>();


        protected void CheckPermission(BaseRequest baseRequest)
        {
            ServiceUser = BOConfiguationData.GetServiceUserByClientID(User.Identity.Name);
            if (ServiceUser == null || ServiceUser.UserID == 0)
                throw new BeHandledException(BEMessageCodes.UnauthorizedError.Status,
                    BEMessageCodes.UnauthorizedError.error);

            action = ControllerContext.RouteData.Values["action"].ToString();
            controller = ControllerContext.RouteData.Values["controller"].ToString();

            if (baseRequest != null)
                CheckCustomer(baseRequest);

            //string key =$"{HttpContext.Request.Host}/{controller}/{action}/{HttpContext.Request.Method}";
            //string key = $"{AppSettings.ServiceName}/{controller}/{action}/{HttpContext.Request.Method}";
            if (!ServiceUser.APIPermissions.ContainsKey(
                $"{AppSettings.ServiceName}/{controller}/{action}/{_accessor.HttpContext.Request.Method}"))
                throw new BeHandledException(BEMessageCodes.UnauthorizedError.Status,
                    BEMessageCodes.UnauthorizedError.error);
            // Check API permission
            //if (API not permitted)
            // throw new BEHandledException(BEMessageCodes.UnauthorizedError.Status, BEMessageCodes.UnauthorizedError.error);
        }

        private void CheckCustomer(BaseRequest baseRequest)
        {
            if (string.IsNullOrEmpty(baseRequest.MSISDN) && string.IsNullOrEmpty(baseRequest.SubscriptionID))
                throw new BeHandledException(BEMessageCodes.BadRequest.Status, BEMessageCodes.BadRequest.error,
                    "Please provide MSISDN or subscriber ID");
            // log.MSISDN = $"M:{baseRequest.MSISDN};S:{baseRequest.SubscriptionID}";

            if (string.IsNullOrEmpty(baseRequest.SubscriptionID))
                log.Msisdn = baseRequest.MSISDN;
            else
                log.Msisdn = baseRequest.SubscriptionID;

            // throw new BEHandledException(BEMessageCodes.BadRequest.Status, BEMessageCodes.BadRequest.error, "Please enter MSISDN");
        }

        //static BaseAPIController()
        //{
        //    LoadServiceUserInfo();

        //}
        //private static void LoadServiceUserInfo()
        //{
        //    BOServiceUserInfo boServiceUserInfo = new BOServiceUserInfo();

        //    BEServiceUserInfos serviceUserInfos = boServiceUserInfo.GetServiceUserInfos();
        //    foreach (BEServiceUserInfo item in serviceUserInfos)
        //    {
        //        ServiceUserInfo.Add(item.UserName, item);
        //    }

        //}

        //protected void WriteLog(string MSISDN, params object[] paramiters)
        //{

        //    try
        //    {

        //        string sourceIP = string.Empty;

        //        //int i = 1;

        //        //ActionExecutingContext filterContext

        //        //var identity = (ClaimsPrincipal)Thread.CurrentPrincipal;

        //        //var claims = ClaimsPrincipal.Current.Identities.First().Claims.ToList();


        //        var dpa = Claims?.FirstOrDefault(c => c.Type == ClaimTypes.Role && c.Value == "dpa")?.Value;
        //        //var userID = Claims?.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value;
        //        var customerID = Claims?.FirstOrDefault(c => c.Type == "CustomerID")?.Value;


        //        //var isAccessTokenByCustomer = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role && c.Value == "CustomerToken")?.Value;

        //        //           var isAccessTokenByCustomer = identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role && c.Value == "CustomerToken")?.Value;

        //        //   var userID = HttpContext.User.Claims.FirstOrDefault(c => c. == "Name")?.Name; 
        //        var Channel = User.Identities.FirstOrDefault(c => c.Name == "Channel")?.Name;
        //        //var CustomerID = identity.Claims.FirstOrDefault(c => c.Type == "CustomerID")?.Value;
        //        //var CustomerEmail = identity.Claims.FirstOrDefault(c => c.Type == "CustomerEmail")?.Value;
        //        //var CustomerEmail1 = identity.Claims.FirstOrDefault(c => c.Type == "CustomerEmail1")?.Value;

        //        //string asdf = ControllerContext.ActionDescriptor.ActionName;
        //        //string asdf1 = ControllerContext.ActionDescriptor.ControllerName;


        //        BEAccessLog accessLog = new BEAccessLog();
        //        //accessLog.AdminUserID = string.Empty;
        //        accessLog.BrowseURL = ControllerContext.ActionDescriptor.ControllerName;
        //        accessLog.MethodName = ControllerContext.ActionDescriptor.ActionName;
        //        //accessLog.RootPath = this.ControllerContext.ActionDescriptor.RouteValues.ToString();
        //        accessLog.QueryString = HttpContext.Request.QueryString.Value;
        //        accessLog.IPAddress = HttpContext.Connection.RemoteIpAddress.ToString();
        //        accessLog.HttpMethod = HttpContext.Request.Method;
        //        //accessLog.APIVersion = this.ControllerContext.ActionDescriptor.ve;
        //        accessLog.ServiceUserID = ServiceUser.UserID;
        //        //accessLog.SessionID = Session.SessionID;
        //        accessLog.MSISDN = MSISDN;
        //        ////accessLog.Version = version;
        //        //accessLog.BrowserInfoDeviceID = this.HttpContext.Request.;

        //        //BusinessObject.BOAccessLog bOAccessLog = new BusinessObject.BOAccessLog();
        //        //BusinessObject.BOAccessLog.Save(accessLog);

        //    }
        //    catch (Exception ex)
        //    {
        //        int i = 0;
        //    }

        //}
        //protected string errorMessage;


        protected async Task<bool> IsValidCustomerStatus(string msisdn)
        {
            var result = false;
            var dbssAPI = new DBSSWebAPI(_clientFactory);
            var subscription = await dbssAPI.GetSubscriptions(msisdn, "subscription-type");
            if (subscription.Datas.Count > 0)
            {
                
                //var subscriber = subscription.Datas.First();
                if (subscription.Datas.FindAll(c => c.Attributes.Status.ToLower() == "active").Count > 0) result = true;
            }

            return result;
        }


        protected async Task<IActionResult> WriteAuditTrail(int statusCode, object obj, params object[] paramiters)
        {
            watch.Stop();
            try
            {
                var version = ControllerContext.RouteData.Values["v"].ToString();
                //int.TryParse(User.Identity.Name, out int serviceUserID);
                var context = _accessor.HttpContext;
                int.TryParse(context.User.Identity.Name, out var serviceUserID);
                var dpa = Claims?.FirstOrDefault(c => c.Type == ClaimTypes.Role && c.Value == "dpa")?.Value;
                //var userID = Claims?.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value;
                //var customerID = Claims?.FirstOrDefault(c => c.Type == "CustomerID")?.Value;

                await log.AuditTrail(await new LogHelper().BuildAuditTrailtMetadata(context, log.Msisdn,
                    action, controller, version, ServiceUser, (int) watch.ElapsedMilliseconds, actualErrorMessage,
                    paramiters));

                await new BORequestSummary().Add($"{AppSettings.Provider}.{controller}.{action}",
                    (int) watch.ElapsedMilliseconds, statusCode != 200);


                switch (statusCode)
                {
                    case StatusCodes.Status200OK:
                        return Ok(obj);
                    case StatusCodes.Status401Unauthorized:
                        return Unauthorized(obj);
                    case StatusCodes.Status400BadRequest:
                        return BadRequest(obj);
                    case int n when n >= 5000 && n < 7000:
                        //return StatusCode(statusCode, obj);
                        return UnprocessableEntity(obj);
                    //case StatusCodes.Status500InternalServerError:
                    //    return notok(obj);

                    default:
                        return StatusCode(statusCode,obj);
                }
            }
            catch (Exception ex)
            {
                ActualErrorMessage = ex.Message;
                await log.Error(ex, watch.ElapsedMilliseconds, await new LogHelper().ReadRequestBody(Request));
                return Ok(obj);
            }
        }

        //protected async Task<IActionResult> HttpResponse(int statusCode, object obj)
        //{
        //    switch (statusCode)
        //    {
        //        case StatusCodes.Status200OK:
        //            return Ok(obj);
        //        case StatusCodes.Status401Unauthorized:
        //            return Unauthorized(obj);

        //        default:
        //            return Ok(obj);
        //    }

        //}
    }
}