﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using APIHub2.APIHandler;
using APIHub2.Helper;
using AutoMapper;
using BusinessEntity.APIHub2;
using BusinessEntity.ConnectorModels.CMS;
using BusinessEntity.RequestResponseModels;
using Connector.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APIHub2.Controllers
{
    [ApiController]
    // [CustomAuthorize(ClaimTypes.Role, "CustomerToken")]\
    [CustomAuthorize(ClaimTypes.Role, "dpa,BasicData")]
    public class CMSController : BaseAPIController
    {
        //protected static Dictionary<string, BEServiceUser> ServiceUserInfo = new Dictionary<string, BEServiceUser>();
        public CMSController(IHttpContextAccessor accessor, IMapper mapper, IHttpClientFactory clientFactory) : base(accessor, mapper,clientFactory)

        {
        }

        [HttpGet]
        [Route("GetMyOfferList")]
        //public async Task<IActionResult> GetMyOfferList(
        //    [StringLength(10, MinimumLength = 10, ErrorMessage = "MSISDN must be 10 char")] string MSISDN,
        //   // [StringLength(10, MinimumLength = 10, ErrorMessage = "MSISDN must be 10 char")] CustomerTypeEnum CustomerType,
        //    bool showBalanceWiseProduct=true, bool showLoanProduct=true)
        public async Task<IActionResult> GetMyOfferList([FromQuery] OfferList offerListRequest)

        {
            //Stopwatch watch = //Stopwatch.StartNew();
            var messageData = new BEMessageData<List<BEMyOffer>>();
            //messageData.Provider = AppSettings.Provider;
            //BEMyOffers dataList = new BEMyOffers();

            //List<BEMyOffer> offerList = new List<BEMyOffer>();
            //DateTime startTime = DateTime.Now;
            //string channel = string.Empty;
            //string ActualErrorMessage = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(offerListRequest.MSISDN))
                    ModelState.AddModelError("MSISDN", "Please provide MSISDN");

                CheckPermission(offerListRequest);

                if (!ModelState.IsValid) messageData.AddModelErrorMessages(ModelState);
                // BEMyOffer offer = _mapper.Map<BEMyOffer>(offerListRequest);

                var requestObject = new MyOfferListRequest();
                var serviceUserID = User.Identity.Name;
                //BEServiceUser serviceUserInfo = BOConfiguationData.ServiceUserInfo[serviceUserID];
                var dbssAPI = new DBSSWebAPI(_clientFactory);
                var cmsConnectorAPI = new CMSAPIHandler(_clientFactory);
                var responseList = await cmsConnectorAPI.GetMyOfferList(ServiceUser, offerListRequest.MSISDN,
                    offerListRequest.ShowBalanceWiseProduct, offerListRequest.ShowLoanProduct);


                if (!(responseList == null || responseList.Count == 0))
                {
                    messageData.Data = new List<BEMyOffer>();
                    foreach (var item in responseList)
                    {
                        var beMyOffer = new BEMyOffer(Convert.ToInt64(item.offerID),
                            item.offerType, item.offerName,
                            Convert.ToInt32(item.offerRank), item.offerDescription,
                            item.offerCategoryName, string.Empty, item.offerPrice);
                        messageData.Data.Add(beMyOffer);
                    }
                }
                else
                {
                    ActualErrorMessage = "No offer after fltering";
                }
            }

            catch (BeHandledException ex)
            {
                //BEDefaultResponse exceptionMessage = new CustomException().GetExceptionMessage(ex);
                ActualErrorMessage = ex.GetMessage;
                messageData.MapBEHandledException(ex);
                //await log.Error(ex);
            }
            catch (Exception ex)
            {
                ActualErrorMessage = ex.Message;
                messageData.MapException(ex, "Data can not be load due to technical error");
                await log.Error(ex);
            }

            //WriteLog("M:");
            //messageData.Data = offerList;
            return await WriteAuditTrail(messageData.Status, messageData);
            // return Json(dataList);
        }


        //[HttpPost]
        //[Route("GetMyOfferList1")]
        //public async Task<IActionResult> GetMyOfferList1([FromBody] BEGetOfferRequest getOfferRequest)
        //{
        //    //Stopwatch watch = //Stopwatch.StartNew();
        //    BEMessageData<List<BEMyOffer>> messageData = new BEMessageData<List<BEMyOffer>>();
        //    //messageData.Provider = AppSettings.Provider;
        //    //BEMyOffers dataList = new BEMyOffers();

        //    List<BEMyOffer> offerList = new List<BEMyOffer>();
        //    DateTime startTime = DateTime.Now;
        //    string channel = string.Empty;
        //    //string ActualErrorMessage = string.Empty;

        //    try
        //    {
        //        CheckPermission();

        //        if (getOfferRequest.MSISDN == "1926662002")
        //            Thread.Sleep(30000);

        //        MyOfferListRequest requestObject = new MyOfferListRequest();
        //        string serviceUserID = User.Identity.Name;
        //        BEServiceUser serviceUserInfo = BOConfiguationData.ServiceUserInfo[serviceUserID];
        //        DBSSWebAPI dbssAPI = new DBSSWebAPI();
        //        CMSConnectorAPI cmsConnectorAPI = new CMSConnectorAPI();
        //        List<MyOfferListResponse> responseList = await cmsConnectorAPI.GetMyOfferList(serviceUserInfo, getOfferRequest.MSISDN,
        //            getOfferRequest.showBalanceWiseProduct, getOfferRequest.showLoanProduct);


        //        if (!(responseList == null || responseList.Count == 0))
        //        {
        //            foreach (MyOfferListResponse item in responseList)
        //            {
        //                BEMyOffer beMyOffer = new BEMyOffer(Convert.ToInt64(item.offerID),
        //                          item.offerType, item.offerName,
        //                          Convert.ToInt32(item.offerRank), item.offerDescription,
        //                          item.offerCategoryName, string.Empty, item.offerPrice);
        //                offerList.Add(beMyOffer);
        //            }
        //        }
        //        else
        //        {
        //            ActualErrorMessage = "No offer after fltering";
        //        }
        //    }

        //    catch (BEHandledException ex)
        //    {
        //        //BEDefaultResponse exceptionMessage = new CustomException().GetExceptionMessage(ex);
        //        ActualErrorMessage = ex.Message;
        //        messageData.MapBEHandledException(ex);
        //        await log.Error(ex, getOfferRequest.MSISDN);
        //    }
        //    catch (Exception ex)
        //    {
        //        ActualErrorMessage = ex.Message;
        //        messageData.MapException(ex);
        //        await log.Error(ex, getOfferRequest.MSISDN);
        //    }
        //    //WriteLog("M:");
        //    messageData.Data = offerList;
        //    return await WriteAuditTrail(messageData.Status, messageData, $"M:{getOfferRequest.MSISDN}");
        //    // return Json(dataList);

        //}


        [HttpPost]
        [Route("PurchaseMyOffer")]
        public async Task<IActionResult> PurchaseMyOffer([FromQuery] BaseRequest baseRequest, PurchaseOffer jsonRequest)
        {
            //BEPurchaseOffer purchasePlanReturnObj = new BEPurchaseOffer();

            var messageData = new BEMessageData<BEPurchaseOffer>();
            //messageData.Provider = AppSettings.Provider;
            //string msisdn = baseRequest.MSISDN;

            try
            {
                CheckPermission(baseRequest);


                if (string.IsNullOrEmpty(baseRequest.MSISDN))
                    ModelState.AddModelError("MSISDN", "Please provide MSISDN");

                if (!ModelState.IsValid) messageData.AddModelErrorMessages(ModelState);


                //string serviceUserID = User.Identity.Name;
                //BEServiceUser serviceUserInfo = BOConfiguationData.ServiceUserInfo[serviceUserID];
                var cmsConnectorAPI = new CMSAPIHandler(_clientFactory);
                var myOfferPurchaseResponse =
                    await cmsConnectorAPI.PurchaseMyOffer(ServiceUser, baseRequest.MSISDN, jsonRequest.offerId);
                if (myOfferPurchaseResponse != null)
                {
                    if (!string.IsNullOrEmpty(myOfferPurchaseResponse.ErrorMessage) ||
                        myOfferPurchaseResponse.ErrorCode > 0)
                        //ActualErrorMessage = myOfferPurchaseResponse.ErrorMessage;
                        throw new BeHandledException(myOfferPurchaseResponse.ErrorCode,
                            myOfferPurchaseResponse.ErrorMessage);

                    messageData.Message = "Purchase Request Successful";
                    messageData.Data.TransectionID = myOfferPurchaseResponse.ID;
                }
                else
                {
                    throw new BeHandledException(6001, "No Response from CMS");
                }
            }
            catch (BeHandledException ex)
            {
                //ActualErrorMessage = ex.ErrorDescription;
                //messageData.Error = ex.Message;
                //messageData.ErrorDescription = ex.ErrorDescription;
                //messageData.Status = ex.Status;
                //await log.Error(ex);

                ActualErrorMessage = ex.GetMessage;
                messageData.MapBEHandledException(ex);
                //await log.Error(ex);
            }
            catch (Exception ex)
            {
                //ActualErrorMessage = ex.Message;
                //messageData.Message = ex.Message;
                //messageData.Status = BEMessageCodes.InternaError.Status;

                //await log.Error(ex);
                ActualErrorMessage = ex.Message;
                messageData.MapException(ex);
                await log.Error(ex);
            }

            return await WriteAuditTrail(messageData.Status, messageData);

            //   return Json(purchasePlanReturnObj);
        }
    }
}