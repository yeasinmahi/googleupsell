﻿using System.Collections.Generic;
using BusinessEntity.APIHub2;
using Microsoft.AspNetCore.Mvc;

namespace APIHub2.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }    
        public IActionResult ErrorCodes()
        {
            List<KeyValuePair<string, string>> _dict = MessageCodesList.GetProperties(new BEMessageCodes());


            return View(_dict);
        }
    }
}
