﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace APIHub2.Controllers
{
    public class ErrorController : Controller
    {
        //public async Task<IActionResult> Index()
        //{
        //    return View();
        //}
        [Route("Error/{statusCode}")]
        public async Task<IActionResult> HttpStatusCodeHandler(int statusCode)
        {
            switch (statusCode)
            {
                case 404:
                    ViewBag.ErrorMessage = "Sorry, the resource you requested could not be found";
                    break;
            }

            return View("NotFound");
        }
    }
}