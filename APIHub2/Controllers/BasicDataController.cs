﻿using System;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using APIHub2.Helper;
using AutoMapper;
using BusinessEntity.APIHub2;
using BusinessObject;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APIHub2.Controllers
{
    [ApiController]
    [CustomAuthorize(ClaimTypes.Role, "BasicData")]
    public class BasicDataController : BaseAPIController
    {
        public BasicDataController(IHttpContextAccessor accessor, IMapper mapper, IHttpClientFactory clientFactory) : base(accessor, mapper,clientFactory)
        {
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("Get")]
        public ActionResult Get()
        {
            return Ok("Basic data API loaded successfully");
        }


        [HttpGet]
        [Route("GetDataPackList")]
        public async Task<IActionResult> GetDataPackList(int dataPackTypeID)
        {
            //Stopwatch watch = //Stopwatch.StartNew();
            //string ActualErrorMessage = string.Empty;
            //BEServiceUser serviceUserInfo = BOConfiguationData.GetServiceUserByID(User.Identity.Name);
            //BEServiceUser serviceUserInfo = BOConfiguationData.GetBEServiceUserByID("40");

            var messageData = new BEMessageData<BEDataProducts>();
            try
            {
                //    dataPacks.AddRange(avproducts);
                //    messageData.Data = dataPacks;
                if (dataPackTypeID == 0)
                    messageData.Data = BOConfiguationData.DataProducts;
                else
                    messageData.Data = BOConfiguationData.DataProducts;

                messageData.Status = BEMessageCodes.SuccessCode.Status;

                //if (avproducts.Any())
                //{
                //    dataPacks.AddRange(avproducts);
                //    messageData.Data = dataPacks;
                //    messageData.Status = BEMessageCodes.SuccessCode.Status;
                //    messageData.Message = "Data pack Load Successfully";
                //}
                //else
                //{
                //    messageData.Status = BEMessageCodes.DBSSError.Status;
                //    messageData.Error = BEMessageCodes.DBSSError.error;
                //    messageData.Message = "No Data available for current balance";
                //    messageData.ErrorDescription = "No Data available for current balance";
                //}
            }
            catch (BeHandledException ex)
            {
                //BEDefaultResponse exceptionMessage = new CustomException().GetExceptionMessage(ex);
                //ActualErrorMessage = ex.Message;
                //messageData.Status = ex.Status;
                //messageData.Error = ex.Message;
                //messageData.Message = 
                ActualErrorMessage = ex.GetMessage;
                messageData.MapBEHandledException(ex, "No Data load due to technical problem");
                //messageData.ErrorDescription = exceptionMessage.Message;
                //await log.Error(ex);
            }
            catch (Exception ex)
            {
                //messageData.Status = BEMessageCodes.InternaError.Status;
                //messageData.Error = BEMessageCodes.InternaError.error;
                //messageData.Message = "No Data load due to technical problem"; 
                //messageData.ErrorDescription = ex.Message;
                ActualErrorMessage = ex.Message;
                messageData.MapException(ex, "No Data load due to technical problem");
                await log.Error(ex);
            }

            //messageData.Provider = AppSettings.Provider;

            //         await WriteAuditTrail("M:No MSISDN",  watch.ElapsedMilliseconds, new[] { "DataPackTypeID:" + dataPackTypeID });
            //   return Json(messageData);

            return await WriteAuditTrail(messageData.Status, messageData, string.Empty,
                new[] {$"DataPackTypeID: {dataPackTypeID}"});
        }
        //[HttpGet]
        //[Route("GetPrepaidPostPaidType")]
        //public async Task<IActionResult>  GetPrepaidPostPaidType(BECommonRequest jsonRequest)
        //{
        //    string serviceMSISDN = jsonRequest.MSISDN;
        //    //Stopwatch watch = //Stopwatch.StartNew();
        //    BECommonResponse commonMessage = new BECommonResponse();
        //    try
        //    {

        //        double rtt = 0;
        //        DBSSWebAPI dbssAPI = new DBSSWebAPI();
        //        Subscription subscription = dbssAPI.GetSubscriptions(serviceMSISDN, "subscription-type", out rtt);
        //        if (subscription.Datas.Count > 0)
        //        {
        //            if (subscription.Datas.First().Attributes.Status.ToLower() != "active")
        //            {
        //                commonMessage.Message = "INACTIVE";
        //                commonMessage.IsSuccess = false;
        //            }
        //            else
        //            {
        //                if (subscription.Datas.First().Attributes.PaymentType.ToLower() == "prepaid")
        //                    commonMessage.Data = "PREP";
        //                else
        //                    commonMessage.Data = "POST";

        //                commonMessage.IsSuccess = true;
        //            }

        //        }
        //        else
        //        {
        //            commonMessage.Data = "NOTBL";
        //            commonMessage.IsSuccess = false;
        //        }
        //    }
        //    catch (BEException ex)
        //    {
        //        commonMessage.IsSuccess = false;
        //        BEMessageData<string> exceptionMessage = GetExceptionMessage(ex);
        //        commonMessage.Message = exceptionMessage.Message;
        //        commonMessage.MessageCode = exceptionMessage.MessageCode;
        //    }
        //    catch (Exception ex)
        //    {
        //        commonMessage.IsSuccess = false;

        //        commonMessage.Message = ex.Message;
        //        commonMessage.MessageCode = exceptionMessage.MessageCode;
        //    }

        //    commonMessage.Provider = AppSettings.Provider;
        //    watch.Stop();
        //    await WriteAuditTrail("M:" + jsonRequest.MSISDN, watch.ElapsedMilliseconds, new[] { "msisdn:" + jsonRequest.MSISDN });
        //    return Json(commonMessage);


        //}


        //[HttpGet]
        //[Route("GetRequestStatus")]
        //public async Task<IActionResult> GetRequestStatus(string userName, string token, string requestId)
        //{
        //    DBSSRequestStatus status = new DBSSRequestStatus();
        //    try
        //    {


        //        DBSSWebAPI dbssAPI = new DBSSWebAPI();
        //        // string requestStatusUrl = string.Format(DBSSAPIUrls.RequestStatusUrl, requestId);
        //        DBSSRequestResponse messageData = dbssAPI.GetRequestStatus(requestId.ToString());
        //        if (messageData.Data != null)
        //        {
        //            status.Data = new RequestStatus
        //            {
        //                Id = messageData.Data.Id,
        //                ScheduledAt = messageData.Data.Attributes.ScheduledAt,//.ToString("dd-MM-yyyy hh:mm:ss tt"),
        //                Status = messageData.Data.Attributes.Status
        //            };
        //            status.IsSuccess = true;
        //        }
        //    }
        //    catch (BEException ex)
        //    {
        //        status.IsSuccess = false;
        //        BEMessageData<string> exceptionMessage = GetExceptionMessage(ex);
        //        status.Message = exceptionMessage.Message;
        //        status.MessageCode = exceptionMessage.MessageCode;
        //    }
        //    catch (Exception ex)
        //    {
        //        status.IsSuccess = false;
        //        BEMessageData<string> exceptionMessage = GetExceptionMessage(ex);
        //        status.Message = exceptionMessage.Message;
        //        status.MessageCode = exceptionMessage.MessageCode;
        //    }
        //    status.Provider = BOSWebServiceSecurity4.Provider;
        //    SaveLog(userName, "", string.Empty, MethodBase.GetCurrentMethod().Name, this.GetType().ToString(), string.Empty, token, status.IsSuccess, status.Message, null);
        //    return status;
        //}

        //[WebMethod(EnableSession = false)]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
        //public APIHub.Models.BEDataPackPurchase VolumeTransfer(string userName, string transferFromMSISDN, string token, string transferToMSISDN, string packCode)
        //{
        //    BEDataPackPurchase dataPackPurchase = new BEDataPackPurchase();
        //    try
        //    {
        //        base.CheckToken(userName, string.Empty, string.Empty, token);
        //        HVC.BusinessEntity.BEServiceUserInfo bEServiceUser = ServiceUserInfo[userName];
        //        if (string.IsNullOrWhiteSpace(transferFromMSISDN))
        //        {
        //            transferFromMSISDN = bEServiceUser.MotherMobileNo;
        //        }
        //        string requestFrom = "880" + transferFromMSISDN;
        //        string requestTo = "880" + transferToMSISDN;
        //        DBSSSubscriber dbssSubscriber = this.dbssAPI.GetSubscriptionInformation(transferFromMSISDN);
        //        DBSSCommandResponse messageData;

        //        messageData = new DBSSWebAPI().TransferDataPack(packCode, requestFrom, requestTo, bEServiceUser.DBSSUserName, dbssSubscriber.SubscriptionID);

        //        if (messageData.Data.Any())
        //        {
        //            dataPackPurchase.IsSuccess = true;
        //            dataPackPurchase.Message = "Data pack transferred successfully";
        //            DBSSCommandResponse.DBSSCommandData successResponse
        //                = messageData.Data.First();
        //            dataPackPurchase.Data = new RequestStatus
        //            {
        //                Id = successResponse.Id,
        //                ScheduledAt = successResponse.Attributes.ScheduledAt.ToString("dd-MM-yyyy hh:mm:ss tt"),
        //                Status = successResponse.Attributes.Status
        //            };
        //        }
        //        else
        //        {
        //            dataPackPurchase.IsSuccess = false;
        //            dataPackPurchase.IsAttemptNeeded = true;
        //            dataPackPurchase.Message = messageData.Errors.FirstOrDefault().Detail;
        //            dataPackPurchase.MessageCode = messageData.Errors.FirstOrDefault().Status.ToString();
        //        }
        //    }
        //    catch (BEException ex)
        //    {
        //        dataPackPurchase.IsSuccess = false;
        //        BEMessageData<string> exceptionMessage = GetExceptionMessage(ex);
        //        dataPackPurchase.Message = exceptionMessage.Message;
        //        dataPackPurchase.MessageCode = exceptionMessage.MessageCode;
        //    }
        //    catch (Exception ex)
        //    {
        //        dataPackPurchase.IsSuccess = false;
        //        BEMessageData<string> exceptionMessage = GetExceptionMessage(ex);
        //        dataPackPurchase.Message = exceptionMessage.Message;
        //        dataPackPurchase.MessageCode = exceptionMessage.MessageCode;
        //    }

        //    dataPackPurchase.Provider = BOSWebServiceSecurity4.Provider;
        //    SaveLog(userName, transferFromMSISDN, string.Empty, MethodBase.GetCurrentMethod().Name, this.GetType().ToString(), string.Empty, token, dataPackPurchase.IsSuccess, dataPackPurchase.Message, null);
        //    return dataPackPurchase;
        //}


        //[WebMethod(EnableSession = false)]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
        //public APIHub.Models.DBSSAvailableProducts GetAvailableDataProduct(string userName, string dataProductForMSISDN, string token)
        //{
        //    APIHub.Models.DBSSAvailableProducts availableDataProduct = new APIHub.Models.DBSSAvailableProducts();
        //    try
        //    {
        //        base.CheckToken(userName, string.Empty, string.Empty, token);

        //        double rtt = 0;
        //        string subscriptionId = string.Empty;
        //        if (string.IsNullOrEmpty(dataProductForMSISDN))
        //        {
        //            HVC.BusinessEntity.BEServiceUserInfo bEServiceUser = ServiceUserInfo[userName];
        //            if (bEServiceUser != null && !string.IsNullOrWhiteSpace(bEServiceUser.MotherSubscriptionId))
        //            {
        //                subscriptionId = bEServiceUser.MotherSubscriptionId;
        //            }
        //            else
        //            {
        //                throw new Exception("There are no mother subscription Id set for this user");
        //            }

        //        }
        //        else
        //        {
        //            var dbssSubscriber = this.dbssAPI.GetSubscriptionInformation(dataProductForMSISDN);
        //            if (dbssSubscriber != null && !string.IsNullOrWhiteSpace(dbssSubscriber.SubscriptionID))
        //            {
        //                subscriptionId = dbssSubscriber.SubscriptionID;
        //            }
        //            else
        //            {
        //                throw new Exception("No data product assigned for this MSISDN. Please check the msisdn properly.");
        //            }

        //        }

        //        DBSSProducts product = this.dbssAPI.GetAvailableProductsByFamilyName(subscriptionId, "DATA", out rtt);
        //        availableDataProduct.IsSuccess = true;
        //        availableDataProduct.Data = product;
        //    }
        //    catch (BEException ex)
        //    {
        //        availableDataProduct.IsSuccess = false;
        //        BEMessageData<string> exceptionMessage = GetExceptionMessage(ex);
        //        availableDataProduct.Message = exceptionMessage.Message;
        //        availableDataProduct.MessageCode = exceptionMessage.MessageCode;
        //    }
        //    catch (Exception ex)
        //    {
        //        availableDataProduct.IsSuccess = false;
        //        BEMessageData<string> exceptionMessage = GetExceptionMessage(ex);
        //        availableDataProduct.Message = exceptionMessage.Message;
        //        availableDataProduct.MessageCode = exceptionMessage.MessageCode;
        //    }
        //    availableDataProduct.Provider = BOSWebServiceSecurity4.Provider;
        //    SaveLog(userName, dataProductForMSISDN, string.Empty, MethodBase.GetCurrentMethod().Name, this.GetType().ToString(), string.Empty, token, availableDataProduct.IsSuccess, availableDataProduct.Message, null);
        //    return availableDataProduct;
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="userName"></param>
        ///// <param name="dataProductForMSISDN"></param>
        ///// <param name="token"></param>
        ///// <returns></returns>
        //[WebMethod(EnableSession = false)]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
        //public APIHub.Models.DBSSAvailableProducts GetAvailableBulkDataProduct(string userName, string dataProductForMSISDN, string token)
        //{
        //    APIHub.Models.DBSSAvailableProducts availableDataProduct = new APIHub.Models.DBSSAvailableProducts();

        //    try
        //    {
        //        base.CheckToken(userName, string.Empty, string.Empty, token);

        //        double rtt = 0;

        //        HVC.BusinessEntity.BEServiceUserInfo bEServiceUser = ServiceUserInfo[userName];
        //        if (string.IsNullOrWhiteSpace(dataProductForMSISDN))
        //        {
        //            dataProductForMSISDN = bEServiceUser.MotherMobileNo;
        //        }

        //        DBSSProducts product = this.dbssAPI.GetBulkAvailableProducts(dataProductForMSISDN, bEServiceUser.MotherCompanyName, out rtt);
        //        availableDataProduct.IsSuccess = true;
        //        availableDataProduct.Data = product;
        //    }
        //    catch (BEException ex)
        //    {
        //        availableDataProduct.IsSuccess = false;
        //        BEMessageData<string> exceptionMessage = GetExceptionMessage(ex);
        //        availableDataProduct.Message = exceptionMessage.Message;
        //        availableDataProduct.MessageCode = exceptionMessage.MessageCode;
        //    }
        //    catch (Exception ex)
        //    {
        //        availableDataProduct.IsSuccess = false;
        //        BEMessageData<string> exceptionMessage = GetExceptionMessage(ex);
        //        availableDataProduct.Message = exceptionMessage.Message;
        //        availableDataProduct.MessageCode = exceptionMessage.MessageCode;
        //    }
        //    availableDataProduct.Provider = BOSWebServiceSecurity4.Provider;
        //    SaveLog(userName, dataProductForMSISDN, string.Empty, MethodBase.GetCurrentMethod().Name, this.GetType().ToString(), string.Empty, token, availableDataProduct.IsSuccess, availableDataProduct.Message, null);
        //    return availableDataProduct;
        //}
    }
}