﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using APIHub2.APIHandler;
using APIHub2.Helper;
using AutoMapper;
using BusinessEntity.APIHub2;
using BusinessEntity.ConnectorModels.CMS;
using BusinessEntity.ConnectorModels.DBSS;
using BusinessEntity.Google;
using BusinessEntity.Google.DPA;
using BusinessEntity.RequestResponseModels;
using BusinessObject;
using Connector.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Utility.Extensions;

//using System.Security.Claims;

namespace APIHub2.Controllers
{
    [ApiController]
    [CustomAuthorize(ClaimTypes.Role, "dpa")]
    // [CustomAuthorize(ClaimTypes.Role, "dpa,BasicData")]
    //[CustomAuthorize(ClaimTypes.Role, "dpa1")]
    //[Route("api/v1.0/[controller]")]
    public class DPAController : BaseAPIController
    {
        public DPAController(IHttpContextAccessor accessor, IMapper mapper, IHttpClientFactory clientFactory) : base(accessor, mapper,clientFactory)
        {
        }
        //public async Task<IActionResult> Index()
        //{
        //    return View();
        //}

        // [Route("DataPackList")]
        // [Authorize]


        //[AllowAnonymous]
        //[Authorize]
        //[Authorize(Roles = "dpa")]
        //[CustomAuthorize]
        //[CustomAuthorize(ClaimTypes.Role, "dpa")]

        ////[Route("{userKey}/DataPackList/MSISDN/{client_id}")]
        //[Route("{userKey}/DataPackList/CPID/{client_id}")]
        ////[Route("DataPackList")]
        //[HttpGet]//http://localhost:65173/api/v1.0/Google/userKey-123123/DataPackList/CPID/client_id-xyz

        //public BEMessageData<List<DataPack>> DataPackList(string userKey, string key_type, string client_id)
        //{
        //    //var rng = new Random();
        //    //return Enumerable.Range(1, 5).Select(index => new WeatherForecast
        //    //{
        //    //    Date = DateTime.Now.AddDays(index),
        //    //    TemperatureC = rng.Next(-20, 55),
        //    //    Summary = Summaries[rng.Next(Summaries.Length)]
        //    //})
        //    //.ToArray();

        //    string msisdn = new BOCpid().GetMsisdn(userKey);

        //    BEMessageData<List<DataPack>> dataList = new BEMessageData<List<DataPack>>();

        //    dataList.Data = new List<DataPack>();
        //    dataList.Data.Add(new DataPack() { id = "1" });
        //    dataList.Data.Add(new DataPack() { id = "2" });
        //    dataList.Data.Add(new DataPack() { id = "3" });

        //    WriteLog("M:");
        //    return dataList;
        //}


        //  [Route("{userKey}/planStatus/CPID/{client_id}")]
        [Route("{userKey}/planStatus")]
        [HttpGet]
        //{{host}}/api/v1.0/DPA/aUl6ejg4ZjNkYkZsYWNGR3R0NXpwUHRSNFNVNnUzV09oVGNodmtSdlpOaHN1KzFBUjZwNHRaQkRmZmVZaTk4SQ==/planStatus?key_type=CPID&client_id=9135fe0af9534e9d9741c9a4d2b4729d&context=Facebook
        public async Task<IActionResult> planStatus(string userKey, string key_type, string client_id)
        {
            var msisdn = string.Empty;
            var dataList = new PlanStatus();
            var statusCodes = StatusCodes.Status500InternalServerError;
            try
            {
                msisdn = await new BOCpid().GetMsisdn(userKey);
                CheckPermission(new BaseRequest { MSISDN = msisdn });
                AccountBalance accountBalance;

                var dbssApi = new DBSSWebAPI(_clientFactory);
                dataList.languageCode = AppSettings.LanguageCode;
                dataList.expireTime = DateTime.UtcNow.AddMinutes(5);
                dataList.updateTime = DateTime.UtcNow;


                dataList.title = "Prepaid";
                try
                {
                    accountBalance = await dbssApi.GetBalanceInformation(msisdn, string.Empty, "PREP");
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                var paymentType = accountBalance.PaymentType;

                if (paymentType != null && paymentType != "prepaid") dataList.title = "Postpaid";

                var counter = 0;
                var plans = new List<Plan>();

                foreach (var item in accountBalance.DadicatedAccountList.Where(x => x.GsmServiceType == "data"))
                {
                    counter++;
                    var module = new PlanModule
                    {
                        coarseBalanceLevel = BalanceLevel.BALANCE_LEVEL_UNSPECIFIED.ConvertToString(),
                        moduleName = item.ProductName,
                        description = item.ProductName,
                        expirationTime = item.ExpieryDate.ToUniversalTime(),

                        byteBalance = new ByteQuota
                        {
                            //quotaBytes = ((int)(item.TotalAmount * 1048576)).ToString(),
                            //quotaBytes = (512 * 1048576).ToString(),
                            remainingBytes = ((long)(item.RemainingAmount * 1048576)).ToString()
                        },
                        usedBytes = ((long)((item.TotalAmount - item.RemainingAmount) * 1048576)).ToString()

                    };
                    var modules = new List<PlanModule>();
                    modules.Add(module);
                    var plan = new Plan
                    {
                        planId = item.ProductId == null ? counter.ToString() : item.ProductId.ToString(),
                        planName = item.ProductName,

                        expirationTime = item.ExpieryDate.ToUniversalTime(),
                        //planCategory = !string.IsNullOrWhiteSpace(item.ProductCode) ?"Purchased" : "Bonus"
                        planCategory = paymentType == "prepaid"
                            ? PlanCategory.PREPAID.ConvertToString()
                            : PlanCategory.POSTPAID.ToString(),
                        planModules = modules
                    };
                    
                    var isShow = false;
                    if (plan.planModules != null && plan.planModules.Any())
                        isShow = plan.planModules.Any(x => x.byteBalance?.remainingBytes != null && Convert.ToInt64(x.byteBalance.remainingBytes) > 0);
                    if (isShow)
                        plans.Add(plan);
                }

                dataList.plans = plans;
                //throw new Exception("Test exception");
                //WriteLog("M:");
                statusCodes = 200;
            }
            catch (Exception ex)
            {
                await log.Error(ex, watch.ElapsedMilliseconds, await new LogService.LogHelper().ReadRequestBody(Request));
                ActualErrorMessage = ex.Message;
                return await WriteAuditTrail(StatusCodes.Status500InternalServerError,
                    new ErrorResponseGoogle
                    {
                        cause = ErrorStatusEnum.BACKEND_FAILURE,
                        errorMessage = errorValue.Error_dict[ErrorStatusEnum.BACKEND_FAILURE.ToString()]
                    }, "userKey:" + userKey, "key_type:" + key_type, "client_id:" + client_id);
            }

            return await WriteAuditTrail(statusCodes, dataList, "userKey:" + userKey, "key_type:" + key_type,
                "client_id:" + client_id);

        }

        // [Route("{userKey}/planOffer/CPID/{client_id}")]
        [Route("{userKey}/planOffer")]
        [HttpGet]
        //{{host}/api/v1.0/DPA/aUl6ejg4ZjNkYkZsYWNGR3R0NXpwUHRSNFNVNnUzV09oVGNodmtSdlpOaHN1KzFBUjZwNHRaQkRmZmVZaTk4SQ==/planOffer? key_type = CPID & client_id = 9135fe0af9534e9d9741c9a4d2b4729d&context=Facebook
        public async Task<IActionResult> planOffer(string userKey, string key_type, string client_id)
        {
            Stopwatch full = Stopwatch.StartNew();

            var dataList = new PlanOffer();
            var offerList = new List<Offer>();
            int statusCodes;
            try
            {
                var requestObject = new MyOfferListRequest();
                string msisdn = await new BOCpid().GetMsisdn(userKey);

                CheckPermission(new BaseRequest { MSISDN = msisdn });


                dataList.expireTime = DateTime.UtcNow.AddMinutes(5);

                //var paymentType = Claims?.FirstOrDefault(c => c.Type == "PaymentType")?.Value;
                //var subscriptionID = Claims?.FirstOrDefault(c => c.Type == "SubscriptionID")?.Value;


                //string serviceUserID = User.Identity.Name;

                //BEServiceUser serviceUserInfo = BOConfiguationData.ServiceUserInfo[serviceUserID];

                //var dbssApi = new DBSSWebAPI();
                //Subscription subscription = dbssAPI.GetSubscriptions(msisdn, "subscription-type", out double rtt);
                //Subscription.Datum subscriber = subscription.Datas.First();


                //var paymentType = subscriber.Attributes.PaymentType;

                //if (paymentType != "prepaid")
                //{
                //    PrepaidPostpaidType = 2;
                //}

                //offers = null;


                var cmsConnectorApi = new CMSAPIHandler(_clientFactory);
                var responseList = await cmsConnectorApi.GetMyOfferList(ServiceUser, msisdn, true, false);
                if (!(responseList == null || responseList.Count == 0))
                    foreach (var responseObj in responseList)
                    {
                        var unit = (int)responseObj.offerPrice;
                        var nanos = (responseObj.offerPrice - unit) * 1000000000;

                        //Offer beMyOffer = new Offer(responseObj.offerDescription, responseObj.offerID, responseObj.offerDescription, string.Empty,
                        //      AppSettings.LanguageCode, "BLOCKED", new Cost { currencyCode = "BDT", units = unit.ToString(), nanos = (int)nanos },
                        //      "3600s", string.Empty, new List<string>(), "123456"//check this value
                        //    );

                        var beMyOffer = new Offer(responseObj.offerShortDescription, responseObj.offerID,
                            responseObj.offerLongDescription, string.Empty,
                            AppSettings.LanguageCode, "BLOCKED",
                            new Cost { units = unit.ToString(), nanos = (int)nanos },
                            "3600s", string.Empty, new List<string>(), "123456" //check this value
                        );

                        offerList.Add(beMyOffer);
                    }
                else
                    ActualErrorMessage = "No offer after filtering";

                statusCodes = 200;
            }

            catch (BeHandledException ex)
            {
                statusCodes = ex.Status;
                //await log.Error(ex);
                ActualErrorMessage = ex.GetMessage;
                return await WriteAuditTrail(statusCodes,
                    new ErrorResponseGoogle
                    {
                        cause = ErrorStatusEnum.BACKEND_FAILURE,
                        errorMessage = errorValue.Error_dict[ErrorStatusEnum.BACKEND_FAILURE.ToString()]
                    }, "userKey:" + userKey, "key_type:" + key_type, "client_id:" + client_id);
            }
            catch (Exception ex)
            {
                statusCodes = StatusCodes.Status500InternalServerError;
                await log.Error(ex, watch.ElapsedMilliseconds, await new LogService.LogHelper().ReadRequestBody(Request));
                ActualErrorMessage = ex.Message;


                return await WriteAuditTrail(statusCodes,
                    new ErrorResponseGoogle
                    {
                        cause = ErrorStatusEnum.BACKEND_FAILURE,
                        errorMessage = errorValue.Error_dict[ErrorStatusEnum.BACKEND_FAILURE.ToString()]
                    }, "userKey:" + userKey, "key_type:" + key_type, "client_id:" + client_id);
                // return Json(new ErrorResponseGoogle { cause = ErrorStatusEnum.BACKEND_FAILURE, errorMessage = errorValue.Error_dict[ErrorStatusEnum.BACKEND_FAILURE.ToString()] });
                //isError = true;
                //     return await WriteAuditTrail(StatusCodes.Status500InternalServerError, new ErrorResponseGoogle { cause = ErrorStatusEnum.BACKEND_FAILURE, errorMessage = errorValue.Error_dict[ErrorStatusEnum.BACKEND_FAILURE.ToString()] }
                //, "M:" + msisdn, new[] { "userKey:" + userKey, "key_type:" + key_type, "client_id:" + client_id });
            }
            //WriteLog("M:");

            //if(isError)
            //  //  return Json(new ErrorResponseGoogle { cause = ErrorStatusEnum.BACKEND_FAILURE, errorMessage = errorValue.Error_dict[ErrorStatusEnum.BACKEND_FAILURE.ToString()] });

            //else
            dataList.offers = offerList;
            //full.Stop();
            //log.Info("Full " + full.ElapsedMilliseconds);
            return await WriteAuditTrail(statusCodes, dataList, "userKey:" + userKey, "key_type:" + key_type,
                "client_id:" + client_id);

            // return Json(dataList);

            //Thread.Sleep(20000);

            //return await WriteAuditTrail(200, new[] { "a","b"}, "userKey:" + userKey, "key_type:" + key_type,
                //"client_id:" + client_id);
        }

        //   [Route("{userKey}/purchasePlan/CPID/{client_id}")]
        [Route("{userKey}/purchasePlan")]
        [HttpPost] //http://localhost:65173/api/v1.0/Google/userKey-123123/purchasePlan/CPID/client_id-xyz

        //public async Task<IActionResult> purchasePlan(string userKey, string key_type, string client_id, [FromBody] BEpurchasePlanRequest jsonRequest)
        public async Task<IActionResult> purchasePlan(BEpurchasePlanRequest jsonRequest, string userKey,
            string key_type, string client_id)
        {
            //Stopwatch watch = //Stopwatch.StartNew();


            //string ActualErrorMessage = string.Empty;


            var purchasePlanReturnObj = new PurchasePlan();
            //CMSApi cmsAPI = new CMSApi();

            MyOfferPurchaseResponse responsefromApi;


            purchasePlanReturnObj.purchase.planId = jsonRequest.planId;

            //bool isError = false;

            try
            {
                var msisdn = await new BOCpid().GetMsisdn(userKey);
                CheckPermission(new BaseRequest { MSISDN = msisdn });
                //string serviceMSISDN = msisdn;
                //log.MSISDN = msisdn;
                //var paymentType = Claims?.FirstOrDefault(c => c.Type == "PaymentType")?.Value;
                //var subscriptionID = Claims?.FirstOrDefault(c => c.Type == "SubscriptionID")?.Value;
                //string serviceUserID = User.Identity.Name;
                //BEServiceUser serviceUserInfo = BOConfiguationData.ServiceUserInfo[serviceUserID];
                var cmsConnectorAPI = new CMSAPIHandler(_clientFactory);
                responsefromApi = await cmsConnectorAPI.PurchaseMyOffer(ServiceUser, msisdn, jsonRequest.planId);
                if (responsefromApi != null)
                {
                    if (!string.IsNullOrEmpty(responsefromApi.ErrorMessage) || responsefromApi.ErrorCode > 0)
                    {
                        purchasePlanReturnObj.transactionStatus = transactionStatusEnum.error;
                        var error = new ErrorResponseGoogle();
                        ActualErrorMessage = responsefromApi.ErrorMessage;
                        if (responsefromApi.ErrorCode == 108)
                        {
                            error.cause = ErrorStatusEnum.INCOMPATIBLE_PLAN;
                            error.errorMessage = errorValue.Error_dict[ErrorStatusEnum.INCOMPATIBLE_PLAN.ToString()];
                            ActualErrorMessage = error.errorMessage;
                        }
                        else
                        {
                            error.cause = ErrorStatusEnum.ERROR_CAUSE_UNSPECIFIED;
                            error.errorMessage =
                                errorValue.Error_dict[ErrorStatusEnum.ERROR_CAUSE_UNSPECIFIED.ToString()];
                            ActualErrorMessage = error.errorMessage;
                        }

                        return Ok(error);
                    }

                    purchasePlanReturnObj.transactionStatus = transactionStatusEnum.success;
                    purchasePlanReturnObj.purchase = new Purchase
                    {
                        transactionId = responsefromApi.ID,
                        planId = jsonRequest.planId,
                        //transactionMessage = string.Empty, confirmationCode = string.Empty, planActivationTime = DateTime.Now.AddMinutes(5) };
                        transactionMessage = string.Empty,
                        confirmationCode = string.Empty,
                        planActivationTime = DateTime.UtcNow
                    };
                    var balance = await GetCurrentBalance(msisdn);
                    var unit = (int)balance;
                    var nanos = (balance - unit) * 1000000000;
                    purchasePlanReturnObj.walletBalance = new WalletBalance
                    { currencyCode = "BDT", units = unit.ToString(), nanos = (int)nanos };
                }
                else
                {
                    ActualErrorMessage = "No Response from CMS";
                    //isError = true;
                    // return Json(new ErrorResponseGoogle { cause = ErrorStatusEnum.BACKEND_FAILURE, errorMessage = errorValue.Error_dict[ErrorStatusEnum.BACKEND_FAILURE.ToString()] });
                }
            }
            catch (Exception ex)
            {
                await log.Error(ex, watch.ElapsedMilliseconds, await new LogService.LogHelper().ReadRequestBody(Request));
                ActualErrorMessage = ex.Message;
                //isError = true;
                //return Json(new ErrorResponseGoogle { cause = ErrorStatusEnum.BACKEND_FAILURE, errorMessage = errorValue.Error_dict[ErrorStatusEnum.BACKEND_FAILURE.ToString()] });

                return await WriteAuditTrail(StatusCodes.Status500InternalServerError, new ErrorResponseGoogle
                {
                    cause = ErrorStatusEnum.BACKEND_FAILURE,
                    errorMessage = errorValue.Error_dict[ErrorStatusEnum.BACKEND_FAILURE.ToString()]
                }, $"offerID:{jsonRequest.planId};client_id:{client_id}");
            }

            //if (isError)
            //{
            //    return await WriteAuditTrail(StatusCodes.Status500InternalServerError, new ErrorResponseGoogle { cause = ErrorStatusEnum.BACKEND_FAILURE, 
            //        errorMessage = errorValue.Error_dict[ErrorStatusEnum.BACKEND_FAILURE.ToString()] }

            //        , $"M:{msisdn}", 
            //  new[] { $"offerID:{jsonRequest.planId};client_id:{client_id}" });
            //}
            //else
            //{
            return await WriteAuditTrail(StatusCodes.Status200OK, purchasePlanReturnObj,
                $"offerID:{jsonRequest.planId};TranID:{responsefromApi?.ID};client_id:{client_id}");
            //}
            //return Json(purchasePlanReturnObj);
        }

        [Route("{userKey}/RegisterCpid")]
        [HttpPost]
        public async Task RegisterCpid([FromBody] RegisterCpidRequest request, string userKey, string key_type, string client_id)
        {
            try
            {
                var obj = new BOCpid();
                var decrypt = await obj.GetDycrypt(userKey);
                if (decrypt.Status == 200)
                {
                    DateTime.TryParse(request.Staletime, out DateTime staletime);
                    var msisdn = decrypt.Msisdn;
                    await obj.SaveRegisterCpid(msisdn, userKey, staletime);
                }
                await WriteAuditTrail(StatusCodes.Status200OK, null,
                $"offerID:{request.Staletime};userKey:{userKey};key_type:{key_type};client_id:{client_id}");
            }
            catch(Exception ex)
            {
                await log.Error(ex, watch.ElapsedMilliseconds, await new LogService.LogHelper().ReadRequestBody(Request));
                ActualErrorMessage = ex.Message;

                await WriteAuditTrail(StatusCodes.Status500InternalServerError, null,
                 $"offerID:{request.Staletime};userKey:{userKey};key_type:{key_type};client_id:{client_id}");
            }
            

        }
        public async Task<double> GetCurrentBalance(string msisdn)
        {
            var dbssSubscriber = new DBSSSubscriber();
            double pbalance = 0;

            var includeSubscriptionsAndOwner = string.Empty;
            var dbssAPI = new DBSSWebAPI(_clientFactory);
            var subscription = await dbssAPI.GetSubscriptionsBalance(msisdn, "subscription-type,owner-customer,barrings,balances");
            if (subscription != null)
            {
                if (subscription. Errors != null) subscription.HandleError();
                if (subscription.Datas != null)
                {
                    var subscriptionType = subscription.Includeds.Find(x => x.Type == "subscription-types");
                    var balance = subscription.Includeds.FindAll(x => x.Type == "balances");

                    var mainBalance = balance.FindAll(x => x.Attributes.IsMainBalance);
                    if (mainBalance.Count > 0)
                        pbalance = (double)mainBalance.FirstOrDefault().Attributes.Amount;
                    dbssSubscriber.SubscriptionID = subscription.Datas[0].Id;
                    var subscriptionData = subscription.Datas.FirstOrDefault();

                    if (subscriptionData != null)
                    {
                        dbssSubscriber.CurrentPackage = subscriptionType != null && subscriptionType.Attributes.Name != null
                            ? subscriptionType.Attributes.Name.En ?? string.Empty
                            : string.Empty;
                        dbssSubscriber.PrepaidPostpaidType =
                            subscriptionType != null && !string.IsNullOrEmpty(subscriptionType.Attributes.PaymentType)
                                ? subscriptionType.Attributes.PaymentType
                                : string.Empty;
                        dbssSubscriber.PrepaidPostpaidType =
                            !string.IsNullOrEmpty(dbssSubscriber.PrepaidPostpaidType) &&
                            dbssSubscriber.PrepaidPostpaidType.ToLower() == "prepaid"
                                ? "PREP"
                                : "POST";
                    }
                }

                if (dbssSubscriber.PrepaidPostpaidType != "PREP") pbalance = 10000;
            }

            return pbalance;
        }
    }
}