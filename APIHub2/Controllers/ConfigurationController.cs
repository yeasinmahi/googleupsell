﻿using AutoMapper;
using BusinessEntity.APIHub2;
using BusinessEntity.ConnectorModels.Base;
using BusinessObject;
using DataAccess;
using LogService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using Utility;

namespace APIHub2.Controllers
{
    public class ConfigurationController : BaseAPIController
    {
        public ConfigurationController(IHttpContextAccessor accessor, IMapper mapper, IHttpClientFactory clientFactory) : base(accessor, mapper,clientFactory)
        {
        }

        [Route("All")]
        public IActionResult GetAll()
        {
            var appStatus = AppStatus();
            var appVersions = AppGetAppVersions();
            var appSettings = GetAppSettingsValue();
            dynamic obj = new
            {
                appStatus,
                appVersions,
                appSettings
            };
            return Ok(obj);
        }

        [Route("AllData")]
        public IActionResult GetAllData()
        {
            var userData = UserData();
            //dynamic appVersions = AppGetAppVersions();
            //dynamic appSettings = GetAppSettingsValue();
            dynamic obj = new
            {
                userData
                //appVersions,
                //appSettings,
            };
            return Ok(obj);
        }

        public dynamic UserData()
        {
            // double configuationDataLoadingTime = BusinessObject.BOConfiguationData.FullDataLoadingTime;


            dynamic obj = new
            {
                ServiceUserCount = BOConfiguationData.ServiceUser.Count,
                LastRefreshTime = BOConfiguationData.ServiceUserRefreshTime.ToString("dd-MMM-yyyy HH:mm:ss tt")
            };
            return obj;
        }

        [Route("GetAppSettings")]
        public IActionResult GetAppSettings()
        {
            var getAppSettings = GetAppSettingsValue();
            return Ok(getAppSettings);
        }

        [Route("GetAppVersions")]
        public IActionResult GetAppVersions()
        {
            var appVersions = AppGetAppVersions();
            return Ok(appVersions);
        }
        [Route("GetAppStatus")]
        public IActionResult GetAppStatus()
        {
            var appStatus = AppStatus(); ;
            return Ok(appStatus);
        }

        public dynamic AppStatus()
        {
            // double configuationDataLoadingTime = BusinessObject.BOConfiguationData.FullDataLoadingTime;


            dynamic obj = new
            {
                AppSettings.ApplicationStartTime,
                AppSettings.ApplicationLoadingTime,
                BOConfiguationData.FullDataLoadingTime,
                BOConfiguationData.ServiceUserLoadingTime
            };
            return obj;
        }

        public dynamic GetAppSettingsValue()
        {
            dynamic obj = new
            {
                AppSettings.ClientValidationEnabled,
                AppSettings.CMSApiBaseURL,
                AppSettings.CpidSecrtKey,
                AppSettings.DBBSApiBaseURL,
                AppSettings.DBBSErrorLogPath,
                AppSettings.DBSSHttpRequestTimeOut,
                AppSettings.DemoPrePostType,
                AppSettings.DNBOLMSApiBaseURL,
                AppSettings.EnableDemoAccess,
                AppSettings.EnableWriteDBBSErrorLog,
                AppSettings.FacebookAppID,
                AppSettings.FacebookAppSecret,
                AppSettings.HttpRequestTimeOut,
                AppSettings.ImageResizeTo,
                AppSettings.IsLoadSettings,
                AppSettings.IsTestSite,
                AppSettings.LargeResponse,
                AppSettings.LMSApiBaseURL,
                AppSettings.LOGMSISDNS,
                AppSettings.MediumResponse,
                AppSettings.MinimumRTTToLog,
                AppSettings.MoneyUnitText,
                AppSettings.NumberOfShipperForSendingParcel,
                AppSettings.OpenCodeBaseURL,
                AppSettings.OpenCodePassword,
                AppSettings.OpenCodeUserName,
                AppSettings.OTPLength,
                AppSettings.Provider,
                AppSettings.RandomNumberGeneratingString,
                AppSettings.RecordsPerPage,
                AppSettings.RequestLogPath,
                AppSettings.ServiceUserID,
                AppSettings.SiteURL,
                AppSettings.SmallReponse,
                AppSettings.TemporaryPasswordValidity,
                AppSettings.TrackingNumberLength,
                AppSettings.TravelDaysGap,
                AppSettings.UnobtrusiveJavaScriptEnabled
            };
            return obj;
        }

        public dynamic AppGetAppVersions()
        {
            //string CurrentAppName = GetType().Assembly.GetName().Name;
            //string businessEntityAssemblyVersion = new BusinessEntity.APIHub2.BEBase().GetType().Assembly.GetName().Name;
            //string businessObjectAssemblyVersion = new BusinessObject.BOAccount().GetType().Assembly.GetName().Name;
            //string dataAccessAssemblyVersion = new DataAccess.DACpid().GetType().Assembly.GetName().Name;
            //string connectorAssemblyVersion = new HttpError().GetType().Assembly.GetName().Name;
            //string logServiceAssemblyVersion = new LogService.Error().GetType().Assembly.GetName().Name;
            //string utilityAssemblyVersion = new EmailService().GetType().Assembly.GetName().Name;
            ////string sqlFactoryAssemblyVersion = new BusinessObject.BOAccount().GetSQLHelperVersion();

            dynamic obj = new
            {
                AppName = GetType().Assembly.GetName().Name,
                CurrentApp = GetType().Assembly.GetName().Version.ToString(),
                BusinessEntity = new BEBase().GetType().Assembly.GetName().Version.ToString(),
                BusinessObject = new BOAccount().GetType().Assembly.GetName().Version.ToString(),
                DataAccess = new DACpid().GetType().Assembly.GetName().Version.ToString(),
                Connector = new HttpError().GetType().Assembly.GetName().Version.ToString(),
                LogService = new Error().GetType().Assembly.GetName().Version.ToString(),
                Utility = new EmailService().GetType().Assembly.GetName().Version.ToString(),
                SqlFactory = new BOAccount().GetSqlHelperVersion()
            };
            return obj;
        }
    }
}