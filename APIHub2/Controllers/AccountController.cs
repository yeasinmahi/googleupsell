﻿using System;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using APIHub2.Helper;
using AutoMapper;
using BusinessEntity.APIHub2;
using BusinessEntity.RequestResponseModels;
using BusinessObject;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APIHub2.Controllers
{
    [CustomAuthorize(ClaimTypes.Role, "BasicData")]
    [ApiController]
    public class AccountController : BaseAPIController
    {
        public AccountController(IHttpContextAccessor accessor, IMapper mapper, IHttpClientFactory clientFactory) : base(accessor, mapper,clientFactory)
        {
        }

        [HttpPost]
        [Route("SendOTP")]
        public async Task<IActionResult> SendOTP([FromHeader] BaseRequest baseRequest, SendOTPRequest jsonRequest)
        {
            //string ActualErrorMessage = string.Empty;
            var messageData = new BEMessageData<string>();
            //Stopwatch watch = //Stopwatch.StartNew();
            try
            {
                CheckPermission(baseRequest);

                var data = new BEMessageData<string>();

                if (string.IsNullOrEmpty(baseRequest.MSISDN) || baseRequest.MSISDN.Length != 10)
                    throw new BeHandledException(BEMessageCodes.BadRequest.Status, BEMessageCodes.BadRequest.error,
                        "MSISDN must be 10 char");

                if (!jsonRequest.AllOperator)
                {
                    var isValidCustomerStatus = await IsValidCustomerStatus(baseRequest.MSISDN);
                    if (!isValidCustomerStatus)
                        throw new BeHandledException(BEMessageCodes.InvalidCustomer.Status,
                            BEMessageCodes.InvalidCustomer.error);
                }

                var boAccount = new BOAccount();
                var OTP = await boAccount.SendOTP(baseRequest.MSISDN, ServiceUser.UserID, true);
                messageData.Message = "SMS send to customer mobile";
                //messageData.Provider = AppSettings.Provider;
            }
            catch (BeHandledException ex)
            {
                ActualErrorMessage = ex.GetMessage;
                messageData.MapBEHandledException(ex);
                //await log.Error(ex);
            }
            catch (Exception ex)
            {
                ActualErrorMessage = ex.Message;
                messageData.MapException(ex);
                //messageData.Status = BEMessageCodes.InternaError.Status;
                //messageData.Error = BEMessageCodes.InternaError.error;
                //messageData.ErrorDescription = ex.Message;
                await log.Error(ex);
            }

            return await WriteAuditTrail(messageData.Status, messageData, "OTPFor:" + jsonRequest.OTPFor);
        }


        [HttpPost]
        [Route("IsValidOTP")]
        public async Task<IActionResult> IsValidOTP(IsValidOTPRequest jsonRequest)
        {
            /////////////////////**********************************************
            ///
            //string ActualErrorMessage = string.Empty;
            var isValidOTP = false;
            var messageData = new BEMessageData<string>();
            //OTPResponse Responsedata = new OTPResponse();
            //Stopwatch watch = //Stopwatch.StartNew();
            try
            {
                CheckPermission(jsonRequest);

                var errorMessage = string.Empty;
                try
                {
                    isValidOTP = await IsValidOTP(jsonRequest.MSISDN, jsonRequest.OTP);
                }
                catch (Exception ex)
                {
                    errorMessage = ex.Message;
                }

                if (isValidOTP)
                    throw new BeHandledException(BEMessageCodes.InvalidOTP.Status,
                        BEMessageCodes.InvalidOTP.error, string.Empty, "OTP is not valid");
                //if (isValidOTP)
                //{
                //    Responsedata.Status = BEMessageCodes.SuccessCode.Status;
                //    Responsedata.Message = BEMessageCodes.SuccessCode.error;
                //}
                //else
                //{
                //    Responsedata.Status = BEMessageCodes.DataNotFound.Status;
                //    Responsedata.Error = BEMessageCodes.DataNotFound.error;


                //}


                //Responsedata.Status = BEMessageCodes.SuccessCode.Status;
                messageData.Message = BEMessageCodes.SuccessCode.error;
            }
            catch (BeHandledException ex)
            {
                ActualErrorMessage = ex.GetMessage;
                messageData.MapBEHandledException(ex);
                //await log.Error(ex);
            }
            catch (Exception ex)
            {
                ActualErrorMessage = ex.Message;
                messageData.MapException(ex);
                await log.Error(ex);
            }

            return await WriteAuditTrail(messageData.Status, messageData, "OTPFor:" + jsonRequest.OTPFor,
                "OTP:" + jsonRequest.OTP, "OtherOperator:" + jsonRequest.IsOtherOperator);


            // return Json(Responsedata);

            ////////////////////
        }

        /// <exclude />
        protected async Task<bool> IsValidOTP(string requesterMSISDN, string customerOTP)
        {
            var isValidOTP = false;

            try
            {
                isValidOTP = await IsValidCustomer(requesterMSISDN, customerOTP, true);


                if (!isValidOTP)
                    throw new BeHandledException(BEMessageCodes.InvalidOTP.Status,
                        BEMessageCodes.InvalidOTP.error, string.Empty, "OTP is not valid");
            }
            catch (BeHandledException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isValidOTP;
        }

        private async Task<bool> IsValidCustomer(string customerMSISDN, string customerPasswordOrOTP, bool useOTP)
        {
            try
            {
                var boCustomer = new BOCustomer();
                var tempLockWrongAttemptLimit = int.Parse(AppSettings.temporaryLockWrongAttemptLimit);
                var tempLockWrongRemovalTimeMin = int.Parse(AppSettings.temporaryLockRemovalTimeMin);

                if (useOTP)
                {
                    var boService = new BOOTP();
                    await boService.IsValidOTPSHA1Encrypted(customerMSISDN, customerPasswordOrOTP);
                }
            }
            catch (BeHandledException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }
    }
}