﻿using System;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using APIHub2.Helper;
using AutoMapper;
using BusinessEntity.APIHub2;
using BusinessEntity.RequestResponseModels;
using BusinessObject;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APIHub2.Controllers
{
    [ApiController]
    [CustomAuthorize(ClaimTypes.Role, "BasicData")]
    //[ApiController]
    public class SelfCareController : BaseAPIController
    {
        public SelfCareController(IHttpContextAccessor accessor, IMapper mapper, IHttpClientFactory clientFactory) : base(accessor, mapper,clientFactory)
        {
        }

        [HttpPost]
        //[ApiVersion("1.0")]
        [Route("LoginBonus")]
        public async Task<IActionResult> LoginBonus(BonusRequest jsonRequest, DateTime browseDate)
        {
            //string ActualErrorMessage = string.Empty;
            var Responsedata = new BonusResponse();
            //Stopwatch watch = //Stopwatch.StartNew();
            try
            {
                try
                {
                    if (jsonRequest.MSISDN.Length != 10) throw new Exception();
                    var isValidCustomerStatus = await IsValidCustomerStatus(jsonRequest.MSISDN);
                    if (!isValidCustomerStatus) throw new Exception();

                    var bonus = new BENew_App_Bonus
                    {
                        Msisdn = jsonRequest.MSISDN,
                        BonusType = "login",
                        Browsedt = browseDate,
                        BrowseSource = ServiceUser.UserID,
                        IsProcessed = 0
                    };
                    CheckPermission(jsonRequest);

                    await new BONew_App_Bonus().Save(bonus);
                }
                catch (Exception ex)
                {
                    Responsedata.Status = BEMessageCodes.DataNotFound.Status;
                    Responsedata.Error = ex.Message;
                    await log.Error(ex, jsonRequest.MSISDN);
                    return null; // Json(Responsedata);
                }
            }
            catch (BeHandledException ex)
            {
                Responsedata.Status = ex.Status;
                Responsedata.Error = ex.Message;
                //await log.Error(ex);
            }
            catch (Exception ex)
            {
                Responsedata.Status = BEMessageCodes.DataNotFound.Status;
                Responsedata.Error = ex.Message;
                await log.Error(ex);
            }
            finally
            {
                //watch.Stop();
                Responsedata.Status = BEMessageCodes.SuccessCode.Status;
                Responsedata.Message = BEMessageCodes.SuccessCode.error;
            }

            return await WriteAuditTrail(Responsedata.Status, Responsedata, "date:" + jsonRequest.date);

            //   return Json(Responsedata);
        }

        [HttpPost]
        //[ApiVersion("1.0")]
        [Route("SignupBonus")]
        public async Task<IActionResult> SignupBonus(BonusRequest jsonRequest, DateTime browseDate)
        {
            //string ActualErrorMessage = string.Empty;
            var Responsedata = new BonusResponse();
            //Stopwatch watch = //Stopwatch.StartNew();
            try
            {
                CheckPermission(jsonRequest);
                //// Date checking 
                //try
                //{
                //    DateTime browseDt;
                //    if (DateTime.TryParse(jsonRequest.date, out browseDt))
                //    {
                //        if (browseDt < DateTime.Now.AddDays(-15))
                //        {
                //            throw new Exception();
                //        }
                //    }
                //    else
                //    {
                //        throw new Exception();
                //    }

                //}
                //catch (Exception ex)
                //{
                //    Responsedata.Status = BEMessageCodes.BonusDateErrorCode.Status;
                //    Responsedata.Error = BEMessageCodes.BonusDateErrorCode.error;
                //    await log.Error(ex);
                //    return Json(Responsedata);
                //}
                // msisdn checking 

                if (string.IsNullOrEmpty(jsonRequest.MSISDN) || jsonRequest.MSISDN.Length != 10)
                    // throw new Exception();

                    throw new BeHandledException(BEMessageCodes.InvalidMSISDN.Status,
                        BEMessageCodes.InvalidMSISDN.error);
                var isValidCustomerStatus = await IsValidCustomerStatus(jsonRequest.MSISDN);
                if (!isValidCustomerStatus)
                    throw new BeHandledException(BEMessageCodes.InvalidCustomer.Status,
                        BEMessageCodes.InvalidCustomer.error);
                //DBSSWebAPI dbssAPI = new DBSSWebAPI();
                //Subscription subscription = dbssAPI.GetSubscriptions(jsonRequest.msisdn, "subscription-type", out double rtt);
                //if(subscription.Datas.Count < 0)
                //{
                //    throw new Exception();
                //}
                //Subscription.Datum subscriber = subscription.Datas.First();
                //if (subscriber.Attributes.Status !="active")
                //{
                //    throw new Exception();
                //}
                var bonus = new BENew_App_Bonus
                {
                    Msisdn = jsonRequest.MSISDN,
                    BonusType = "signup",
                    Browsedt = browseDate,
                    BrowseSource = ServiceUser.UserID,
                    IsProcessed = 0
                };

                var is_exist = await new BONew_App_Bonus().GetNew_App_Bonuss("signup", jsonRequest.MSISDN, browseDate);
                if (!is_exist)
                {
                    await new BONew_App_Bonus().Save(bonus);
                }
                else
                {
                    Responsedata.Status = BEMessageCodes.DataNotFound.Status;
                    Responsedata.Error = "Repeat Insert";
                }

                //watch.Stop();
                //Responsedata.Status = BEMessageCodes.SuccessCode.Status;
                //Responsedata.Message = BEMessageCodes.SuccessCode.error;
            }
            catch (BeHandledException ex)
            {
                Responsedata.Status = ex.Status;
                Responsedata.Error = ex.Message;
                //await log.Error(ex);
            }
            catch (Exception ex)
            {
                Responsedata.Status = BEMessageCodes.DataNotFound.Status;
                Responsedata.Error = ex.Message;
                await log.Error(ex);
            }

            return await WriteAuditTrail(Responsedata.Status, Responsedata, "date:" + jsonRequest.date);

            //     return   Json(Responsedata);
        }

        [HttpPost]
        [ApiVersion("1.0")]
        [Route("ProcessBonus")]
        public async Task<IActionResult> ProcessBonus(BonusRequest jsonRequest)
        {
            //string ActualErrorMessage = string.Empty;
            var Responsedata = new BonusResponse();
            //Stopwatch watch = //Stopwatch.StartNew();
            // Date checking
            //DateTime browseDt;
            try
            {
                CheckPermission(jsonRequest);

                // DateTime.TryParseExact(jsonRequest.date, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out browseDt);
                //if (DateTime.TryParseExact(jsonRequest.date, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out browseDt))
                //if (DateTime.TryParseExact(jsonRequest.date, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out browseDt))
                //{
                if (jsonRequest.date < DateTime.Now.AddDays(-15))
                    throw new BeHandledException(BEMessageCodes.BonusDateErrorCode.Status,
                        "Please check your date properly", "Date can not be 15 days previous of current date",
                        BEMessageCodes.BonusDateErrorCode.error);
                //}
                //else
                //{
                //    throw new BEHandledException(BEMessageCodes.BonusDateErrorCode.Status, BEMessageCodes.BonusDateErrorCode.error, "Date format error", "Date format should be dd-MMM-yyyy");
                //}
            }
            catch (BeHandledException ex)
            {
                Responsedata.Status = BEMessageCodes.BonusDateErrorCode.Status;
                Responsedata.Error = BEMessageCodes.BonusDateErrorCode.error;

                await log.Error(ex);
                //return Json(Responsedata);
                return Ok(Responsedata);
            }
            catch (Exception ex)
            {
                Responsedata.Status = BEMessageCodes.BonusDateErrorCode.Status;
                Responsedata.Error = BEMessageCodes.BonusDateErrorCode.error;
                await log.Error(ex);
                //return Json(Responsedata);
                return Ok(Responsedata);
            }

            try
            {
                if (jsonRequest.bonusType == BonusType.SignIn) return await LoginBonus(jsonRequest, jsonRequest.date);

                if (jsonRequest.bonusType == BonusType.SignUp) return await SignupBonus(jsonRequest, jsonRequest.date);

                Responsedata.Status = BEMessageCodes.BonusDateErrorCode.Status;
                Responsedata.Error = BEMessageCodes.BonusDateErrorCode.error;
                //return Json(Responsedata);
            }
            catch (Exception ex)
            {
                await log.Error(ex);
            }

            return await WriteAuditTrail(Responsedata.Status, Responsedata, "date:" + jsonRequest.date);

            //  return Json(Responsedata);
        }

        public async Task<BEMessageData<string>> SaveBonus(string msisdn, DateTime browseDate, string bonusType)
        {
            var response = new BEMessageData<string>();
            //response.Provider = AppSettings.Provider;
            response.Message = "Your request has been processed successfully";

            try
            {
                var bonus = new BENew_App_Bonus
                {
                    Msisdn = msisdn,
                    BonusType = bonusType,
                    Browsedt = browseDate,
                    BrowseSource = ServiceUser.UserID,
                    IsProcessed = 0
                };

                //bool is_exist = await new BONew_App_Bonus().GetNew_App_Bonuss(bonusType, msisdn, browseDate);
                //if (!is_exist)
                //{
                await new BONew_App_Bonus().Save(bonus);
                //}
                //else
                //{
                //    throw new BEHandledException(BEMessageCodes.AlreadyExist.Status, BEMessageCodes.AlreadyExist.error, "The requested data is already exist in our system", "Already exist");
                //}
            }
            catch (BeHandledException ex)
            {
                throw ex;
                //ActualErrorMessage = ex.Message;
                //response.MapBEHandledException(ex);
                //await log.Error(ex);
            }
            catch (Exception ex)
            {
                //ActualErrorMessage = ex.Message;
                //response.MapException(ex);
                //await log.Error(ex);

                throw ex;
            }


            return response;
            // return await WriteAuditTrail(response.Status, response, $"M:{msisdn}", new[] { $"browseDate:{browseDate};bonusType:{bonusType}" });

            //   return Json(Responsedata);
        }


        [ApiVersion("2.0")]
        [HttpPost]
        [Route("ProcessBonus")]
        public async Task<IActionResult> ProcessBonus1(BonusRequest jsonRequest, [FromQuery] BaseRequest baseRequest)
        {
            //BonusResponse Responsedata = new BonusResponse();
            var response = new BEMessageData<string>();
            try
            {
                CheckPermission(baseRequest);

                if (string.IsNullOrEmpty(baseRequest.MSISDN) || baseRequest.MSISDN.Length != 10)
                    throw new BeHandledException(BEMessageCodes.InvalidMSISDN.Status,
                        BEMessageCodes.InvalidMSISDN.error, "MSISDN should be 10 digit", "MSISDN should be 10 digit");
                var isValidCustomerStatus = await IsValidCustomerStatus(baseRequest.MSISDN);
                if (!isValidCustomerStatus)
                    throw new BeHandledException(BEMessageCodes.InvalidCustomer.Status,
                        BEMessageCodes.InvalidCustomer.error, "Provided MSISDN " + baseRequest.MSISDN + " is invalid",
                        "Provided MSISDN " + baseRequest.MSISDN + " is invalid");
                //if (DateTime.TryParseExact(jsonRequest.date, "dd-MMM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out browseDt))
                //{
                if (jsonRequest.date < DateTime.Now.AddDays(-15))
                    throw new BeHandledException(BEMessageCodes.BonusDateErrorCode.Status,
                        BEMessageCodes.BonusDateErrorCode.error, "Please check your date properly",
                        "Date can not be 15 days previous of current date");
                //}
                //else
                //{
                //    throw new BEHandledException(BEMessageCodes.BonusDateErrorCode.Status, BEMessageCodes.BonusDateErrorCode.error, "Date format error", "Date format should be dd-MMM-yyyy");
                //}

                if (jsonRequest.bonusType == BonusType.SignIn)
                    response = await SaveBonus(baseRequest.MSISDN, jsonRequest.date, "login");
                else if (jsonRequest.bonusType == BonusType.SignUp)
                    response = await SaveBonus(baseRequest.MSISDN, jsonRequest.date, "signup");
                else
                    throw new BeHandledException(BEMessageCodes.BadRequest.Status, BEMessageCodes.BadRequest.error,
                        "Please use bonus type SignIn/SignUp", "BonusTypeProblem");
            }
            catch (BeHandledException ex)
            {
                ActualErrorMessage = ex.GetMessage;
                response.MapBEHandledException(ex);
                //await log.Error(ex);
            }
            catch (Exception ex)
            {
                ActualErrorMessage = ex.Message;
                response.MapException(ex);
                await log.Error(ex);
            }

            return await WriteAuditTrail(response.Status, response, "msisdn:" + baseRequest.MSISDN,
                "date:" + jsonRequest.date, "bonusType:" + jsonRequest.bonusType);
        }
    }
}