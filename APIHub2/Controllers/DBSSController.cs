﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using APIHub2.Helper;
using AutoMapper;
using BusinessEntity.APIHub2;
using BusinessEntity.RequestResponseModels;
using Connector.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace APIHub2.Controllers
{
    [ApiController]
    [CustomAuthorize(ClaimTypes.Role, "CustomerToken")]
    public class DBSSController : BaseAPIController
    {
        public DBSSController(IHttpContextAccessor accessor, IMapper mapper,IHttpClientFactory clientFactory) : base(accessor, mapper, clientFactory)
        {
        }

        [HttpPost]
        [Route("DataPackPurchase")]
        public async Task<IActionResult> DataPackPurchase([FromQuery] BaseRequest baseRequest,
            BEDataPackPurchaseRequest jsonRequest)
        {
            //string ActualErrorMessage = string.Empty;
            //Stopwatch watch = //Stopwatch.StartNew();
            var messageData = new BEMessageData<BEProductPurchaseStatus>();
            //messageData.Provider = AppSettings.Provider;

            try
            {
                CheckPermission(baseRequest);
                //BEServiceUser serviceUserInfo = BOConfiguationData.GetServiceUserByID(User.Identity.Name);
                //BEServiceUser serviceUserInfo = BOConfiguationData.GetBEServiceUserByID("40");

                var dbssAPI = new DBSSWebAPI(_clientFactory);

                if (string.IsNullOrEmpty(baseRequest.SubscriptionID))
                {
                    var dbssSubscription = await dbssAPI.GetSubscriptionInformation(baseRequest.MSISDN);

                    if (dbssSubscription == null || dbssSubscription == null)
                    {
                        throw new BeHandledException(BEMessageCodes.InvalidMSISDN.Status, BEMessageCodes.InvalidMSISDN.error,"Invalid MSISDN or Other Operator");
                    }
                    baseRequest.SubscriptionID = dbssSubscription.SubscriptionID;

                    //if (dbssSubscription != null && dbssSubscription.Result != null)
                    //{

                    //    baseRequest.SubscriptionID = dbssSubscription.Result.SubscriptionID;
                    //}
                    //else
                    //{
                    //    throw new Exception("This MSISDN is not registered for purchase data pack");
                    //}


                }

                var dataPackResponse = await dbssAPI.DataPackBySubscription(jsonRequest.dataPackCode,
                    baseRequest.SubscriptionID, ServiceUser.DBSSUserName);

                if (dataPackResponse != null && dataPackResponse.Data != null && dataPackResponse.Data.Any())
                {
                    messageData.Status = BEMessageCodes.SuccessCode.Status;
                    messageData.Message = "DataPack purchase successfully";

                    var successResponse = dataPackResponse.Data.First();

                    messageData.Data = new BEProductPurchaseStatus
                    {
                        Id = successResponse.Id,
                        ScheduledAt = successResponse.Attributes.ScheduledAt.ToString("dd-MM-yyyy hh:mm:ss tt"),
                        Status = successResponse.Attributes.Status
                    };
                    // dataPackPurchase.Status = successResponse.Attributes.Status;
                }
                else
                {
                    messageData.Message = "Data can not be purchased successfully due to technical error";
                    //messageData.Status = BEMessageCodes.DBSSError.Status;
                    //messageData.Error = BEMessageCodes.DBSSError.error;
                    //messageData.ErrorDescription = dataPackResponse.Errors.FirstOrDefault().Detail;
                    //if (dataPackResponse.Errors.Count > 1)
                    //{
                    //    messageData.ErrorDescription = string.Empty;
                    //    foreach (Error r in dataPackResponse.Errors)
                    //    {
                    //        messageData.ErrorDescription += r.Detail+";";
                    //    }
                    //}

                    if (dataPackResponse!=null&&dataPackResponse.Errors!=null)
                    {
                        ActualErrorMessage = dataPackResponse.Errors.FirstOrDefault().Detail;
                        if (dataPackResponse.Errors.Count > 1)
                        {
                            ActualErrorMessage = string.Empty;
                            foreach (var r in dataPackResponse.Errors) ActualErrorMessage = r.Detail;
                        }
                    }
                    


                    throw new BeHandledException(BEMessageCodes.DBSSError.Status,
                        BEMessageCodes.DBSSError.error, ActualErrorMessage,
                        "Data can not be purchased successfully due to technical error");
                }
            }
            catch (BeHandledException ex)
            {
                //BEDefaultResponse exceptionMessage = new CustomException().GetExceptionMessage(ex);
                ActualErrorMessage = ex.GetMessage;
                messageData.MapBEHandledException(ex, "Data can not be purchased successfully due to technical error");
                //await log.Error(ex);
            }
            catch (Exception ex)
            {
                ActualErrorMessage = ex.Message;
                messageData.MapException(ex, "Data can not be purchased successfully due to technical error");
                await log.Error(ex,watch.ElapsedMilliseconds,await new LogService.LogHelper().ReadRequestBody(Request));
            }

            //messageData.Provider = AppSettings.Provider;

            //ActualErrorMessage = messageData.Error + " " + messageData.ErrorDescription;
            return await WriteAuditTrail(messageData.Status, messageData,
                $"dataPackCode:{jsonRequest.dataPackCode};TranID:{messageData.Data?.Id}");
            //return Json(messageData);
            //return HttpResponse(messageData.Status, messageData);
        }

        [HttpGet]
        [Route("VolumeTransfer")]
        public async Task<IActionResult> VolumeTransfer(string msisdn, string subcriptionId, string transferToMSISDN,
            string packCode)
        {
            //string ActualErrorMessage = string.Empty;
            //Stopwatch watch = //Stopwatch.StartNew();
            var messageData = new BEMessageData<BEProductPurchaseStatus>();
            //messageData.Provider = AppSettings.Provider;
            try
            {
                CheckPermission(new BaseRequest {MSISDN = msisdn, SubscriptionID = subcriptionId});
                //BEServiceUser serviceUserInfo = BOConfiguationData.GetServiceUserByID(User.Identity.Name);
                //BEServiceUser serviceUserInfo = BOConfiguationData.GetBEServiceUserByID("40");
                if (string.IsNullOrWhiteSpace(msisdn)) msisdn = ServiceUser.MotherMobileNo;

                var requestFrom = "880" + msisdn;
                var requestTo = "880" + transferToMSISDN;
                var dbssAPI = new DBSSWebAPI(_clientFactory);
                if (string.IsNullOrEmpty(subcriptionId))
                {
                    var dbssSubscriber = await dbssAPI.GetSubscriptionInformation(msisdn);
                    subcriptionId = dbssSubscriber.SubscriptionID;
                }

                var DbssResponse = await dbssAPI.TransferDataPack(packCode, requestFrom, requestTo,
                    ServiceUser.DBSSUserName, subcriptionId);

                if (DbssResponse.Data.Any())
                {
                    messageData.Status = BEMessageCodes.SuccessCode.Status;
                    messageData.Message = "Volume transfered successfully";

                    var successResponse = DbssResponse.Data.First();
                    messageData.Data = new BEProductPurchaseStatus
                    {
                        Id = successResponse.Id,
                        ScheduledAt = successResponse.Attributes.ScheduledAt.ToString("dd-MM-yyyy hh:mm:ss tt"),
                        Status = successResponse.Attributes.Status
                    };
                }
                else
                {
                    //messageData.Status = BEMessageCodes.DBSSError.Status;
                    //messageData.Error = BEMessageCodes.DBSSError.error;
                    //messageData.Message = "Volume can not be transferred due to technical problem";
                    //messageData.ErrorDescription = DbssResponse.Errors.FirstOrDefault().Detail;
                    throw new BeHandledException(BEMessageCodes.DBSSError.Status, BEMessageCodes.DBSSError.error, null,
                        DbssResponse.Errors.FirstOrDefault().Detail);
                }
            }
            catch (BeHandledException ex)
            {
                //BEDefaultResponse exceptionMessage = new CustomException().GetExceptionMessage(ex);
                ActualErrorMessage = ex.GetMessage;
                messageData.MapBEHandledException(ex, "Volume can not be transferred due to technical problem");
                //await log.Error(ex);
            }
            catch (Exception ex)
            {
                ActualErrorMessage = ex.Message;
                messageData.MapException(ex, "Volume can not be transferred due to technical problem");
                await log.Error(ex, watch.ElapsedMilliseconds, await new LogService.LogHelper().ReadRequestBody(Request));
            }

            //messageData.Provider = AppSettings.Provider;

            //  return await WriteAuditTrail(messageData.Status, messageData, "M:" + requestFrom,  new[] { "TransferFrom:" + msisdn, "TransferTo:" + transferToMSISDN, "packCode" + packCode });
            return await WriteAuditTrail(messageData.Status, messageData,
                $"TransferFrom:{msisdn};packCode:{packCode}TranID:{messageData.Data?.Id}");
            //return Json(messageData);
            //   return HttpResponse(messageData.Status, messageData);
        }

        [HttpGet]
        [Route("GetAvailableDataPackList")]
        public async Task<IActionResult> GetAvailableDataPackList(
            [FromQuery] AvailableDataPackListRequest availableDataPackListRequest)
        {
            //string ActualErrorMessage = string.Empty;
            //Stopwatch watch = //Stopwatch.StartNew();


            //BEServiceUser serviceUserInfo = BOConfiguationData.GetServiceUserByID(User.Identity.Name);
            //BEServiceUser serviceUserInfo = BOConfiguationData.GetBEServiceUserByID("40");

            var messageData = new BEMessageData<List<BEDataServicePack>>();
            //messageData.Provider = AppSettings.Provider;

            var dbssAPI = new DBSSWebAPI(_clientFactory);
            //BEDefaultResponse messageData = new BEDefaultResponse();
            try
            {
                CheckPermission(availableDataPackListRequest);

                var dataPacks = new BEDataServicePacks();
                //IEnumerable<BEDataProduct> products = await dbssAPI.GetAllSubscriberDataProduct(User.Identity.Name, msisdn).Where(s => s.ProductFamilyId == dataPackTypeID);

                var products =
                    await dbssAPI.GetAllSubscriberDataProduct(User.Identity.Name, availableDataPackListRequest.MSISDN);
                products = products.Where(s => s.ProductFamilyId == availableDataPackListRequest.DataPackTypeID);


                var dataProducts = products.Where(item => !((string.IsNullOrWhiteSpace(item.Price) || item.Price == "0")
                                                            && !(availableDataPackListRequest.DataPackTypeID == 16
                                                                 || availableDataPackListRequest.DataPackTypeID == 76
                                                                 || availableDataPackListRequest.DataPackTypeID == 77)))
                    .Select(item => new BEDataServicePack
                    {
                        DataServicePackID = item.DataProductId,
                        DataPackTypeID = item.ProductFamilyId,
                        DataServicePackName = item.DataProductName,
                        VASServieActivationCode = item.DataCode,
                        PackName = item.DataProductNameDisplay,
                        Price = item.Price,
                        Volume = item.Volume,
                        Validity = item.Validity,
                        Description = item.Description,
                        PriceText = string.Format("BDT {0}", item.Price),
                        IsAutoRenewal = !string.IsNullOrWhiteSpace(item.AutoRenewCode)
                    }).ToList();

                //    dataPacks.AddRange(avproducts);
                //    messageData.Data = dataPacks;

                messageData.Data = dataProducts;
                messageData.Status = BEMessageCodes.SuccessCode.Status;

                //if (avproducts.Any())
                //{
                //    dataPacks.AddRange(avproducts);
                //    messageData.Data = dataPacks;
                //    messageData.Status = BEMessageCodes.SuccessCode.Status;
                //    messageData.Message = "Data pack Load Successfully";
                //}
                //else
                //{
                //    messageData.Status = BEMessageCodes.DBSSError.Status;
                //    messageData.Error = BEMessageCodes.DBSSError.error;
                //    messageData.Message = "No Data available for current balance";
                //    messageData.ErrorDescription = "No Data available for current balance";
                //}
            }
            catch (BeHandledException ex)
            {
                //BEDefaultResponse exceptionMessage = new CustomException().GetExceptionMessage(ex);
                ActualErrorMessage = ex.GetMessage;
                messageData.MapBEHandledException(ex);
                //await log.Error(ex);
                //return Unauthorized(messageData);
            }
            catch (Exception ex)
            {
                ActualErrorMessage = ex.Message;
                messageData.MapException(ex, "No Data load due to technical problem");
                await log.Error(ex, watch.ElapsedMilliseconds, await new LogService.LogHelper().ReadRequestBody(Request));
            }

            return await WriteAuditTrail(messageData.Status, messageData,
                $"DataPackTypeID:{availableDataPackListRequest.DataPackTypeID}");
            //return Json(messageData);
            //return HttpResponse(messageData.Status , messageData);
        }
        //[HttpGet]
        //[Route("GetPrepaidPostPaidType")]
        //public async Task<IActionResult>  GetPrepaidPostPaidType(BECommonRequest jsonRequest)
        //{
        //    string serviceMSISDN = jsonRequest.MSISDN;
        //    //Stopwatch watch = //Stopwatch.StartNew();
        //    BECommonResponse commonMessage = new BECommonResponse();
        //    try
        //    {

        //        double rtt = 0;
        //        DBSSWebAPI dbssAPI = new DBSSWebAPI();
        //        Subscription subscription = dbssAPI.GetSubscriptions(serviceMSISDN, "subscription-type", out rtt);
        //        if (subscription.Datas.Count > 0)
        //        {
        //            if (subscription.Datas.First().Attributes.Status.ToLower() != "active")
        //            {
        //                commonMessage.Message = "INACTIVE";
        //                commonMessage.IsSuccess = false;
        //            }
        //            else
        //            {
        //                if (subscription.Datas.First().Attributes.PaymentType.ToLower() == "prepaid")
        //                    commonMessage.Data = "PREP";
        //                else
        //                    commonMessage.Data = "POST";

        //                commonMessage.IsSuccess = true;
        //            }

        //        }
        //        else
        //        {
        //            commonMessage.Data = "NOTBL";
        //            commonMessage.IsSuccess = false;
        //        }
        //    }
        //    catch (BEException ex)
        //    {
        //        commonMessage.IsSuccess = false;
        //        BEMessageData<string> exceptionMessage = GetExceptionMessage(ex);
        //        commonMessage.Message = exceptionMessage.Message;
        //        commonMessage.MessageCode = exceptionMessage.MessageCode;
        //    }
        //    catch (Exception ex)
        //    {
        //        commonMessage.IsSuccess = false;

        //        commonMessage.Message = ex.Message;
        //        commonMessage.MessageCode = exceptionMessage.MessageCode;
        //    }

        //    commonMessage.Provider = AppSettings.Provider;
        //    watch.Stop();
        //    await WriteAuditTrail("M:" + jsonRequest.MSISDN, new[] { "msisdn:" + jsonRequest.MSISDN });
        //    return Json(commonMessage);


        //}


        //[HttpGet]
        //[Route("GetRequestStatus")]
        //public async Task<IActionResult> GetRequestStatus(string userName, string token, string requestId)
        //{
        //    DBSSRequestStatus status = new DBSSRequestStatus();
        //    try
        //    {


        //        DBSSWebAPI dbssAPI = new DBSSWebAPI();
        //        // string requestStatusUrl = string.Format(DBSSAPIUrls.RequestStatusUrl, requestId);
        //        DBSSRequestResponse messageData = dbssAPI.GetRequestStatus(requestId.ToString());
        //        if (messageData.Data != null)
        //        {
        //            status.Data = new ProductPurchaseStatus
        //            {
        //                Id = messageData.Data.Id,
        //                ScheduledAt = messageData.Data.Attributes.ScheduledAt,//.ToString("dd-MM-yyyy hh:mm:ss tt"),
        //                Status = messageData.Data.Attributes.Status
        //            };
        //            status.IsSuccess = true;
        //        }
        //    }
        //    catch (BEException ex)
        //    {
        //        status.IsSuccess = false;
        //        BEMessageData<string> exceptionMessage = GetExceptionMessage(ex);
        //        status.Message = exceptionMessage.Message;
        //        status.MessageCode = exceptionMessage.MessageCode;
        //    }
        //    catch (Exception ex)
        //    {
        //        status.IsSuccess = false;
        //        BEMessageData<string> exceptionMessage = GetExceptionMessage(ex);
        //        status.Message = exceptionMessage.Message;
        //        status.MessageCode = exceptionMessage.MessageCode;
        //    }
        //    status.Provider = BOSWebServiceSecurity4.Provider;
        //    SaveLog(userName, "", string.Empty, MethodBase.GetCurrentMethod().Name, this.GetType().ToString(), string.Empty, token, status.IsSuccess, status.Message, null);
        //    return status;
        //}

        //[WebMethod(EnableSession = false)]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
        //public APIHub.Models.BEDataPackPurchase VolumeTransfer(string userName, string transferFromMSISDN, string token, string transferToMSISDN, string packCode)
        //{
        //    BEDataPackPurchase dataPackPurchase = new BEDataPackPurchase();
        //    try
        //    {
        //        base.CheckToken(userName, string.Empty, string.Empty, token);
        //        HVC.BusinessEntity.BEServiceUserInfo bEServiceUser = ServiceUserInfo[userName];
        //        if (string.IsNullOrWhiteSpace(transferFromMSISDN))
        //        {
        //            transferFromMSISDN = bEServiceUser.MotherMobileNo;
        //        }
        //        string requestFrom = "880" + transferFromMSISDN;
        //        string requestTo = "880" + transferToMSISDN;
        //        DBSSSubscriber dbssSubscriber = this.dbssAPI.GetSubscriptionInformation(transferFromMSISDN);
        //        DBSSCommandResponse messageData;

        //        messageData = new DBSSWebAPI().TransferDataPack(packCode, requestFrom, requestTo, bEServiceUser.DBSSUserName, dbssSubscriber.SubscriptionID);

        //        if (messageData.Data.Any())
        //        {
        //            dataPackPurchase.IsSuccess = true;
        //            dataPackPurchase.Message = "Data pack transferred successfully";
        //            DBSSCommandResponse.DBSSCommandData successResponse
        //                = messageData.Data.First();
        //            dataPackPurchase.Data = new ProductPurchaseStatus
        //            {
        //                Id = successResponse.Id,
        //                ScheduledAt = successResponse.Attributes.ScheduledAt.ToString("dd-MM-yyyy hh:mm:ss tt"),
        //                Status = successResponse.Attributes.Status
        //            };
        //        }
        //        else
        //        {
        //            dataPackPurchase.IsSuccess = false;
        //            dataPackPurchase.IsAttemptNeeded = true;
        //            dataPackPurchase.Message = messageData.Errors.FirstOrDefault().Detail;
        //            dataPackPurchase.MessageCode = messageData.Errors.FirstOrDefault().Status.ToString();
        //        }
        //    }
        //    catch (BEException ex)
        //    {
        //        dataPackPurchase.IsSuccess = false;
        //        BEMessageData<string> exceptionMessage = GetExceptionMessage(ex);
        //        dataPackPurchase.Message = exceptionMessage.Message;
        //        dataPackPurchase.MessageCode = exceptionMessage.MessageCode;
        //    }
        //    catch (Exception ex)
        //    {
        //        dataPackPurchase.IsSuccess = false;
        //        BEMessageData<string> exceptionMessage = GetExceptionMessage(ex);
        //        dataPackPurchase.Message = exceptionMessage.Message;
        //        dataPackPurchase.MessageCode = exceptionMessage.MessageCode;
        //    }

        //    dataPackPurchase.Provider = BOSWebServiceSecurity4.Provider;
        //    SaveLog(userName, transferFromMSISDN, string.Empty, MethodBase.GetCurrentMethod().Name, this.GetType().ToString(), string.Empty, token, dataPackPurchase.IsSuccess, dataPackPurchase.Message, null);
        //    return dataPackPurchase;
        //}


        //[WebMethod(EnableSession = false)]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
        //public APIHub.Models.DBSSAvailableProducts GetAvailableDataProduct(string userName, string dataProductForMSISDN, string token)
        //{
        //    APIHub.Models.DBSSAvailableProducts availableDataProduct = new APIHub.Models.DBSSAvailableProducts();
        //    try
        //    {
        //        base.CheckToken(userName, string.Empty, string.Empty, token);

        //        double rtt = 0;
        //        string subscriptionId = string.Empty;
        //        if (string.IsNullOrEmpty(dataProductForMSISDN))
        //        {
        //            HVC.BusinessEntity.BEServiceUserInfo bEServiceUser = ServiceUserInfo[userName];
        //            if (bEServiceUser != null && !string.IsNullOrWhiteSpace(bEServiceUser.MotherSubscriptionId))
        //            {
        //                subscriptionId = bEServiceUser.MotherSubscriptionId;
        //            }
        //            else
        //            {
        //                throw new Exception("There are no mother subscription Id set for this user");
        //            }

        //        }
        //        else
        //        {
        //            var dbssSubscriber = this.dbssAPI.GetSubscriptionInformation(dataProductForMSISDN);
        //            if (dbssSubscriber != null && !string.IsNullOrWhiteSpace(dbssSubscriber.SubscriptionID))
        //            {
        //                subscriptionId = dbssSubscriber.SubscriptionID;
        //            }
        //            else
        //            {
        //                throw new Exception("No data product assigned for this MSISDN. Please check the msisdn properly.");
        //            }

        //        }

        //        DBSSProducts product = this.dbssAPI.GetAvailableProductsByFamilyName(subscriptionId, "DATA", out rtt);
        //        availableDataProduct.IsSuccess = true;
        //        availableDataProduct.Data = product;
        //    }
        //    catch (BEException ex)
        //    {
        //        availableDataProduct.IsSuccess = false;
        //        BEMessageData<string> exceptionMessage = GetExceptionMessage(ex);
        //        availableDataProduct.Message = exceptionMessage.Message;
        //        availableDataProduct.MessageCode = exceptionMessage.MessageCode;
        //    }
        //    catch (Exception ex)
        //    {
        //        availableDataProduct.IsSuccess = false;
        //        BEMessageData<string> exceptionMessage = GetExceptionMessage(ex);
        //        availableDataProduct.Message = exceptionMessage.Message;
        //        availableDataProduct.MessageCode = exceptionMessage.MessageCode;
        //    }
        //    availableDataProduct.Provider = BOSWebServiceSecurity4.Provider;
        //    SaveLog(userName, dataProductForMSISDN, string.Empty, MethodBase.GetCurrentMethod().Name, this.GetType().ToString(), string.Empty, token, availableDataProduct.IsSuccess, availableDataProduct.Message, null);
        //    return availableDataProduct;
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="userName"></param>
        ///// <param name="dataProductForMSISDN"></param>
        ///// <param name="token"></param>
        ///// <returns></returns>
        //[WebMethod(EnableSession = false)]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
        //public APIHub.Models.DBSSAvailableProducts GetAvailableBulkDataProduct(string userName, string dataProductForMSISDN, string token)
        //{
        //    APIHub.Models.DBSSAvailableProducts availableDataProduct = new APIHub.Models.DBSSAvailableProducts();

        //    try
        //    {
        //        base.CheckToken(userName, string.Empty, string.Empty, token);

        //        double rtt = 0;

        //        HVC.BusinessEntity.BEServiceUserInfo bEServiceUser = ServiceUserInfo[userName];
        //        if (string.IsNullOrWhiteSpace(dataProductForMSISDN))
        //        {
        //            dataProductForMSISDN = bEServiceUser.MotherMobileNo;
        //        }

        //        DBSSProducts product = this.dbssAPI.GetBulkAvailableProducts(dataProductForMSISDN, bEServiceUser.MotherCompanyName, out rtt);
        //        availableDataProduct.IsSuccess = true;
        //        availableDataProduct.Data = product;
        //    }
        //    catch (BEException ex)
        //    {
        //        availableDataProduct.IsSuccess = false;
        //        BEMessageData<string> exceptionMessage = GetExceptionMessage(ex);
        //        availableDataProduct.Message = exceptionMessage.Message;
        //        availableDataProduct.MessageCode = exceptionMessage.MessageCode;
        //    }
        //    catch (Exception ex)
        //    {
        //        availableDataProduct.IsSuccess = false;
        //        BEMessageData<string> exceptionMessage = GetExceptionMessage(ex);
        //        availableDataProduct.Message = exceptionMessage.Message;
        //        availableDataProduct.MessageCode = exceptionMessage.MessageCode;
        //    }
        //    availableDataProduct.Provider = BOSWebServiceSecurity4.Provider;
        //    SaveLog(userName, dataProductForMSISDN, string.Empty, MethodBase.GetCurrentMethod().Name, this.GetType().ToString(), string.Empty, token, availableDataProduct.IsSuccess, availableDataProduct.Message, null);
        //    return availableDataProduct;
        //}
    }
}