using System;
using System.Diagnostics;
using APIHub2.Helper;
using AutoMapper;
using BusinessEntity.APIHub2;
using BusinessObject;
using LogService.Extension;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Utility.Extensions;

namespace APIHub2
{
    public class Startup
    {
        private readonly Stopwatch watch = new Stopwatch();

        public Startup(IConfiguration configuration)
        {
            watch.Start();
            Configuration = configuration;
            //AppSettings.Load(configuration);
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver
                { NamingStrategy = new CamelCaseNamingStrategy { OverrideSpecifiedNames = false } }
            };
            new AppSettingsHelper(configuration);

            if (AppSettings.IsLoadConfig)
                BOConfiguationData.LoadAllConfigurationData(true, true);

            // AppConfigSettingsLoader.Bind();
            // Application["SessionCounter"] = ((int)Application["SessionCounter"] + 1);
        }

        public IConfiguration Configuration { get; }
        public object AppConfigSettingsLoader { get; }
        public object Application { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers().AddNewtonsoftJson(options =>
            {
                // Use the default property (Pascal) casing
                options.SerializerSettings.ContractResolver = new DefaultContractResolver
                { NamingStrategy = new CamelCaseNamingStrategy { OverrideSpecifiedNames = false } };
                options.SerializerSettings.Converters.Add(new StringEnumConverter());

                //// Configure a custom converter
                //options.SerializerOptions.Converters.Add(new MyCustomJsonConverter());
            });
            BuildHttpClientFactory(services);

            // services.AddApiVersioning();
            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(1, 0);
            });
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Latest)
                .ConfigureApiBehaviorOptions(options =>
                {
                    options.InvalidModelStateResponseFactory = context =>
                    {
                        var problems = new CustomBadRequest(context);
                        return new BadRequestObjectResult(problems);
                    };
                });


            services.AddAutoMapper(typeof(Startup));
            ExceptionExtensions.DBBSErrorLogPath = AppSettings.DBBSErrorLogPath;

            //services.Configure(a =>
            //{
            //    a.InvalidModelStateResponseFactory = context =>
            //    {
            //        var problemDetails = new CustomBadRequest(context);
            //        return new BadRequestObjectResult(problemDetails)
            //        {
            //            ContentTypes = { "application / problem + json", "application / problem + xml" }
            //        };
            //    };
            //});
            //services.AddCustomJwt(Configuration);
            //services.AddSingleton(typeof(LoadAPPSettings));
            //IServiceCollection serviceCollection = services.Configure<AppSettingJSON.Jwtconfiguration>(o => Configuration.GetSection("Jwtconfiguration").Bind(o));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                //app.UseStatusCodePages();
                app.UseStatusCodePagesWithRedirects("/Error/{0}");

            app.UseHttpsRedirection();

            app.UseRouting();
            //This is custom middleware for request response write
            app.UseLoggerMiddleware();

            app.UseAuthorization();

            //app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            watch.Stop();
            AppSettings.ApplicationLoadingTime = watch.ElapsedMilliseconds;
        }
        public void BuildHttpClientFactory(IServiceCollection services)
        {
            services.AddHttpClient("dbssHttpClientFactory", c =>
            {
                c.DefaultRequestHeaders.Add("Accept", "application/vnd.api+json");
            });
            services.AddHttpClient("cmsHttpClientFactory", c =>
            {
                c.DefaultRequestHeaders.Add("Accept", "application/json");
            });
            services.AddHttpClient("googleAuthHttpClientFactory", c =>
            {
                c.DefaultRequestHeaders.Add("Accept", "application/json");
            });
            services.AddHttpClient("googleMDPHttpClientFactory", c =>
            {
                c.DefaultRequestHeaders.Add("Accept", "application/json");
            });
            services.AddHttpClient("googleTokenHttpClientFactory", c =>
            {
                c.DefaultRequestHeaders.Add("Accept", "application/json");
            });
        }
    }
}