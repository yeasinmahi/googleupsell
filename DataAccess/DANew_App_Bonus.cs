using System;
using System.Data.Common;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using SQLFactory;

namespace DataAccess
{
    #region Object of DANew_App_Bonus

    public class DANew_App_Bonus
    {
        #region Destructor

        ~DANew_App_Bonus()
        {
        }

        #endregion

        #region Save

        //public async Task Save(SQLHelper sqlHelper, BENew_App_Bonus new_App_Bonus)
        //{
        //    var isExist = await GetNew_App_Bonus(sqlHelper, new_App_Bonus.BonusType, new_App_Bonus.Msisdn,
        //        new_App_Bonus.Browsedt);
        //    var isModified = false;
        //    if (!isExist && new_App_Bonus.BonusType == "signup")
        //    {
        //        isExist = await GetCustomer(sqlHelper, new_App_Bonus.Msisdn);
        //        if (isExist)
        //        {
        //            new_App_Bonus.BonusType = "login";
        //            isExist = false;
        //            isModified = true;
        //        }
        //    }
        //    //if(new_App_Bonus.IsNew)
        //    //	{
        //    //sql=sqlHelper.MakeSQL("INSERT INTO New_App_Bonus(Id, Msisdn, Browsedt, BrowseSource, BonusType,"
        //    //	+ " IsProcessed) VALUES ($n,"
        //    //	+ " $s, $D, $n, $s, $n)", new_App_Bonus.Id, new_App_Bonus.Msisdn, new_App_Bonus.Browsedt,
        //    //	  new_App_Bonus.BrowseSource, new_App_Bonus.BonusType, new_App_Bonus.IsProcessed);
        //    if (!isExist)
        //    {
        //        var sql = sqlHelper.MakeSQL("INSERT INTO New_App_Bonus(Id, Msisdn, Browsedt, BrowseSource, BonusType,"
        //                                    + " IsProcessed) VALUES (SQ_new_app_bonus_ID.Nextval,"
        //                                    + " $s, $D, $n, $s, $n)", new_App_Bonus.Msisdn, new_App_Bonus.Browsedt,
        //            new_App_Bonus.BrowseSource, new_App_Bonus.BonusType, new_App_Bonus.IsProcessed);

        //        await sqlHelper.ExecuteNonQueryAsync(sql);
        //        if (isModified)
        //        {
        //            sqlHelper.CommitTran(); //Do not remove it, There are a special reason to put it here.
        //            throw new BEHandledException(BEMessageCodes.AlreadyExistAndModified.Status, BEMessageCodes.AlreadyExistAndModified.error,
        //             "The requested data is already exist in our system and modified into sign in request ", "Already exist and modified");
        //        }
        //    }
        //    else
        //    {
        //        throw new BEHandledException(BEMessageCodes.AlreadyExist.Status, BEMessageCodes.AlreadyExist.error,
        //            "The requested data is already exist in our system", "Already exist");

        //    }
        //}


        public async Task<int> Save(SQLHelper sqlHelper, BENew_App_Bonus new_App_Bonus)
        {
            var isModified = false;
            bool isExist;
            if (new_App_Bonus.BonusType == "signup")
            {
                isExist = await GetCustomer(sqlHelper, new_App_Bonus.Msisdn);
                if (isExist)
                {
                    new_App_Bonus.BonusType = "login";
                    isModified = true;
                }
            }

            isExist = await GetNew_App_Bonus(sqlHelper, new_App_Bonus.BonusType, new_App_Bonus.Msisdn,
                new_App_Bonus.Browsedt);
            //if (new_App_Bonus.BonusType == "signup")
            //{
            //    if (isExist)
            //    {
            //        new_App_Bonus.BonusType = "login";
            //        isExist = false;
            //        isModified = true;
            //    }
            //}

            if (!isExist)
            {
                var sql = sqlHelper.MakeSQL("INSERT INTO New_App_Bonus(Id, Msisdn, Browsedt, BrowseSource, BonusType,"
                                            + " IsProcessed) VALUES (SQ_new_app_bonus_ID.Nextval,"
                                            + " $s, $D, $n, $s, $n)", new_App_Bonus.Msisdn, new_App_Bonus.Browsedt,
                    new_App_Bonus.BrowseSource, new_App_Bonus.BonusType, new_App_Bonus.IsProcessed);

                await sqlHelper.ExecuteNonQueryAsync(sql);
                if (isModified)
                {
                    return 3;
                }
                return 1;
            }
            else
            {
                return 2;


            }
        }

        #endregion

        //	#region Delete
        //	public async Task Delete(SQLHelper sqlHelper, , int deletedBy)
        //	{
        //		try
        //		{
        //			string sql=string.Empty;
        //			sql = sqlHelper.MakeSQL("UPDATE New_App_Bonus SET IsDeleted=$b, DeletedBy=$n , DeletedDate=sysdate  WHERE ", true, deletedBy, );
        //			await sqlHelper.ExecuteNonQueryAsync(sql);
        //		}
        //		catch(Exception ex)
        //		{
        //			throw new Exception(ex.Message);
        //		}
        //	}
        //	#endregion

        //	#region Get
        public async Task<bool> GetNew_App_Bonus(SQLHelper sqlHelper, string bonustype, string msisdn,
            DateTime browsedt)
        {
            var sql = string.Empty;
            //BENew_App_Bonuss new_App_Bonuss = new BENew_App_Bonuss();

            var totalCount = 0;
            try
            {
                if (bonustype == "login")
                    sql = sqlHelper.MakeSQL(
                        "SELECT count(Id) FROM New_App_Bonus WHERE msisdn  = $s and bonustype = $s and Browsedt = $D",
                        msisdn, bonustype, browsedt);
                else if (bonustype == "signup")
                    sql = sqlHelper.MakeSQL(
                        "SELECT Count(Id) FROM New_App_Bonus WHERE msisdn  = $s and bonustype = $s ", msisdn,
                        bonustype);

                var objTotalCount = await sqlHelper.ExecuteScalarAsync(sql);

                if (!(objTotalCount == DBNull.Value || objTotalCount == null))
                    totalCount = Convert.ToInt32(objTotalCount);

                //if (bonustype == "login")
                //{
                //	sql = sqlHelper.MakeSQL("SELECT Id FROM New_App_Bonus WHERE msisdn  = $s and bonustype = $s and Browsedt = $D", msisdn, bonustype, browsedt);
                //}
                //else if (bonustype == "signup")
                //{
                //	sql = sqlHelper.MakeSQL("SELECT Id FROM New_App_Bonus WHERE msisdn  = $s and bonustype = $s ", msisdn, bonustype);
                //}

                //DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
                //AddToCollection(new_App_Bonuss, reader);
                //reader.Close();
                //totalCount=new_App_Bonuss.Count;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (totalCount > 0)
                return true;
            return false;
        }
        public async Task<bool> GetCustomer(SQLHelper sqlHelper, string msisdn)
        {
            var sql = sqlHelper.MakeSQL("select 1 from hvcdb.tblcustomer where msisdn = $s ", msisdn);

            var totalCount = 0;
            try
            {
                var objTotalCount = await sqlHelper.ExecuteScalarAsync(sql);

                if (!(objTotalCount == DBNull.Value || objTotalCount == null))
                    totalCount = Convert.ToInt32(objTotalCount);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (totalCount > 0)
                return true;
            return false;
        }

        //	public BENew_App_Bonuss GetNew_App_Bonuss(SQLHelper sqlHelper, bool activeOnly)
        //	{
        //		string sql = string.Empty;
        //		string whereClause = string.Empty;
        //		BENew_App_Bonuss new_App_Bonuss = new BENew_App_Bonuss();

        //		try
        //		{
        //			if (activeOnly)
        //				whereClause += sqlHelper.MakeSQL(" AND IsActive = $b", activeOnly);
        //			sql = sqlHelper.MakeSQL("SELECT Id, Msisdn, Browsedt, CreateDate, BrowseSource, BonusType, IsProcessed, ProcesseDate,  CreatedBy,"
        //				+ "CreatedDate, UpdatedBy, UpdatedDate, IsDeleted, DeletedBy, DeletedDate, IsActive, SerialNo FROM New_App_Bonus WHERE IsDeleted=$b $q",
        //				false, whereClause);

        //			DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
        //			AddToCollection(new_App_Bonuss, reader);
        //			reader.Close();
        //		}
        //		catch(Exception ex)
        //		{
        //			throw new Exception(ex.Message);
        //		}
        //		return new_App_Bonuss;
        //	}

        //	public BENew_App_Bonuss GetNew_App_Bonuss(SQLHelper sqlHelper , BENew_App_Bonus searchCriteria, 
        //int pageIndex, int numberOfRecordsPerPage, string orderByQuery, out int totalCount)
        //	{
        //		string sql = string.Empty;
        //		string countSQL = string.Empty;
        //		string whereClause = string.Empty;
        //		BENew_App_Bonuss new_App_Bonuss = new BENew_App_Bonuss();

        //		try
        //		{
        //			sql = sqlHelper.MakeSQL("SELECT Id, Msisdn, Browsedt, CreateDate, BrowseSource, BonusType, IsProcessed, ProcesseDate,  CreatedBy,"
        //				+ "CreatedDate, UpdatedBy, UpdatedDate, IsDeleted, DeletedBy, DeletedDate, IsActive, SerialNo FROM New_App_Bonus WHERE IsDeleted=$b $q",
        //				false, whereClause);
        //			totalCount = 0;
        //			countSQL = "SELECT COUNT(1) FROM (" + sql + ") CountSQL";
        //			object objTotalCount = await sqlHelper.ExecuteScalarAsync(countSQL);

        //			if (!(objTotalCount == DBNull.Value || objTotalCount == null))
        //				totalCount = Convert.ToInt32(objTotalCount);

        //			sql= DACommon.AddPagingQuery(sql, pageIndex, numberOfRecordsPerPage, orderByQuery);


        //			DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
        //			AddToCollection(new_App_Bonuss, reader);
        //			reader.Close();
        //		}
        //		catch(Exception ex)
        //		{
        //			throw new Exception(ex.Message);
        //		}
        //		return new_App_Bonuss;
        //	}

        private BENew_App_Bonuss AddToCollection(BENew_App_Bonuss new_App_Bonuss, DbDataReader reader)
        {
            try
            {
                var nullHandler = new NULLHandler(reader);
                while (reader.Read()) new_App_Bonuss.Add(PreaperObject(nullHandler));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return new_App_Bonuss;
        }

        private BENew_App_Bonus PreaperObject(NULLHandler nullHandler)
        {
            var new_App_Bonus = new BENew_App_Bonus();

            try
            {
                //new_App_Bonus.IsNew = false;
                new_App_Bonus.Id = nullHandler.GetInt("Id");
                //new_App_Bonus.Msisdn = nullHandler.GetString("Msisdn");
                //new_App_Bonus.Browsedt = nullHandler.GetDateTime("Browsedt");
                //new_App_Bonus.CreateDate = nullHandler.GetDateTime("CreateDate");
                //new_App_Bonus.BrowseSource = nullHandler.GetInt("BrowseSource");
                //new_App_Bonus.BonusType = nullHandler.GetString("BonusType");
                //new_App_Bonus.IsProcessed = nullHandler.GetInt("IsProcessed");
                //new_App_Bonus.ProcesseDate = nullHandler.GetDateTime("ProcesseDate");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return new_App_Bonus;
        }

        #region Constructor

        #endregion

        //	#endregion
    }

    #endregion
}