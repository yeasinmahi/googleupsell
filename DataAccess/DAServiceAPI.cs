using System;
using System.Data.Common;
using System.Threading.Tasks;
using BusinessEntity;
using SQLFactory;

namespace DataAccess
{
    #region Object of DAServiceAPI

    public class DAServiceAPI
    {
        #region Destructor

        ~DAServiceAPI()
        {
        }

        #endregion

        #region Save

        public async Task Save(SQLHelper sqlHelper, BEServiceAPI serviceAPI)
        {
            try
            {
                var sql = string.Empty;

                if (serviceAPI.IsNew)
                    sql = sqlHelper.MakeSQL(
                        "INSERT INTO ServiceAPI(ID, ApiName, ServiceID, IsDeleted, IsActive, CreatedBy,"
                        + " CreatedDate, SerialNo) VALUES ($n, $s, $n, $b, $b, $n, $D, $n)", serviceAPI.ID,
                        serviceAPI.ApiName,
                        serviceAPI.ServiceID, serviceAPI.IsDeleted, serviceAPI.IsActive, serviceAPI.CreatedBy,
                        serviceAPI.CreatedDate,
                        serviceAPI.SerialNo);
                else
                    sql = sqlHelper.MakeSQL(
                        "UPDATE ServiceAPI SET ApiName = $s, ServiceID = $n, UpdatedDate = $D, UpdatedBy = $n,"
                        + " IsActive=$b"
                        + " WHERE ID=$n ", serviceAPI.ApiName, serviceAPI.ServiceID, serviceAPI.UpdatedDate,
                        serviceAPI.UpdatedBy, serviceAPI.IsActive,
                        serviceAPI.ID);

                await sqlHelper.ExecuteNonQueryAsync(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Delete

        public async Task Delete(SQLHelper sqlHelper, int iD, int deletedBy)
        {
            try
            {
                var sql = string.Empty;
                sql = sqlHelper.MakeSQL(
                    "UPDATE ServiceAPI SET IsDeleted=$b, DeletedBy=$n , DeletedDate=sysdate  WHERE ID=$n ", true,
                    deletedBy, iD);
                await sqlHelper.ExecuteNonQueryAsync(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Constructor

        #endregion

        #region Get

        public async Task<BEServiceAPI> GetServiceAPI(SQLHelper sqlHelper, int iD)
        {
            var sql = string.Empty;
            var serviceAPIs = new BEServiceAPIs();

            try
            {
                sql = sqlHelper.MakeSQL(
                    "SELECT ID, ApiName, ServiceID,  CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, IsDeleted, DeletedBy, DeletedDate, IsActive, SerialNo FROM ServiceAPI WHERE ID=$n ",
                    iD);
                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(serviceAPIs, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (serviceAPIs.Count > 0)
                return serviceAPIs[0];
            return new BEServiceAPI();
        }

        public async Task<BEServiceAPIs> GetServiceAPIs(SQLHelper sqlHelper, bool activeOnly)
        {
            var sql = string.Empty;
            var whereClause = string.Empty;
            var serviceAPIs = new BEServiceAPIs();

            try
            {
                if (activeOnly)
                    whereClause += sqlHelper.MakeSQL(" AND IsActive = $b", activeOnly);
                sql = sqlHelper.MakeSQL(
                    "SELECT ID, ApiName, ServiceID,  CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, IsDeleted, DeletedBy, DeletedDate, IsActive, SerialNo FROM ServiceAPI WHERE IsDeleted=$b $q",
                    false, whereClause);
                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(serviceAPIs, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return serviceAPIs;
        }

        public async Task<BEServiceAPIs> GetServiceAPIs(SQLHelper sqlHelper, BEServiceAPI searchCriteria,
            int pageIndex, int numberOfRecordsPerPage, string orderByQuery)
        {
            var sql = string.Empty;
            var countSQL = string.Empty;
            var whereClause = string.Empty;
            var serviceAPIs = new BEServiceAPIs();

            try
            {
                sql = sqlHelper.MakeSQL(
                    "SELECT ID, ApiName, ServiceID,  CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, IsDeleted, DeletedBy, DeletedDate, IsActive, SerialNo FROM ServiceAPI WHERE IsDeleted=$b $q",
                    false, whereClause);
                //totalCount = 0;
                countSQL = "SELECT COUNT(1) FROM (" + sql + ") CountSQL";
                var objTotalCount = await sqlHelper.ExecuteScalarAsync(countSQL);

                //if (!(objTotalCount == DBNull.Value || objTotalCount == null))
                //	totalCount = Convert.ToInt32(objTotalCount);

                //sql= DACommon.AddPagingQuery(sql, pageIndex, numberOfRecordsPerPage, orderByQuery);


                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(serviceAPIs, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return serviceAPIs;
        }

        private BEServiceAPIs AddToCollection(BEServiceAPIs serviceAPIs, DbDataReader reader)
        {
            try
            {
                var nullHandler = new NULLHandler(reader);
                while (reader.Read()) serviceAPIs.Add(PreaperObject(nullHandler));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return serviceAPIs;
        }

        private BEServiceAPI PreaperObject(NULLHandler nullHandler)
        {
            var serviceAPI = new BEServiceAPI();

            try
            {
                serviceAPI.IsNew = false;
                serviceAPI.ID = nullHandler.GetInt("ID");
                serviceAPI.ApiName = nullHandler.GetString("ApiName");
                serviceAPI.ServiceID = nullHandler.GetInt("ServiceID");
                serviceAPI.IsDeleted = nullHandler.GetBoolean("IsDeleted");
                serviceAPI.IsActive = nullHandler.GetBoolean("IsActive");
                serviceAPI.CreatedBy = nullHandler.GetString("CreatedBy");
                serviceAPI.CreatedDate = nullHandler.GetDateTime("CreatedDate");
                serviceAPI.UpdatedBy = nullHandler.GetString("UpdatedBy");
                serviceAPI.UpdatedDate = nullHandler.GetDateTime("UpdatedDate");
                serviceAPI.DeletedBy = nullHandler.GetString("DeletedBy");
                serviceAPI.DeletedDate = nullHandler.GetDateTime("DeletedDate");
                serviceAPI.SerialNo = nullHandler.GetInt("SerialNo");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return serviceAPI;
        }

        #endregion
    }

    #endregion
}