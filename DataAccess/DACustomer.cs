﻿using System;
using System.Data.Common;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using SQLFactory;

namespace DataAccess
{
    public class DACustomer
    {
        ~DACustomer()
        {
        }

        //public bool IsValidPassword(SQLHelper sqlHelper, string MSISDN, string password)
        //{
        //    string sql = string.Empty;
        //    bool isValidCustomer = false;
        //    try
        //    {
        //        //SELECT SubNO FROM  CUSTOMER_PASS_INFO CP WHERE CP.SubNO = '1911310701' AND CP.CUSTPASS='1'
        //        sql = sqlHelper.MakeSQL("SELECT SubNO FROM  CUSTOMER_PASS_INFO CP WHERE CP.SubNO = $s AND CP.CUSTPASS=$s", MSISDN, password);

        //        object obj = await sqlHelper.ExecuteScalarAsync(sql);

        //        if (obj != null && obj != DBNull.Value)
        //            isValidCustomer = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }


        //    return isValidCustomer;
        //}

        public async Task<bool> IsValidPassword(SQLHelper sqlHelper, string MSISDN, string password,
            int wrongAttemptLimit = 5, int unlockTimeMin = 15)
        {
            var sql = string.Empty;
            var isValidCustomer = false;
            try
            {
                //SELECT SubNO FROM  CUSTOMER_PASS_INFO CP WHERE CP.SubNO = '1911310701' AND CP.CUSTPASS='1'

                //  sql = sqlHelper.MakeSQL("SELECT CUSTPASS FROM  CUSTOMER_PASS_INFO CP WHERE CP.SubNO = $s", MSISDN);
                //sql = sqlHelper.MakeSQL("SELECT CP.CUSTPASS FROM  CUSTOMER_PASS_INFO CP WHERE CP.SubNO = $s AND (CP.CUSTPASS=$s OR UPPER(fn_GetSHA1Text(CP.CUSTPASS))=UPPER($s))", MSISDN, password, password);
                sql = sqlHelper.MakeSQL(
                    "SELECT CP.CUSTPASS FROM  CUSTOMER_PASS_INFO CP WHERE CP.SubNO = $s AND (UPPER(fn_GetSHA1Text(CP.CUSTPASS))=UPPER($s))",
                    MSISDN, password, password);


                var obj = await sqlHelper.ExecuteScalarAsync(sql);

                if (obj != null && obj != DBNull.Value)
                    isValidCustomer = true;

                //if (obj != null && obj != DBNull.Value)
                //{
                //    string pass = string.Empty;

                //    if (obj != null && obj != DBNull.Value)
                //        pass = Convert.ToString(obj);

                //    if(//pass == password || 
                //        HVC.Common.SHA1Hash.IsSHA1HashEqual(password, pass))

                //    isValidCustomer = true;
                //}
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return isValidCustomer;
        }

        public async Task<bool> IsValidCustomerSatus(SQLHelper sqlHelper, string MSISDN)
        {
            var sql = string.Empty;
            var isValidCustomer = false;
            object val = null;
            try
            {
                sql = sqlHelper.MakeSQL("select  fn_isvalid_customer($s) result from dual", MSISDN);
                val = null;
                val = await sqlHelper.ExecuteScalarAsync(sql);
                if (Convert.ToInt32(val) != 0) isValidCustomer = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return isValidCustomer;
        }

        public async Task<bool> IsValidBLCustomer(SQLHelper sqlHelper, string MSISDN)
        {
            var sql = string.Empty;
            var isValidCustomer = false;
            try
            {
                sql = sqlHelper.MakeSQL("SELECT SubNO FROM  dwh_pull.dwh_customer_product CP WHERE CP.SubNO = $s",
                    MSISDN);

                var obj = await sqlHelper.ExecuteScalarAsync(sql);

                if (obj != null && obj != DBNull.Value)
                    isValidCustomer = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return isValidCustomer;
        }

        public async Task<BECustomer> GetCustomer(SQLHelper sqlHelper, string MSISDN, bool unregisteredCustomer)
        {
            var sql = string.Empty;
            var customers = new BECustomers();
            try
            {
                if (unregisteredCustomer)
                {
                    //                    sql = @"SELECT NVL(CP.CUSTREGISTEREDONWEBYN, 0) AS CustomerRegisteredOnWeb,
                    //                                        CP.CUSTPASS AS CustomerPassword ,CP.CONTRNO AS ContractNumber,C.MSISDN AS MSISDN, 
                    //                                        C.*, G.COMMONCONFIGNAME AS GENDERTEXT, M.COMMONCONFIGNAME AS MARITALSTATUSTEXT, 
                    //                                        S.COMMONCONFIGNAME AS SALUTATIONTEXT, R.COMMONCONFIGNAME AS RELIGIONTEXT 
                    //                                        FROM TBLCUSTOMER C
                    //                                         LEFT JOIN CUSTOMER_PASS_INFO CP ON C.MSISDN = CP.SUBNO 
                    //                                         LEFT JOIN TBLCOMMONCONFIG G ON G.COMMONCONFIGID = C.GENDER
                    //                                         LEFT JOIN TBLCOMMONCONFIG M ON M.COMMONCONFIGID = C.MARITALSTATUS
                    //                                         LEFT JOIN TBLCOMMONCONFIG S ON S.COMMONCONFIGID = C.SALUTATION
                    //                                         LEFT JOIN TBLCOMMONCONFIG R ON R.COMMONCONFIGID = C.RELIGION";

                    //                    sql = @"SELECT NVL(CP.CUSTREGISTEREDONWEBYN, 0) AS CustomerRegisteredOnWeb,
                    //                                        CP.CUSTPASS AS CustomerPassword ,CP.CONTRNO AS ContractNumber,C.MSISDN AS MSISDN, C.MyReferralCode,C.ReferrerCode,
                    //                                        C.customertype, C.lastlogin, C.homecontentindex, C.homecontenttime, C.isblocked, C.lockedreason, C.lastemailtext,
                    //                                        C.ismustchangepassword, C.secretanswer1, C.secretanswer2, C.secretanswer3, C.secretanswer4, C.lastsecuritykey, C.wrongattempt,
                    //                                        C.customerid, C.username, C.companyname, C.emailaddress, C.alternativecontactnumber, C.birthdate, C.gender, C.maritalstatus,
                    //                                        C.salutation, C.moreaboutyou, C.spousebirthday, C.spousecontactnumber, C.spouseemail, C.marriageanniversary, C.religion,
                    //                                        C.freeemailalertservice, C.freeenewsletter, C.msisdn, C.virtualcontactno, C.ishvc, C.connectiontype, C.isemailverified,
                    //                                        C.verificationcode, C.profession, C.hasotherinterest, C.otherinterest, C.CreatedBy, C.CreatedDate, C.UpdatedBy, C.UpdatedDate,
                    //                                        C.IsDeleted, G.COMMONCONFIGNAME AS GENDERTEXT, M.COMMONCONFIGNAME AS MARITALSTATUSTEXT, 
                    //                                        S.COMMONCONFIGNAME AS SALUTATIONTEXT, R.COMMONCONFIGNAME AS RELIGIONTEXT 
                    //                                        FROM TBLCUSTOMER C
                    //                                         LEFT JOIN CUSTOMER_PASS_INFO CP ON C.MSISDN = CP.SUBNO 
                    //                                         LEFT JOIN TBLCOMMONCONFIG G ON G.COMMONCONFIGID = C.GENDER
                    //                                         LEFT JOIN TBLCOMMONCONFIG M ON M.COMMONCONFIGID = C.MARITALSTATUS
                    //                                         LEFT JOIN TBLCOMMONCONFIG S ON S.COMMONCONFIGID = C.SALUTATION
                    //                                         LEFT JOIN TBLCOMMONCONFIG R ON R.COMMONCONFIGID = C.RELIGION";

                    sql = @"SELECT NVL(CP.CUSTREGISTEREDONWEBYN, 0) AS CustomerRegisteredOnWeb,
                                        CP.CUSTPASS AS CustomerPassword ,CP.CONTRNO AS ContractNumber,C.MSISDN AS MSISDN, C.MyReferralCode,C.ReferrerCode,
                                        C.customertype, C.lastlogin, C.homecontentindex, C.homecontenttime, C.isblocked, C.lockedreason, C.lastemailtext,
                                        C.ismustchangepassword, C.secretanswer1, C.secretanswer2, C.secretanswer3, C.secretanswer4, C.lastsecuritykey, C.wrongattempt,
                                        C.customerid, C.username, C.companyname, C.emailaddress, C.alternativecontactnumber, C.birthdate, C.gender, C.maritalstatus,
                                        C.salutation, C.moreaboutyou, C.spousebirthday, C.spousecontactnumber, C.spouseemail, C.marriageanniversary, C.religion,
                                        C.freeemailalertservice, C.freeenewsletter, C.msisdn, C.virtualcontactno, C.ishvc, C.connectiontype, C.isemailverified,
                                        C.verificationcode, C.profession, C.hasotherinterest, C.otherinterest, C.CreatedBy, C.CreatedDate, C.UpdatedBy, C.UpdatedDate,
                                        C.IsDeleted 
                                        FROM TBLCUSTOMER C
                                         LEFT JOIN CUSTOMER_PASS_INFO CP ON C.MSISDN = CP.SUBNO";

                    sql = sqlHelper.MakeSQL(sql + " WHERE C.MSISDN = $s", MSISDN);
                }
                else
                {
                    //                    sql = @"SELECT CP.CUSTREGISTEREDONWEBYN AS CustomerRegisteredOnWeb,
                    //                                        CP.CUSTPASS AS CustomerPassword ,CP.CONTRNO AS ContractNumber,CP.SUBNO AS MSISDN, C.MyReferralCode,C.ReferrerCode,
                    //                                        C.customertype, C.lastlogin, C.homecontentindex, C.homecontenttime, C.isblocked, C.lockedreason, C.lastemailtext,
                    //          C.ismustchangepassword, C.secretanswer1, C.secretanswer2, C.secretanswer3, C.secretanswer4, C.lastsecuritykey, C.wrongattempt,
                    //          C.customerid, C.username, C.companyname, C.emailaddress, C.alternativecontactnumber, C.alternativecontactnumber AlternativeContactNumberHome, C.birthdate, C.gender, C.maritalstatus,
                    //          C.salutation, C.moreaboutyou, C.spousebirthday, C.spousecontactnumber, C.spouseemail, C.marriageanniversary, C.religion,
                    //          C.freeemailalertservice, C.freeenewsletter, C.msisdn, C.virtualcontactno, C.ishvc, C.connectiontype, C.isemailverified,
                    //          C.verificationcode, C.profession, C.hasotherinterest, C.otherinterest, C.CreatedBy, C.CreatedDate, C.UpdatedBy, C.UpdatedDate,
                    //          C.IsDeleted, G.COMMONCONFIGNAME AS GENDERTEXT, M.COMMONCONFIGNAME AS MARITALSTATUSTEXT, 
                    //                                        S.COMMONCONFIGNAME AS SALUTATIONTEXT, R.COMMONCONFIGNAME AS RELIGIONTEXT FROM CUSTOMER_PASS_INFO CP
                    //                                         LEFT JOIN TBLCUSTOMER C ON C.MSISDN = CP.SUBNO
                    //                                         LEFT JOIN TBLCOMMONCONFIG G ON G.COMMONCONFIGID = C.GENDER
                    //                                         LEFT JOIN TBLCOMMONCONFIG M ON M.COMMONCONFIGID = C.MARITALSTATUS
                    //                                         LEFT JOIN TBLCOMMONCONFIG S ON S.COMMONCONFIGID = C.SALUTATION
                    //                                         LEFT JOIN TBLCOMMONCONFIG R ON R.COMMONCONFIGID = C.RELIGION";

                    sql = @"SELECT CP.CUSTREGISTEREDONWEBYN AS CustomerRegisteredOnWeb,
                                        CP.CUSTPASS AS CustomerPassword ,CP.CONTRNO AS ContractNumber,CP.SUBNO AS MSISDN, C.MyReferralCode,C.ReferrerCode,
                                        C.customertype, C.lastlogin, C.homecontentindex, C.homecontenttime, C.isblocked, C.lockedreason, C.lastemailtext,
          C.ismustchangepassword, C.secretanswer1, C.secretanswer2, C.secretanswer3, C.secretanswer4, C.lastsecuritykey, C.wrongattempt,
          C.customerid, C.username, C.companyname, C.emailaddress, C.alternativecontactnumber, C.alternativecontactnumber AlternativeContactNumberHome, C.birthdate, C.gender, C.maritalstatus,
          C.salutation, C.moreaboutyou, C.spousebirthday, C.spousecontactnumber, C.spouseemail, C.marriageanniversary, C.religion,
          C.freeemailalertservice, C.freeenewsletter, C.msisdn, C.virtualcontactno, C.ishvc, C.connectiontype, C.isemailverified,
          C.verificationcode, C.profession, C.hasotherinterest, C.otherinterest, C.CreatedBy, C.CreatedDate, C.UpdatedBy, C.UpdatedDate,
          C.IsDeleted FROM CUSTOMER_PASS_INFO CP
                                         LEFT JOIN TBLCUSTOMER C ON C.MSISDN = CP.SUBNO";

                    sql = sqlHelper.MakeSQL(sql + " WHERE CP.SUBNO = $s", MSISDN);
                }

                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(customers, reader);
                reader.Close();

                //foreach (BECustomer cust in customers)
                //{
                //    DAInterest daInterest = new DAInterest();
                //    cust.Interests = daInterest.GetInterestsByCustomerId(sqlHelper, cust.CustomerId);
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (customers.Count > 0)
                return customers[0];
            return null;
        }

        private BECustomers AddToCollection(BECustomers Customers, DbDataReader reader)
        {
            //while (reader.Read())
            //{
            //    BECustomer customer = new BECustomer();
            //    //customer = (BECustomer)DBUtility.MapItem(customer, reader);
            //    //customer = (BECustomer)PreaperObject(customer, reader);

            //    Customers.Add(PreaperObject(reader));
            //}

            var nullHandler = new NULLHandler(reader);
            while (reader.Read()) Customers.Add(PreaperObject(nullHandler));
            return Customers;
        }

        private BECustomer PreaperObject(NULLHandler nullHandler)
        {
            var customer = new BECustomer();

            try
            {
                customer.IsNew = false;
                customer.CustomerType = nullHandler.GetInt("CustomerType");
                customer.LastLogin = nullHandler.GetDateTime("LastLogin");
                customer.HomeContentIndex = nullHandler.GetInt("HomeContentIndex");
                //customer.HomeContentTime = new TimeSpan(nullHandler.GetDateTime("HomeContentTime");
                customer.IsBlocked = nullHandler.GetBoolean("IsBlocked");
                customer.LockedReason = nullHandler.GetString("LockedReason");
                customer.LastEmailText = nullHandler.GetString("LastEmailText");
                customer.IsMustChangePassword = nullHandler.GetBoolean("IsMustChangePassword");
                customer.SecretAnswer1 = nullHandler.GetString("SecretAnswer1");
                customer.SecretAnswer2 = nullHandler.GetString("SecretAnswer2");
                customer.SecretAnswer3 = nullHandler.GetString("SecretAnswer3");
                customer.SecretAnswer4 = nullHandler.GetString("SecretAnswer4");
                customer.LastSecurityKey = nullHandler.GetString("LastSecurityKey");
                customer.WrongAttempt = nullHandler.GetInt("WrongAttempt");
                customer.CustomerId = nullHandler.GetInt("CustomerId");
                customer.UserName = nullHandler.GetString("UserName");
                customer.CompanyName = nullHandler.GetString("CompanyName");
                customer.EmailAddress = nullHandler.GetString("EmailAddress");
                customer.AlternativeContactNumber = nullHandler.GetString("AlternativeContactNumber");
                customer.BirthDate = nullHandler.GetDateTime("BirthDate");
                customer.Gender = nullHandler.GetInt("Gender");
                customer.MaritalStatus = nullHandler.GetInt("MaritalStatus");
                customer.Salutation = nullHandler.GetInt("Salutation");
                customer.MoreAboutYou = nullHandler.GetString("MoreAboutYou");
                customer.SpouseBirthDay = nullHandler.GetDateTime("SpouseBirthDay");
                customer.SpouseContactNumber = nullHandler.GetString("SpouseContactNumber");
                customer.SpouseEmail = nullHandler.GetString("SpouseEmail");
                customer.MarriageAnniversary = nullHandler.GetDateTime("MarriageAnniversary");
                customer.Religion = nullHandler.GetInt("Religion");
                customer.FreeEmailAlertService = nullHandler.GetBoolean("FreeEmailAlertService");
                customer.FreeENewsLetter = nullHandler.GetBoolean("FreeENewsLetter");
                customer.MSISDN = nullHandler.GetString("MSISDN");
                customer.VirtualContactNo = nullHandler.GetString("VirtualContactNo");
                customer.IsHVC = nullHandler.GetBoolean("IsHVC");
                customer.ConnectionType = nullHandler.GetString("ConnectionType");
                customer.IsEmailVerified = nullHandler.GetBoolean("IsEmailVerified");
                customer.VerificationCode = nullHandler.GetString("VerificationCode");
                customer.Profession = nullHandler.GetString("Profession");
                customer.HasOtherInterest = nullHandler.GetBoolean("HasOtherInterest");
                customer.OtherInterest = nullHandler.GetString("OtherInterest");
                customer.IsDeleted = nullHandler.GetBoolean("IsDeleted");
                //customer.IsActive = nullHandler.GetBoolean("IsActive");
                customer.CreatedBy = nullHandler.GetString("CreatedBy");
                customer.CreatedDate = nullHandler.GetDateTime("CreatedDate");
                customer.UpdatedBy = nullHandler.GetString("UpdatedBy");
                customer.UpdatedDate = nullHandler.GetDateTime("UpdatedDate");
                //customer.GenderText = nullHandler.GetString("GenderText");
                //customer.MaritalStatusText = nullHandler.GetString("MaritalStatusText");
                //customer.SalutationText = nullHandler.GetString("SalutationText");
                //customer.ReligionText = nullHandler.GetString("ReligionText");
                //customer.ReligionText = nullHandler.GetString("ReligionText");
                //customer.ReligionText = nullHandler.GetString("ReligionText");
                customer.CustomerRegisteredOnWeb = nullHandler.GetBoolean("CustomerRegisteredOnWeb");
                customer.CustomerPassword = nullHandler.GetString("CustomerPassword");
                customer.ContractNumber = nullHandler.GetString("ContractNumber");
                //customer.AlternativeContactNumberHome = nullHandler.GetString("AlternativeContactNumberHome");
                customer.Profession = nullHandler.GetString("Profession");

                //coupon referral
                customer.MyReferralCode = nullHandler.GetString("MyReferralCode");
                customer.ReferrerCode = nullHandler.GetString("ReferrerCode");

                if (string.IsNullOrEmpty(customer.EmailAddress)) customer.EmailAddress = "";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return customer;
        }
    }
}