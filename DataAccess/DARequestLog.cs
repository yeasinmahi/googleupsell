﻿using System;
using System.Threading.Tasks;
using BusinessEntity.ConnectorModels.CMS;
using SQLFactory;

namespace DataAccess
{
    public class DArequestlog
    {
        #region Destructor

        ~DArequestlog()
        {
        }

        #endregion

        #region Save

        public async Task Save(SQLHelper sqlHelper, CmsRequestLog requestLog)
        {
            try
            {
                var sql = string.Empty;
                sql = sqlHelper.MakeSQL(
                    "INSERT INTO tblrequestlog(id, requestName,msisdn,requestdate,responsedate,rtt,error,url) VALUES (SEQ_tblRequestLog_ID.nextval, $s, $s, $D, $D, $n, $s, $s)",
                    requestLog.Name,requestLog.Msisdn,requestLog.RequestDate,requestLog.ResponseDate,requestLog.Rtt,requestLog.Error, requestLog.Url);
                await sqlHelper.ExecuteNonQueryAsync(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Constructor

        #endregion
    }
}