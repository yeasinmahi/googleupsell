﻿using System;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using SQLFactory;

namespace DataAccess
{
    #region Object of DARequestSummary

    public class DARequestSummary
    {
        #region Destructor

        ~DARequestSummary()
        {
        }

        #endregion

        #region Save

        public async Task Save(SQLHelper sqlHelper, BERequestSummary requestSummary)
        {
            try
            {
                var sql = string.Empty;

                sql = sqlHelper.MakeSQL(
                    "INSERT INTO tblRequestSummary(ID, RequestName, TotalRequest, MinimumTime, MaxTime, AverageTime, RecordDate, ErrorCount)"
                    + " VALUES (SQ_RequestSummaryID.nextval, $s, $n, $n, $n, $n, $D, $n)", requestSummary.RequestName,
                    requestSummary.TotalRequest,
                    requestSummary.MinimumTime, requestSummary.MaxTime, requestSummary.AverageTime,
                    requestSummary.RecordDate, requestSummary.ErrorCount);


                //if(aequestSummary.IsNew)
                //    {
                //        sql=sqlHelper.MakeSQL("INSERT INTO RequestSummary(RequestSummaryID, Code, RequestSummaryName, RequestSummaryMessage, IsRequestSummary, IsDeleted, IsActive,"
                //            + " CreatedBy, CreatedDate, Serial) VALUES ($n, $s, $s, $s, $s, $b, $b, $n, $D, $n)", aequestSummary.RequestSummaryID,
                //             aequestSummary.Code, aequestSummary.RequestSummaryName, aequestSummary.RequestSummaryMessage, aequestSummary.IsRequestSummary, aequestSummary.IsDeleted, aequestSummary.IsActive,
                //             aequestSummary.CreatedBy, aequestSummary.CreatedDate, aequestSummary.Serial );

                //    }
                //else
                //    {
                //        sql = sqlHelper.MakeSQL("UPDATE RequestSummary SET Code = $s, RequestSummaryName = $s, RequestSummaryMessage = $s, IsRequestSummary = $s,"
                //            + " UpdatedDate = $D, UpdatedBy = $n"
                //            + " WHERE RequestSummaryID=$n ", aequestSummary.Code, aequestSummary.RequestSummaryName, aequestSummary.RequestSummaryMessage, aequestSummary.IsRequestSummary, aequestSummary.UpdatedDate, aequestSummary.UpdatedBy, aequestSummary.RequestSummaryID);
                //    }

                await sqlHelper.ExecuteNonQueryAsync(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Delete

        public async Task Delete(SQLHelper sqlHelper, int aequestSummaryID, int deletedBy)
        {
            try
            {
                var sql = string.Empty;
                sql = sqlHelper.MakeSQL(
                    "UPDATE RequestSummary SET IsDeleted=$b, DeletedBy=$n , DeletedDate=sysdate  WHERE ID=$n ", true,
                    deletedBy, aequestSummaryID);
                await sqlHelper.ExecuteNonQueryAsync(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Constructor

        #endregion

        //       #region Get
        //       public BERequestSummary GetRequestSummary(SQLHelper sqlHelper, int aequestSummaryID )
        //       {
        //           string sql = string.Empty;
        //           BERequestSummarys aequestSummarys = new BERequestSummarys();

        //           try
        //           {
        //               sql = sqlHelper.MakeSQL("SELECT RequestSummaryID, Code, RequestSummaryName, RequestSummaryMessage, IsRequestSummary, , CreatedBy, CreatedDate, UpdatedBy, UpdatedDate,"
        //                   + "IsDeleted, DeletedBy, DeletedDate, IsActive, Serial FROM RequestSummary WHERE RequestSummaryID=$n ", aequestSummaryID);

        //               DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
        //               AddToCollection(aequestSummarys, reader);
        //               reader.Close();
        //           }
        //           catch(Exception ex)
        //           {
        //               throw new Exception(ex.Message);
        //           }
        //           if (aequestSummarys.Count > 0)
        //           {
        //               return aequestSummarys[0];
        //           }
        //           else return new BERequestSummary();
        //       }

        //       public BERequestSummarys GetRequestSummarys(SQLHelper sqlHelper )
        //       {
        //           string sql = string.Empty;
        //           BERequestSummarys aequestSummarys = new BERequestSummarys();

        //           try
        //           {
        //               sql = sqlHelper.MakeSQL("SELECT RequestSummaryID, Code, RequestSummaryName, RequestSummaryMessage, IsRequestSummary, , CreatedBy, CreatedDate, UpdatedBy, UpdatedDate,"
        //                   + "IsDeleted, DeletedBy, DeletedDate, IsActive, Serial FROM RequestSummary WHERE IsDeleted=$b", false);

        //               DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
        //               AddToCollection(aequestSummarys, reader);
        //               reader.Close();
        //           }
        //           catch(Exception ex)
        //           {
        //               throw new Exception(ex.Message);
        //           }
        //           return aequestSummarys;
        //       }


        //       public BERequestSummarys GetRequestSummarys(SQLHelper sqlHelper , BERequestSummary searchCriteria, 
        //int pageIndex, int numberOfRecordsPerPage, string orderByQuery, out int totalCount)
        //       {
        //           string sql = string.Empty;
        //           string countSQL = string.Empty;
        //           string whereClause = string.Empty;
        //           BERequestSummarys aequestSummarys = new BERequestSummarys();

        //           try
        //           {
        //               sql = sqlHelper.MakeSQL("SELECT RequestSummaryID, Code, RequestSummaryName, RequestSummaryMessage, IsRequestSummary, , CreatedBy, CreatedDate, UpdatedBy, UpdatedDate,"
        //                   + "IsDeleted, DeletedBy, DeletedDate, IsActive, Serial FROM RequestSummary WHERE IsDeleted=$b $q", false, whereClause);
        //               totalCount = 0;
        //               //countSQL = "SELECT COUNT(1) FROM (" + sql + ") CountSQL;
        //               object objTotalCount = await sqlHelper.ExecuteScalarAsync(countSQL);

        //               if (!(objTotalCount == DBNull.Value || objTotalCount == null))
        //                   totalCount = Convert.ToInt32(objTotalCount);

        //               //sql= DACommon.AddPagingQuery(sql, pageIndex, numberOfRecordsPerPage, orderByQuery);


        //               DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
        //               AddToCollection(aequestSummarys, reader);
        //               reader.Close();
        //           }
        //           catch(Exception ex)
        //           {
        //               throw new Exception(ex.Message);
        //           }
        //           return aequestSummarys;
        //       }

        //       private BERequestSummarys AddToCollection(BERequestSummarys aequestSummarys, DbDataReader reader)
        //       {
        //           try
        //           {
        //               NULLHandler nullHandler = new NULLHandler(reader);
        //               while (reader.Read())
        //               {
        //                   aequestSummarys.Add(PreaperObject(nullHandler));
        //               }
        //           }
        //           catch(Exception ex)
        //           {
        //               throw new Exception(ex.Message);
        //           }
        //           return aequestSummarys;
        //       }

        //       private BERequestSummary PreaperObject(NULLHandler nullHandler)
        //       {
        //           BERequestSummary aequestSummary = new BERequestSummary();

        //           try
        //           {
        //               aequestSummary.IsNew = false;
        //               aequestSummary.RequestSummaryID = nullHandler.GetInt("RequestSummaryID");
        //               aequestSummary.Code = nullHandler.GetString("Code");
        //               aequestSummary.RequestSummaryName = nullHandler.GetString("RequestSummaryName");
        //               aequestSummary.RequestSummaryMessage = nullHandler.GetString("RequestSummaryMessage");
        //               aequestSummary.IsRequestSummary = nullHandler.GetString("IsRequestSummary");
        //               aequestSummary.IsDeleted = nullHandler.GetBoolean("IsDeleted");
        //               aequestSummary.IsActive = nullHandler.GetBoolean("IsActive");
        //               aequestSummary.CreatedBy = nullHandler.GetInt("CreatedBy");
        //               aequestSummary.CreatedDate = nullHandler.GetDateTime("CreatedDate");
        //               aequestSummary.UpdatedBy = nullHandler.GetInt("UpdatedBy");
        //               aequestSummary.UpdatedDate = nullHandler.GetDateTime("UpdatedDate");
        //               aequestSummary.DeletedBy = nullHandler.GetInt("DeletedBy");
        //               aequestSummary.DeletedDate = nullHandler.GetDateTime("DeletedDate");
        //               aequestSummary.Serial = nullHandler.GetInt("Serial");
        //           }
        //           catch(Exception ex)
        //           {
        //               throw new Exception(ex.Message);
        //           }
        //           return aequestSummary;
        //       }
        //       #endregion
    }

    #endregion
}