﻿using System;
using System.Data.Common;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using SQLFactory;

namespace DataAccess
{
    #region Object of DAServiceUserRole

    public class DAServiceUserRole
    {
        #region Destructor

        ~DAServiceUserRole()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Get

        public async Task<BEServiceUserRoles> GetServiceUserRole(SQLHelper sqlHelper)
        {
            var sql = string.Empty;
            var serviceUserInfos = new BEServiceUserRoles();

            try
            {
                sql = sqlHelper.MakeSQL(
                    "SELECT a.UserID, a.RoleID,a.ServiceUserRoleID,b.RoleName  FROM tblServiceUserRole a " +
                    "left join tblServiceRole b on a.RoleID = b.RoleID");


                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(serviceUserInfos, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (serviceUserInfos.Count > 0)
                return serviceUserInfos;
            return new BEServiceUserRoles();
        }


        private BEServiceUserRoles AddToCollection(BEServiceUserRoles serviceUserInfos, DbDataReader reader)
        {
            try
            {
                var nullHandler = new NULLHandler(reader);
                while (reader.Read()) serviceUserInfos.Add(PreaperObject(nullHandler));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return serviceUserInfos;
        }

        private BEServiceUserRole PreaperObject(NULLHandler nullHandler)
        {
            var serviceUserInfo = new BEServiceUserRole();

            try
            {
                serviceUserInfo.RoleName = nullHandler.GetString("RoleName");
                serviceUserInfo.RoleID = nullHandler.GetInt("RoleID");
                serviceUserInfo.UserID = nullHandler.GetInt("UserID");
                serviceUserInfo.ServiceUserRoleID = nullHandler.GetInt("ServiceUserRoleID");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return serviceUserInfo;
        }
    }

    #endregion
}

#endregion