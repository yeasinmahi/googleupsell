﻿using System;
using System.Data.Common;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using SQLFactory;

namespace DataAccess
{
    #region Object of DAServiceRole

    public class DAServiceRole
    {
        #region Destructor

        ~DAServiceRole()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Get

        public async Task<BEServiceRoles> GetServiceRole(SQLHelper sqlHelper)
        {
            var sql = string.Empty;
            var serviceR = new BEServiceRoles();

            try
            {
                sql = sqlHelper.MakeSQL("SELECT RoleID, RoleName  FROM tblServiceRole");

                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(serviceR, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (serviceR.Count > 0)
                return serviceR;
            return new BEServiceRoles();
        }

        private BEServiceRoles AddToCollection(BEServiceRoles serviceUserInfos, DbDataReader reader)
        {
            try
            {
                var nullHandler = new NULLHandler(reader);
                while (reader.Read()) serviceUserInfos.Add(PreaperObject(nullHandler));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return serviceUserInfos;
        }

        private BEServiceRole PreaperObject(NULLHandler nullHandler)
        {
            var serviceUserInfo = new BEServiceRole();

            try
            {
                serviceUserInfo.RoleID = nullHandler.GetInt("RoleID");
                serviceUserInfo.RoleName = nullHandler.GetString("RoleName");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return serviceUserInfo;
        }
    }

    #endregion
}

#endregion