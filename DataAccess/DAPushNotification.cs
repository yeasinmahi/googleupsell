﻿using System;
using System.Data.Common;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using BusinessEntity.Google.Notification;
using SQLFactory;
using Utility.Extensions;

namespace DataAccess
{
    #region Object of DAPushNotification

    public class DAPushNotification
    {
        #region Destructor

        ~DAPushNotification()
        {
        }

        #endregion


        #region Delete

        public async Task Delete(SQLHelper sqlHelper, int ID, int status)
        {
            try
            {
                var sql = string.Empty;
                sql = sqlHelper.MakeSQL(
                    "UPDATE $q SET IsProcessed=$n,ProcessDate=sysdate  WHERE ID=$n ", AppSettings.GoogleNotificationTableName, status, ID);
                await sqlHelper.ExecuteNonQueryAsync(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Constructor

        #endregion

        #region Update

        public async Task UpdateNotification(SQLHelper sqlHelper, string ID)
        {
            try
            {
                var sql = string.Empty;
                sql = sqlHelper.MakeSQL("UPDATE $q Set IsProcessed=1, ProcessDate=sysdate WHERE ID=$s ",
                    AppSettings.GoogleNotificationTableName, ID);
                await sqlHelper.ExecuteNonQueryAsync(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task UpdateNotification(SQLHelper sqlHelper, string ID, int status, long rtt, string comments = null)
        {
            try
            {
                var sql = string.Empty;
                if (comments != null && comments.Length > 190) comments.Substring(0, 190);
                sql = sqlHelper.MakeSQL(
                    "UPDATE $q Set IsProcessed=$n, ProcessDate=sysdate, Errormessage=$s, rtt=$n, BookID=null,BookDate=null WHERE ID=$s ",
                    AppSettings.GoogleNotificationTableName,status, comments, rtt, ID);
                await sqlHelper.ExecuteNonQueryAsync(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Get

        public async Task<PushNotification> GetPushNotification(SQLHelper sqlHelper, int ID)
        {
            var sql = string.Empty;
            var PushNotification = new PushNotifications();

            try
            {
                sql = sqlHelper.MakeSQL("SELECT * FROM $q WHERE ID=$n ",AppSettings.GoogleNotificationTableName, ID);

                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(PushNotification, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (PushNotification.Count > 0)
                return PushNotification[0];
            return new PushNotification();
        }

        public async Task<PushNotifications> GetPushNotifications(SQLHelper sqlHelper)
        {
            var sql = string.Empty;
            var PushNotification = new PushNotifications();

            try
            {
                sql = sqlHelper.MakeSQL("SELECT *  FROM $q ",
                    AppSettings.GoogleNotificationTableName);

                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(PushNotification, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return PushNotification;
        }

        public async Task<PushNotifications> GetPushNotificationServiceData(SQLHelper sqlHelper)
        {
            var sql = string.Empty;
            var campaingnBonus = new PushNotifications();

            try
            {
                sql = sqlHelper.MakeSQL("SELECT * FROM (SELECT * "
                                        + " FROM $q  WHERE IsProcessed=0  ORDER BY ID) WHERE ROWNUM<11",
                    AppSettings.GoogleNotificationTableName);

                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(campaingnBonus, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return campaingnBonus;
        }

        public async Task<PushNotifications> GetPushNotificationServiceDataT24(SQLHelper sqlHelper,
           int noOfRecords, string guid, string triggerId, string subQuery)
        {
            var sql = string.Empty;
            var pNotification = new PushNotifications();

            try
            {
                sql = sqlHelper.MakeSQL(@"Update $q b
                   set b.BookID   = $s,
                       b.BookDate = sysdate
                         where b.ID in (select *
                          from (select t.id
                                 from $q t where
                                    t.triggerid = $s  and  t.IsProcessed = 0 AND t.BookID IS NULL $q
                                 order by t.ID) x
                         where rownum <= $n
                   )", AppSettings.GoogleNotificationTableName, guid, AppSettings.GoogleNotificationTableName, triggerId, subQuery, noOfRecords);
                await sqlHelper.ExecuteQueryAsync(sql);

                sql = sqlHelper.MakeSQL(@"SELECT c.cpid, a.* FROM $q a " +
                                         " INNER JOIN tblcpid c ON a.msisdn = c.msisdn " +
                                        " WHERE a.BookID=$s", AppSettings.GoogleNotificationTableName, guid);
                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(pNotification, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return pNotification;
        }

        public async Task<PushNotifications> GetPushNotificationServiceData(SQLHelper sqlHelper,
            int noOfRecords, string guid, string triggerId, string subQuery)
        {
            var sql = string.Empty;
            PushNotifications pNotification = new PushNotifications();

            try
            {

                sql = sqlHelper.MakeSQL(@"Update $q b
                   set b.BookID   = $s,
                       b.BookDate = sysdate
                         where b.ID in (select *
                          from (select t.id
                                 from $q t
                                 --inner join veon_apps.data_product p on 
                                    where (t.triggerid = $s  and  t.IsProcessed = 0 AND t.BookID IS NULL $q)
                                    --(p.data_code = t.PRODUCT_CODE or p.auto_renew_code = t.PRODUCT_CODE))
                                 --inner join veon_apps.data_productplatform pp on (pp.productid = p.data_product_id and pp.platformid = 1)
                                 order by t.ID) x
                         where rownum <= $n
                   )", AppSettings.GoogleNotificationTableName, guid, AppSettings.GoogleNotificationTableName, triggerId, subQuery, noOfRecords);
                await sqlHelper.ExecuteQueryAsync(sql);

                sql = sqlHelper.MakeSQL(@"SELECT DATA_PRODUCT_NAME_DISPLAY,p.initial_amount,p.amount_unit,p.product_type, a.* FROM $q a " +
                                       // " INNER JOIN tblcpid c ON a.msisdn = c.msisdn " +
                                        "inner join veon_apps.data_product p on (p.data_code = a.PRODUCT_CODE or p.auto_renew_code = a.PRODUCT_CODE)" +
                                        " WHERE a.BookID=$s", AppSettings.GoogleNotificationTableName, guid);
                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToGoogleNotificationCollection(pNotification, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return pNotification;
        }

        private PushNotifications AddToGoogleNotificationCollection(PushNotifications campainBonus, DbDataReader reader)
        {
            try
            {
                var nullHandler = new NULLHandler(reader);
                while (reader.Read()) campainBonus.Add(PreaperGoogleNotificationObject(nullHandler));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return campainBonus;
        }

        private PushNotification PreaperGoogleNotificationObject(NULLHandler nullHandler)
        {
            var PushNotification = new PushNotification();

            try
            {
                PushNotification.KafkaId = nullHandler.GetString("Id");
                PushNotification.TriggerId = nullHandler.GetString("TriggerId");
                // PushNotification.TriggerDescription = nullHandler.GetString("TriggerDescription");
                PushNotification.AggregateId = nullHandler.GetString("AggregateId");
                // PushNotification.IncomingTime = nullHandler.GetString("IncomingTime");
                PushNotification.NotificationTime = nullHandler.GetString("NotificationTime");
                //PushNotification.IsSuccess = nullHandler.GetInt("IsSuccess");
                PushNotification.Msisdn = nullHandler.GetString("Msisdn");
                PushNotification.TRIGGER_ATTR_02 = nullHandler.GetString("TRIGGER_ATTR_02");
                PushNotification.TRIGGER_ATTR_01 = nullHandler.GetString("TRIGGER_ATTR_01");
                PushNotification.IsProcessed = nullHandler.GetInt("IsProcessed");
                //PushNotification.IsProcessed = nullHandler.GetInt("IsProcessed");
                PushNotification.ProcessDate = nullHandler.GetDateTime("ProcessDate");
                PushNotification.BookID = nullHandler.GetString("BookID");
                PushNotification.BookDate = nullHandler.GetDateTime("BookDate");
                PushNotification.ErrorMessage = nullHandler.GetString("ErrorMessage");
                PushNotification.CPID = nullHandler.GetString("CPID");
                PushNotification.PackName = nullHandler.GetString("DATA_PRODUCT_NAME_DISPLAY");
                PushNotification.PackCode = nullHandler.GetString("product_code");
                PushNotification.PackQuta = nullHandler.GetDouble("initial_amount");
                PushNotification.PackType = nullHandler.GetString("product_type");
                PushNotification.PackUnit = nullHandler.GetString("amount_unit");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return PushNotification;
        }

        private PushNotifications AddToCollection(PushNotifications campainBonus, DbDataReader reader)
        {
            try
            {
                var nullHandler = new NULLHandler(reader);
                while (reader.Read()) campainBonus.Add(PreaperObject(nullHandler));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return campainBonus;
        }

        private PushNotification PreaperObject(NULLHandler nullHandler)
        {
            var PushNotification = new PushNotification();

            try
            {
                PushNotification.KafkaId = nullHandler.GetString("Id");
                PushNotification.TriggerId = nullHandler.GetString("TriggerId");
                // PushNotification.TriggerDescription = nullHandler.GetString("TriggerDescription");
                PushNotification.AggregateId = nullHandler.GetString("AggregateId");
                // PushNotification.IncomingTime = nullHandler.GetString("IncomingTime");
                PushNotification.NotificationTime = nullHandler.GetString("NotificationTime");
                //PushNotification.IsSuccess = nullHandler.GetInt("IsSuccess");
                PushNotification.Msisdn = nullHandler.GetString("Msisdn");
                PushNotification.TRIGGER_ATTR_02 = nullHandler.GetString("TRIGGER_ATTR_02");
                PushNotification.TRIGGER_ATTR_01 = nullHandler.GetString("TRIGGER_ATTR_01");
                PushNotification.IsProcessed = nullHandler.GetInt("IsProcessed");
                //PushNotification.IsProcessed = nullHandler.GetInt("IsProcessed");
                PushNotification.ProcessDate = nullHandler.GetDateTime("ProcessDate");
                PushNotification.BookID = nullHandler.GetString("BookID");
                PushNotification.BookDate = nullHandler.GetDateTime("BookDate");
                PushNotification.ErrorMessage = nullHandler.GetString("ErrorMessage");
                PushNotification.CPID = nullHandler.GetString("CPID");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return PushNotification;
        }

        #endregion
        public async Task SaveKafka(SQLHelper sqlHelper, PushNotification pushNotification)
        {
            try
            {
                pushNotification = SubstringNotification(pushNotification);
                var sql = string.Empty;

                sql = sqlHelper.MakeSQL(
                    "INSERT INTO kafkanotification(trigger_attr_01, trigger_attr_02, trigger_attr_03,"
                    + " trigger_attr_04, trigger_attr_05, trigger_attr_06, trigger_attr_07,"
                    + " trigger_attr_08, trigger_attr_09, trigger_attr_10, trigger_attr_11, trigger_attr_12, trigger_attr_13,"
                    + " trigger_attr_14, trigger_attr_15, trigger_attr_16, trigger_attr_17, trigger_attr_18, trigger_attr_19,"
                    + " trigger_attr_20, id, triggerid, aggregateid, "
                    + " notificationtime, msisdn) VALUES ($s, $s, $s,"
                    + " $s, $s, $s, $s, $s, $s, $s, $s, $s, $s, $s, $s, $s, $s, $s, $s, $s,"
                    + " $s, $s, $s, $s, $s)",
                    pushNotification.TRIGGER_ATTR_01,
                    pushNotification.TRIGGER_ATTR_02, pushNotification.TRIGGER_ATTR_03,
                    pushNotification.TRIGGER_ATTR_04,
                    pushNotification.TRIGGER_ATTR_05, pushNotification.TRIGGER_ATTR_06,
                    pushNotification.TRIGGER_ATTR_07, pushNotification.TRIGGER_ATTR_08,
                    pushNotification.TRIGGER_ATTR_09,
                    pushNotification.TRIGGER_ATTR_10, pushNotification.TRIGGER_ATTR_11,
                    pushNotification.TRIGGER_ATTR_12,
                    pushNotification.TRIGGER_ATTR_13, pushNotification.TRIGGER_ATTR_14,
                    pushNotification.TRIGGER_ATTR_15,
                    pushNotification.TRIGGER_ATTR_16, pushNotification.TRIGGER_ATTR_17,
                    pushNotification.TRIGGER_ATTR_18,
                    pushNotification.TRIGGER_ATTR_19, pushNotification.TRIGGER_ATTR_20,
                    pushNotification.KafkaId+"_"+Guid.NewGuid(),
                    pushNotification.TriggerId, pushNotification.AggregateId, pushNotification.NotificationTime,
                    pushNotification.Msisdn
                    );

                await sqlHelper.ExecuteNonQueryAsync(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task SaveGoogle(SQLHelper sqlHelper, PushNotification pushNotification)
        {
            try
            {
                pushNotification = SubstringNotification(pushNotification);
                var sql = string.Empty;

                sql = sqlHelper.MakeSQL(
                    "INSERT INTO $q (trigger_attr_01, trigger_attr_02, trigger_attr_03,"
                    + " trigger_attr_04, trigger_attr_05, trigger_attr_06, trigger_attr_07,"
                    + " trigger_attr_08, trigger_attr_09, trigger_attr_10, trigger_attr_11, trigger_attr_12, trigger_attr_13,"
                    + " trigger_attr_14, trigger_attr_15, trigger_attr_16, trigger_attr_17, trigger_attr_18, trigger_attr_19,"
                    + " trigger_attr_20, id,kafkaId, triggerid, aggregateid, "
                    + " notificationtime, msisdn,product_code,cpid) VALUES ($s, $s, $s,"
                    + " $s, $s, $s, $s, $s, $s, $s, $s, $s, $s, $s, $s, $s, $s, $s, $s, $s,"
                    + " SQ_GOOGLE_NOTIFICATION.nextval, $s, $s, $s, $s, $s,$s,$s)",
                    AppSettings.GoogleNotificationTableName,
                    pushNotification.TRIGGER_ATTR_01,
                    pushNotification.TRIGGER_ATTR_02, pushNotification.TRIGGER_ATTR_03,
                    pushNotification.TRIGGER_ATTR_04,
                    pushNotification.TRIGGER_ATTR_05, pushNotification.TRIGGER_ATTR_06,
                    pushNotification.TRIGGER_ATTR_07, pushNotification.TRIGGER_ATTR_08,
                    pushNotification.TRIGGER_ATTR_09,
                    pushNotification.TRIGGER_ATTR_10, pushNotification.TRIGGER_ATTR_11,
                    pushNotification.TRIGGER_ATTR_12,
                    pushNotification.TRIGGER_ATTR_13, pushNotification.TRIGGER_ATTR_14,
                    pushNotification.TRIGGER_ATTR_15,
                    pushNotification.TRIGGER_ATTR_16, pushNotification.TRIGGER_ATTR_17,
                    pushNotification.TRIGGER_ATTR_18,
                    pushNotification.TRIGGER_ATTR_19, pushNotification.TRIGGER_ATTR_20,
                    pushNotification.KafkaId + "_" + Guid.NewGuid(),
                    pushNotification.TriggerId, pushNotification.AggregateId, pushNotification.NotificationTime,
                    pushNotification.Msisdn,
                    pushNotification.PackCode,
                    pushNotification.CPID
                    );

                await sqlHelper.ExecuteNonQueryAsync(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public PushNotification SubstringNotification(PushNotification pushNotification)
        {
            pushNotification.TRIGGER_ATTR_01 = pushNotification.TRIGGER_ATTR_01.MySubString(4000);
            pushNotification.TRIGGER_ATTR_02 = pushNotification.TRIGGER_ATTR_02.MySubString(4000);
            pushNotification.TRIGGER_ATTR_03 = pushNotification.TRIGGER_ATTR_03.MySubString(4000);
            pushNotification.TRIGGER_ATTR_04 = pushNotification.TRIGGER_ATTR_04.MySubString(4000);
            pushNotification.TRIGGER_ATTR_05 = pushNotification.TRIGGER_ATTR_05.MySubString(4000);
            pushNotification.TRIGGER_ATTR_06 = pushNotification.TRIGGER_ATTR_06.MySubString(4000);
            pushNotification.TRIGGER_ATTR_07 = pushNotification.TRIGGER_ATTR_07.MySubString(4000);
            pushNotification.TRIGGER_ATTR_08 = pushNotification.TRIGGER_ATTR_08.MySubString(4000);
            pushNotification.TRIGGER_ATTR_09 = pushNotification.TRIGGER_ATTR_09.MySubString(4000);
            pushNotification.TRIGGER_ATTR_10 = pushNotification.TRIGGER_ATTR_10.MySubString(4000);
            pushNotification.TRIGGER_ATTR_11 = pushNotification.TRIGGER_ATTR_11.MySubString(4000);
            pushNotification.TRIGGER_ATTR_12 = pushNotification.TRIGGER_ATTR_12.MySubString(4000);
            pushNotification.TRIGGER_ATTR_13 = pushNotification.TRIGGER_ATTR_13.MySubString(4000);
            pushNotification.TRIGGER_ATTR_14 = pushNotification.TRIGGER_ATTR_14.MySubString(4000);
            pushNotification.TRIGGER_ATTR_15 = pushNotification.TRIGGER_ATTR_15.MySubString(4000);
            pushNotification.TRIGGER_ATTR_16 = pushNotification.TRIGGER_ATTR_16.MySubString(4000);
            pushNotification.TRIGGER_ATTR_17 = pushNotification.TRIGGER_ATTR_17.MySubString(4000);
            pushNotification.TRIGGER_ATTR_18 = pushNotification.TRIGGER_ATTR_18.MySubString(4000);
            pushNotification.TRIGGER_ATTR_19 = pushNotification.TRIGGER_ATTR_19.MySubString(4000);
            pushNotification.TRIGGER_ATTR_20 = pushNotification.TRIGGER_ATTR_20.MySubString(4000);
            pushNotification.KafkaId = pushNotification.KafkaId.MySubString(100);
            pushNotification.TriggerId = pushNotification.TriggerId.MySubString(100);
            pushNotification.AggregateId = pushNotification.AggregateId.MySubString(100);
            pushNotification.NotificationTime = pushNotification.NotificationTime.MySubString(100);
            pushNotification.Msisdn = pushNotification.Msisdn.MySubString(20);

            return pushNotification;
        }

    }

    #endregion
}