﻿//using BusinessEntity.APIHub2;
//using SQLFactory;
//using System;
//using System.Data.Common;
//using System.Threading.Tasks;

//namespace DataAccess
//{
//    #region Object of DARequestResponseLog
//    public class DARequestResponseLog
//    {
//        #region Constructor
//        public DARequestResponseLog() { }
//        #endregion

//        #region Destructor
//        ~DARequestResponseLog() { }
//        #endregion

//        #region Save
//        public async Task Create(SQLHelper sqlHelper, BERequestResponseLog log)
//        {
//            try
//            {
//                string sql = string.Empty;

//                var logIDSQL = sqlHelper.MakeSQL(@"SELECT SQ_AppRequestResponseLog.NEXTVAL FROM DUAL");
//                object obj = await sqlHelper.ExecuteScalarAsync(logIDSQL);
//                if (!(obj == DBNull.Value || obj == null))
//                    log.ID = Convert.ToInt32(obj);


//                //sql = @" INSERT INTO AppRequestResponseLog ( ID, MSISDN, LogTime, Request, Response, RequestMethod, RequestFrom, RTT, IsActive ) "
//                //     + " VALUES ( :LOOGID3, :MSISDN3, :LOGTIME3, :REQUEST3, :RESPONSE3, :REQUESTMETHOD3, :REQUESTFROM3, :RTT3)";

//                //object[] objParam = { log.ID, log.MSISDN, log.LogTime, log.Request, log.Response, log.RequestMethod, log.RequestFrom, log.RTT, log.IsActive };

//                //DbDataReader reader = await sqlHelper.ExecuteQueryAsCommandAsync(
//                //    sql,
//                //    new string[] { "LOOGID3|N", "MSISDN3|S", "LOGTIME3|D", "REQUEST3|NS", "RESPONSE3|NS", "REQUESTMETHOD3|S", "REQUESTFROM3|S", "RTT3|N", "ISACTIVE3|B" }
//                //    , objParam);


//                sql = @" INSERT INTO AppRequestResponseLog (  ID, MSISDN, REQUESTURI, REQUESTBODY, REQUESTHADERS, CLIENTIPADDRESS, RESPONSEBODY, RESPONSESTATUSCODE,
//  RESPONSEHADERS, CREATEDATE, SERVERNAME, SERVERIP) 
//                       VALUES ( :p_ID, :p_MSISDN, :p_REQUESTURI, :p_REQUESTBODY, :p_REQUESTHADERS, :p_CLIENTIPADDRESS, :p_RESPONSEBODY, :p_RESPONSESTATUSCODE,
//  RESPONSEHADERS, :p_CREATEDATE, :p_SERVERNAME, :p_SERVERIP)";

//                object[] objParam = { log.ID, log.MSISDN, log.Request, log.RequestBody, log.REQUESTHADERS, log.CLIENTIPADDRESS, log.RESPONSEBODY, log.RESPONSESTATUSCODE,
//  log.RESPONSEHADERS, log.CREATEDATE, log.SERVERNAME, log.SERVERIP };

//                DbDataReader reader = await sqlHelper.ExecuteQueryAsCommandAsync(
//                    sql,
//                    new string[] {  "p_ID|N", "p_MSISDN|S", "p_REQUESTURI|S", "p_REQUESTBODY|S", "p_REQUESTHADERS|S", "p_CLIENTIPADDRESS|S",
// "p_RESPONSEBODY|NS", "p_RESPONSESTATUSCODE|S",
//  "RESPONSEHADERS|S", "p_CREATEDATE|D", "p_SERVERNAME|S", "p_SERVERIP|S"}
//                    , objParam);

//                reader.Close();
//            }
//            catch (Exception ex)
//            {
//                throw;
//            }
//        }

//        public async Task Update(SQLHelper sqlHelper, BERequestResponseLog log)
//        {
//            try
//            {
//                string sql = string.Empty;


//                sql = sqlHelper.MakeSQL("UPDATE AppRequestResponseLog SET "
//                    + "IsActive=$b "
//                    + "WHERE ID=$n ", log.IsActive, log.ID);

//                await sqlHelper.ExecuteNonQueryAsync(sql);
//            }
//            catch (Exception)
//            {
//                throw;
//            }
//        }

//        #endregion

//        #region Delete
//        public async Task Delete(SQLHelper sqlHelper, int id)
//        {
//            try
//            {
//                string sql = string.Empty;
//                sql = sqlHelper.MakeSQL("UPDATE AppRequestResponseLog SET IsActive=$b  WHERE ID=$n ", false, id);
//                await sqlHelper.ExecuteNonQueryAsync(sql);
//            }
//            catch (Exception)
//            {
//                throw;
//            }
//        }
//        #endregion

//        #region Get
//        public async Task<BERequestResponseLog> GetRequestResponseLog(SQLHelper sqlHelper, int id)
//        {
//            string sql = string.Empty;
//            BERequestResponseLogs logs = new BERequestResponseLogs();

//            try
//            {
//                sql = sqlHelper.MakeSQL("SELECT ID,MSISDN,LogTime,Request,Response,RequestMethod,RequestFrom,RTT,IsActive"
//                    + " FROM AppRequestResponseLog"
//                    + " WHERE ID=$n ", id);

//                DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
//                AddToCollection(logs, reader);
//                reader.Close();
//            }
//            catch (Exception)
//            {
//                throw;
//            }
//            if (logs.Count > 0)
//            {
//                return logs[0];
//            }
//            else return new BERequestResponseLog();
//        }

//        public async Task<BERequestResponseLogs> GetRequestResponseLogs(SQLHelper sqlHelper, bool activeOnly)
//        {
//            string sql = string.Empty;
//            string whereClause = string.Empty;
//            BERequestResponseLogs logs = new BERequestResponseLogs();

//            try
//            {
//                if (activeOnly)
//                    whereClause += sqlHelper.MakeSQL(" WHERE IsActive = $b", activeOnly);
//                sql = sqlHelper.MakeSQL("SELECT ID,MSISDN,LogTime,Request,Response,RequestMethod,RequestFrom,RTT,IsActive"
//                    + " FROM AppRequestResponseLog"
//                    + "$q", whereClause);

//                DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
//                AddToCollection(logs, reader);
//                reader.Close();
//            }
//            catch (Exception)
//            {
//                throw;
//            }
//            return logs;
//        }

//        private BERequestResponseLogs AddToCollection(BERequestResponseLogs logs, DbDataReader reader)
//        {
//            try
//            {
//                NULLHandler nullHandler = new NULLHandler(reader);
//                while (reader.Read())
//                {
//                    logs.Add(PreaperObject(nullHandler));
//                }
//            }
//            catch (Exception)
//            {
//                throw;
//            }
//            return logs;
//        }

//        private BERequestResponseLog PreaperObject(NULLHandler nullHandler)
//        {
//            BERequestResponseLog log = new BERequestResponseLog();

//            try
//            {
//                log.ID = nullHandler.GetInt("ID");
//                log.MSISDN = nullHandler.GetString("MSISDN");
//                log.LogTime = nullHandler.GetDateTime("LogTime");
//                log.Request = nullHandler.GetString("Request");
//                log.Response = nullHandler.GetString("Response");
//                log.RequestMethod = nullHandler.GetString("RequestMethod");
//                log.RTT = nullHandler.GetInt("RTT");
//                log.RequestFrom = nullHandler.GetString("RequestFrom");
//                log.IsActive = nullHandler.GetBoolean("ISACTIVE");
//            }
//            catch (Exception)
//            {
//                throw;
//            }
//            return log;
//        }
//        #endregion

//    }
//    #endregion
//}

