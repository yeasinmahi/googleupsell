//using System;
//using System.Threading.Tasks;
//using BusinessEntity.Kafka;
//using SQLFactory;

//namespace DataAccess
//{
//    #region Object of DAKafkaNotification

//    public class DAkafkaNotification
//    {
//        #region Destructor

//        ~DAkafkaNotification()
//        {
//        }

//        #endregion

//        #region Save

//        public async Task Save(SQLHelper sqlHelper, BEKafkaNotification kafkaNotification)
//        {
//            try
//            {
//                var sql = string.Empty;

//                sql = sqlHelper.MakeSQL(
//                    "INSERT INTO KafkaNotification(trigger_attr_05, trigger_attr_06, trigger_attr_07,"
//                    + " trigger_attr_08, trigger_attr_09, trigger_attr_10, trigger_attr_11, trigger_attr_12, trigger_attr_13,"
//                    + " trigger_attr_14, trigger_attr_15, trigger_attr_16, trigger_attr_17, trigger_attr_18, trigger_attr_19,"
//                    + " trigger_attr_20, isprocessed1, id, triggerid, aggregateid, "
//                    + " notificationtime, isprocessed, issuccess, msisdn, trigger_attr_01, trigger_attr_02, trigger_attr_03,"
//                    + " trigger_attr_04) VALUES ($s, $s, $s,"
//                    + " $s, $s, $s, $s, $s, $s, $s, $s, $s, $s, $s, $s, $s, $n, $s, $s, $s, $s, $b,"
//                    + " $b, $s, $s, $s, $s, $s)", kafkaNotification.trigger_attr_05, kafkaNotification.trigger_attr_06,
//                    kafkaNotification.trigger_attr_07, kafkaNotification.trigger_attr_08,
//                    kafkaNotification.trigger_attr_09,
//                    kafkaNotification.trigger_attr_10, kafkaNotification.trigger_attr_11,
//                    kafkaNotification.trigger_attr_12,
//                    kafkaNotification.trigger_attr_13, kafkaNotification.trigger_attr_14,
//                    kafkaNotification.trigger_attr_15,
//                    kafkaNotification.trigger_attr_16, kafkaNotification.trigger_attr_17,
//                    kafkaNotification.trigger_attr_18,
//                    kafkaNotification.trigger_attr_19, kafkaNotification.trigger_attr_20,
//                    kafkaNotification.isprocessed1,
//                    kafkaNotification.id,
//                    kafkaNotification.triggerid, kafkaNotification.aggregateid, kafkaNotification.notificationtime,
//                    kafkaNotification.isprocessed, kafkaNotification.issuccess, kafkaNotification.msisdn,
//                    kafkaNotification.trigger_attr_01,
//                    kafkaNotification.trigger_attr_02, kafkaNotification.trigger_attr_03,
//                    kafkaNotification.trigger_attr_04);

//                await sqlHelper.ExecuteNonQueryAsync(sql);
//            }
//            catch (Exception ex)
//            {
//                throw new Exception(ex.Message);
//            }
//        }

//        #endregion

//        #region Constructor

//        #endregion
//    }

//    #endregion
//}