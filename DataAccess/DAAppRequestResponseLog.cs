using System;
using System.Data.Common;
using System.Threading.Tasks;
using BusinessEntity.Logging;
using SQLFactory;

namespace DataAccess
{
    #region Object of DAAppRequestResponseLog

    public class DAAppRequestResponseLog
    {
        #region Destructor

        ~DAAppRequestResponseLog()
        {
        }

        #endregion

        #region Delete

        public async Task Delete(SQLHelper sqlHelper, int id, int deletedBy)
        {
            try
            {
                var sql = string.Empty;
                sql = sqlHelper.MakeSQL(
                    "UPDATE AppRequestResponseLog SET IsDeleted=$b, DeletedBy=$n , DeletedDate=sysdate  WHERE Id=$n ",
                    true, deletedBy, id);
                await sqlHelper.ExecuteNonQueryAsync(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Constructor

        #endregion

        #region Save

        public async Task Save(SQLHelper sqlHelper, BEAppRequestResponseLog appRequestResponseLog)
        {
            try
            {
                var sql = string.Empty;
                sql = sqlHelper.MakeSQL(
                    "INSERT INTO AppRequestResponseLog(ServerName, ServerIp, Id, Msisdn, RequestUri,"
                    + " RequestBody, REQUESTHADERS, CLIENTIPADDRESS, ResponseBody, ResponsestatusCode, RESPONSEHADERS) VALUES ($s, $s, SQ_APPREQUESTRESPONSELOG.NEXTVAL, $s, $s, $s, $s,"
                    + " $s, $s, $s, $s)", appRequestResponseLog.ServerName, appRequestResponseLog.ServerIp,
                    appRequestResponseLog.Msisdn, appRequestResponseLog.RequestUri, appRequestResponseLog.RequestBody,
                    appRequestResponseLog.RequestHeaders, appRequestResponseLog.ClienIipAddress,
                    appRequestResponseLog.ResponseBody,
                    appRequestResponseLog.ResponSestatusCode, appRequestResponseLog.ResponseHeaders);

                await sqlHelper.ExecuteNonQueryAsync(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task Create(SQLHelper sqlHelper, BEAppRequestResponseLog log)
        {
            var sql = string.Empty;

            //var logIDSQL = sqlHelper.MakeSQL(@"SELECT SQ_AppRequestResponseLog.NEXTVAL FROM DUAL");
            //object obj = await sqlHelper.ExecuteScalarAsync(logIDSQL);
            //if (!(obj == DBNull.Value || obj == null))
            //	log.Id = Convert.ToInt32(obj);


            //sql = @" INSERT INTO AppRequestResponseLog ( ID, MSISDN, LogTime, Request, Response, RequestMethod, RequestFrom, RTT, IsActive ) "
            //     + " VALUES ( :LOOGID3, :MSISDN3, :LOGTIME3, :REQUEST3, :RESPONSE3, :REQUESTMETHOD3, :REQUESTFROM3, :RTT3)";

            //object[] objParam = { log.ID, log.MSISDN, log.LogTime, log.Request, log.Response, log.RequestMethod, log.RequestFrom, log.RTT, log.IsActive };

            //DbDataReader reader = await sqlHelper.ExecuteQueryAsCommandAsync(
            //    sql,
            //    new string[] { "LOOGID3|N", "MSISDN3|S", "LOGTIME3|D", "REQUEST3|NS", "RESPONSE3|NS", "REQUESTMETHOD3|S", "REQUESTFROM3|S", "RTT3|N", "ISACTIVE3|B" }
            //    , objParam);


            //			sql = @" INSERT INTO AppRequestResponseLog (  ID, MSISDN, RequestMethod, REQUESTURI, REQUESTBODY, REQUESTHADERS, CLIENTIPADDRESS, RESPONSEBODY, RESPONSESTATUSCODE,
            // RESPONSEHADERS, CREATEDATE, SERVERNAME, SERVERIP) 
            //                      VALUES ( :p_ID, :p_MSISDN, :p_RequestMethod, :p_REQUESTURI, :p_REQUESTBODY, :p_REQUESTHADERS, :p_CLIENTIPADDRESS, :p_RESPONSEBODY, :p_RESPONSESTATUSCODE,
            // :p_RESPONSEHADERS, :p_CREATEDATE, :p_SERVERNAME, :p_SERVERIP)";

            //			object[] objParam = { log.Id, log.Msisdn, log.RequestMethod, log.RequestUri, log.RequestBody, log.RequestHeaders, log.ClienIipAddress, log.ResponseBody, log.ResponSestatusCode,
            // log.ResponseHeaders, DateTime.Now, log.ServerName, log.ServerIp };

            //			DbDataReader reader = await sqlHelper.ExecuteQueryAsCommandAsync(
            //				sql,
            //				new string[] {  "p_ID|N", "p_MSISDN|S", "p_RequestMethod|S", "p_REQUESTURI|S", "p_REQUESTBODY|S", "p_REQUESTHADERS|S", "p_CLIENTIPADDRESS|S",
            //"p_RESPONSEBODY|NS", "p_RESPONSESTATUSCODE|S",
            // "p_RESPONSEHADERS|S", "p_CREATEDATE|D", "p_SERVERNAME|S", "p_SERVERIP|S"}
            //				, objParam);

            sql =
                @" INSERT INTO AppRequestResponseLog (  ID, MSISDN, RequestMethod, REQUESTURI, REQUESTBODY, REQUESTHADERS, CLIENTIPADDRESS, RESPONSEBODY, RESPONSESTATUSCODE,
  RESPONSEHADERS, CREATEDATE, SERVERNAME, SERVERIP) 
                       VALUES (  SQ_AppRequestResponseLog.NEXTVAL, :p_MSISDN, :p_RequestMethod, :p_REQUESTURI, :p_REQUESTBODY, :p_REQUESTHADERS, :p_CLIENTIPADDRESS, :p_RESPONSEBODY, :p_RESPONSESTATUSCODE,
  :p_RESPONSEHADERS, :p_CREATEDATE, :p_SERVERNAME, :p_SERVERIP)";

            if (log.ResponseBody != null && log.ResponseBody.Length > 4000)
                log.ResponseBody = log.ResponseBody.Substring(0, 3900);
            object[] objParam =
            {
                log.Msisdn, log.RequestMethod, log.RequestUri, log.RequestBody, log.RequestHeaders, log.ClienIipAddress,
                log.ResponseBody, log.ResponSestatusCode,
                log.ResponseHeaders, DateTime.Now, log.ServerName, log.ServerIp
            };

            var reader = await sqlHelper.ExecuteQueryAsCommandAsync(
                sql,
                new[]
                {
                    "p_MSISDN|S", "p_RequestMethod|S", "p_REQUESTURI|S", "p_REQUESTBODY|S", "p_REQUESTHADERS|S",
                    "p_CLIENTIPADDRESS|S",
                    "p_RESPONSEBODY|S", "p_RESPONSESTATUSCODE|S",
                    "p_RESPONSEHADERS|S", "p_CREATEDATE|D", "p_SERVERNAME|S", "p_SERVERIP|S"
                }
                , objParam);

            reader.Close();
        }

        #endregion

        #region Get

        public async Task<BEAppRequestResponseLog> GetAppRequestResponseLog(SQLHelper sqlHelper, int id)
        {
            var sql = string.Empty;
            var appRequestResponseLogs = new BEAppRequestResponseLogs();

            try
            {
                sql = sqlHelper.MakeSQL(
                    "SELECT ServerName, ServerIp, Id, Msisdn, RequestUri, RequestBody, RequestHeaders, ClienIipAddress, ResponseBody,"
                    + "ResponSestatusCode, ResponseHeaders,  CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, IsDeleted, DeletedBy,"
                    + "DeletedDate, IsActive, SerialNo FROM AppRequestResponseLog WHERE Id=$n ", id);

                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(appRequestResponseLogs, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (appRequestResponseLogs.Count > 0)
                return appRequestResponseLogs[0];
            return new BEAppRequestResponseLog();
        }

        public async Task<BEAppRequestResponseLogs> GetAppRequestResponseLogs(SQLHelper sqlHelper, bool activeOnly)
        {
            var sql = string.Empty;
            var whereClause = string.Empty;
            var appRequestResponseLogs = new BEAppRequestResponseLogs();

            try
            {
                if (activeOnly)
                    whereClause += sqlHelper.MakeSQL(" AND IsActive = $b", activeOnly);
                sql = sqlHelper.MakeSQL(
                    "SELECT ServerName, ServerIp, Id, Msisdn, RequestUri, RequestBody, RequestHeaders, ClienIipAddress, ResponseBody,"
                    + "ResponSestatusCode, ResponseHeaders,  CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, IsDeleted, DeletedBy,"
                    + "DeletedDate, IsActive, SerialNo FROM AppRequestResponseLog WHERE IsDeleted=$b $q", false,
                    whereClause);

                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(appRequestResponseLogs, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return appRequestResponseLogs;
        }

        public async Task<BEAppRequestResponseLogs> GetAppRequestResponseLogs(SQLHelper sqlHelper,
            BEAppRequestResponseLog searchCriteria,
            int pageIndex, int numberOfRecordsPerPage, string orderByQuery)
        {
            var sql = string.Empty;
            var countSQL = string.Empty;
            var whereClause = string.Empty;
            var appRequestResponseLogs = new BEAppRequestResponseLogs();

            try
            {
                sql = sqlHelper.MakeSQL(
                    "SELECT ServerName, ServerIp, Id, Msisdn, RequestUri, RequestBody, RequestHeaders, ClienIipAddress, ResponseBody,"
                    + "ResponSestatusCode, ResponseHeaders,  CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, IsDeleted, DeletedBy,"
                    + "DeletedDate, IsActive, SerialNo FROM AppRequestResponseLog WHERE IsDeleted=$b $q", false,
                    whereClause);
                //totalCount = 0;
                countSQL = "SELECT COUNT(1) FROM (" + sql + ") CountSQL";
                var objTotalCount = await sqlHelper.ExecuteScalarAsync(countSQL);

                //if (!(objTotalCount == DBNull.Value || objTotalCount == null))
                //	totalCount = Convert.ToInt32(objTotalCount);

                //sql= DACommon.AddPagingQuery(sql, pageIndex, numberOfRecordsPerPage, orderByQuery);


                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(appRequestResponseLogs, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return appRequestResponseLogs;
        }

        private BEAppRequestResponseLogs AddToCollection(BEAppRequestResponseLogs appRequestResponseLogs,
            DbDataReader reader)
        {
            try
            {
                var nullHandler = new NULLHandler(reader);
                while (reader.Read()) appRequestResponseLogs.Add(PreaperObject(nullHandler));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return appRequestResponseLogs;
        }

        private BEAppRequestResponseLog PreaperObject(NULLHandler nullHandler)
        {
            var appRequestResponseLog = new BEAppRequestResponseLog();

            try
            {
                //appRequestResponseLog.IsNew = false;
                //appRequestResponseLog.ServerName = nullHandler.GetString("ServerName");
                //appRequestResponseLog.ServerIp = nullHandler.GetString("ServerIp");
                //appRequestResponseLog.Id = nullHandler.GetInt("Id");
                //appRequestResponseLog.Msisdn = nullHandler.GetString("Msisdn");
                //appRequestResponseLog.RequestUri = nullHandler.GetString("RequestUri");
                //appRequestResponseLog.RequestBody = nullHandler.GetString("RequestBody");
                //appRequestResponseLog.RequestHeaders = nullHandler.GetString("RequestHeaders");
                //appRequestResponseLog.ClienIipAddress = nullHandler.GetString("ClienIipAddress");
                //appRequestResponseLog.ResponseBody = nullHandler.GetString("ResponseBody");
                //appRequestResponseLog.ResponSestatusCode = nullHandler.GetString("ResponSestatusCode");
                //appRequestResponseLog.ResponseHeaders = nullHandler.GetString("ResponseHeaders");
                //appRequestResponseLog.IsDeleted = nullHandler.GetBoolean("IsDeleted");
                //appRequestResponseLog.IsActive = nullHandler.GetBoolean("IsActive");
                //appRequestResponseLog.CreatedBy = nullHandler.GetInt("CreatedBy");
                //appRequestResponseLog.CreatedDate = nullHandler.GetDateTime("CreatedDate");
                //appRequestResponseLog.UpdatedBy = nullHandler.GetInt("UpdatedBy");
                //appRequestResponseLog.UpdatedDate = nullHandler.GetDateTime("UpdatedDate");
                //appRequestResponseLog.DeletedBy = nullHandler.GetInt("DeletedBy");
                //appRequestResponseLog.DeletedDate = nullHandler.GetDateTime("DeletedDate");
                //appRequestResponseLog.SerialNo = nullHandler.GetInt("SerialNo");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return appRequestResponseLog;
        }

        #endregion
    }

    #endregion
}