using BusinessEntity.OGate;
using SQLFactory;
using System;
using System.Data.Common;
using System.Threading.Tasks;

namespace DataAccess
{
    //   #region Object of DACpidHistory
    //   public class DACpidHistory
    //   {
    //       #region Constructor
    //       public DACpidHistory() { }
    //       #endregion

    //       #region Destructor
    //       ~DACpidHistory() { }
    //       #endregion

    //       #region Save
    //       public async Task Save(SQLHelper sqlHelper, BECpidHistory cpidHistory)
    //       {
    //           try
    //           {
    //               string sql = string.Empty;

    //               if (cpidHistory.IsNew)
    //               {
    //                   sql = sqlHelper.MakeSQL("INSERT INTO tblCpidHistory(id, msisdn, cpid, ttl, appid) VALUES (SQ_TBLCPIDHISTORY.nextval, $s, $s, $n, $s)",
    //                        cpidHistory.Msisdn, cpidHistory.Cpid, cpidHistory.Ttl, cpidHistory.AppId);

    //               }
    //               else
    //               {
    //                   sql = sqlHelper.MakeSQL("UPDATE CpidHistory SET msisdn = $s, createdate = $D, cpid = $s, ttl = $n, appid = $s"
    //                       + " WHERE id=$n ", cpidHistory.Msisdn, cpidHistory.CreateDate, cpidHistory.Cpid, cpidHistory.Ttl, cpidHistory.AppId,
    //                         cpidHistory.Id);
    //               }

    //               await sqlHelper.ExecuteNonQueryAsync(sql);
    //           }
    //           catch (Exception ex)
    //           {
    //               throw new Exception(ex.Message);
    //           }
    //       }
    //       #endregion

    //       #region Delete
    //       public async Task Delete(SQLHelper sqlHelper, int id, int deletedBy)
    //       {
    //           try
    //           {
    //               string sql = string.Empty;
    //               sql = sqlHelper.MakeSQL("UPDATE CpidHistory SET IsDeleted=$b, DeletedBy=$n , DeletedDate=sysdate  WHERE id=$n ", true, deletedBy, id);
    //               await sqlHelper.ExecuteNonQueryAsync(sql);
    //           }
    //           catch (Exception ex)
    //           {
    //               throw new Exception(ex.Message);
    //           }
    //       }
    //       #endregion

    //       #region Get
    //       public BECpidHistory GetCpidHistory(SQLHelper sqlHelper, int id)
    //       {
    //           string sql = string.Empty;
    //           BECpidHistorys cpidHistorys = new BECpidHistorys();

    //           try
    //           {
    //               sql = sqlHelper.MakeSQL("SELECT id, msisdn, createdate, cpid, ttl, appid,  CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, IsDeleted,"
    //                   + "DeletedBy, DeletedDate, IsActive, SerialNo FROM CpidHistory WHERE id=$n ", id);

    //               DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
    //               AddToCollection(cpidHistorys, reader);
    //               reader.Close();
    //           }
    //           catch (Exception ex)
    //           {
    //               throw new Exception(ex.Message);
    //           }
    //           if (cpidHistorys.Count > 0)
    //           {
    //               return cpidHistorys[0];
    //           }
    //           else return new BECpidHistory();
    //       }

    //       public BECpidHistorys GetCpidHistorys(SQLHelper sqlHelper, bool activeOnly)
    //       {
    //           string sql = string.Empty;
    //           string whereClause = string.Empty;
    //           BECpidHistorys cpidHistorys = new BECpidHistorys();

    //           try
    //           {
    //               if (activeOnly)
    //                   whereClause += sqlHelper.MakeSQL(" AND IsActive = $b", activeOnly);
    //               sql = sqlHelper.MakeSQL("SELECT id, msisdn, createdate, cpid, ttl, appid,  CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, IsDeleted,"
    //                   + "DeletedBy, DeletedDate, IsActive, SerialNo FROM CpidHistory WHERE IsDeleted=$b $q", false, whereClause);

    //               DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
    //               AddToCollection(cpidHistorys, reader);
    //               reader.Close();
    //           }
    //           catch (Exception ex)
    //           {
    //               throw new Exception(ex.Message);
    //           }
    //           return cpidHistorys;
    //       }

    //       public BECpidHistorys GetCpidHistorys(SQLHelper sqlHelper, BECpidHistory searchCriteria,
    //int pageIndex, int numberOfRecordsPerPage, string orderByQuery, out int totalCount)
    //       {
    //           string sql = string.Empty;
    //           string countSQL = string.Empty;
    //           string whereClause = string.Empty;
    //           BECpidHistorys cpidHistorys = new BECpidHistorys();

    //           try
    //           {
    //               sql = sqlHelper.MakeSQL("SELECT id, msisdn, createdate, cpid, ttl, appid,  CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, IsDeleted,"
    //                   + "DeletedBy, DeletedDate, IsActive, SerialNo FROM CpidHistory WHERE IsDeleted=$b $q", false, whereClause);
    //               totalCount = 0;
    //               countSQL = "SELECT COUNT(1) FROM (" + sql + ") CountSQL";
    //               object objTotalCount = await sqlHelper.ExecuteScalarAsync(countSQL);

    //               if (!(objTotalCount == DBNull.Value || objTotalCount == null))
    //                   totalCount = Convert.ToInt32(objTotalCount);

    //               //sql= DACommon.AddPagingQuery(sql, pageIndex, numberOfRecordsPerPage, orderByQuery);


    //               DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
    //               AddToCollection(cpidHistorys, reader);
    //               reader.Close();
    //           }
    //           catch (Exception ex)
    //           {
    //               throw new Exception(ex.Message);
    //           }
    //           return cpidHistorys;
    //       }

    //       private BECpidHistorys AddToCollection(BECpidHistorys cpidHistorys, DbDataReader reader)
    //       {
    //           try
    //           {
    //               NULLHandler nullHandler = new NULLHandler(reader);
    //               while (reader.Read())
    //               {
    //                   cpidHistorys.Add(PreaperObject(nullHandler));
    //               }
    //           }
    //           catch (Exception ex)
    //           {
    //               throw new Exception(ex.Message);
    //           }
    //           return cpidHistorys;
    //       }

    //       private BECpidHistory PreaperObject(NULLHandler nullHandler)
    //       {
    //           BECpidHistory cpidHistory = new BECpidHistory();

    //           try
    //           {
    //               cpidHistory.IsNew = false;
    //               cpidHistory.Id = nullHandler.GetInt("id");
    //               cpidHistory.Msisdn = nullHandler.GetString("msisdn");
    //               cpidHistory.CreateDate = nullHandler.GetDateTime("createdate");
    //               cpidHistory.Cpid = nullHandler.GetString("cpid");
    //               cpidHistory.Ttl = nullHandler.GetInt("ttl");
    //               cpidHistory.AppId = nullHandler.GetString("appid");
    //           }
    //           catch (Exception ex)
    //           {
    //               throw new Exception(ex.Message);
    //           }
    //           return cpidHistory;
    //       }
    //       #endregion

    //   }
    //   #endregion
    public class DACpid
    {
        public async Task<string> GetCpid(SQLHelper sqlHelper, string msisdn)
        {
            string cpID = string.Empty;
            try
            {
                var sql = sqlHelper.MakeSQL("select cpid from tblcpid where msisdn = $s", msisdn);
                var obj = await sqlHelper.ExecuteScalarAsync(sql);
                cpID = obj != DBNull.Value ? Convert.ToString(obj) : null;

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return cpID;
        }
        public async Task<string> GetRegisteredCpid(SQLHelper sqlHelper, string msisdn)
        {
            string cpID = string.Empty;
            try
            {
                var sql = sqlHelper.MakeSQL("select nvl(registercpid,cpid) cpid from tblcpid where msisdn  = $s", msisdn);
                var obj = await sqlHelper.ExecuteScalarAsync(sql);
                cpID = obj != DBNull.Value ? Convert.ToString(obj) : null;

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return cpID;
        }

        public async Task Save(SQLHelper sqlHelper, BECpid cpid)
        {
            try
            {
                var sql = sqlHelper.MakeSQL("select count(id) from tblcpid where msisdn = $s",
                    cpid.Msisdn);
                //DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
                //BECpids cpids = new BECpids();
                //AddToCollection(cpids, reader);
                //reader.Close();

                var hasCPIDinDB = await sqlHelper.ExecuteScalarAsync(sql);
                if (cpid.isRegisterd)
                {
                    if (!(hasCPIDinDB == null || hasCPIDinDB == DBNull.Value || Convert.ToInt32(hasCPIDinDB) == 0))
                        sql = sqlHelper.MakeSQL("UPDATE tblCpid SET UpdateDateRegister = sysdate, RegisterCpid = $s, StaleTime=$d"
                                                + " WHERE msisdn=$s ", cpid.RegisterCpid, cpid.StaleTime,
                            cpid.Msisdn);
                    else
                        sql = sqlHelper.MakeSQL(
                            "INSERT INTO tblCpid(id, msisdn, REGISTERCPID,StaleTime,UpdateDateRegister) VALUES (SQ_TBLCPID.nextval, $s,$n, $s,$d,sysdate)",
                            cpid.Msisdn, cpid.RegisterCpid, cpid.StaleTime);
                }
                else
                {
                    if (!(hasCPIDinDB == null || hasCPIDinDB == DBNull.Value || Convert.ToInt32(hasCPIDinDB) == 0))
                        sql = sqlHelper.MakeSQL("UPDATE tblCpid SET UPDATEDATE = sysdate, cpid = $s,ttl=$n, provider=$s, CpidExpireDate = $D "
                                                + " WHERE msisdn=$s ",  cpid.Cpid, cpid.Ttl, cpid.Provider, cpid.CpidExpireDate, cpid.Msisdn);
                    else
                        sql = sqlHelper.MakeSQL(
                            "INSERT INTO tblCpid(id, msisdn, cpid, ttl, provider,CpidExpireDate) VALUES (SQ_TBLCPID.nextval, $s, $s,$s,$s,$D)",
                            cpid.Msisdn, cpid.Cpid, cpid.Ttl, cpid.Provider,cpid.CpidExpireDate);
                }


                await sqlHelper.ExecuteNonQueryAsync(sql);
                if (cpid.isRegisterd)
                {
                    sql = sqlHelper.MakeSQL(
                  "INSERT INTO tblCpidHistory(id, msisdn, cpid, ttl, appid,provider,ISREGISTER) VALUES (SQ_TBLCPIDHISTORY.nextval, $s, $s, $n, $s,$s,$n)",
                  cpid.Msisdn, cpid.RegisterCpid, cpid.Ttl, cpid.AppId, cpid.Provider, 1);
                }
                else
                {
                    sql = sqlHelper.MakeSQL(
                   "INSERT INTO tblCpidHistory(id, msisdn, cpid, ttl, appid,provider,ISREGISTER) VALUES (SQ_TBLCPIDHISTORY.nextval, $s, $s, $n, $s,$s,$n)",
                   cpid.Msisdn, cpid.Cpid, cpid.Ttl, cpid.AppId, cpid.Provider, 0);
                }

                await sqlHelper.ExecuteNonQueryAsync(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private BECpids AddToCollection(BECpids cpids, DbDataReader reader)
        {
            try
            {
                var nullHandler = new NULLHandler(reader);
                while (reader.Read()) cpids.Add(PreaperObject(nullHandler));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return cpids;
        }

        private BECpid PreaperObject(NULLHandler nullHandler)
        {
            var cpid = new BECpid();

            try
            {
                cpid.IsNew = false;
                cpid.Id = nullHandler.GetInt("id");
                cpid.Msisdn = nullHandler.GetString("msisdn");
                cpid.CreateDate = nullHandler.GetDateTime("createdate");
                cpid.Cpid = nullHandler.GetString("cpid");
                cpid.Provider = nullHandler.GetString("provider");
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return cpid;
        }
    }
}