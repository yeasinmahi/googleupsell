using System;
using System.Data.Common;
using System.Threading.Tasks;
using BusinessEntity;
using SQLFactory;

namespace DataAccess
{
    #region Object of DAServiceUserAPI

    public class DAServiceUserAPI
    {
        #region Destructor

        ~DAServiceUserAPI()
        {
        }

        #endregion

        #region Save

        public async Task Save(SQLHelper sqlHelper, BEServiceUserAPI serviceUserAPI)
        {
            try
            {
                var sql = string.Empty;

                if (serviceUserAPI.IsNew)
                    sql = sqlHelper.MakeSQL(
                        "INSERT INTO ServiceUserAPI(ServiceUserServiceAPIID, UserID, ServiceAPIID, IsDeleted,"
                        + " IsActive, CreatedBy, CreatedDate, SerialNo) VALUES ($n, $n, $n, $b, $b, $n, $D, $n)",
                        serviceUserAPI.ServiceUserServiceAPIID,
                        serviceUserAPI.UserID, serviceUserAPI.ServiceAPIID, serviceUserAPI.IsDeleted,
                        serviceUserAPI.IsActive,
                        serviceUserAPI.CreatedBy, serviceUserAPI.CreatedDate, serviceUserAPI.SerialNo);
                else
                    sql = sqlHelper.MakeSQL(
                        "UPDATE ServiceUserAPI SET UserID = $n, ServiceAPIID = $n, UpdatedDate = $D, UpdatedBy = $n,"
                        + " IsActive=$b"
                        + " WHERE ServiceUserServiceAPIID=$n ", serviceUserAPI.UserID, serviceUserAPI.ServiceAPIID,
                        serviceUserAPI.UpdatedDate,
                        serviceUserAPI.UpdatedBy, serviceUserAPI.IsActive, serviceUserAPI.ServiceUserServiceAPIID);

                await sqlHelper.ExecuteNonQueryAsync(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Delete

        public async Task Delete(SQLHelper sqlHelper, int serviceUserAPIServiceAPIID, int deletedBy)
        {
            try
            {
                var sql = string.Empty;
                sql = sqlHelper.MakeSQL(
                    "UPDATE ServiceUserAPI SET IsDeleted=$b, DeletedBy=$n , DeletedDate=sysdate  WHERE ServiceUserServiceAPIID=$n ",
                    true, deletedBy, serviceUserAPIServiceAPIID);
                await sqlHelper.ExecuteNonQueryAsync(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Constructor

        #endregion

        #region Get

        public async Task<BEServiceUserAPI> GetServiceUserAPI(SQLHelper sqlHelper, int serviceUserAPIServiceAPIID)
        {
            var sql = string.Empty;
            var serviceUserAPIs = new BEServiceUserAPIs();

            try
            {
                sql = sqlHelper.MakeSQL(
                    "SELECT ServiceUserServiceAPIID, UserID, ServiceAPIID,  CreatedBy, CreatedDate, UpdatedBy, UpdatedDate,"
                    + "IsDeleted, DeletedBy, DeletedDate, IsActive, SerialNo FROM ServiceUserAPI WHERE ServiceUserServiceAPIID=$n ",
                    serviceUserAPIServiceAPIID);

                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(serviceUserAPIs, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (serviceUserAPIs.Count > 0)
                return serviceUserAPIs[0];
            return new BEServiceUserAPI();
        }

        public async Task<BEServiceUserAPIs> GetServiceUserAPIs(SQLHelper sqlHelper, bool activeOnly)
        {
            var sql = string.Empty;
            var whereClause = string.Empty;
            var serviceUserAPIs = new BEServiceUserAPIs();

            try
            {
                if (activeOnly)
                    //whereClause += sqlHelper.MakeSQL(" AND IsActive = $b", activeOnly);
                    sql = sqlHelper.MakeSQL(@"SELECT a.SERVICEUSERSERVICEAPIID, a.UserID, a.ServiceAPIID, b.apiname
FROM tblServiceUserAPI a inner join tblserviceapi b
on a.serviceapiid = b.id",
                        whereClause);

                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(serviceUserAPIs, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return serviceUserAPIs;
        }

        public async Task<BEServiceUserAPIs> GetServiceUserAPIs(SQLHelper sqlHelper, BEServiceUserAPI searchCriteria,
            int pageIndex, int numberOfRecordsPerPage, string orderByQuery //, out int totalCount
        )
        {
            var sql = string.Empty;
            var countSQL = string.Empty;
            var whereClause = string.Empty;
            var serviceUserAPIs = new BEServiceUserAPIs();

            try
            {
                sql = sqlHelper.MakeSQL(
                    "SELECT ServiceUserServiceAPIID, UserID, ServiceAPIID,  CreatedBy, CreatedDate, UpdatedBy, UpdatedDate,"
                    + "IsDeleted, DeletedBy, DeletedDate, IsActive, SerialNo FROM ServiceUserAPI WHERE IsDeleted=$b $q",
                    false,
                    whereClause);
                //totalCount = 0;
                countSQL = "SELECT COUNT(1) FROM (" + sql + ") CountSQL";
                var objTotalCount = await sqlHelper.ExecuteScalarAsync(countSQL);

                //if (!(objTotalCount == DBNull.Value || objTotalCount == null))
                //	totalCount = Convert.ToInt32(objTotalCount);

                //sql= DACommon.AddPagingQuery(sql, pageIndex, numberOfRecordsPerPage, orderByQuery);


                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(serviceUserAPIs, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return serviceUserAPIs;
        }

        private BEServiceUserAPIs AddToCollection(BEServiceUserAPIs serviceUserAPIs, DbDataReader reader)
        {
            try
            {
                var nullHandler = new NULLHandler(reader);
                while (reader.Read()) serviceUserAPIs.Add(PreaperObject(nullHandler));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return serviceUserAPIs;
        }

        private BEServiceUserAPI PreaperObject(NULLHandler nullHandler)
        {
            var serviceUserAPI = new BEServiceUserAPI();

            try
            {
                serviceUserAPI.IsNew = false;
                serviceUserAPI.ServiceUserServiceAPIID = nullHandler.GetInt("ServiceUserServiceAPIID");
                serviceUserAPI.UserID = nullHandler.GetInt("UserID");
                serviceUserAPI.ServiceAPIID = nullHandler.GetInt("ServiceAPIID");
                serviceUserAPI.APIName = nullHandler.GetString("APIName");
                //serviceUserAPI.IsDeleted = nullHandler.GetBoolean("IsDeleted");
                //serviceUserAPI.IsActive = nullHandler.GetBoolean("IsActive");
                //serviceUserAPI.CreatedBy = nullHandler.GetString("CreatedBy");
                //serviceUserAPI.CreatedDate = nullHandler.GetDateTime("CreatedDate");
                //serviceUserAPI.UpdatedBy = nullHandler.GetString("UpdatedBy");
                //serviceUserAPI.UpdatedDate = nullHandler.GetDateTime("UpdatedDate");
                //serviceUserAPI.DeletedBy = nullHandler.GetString("DeletedBy");
                //serviceUserAPI.DeletedDate = nullHandler.GetDateTime("DeletedDate");
                //serviceUserAPI.SerialNo = nullHandler.GetInt("SerialNo");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return serviceUserAPI;
        }

        #endregion
    }

    #endregion
}