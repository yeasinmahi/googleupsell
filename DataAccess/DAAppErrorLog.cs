using System;
using System.Threading.Tasks;
using BusinessEntity.Logging;
using SQLFactory;

namespace DataAccess
{
    #region Object of DAAppErrorLog

    public class DAAppErrorLog
    {
        #region Destructor

        ~DAAppErrorLog()
        {
        }

        #endregion

        //#region Delete
        //public async Task Delete(SQLHelper sqlHelper, , int deletedBy)
        //{
        //	try
        //	{
        //		string sql=string.Empty;
        //		sql = sqlHelper.MakeSQL("UPDATE AppErrorLog SET IsDeleted=$b, DeletedBy=$n , DeletedDate=sysdate  WHERE ", true, deletedBy, );
        //		await sqlHelper.ExecuteNonQueryAsync(sql);
        //	}
        //	catch(Exception ex)
        //	{
        //		throw new Exception(ex.Message);
        //	}
        //}
        //#endregion

        #region Get

        //public BEAppErrorLog GetAppErrorLog(SQLHelper sqlHelper )
        //{
        //	string sql = string.Empty;
        //	BEAppErrorLogs appErrorLogs = new BEAppErrorLogs();

        //	try
        //	{
        //		sql = sqlHelper.MakeSQL("SELECT Id, Msisdn, LogTime, Message, Stacktrace, RequestMethod, RequestFrom, RequestUrl, Request, Response,"
        //			+ "Rtt,  CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, IsDeleted, DeletedBy, DeletedDate, IsActive, SerialNo FROM AppErrorLog WHERE ",
        //			);

        //		DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
        //		AddToCollection(appErrorLogs, reader);
        //		reader.Close();
        //	}
        //	catch(Exception ex)
        //	{
        //		throw new Exception(ex.Message);
        //	}
        //	if (appErrorLogs.Count > 0)
        //	{
        //		return appErrorLogs[0];
        //	}
        //	else return new BEAppErrorLog();
        //}

        //	public BEAppErrorLogs GetAppErrorLogs(SQLHelper sqlHelper, bool activeOnly)
        //	{
        //		string sql = string.Empty;
        //		string whereClause = string.Empty;
        //		BEAppErrorLogs appErrorLogs = new BEAppErrorLogs();

        //		try
        //		{
        //			if (activeOnly)
        //				whereClause += sqlHelper.MakeSQL(" AND IsActive = $b", activeOnly);
        //			sql = sqlHelper.MakeSQL("SELECT Id, Msisdn, LogTime, Message, Stacktrace, RequestMethod, RequestFrom, RequestUrl, Request, Response,"
        //				+ "Rtt,  CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, IsDeleted, DeletedBy, DeletedDate, IsActive, SerialNo FROM AppErrorLog WHERE IsDeleted=$b $q",
        //				false, whereClause);

        //			DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
        //			AddToCollection(appErrorLogs, reader);
        //			reader.Close();
        //		}
        //		catch(Exception ex)
        //		{
        //			throw new Exception(ex.Message);
        //		}
        //		return appErrorLogs;
        //	}

        //	public BEAppErrorLogs GetAppErrorLogs(SQLHelper sqlHelper , BEAppErrorLog searchCriteria, 
        //int pageIndex, int numberOfRecordsPerPage, string orderByQuery, out int totalCount)
        //	{
        //		string sql = string.Empty;
        //		string countSQL = string.Empty;
        //		string whereClause = string.Empty;
        //		BEAppErrorLogs appErrorLogs = new BEAppErrorLogs();

        //		try
        //		{
        //			sql = sqlHelper.MakeSQL("SELECT Id, Msisdn, LogTime, Message, Stacktrace, RequestMethod, RequestFrom, RequestUrl, Request, Response,"
        //				+ "Rtt,  CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, IsDeleted, DeletedBy, DeletedDate, IsActive, SerialNo FROM AppErrorLog WHERE IsDeleted=$b $q",
        //				false, whereClause);
        //			totalCount = 0;
        //			countSQL = "SELECT COUNT(1) FROM (" + sql + ") CountSQL";
        //			object objTotalCount = await sqlHelper.ExecuteScalarAsync(countSQL);

        //			if (!(objTotalCount == DBNull.Value || objTotalCount == null))
        //				totalCount = Convert.ToInt32(objTotalCount);

        //			//sql= DACommon.AddPagingQuery(sql, pageIndex, numberOfRecordsPerPage, orderByQuery);


        //			DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
        //			AddToCollection(appErrorLogs, reader);
        //			reader.Close();
        //		}
        //		catch(Exception ex)
        //		{
        //			throw new Exception(ex.Message);
        //		}
        //		return appErrorLogs;
        //	}

        //	private BEAppErrorLogs AddToCollection(BEAppErrorLogs appErrorLogs, DbDataReader reader)
        //	{
        //		try
        //		{
        //			NULLHandler nullHandler = new NULLHandler(reader);
        //			while (reader.Read())
        //			{
        //				appErrorLogs.Add(PreaperObject(nullHandler));
        //			}
        //		}
        //		catch(Exception ex)
        //		{
        //			throw new Exception(ex.Message);
        //		}
        //		return appErrorLogs;
        //	}

        private BEAppErrorLog PreaperObject(NULLHandler nullHandler)
        {
            var appErrorLog = new BEAppErrorLog();

            try
            {
                //appErrorLog.Id = nullHandler.GetString("Id");
                //appErrorLog.Msisdn = nullHandler.GetString("Msisdn");
                //appErrorLog.LogTime = nullHandler.GetDateTime("LogTime");
                //appErrorLog.Message = nullHandler.GetString("Message");
                //appErrorLog.Stacktrace = nullHandler.GetString("Stacktrace");
                //appErrorLog.RequestMethod = nullHandler.GetString("RequestMethod");
                //appErrorLog.RequestFrom = nullHandler.GetString("RequestFrom");
                //appErrorLog.RequestUrl = nullHandler.GetString("RequestUrl");
                //appErrorLog.Request = nullHandler.GetString("Request");
                //appErrorLog.Response = nullHandler.GetString("Response");
                //appErrorLog.Rtt = nullHandler.GetInt("Rtt");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return appErrorLog;
        }

        #endregion

        #region Constructor

        #endregion

        #region Save

        public async Task Create(SQLHelper sqlHelper, BEAppErrorLog log)
        {
            try
            {
                var sql = string.Empty;


                var logIDSQL = sqlHelper.MakeSQL(@"SELECT SQ_AppErrorLog.NEXTVAL FROM DUAL");


                //object obj = await sqlHelper.ExecuteScalarAsync(logIDSQL);
                //if (!(obj == DBNull.Value || obj == null))
                //    log.ID = Convert.ToInt32(obj);


                //sql = @" INSERT INTO AppErrorLog ( ID,MSISDN,LogTime,Message,StackTrace,RequestMethod,RequestFrom,RTT,IsActive,RequestURL,Request,Response ) "
                //     + " VALUES ( :LOOGID33, :MSISDN33, :LOGTIME33, :MESSAGE33, :STACKTRACE33, :REQUESTMETHOD33, :REQUESTFROM33, :RTT33, :ISACTIVE33,:REQUESTURL33, :REQUEST33, :RESPONSE33)";

                //object[] objParam = { log.ID, log.MSISDN, log.LogTime, log.Message, log.StackTrace, log.RequestMethod, log.RequestFrom, log.RTT, log.IsActive, log.RequestURL, log.Request, log.Response };

                //DbDataReader reader = await sqlHelper.ExecuteQueryAsCommandAsync(
                //    sql,
                //    new string[] { "LOOGID33|N", "MSISDN33|S", "LOGTIME33|D", "MESSAGE33|NS", "STACKTRACE33|NS", "REQUESTMETHOD33|S", "REQUESTFROM33|S", "RTT33|N", "ISACTIVE33|B", "REQUESTURL33|S", "REQUEST33|NS", "RESPONSE33|NS" }
                //    , objParam);


                sql =
                    @" INSERT INTO AppErrorLog (ID, MSISDN,LogTime,Message,StackTrace,RequestMethod,RequestFrom,RTT,RequestURL,Request,Response ) "
                    + " VALUES (SQ_AppErrorLog.NEXTVAL, :MSISDN33, :LOGTIME33, :MESSAGE33, :STACKTRACE33, :REQUESTMETHOD33, :REQUESTFROM33, :RTT33, :REQUESTURL33, :REQUEST33, :RESPONSE33)";

                object[] objParam =
                {
                    log.Msisdn, log.LogTime, log.Message, log.Stacktrace, log.RequestMethod, log.RequestFrom, log.Rtt,
                    log.RequestUrl, log.Request, log.Response
                };

                var reader = await sqlHelper.ExecuteQueryAsCommandAsync(
                    sql,
                    new[]
                    {
                        "MSISDN33|S", "LOGTIME33|D", "MESSAGE33|NS", "STACKTRACE33|NS", "REQUESTMETHOD33|S",
                        "REQUESTFROM33|S", "RTT33|N", "REQUESTURL33|S", "REQUEST33|NS", "RESPONSE33|NS"
                    }
                    , objParam);

                reader.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task Save(SQLHelper sqlHelper, BEAppErrorLog appErrorLog)
        {
            try
            {
                if (!string.IsNullOrEmpty(appErrorLog.Message) && appErrorLog.Message.Length >= 4000)
                    appErrorLog.Message = appErrorLog.Message.Substring(0, 4000);
                if (!string.IsNullOrEmpty(appErrorLog.Stacktrace) && appErrorLog.Stacktrace.Length >= 4000)
                    appErrorLog.Stacktrace = appErrorLog.Stacktrace.Substring(0, 4000);
                if (!string.IsNullOrEmpty(appErrorLog.Request) && appErrorLog.Request.Length >= 4000)
                    appErrorLog.Request = appErrorLog.Request.Substring(0, 4000);
                var sql = sqlHelper.MakeSQL(
                    "INSERT INTO AppErrorLog(Id, Msisdn, LogTime, Message, Stacktrace, RequestMethod,"
                    + " RequestFrom, RequestUrl, Request, Response, Rtt) VALUES (SQ_APPERRORLOG.nextval, $s, $D, $s, $s, $s, $s, $s, $s, $s, $n)",
                    appErrorLog.Msisdn, appErrorLog.LogTime, appErrorLog.Message, appErrorLog.Stacktrace,
                    appErrorLog.RequestMethod,
                    appErrorLog.RequestFrom, appErrorLog.RequestUrl, appErrorLog.Request, appErrorLog.Response,
                    appErrorLog.Rtt);

                await sqlHelper.ExecuteNonQueryAsync(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion
    }

    #endregion
}