﻿//using BusinessEntity.APIHub2;
//using SQLFactory;
//using System;
//using System.Data.Common;
//using Utility;

//namespace DataAccess
//{
//    public class DAInterest
//    {
//        ~DAInterest() { }
//        public BEInterests GetInterests(SQLHelper sqlHelper)
//        {
//            string sql = string.Empty;
//            BEInterests Interests = new BEInterests();
//            try
//            {
//                sql = sqlHelper.MakeSQL(@"SELECT * FROM TblInterest");
//                DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
//                AddToCollection(Interests, reader);
//                reader.Close();
//            }
//            catch (Exception ex)
//            {
//                throw new Exception(ex.Message);
//            }
//            return Interests;
//        }

//        public BEInterest GetInterest(SQLHelper sqlHelper, int InterestId)
//        {
//            string sql = string.Empty;
//            BEInterests Interests = new BEInterests();
//            try
//            {
//                sql = sqlHelper.MakeSQL(@"SELECT * FROM TblInterest where InterestId = $n", InterestId);

//                DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
//                AddToCollection(Interests, reader);
//                reader.Close();
//            }
//            catch (Exception ex)
//            {
//                throw new Exception(ex.Message);
//            }

//            if (Interests.Count > 0)
//                return Interests[0];
//            else return new BEInterest();
//        }

//        public BEInterests GetInterestsByCustomerId(SQLHelper sqlHelper, int customerId)
//        {
//            string sql = string.Empty;
//            BEInterests Interests = new BEInterests();
//            try
//            {
//                sql = sqlHelper.MakeSQL(@"SELECT I.* FROM TblInterest I INNER JOIN TBLCUSTOMERINTEREST C ON C.INTERESTID  = I.INTERESTID WHERE C.CUSTOMERID = $n", customerId);
//                DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
//                AddToCollection(Interests, reader);
//                reader.Close();
//            }
//            catch (Exception ex)
//            {
//                throw new Exception(ex.Message);
//            }
//            return Interests;
//        }

//        public BEInterests GetIconInterestsByCustomerId(SQLHelper sqlHelper, int customerId)
//        {
//            string sql = string.Empty;
//            BEInterests Interests = new BEInterests();
//            try
//            {
//                sql = sqlHelper.MakeSQL(@"SELECT I.* FROM TblInterest I INNER JOIN TBLICONCUSTOMERINTEREST C ON C.INTERESTID  = I.INTERESTID WHERE C.CUSTOMERID = $n", customerId);
//                DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
//                AddToCollection(Interests, reader);
//                reader.Close();
//            }
//            catch (Exception ex)
//            {
//                throw new Exception(ex.Message);
//            }
//            return Interests;
//        }

//        private BEInterests AddToCollection(BEInterests Interests, DbDataReader reader)
//        {
//            while (reader.Read())
//            {
//                BEInterest inerest = new BEInterest();
//                inerest = (BEInterest) new CustomObject().MapItem(inerest, reader);
//                Interests.Add(inerest);
//            }
//            return Interests;
//        }

//        public async Task SaveCustomerInterest(SQLHelper sqlHelper, BEInterests interests, int customerId)
//        {
//            string sql = string.Empty;
//            try
//            {
//                sql = sqlHelper.MakeSQL(@"DELETE FROM TBLCUSTOMERINTEREST WHERE CUSTOMERID=$n", customerId);
//                await sqlHelper.ExecuteNonQueryAsync(sql);

//                foreach (BEInterest interest in interests)
//                {
//                    sql = sqlHelper.MakeSQL(@"INSERT INTO TBLCUSTOMERINTEREST VALUES($n,$n)", customerId, interest.InterestId);
//                    await sqlHelper.ExecuteNonQueryAsync(sql);
//                }
//            }
//            catch (Exception ex)
//            {
//                throw new Exception(ex.Message);
//            }
//        }

//        public async Task SaveIconCustomerInterest(SQLHelper sqlHelper, BEInterests interests, int customerId)
//        {
//            string sql = string.Empty;
//            try
//            {
//                sql = sqlHelper.MakeSQL(@"DELETE FROM TBLICONCUSTOMERINTEREST WHERE CUSTOMERID=$n", customerId);
//                await sqlHelper.ExecuteNonQueryAsync(sql);

//                foreach (BEInterest interest in interests)
//                {
//                    sql = sqlHelper.MakeSQL(@"INSERT INTO TBLICONCUSTOMERINTEREST(CUSTOMERID,INTERESTID) VALUES($n,$n)", customerId, interest.InterestId);
//                    await sqlHelper.ExecuteNonQueryAsync(sql);
//                }
//            }
//            catch (Exception ex)
//            {
//                throw new Exception(ex.Message);
//            }
//        }
//    }
//}

