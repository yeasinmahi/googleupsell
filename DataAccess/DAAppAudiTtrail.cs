using System;
using System.Data.Common;
using System.Threading.Tasks;
using BusinessEntity.Logging;
using SQLFactory;

namespace DataAccess
{
    #region Object of DAAppAudiTtrail

    public class DAAppAudiTtrail
    {
        #region Destructor

        ~DAAppAudiTtrail()
        {
        }

        #endregion

        #region Save

        public async Task Save(SQLHelper sqlHelper, BEAppAudiTtrail appAudiTtrail)
        {
            try
            {
                var sql = string.Empty;
                if (!string.IsNullOrEmpty(appAudiTtrail.ErrorMessage) && appAudiTtrail.ErrorMessage.Length > 495)
                    appAudiTtrail.ErrorMessage = appAudiTtrail.ErrorMessage.Substring(0, 495);

                sql = sqlHelper.MakeSQL(
                    "INSERT INTO tblappaudittrail(Id, Msisdn, RequestUrl, Params, IpAddress, RTT, ErrorMessage,"
                    + " RequestQueryString, Method, ApiVersion, ServiceUserId, Provider) VALUES (SQ_TBLAPPAUDITTRAIL.nextval, $s, $s, $s, $s, $n, $s, $s, $s, $s, $n, $s)",
                    appAudiTtrail.Msisdn, appAudiTtrail.RequestUrl, appAudiTtrail.Params, appAudiTtrail.IpAddress,
                    appAudiTtrail.RTT, appAudiTtrail.ErrorMessage, appAudiTtrail.RequestQueryString,
                    appAudiTtrail.Method,
                    appAudiTtrail.ApiVersion, appAudiTtrail.ServiceUserId, appAudiTtrail.Provider);

                await sqlHelper.ExecuteNonQueryAsync(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Delete

        public async Task Delete(SQLHelper sqlHelper, int id, int deletedBy)
        {
            try
            {
                var sql = string.Empty;
                sql = sqlHelper.MakeSQL(
                    "UPDATE AppAudiTtrail SET IsDeleted=$b, DeletedBy=$n , DeletedDate=sysdate  WHERE Id=$n ", true,
                    deletedBy, id);
                await sqlHelper.ExecuteNonQueryAsync(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Constructor

        #endregion

        #region Get

        public async Task<BEAppAudiTtrail> GetAppAudiTtrail(SQLHelper sqlHelper, int id)
        {
            var sql = string.Empty;
            var appAudiTtrails = new BEAppAudiTtrails();

            try
            {
                //sql = sqlHelper.MakeSQL("SELECT Id, Msisdn, RequestUrl, Params, IpAddress, RTT, ErrorMessage, RequestQueryString, Method, ApiVersion,"
                //	+ "ServiceUserId,  CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, IsDeleted, DeletedBy, DeletedDate, IsActive,"
                //	+ "SerialNo FROM AppAudiTtrail WHERE Id=$n ", id);

                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(appAudiTtrails, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (appAudiTtrails.Count > 0)
                return appAudiTtrails[0];
            return new BEAppAudiTtrail();
        }

        public async Task<BEAppAudiTtrails> GetAppAudiTtrails(SQLHelper sqlHelper, bool activeOnly)
        {
            var sql = string.Empty;
            var whereClause = string.Empty;
            var appAudiTtrails = new BEAppAudiTtrails();

            try
            {
                if (activeOnly)
                    whereClause += sqlHelper.MakeSQL(" AND IsActive = $b", activeOnly);
                //sql = sqlHelper.MakeSQL("SELECT Id, Msisdn, RequestUrl, Params, IpAddress, RTT, ErrorMessage, RequestQueryString, Method, ApiVersion,"
                //	+ "ServiceUserId,  CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, IsDeleted, DeletedBy, DeletedDate, IsActive,"
                //	+ "SerialNo FROM AppAudiTtrail WHERE IsDeleted=$b $q", false, whereClause);

                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(appAudiTtrails, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return appAudiTtrails;
        }

        public async Task<BEAppAudiTtrails> GetAppAudiTtrails(SQLHelper sqlHelper, BEAppAudiTtrail searchCriteria,
            int pageIndex, int numberOfRecordsPerPage, string orderByQuery)
        {
            var sql = string.Empty;
            var countSQL = string.Empty;
            var whereClause = string.Empty;
            var appAudiTtrails = new BEAppAudiTtrails();

            try
            {
                sql = sqlHelper.MakeSQL(
                    "SELECT Id, Msisdn, RequestUrl, Params, IpAddress, RTT, ErrorMessage, RequestQueryString, Method, ApiVersion,"
                    + "ServiceUserId,  CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, IsDeleted, DeletedBy, DeletedDate, IsActive,"
                    + "SerialNo FROM AppAudiTtrail WHERE IsDeleted=$b $q", false, whereClause);
                long totalCount = 0;
                countSQL = "SELECT COUNT(1) FROM (" + sql + ") CountSQL";
                var objTotalCount = await sqlHelper.ExecuteScalarAsync(countSQL);

                if (!(objTotalCount == DBNull.Value || objTotalCount == null))
                    totalCount = Convert.ToInt32(objTotalCount);

                //sql= DACommon.AddPagingQuery(sql, pageIndex, numberOfRecordsPerPage, orderByQuery);


                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(appAudiTtrails, reader);
                reader.Close();

                appAudiTtrails.TotalRecords = totalCount;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return appAudiTtrails;
        }

        private BEAppAudiTtrails AddToCollection(BEAppAudiTtrails appAudiTtrails, DbDataReader reader)
        {
            try
            {
                var nullHandler = new NULLHandler(reader);
                while (reader.Read()) appAudiTtrails.Add(PreaperObject(nullHandler));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return appAudiTtrails;
        }

        private BEAppAudiTtrail PreaperObject(NULLHandler nullHandler)
        {
            var appAudiTtrail = new BEAppAudiTtrail();

            try
            {
                //appAudiTtrail.IsNew = false;
                //appAudiTtrail.Id = nullHandler.GetInt("Id");
                //appAudiTtrail.Msisdn = nullHandler.GetString("Msisdn");
                //appAudiTtrail.RequestUrl = nullHandler.GetString("RequestUrl");
                //appAudiTtrail.Params = nullHandler.GetString("Params");
                //appAudiTtrail.IpAddress = nullHandler.GetString("IpAddress");
                //appAudiTtrail.RTT = nullHandler.GetInt("RTT");
                //appAudiTtrail.ErrorMessage = nullHandler.GetString("ErrorMessage");
                //appAudiTtrail.RequestQueryString = nullHandler.GetString("RequestQueryString");
                //appAudiTtrail.Method = nullHandler.GetString("Method");
                //appAudiTtrail.ApiVersion = nullHandler.GetString("ApiVersion");
                //appAudiTtrail.ServiceUserId = nullHandler.GetInt("ServiceUserId");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return appAudiTtrail;
        }

        #endregion
    }

    #endregion
}