﻿using System;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using SQLFactory;

namespace DataAccess
{
    #region Object of DADataProduct

    public class DADataProduct
    {
        #region Destructor

        ~DADataProduct()
        {
        }

        #endregion

        #region Save

        public async Task Save(SQLHelper sqlHelper, BEDataProduct dataProduct)
        {
            try
            {
                var sql = string.Empty;

                if (dataProduct.IsNew)
                    sql = sqlHelper.MakeSQL("INSERT INTO DataProduct(DataProductNameDisplay, DataProductId,"
                                            + " DataProductName, Description, RelatedUrl, DataCode, AutoRenewCode, ShortCode, DisplayOrder,"
                                            + " Price, Volume, Validity, ProductFamilyId, ActivationMessage, DeactivationMessage, IsDeleted,"
                                            + " IsActive, CreatedBy, CreatedDate) VALUES ($s, $n, $s, $s, $s, $s, $s, $s, $n,"
                                            + " $s, $s, $s, $s, $s, $s, $b, $b, $n, $D)",
                        dataProduct.DataProductNameDisplay, dataProduct.DataProductId,
                        dataProduct.DataProductName, dataProduct.Description, dataProduct.RelatedUrl,
                        dataProduct.DataCode, dataProduct.AutoRenewCode, dataProduct.ShortCode,
                        dataProduct.DisplayOrder,
                        dataProduct.Price, dataProduct.Volume, dataProduct.Validity, dataProduct.ProductFamilyId,
                        dataProduct.ActivationMessage,
                        dataProduct.DeactivationMessage, dataProduct.IsDeleted, dataProduct.IsActive,
                        dataProduct.CreatedBy,
                        dataProduct.CreatedDate);
                else
                    sql = sqlHelper.MakeSQL("UPDATE DataProduct SET DataProductNameDisplay = $s, "
                                            + " DataProductName = $s, Description = $s, RelatedUrl = $s, DataCode = $s, AutoRenewCode = $s,"
                                            + " ShortCode = $s, DisplayOrder = $n, Price = $s, Volume = $s, Validity = $s, ProductFamilyId = $s,"
                                            + " ActivationMessage = $s, DeactivationMessage = $s, UpdatedDate = $D, UpdatedBy = $n, IsActive=$b"
                                            + " WHERE DataProductId=$n ", dataProduct.DataProductNameDisplay,
                        dataProduct.DataProductName,
                        dataProduct.Description, dataProduct.RelatedUrl, dataProduct.DataCode,
                        dataProduct.AutoRenewCode,
                        dataProduct.ShortCode, dataProduct.DisplayOrder, dataProduct.Price, dataProduct.Volume,
                        dataProduct.Validity,
                        dataProduct.ProductFamilyId, dataProduct.ActivationMessage, dataProduct.DeactivationMessage,
                        dataProduct.UpdatedDate,
                        dataProduct.UpdatedBy, dataProduct.IsActive, dataProduct.DataProductId);

                await sqlHelper.ExecuteNonQueryAsync(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Delete

        public async Task Delete(SQLHelper sqlHelper, int dataProductId, int deletedBy)
        {
            try
            {
                var sql = string.Empty;
                sql = sqlHelper.MakeSQL(
                    "UPDATE DataProduct SET IsDeleted=$b, DeletedBy=$n , DeletedDate=sysdate  WHERE DataProductId=$n ",
                    true, deletedBy, dataProductId);
                await sqlHelper.ExecuteNonQueryAsync(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion

        #region Constructor

        #endregion

        #region Get

        public async Task<BEDataProduct> GetDataProduct(SQLHelper sqlHelper, int dataProductId)
        {
            var sql = string.Empty;
            var dataProducts = new BEDataProducts();

            try
            {
                sql = sqlHelper.MakeSQL(
                    "SELECT DATA_PRODUCT_NAME_DISPLAY,DATA_PRODUCT_ID,DATA_PRODUCT_TYPE_ID,DATA_PRODUCT_NAME"
                    + " ,DESCRIPTION,RELATED_URL,DATA_CODE,AUTO_RENEW_CODE,SHORT_CODE,ISDELETED,ISACTIVE,CREATEDBY,CREATEDDATE"
                    + " ,UPDATEDBY,UPDATEDDATE,DELETEDBY,DELETEDDATE,DISPLAY_ORDER,PRICE,VOLUME,VALIDITY,PRODUCT_FAMILY_ID"
                    + " ,ACTIVATION_MESSAGE,DEACTIVATION_MESSAGE "
                    + " FROM DATA_PRODUCT"
                    + " WHERE DataProductId=$n ", dataProductId);

                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(dataProducts, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (dataProducts.Count > 0)
                return dataProducts[0];
            return new BEDataProduct();
        }

        public async Task<string> GetRefeilProfileIdByPackCode(SQLHelper sqlHelper, string packcode)
        {
            try
            {
                var sql = sqlHelper.MakeSQL(
                    "select refillprofileid from veon_apps.data_product" 
                    + " where data_code = $s"
                    + " union "
                    + " select refillprofileidrenew refillprofileid from veon_apps.data_product"
                    + " where auto_renew_code = $s", packcode, packcode);

                var obj = await sqlHelper.ExecuteScalarAsync(sql);
                return (obj == null) ? string.Empty : obj.ToString();


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<BEDataProducts> GetDataProducts(SQLHelper sqlHelper, bool activeOnly)
        {
            var sql = string.Empty;
            var whereClause = string.Empty;
            var dataProducts = new BEDataProducts();

            try
            {
                if (activeOnly)
                    whereClause += sqlHelper.MakeSQL(" AND IsActive = $b", activeOnly);
                sql = sqlHelper.MakeSQL(
                    "SELECT DATA_PRODUCT_NAME_DISPLAY,DATA_PRODUCT_ID,DATA_PRODUCT_TYPE_ID,DATA_PRODUCT_NAME"
                    + " ,DESCRIPTION,RELATED_URL,DATA_CODE,AUTO_RENEW_CODE,SHORT_CODE,ISDELETED,ISACTIVE,CREATEDBY,CREATEDDATE"
                    + " ,UPDATEDBY,UPDATEDDATE,DELETEDBY,DELETEDDATE,DISPLAY_ORDER,PRICE,VOLUME,VALIDITY,PRODUCT_FAMILY_ID"
                    + " ,ACTIVATION_MESSAGE,DEACTIVATION_MESSAGE"
                    + " FROM DATA_PRODUCT"
                    + " WHERE IsDeleted=$b $q order by DATA_CODE", false, whereClause);

                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                
                AddToCollection(dataProducts, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return dataProducts;
        }

        public async Task<BEDataProducts> GetDataProducts(SQLHelper sqlHelper, BEDataProduct searchCriteria,
            int pageIndex, int numberOfRecordsPerPage, string orderByQuery)
        {
            var sql = string.Empty;
            var countSQL = string.Empty;
            var whereClause = string.Empty;
            var dataProducts = new BEDataProducts();

            try
            {
                sql = sqlHelper.MakeSQL(
                    "SELECT DATA_PRODUCT_NAME_DISPLAY,DATA_PRODUCT_ID,DATA_PRODUCT_TYPE_ID,DATA_PRODUCT_NAME"
                    + " ,DESCRIPTION,RELATED_URL,DATA_CODE,AUTO_RENEW_CODE,SHORT_CODE,ISDELETED,ISACTIVE,CREATEDBY,CREATEDDATE"
                    + " ,UPDATEDBY,UPDATEDDATE,DELETEDBY,DELETEDDATE,DISPLAY_ORDER,PRICE,VOLUME,VALIDITY,PRODUCT_FAMILY_ID"
                    + " ,ACTIVATION_MESSAGE,DEACTIVATION_MESSAGE "
                    + " FROM DATA_PRODUCT"
                    + " WHERE IsDeleted=$b $q", false, whereClause);
                //totalCount = 0;
                countSQL = "SELECT COUNT(1) FROM (" + sql + ") CountSQL";
                var objTotalCount = await sqlHelper.ExecuteScalarAsync(countSQL);

                //if (!(objTotalCount == DBNull.Value || objTotalCount == null))
                //    totalCount = Convert.ToInt32(objTotalCount);

                //sql= DACommon.AddPagingQuery(sql, pageIndex, numberOfRecordsPerPage, orderByQuery);


                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(dataProducts, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return dataProducts;
        }

        private BEDataProducts AddToCollection(BEDataProducts dataProducts, DbDataReader reader)
        {
            try
            {
                var nullHandler = new NULLHandler(reader);
                while (reader.Read()) dataProducts.Add(PreaperObject(nullHandler));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return dataProducts;
        }

        private BEDataProduct PreaperObject(NULLHandler nullHandler)
        {
            var dataProduct = new BEDataProduct();

            try
            {
                dataProduct.IsNew = false;
                dataProduct.DataProductNameDisplay = nullHandler.GetString("DATA_PRODUCT_NAME_DISPLAY");
                dataProduct.DataProductId = nullHandler.GetInt("DATA_PRODUCT_ID");
                //dataProduct.DataProductTypeId = nullHandler.GetInt("DATA_PRODUCT_TYPE_ID");
                dataProduct.DataProductName = nullHandler.GetString("DATA_PRODUCT_NAME");
                dataProduct.Description = nullHandler.GetString("DESCRIPTION");
                dataProduct.RelatedUrl = nullHandler.GetString("RELATED_URL");
                dataProduct.DataCode = nullHandler.GetString("DATA_CODE");
                dataProduct.AutoRenewCode = nullHandler.GetString("AUTO_RENEW_CODE");
                dataProduct.ShortCode = nullHandler.GetString("SHORT_CODE");
                dataProduct.DisplayOrder = nullHandler.GetInt("DISPLAY_ORDER");
                dataProduct.Price = nullHandler.GetString("PRICE");
                dataProduct.Volume = nullHandler.GetString("VOLUME");
                dataProduct.Validity = nullHandler.GetString("VALIDITY");
                dataProduct.ProductFamilyId = nullHandler.GetInt("PRODUCT_FAMILY_ID");
                dataProduct.ActivationMessage = nullHandler.GetString("ACTIVATION_MESSAGE");
                dataProduct.DeactivationMessage = nullHandler.GetString("DEACTIVATION_MESSAGE");
                dataProduct.IsDeleted = nullHandler.GetBoolean("ISDELETED");
                dataProduct.IsActive = nullHandler.GetBoolean("ISACTIVE");
                dataProduct.CreatedBy = nullHandler.GetString("CREATEDBY");
                dataProduct.CreatedDate = nullHandler.GetDateTime("CREATEDDATE");
                dataProduct.UpdatedBy = nullHandler.GetString("UPDATEDBY");
                dataProduct.UpdatedDate = nullHandler.GetDateTime("UPDATEDDATE");
                dataProduct.DeletedBy = nullHandler.GetString("DELETEDBY");
                dataProduct.DeletedDate = nullHandler.GetDateTime("DELETEDDATE");
                //dataProduct.SerialNo = nullHandler.GetInt("SerialNo");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return dataProduct;
        }

        #endregion
    }

    #endregion
}