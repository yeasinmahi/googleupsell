﻿//using BusinessEntity.APIHub2;
//using SQLFactory;
//using System;
//using System.Data.Common;
//using System.Threading.Tasks;

//namespace DataAccess
//{
//    public class DACommonConfig
//    {
//        ~DACommonConfig() { }
//        public async Task<BECommonConfigs> GetCommonConfigs(SQLHelper sqlHelper, string GroupName)
//        {
//            string sql = string.Empty;
//            BECommonConfigs CommonConfigs = new BECommonConfigs();
//            try
//            {
//                sql = sqlHelper.MakeSQL("SELECT * FROM TblCommonConfig");

//                if (GroupName != "")
//                {
//                    sql = sqlHelper.MakeSQL(sql + " WHERE GroupName=$s", GroupName);
//                }

//                DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
//                AddToCommonConfigCollection(CommonConfigs, reader);
//                reader.Close();
//            }
//            catch (Exception ex)
//            {
//                throw new Exception(ex.Message);
//            }
//            return CommonConfigs;
//        }

//        public async Task<BECommonConfig> GetCommonConfig(SQLHelper sqlHelper, string GroupName, int id)
//        {
//            string sql = string.Empty;
//            BECommonConfigs CommonConfigs = new BECommonConfigs();
//            try
//            {
//                sql = sqlHelper.MakeSQL("SELECT * FROM TblCommonConfig");

//                if (GroupName != "")
//                {
//                    sql = sqlHelper.MakeSQL(sql + " WHERE GroupName=$s", GroupName);
//                }

//                DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
//                AddToCommonConfigCollection(CommonConfigs, reader);
//                reader.Close();
//            }
//            catch (Exception ex)
//            {
//                throw new Exception(ex.Message);
//            }

//            if (CommonConfigs.Count > 0)
//                return CommonConfigs[0];
//            else
//                return new BECommonConfig();
//        }

//        private BECommonConfigs AddToCommonConfigCollection(BECommonConfigs CommonConfigs, DbDataReader reader)
//        {
//            NULLHandler nullHandler = new NULLHandler(reader);
//            while (reader.Read())
//            {
//                CommonConfigs.Add(PreaperCommonConfigObject(nullHandler));
//            }
//            return CommonConfigs;
//        }

//        private BECommonConfig PreaperCommonConfigObject(NULLHandler nullHandler)
//        {
//            BECommonConfig CommonConfig = new BECommonConfig();
//            CommonConfig.CommonConfigId = nullHandler.GetInt("CommonConfigId");
//            CommonConfig.CommonConfigName = nullHandler.GetString("CommonConfigName");
//            CommonConfig.GroupName = nullHandler.GetString("GroupName");
//            return CommonConfig;
//        }

//        public DataTable BillCycleHistoryListForIcon(SQLHelper sqlHelper, DateTime billDate, string msisdn)
//        {
//            DataSet ds = new DataSet();
//            string sql = string.Empty;
//            try
//            {
//                //select chargetype, CASE WHEN chargetype = 'L' THEN 'Local voice calls' WHEN chargetype = 'S' THEN 'SMS' WHEN chargetype = 'O' THEN 'ISD calls' WHEN chargetype = 'P' THEN 'GPRS' WHEN chargetype = 'V' THEN 'Video calls' WHEN chargetype = 'U' THEN 'MMS' END AS chargetypetitle, CASE WHEN chargetype = 'P' THEN (sum(free_duration) / 1024) WHEN chargetype = 'L' THEN (sum(free_duration) / 60) WHEN chargetype = 'O' THEN (sum(free_duration) / 60) WHEN chargetype = 'V' THEN (sum(free_duration) / 60) ELSE sum(free_duration) END AS free_duration, CASE WHEN chargetype = 'L' THEN 'MINUTE' WHEN chargetype = 'S' THEN '' WHEN chargetype = 'O' THEN 'MINUTE' WHEN chargetype = 'P' THEN 'MB' WHEN chargetype = 'V' THEN 'MINUTE' WHEN chargetype = 'U' THEN '' END AS chargetypepackage from crm_pkg_subsequip_airtime where billed_from_date>='01-mar-2014' and chargetype IN ('L','S','O','P','V','U') and free_duration>0 and subno='1938855087' group by chargetype

//                //SELECT chargetype, chargetypetitle,  ROUND(free_duration,0) , chargetypepackage FROM (select chargetype, CASE         WHEN chargetype = 'L' THEN 'Local voice calls' WHEN chargetype = 'S' THEN 'SMS' WHEN chargetype = 'O' THEN 'ISD calls' WHEN chargetype = 'P' THEN 'GPRS' WHEN chargetype = 'V' THEN 'Video calls' WHEN chargetype = 'U' THEN 'MMS' END AS chargetypetitle, CASE         WHEN chargetype = 'P' THEN (sum(free_duration) / 1024) WHEN chargetype = 'L' THEN (sum(free_duration) / 3600)  WHEN chargetype = 'O' THEN (sum(free_duration) / 3600) WHEN chargetype = 'V' THEN (sum(free_duration) / 3600) ELSE sum(free_duration) END AS free_duration, CASE WHEN chargetype = 'L' THEN 'MINUTE' WHEN chargetype = 'S' THEN ''  WHEN chargetype = 'O' THEN 'MINUTE' WHEN chargetype = 'P' THEN 'MB' WHEN chargetype = 'V' THEN 'MINUTE' WHEN chargetype = 'U' THEN '' END AS chargetypepackage from crm_pkg_subsequip_airtime where billed_from_date >= '21-Feb-2014'  and chargetype IN ('L', 'S', 'O', 'P', 'V', 'U')  and free_duration > 0 and subno = '1919374324' group by chargetype)


//                //sql = sqlHelper.MakeSQL(@"SELECT chargetype, chargetypetitle,  ROUND(free_duration,0) as free_duration, ROUND(used_duration,0)  AS used_duration, chargetypepackage FROM (select chargetype, CASE WHEN chargetype = 'L' THEN 'Local voice calls' WHEN chargetype = 'S' THEN 'SMS' WHEN chargetype = 'O' THEN 'ISD calls' WHEN chargetype = 'P' THEN 'GPRS' WHEN chargetype = 'V' THEN 'Video calls' WHEN chargetype = 'U' THEN 'MMS' END AS chargetypetitle, CASE WHEN chargetype = 'P' THEN (sum(free_duration) / 1024) WHEN chargetype = 'L' THEN (sum(free_duration) / 3600) WHEN chargetype = 'O' THEN (sum(free_duration) / 3600) WHEN chargetype = 'V' THEN (sum(free_duration) / 3600) ELSE sum(free_duration) END AS free_duration, CASE WHEN chargetype = 'P' THEN (sum(used_duration) / 1024) WHEN chargetype = 'L' THEN (sum(used_duration) / 3600) WHEN chargetype = 'O' THEN (sum(used_duration) / 3600)  WHEN chargetype = 'V' THEN (sum(used_duration) / 3600) ELSE sum(used_duration) END AS used_duration, CASE WHEN chargetype = 'L' THEN 'MINUTE' WHEN chargetype = 'S' THEN '' WHEN chargetype = 'O' THEN 'MINUTE' WHEN chargetype = 'P' THEN 'MB' WHEN chargetype = 'V' THEN 'MINUTE' WHEN chargetype = 'U' THEN '' END AS chargetypepackage from crm_pkg_subsequip_airtime where billed_from_date>=$d and chargetype IN ('L','S','O','P','V','U') and free_duration>0 and subno=$s group by chargetype)", billDate, msisdn);
//                sql = sqlHelper.MakeSQL(@"SELECT chargetype,
//       chargetypetitle,free_amount, used_amount,
//       ROUND(free_duration + free_amount, 0) as free_duration,
//       ROUND(used_duration + used_amount, 0) AS used_duration,
//       chargetypepackage
//  FROM (select chargetype, free_amount, used_amount,
//               CASE
//                 WHEN chargetype = 'L' THEN
//                  'Local voice calls'
//                 WHEN chargetype = 'S' THEN
//                  'SMS'
//                 WHEN chargetype = 'O' THEN
//                  'ISD calls'
//                 WHEN chargetype = 'P' THEN
//                  'GPRS'
//                 WHEN chargetype = 'V' THEN
//                  'Video calls'
//                 WHEN chargetype = 'U' THEN
//                  'MMS'
//               END AS chargetypetitle,
//               CASE
//                 WHEN chargetype = 'P' THEN
//                  (sum(free_duration) / 1024)
//                 WHEN chargetype = 'L' THEN
//                  (sum(free_duration) / 3600)
//                 WHEN chargetype = 'O' THEN
//                  (sum(free_duration) / 3600)
//                 WHEN chargetype = 'V' THEN
//                  (sum(free_duration) / 3600)
//                 ELSE
//                  sum(free_duration)
//               END AS free_duration,
//               CASE
//                 WHEN chargetype = 'P' THEN
//                  (sum(used_duration) / 1024)
//                 WHEN chargetype = 'L' THEN
//                  (sum(used_duration) / 3600)
//                 WHEN chargetype = 'O' THEN
//                  (sum(used_duration) / 3600)
//                 WHEN chargetype = 'V' THEN
//                  (sum(used_duration) / 3600)
//                 ELSE
//                  sum(used_duration)
//               END AS used_duration,
//               CASE
//                 WHEN chargetype = 'L' THEN
//                  'MINUTE'
//                 WHEN chargetype = 'S' THEN
//                  ''
//                 WHEN chargetype = 'O' THEN
//                  'Tk.'
//                 WHEN chargetype = 'P' THEN
//                  'MB'
//                 WHEN chargetype = 'V' THEN
//                  'MINUTE'
//                 WHEN chargetype = 'U' THEN
//                  ''
//               END AS chargetypepackage
//          from crm_pkg_subsequip_airtime
//         where billed_from_date >= $d
//           and chargetype IN ('L', 'S', 'O', 'P', 'V', 'U')
//           and (free_duration > 0 or free_amount > 0)
//           and subno = $s
//         group by chargetype, used_amount, free_amount)", billDate, msisdn);

//                sqlHelper.ExecuteQuery(sql, ds);
//            }
//            catch (Exception ex)
//            {
//                throw new Exception(ex.Message);
//            }

//            return ds.Tables[0];
//        }
//    }
//}

