﻿//using BusinessEntity.APIHub2;
//using SQLFactory;
//using System;
//using System.Data.Common;
//using System.Threading.Tasks;

//namespace DataAccess
//{
//    #region Object of DAErrorLog
//    public class DAErrorLog
//    {
//        #region Constructor
//        public DAErrorLog() { }
//        #endregion

//        #region Destructor
//        ~DAErrorLog() { }
//        #endregion

//        #region Save

//        public async Task Update(SQLHelper sqlHelper, BEErrorLog log)
//        {
//            try
//            {
//                string sql = string.Empty;


//                sql = sqlHelper.MakeSQL("UPDATE AppErrorLog SET "
//                    + "IsActive=$b "
//                    + "WHERE ID=$n ", log.IsActive, log.ID);

//                await sqlHelper.ExecuteNonQueryAsync(sql);
//            }
//            catch (Exception)
//            {
//                throw;
//            }
//        }

//        #endregion

//        #region Delete
//        public async Task Delete(SQLHelper sqlHelper, int id)
//        {
//            try
//            {
//                string sql = string.Empty;
//                sql = sqlHelper.MakeSQL("UPDATE AppErrorLog SET IsActive=$b  WHERE ID=$n ", false, id);
//                await sqlHelper.ExecuteNonQueryAsync(sql);
//            }
//            catch (Exception)
//            {
//                throw;
//            }
//        }
//        #endregion

//        #region Get
//        public async Task<BEErrorLog> GetErrorLog(SQLHelper sqlHelper, int id)
//        {
//            string sql = string.Empty;
//            BEErrorLogs logs = new BEErrorLogs();

//            try
//            {
//                sql = sqlHelper.MakeSQL("SELECT ID,MSISDN,LogTime,Message,StackTrace,RequestMethod,RequestFrom,RTT,IsActive,RequestURL,Request,Response"
//                    + " FROM AppErrorLog"
//                    + " WHERE ID=$n ", id);

//                DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
//                AddToCollection(logs, reader);
//                reader.Close();
//            }
//            catch (Exception)
//            {
//                throw;
//            }
//            if (logs.Count > 0)
//            {
//                return logs[0];
//            }
//            else return new BEErrorLog();
//        }

//        public async Task<BEErrorLogs> GetErrorLogs(SQLHelper sqlHelper, bool activeOnly)
//        {
//            string sql = string.Empty;
//            string whereClause = string.Empty;
//            BEErrorLogs logs = new BEErrorLogs();

//            try
//            {
//                if (activeOnly)
//                    whereClause += sqlHelper.MakeSQL(" WHERE IsActive = $b", activeOnly);
//                sql = sqlHelper.MakeSQL("SELECT ID,MSISDN,LogTime,Message,StackTrace,RequestMethod,RequestFrom,RTT,IsActive,RequestURL,Request,Response"
//                    + " FROM AppErrorLog"
//                    + "$q", whereClause);

//                DbDataReader reader = await sqlHelper.ExecuteQueryAsync(sql);
//                AddToCollection(logs, reader);
//                reader.Close();
//            }
//            catch (Exception)
//            {
//                throw;
//            }
//            return logs;
//        }

//        private BEErrorLogs AddToCollection(BEErrorLogs logs, DbDataReader reader)
//        {
//            try
//            {
//                NULLHandler nullHandler = new NULLHandler(reader);
//                while (reader.Read())
//                {
//                    logs.Add(PreaperObject(nullHandler));
//                }
//            }
//            catch (Exception)
//            {
//                throw;
//            }
//            return logs;
//        }

//        private BEErrorLog PreaperObject(NULLHandler nullHandler)
//        {
//            BEErrorLog log = new BEErrorLog();

//            try
//            {
//                log.ID = nullHandler.GetInt("ID");
//                log.MSISDN = nullHandler.GetString("MSISDN");
//                log.LogTime = nullHandler.GetDateTime("LogTime");
//                log.Message = nullHandler.GetString("Message");
//                log.StackTrace = nullHandler.GetString("StackTrace");
//                log.RequestMethod = nullHandler.GetString("RequestMethod");
//                log.RTT = nullHandler.GetInt("RTT");
//                log.RequestFrom = nullHandler.GetString("RequestFrom");
//                log.RequestURL = nullHandler.GetString("RequestURL");
//                log.IsActive = nullHandler.GetBoolean("ISACTIVE");
//                log.Request = nullHandler.GetString("Request");
//                log.Response = nullHandler.GetString("Response");
//            }
//            catch (Exception)
//            {
//                throw;
//            }
//            return log;
//        }
//        #endregion

//    }
//    #endregion
//}

