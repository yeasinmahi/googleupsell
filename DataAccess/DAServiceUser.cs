﻿using System;
using System.Data.Common;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using SQLFactory;

namespace DataAccess
{
    #region Object of DAServiceUser

    public class DAServiceUser
    {
        #region Destructor

        ~DAServiceUser()
        {
        }

        #endregion

        #region Constructor

        #endregion


        #region Get

        public async Task<BEServiceUser> GetServiceUser(SQLHelper sqlHelper, string userID, string password)
        {
            var sql = string.Empty;
            var serviceUserInfos = new BEServiceUsers();

            try
            {
                //sql = sqlHelper.MakeSQL("SELECT UserID, UserName, Password, IsLocked,  CreatedBy, CreatedDate, UpdatedBy, UpdatedDate, IsDeleted,"
                //	+ "DeletedBy, DeletedDate, IsActive FROM ServiceUser WHERE UserID=$s AND Password=$s", userID, password);

                sql = sqlHelper.MakeSQL(
                    "SELECT UserID, UserName  FROM tblServiceUserInfo WHERE ClientID=$s AND ClientSecret=$s", userID,
                    password);


                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollection(serviceUserInfos, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (serviceUserInfos.Count > 0)
                return serviceUserInfos[0];
            return new BEServiceUser();
        }

        public async Task<BEServiceUser> GetServiceUserInfo(SQLHelper sqlHelper, string userName)
        {
            var sql = string.Empty;
            var serviceUserInfos = new BEServiceUsers();

            try
            {
                sql = sqlHelper.MakeSQL(
                    "SELECT UserID, UserName, Password, InVasUserName, InVasPassword, Description, CreatedBy, CreatedDate,DBSSUserName, DBSSPassword,LOXYSOFTPASSWORD,LOXYSOFTUSERNAME,LOXYSOFTCHANNELNAME, MOTHERMOBILENUMBER, MOTHERSUBSCRIPTIONID,"
                    + "UpdatedBy, UpdatedDate, IsDeleted, DeletedBy, DeletedDate, IsActive, Serial, LMSWEBSERVICEUSER, LMSWEBSERVICEPASSWORD,CMSChanel,CMSSalesChannel,MOTHERCOMPANYNAME,LMSChanel,LMSSalesChannel,LMSUSER,LMSPASSWORD FROM tblserviceuserinfo WHERE UserName=$s ",
                    userName);

                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollectionWithAllProperty(serviceUserInfos, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            if (serviceUserInfos.Count > 0)
                return serviceUserInfos[0];
            return new BEServiceUser();
        }

        public async Task<BEServiceUsers> GetServiceUserInfos(SQLHelper sqlHelper)
        {
            var sql = string.Empty;
            var serviceUserInfos = new BEServiceUsers();

            try
            {
                sql = sqlHelper.MakeSQL(
                    "SELECT UserID, UserName, Password, InVasUserName, InVasPassword, Description, CreatedBy, CreatedDate, DBSSUserName, DBSSPassword,LOXYSOFTPASSWORD,LOXYSOFTUSERNAME,LOXYSOFTCHANNELNAME,"
                    + "UpdatedBy, UpdatedDate, IsDeleted, DeletedBy, DeletedDate, IsActive, Serial, " +
                    "LMSWEBSERVICEUSER, LMSWEBSERVICEPASSWORD,CMSChanel,CMSSalesChannel, MOTHERMOBILENUMBER, MOTHERSUBSCRIPTIONID,MOTHERCOMPANYNAME,LMSChanel," +
                    "LMSSalesChannel ,LMSUSER,LMSPASSWORD ,ClientID,ClientSecret FROM tblserviceuserinfo WHERE IsDeleted=$b AND IsActive=1 AND ClientID is not null",
                    false);

                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                AddToCollectionWithAllProperty(serviceUserInfos, reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return serviceUserInfos;
        }

        private BEServiceUsers AddToCollection(BEServiceUsers serviceUserInfos, DbDataReader reader)
        {
            try
            {
                var nullHandler = new NULLHandler(reader);
                while (reader.Read()) serviceUserInfos.Add(PreaperObject(nullHandler));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return serviceUserInfos;
        }

        private BEServiceUser PreaperObject(NULLHandler nullHandler)
        {
            var serviceUserInfo = new BEServiceUser();

            try
            {
                serviceUserInfo.IsNew = false;
                serviceUserInfo.UserID = nullHandler.GetInt("UserID");
                serviceUserInfo.UserName = nullHandler.GetString("UserName");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return serviceUserInfo;
        }

        private BEServiceUsers AddToCollectionWithAllProperty(BEServiceUsers serviceUserInfos, DbDataReader reader)
        {
            try
            {
                var nullHandler = new NULLHandler(reader);
                while (reader.Read()) serviceUserInfos.Add(PreaperObjectWithAllProperty(nullHandler));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return serviceUserInfos;
        }

        private BEServiceUser PreaperObjectWithAllProperty(NULLHandler nullHandler)
        {
            var serviceUserInfo = new BEServiceUser();

            try
            {
                serviceUserInfo.IsNew = false;
                serviceUserInfo.UserID = nullHandler.GetInt("UserID");
                serviceUserInfo.UserName = nullHandler.GetString("UserName");
                serviceUserInfo.Password = nullHandler.GetString("Password");
                serviceUserInfo.InVasUserName = nullHandler.GetString("InVasUserName");
                serviceUserInfo.InVasPassword = nullHandler.GetString("InVasPassword");
                serviceUserInfo.Description = nullHandler.GetString("Description");
                serviceUserInfo.IsDeleted = nullHandler.GetBoolean("IsDeleted");
                serviceUserInfo.IsActive = nullHandler.GetBoolean("IsActive");
                serviceUserInfo.CreatedBy = nullHandler.GetString("CreatedBy");
                serviceUserInfo.CreatedDate = nullHandler.GetDateTime("CreatedDate");
                serviceUserInfo.UpdatedBy = nullHandler.GetString("UpdatedBy");
                serviceUserInfo.UpdatedDate = nullHandler.GetDateTime("UpdatedDate");
                serviceUserInfo.DeletedBy = nullHandler.GetString("DeletedBy");
                serviceUserInfo.DeletedDate = nullHandler.GetDateTime("DeletedDate");
                serviceUserInfo.Serial = nullHandler.GetInt("Serial");
                serviceUserInfo.LMSWebServiceUser = nullHandler.GetString("LMSWEBSERVICEUSER");
                serviceUserInfo.LMSWebServicePassword = nullHandler.GetString("LMSWEBSERVICEPASSWORD");
                serviceUserInfo.CMSChanel = nullHandler.GetInt("CMSChanel");
                serviceUserInfo.DBSSUserName = nullHandler.GetString("DBSSUserName");
                serviceUserInfo.DBSSPassword = nullHandler.GetString("DBSSPassword");
                serviceUserInfo.LoxySoftUserName = nullHandler.GetString("LOXYSOFTUSERNAME");
                serviceUserInfo.LoxySoftPassword = nullHandler.GetString("LOXYSOFTPASSWORD");
                serviceUserInfo.LoxySoftChannelName = nullHandler.GetString("LOXYSOFTCHANNELNAME");
                serviceUserInfo.CMSSalesChannel = nullHandler.GetInt("CMSSalesChannel");
                serviceUserInfo.MotherMobileNo = nullHandler.GetString("MOTHERMOBILENUMBER");
                serviceUserInfo.MotherSubscriptionId = nullHandler.GetString("MOTHERSUBSCRIPTIONID");
                serviceUserInfo.MotherCompanyName = nullHandler.GetString("MOTHERCOMPANYNAME");
                serviceUserInfo.LMSChanel = nullHandler.GetInt("LMSChanel");
                serviceUserInfo.LMSSalesChannel = nullHandler.GetInt("LMSSalesChannel");
                serviceUserInfo.LMSUSER = nullHandler.GetString("LMSUSER");
                serviceUserInfo.LMSPASSWORD = nullHandler.GetString("LMSPASSWORD");
                serviceUserInfo.ClientID = nullHandler.GetString("ClientID");
                serviceUserInfo.ClientSecret = nullHandler.GetString("ClientSecret");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return serviceUserInfo;
        }

        #endregion
    }

    #endregion
}