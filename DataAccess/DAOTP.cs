﻿using System;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using SQLFactory;
using Utility;

//using System.Text;

namespace DataAccess
{
    public class DAOTP
    {
        ~DAOTP()
        {
        }

        public async Task SendSMS(SQLHelper sqlHelper, string sender, string recipient, string message)
        {
            var sql = string.Empty;
            try
            {
                sql = @"INSERT INTO SMSQUEUE (ID, SENDER, RECIEVER, SMSMSG, STATUS, SNDRCV) "
                      + "  Select SQ_SMSQUEUE_ID.NEXTVAL ,  :SENDER1, :RECIEVER1, :SMSMSG1, :STATUS1, :SNDRCV1 from dual";

                object[] objParam = {sender, recipient, message, 20, 'S'};

                var reader = await sqlHelper.ExecuteQueryAsCommandAsync(sql,
                    new[] {"SENDER1|S", "RECIEVER1|S", "SMSMSG1|NS", "STATUS1|N", "SNDRCV1|C"}, objParam);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task SendSMS(SQLHelper sqlHelper, string sender, string recipient, string message,
            SMSChannelEnum channel)
        {
            var sql = string.Empty;
            try
            {
                sql = @"INSERT INTO SMSQUEUE (ID, SENDER, RECIEVER, SMSMSG, STATUS, SNDRCV , CHANNEL) "
                      + "  Select SQ_SMSQUEUE_ID.NEXTVAL ,  :SENDER1, :RECIEVER1, :SMSMSG1, :STATUS1, :SNDRCV1 ,:CHANNEL1 from dual";

                object[] objParam = {sender, recipient, message, 20, 'S', (int) channel};

                var reader = await sqlHelper.ExecuteQueryAsCommandAsync(sql,
                    new[] {"SENDER1|S", "RECIEVER1|S", "SMSMSG1|NS", "STATUS1|N", "SNDRCV1|S", "CHANNEL1|N"}, objParam);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task SendSMS(SQLHelper sqlHelper, string sender, string recipient, string message,
            string serviceUserID, SMSChannelEnum channel, string SMSAcknowledgementID)
        {
            var sql = string.Empty;
            try
            {
                sql =
                    @"INSERT INTO SMSQUEUE (ID, SENDER, RECIEVER, SMSMSG, STATUS, SNDRCV, CHANNEL, SMSACKNOWLEDGEMENTID,BROWSESOURCE) "
                    + "  Select SQ_SMSQUEUE_ID.NEXTVAL, :SENDER1, :RECIEVER1, :SMSMSG1, :STATUS1, :SNDRCV1, :CHANNEL1, :SMSACKNOWLEDGEMENTID1 ,:BROWSESOURCE1 from dual";

                object[] objParam =
                    {sender, recipient, message, 20, 'S', (int) channel, SMSAcknowledgementID, serviceUserID};

                var reader = await sqlHelper.ExecuteQueryAsCommandAsync(sql,
                    new[]
                    {
                        "SENDER1|S", "RECIEVER1|S", "SMSMSG1|NS", "STATUS1|N", "SNDRCV1|C", "CHANNEL1|N",
                        "SMSACKNOWLEDGEMENTID1|S", "BROWSESOURCE1|Name"
                    }, objParam);
                reader.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task SaveOTP(SQLHelper sqlHelper, string msisdnNo, string otpNo, int pageType)
        {
            var sql = string.Empty;
            try
            {
                sql = sqlHelper.MakeSQL(
                    @"INSERT INTO tblCustomerOTP (CustomerOTPID, MSISDN, OTP,PAGETYPE) VALUES (SQ_CustomerOTPID.Nextval, $s, $s,$n)",
                    msisdnNo, otpNo, pageType);
                await sqlHelper.ExecuteNonQueryAsync(sql);
            }

            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<string> GetOTP(SQLHelper sqlHelper, string msisdnNo)
        {
            var otp = string.Empty;
            try
            {
                var sql = string.Empty;

                sql = sqlHelper.MakeSQL(@" select * from (select t.otp "
                                        + "from tblCustomerOTP t "
                                        + "where t.msisdn = $s order by t.createdate desc) "
                                        + "where rownum=1", msisdnNo);
                var obj = await sqlHelper.ExecuteScalarAsync(sql);

                if (obj != DBNull.Value) otp = Convert.ToString(obj);

                return otp;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public async Task<bool> IsValidOTP(SQLHelper sqlHelper, string MSISDN, int otpValidityDuration, string OTP,
            int pageType, int otpWrongAttempLimit)
        {
            var otp = string.Empty;
            var otpDateTime = DateTime.MinValue;
            var isValidOTP = false;
            try
            {
                var sql = string.Empty;

                sql = sqlHelper.MakeSQL(@"select * from (select t.otp, t.createdate 
                                                    from tblCustomerOTP t  where sysdate <= t.createdate + $n * 60 / (24 * 60 * 60) 
                                                    AND t.msisdn = $s order by t.createdate desc) 
                                                    where rownum=1", otpValidityDuration, MSISDN);
                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                var nullHandler = new NULLHandler(reader);
                if (reader.Read())
                {
                    otp = nullHandler.GetString("otp");
                    otpDateTime = nullHandler.GetDateTime("CreateDate");
                }

                reader.Close();

                if (string.IsNullOrEmpty(otp))
                    throw new BeHandledException(BEMessageCodes.InvalidOTP.Status, BEMessageCodes.InvalidOTP.error,
                        "Please generate OTP first");

                var wrongAttemptCount = await GetWrongAttemptCount(sqlHelper, MSISDN, otp);

                if (wrongAttemptCount >= otpWrongAttempLimit)
                    throw new BeHandledException(BEMessageCodes.InvalidOTP.Status, BEMessageCodes.InvalidOTP.error,
                        "Sorry! Your OTP number has expired for maximum wrong attempts. Please generate a new OTP and try again. Thank you.");


                if (otpDateTime != DateTime.MinValue && !string.IsNullOrEmpty(otp) &&
                    (otp.ToUpper() == OTP.ToUpper() ||
                     await new Encryption().IsSHA1HashEqual(OTP, otp)))
                    isValidOTP = true;

                if (!isValidOTP)
                    await SaveOTPWrongAttemp(sqlHelper, MSISDN, OTP, pageType, otp);

                //  return otp;
            }
            catch (BeHandledException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return isValidOTP;
        }


        public async Task SaveOTPWrongAttemp(SQLHelper sqlHelper, string msisdnNo, string otpNo, int pageType,
            string correctOTP)
        {
            var sql = string.Empty;
            try
            {
                sql = sqlHelper.MakeSQL(
                    @"INSERT INTO tblCustomerOTPWrongAttemp (CustomerOTPID, MSISDN, OTP,PAGETYPE, CORRECTOTP) 
VALUES (SQ_CUSTOMEROTPWrongAttempID.Nextval, $s, $s,$n, $s)", msisdnNo, otpNo, pageType, correctOTP);
                await sqlHelper.ExecuteNonQueryAsync(sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<int> GetWrongAttemptCount(SQLHelper sqlHelper, string MSISDN, string correctOTP)
        {
            var sql = string.Empty;
            var wrongAttemptCount = 0;
            try
            {
                sql = sqlHelper.MakeSQL(
                    @"select count(*) from tblcustomerotpwrongattemp w where w.createdate>trunc(sysdate) AND MSISDN=$s and CORRECTOTP=$s",
                    MSISDN, correctOTP);

                var obj = await sqlHelper.ExecuteScalarAsync(sql);

                if (obj != DBNull.Value) wrongAttemptCount = Convert.ToInt32(obj);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return wrongAttemptCount;
        }

        public async Task<DateTime> OTPValidateUsingValidityDuration(SQLHelper sqlHelper, int otpValidityDuration,
            string msisdnNo)
        {
            var otpDateTime = DateTime.MinValue;
            try
            {
                var sql = string.Empty;

                sql = sqlHelper.MakeSQL(@" select * from (select t.createdate "
                                        + "from tblCustomerOTP t "
                                        + "where sysdate <= t.createdate + $n *60 / (24 * 60 * 60) and t.msisdn = $s order by t.createdate desc) "
                                        + "where rownum=1", otpValidityDuration, msisdnNo);
                var obj = await sqlHelper.ExecuteScalarAsync(sql);

                if (obj != DBNull.Value) otpDateTime = Convert.ToDateTime(obj);


                return otpDateTime;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<string> CheckOTP(SQLHelper sqlHelper, string msisdnNo, int otpIntervalTime,
            int otpMinimumInADay)
        {
            var msg = string.Empty;
            try
            {
                var maxTime = DateTime.MinValue;
                var otpCount = 0;
                var sql = string.Empty;

                sql = sqlHelper.MakeSQL(@"SELECT max(t.createdate) MaxTime, count(msisdn) otpCount 
                FROM tblCustomerOTP t where t.msisdn=$s and t.createdate>trunc(sysdate) group by msisdn", msisdnNo);

                var reader = await sqlHelper.ExecuteQueryAsync(sql);
                var nullHandler = new NULLHandler(reader);
                if (reader.Read())
                {
                    maxTime = nullHandler.GetDateTime("MaxTime");
                    otpCount = nullHandler.GetInt("otpCount");
                }

                reader.Close();


                if (maxTime != DateTime.MinValue && maxTime.AddSeconds(otpIntervalTime) >= DateTime.Now)
                {
                    var span = maxTime.AddSeconds(otpIntervalTime) - DateTime.Now;

                    msg = "You can send OTP after " + string.Format("{0} seconds", span.Seconds);
                    throw new BeHandledException(BEMessageCodes.TooMuchQuickly.Status,
                        BEMessageCodes.TooMuchQuickly.error, string.Empty, msg);
                }

                if (otpCount >= otpMinimumInADay)
                {
                    msg = "Your OTP quota has been finished, please try next day!";
                    throw new BeHandledException(BEMessageCodes.Quota.Status, BEMessageCodes.Quota.error, msg);
                }
            }
            catch (BeHandledException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return msg;
        }
    }
}