﻿using System;
using BusinessEntity.APIHub2;
using LogService;
using Microsoft.Extensions.Configuration;

namespace OGate.Helper
{
    public class AppSettingsHelper
    {
        public AppSettingsHelper(IConfiguration configuration)
        {
            var section = configuration.GetSection("AppSettings");
            AppSettings.Provider = GetValue<string>(section, "Provider");
            AppSettings.ServiceName = GetValue<string>(section, "ServiceName");
            AppSettings.TtlSeconds = GetValue<int>(section, "TtlSeconds");

            //Log Configuration
            ConfigLog(section);
        }

        public void ConfigLog(IConfigurationSection section)
        {
            LogConfig.IsWriteErrorLogFile = GetValue<bool>(section, "IsWriteErrorLogFile");
            LogConfig.IsWriteErrorLogDb = GetValue<bool>(section, "IsWriteErrorLogDb");
            LogConfig.IsWriteRequestResponseFile = GetValue<bool>(section, "IsWriteRequestResponseFile");
            LogConfig.IsWriteRequestResponseDb = GetValue<bool>(section, "IsWriteRequestResponseDb");
            //LogConfig.IsWriteAuditTrailDb = GetValue<bool>(section, "IsWriteAuditTrailDb");
        }

        private TConvertType GetValue<TConvertType>(IConfigurationSection section, string sectionName)
        {
            var value = section[sectionName];
            var converted = default(TConvertType);
            try
            {
                converted = (TConvertType)
                    Convert.ChangeType(value, typeof(TConvertType));
            }
            catch
            {
            }

            return converted;
        }
    }
}