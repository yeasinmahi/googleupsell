using System.Diagnostics;
using BusinessEntity.APIHub2;
using LogService.Extension;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using OGate.Helper;
using Utility.Extensions;

namespace OGate
{
    public class Startup
    {
        private readonly Stopwatch watch = new Stopwatch();

        public Startup(IConfiguration configuration)
        {
            watch.Start();

            Configuration = configuration;
            //AppSettings.Load(configuration);
            new AppSettingsHelper(configuration);
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.Configure<IISServerOptions>(options => { options.AllowSynchronousIO = true; });
            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(1, 0);
            });
            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new OpenApiInfo {Title = "API Docs", Version = "v1"}); });

            ExceptionExtensions.DBBSErrorLogPath = AppSettings.DBBSErrorLogPath;
            //services.AddGrpc(options =>
            //{
            //    options.EnableDetailedErrors = true;
            //    options.MaxReceiveMessageSize = 2 * 1024 * 1024; // 2 MB
            //    options.MaxSendMessageSize = 5 * 1024 * 1024; // 5 MB
            //});
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseExceptionHandler("/Error");
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");
                c.RoutePrefix = string.Empty;
            });
            app.UseLoggerMiddleware();
            app.UseHttpsRedirection();
            app.UseRouting();

            //app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });

            watch.Stop();
            AppSettings.ApplicationLoadingTime = watch.ElapsedMilliseconds;
        }
    }
}