﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;

namespace OGate.Controllers.OthersController
{
    [Route("Log")]
    [AllowAnonymous]
    public class ErrorController : ControllerBase
    {
        [ApiExplorerSettings(IgnoreApi = true)]
        public void Log()
        {
            var exceptionDetails = HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            _ = exceptionDetails.Error.Message;
            _ = exceptionDetails.Error.StackTrace;
            _ = exceptionDetails.Error.Source;
            _ = exceptionDetails.Error.Data;
            _ = exceptionDetails.Path;
        }
    }
}