﻿using LogService;
using Microsoft.AspNetCore.Mvc;

namespace OGate.Controllers.OthersController
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{v:apiVersion}/[controller]")]
    public class BaseController : ControllerBase
    {
        protected string errorMessage;
        protected Log log;

        public BaseController()
        {
            log = new Log(string.Empty);
        }

        //[ApiExplorerSettings(IgnoreApi = true)]
        //public CpidError GetErroResponse(string errorMessage, string cause)
        //{
        //    return new CpidError { ErrorMessage = errorMessage, Cause = cause };
        //}
    }
}