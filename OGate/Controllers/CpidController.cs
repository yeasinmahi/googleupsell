﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using BusinessEntity.OGate;
using BusinessObject;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using OGate.Controllers.OthersController;

namespace OGate.Controllers
{
    [ApiController]
    public class CpidController : BaseController
    {
        //private string msisdn = "1404850126";
        private readonly string language = "en-US";
        //private string secret="123";

        [HttpGet]
        //http://localhost:31458/Api/v1.0/cpid?app=ls
        public async Task<IActionResult> GetCpid(string app = null)
        {
            //log.Info("CPID method start", "GetCpid");
            Stopwatch watch = Stopwatch.StartNew();
            CpidResponse response=null;
            StringValues msisdn;
            try
            {
                if (!Request.Headers.TryGetValue("msisdn", out msisdn))
                {
                    errorMessage = "Missing MSISDN into header";
                    return Ok(new CpidError("InvalidMsisdn", "Missing MSISDN into header"));
                }

                if (string.IsNullOrEmpty(msisdn) || msisdn.ToString().Length < 10 || msisdn.ToString().Length > 15)
                {
                    errorMessage = "Invalid MSISDN passed into header-" + msisdn;
                    return Ok(new CpidError("InvalidMsisdn", errorMessage));
                }

                var MSISDN = msisdn.ToString();
                if (MSISDN.Length > 10) MSISDN = MSISDN.Substring(MSISDN.Length - 10, 10);

                //int y = 0;
                //int x = 100 / y;
                //response = new BOCpid().GenerateCpid(MSISDN, language, AppSettings.Provider);
                //try
                //{
                //    //BOCpid boCpidHistory = new BOCpid();
                //    var boCpid = new BOCpid();
                //    var beCpidHistory = new BECpidHistory();
                //    beCpidHistory.IsNew = true;
                //    beCpidHistory.Msisdn = MSISDN;
                //    beCpidHistory.Cpid = response.Cpid;
                //    beCpidHistory.Ttl = response.TtlSeconds;
                //    beCpidHistory.AppId = app;
                //    beCpidHistory.UpdateDate = DateTime.Now;
                //    beCpidHistory.Provider = AppSettings.Provider;
                //    //response.Provider = AppSettings.Provider;

                //    //boCpidHistory.Save(beCpidHistory);
                //    await boCpid.Save(beCpidHistory);
                //}

                try
                {
                    response = await new BOCpid().GenerateCpid(MSISDN, language, AppSettings.Provider, app, true);
                }
                catch (Exception ex)
                {
                    await log.Error(ex, watch.ElapsedMilliseconds, await new LogService.LogHelper().ReadRequestBody(Request));
                    errorMessage = ex.Message;
                }
            }
            //catch (BEHandledException ex)
            //{
            //    await log.Error(ex);
            //    errorMessage = ex.Message;
            //    return Json(new CpidError("Unknown", ex.Message));
            //  //  return Json(new CpidError() { ErrorMessage = "Unknown", Cause = ex.Message }); 
            //   // return Json(GetErroResponse("Unknown", ex.Message));
            //}

            catch (Exception ex)
            {
                await log.Error(ex, watch.ElapsedMilliseconds, await new LogService.LogHelper().ReadRequestBody(Request));
                errorMessage = ex.Message;
                //return Json(GetErroResponse("Unknown", ex.Message));
                return Ok(new CpidError("Unknown", errorMessage));
            }

            //log.Info("CPID method end", "GetCpid");
            return Ok(response);
        }

        [HttpGet]
        [Route("GetMsisdn")]
        //http://localhost:31458/api/v1.0/cpid/GetMsisdn?cpid=aGdNMEFsb2JjVHFtamFkUnVVUElTekRwZDlwbjJkSlp1MGpzQVlEcVdYSzBuejJpRUFMcDR4cEo0dTIrZGdieQ==
        public async Task<string> GetMsisdn(string cpid)
        {
            return await new BOCpid().GetMsisdn(cpid);
        }

        [HttpGet]
        [Route("GetDycrypt")]
        //http://localhost:31458/api/v1.0/cpid/GetDycrypt?cpid=aGdNMEFsb2JjVHFtamFkUnVVUElTekRwZDlwbjJkSlp1MGpzQVlEcVdYSzBuejJpRUFMcDR4cEo0dTIrZGdieQ==
        public async Task<IActionResult> GetDycrypt(string cpid)
        {
            return Ok(await new BOCpid().GetDycrypt(cpid));
        }
    }
}