﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Sample.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
        [HttpGet]
        public Object Dbss()
        {
            ViewBag.Title = "Home Page";

            string s = @"
{
  'data': [{
    'attributes': {
      'monthly-costs': 0,
      'allow-reactivation': false,
      'contract-status': 'rollover',
      'first-call-date': null,
      'termination-time': null,
      'contract-id': '64302151',
      'msisdn': '8801962424480',
      'activation-time': '2017-07-17T00:00:00.000+06:00',
      'status': 'active',
      'latest-contract-termination-time': '2020-01-10T00:00:00.000+06:00',
      'directory-listing': 'none',
      'payment-type': 'prepaid',
      'original-contract-confirmation-code': 'MIG_64302151'
    },
    'relationships': {
      'sim-cards': {
        'links': {
          'related': '/api/v1/subscriptions/64302151/sim-cards'
        }
      },
      'billing-accounts': {
        'links': {
          'related': '/api/v1/subscriptions/64302151/billing-accounts'
        }
      },
      'services': {
        'links': {
          'related': '/api/v1/subscriptions/64302151/services'
        }
      },
      'combined-usage-reports': {
        'links': {
          'related': '/api/v1/subscriptions/64302151/combined-usage-reports'
        }
      },
      'subscription-discounts': {
        'links': {
          'related': '/api/v1/subscriptions/64302151/subscription-discounts'
        }
      },
      'network-services': {
        'links': {
          'related': '/api/v1/subscriptions/64302151/network-services'
        }
      },
      'available-loan-products': {
        'links': {
          'related': '/api/v1/subscriptions/64302151/available-loan-products'
        }
      },
      'owner-customer': {
        'data': {
          'type': 'customers',
          'id': '1085331525'
        },
        'links': {
          'related': '/api/v1/subscriptions/64302151/owner-customer'
        }
      },
      'products': {
        'links': {
          'related': '/api/v1/subscriptions/64302151/products'
        }
      },
      'payer-customer': {
        'data': {
          'type': 'customers',
          'id': '1085331525'
        },
        'links': {
          'related': '/api/v1/subscriptions/64302151/payer-customer'
        }
      },
      'available-subscription-types': {
        'links': {
          'related': '/api/v1/subscriptions/64302151/available-subscription-types'
        }
      },
      'document-validations': {
        'links': {
          'related': '/api/v1/subscriptions/64302151/document-validations'
        }
      },
      'coordinator-customer': {
        'data': {
          'type': 'customers',
          'id': '1089106803'
        },
        'links': {
          'related': '/api/v1/subscriptions/64302151/coordinator-customer'
        }
      },
      'product-usages': {
        'links': {
          'related': '/api/v1/subscriptions/64302151/product-usages'
        }
      },
      'porting-requests': {
        'links': {
          'related': '/api/v1/subscriptions/64302151/porting-requests'
        }
      },
      'billing-rate-plan': {
        'data': {
          'type': 'billing-rate-plans',
          'id': '1'
        },
        'links': {
          'related': '/api/v1/subscriptions/64302151/billing-rate-plan'
        }
      },
      'user-customer': {
        'data': {
          'type': 'customers',
          'id': '1037280560'
        },
        'links': {
          'related': '/api/v1/subscriptions/64302151/user-customer'
        }
      },
      'gsm-service-usages': {
        'links': {
          'related': '/api/v1/subscriptions/64302151/gsm-service-usages'
        }
      },
      'balances': {
        'data': [{
          'type': 'balances',
          'id': '64302151'
        }, {
          'type': 'balances',
          'id': '64302151-189'
        }, {
          'type': 'balances',
          'id': '64302151-197'
        }, {
          'type': 'balances',
          'id': '64302151-143'
        }, {
          'type': 'balances',
          'id': '64302151-245'
        }, {
          'type': 'balances',
          'id': '64302151-255'
        }, {
          'type': 'balances',
          'id': '64302151-155'
        }, {
          'type': 'balances',
          'id': '64302151-148'
        }, {
          'type': 'balances',
          'id': '64302151'
        }, {
          'type': 'balances',
          'id': '64302151'
        }, {
          'type': 'balances',
          'id': '64302151'
        }, {
          'type': 'balances',
          'id': '64302151'
        }, {
          'type': 'balances',
          'id': '64302151'
        }, {
          'type': 'balances',
          'id': '64302151'
        }, {
          'type': 'balances',
          'id': '64302151'
        }, {
          'type': 'balances',
          'id': '64302151'
        }, {
          'type': 'balances',
          'id': '64302151'
        }, {
          'type': 'balances',
          'id': '64302151'
        }, {
          'type': 'balances',
          'id': '64302151'
        }, {
          'type': 'balances',
          'id': '64302151'
        }, {
          'type': 'balances',
          'id': '64302151'
        }, {
          'type': 'balances',
          'id': '64302151'
        }],
        'links': {
          'related': '/api/v1/subscriptions/64302151/balances'
        }
      },
      'billing-usages': {
        'links': {
          'related': '/api/v1/subscriptions/64302151/billing-usages'
        }
      },
      'barrings': {
        'links': {
          'related': '/api/v1/subscriptions/64302151/barrings'
        }
      },
      'subscription-type': {
        'data': {
          'type': 'subscription-types',
          'id': '1'
        },
        'links': {
          'related': '/api/v1/subscriptions/64302151/subscription-type'
        }
      },
      'available-products': {
        'links': {
          'related': '/api/v1/subscriptions/64302151/available-products'
        }
      },
      'catalog-sim-cards': {
        'links': {
          'related': '/api/v1/subscriptions/64302151/catalog-sim-cards'
        }
      },
      'connected-products': {
        'links': {
          'related': '/api/v1/subscriptions/64302151/connected-products'
        }
      },
      'connection-type': {
        'data': {
          'type': 'connection-types',
          'id': '3'
        },
        'links': {
          'related': '/api/v1/subscriptions/64302151/connection-type'
        }
      },
      'available-child-products': {
        'links': {
          'related': '/api/v1/subscriptions/64302151/available-child-products'
        }
      },
      'sim-card-orders': {
        'links': {
          'related': '/api/v1/subscriptions/64302151/sim-card-orders'
        }
      }
    },
    'links': {
      'self': '/api/v1/subscriptions/64302151'
    },
    'id': '64302151',
    'type': 'subscriptions'
  }],
  'included': [{
    'attributes': {
      'expiry-at': '9999-12-31T12:00:00.000Z',
      'dedicated-account-id': null,
      'balance-name': null,
      'lifecycle': null,
      'is-main-balance': false,
      'product-id': null,
      'offer-id': '10101',
      'amount': 0,
      'total-amount': null,
      'gsm-service-type': null,
      'product-code': null,
      'unit': null,
      'start': '2018-07-30T12:00:00.000Z',
      'product-name': null
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151'
    },
    'id': '64302151',
    'type': 'balances'
  }, {
    'attributes': {
      'expiry-at': '2020-08-16T12:00:00.000Z',
      'dedicated-account-id': null,
      'balance-name': null,
      'lifecycle': null,
      'is-main-balance': false,
      'product-id': null,
      'offer-id': '3629',
      'amount': 0,
      'total-amount': null,
      'gsm-service-type': null,
      'product-code': null,
      'unit': null,
      'start': '2020-05-31T12:00:00.000Z',
      'product-name': null
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151'
    },
    'id': '64302151',
    'type': 'balances'
  }, {
    'attributes': {
      'expiry-at': '9999-12-31T12:00:00.000Z',
      'dedicated-account-id': '245',
      'balance-name': 'DA LOAN',
      'lifecycle': null,
      'is-main-balance': false,
      'product-id': null,
      'offer-id': null,
      'amount': 0.00,
      'total-amount': 0.00,
      'gsm-service-type': null,
      'product-code': null,
      'unit': 'BDT',
      'start': null,
      'product-name': null
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151-245/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151-245'
    },
    'id': '64302151-245',
    'type': 'balances'
  }, {
    'attributes': {
      'expiry-at': '9999-12-31T12:00:00.000Z',
      'dedicated-account-id': '255',
      'balance-name': '255',
      'lifecycle': null,
      'is-main-balance': false,
      'product-id': null,
      'offer-id': null,
      'amount': 0.00,
      'total-amount': 0.00,
      'gsm-service-type': null,
      'product-code': null,
      'unit': 'BDT',
      'start': null,
      'product-name': null
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151-255/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151-255'
    },
    'id': '64302151-255',
    'type': 'balances'
  }, {
    'attributes': {
      'expiry-at': '9999-12-31T12:00:00.000Z',
      'dedicated-account-id': null,
      'balance-name': null,
      'lifecycle': null,
      'is-main-balance': false,
      'product-id': null,
      'offer-id': '11057',
      'amount': 0,
      'total-amount': null,
      'gsm-service-type': null,
      'product-code': null,
      'unit': null,
      'start': '2018-07-30T12:00:00.000Z',
      'product-name': null
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151'
    },
    'id': '64302151',
    'type': 'balances'
  }, {
    'attributes': {
      'expiry-at': '9999-12-31T12:00:00.000Z',
      'dedicated-account-id': null,
      'balance-name': null,
      'lifecycle': null,
      'is-main-balance': false,
      'product-id': null,
      'offer-id': '10102',
      'amount': 0,
      'total-amount': null,
      'gsm-service-type': null,
      'product-code': null,
      'unit': null,
      'start': '2018-07-30T12:00:00.000Z',
      'product-name': null
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151'
    },
    'id': '64302151',
    'type': 'balances'
  }, {
    'attributes': {
      'expiry-at': '2020-08-17T12:00:00.000Z',
      'dedicated-account-id': '155',
      'balance-name': '155',
      'lifecycle': null,
      'is-main-balance': false,
      'product-id': null,
      'offer-id': null,
      'amount': 0.00,
      'total-amount': 0.00,
      'gsm-service-type': null,
      'product-code': null,
      'unit': 'MB',
      'start': null,
      'product-name': null
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151-155/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151-155'
    },
    'id': '64302151-155',
    'type': 'balances'
  }, {
    'attributes': {
      'expiry-at': '2020-08-19T12:00:00.000Z',
      'dedicated-account-id': '189',
      'balance-name': '189',
      'lifecycle': null,
      'is-main-balance': false,
      'product-id': null,
      'offer-id': null,
      'amount': 4049.12,
      'total-amount': 4049.12,
      'gsm-service-type': 'data',
      'product-code': 'TK61BTLUSSD4GB4D',
      'unit': 'MB',
      'start': null,
      'product-name': {
        'en': '4GB-4Days-68TK',
        'bn': '4GB-4Days-68TK'
      }
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151-189/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151-189'
    },
    'id': '64302151-189',
    'type': 'balances'
  }, {
    'attributes': {
      'expiry-at': '2020-08-16T12:00:00.000Z',
      'dedicated-account-id': null,
      'balance-name': null,
      'lifecycle': null,
      'is-main-balance': false,
      'product-id': null,
      'offer-id': '6468',
      'amount': 0,
      'total-amount': null,
      'gsm-service-type': null,
      'product-code': null,
      'unit': null,
      'start': '2020-08-16T12:00:00.000Z',
      'product-name': null
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151'
    },
    'id': '64302151',
    'type': 'balances'
  }, {
    'attributes': {
      'expiry-at': '9999-12-31T12:00:00.000Z',
      'dedicated-account-id': null,
      'balance-name': null,
      'lifecycle': null,
      'is-main-balance': false,
      'product-id': null,
      'offer-id': '1000',
      'amount': 0,
      'total-amount': null,
      'gsm-service-type': null,
      'product-code': null,
      'unit': null,
      'start': '2018-07-29T00:00:00.000Z',
      'product-name': null
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151'
    },
    'id': '64302151',
    'type': 'balances'
  }, {
    'attributes': {
      'expiry-at': '9999-12-31T12:00:00.000Z',
      'dedicated-account-id': null,
      'balance-name': null,
      'lifecycle': null,
      'is-main-balance': false,
      'product-id': null,
      'offer-id': '10111',
      'amount': 0,
      'total-amount': null,
      'gsm-service-type': null,
      'product-code': null,
      'unit': null,
      'start': '2018-07-30T12:00:00.000Z',
      'product-name': null
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151'
    },
    'id': '64302151',
    'type': 'balances'
  }, {
    'attributes': {
      'expiry-at': '9999-12-31T12:00:00.000Z',
      'dedicated-account-id': null,
      'balance-name': null,
      'lifecycle': null,
      'is-main-balance': false,
      'product-id': null,
      'offer-id': '10110',
      'amount': 0,
      'total-amount': null,
      'gsm-service-type': null,
      'product-code': null,
      'unit': null,
      'start': '2018-07-30T12:00:00.000Z',
      'product-name': null
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151'
    },
    'id': '64302151',
    'type': 'balances'
  }, {
    'attributes': {
      'expiry-at': '2020-08-16T12:00:00.000Z',
      'dedicated-account-id': '148',
      'balance-name': '148',
      'lifecycle': null,
      'is-main-balance': false,
      'product-id': null,
      'offer-id': null,
      'amount': 0.00,
      'total-amount': 0.00,
      'gsm-service-type': null,
      'product-code': null,
      'unit': 'MB',
      'start': null,
      'product-name': null
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151-148/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151-148'
    },
    'id': '64302151-148',
    'type': 'balances'
  }, {
    'attributes': {
      'expiry-at': '2020-08-16T12:00:00.000Z',
      'dedicated-account-id': null,
      'balance-name': null,
      'lifecycle': null,
      'is-main-balance': false,
      'product-id': null,
      'offer-id': '3940',
      'amount': 0,
      'total-amount': null,
      'gsm-service-type': null,
      'product-code': null,
      'unit': null,
      'start': '2020-08-16T12:00:00.000Z',
      'product-name': null
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151'
    },
    'id': '64302151',
    'type': 'balances'
  }, {
    'attributes': {
      'expiry-at': '9999-12-31T12:00:00.000Z',
      'dedicated-account-id': null,
      'balance-name': null,
      'lifecycle': null,
      'is-main-balance': false,
      'product-id': null,
      'offer-id': '10103',
      'amount': 0,
      'total-amount': null,
      'gsm-service-type': null,
      'product-code': null,
      'unit': null,
      'start': '2018-07-30T12:00:00.000Z',
      'product-name': null
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151'
    },
    'id': '64302151',
    'type': 'balances'
  }, {
    'attributes': {
      'expiry-at': '2020-09-14T12:00:00.000Z',
      'dedicated-account-id': '197',
      'balance-name': '197',
      'lifecycle': null,
      'is-main-balance': false,
      'product-id': null,
      'offer-id': null,
      'amount': 0.00,
      'total-amount': 0.00,
      'gsm-service-type': 'data',
      'product-code': 'TK76USSD1GB7D',
      'unit': 'MB',
      'start': null,
      'product-name': {
        'en': '1GB-30Days-76TK',
        'bn': '1GB-30Days-76TK'
      }
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151-197/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151-197'
    },
    'id': '64302151-197',
    'type': 'balances'
  }, {
    'attributes': {
      'expiry-at': '9999-12-31T12:00:00.000Z',
      'dedicated-account-id': null,
      'balance-name': null,
      'lifecycle': null,
      'is-main-balance': false,
      'product-id': null,
      'offer-id': '667',
      'amount': 0,
      'total-amount': null,
      'gsm-service-type': null,
      'product-code': null,
      'unit': null,
      'start': '2018-07-30T12:00:00.000Z',
      'product-name': null
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151'
    },
    'id': '64302151',
    'type': 'balances'
  }, {
    'attributes': {
      'expiry-at': '9999-12-31T12:00:00.000Z',
      'dedicated-account-id': null,
      'balance-name': null,
      'lifecycle': null,
      'is-main-balance': false,
      'product-id': null,
      'offer-id': '10112',
      'amount': 0,
      'total-amount': null,
      'gsm-service-type': null,
      'product-code': null,
      'unit': null,
      'start': '2018-07-30T12:00:00.000Z',
      'product-name': null
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151'
    },
    'id': '64302151',
    'type': 'balances'
  }, {
    'attributes': {
      'expiry-at': '2020-08-19T12:00:00.000Z',
      'dedicated-account-id': '143',
      'balance-name': '143',
      'lifecycle': null,
      'is-main-balance': false,
      'product-id': null,
      'offer-id': null,
      'amount': 0.00,
      'total-amount': 0.00,
      'gsm-service-type': 'data',
      'product-code': '25MBLOGINBONUS',
      'unit': 'MB',
      'start': null,
      'product-name': {
        'en': '25MB Login Bonus-4Days',
        'bn': '?????? ????? - ? ???'
      }
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151-143/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151-143'
    },
    'id': '64302151-143',
    'type': 'balances'
  }, {
    'attributes': {
      'expiry-at': '9999-12-31T12:00:00.000Z',
      'dedicated-account-id': null,
      'balance-name': null,
      'lifecycle': null,
      'is-main-balance': false,
      'product-id': null,
      'offer-id': '10113',
      'amount': 0,
      'total-amount': null,
      'gsm-service-type': null,
      'product-code': null,
      'unit': null,
      'start': '2018-07-30T12:00:00.000Z',
      'product-name': null
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151'
    },
    'id': '64302151',
    'type': 'balances'
  }, {
    'attributes': {
      'expiry-at': null,
      'dedicated-account-id': null,
      'balance-name': null,
      'lifecycle': {
        'service-removal-date': '2024-02-17T12:00:00.000Z',
        'credit-clearance-date': '2024-05-20T12:00:00.000Z',
        'supervision-expiry-date': '2021-08-01T12:00:00.000Z',
        'service-fee-expiry-date': '2021-08-31T12:00:00.000Z'
      },
      'is-main-balance': true,
      'product-id': null,
      'offer-id': null,
      'amount': 1464.76,
      'total-amount': null,
      'gsm-service-type': null,
      'product-code': null,
      'unit': 'BDT',
      'start': null,
      'product-name': null
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151'
    },
    'id': '64302151',
    'type': 'balances'
  }, {
    'attributes': {
      'expiry-at': '2022-12-31T12:00:00.000Z',
      'dedicated-account-id': null,
      'balance-name': null,
      'lifecycle': null,
      'is-main-balance': false,
      'product-id': null,
      'offer-id': '8742',
      'amount': 0,
      'total-amount': null,
      'gsm-service-type': null,
      'product-code': null,
      'unit': null,
      'start': '2019-11-13T12:00:00.000Z',
      'product-name': null
    },
    'relationships': {
      'subscription': {
        'data': {
          'type': 'subscriptions',
          'id': '64302151'
        },
        'links': {
          'related': '/api/v1/balances/64302151/subscription'
        }
      }
    },
    'links': {
      'self': '/api/v1/balances/64302151'
    },
    'id': '64302151',
    'type': 'balances'
  }]
}
";
            
            object obj = JsonConvert.DeserializeObject(s);
            Thread.Sleep(int.Parse(WebConfigurationManager.AppSettings["dbssTimeOut"]));
            return obj;
        }
        [HttpGet]
        public Object CMS()
        {
            ViewBag.Title = "Home Page";

            string s = @"
[
   {
      'offerID':'20319',
      'offerName':'Prepaid_766_BTL_DATA_2GB@49Tk-3D_DBSS',
      'offerDescription':'2GB-@49Tk-3D (Incl all taxes)',
      'offerScore':'0',
      'offerRank':'1',
      'imageURL':null,
      'offerPrice':'49.0',
      'offerDescriptionWeb':'VOICE|0|MIN;SMS|0|SMS;DATA|2|GB;TK|49|TK;VAL|3|DAYS;TARIFF|0|P/SEC ON-NET;TARIFF|0|P/SEC OFF-NET;CAT|DAT|',
      'offerShortDescription':'2GB@49Tk-3D',
      'offerLongDescription':'2GB@49Tk-3D (Incl all taxes)'
   },
   {
      'offerID':'20280',
      'offerName':'DBSS_60min@37Tk-7Day',
      'offerDescription':'60min@35Tk-7Day',
      'offerScore':'0',
      'offerRank':'2',
      'imageURL':null,
      'offerPrice':'35.0',
      'offerDescriptionWeb':'VOICE|60|MIN;SMS|0|SMS;DATA|0|MB;TK|37|TK;VAL|7|DAYS;TARIFF|0|P/SEC ON-NET;TARIFF|0|P/SEC OFF-NET;CAT|VOI|',
      'offerShortDescription':'60min@37Tk-7D',
      'offerLongDescription':'60min@37Tk- 7Day (including taxes)'
   },
   {
      'offerID':'20549',
      'offerName':'Prepaid_6469_BTL_DATA_4GB@68Tk-4D_DBSS',
      'offerDescription':'4GB@68Tk-4D (Incl all taxes)',
      'offerScore':'0',
      'offerRank':'3',
      'imageURL':null,
      'offerPrice':'68.0',
      'offerDescriptionWeb':'VOICE|0|MIN;SMS|0|SMS;DATA|4|GB;TK|68|TK;VAL|4|DAYS;TARIFF|0|P/SEC ON-NET;TARIFF|0|P/SEC OFF-NET;CAT|DAT|',
      'offerShortDescription':'4GB@68Tk-4D',
      'offerLongDescription':'4GB@68Tk-4D (Incl all taxes)'
   },
   {
      'offerID':'20913',
      'offerName':'DBSS_35min-300MB-10SMS-7D-38tk',
      'offerDescription':'35min-300MB-10SMS-7D-38tk (including all taxes)',
      'offerScore':'0',
      'offerRank':'4',
      'imageURL':null,
      'offerPrice':'38.0',
      'offerDescriptionWeb':'VOICE|35|MIN;SMS|10|SMS;DATA|300|MB;TK|38|TK;VAL|7|DAY;TARIFF|0|P/SEC ON-NET;TARIFF|0|P/SEC OFF-NET;CAT|VOI|',
      'offerShortDescription':'35min+300mb+10sms@38Tk-7D',
      'offerLongDescription':'35min+300mb+10sms@38Tk-7D (including all taxes)'
   },
   {
      'offerID':'20937',
      'offerName':'6708_BTL_DATA_5GB@79Tk-5D_DBSS',
      'offerDescription':'5GB@79Tk-5D (Incl all taxes)',
      'offerScore':'0',
      'offerRank':'5',
      'imageURL':null,
      'offerPrice':'79.0',
      'offerDescriptionWeb':'VOICE|0|MIN;SMS|0|SMS;DATA|5|GB;TK|79|TK;VAL|5|DAYS;TARIFF|0|P/SEC ON-NET;TARIFF|0|P/SEC OFF-NET;CAT|DAT|',
      'offerShortDescription':'5GB@79Tk-5D',
      'offerLongDescription':'5GB@79Tk-5D (Incl all taxes)'
   },
   {
      'offerID':'20547',
      'offerName':'DBSS_90min@57Tk-7 day',
      'offerDescription':'99min@57Tk-7 day (including all taxes)',
      'offerScore':'0',
      'offerRank':'6',
      'imageURL':null,
      'offerPrice':'57.0',
      'offerDescriptionWeb':'VOICE|90|MIN;SMS|0|SMS;DATA|0|MB;TK|57|TK;VAL|7|DAY;TARIFF|0|P/SEC ON-NET;TARIFF|0|P/SEC OFF-NET;CAT|VOI|',
      'offerShortDescription':'90min@57Tk-7D',
      'offerLongDescription':'90min@57Tk-7 days (including all taxes)'
   }
]
";
            
            object obj = JsonConvert.DeserializeObject(s);
            Thread.Sleep(int.Parse(WebConfigurationManager.AppSettings["cmsTimeOut"]));
            return obj;
        }
    
    }
}
