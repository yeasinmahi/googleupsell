using System;
using System.Threading.Tasks;
using BusinessEntity;
using DataAccess;
using SQLFactory;

namespace BusinessObject
{
    #region Object of BOServiceAPI

    public class BOServiceAPI
    {
        #region Destructor

        ~BOServiceAPI()
        {
        }

        #endregion

        #region Save

        public async Task Save(BEServiceAPI serviceAPI)
        {
            try
            {
                sqlHelper = new SQLHelper(true);
                await daServiceAPI.Save(sqlHelper, serviceAPI);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
        }

        #endregion

        #region Delete

        public async Task Delete(int iD, int deletedBy)
        {
            try
            {
                sqlHelper = new SQLHelper(true);
                daServiceAPI.Delete(sqlHelper, iD, deletedBy);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
        }

        #endregion

        public async Task<BEServiceAPIs> GetServiceAPIs(BEServiceAPI searchCriteria,
            int pageIndex, int numberOfRecordsPerPage, string orderByQuery)
        {
            BEServiceAPIs serviceAPIs = null;
            var daServiceAPI = new DAServiceAPI();

            try
            {
                sqlHelper = new SQLHelper();
                serviceAPIs = await daServiceAPI.GetServiceAPIs(sqlHelper, searchCriteria, pageIndex,
                    numberOfRecordsPerPage, orderByQuery);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return serviceAPIs;
        }

        #region Constructor

        #endregion

        #region Declaration

        private SQLHelper sqlHelper;
        private readonly DAServiceAPI daServiceAPI = new DAServiceAPI();

        #endregion

        #region Get

        public async Task<BEServiceAPI> GetServiceAPI(int iD)
        {
            BEServiceAPI serviceAPI = null;
            var daServiceAPI = new DAServiceAPI();

            try
            {
                sqlHelper = new SQLHelper();
                serviceAPI = await daServiceAPI.GetServiceAPI(sqlHelper, iD);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
            return serviceAPI;
        }

        public async Task<BEServiceAPIs> GetServiceAPIs(bool activeOnly)
        {
            BEServiceAPIs serviceAPIs = null;
            var daServiceAPI = new DAServiceAPI();

            try
            {
                sqlHelper = new SQLHelper();
                serviceAPIs = await daServiceAPI.GetServiceAPIs(sqlHelper, activeOnly);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return serviceAPIs;
        }

        #endregion
    }

    #endregion
}