﻿//using BusinessEntity.APIHub2;
//using DataAccess;
//using SQLFactory;
//using System;
//using System.Threading.Tasks;

//namespace BusinessObject
//{
//    #region Object of BOErrorLog
//    public class BOErrorLog
//    {
//        #region Constructor
//        public BOErrorLog() { }
//        #endregion

//        #region Destructor
//        ~BOErrorLog() { }
//        #endregion

//        #region Declaration
//        SQLHelper sqlHelper = null;
//        DAErrorLog daErrorLog = new DAErrorLog();
//        #endregion

//        #region Save
//        public async Task Create(BEErrorLog log)
//        {
//            SQLHelper createSQLHelper = null;
//            DAErrorLog daLog = new DAErrorLog();
//            try
//            {
//                createSQLHelper = new SQLHelper(true);
//                await daLog.Create(createSQLHelper, log);
//                createSQLHelper.CommitTran();
//            }
//            catch (Exception ex)
//            {
//                if (createSQLHelper != null) createSQLHelper.Rollback();
//                throw ex;
//            }
//            finally
//            {
//                createSQLHelper = null;
//            }
//        }

//        public async Task Update(BEErrorLog log)
//        {
//            try
//            {
//                sqlHelper = new SQLHelper(true);
//                await daErrorLog.Create(sqlHelper, log);
//                sqlHelper.CommitTran();
//            }
//            catch (Exception ex)
//            {
//                if (sqlHelper != null) sqlHelper.Rollback();
//                throw ex;
//            }
//            finally
//            {
//                sqlHelper = null;
//            }
//        }

//        #endregion

//        #region Delete
//        public async Task Delete(int logId, int deletedBy)
//        {
//            try
//            {
//                sqlHelper = new SQLHelper(true);
//                await daErrorLog.Delete(sqlHelper, logId);
//                sqlHelper.CommitTran();
//            }
//            catch (Exception ex)
//            {
//                if (sqlHelper != null) sqlHelper.Rollback();
//                throw ex;
//            }
//            finally
//            {
//                sqlHelper = null;
//            }
//        }
//        #endregion

//        #region Get
//        public async Task<BEErrorLog> GetErrorLog(int logId)
//        {
//            BEErrorLog bundleProduct = null;
//            DAErrorLog daErrorLog = new DAErrorLog();

//            try
//            {
//                sqlHelper = new SQLHelper(true);
//                bundleProduct = await daErrorLog.GetErrorLog(sqlHelper, logId);
//                sqlHelper.CommitTran();
//            }
//            catch (Exception ex)
//            {
//                if (sqlHelper != null) sqlHelper.Rollback();
//                throw ex;
//            }
//            finally
//            {
//                sqlHelper = null;
//            }
//            return bundleProduct;
//        }

//        public async Task<BEErrorLogs> GetErrorLogs(bool activeOnly)
//        {
//            BEErrorLogs bundleProducts = null;
//            DAErrorLog daErrorLog = new DAErrorLog();

//            try
//            {
//                sqlHelper = new SQLHelper("VEON_APPS");
//                bundleProducts = await daErrorLog.GetErrorLogs(sqlHelper, activeOnly);
//                sqlHelper.CommitTran();
//            }
//            catch (Exception ex)
//            {
//                if (sqlHelper != null) sqlHelper.Rollback();
//                throw ex;
//            }
//            finally
//            {
//                sqlHelper = null;
//            }
//            return bundleProducts;
//        }
//        #endregion

//    }
//    #endregion
//}

