﻿//using BusinessEntity.APIHub2;
//using DataAccess;
//using SQLFactory;
//using System;
//using System.Threading.Tasks;

//namespace BusinessObject
//{
//    #region Object of BORequestResponseLog
//    public class BORequestResponseLog
//    {
//        #region Constructor
//        public BORequestResponseLog() { }
//        #endregion

//        #region Destructor
//        ~BORequestResponseLog() { }
//        #endregion

//        #region Declaration
//        SQLHelper sqlHelper = null;
//        DARequestResponseLog daRequestResponseLog = new DARequestResponseLog();
//        #endregion

//        #region Save
//        public async Task Create(BERequestResponseLog log)
//        {
//            SQLHelper createSQLHelper = null;
//            DARequestResponseLog daLog = new DARequestResponseLog();
//            try
//            {
//                createSQLHelper = new SQLHelper(true);
//                await daLog.Create(createSQLHelper, log);
//                createSQLHelper.CommitTran();
//            }
//            catch (Exception ex)
//            {
//                if (createSQLHelper != null) createSQLHelper.Rollback();
//                throw ex;
//            }
//            finally
//            {
//                createSQLHelper = null;
//            }
//        }

//        public async Task Update(BERequestResponseLog log)
//        {
//            try
//            {
//                sqlHelper = new SQLHelper(true);
//                daRequestResponseLog.Create(sqlHelper, log);
//                sqlHelper.CommitTran();
//            }
//            catch (Exception ex)
//            {
//                if (sqlHelper != null) sqlHelper.Rollback();
//                throw ex;
//            }
//            finally
//            {
//                sqlHelper = null;
//            }
//        }

//        #endregion

//        #region Delete
//        public async Task Delete(int logId, int deletedBy)
//        {
//            try
//            {
//                sqlHelper = new SQLHelper(true);
//                daRequestResponseLog.Delete(sqlHelper, logId);
//                sqlHelper.CommitTran();
//            }
//            catch (Exception ex)
//            {
//                if (sqlHelper != null) sqlHelper.Rollback();
//                throw ex;
//            }
//            finally
//            {
//                sqlHelper = null;
//            }
//        }
//        #endregion

//        #region Get
//        public async Task<BERequestResponseLog> GetRequestResponseLog(int logId)
//        {
//            BERequestResponseLog bundleProduct = null;
//            DARequestResponseLog daRequestResponseLog = new DARequestResponseLog();

//            try
//            {
//                sqlHelper = new SQLHelper(true);
//                bundleProduct = await daRequestResponseLog.GetRequestResponseLog(sqlHelper, logId);
//                sqlHelper.CommitTran();
//            }
//            catch (Exception ex)
//            {
//                if (sqlHelper != null) sqlHelper.Rollback();
//                throw ex;
//            }
//            finally
//            {
//                sqlHelper = null;
//            }
//            return bundleProduct;
//        }

//        public async Task<BERequestResponseLogs> GetRequestResponseLogs(bool activeOnly)
//        {
//            BERequestResponseLogs bundleProducts = null;
//            DARequestResponseLog daRequestResponseLog = new DARequestResponseLog();

//            try
//            {
//                sqlHelper = new SQLHelper("VEON_APPS");
//                bundleProducts = await daRequestResponseLog.GetRequestResponseLogs(sqlHelper, activeOnly);
//                sqlHelper.CommitTran();
//            }
//            catch (Exception ex)
//            {
//                if (sqlHelper != null) sqlHelper.Rollback();
//                throw ex;
//            }
//            finally
//            {
//                sqlHelper = null;
//            }
//            return bundleProducts;
//        }
//        #endregion

//    }
//    #endregion
//}

