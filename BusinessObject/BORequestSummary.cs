﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using DataAccess;
using SQLFactory;

namespace BusinessObject
{
    public class BORequestSummary
    {
        //private static int x = 0;
        public static Dictionary<string, BERequestSummary> dictionary = new Dictionary<string, BERequestSummary>();

        //public static async Task Add(string requestName, double rtt)
        //{
        //    BERequestSummary requestSummary = null;

        //    if (dictionary.ContainsKey(requestName))
        //    {
        //         requestSummary = dictionary[requestName];
        //    }
        //    else
        //    {
        //        requestSummary = new BERequestSummary(requestName);
        //        dictionary.Add(requestName, requestSummary);
        //    }

        //    requestSummary.TotalRequest++;

        //    requestSummary.AverageTime = (requestSummary.AverageTime * requestSummary.TotalRequest + rtt)
        //        /
        //        (requestSummary.TotalRequest);
        //    if (requestSummary.MaxTime < rtt)
        //        requestSummary.MaxTime = rtt;

        //    if (requestSummary.MinimumTime > rtt)
        //        requestSummary.MinimumTime = rtt;

        //    if (requestSummary.TotalRequest > 3 || DateTime.Now.Subtract(requestSummary.RecordDate).Minutes > 5)
        //    {
        //        dictionary[requestName] = new BERequestSummary(requestName);
        //        //dictionary.Add(requestName, requestSummary);
        //        Save(requestSummary);
        //    }

        //}

        //private static async Task Save(BERequestSummary requestSummary)
        //{
        //    DARequestSummary daRequestSummary = new DARequestSummary();

        //    SQLHelper sqlHelper = null;
        //    try
        //    {
        //        sqlHelper = new SQLHelper(true);
        //        daRequestSummary.Save(sqlHelper, requestSummary);
        //        sqlHelper.CommitTran();
        //    }
        //    catch (Exception ex)
        //    {
        //        if (sqlHelper != null) sqlHelper.Rollback();
        //        throw ex;
        //    }
        //    finally
        //    {
        //        sqlHelper = null;
        //    }
        //}


        public async Task Add(string requestName, double rtt, bool isError)
        {
            try
            {
                BERequestSummary requestSummary = null;

                if (dictionary.ContainsKey(requestName))
                {
                    requestSummary = dictionary[requestName];
                }
                else
                {
                    requestSummary = new BERequestSummary(requestName);
                    dictionary.Add(requestName, requestSummary);
                }

                requestSummary.TotalRtt += rtt;
                //requestSummary.AverageTime = (requestSummary.AverageTime * requestSummary.TotalRequest + rtt)
                //                             /
                //                             (requestSummary.TotalRequest + 1);
                if (requestSummary.MaxTime < rtt)
                    requestSummary.MaxTime = rtt;

                if (requestSummary.MinimumTime > rtt)
                    requestSummary.MinimumTime = rtt;

                requestSummary.TotalRequest++;

                requestSummary.ErrorCount += Convert.ToInt16(isError);

                if (requestSummary.TotalRequest >= AppSettings.TotalRequestSummary ||
                    DateTime.Now.Subtract(requestSummary.RecordDate).Minutes > 5)
                {
                    requestSummary.AverageTime = requestSummary.TotalRtt / requestSummary.TotalRequest;
                    dictionary.Remove(requestName);
                    dictionary[requestName] = new BERequestSummary(requestName);
                    var isSave = SaveAsync(requestSummary);
                }

                //async Task<bool> isSave = saveAsync(requestSummary);
            }
            catch (Exception ex)
            {
                var x = ex.Message;
            }
        }

        private async Task<bool> SaveAsync(BERequestSummary requestSummary)
        {
            var isSuccess = false;
            try
            {
                //x++;
                //requestSummary.RequestName += x;
                //if (x % 2 == 1)
                //    await async Task.Delay(2000); // 1 second delay
                //int k=0;
                //if (x % 2 == 1)
                //    for (long i = 0; i < 3000000; i++)
                //    {
                //        k=0;
                //    }

                var daRequestSummary = new DARequestSummary();

                SQLHelper sqlHelper = null;
                try
                {
                    //sqlHelper = new SQLHelper("mnp", true);
                    sqlHelper = new SQLHelper(true);
                    await daRequestSummary.Save(sqlHelper, requestSummary);
                    sqlHelper.CommitTran();
                    isSuccess = true;
                }
                catch (Exception ex)
                {
                    sqlHelper?.Rollback();
                    throw;
                }
                finally
                {
                    //sqlHelper?.Dispose();
                }
            }
            catch (Exception ex)
            {
                var x = ex.Message;
            }

            return isSuccess;
        }


        //public static async Task Add(string requestName, double rtt)
        //{
        //    BERequestSummary requestSummary = null;

        //    if (dictionary.ContainsKey(requestName))
        //    {
        //        requestSummary = dictionary[requestName];
        //    }
        //    else
        //    {
        //        requestSummary = new BERequestSummary(requestName);
        //        dictionary.Add(requestName, requestSummary);
        //    }

        //    requestSummary.TotalRequest++;

        //    requestSummary.AverageTime = (requestSummary.AverageTime * requestSummary.TotalRequest + rtt)
        //        /
        //        (requestSummary.TotalRequest);
        //    if (requestSummary.MaxTime < rtt)
        //        requestSummary.MaxTime = rtt;

        //    if (requestSummary.MinimumTime > rtt)
        //        requestSummary.MinimumTime = rtt;

        //    if (requestSummary.TotalRequest > 3 || DateTime.Now.Subtract(requestSummary.RecordDate).Minutes > 5)
        //    {
        //        dictionary[requestName] = new BERequestSummary(requestName);
        //        //dictionary.Add(requestName, requestSummary);
        //       // Save(requestSummary);
        //        async Task<bool> isSave = saveAsyc(requestSummary);
        //    }

        //}
        //public async Task MyMethodAsync()
        //{
        //    BERequestSummary requestSummary = null;
        //    async Task<bool> isSave = saveAsyc(requestSummary);
        //    // independent work which doesn't need the result of LongRunningOperationAsync can be done here

        //    //and now we call await on the task 
        //    int result = await longRunningTask;
        //    //use the result 
        //    Console.WriteLine(result);
        //}

        //public static async Task<bool> saveAsyc(BERequestSummary requestSummary) // assume we return an int from this long running operation 
        //{
        //    bool isSuccess = false;
        //    DARequestSummary daRequestSummary = new DARequestSummary();
        //    SQLHelper sqlHelper = null;
        //    try
        //    {
        //        sqlHelper = new SQLHelper(true);
        //        daRequestSummary.Save(sqlHelper, requestSummary);
        //        sqlHelper.CommitTran();
        //        isSuccess = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (sqlHelper != null) sqlHelper.Rollback();
        //        throw ex;
        //    }
        //    finally
        //    {
        //        sqlHelper = null;
        //    }

        //    if( requestSummary.TotalRequest%2==1)
        //    await async Task.Delay(10000); // 1 second delay
        //    return isSuccess;
        //}
        //private static  async Task Save(BERequestSummary requestSummary)
        //{
        //    DARequestSummary daRequestSummary = new DARequestSummary();

        //    SQLHelper sqlHelper = null;
        //    try
        //    {
        //        sqlHelper = new SQLHelper(true);
        //        daRequestSummary.Save(sqlHelper, requestSummary);
        //        sqlHelper.CommitTran();
        //    }
        //    catch (Exception ex)
        //    {
        //        if (sqlHelper != null) sqlHelper.Rollback();
        //        throw ex;
        //    }
        //    finally
        //    {
        //        sqlHelper = null;
        //    }
        //}


        //public async Task<int> longRunningTask { get; set; }
    }
}