﻿using System;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using DataAccess;
using SQLFactory;

namespace BusinessObject
{
    public class BOServiceRole
    {
        #region Destructor

        ~BOServiceRole()
        {
        }

        #endregion


        public async Task<BEServiceRoles> GetServiceRole()
        {
            BEServiceRoles serviceUser = null;


            var daServiceRole = new DAServiceRole();
            SQLHelper sqlHelper = null;

            try
            {
                //sqlHelper = new SQLHelper("HVCConnection");
                sqlHelper = new SQLHelper("HVCDB");
                serviceUser = await daServiceRole.GetServiceRole(sqlHelper);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return serviceUser;
        }

        #region Constructor

        #endregion

        #region Declaration

        private SQLHelper sqlHelper = null;
        private DAServiceRole DAServiceRole = new DAServiceRole();

        #endregion
    }
}