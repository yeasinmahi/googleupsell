﻿using System;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using DataAccess;
using SQLFactory;

namespace BusinessObject
{
    public class BOOTP
    {
        //private SQLHelper sqlHelper;

        public async Task SaveOTPAndSendSMSNew(string sender, string msisdnNo, string otpNo, string otpMsg,
            bool isSendToMobile, int otpIntervalTime, int otpMinimumInADay)
        {
            await SaveOTPAndSendSMSNew(sender, msisdnNo, otpNo, otpMsg, isSendToMobile, 0, otpIntervalTime,
                otpMinimumInADay);


        }

        public async Task SaveOTPAndSendSMSNew(string sender, string msisdnNo, string otpNo, string otpMsg,
            bool isSendToMobile,
            int pageType, int otpIntervalTime, int otpMinimumInADay)
        {
            SQLHelper sqlHelper = null;
            try
            {
                var daOTP = new DAOTP();
                var msisdnSMS = msisdnNo;
                //sqlHelper = new SQLHelper("HVCConnection");

                sqlHelper = new SQLHelper();

                await daOTP.CheckOTP(sqlHelper, msisdnNo, otpIntervalTime, otpMinimumInADay);

                await daOTP.SaveOTP(sqlHelper, msisdnNo, otpNo, pageType);

                if (isSendToMobile)
                {
                    if (msisdnNo.Length < 11) msisdnSMS = "0" + msisdnNo;
                    otpMsg = string.Format(otpMsg, msisdnSMS, otpNo, "&");

                    await daOTP.SendSMS(sqlHelper, sender, msisdnNo, otpMsg, SMSChannelEnum.APIHub);
                }

                sqlHelper.CommitTran();
            }
            catch (BeHandledException)
            {
                sqlHelper?.Rollback();
                throw;
            }
            catch (Exception)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
        }

        public async Task<bool> IsValidOTPSHA1Encrypted(string MSISDN, string OTP)
        {
            //  BOOTP boOTP = new BOOTP();

            //  DateTime otpDateTime = boOTP.HasValidotpWithinValidityPeriod(Convert.ToInt16(ConfigurationManager.AppSettings["OTPValidityDuration"]), MSISDN);

            var strOTP = string.Empty;

            int otpValidityDuration = Convert.ToInt16(AppSettings.OTPValidityDuration);
            var otpDateTime = DateTime.MinValue;
            //int otpWrongAttempLimit = Convert.ToInt16(AppSettings.OTPWrongAttempLimit);
            var isValidOTP = await IsValidOTP(MSISDN, otpValidityDuration, OTP, 0, AppSettings.OTPWrongAttempLimit);

            if (isValidOTP)
                return true;
            throw new Exception("OTP is not valid");
        }

        public async Task<bool> IsValidOTP(string msisdnNo, int otpValidityDuration, string OTP, int pageType,
            int otpWrongAttempLimit)
        {
            var isValidOTP = false;
            SQLHelper sqlHelper = null;
            try
            {
                var daOTP = new DAOTP();
                //sqlHelper = new SQLHelper("HVCConnection");
                sqlHelper = new SQLHelper();
                isValidOTP = await daOTP.IsValidOTP(sqlHelper, msisdnNo, otpValidityDuration, OTP, pageType,
                    otpWrongAttempLimit);
                sqlHelper.CommitTran();
            }
            catch (BeHandledException)
            {
                // HVC.Common.Utility.SaveErrorLog(this.GetType().ToString(), "", ex);
                sqlHelper?.Rollback();
                throw;
            }
            catch (Exception)
            {
                // HVC.Common.Utility.SaveErrorLog(this.GetType().ToString(), "", ex);
                sqlHelper?.Rollback();
                throw;
            }

            return isValidOTP;

            // return otp;
        }
    }
}