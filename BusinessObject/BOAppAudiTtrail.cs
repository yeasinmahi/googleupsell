using System;
using System.Threading.Tasks;
using BusinessEntity.Logging;
using DataAccess;
using SQLFactory;

namespace BusinessObject
{
    #region Object of BOAppAudiTtrail

    public class BOAppAudiTtrail
    {
        #region Destructor

        ~BOAppAudiTtrail()
        {
        }

        #endregion

        #region Save

        public async Task Save(BEAppAudiTtrail appAudiTtrail)
        {
            try
            {
                sqlHelper = new SQLHelper(true);
                await daAppAudiTtrail.Save(sqlHelper, appAudiTtrail);

                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                sqlHelper = null;
            }
        }

        #endregion

        #region Delete

        public async Task Delete(int id, int deletedBy)
        {
            try
            {
                sqlHelper = new SQLHelper(true);
                daAppAudiTtrail.Delete(sqlHelper, id, deletedBy);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                sqlHelper = null;
            }
        }

        #endregion

        public async Task<BEAppAudiTtrails> GetAppAudiTtrails(BEAppAudiTtrail searchCriteria,
            int pageIndex, int numberOfRecordsPerPage, string orderByQuery)
        {
            BEAppAudiTtrails appAudiTtrails = null;
            var daAppAudiTtrail = new DAAppAudiTtrail();

            try
            {
                sqlHelper = new SQLHelper();
                appAudiTtrails = await daAppAudiTtrail.GetAppAudiTtrails(sqlHelper, searchCriteria, pageIndex,
                    numberOfRecordsPerPage, orderByQuery);
                sqlHelper.CommitTran();
            }
            catch (Exception)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return appAudiTtrails;
        }

        #region Constructor

        #endregion

        #region Declaration

        private SQLHelper sqlHelper;
        private readonly DAAppAudiTtrail daAppAudiTtrail = new DAAppAudiTtrail();

        #endregion

        #region Get

        public async Task<BEAppAudiTtrail> GetAppAudiTtrail(int id)
        {
            BEAppAudiTtrail appAudiTtrail = null;
            var daAppAudiTtrail = new DAAppAudiTtrail();

            try
            {
                sqlHelper = new SQLHelper();
                appAudiTtrail = await daAppAudiTtrail.GetAppAudiTtrail(sqlHelper, id);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return appAudiTtrail;
        }

        public async Task<BEAppAudiTtrails> GetAppAudiTtrails(bool activeOnly)
        {
            BEAppAudiTtrails appAudiTtrails = null;
            var daAppAudiTtrail = new DAAppAudiTtrail();

            try
            {
                sqlHelper = new SQLHelper();
                appAudiTtrails = await daAppAudiTtrail.GetAppAudiTtrails(sqlHelper, activeOnly);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return appAudiTtrails;
        }

        #endregion
    }

    #endregion
}