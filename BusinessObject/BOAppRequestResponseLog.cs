using System;
using System.Threading.Tasks;
using BusinessEntity.Logging;
using DataAccess;
using SQLFactory;

namespace BusinessObject
{
    #region Object of BOAppRequestResponseLog

    public class BOAppRequestResponseLog
    {
        #region Destructor

        ~BOAppRequestResponseLog()
        {
        }

        #endregion

        #region Delete

        public async Task Delete(int id, int deletedBy)
        {
            try
            {
                sqlHelper = new SQLHelper(true);
                daAppRequestResponseLog.Delete(sqlHelper, id, deletedBy);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
        }

        #endregion

        public async Task<BEAppRequestResponseLogs> GetAppRequestResponseLogs(BEAppRequestResponseLog searchCriteria,
            int pageIndex, int numberOfRecordsPerPage, string orderByQuery)
        {
            BEAppRequestResponseLogs appRequestResponseLogs = null;
            var daAppRequestResponseLog = new DAAppRequestResponseLog();

            try
            {
                sqlHelper = new SQLHelper();
                appRequestResponseLogs = await daAppRequestResponseLog.GetAppRequestResponseLogs(sqlHelper,
                    searchCriteria, pageIndex, numberOfRecordsPerPage, orderByQuery);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return appRequestResponseLogs;
        }

        #region Constructor

        #endregion

        #region Declaration

        private SQLHelper sqlHelper;
        private readonly DAAppRequestResponseLog daAppRequestResponseLog = new DAAppRequestResponseLog();

        #endregion

        #region Save

        public async Task Save(BEAppRequestResponseLog appRequestResponseLog)
        {
            try
            {
                //sqlHelper = new SQLHelper("mnp");
                sqlHelper = new SQLHelper(true);
                await daAppRequestResponseLog.Save(sqlHelper, appRequestResponseLog);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
        }

        public async Task Create(BEAppRequestResponseLog log)
        {
            SQLHelper createSQLHelper = null;
            var daLog = new DAAppRequestResponseLog();
            try
            {
                createSQLHelper = new SQLHelper(true);
                await daLog.Create(createSQLHelper, log);
                createSQLHelper.CommitTran();
            }
            catch (Exception ex)
            {
                createSQLHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
        }

        #endregion

        #region Get

        public async Task<BEAppRequestResponseLog> GetAppRequestResponseLog(int id)
        {
            BEAppRequestResponseLog appRequestResponseLog = null;
            var daAppRequestResponseLog = new DAAppRequestResponseLog();

            try
            {
                sqlHelper = new SQLHelper();
                appRequestResponseLog = await daAppRequestResponseLog.GetAppRequestResponseLog(sqlHelper, id);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return appRequestResponseLog;
        }

        public async Task<BEAppRequestResponseLogs> GetAppRequestResponseLogs(bool activeOnly)
        {
            BEAppRequestResponseLogs appRequestResponseLogs = null;
            var daAppRequestResponseLog = new DAAppRequestResponseLog();

            try
            {
                sqlHelper = new SQLHelper();
                appRequestResponseLogs = await daAppRequestResponseLog.GetAppRequestResponseLogs(sqlHelper, activeOnly);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return appRequestResponseLogs;
        }

        #endregion
    }

    #endregion
}