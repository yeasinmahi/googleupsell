//using System;
//using BusinessEntity.OGate;
//using DataAccess;
//using SQLFactory;

//namespace BusinessObject
//{
//	#region Object of BOCpidHistory
//	public class BOCpidHistory
//	{
//		#region Constructor
//		public BOCpidHistory() { }
//		#endregion

//		#region Destructor
//		~BOCpidHistory() { }
//		#endregion

//		#region Declaration
//		SQLHelper sqlHelper = null;
//		DACpidHistory daCpidHistory = new DACpidHistory();
//		#endregion

//		#region Save
//		public async Task Save(BECpidHistory cpidHistory)
//		{
//			try
//			{
//				sqlHelper = new SQLHelper(true);
//				daCpidHistory.Save(sqlHelper, cpidHistory);
//				sqlHelper.CommitTran();
//			}
//			catch (Exception ex)
//			{
//				if (sqlHelper != null) sqlHelper.Rollback();
//				throw ex;
//			}
//			finally
//			{
//				sqlHelper = null;
//			}
//		}
//		#endregion

//		#region Delete
//		public async Task Delete(int id, int deletedBy)
//		{
//			try
//			{
//				sqlHelper = new SQLHelper(true);
//				daCpidHistory.Delete(sqlHelper, id, deletedBy);
//				sqlHelper.CommitTran();
//			}
//			catch (Exception ex)
//			{
//				if (sqlHelper != null) sqlHelper.Rollback();
//				throw ex;
//			}
//			finally
//			{
//				sqlHelper = null;
//			}
//		}
//		#endregion

//		#region Get
//		public BECpidHistory GetCpidHistory(int id)
//		{
//			BECpidHistory cpidHistory = null;
//			DACpidHistory daCpidHistory = new DACpidHistory();

//			try
//			{
//				sqlHelper = new SQLHelper();
//				cpidHistory = daCpidHistory.GetCpidHistory(sqlHelper, id);
//				sqlHelper.CommitTran();
//			}
//			catch (Exception ex)
//			{
//				if (sqlHelper != null) sqlHelper.Rollback();
//				throw ex;
//			}
//			finally
//			{
//				sqlHelper = null;
//			}
//			return cpidHistory;
//		}

//		public BECpidHistorys GetCpidHistorys(bool activeOnly)
//		{
//			BECpidHistorys cpidHistorys = null;
//			DACpidHistory daCpidHistory = new DACpidHistory();

//			try
//			{
//				sqlHelper = new SQLHelper();
//				cpidHistorys = daCpidHistory.GetCpidHistorys(sqlHelper, activeOnly);
//				sqlHelper.CommitTran();
//			}
//			catch (Exception ex)
//			{
//				if (sqlHelper != null) sqlHelper.Rollback();
//				throw ex;
//			}
//			finally
//			{
//				sqlHelper = null;
//			}
//			return cpidHistorys;
//		}
//		#endregion
//		public BECpidHistorys GetCpidHistorys(BECpidHistory searchCriteria,
// int pageIndex, int numberOfRecordsPerPage, string orderByQuery, out int totalCount)
//		{
//			BECpidHistorys cpidHistorys = null;
//			DACpidHistory daCpidHistory = new DACpidHistory();

//			try
//			{
//				sqlHelper = new SQLHelper();
//				cpidHistorys = daCpidHistory.GetCpidHistorys(sqlHelper, searchCriteria, pageIndex, numberOfRecordsPerPage, orderByQuery, out totalCount);
//				sqlHelper.CommitTran();
//			}
//			catch (Exception ex)
//			{
//				if (sqlHelper != null) sqlHelper.Rollback();
//				throw ex;
//			}
//			finally
//			{
//				sqlHelper = null;
//			}
//			return cpidHistorys;
//		}
//	}
//	#endregion

//}

