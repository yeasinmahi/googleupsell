﻿using System;
using System.Threading.Tasks;
using BusinessEntity.ConnectorModels.CMS;
using DataAccess;
using SQLFactory;

namespace BusinessObject
{
    public class BOrequestlog
    {
        #region Destructor

        ~BOrequestlog()
        {
        }

        #endregion

        #region Save

        public async Task Save(CmsRequestLog requestLog)
        {
            try
            {
                sqlHelper = new SQLHelper(true);
                await darequestlog.Save(sqlHelper, requestLog);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
        }

        #endregion

        #region Constructor

        #endregion

        #region Declaration

        private SQLHelper sqlHelper;
        private readonly DArequestlog darequestlog = new DArequestlog();

        #endregion
    }
}