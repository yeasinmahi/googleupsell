﻿using System;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using BusinessEntity.OGate;
using DataAccess;
using SQLFactory;
using Utility;

namespace BusinessObject
{
    public class BOCpid
    {
        private readonly DACpid daCpidHistory = new DACpid();
        private SQLHelper sqlHelper;

        public async Task<CpidResponse> GenerateCpid(string msisdn, string language, string provider, string app, bool storeInDB=true)
        {
            var encryption = new Encryption();
            var cpid = await encryption.EncryptBase64(await encryption.EncryptAes(
                msisdn + "|" + DateTime.Now.ToShortDateString() + "|" + DateTime.Now.ToShortTimeString() + "|" +
                language + "|" + provider, AppSettings.CpidSecrtKey));

            if (storeInDB)
            {
                await SaveCPID(msisdn, cpid, app);
            }
            return new CpidResponse { Cpid = cpid, TtlSeconds = AppSettings.TtlSeconds };
        }
        public async Task SaveCPID(string msisdn, string cpid, string app)
        {
            var beCpid = new BECpid();
            beCpid.isRegisterd = false;
            beCpid.Msisdn = msisdn;
            beCpid.Cpid = cpid;
            beCpid.Ttl = AppSettings.TtlSeconds;
            beCpid.AppId = app;
            beCpid.UpdateDate = DateTime.Now;
            beCpid.Provider = AppSettings.Provider;
            beCpid.CpidExpireDate = DateTime.Now.AddSeconds(beCpid.Ttl);
            //beCpid.RegisterCpid = string.Empty;
            //beCpidHistory.StaleTime = null;
            //beCpidHistory.UpdateDateRegister = null;
            //response.Provider = AppSettings.Provider;

            //boCpidHistory.Save(beCpidHistory);
            await Save(beCpid);
        }
        public async Task SaveRegisterCpid(string msisdn, string registerCpid, DateTime staleTime)
        {
            var beCpid = new BECpid();
            beCpid.isRegisterd = true;
            beCpid.Msisdn = msisdn;
            beCpid.Provider = AppSettings.Provider;
            beCpid.RegisterCpid = registerCpid;
            beCpid.StaleTime = staleTime;
            beCpid.Ttl = AppSettings.TtlSeconds;
            //boCpidHistory.Save(beCpidHistory);
            await Save(beCpid);
        }

        public async Task<CpidDecryptResponse> GetDycrypt(string cpid)
        {
            var encryption = new Encryption();
            var cpidDecrypt = new CpidDecryptResponse();
            var decbase = string.Empty;
            var text = string.Empty;
            try
            {
                decbase = await encryption.DecryptBase64(cpid);
            }
            catch
            {
                throw new BeHandledException(5006, "CPID is not base64 key");
            }

            try
            {
                try
                {
                    text = await encryption.DecryptAes(decbase, AppSettings.CpidSecrtKey);
                }
                catch (Exception e)
                {
                    text = await encryption.DecryptAesECBMode(decbase, AppSettings.CpidSecrtKey);
                }
                
            }
            catch
            {
                throw new BeHandledException(5006, "Can not decrypt AES");
            }

            if (string.IsNullOrWhiteSpace(text))
                //cpidDecrypt.IsSuccess = false;
                //cpidDecrypt.ErrorMessage = "Can not decrypt the key";
                //return cpidDecrypt;
                throw new BeHandledException(5006, "Can not decrypt the key");

            var data = text.Split("|");
            if (data.Length != 5)
                //cpidDecrypt.IsSuccess = false;
                //cpidDecrypt.ErrorMessage = "The Encrypted text is not well formated";
                //return cpidDecrypt;
                throw new BeHandledException(5006, "The Encrypted text is not well formated");
            cpidDecrypt.Status = 200;
            cpidDecrypt.Msisdn = data[0];
            cpidDecrypt.CreateDate = data[1];
            cpidDecrypt.CreateTime = data[2];
            cpidDecrypt.Language = data[3];
            cpidDecrypt.Provider = data[4];

            return cpidDecrypt;
        }

        public async Task<string> GetMsisdn(string cpid)
        {
            var cpidDecrypt = await GetDycrypt(cpid);
            if (cpidDecrypt.Status == 200) return cpidDecrypt.Msisdn;
            return cpidDecrypt.ErrorMessage;
        }

        public async Task Save(BECpid cpidHistory)
        {
            try
            {
                sqlHelper = new SQLHelper(true);
                await daCpidHistory.Save(sqlHelper, cpidHistory);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                sqlHelper = null;
            }
        }
        public async Task<string> GetCpid(string msisdn)
        {
            string cpid = string.Empty;
            try
            {
                sqlHelper = new SQLHelper(true);
                cpid = await daCpidHistory.GetCpid(sqlHelper, msisdn);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
            return cpid;
        }
        public async Task<string> GetRegisteredCpid(string msisdn)
        {
            string cpid = string.Empty;
            try
            {
                sqlHelper = new SQLHelper(true);
                cpid = await daCpidHistory.GetRegisteredCpid(sqlHelper, msisdn);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
            return cpid;
        }
    }
}