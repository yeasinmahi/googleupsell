﻿using System;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using DataAccess;
using SQLFactory;

namespace BusinessObject
{
    #region Object of BODataProduct

    public class BODataProduct
    {
        #region Destructor

        ~BODataProduct()
        {
        }

        #endregion

        #region Save

        public async Task Save(BEDataProduct dataProduct)
        {
            SQLHelper sqlHelper = null;
            DADataProduct daDataProduct = new DADataProduct();
            try
            {
                sqlHelper = new SQLHelper(true);
                await daDataProduct.Save(sqlHelper, dataProduct);
                sqlHelper.CommitTran();
            }
            catch (Exception)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
        }

        #endregion

        #region Delete

        public async Task Delete(int dataProductId, int deletedBy)
        {
            SQLHelper sqlHelper = null;
            DADataProduct daDataProduct=new DADataProduct();
            try
            {
                sqlHelper = new SQLHelper(true);
                await daDataProduct.Delete(sqlHelper, dataProductId, deletedBy);
                sqlHelper.CommitTran();
            }
            catch (Exception)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

        }

        #endregion

        public async Task<BEDataProducts> GetDataProducts(BEDataProduct searchCriteria,
            int pageIndex, int numberOfRecordsPerPage, string orderByQuery)
        {
            BEDataProducts dataProducts;
            var dataProduct = new DADataProduct();
            SQLHelper sqlHelper = null;
            try
            {
                sqlHelper = new SQLHelper("VEON_APPS");
                dataProducts = await dataProduct.GetDataProducts(sqlHelper, searchCriteria, pageIndex,
                    numberOfRecordsPerPage, orderByQuery);
                sqlHelper.CommitTran();
            }
            catch (Exception)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
            return dataProducts;
        }

        #region Constructor

        #endregion

        #region Declaration

        //private SQLHelper sqlHelper;
        //private readonly DADataProduct daDataProduct = new DADataProduct();

        #endregion

        #region Get

        public async Task<BEDataProduct> GetDataProduct(int dataProductId)
        {
            BEDataProduct dataProduct;
            var daDataProduct = new DADataProduct();
            SQLHelper sqlHelper = null;
            try
            {
                sqlHelper = new SQLHelper("VEON_APPS");
                dataProduct = await daDataProduct.GetDataProduct(sqlHelper, dataProductId);
                sqlHelper.CommitTran();
            }
            catch (Exception)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
            return dataProduct;
        }

        public async Task<BEDataProducts> GetDataProducts(bool activeOnly)
        {
            BEDataProducts dataProducts;
            var daDataProduct = new DADataProduct();
            SQLHelper sqlHelper = null;
            try
            {
                sqlHelper = new SQLHelper("VEON_APPS");
                dataProducts = await daDataProduct.GetDataProducts(sqlHelper, activeOnly);
                sqlHelper.CommitTran();
            }
            catch (Exception)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return dataProducts;
        }
        public async Task<string> GetRefeilProfileIdByPackCode(string packcode)
        {
            string refilProfileId;
            var daDataProduct = new DADataProduct();
            SQLHelper sqlHelper = null;
            try
            {
                sqlHelper = new SQLHelper("VEON_APPS");
                refilProfileId = await daDataProduct.GetRefeilProfileIdByPackCode(sqlHelper, packcode);
                sqlHelper.CommitTran();
            }
            catch (Exception)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return refilProfileId;
        }

        #endregion
    }

    #endregion
}