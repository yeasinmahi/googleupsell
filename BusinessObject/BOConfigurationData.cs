﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;

namespace BusinessObject
{
    public static class BOConfiguationData
    {
        public static double FullDataLoadingTime;
        public static double ServiceUserLoadingTime;

        public static Dictionary<string, BEServiceUser> ServiceUser = new Dictionary<string, BEServiceUser>();

        public static Dictionary<string, BEServiceUser> ServiceUserByClientSecret =
            new Dictionary<string, BEServiceUser>();

        public static BEDataProducts DataProducts = new BEDataProducts();

        //private static BEServiceUser serviceUser;
        public static DateTime ServiceUserRefreshTime { get; set; }
        //public static Dictionary<int, BEServiceRole> ServiceRole = new Dictionary<int, BEServiceRole>();
        //public static Dictionary<int, BEServiceUserRole> ServiceUserRole = new Dictionary<int, BEServiceUserRole>();

        public static BEServiceUser GetServiceUserByClientSecret(string client_id, string clientSecret)
        {
            if (!ServiceUserByClientSecret.ContainsKey(client_id + "-" + clientSecret))
                return new BEServiceUser();

            return ServiceUserByClientSecret[client_id + "-" + clientSecret];
        }

        //public static BEServiceUser GetServiceUserByID(string userID)
        //{
        //    if (!ServiceUserInfo.ContainsKey(userID))
        //        return new BEServiceUser();

        //    return ServiceUserInfo[userID];
        //}

        public static BEServiceUser GetServiceUserByClientID(string clientID)
        {
            if (!ServiceUser.ContainsKey(clientID))
                return new BEServiceUser();

            return ServiceUser[clientID];
        }


        //public static BEServiceUserRole GetServiceUserRoleByID(int userRoleID,int userID,int roleID)
        //{
        //    if (!ServiceUserRole.ContainsKey(userRoleID))
        //        return new BEServiceUserRole();

        //    return ServiceUserRole[userRoleID];
        //}

        // public static Task LoadServiceUser()
        public static Task LoadServiceUser(bool loadData)
        {
            if (!loadData) 
                return Task.FromResult<object>(null);

            return Task.Run(async () =>
            {
                var watch = Stopwatch.StartNew();

                var boServiceUserInfo = new BOServiceUser();
                var boServiceRole = new BOServiceRole();
                var boServiceUserRole = new BOServiceUserRole();
                var bOServiceUserAPI = new BOServiceUserAPI();

                var serviceUserRoles = await boServiceUserRole.GetServiceUserRole();
                var serviceUserAPIs = await bOServiceUserAPI.GetServiceUserAPIs(true);
                //foreach (BEServiceUserRole item in serviceUserRoles)
                //{
                //    ServiceUserRole.Add(item.ServiceUserRoleID, item);
                //}

                var serviceUserInfos = await boServiceUserInfo.GetServiceUserInfos();
                foreach (var item in serviceUserInfos)
                    //   item.Claims.Add

                    //Dictionary<int, BEServiceUserRole> ServiceUserRoleTemp = 
                    //serviceUserRoles.Where(p => p.UserID == Convert.ToInt32(item.UserID)).ToDictionary(p => p.ServiceUserRoleID, p => p);

                    // item.Claims = ServiceUserRole
                    if (!string.IsNullOrEmpty(item.ClientID))
                    {
                        var serviceUserRoleTemp = serviceUserRoles.Where(p => p.UserID == item.UserID);
                        if (serviceUserRoleTemp.Count() > 0)
                            foreach (var rolevalue in serviceUserRoleTemp)
                            {
                                var cl = new Claim(ClaimTypes.Role, rolevalue.RoleName);
                                item.Claims.Add(cl);
                            }

                        var apiPermissionsTemp =
                            serviceUserAPIs.Where(p => p.UserID == item.UserID);

                        if (apiPermissionsTemp.Count() > 0)
                            foreach (var apiPermission in apiPermissionsTemp)
                                item.APIPermissions.Add(apiPermission.APIName, "");

                        ServiceUser.Add(item.ClientID, item);
                        //     BEServiceUser checkUSer = GetServiceUserByClientSecret(item.ClientID, item.ClientSecret);
                        // if (checkUSer.UserID == null)
                        ServiceUserByClientSecret.Add(item.ClientID + "-" + item.ClientSecret, item);
                    }

                //BEServiceRoles serviceRoles = boServiceRole.GetServiceRole();
                //foreach (BEServiceRole item in serviceRoles)
                //{
                //    ServiceRole.Add(item.RoleID, item);
                //}

                watch.Stop();
                ServiceUserRefreshTime = DateTime.Now;
                ServiceUserLoadingTime = watch.ElapsedMilliseconds;
            });
        }

        //#endregion
        public static Task LoadDataProductsAsync(bool loadData)
        {
            //Dictionary<string, BEDataProduct> tempDataProductsByCode = new Dictionary<string, BEDataProduct>();
            //Dictionary<int, BEDataProduct> tempDataProductsByID = new Dictionary<int, BEDataProduct>();

            if (!loadData)
                    return Task.FromResult<object>(null);

            var boDataProduct = new BODataProduct();
            //BEDataProducts tempDataProducts = new BEDataProducts();
            return Task.Run(async () =>
            {
                DataProducts = await boDataProduct.GetDataProducts(true);
                //DataProducts.OrderBy(x => x.DataCode).ToList();
                //foreach (BEDataProduct item in tempDataProducts)
                //{
                //    if (!string.IsNullOrEmpty(item.Code))
                //    {

                //        tempDataProductsByCode.Add(item.Code, item);
                //    }
                //    if (item.ID > 0)
                //    {

                //        tempDataProductsByID.Add(item.ID, item);
                //    }
                //}

                //DataProducts = tempDataProducts;
                //DataProductsByCode = tempDataProductsByCode;
                //DataProductsByID = tempDataProductsByID;
                DataProducts.RefreshEndTime = DateTime.Now;
            });
        }

        public static async Task LoadAllConfigurationData(bool IsLoadServiceUser=false, 
            bool loadDataProduct=false)
        {
            // LoadDataServicePacks();
            var watch = Stopwatch.StartNew();

            //LoadDataProductsAsync(loadDataProduct).Wait();
            //LoadServiceUser(IsLoadServiceUser).Wait();


            Parallel.Invoke(
                //() => LoadProductSubscriptionTypesAsync().Wait()
                //, () => LoadBundleProductsAsync().Wait()
                () => LoadDataProductsAsync(loadDataProduct),
                //, () => LoadDedicatedAccountsAsync().Wait()
                //, () => LoadProductFamilysAsync().Wait()
                () => LoadServiceUser(IsLoadServiceUser)
                // ()=>LoadServiceUser(loadServiceUser1).Wait()
            );

            watch.Stop();
            FullDataLoadingTime = watch.ElapsedMilliseconds;
        }
    }
}