using System;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using DataAccess;
using SQLFactory;

namespace BusinessObject
{
    #region Object of BONew_App_Bonus

    public class BONew_App_Bonus
    {
        #region Destructor

        #endregion

        #region Save

        public async Task Save(BENew_App_Bonus new_App_Bonus)
        {
            int x = 0;
            SQLHelper sqlHelper = null;
            try
            {
                sqlHelper = new SQLHelper(true);
                x = await daNew_App_Bonus.Save(sqlHelper, new_App_Bonus);
                sqlHelper.CommitTran();

            }
            catch (Exception)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {

                //sqlHelper?.Dispose();

                if (x == 2)
                {
                    throw new BeHandledException(BEMessageCodes.AlreadyExist.Status, BEMessageCodes.AlreadyExist.error,
                    "The requested data is already exist in our system", "Already exist");
                }
                else if (x == 3)
                {
                    throw new BeHandledException(BEMessageCodes.AlreadyExistAndModified.Status, BEMessageCodes.AlreadyExistAndModified.error,
                     "The requested data is already exist in our system and modified into sign in request ", "Already exist and modified");
                }
            }
        }

        #endregion

        //	#region Delete
        //	public Task Delete(, int deletedBy)
        //	{
        //		try
        //		{
        //			sqlHelper = new SQLHelper(true);
        //			 daNew_App_Bonus.Delete(sqlHelper, , deletedBy);
        //			sqlHelper.CommitTran();
        //		}
        //		catch(Exception ex)
        //		{
        //			if (sqlHelper != null) sqlHelper.Rollback();
        //			throw ex;
        //		}
        //		finally
        //		{
        //			sqlHelper = null;
        //		}
        //	}
        //	#endregion

        //	#region Get
        //	public BENew_App_Bonus GetNew_App_Bonus()
        //	{
        //		BENew_App_Bonus new_App_Bonus = null;
        //		DANew_App_Bonus daNew_App_Bonus = new DANew_App_Bonus();

        //		try
        //		{
        //			sqlHelper = new SQLHelper();
        //			new_App_Bonus = daNew_App_Bonus.GetNew_App_Bonus(sqlHelper, );
        //			sqlHelper.CommitTran();
        //		}
        //		catch(Exception ex)
        //		{
        //			if (sqlHelper != null) sqlHelper.Rollback();
        //			throw ex;
        //		}
        //		finally
        //		{
        //			sqlHelper = null;
        //		}
        //		 return new_App_Bonus;
        //	}

        public async Task<bool> GetNew_App_Bonuss(string bonustype, string msisdn, DateTime browsedt)
        {
            bool new_App_Bonuss;
            var daNew_App_Bonus = new DANew_App_Bonus();
            SQLHelper sqlHelper = null;
            try
            {
                sqlHelper = new SQLHelper();
                new_App_Bonuss = await daNew_App_Bonus.GetNew_App_Bonus(sqlHelper, bonustype, msisdn, browsedt);
                sqlHelper.CommitTran();
            }
            catch (Exception)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
            return new_App_Bonuss;
        }

        #region Constructor

        #endregion

        #region Declaration

        //private SQLHelper sqlHelper;
        private readonly DANew_App_Bonus daNew_App_Bonus = new DANew_App_Bonus();

        #endregion

        //	#endregion
        //	public BENew_App_Bonuss GetNew_App_Bonuss(BENew_App_Bonus searchCriteria,
        //int pageIndex, int numberOfRecordsPerPage, string orderByQuery, out int totalCount)
        //	{
        //		BENew_App_Bonuss new_App_Bonuss = null;
        //		DANew_App_Bonus daNew_App_Bonus = new DANew_App_Bonus();

        //		try
        //		{
        //			sqlHelper = new SQLHelper();
        //			new_App_Bonuss = daNew_App_Bonus.GetNew_App_Bonuss(sqlHelper, searchCriteria, pageIndex, numberOfRecordsPerPage, orderByQuery, out totalCount);
        //			sqlHelper.CommitTran();
        //		}
        //		catch(Exception ex)
        //		{
        //			if (sqlHelper != null) sqlHelper.Rollback();
        //			throw ex;
        //		}
        //		finally
        //		{
        //			sqlHelper = null;
        //		}
        //		 return new_App_Bonuss;
        //	}
    }

    #endregion
}