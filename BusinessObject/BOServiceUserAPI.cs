using System;
using System.Threading.Tasks;
using BusinessEntity;
using DataAccess;
using SQLFactory;

namespace BusinessObject
{
    #region Object of BOServiceUserAPI

    public class BOServiceUserAPI
    {
        #region Destructor

        ~BOServiceUserAPI()
        {
        }

        #endregion

        #region Save

        public async Task Save(BEServiceUserAPI serviceUserAPI)
        {
            try
            {
                sqlHelper = new SQLHelper(true);
                await daServiceUserAPI.Save(sqlHelper, serviceUserAPI);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
        }

        #endregion

        #region Delete

        public async Task Delete(int serviceUserAPIServiceAPIID, int deletedBy)
        {
            try
            {
                sqlHelper = new SQLHelper(true);
                daServiceUserAPI.Delete(sqlHelper, serviceUserAPIServiceAPIID, deletedBy);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
        }

        #endregion

        public async Task<BEServiceUserAPIs> GetServiceUserAPIs(BEServiceUserAPI searchCriteria,
            int pageIndex, int numberOfRecordsPerPage, string orderByQuery)
        {
            BEServiceUserAPIs serviceUserAPIs = null;
            var daServiceUserAPI = new DAServiceUserAPI();

            try
            {
                sqlHelper = new SQLHelper();
                serviceUserAPIs = await daServiceUserAPI.GetServiceUserAPIs(sqlHelper, searchCriteria, pageIndex,
                    numberOfRecordsPerPage, orderByQuery);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return serviceUserAPIs;
        }

        #region Constructor

        #endregion

        #region Declaration

        private SQLHelper sqlHelper;
        private readonly DAServiceUserAPI daServiceUserAPI = new DAServiceUserAPI();

        #endregion

        #region Get

        public async Task<BEServiceUserAPI> GetServiceUserAPI(int serviceUserAPIServiceAPIID)
        {
            BEServiceUserAPI serviceUserAPI = null;
            var daServiceUserAPI = new DAServiceUserAPI();

            try
            {
                sqlHelper = new SQLHelper();
                serviceUserAPI = await daServiceUserAPI.GetServiceUserAPI(sqlHelper, serviceUserAPIServiceAPIID);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return serviceUserAPI;
        }

        public async Task<BEServiceUserAPIs> GetServiceUserAPIs(bool activeOnly)
        {
            BEServiceUserAPIs serviceUserAPIs = null;
            var daServiceUserAPI = new DAServiceUserAPI();

            try
            {
                sqlHelper = new SQLHelper("HVCDB");
                serviceUserAPIs = await daServiceUserAPI.GetServiceUserAPIs(sqlHelper, activeOnly);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return serviceUserAPIs;
        }

        #endregion
    }

    #endregion
}