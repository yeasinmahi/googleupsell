﻿using System;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using DataAccess;
using SQLFactory;

namespace BusinessObject
{
    public class BOServiceUser
    {
        #region Destructor

        ~BOServiceUser()
        {
        }

        #endregion


        public async Task<BEServiceUser> GetServiceUser(string userID, string password)
        {
            BEServiceUser serviceUser = null;
            //serviceUser = new BEServiceUser();

            //serviceUser.UserID = "asd234dfasd2345fasaaa65e5b03";
            //serviceUser.Password = "c84fae1dc73343709e82a0a6b9a9a48aa65e5b03";

            var daServiceUser = new DAServiceUser();
            SQLHelper sqlHelper = null;

            try
            {
                sqlHelper = new SQLHelper();
                serviceUser = await daServiceUser.GetServiceUser(sqlHelper, userID, password);
                sqlHelper.CommitTran();
            }
            catch (Exception)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return serviceUser;
        }

        #region Constructor

        #endregion

        #region Declaration

        private SQLHelper sqlHelper;
        private DAServiceUser DAServiceUser = new DAServiceUser();

        #endregion


        #region Get

        public async Task<BEServiceUser> GetServiceUserInfo(string userName)
        {
            BEServiceUser serviceUserInfo = null;
            var daServiceUser = new DAServiceUser();

            try
            {
                sqlHelper = new SQLHelper();
                serviceUserInfo = await daServiceUser.GetServiceUserInfo(sqlHelper, userName);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return serviceUserInfo;
        }

        public async Task<BEServiceUsers> GetServiceUserInfos()
        {
            BEServiceUsers serviceUserInfos = null;
            var daServiceUser = new DAServiceUser();

            try
            {
                //sqlHelper = new SQLHelper("HVCConnection");
                sqlHelper = new SQLHelper("HVCDB");
                serviceUserInfos = await daServiceUser.GetServiceUserInfos(sqlHelper);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return serviceUserInfos;
        }

        #endregion
    }
}