﻿using System;
using System.Threading.Tasks;
using BusinessEntity.Google.Notification;
using DataAccess;
using SQLFactory;

namespace BusinessObject
{
    #region Object of BOeSelfCareUser

    public class BOPushNotification
    {
        #region Destructor

        #endregion

        #region Delete

        public async Task Delete(int ID, int status)
        {
            SQLHelper sqlHelper = null;
            try
            {
                sqlHelper = new SQLHelper(true);
                await daNotification.Delete(sqlHelper, ID, status);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
        }

        #endregion

        #region Constructor

        #endregion

        #region Declaration

        //private SQLHelper sqlHelper;
        private readonly DAPushNotification daNotification = new DAPushNotification();

        #endregion


        #region UpdateNotification

        public async Task UpdateNotification(string ID)
        {
            SQLHelper sqlHelper = null;
            try
            {
                sqlHelper = new SQLHelper(true);
                await daNotification.UpdateNotification(sqlHelper, ID);
                sqlHelper.CommitTran();
            }
            catch (Exception)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
        }

        public async Task UpdateNotification(string ID, int status, long rtt, string comments=null)
        {
            SQLHelper sqlHelper = null;
            try
            {
                sqlHelper = new SQLHelper(true);
                await daNotification.UpdateNotification(sqlHelper, ID, status, rtt, comments);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
        }

        #endregion

        #region Get

        public async Task<PushNotification> GetPushNotification(int ID)
        {
            PushNotification campainBonus = null;
            var daCampBonus = new DAPushNotification();
            SQLHelper sqlHelper = null;
            try
            {
                sqlHelper = new SQLHelper();
                campainBonus = await daCampBonus.GetPushNotification(sqlHelper, ID);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return campainBonus;
        }

        public async Task<PushNotifications> GetPushNotifications()
        {
            PushNotifications nlist = null;
            var daNotification = new DAPushNotification();
            SQLHelper sqlHelper = null;
            try
            {
                sqlHelper = new SQLHelper();
                nlist = await daNotification.GetPushNotifications(sqlHelper);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return nlist;
        }

        public async Task<PushNotifications> GetPushNotificationServiceData()
        {
            PushNotifications campainBonus = null;
            var daNotification = new DAPushNotification();
            SQLHelper sqlHelper = null;
            try
            {
                sqlHelper = new SQLHelper();
                campainBonus = await daNotification.GetPushNotificationServiceData(sqlHelper);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return campainBonus;
        }

        public async Task<PushNotifications> GetPushNotificationServiceData(int noOfRecords, string guid,
            string triggerid, string subQuery)
        {
            PushNotifications Notificatoion = null;
            var daNotification = new DAPushNotification();
            SQLHelper sqlHelper = null;
            try
            {
                sqlHelper = new SQLHelper();
                Notificatoion =
                    await daNotification.GetPushNotificationServiceData(sqlHelper, noOfRecords, guid, triggerid,
                        subQuery);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return Notificatoion;
        }
        public async Task<PushNotifications> GetPushNotificationServiceDataT24(int noOfRecords, string guid,
            string triggerid, string subQuery)
        {
            PushNotifications Notificatoion = null;
            var daNotification = new DAPushNotification();
            SQLHelper sqlHelper = null;
            try
            {
                sqlHelper = new SQLHelper();
                Notificatoion =
                    await daNotification.GetPushNotificationServiceDataT24(sqlHelper, noOfRecords, guid, triggerid,
                        subQuery);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return Notificatoion;
        }

        #endregion

        #region Save

        public async Task SaveKafka(PushNotification pushNotification)
        {
            SQLHelper sqlHelper = null;
            try
            {
                sqlHelper = new SQLHelper(true);
                await daNotification.SaveKafka(sqlHelper, pushNotification);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw ex;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
        }
        public async Task SaveGoogle(PushNotification pushNotification)
        {
            SQLHelper sqlHelper = null;
            try
            {
                sqlHelper = new SQLHelper(true);
                await daNotification.SaveGoogle(sqlHelper, pushNotification);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw ex;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
        }

        #endregion
    }

    #endregion
}