﻿using System;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using SQLFactory;
using Utility;

namespace BusinessObject
{
    public class BOAccount
    {
        public string GetSqlHelperVersion()
        {
            return new SQLHelper().GetType().Assembly.GetName().Version?.ToString();
        }

        public async Task<string> SendOTP(string MSISDN, int browseSource, bool newotp)
        {
            string temporaryCode;

            try
            {
                var strSubNo = MSISDN;
                var otpMsg = AppSettings.OTPSMS;
                var SMSsender = AppSettings.SMSsender;

                var bootp = new BOOTP();
                strSubNo = strSubNo.StartsWith("0") ? strSubNo.Substring(1) : strSubNo;

                temporaryCode = new Number().GenerateRandomNo(Convert.ToInt16(AppSettings.OTPLength));

                //int otpIntervalTime = Convert.ToInt32(AppSettings.OTPIntervalTime);
                //int otpMinimumInADay = Convert.ToInt32(AppSettings.MaximumOTPInADay);
                await bootp.SaveOTPAndSendSMSNew(SMSsender, MSISDN, temporaryCode, otpMsg, true,
                    AppSettings.OTPIntervalTime, AppSettings.MaximumOTPInADay);
                ;

                //string serviceName = "SendOTP";
                //string browserInfo = new BECommon().BrowserDetails(serviceName, "MSISDN Code-", new string[] { MSISDN,  temporaryCode });
                //string sessionId = string.Empty;
            }
            catch (BeHandledException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw ex;
            }


            return temporaryCode;
        }
    }
}