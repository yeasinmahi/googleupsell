using System;
using System.Threading.Tasks;
using BusinessEntity.Logging;
using DataAccess;
using SQLFactory;

namespace BusinessObject
{
    #region Object of BOAppErrorLog

    public class BOAppErrorLog
    {
        #region Destructor

        ~BOAppErrorLog()
        {
        }

        #endregion

        #region Constructor

        #endregion

        #region Declaration

        private SQLHelper sqlHelper;
        private readonly DAAppErrorLog daAppErrorLog = new DAAppErrorLog();

        #endregion

        #region Save

        public async Task Create(BEAppErrorLog log)
        {
            SQLHelper createSQLHelper = null;
            var daLog = new DAAppErrorLog();
            try
            {
                createSQLHelper = new SQLHelper(true);
                await daLog.Create(createSQLHelper, log);
                createSQLHelper.CommitTran();
            }
            catch (Exception ex)
            {
                createSQLHelper?.Rollback();
                throw;
            }
            finally
            {
                createSQLHelper = null;
            }
        }

        public async Task Save(BEAppErrorLog appErrorLog)
        {
            try
            {
                sqlHelper = new SQLHelper(true);
                await daAppErrorLog.Save(sqlHelper, appErrorLog);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                sqlHelper = null;
            }
        }

        #endregion

        //#region Delete
        //public async Task Delete(, int deletedBy)
        //{
        //	try
        //	{
        //		sqlHelper = new SQLHelper(true);
        //		 daAppErrorLog.Delete(sqlHelper, , deletedBy);
        //		sqlHelper.CommitTran();
        //	}
        //	catch(Exception ex)
        //	{
        //		if (sqlHelper != null) sqlHelper.Rollback();
        //		throw ex;
        //	}
        //	finally
        //	{
        //		sqlHelper = null;
        //	}
        //}
        //#endregion

        //#region Get
        //public BEAppErrorLog GetAppErrorLog()
        //{
        //	BEAppErrorLog appErrorLog = null;
        //	DAAppErrorLog daAppErrorLog = new DAAppErrorLog();

        //	try
        //	{
        //		sqlHelper = new SQLHelper();
        //		appErrorLog = daAppErrorLog.GetAppErrorLog(sqlHelper);
        //		sqlHelper.CommitTran();
        //	}
        //	catch(Exception ex)
        //	{
        //		if (sqlHelper != null) sqlHelper.Rollback();
        //		throw ex;
        //	}
        //	finally
        //	{
        //		sqlHelper = null;
        //	}
        //	 return appErrorLog;
        //}

        //public BEAppErrorLogs GetAppErrorLogs(bool activeOnly)
        //{
        //	BEAppErrorLogs appErrorLogs = null;
        //	DAAppErrorLog daAppErrorLog = new DAAppErrorLog();

        //	try
        //	{
        //		sqlHelper = new SQLHelper();
        //		appErrorLogs = daAppErrorLog.GetAppErrorLogs(sqlHelper, activeOnly);
        //		sqlHelper.CommitTran();
        //	}
        //	catch(Exception ex)
        //	{
        //		if (sqlHelper != null) sqlHelper.Rollback();
        //		throw ex;
        //	}
        //	finally
        //	{
        //		sqlHelper = null;
        //	}
        //	 return appErrorLogs;
        //}
        //#endregion
        //	public BEAppErrorLogs GetAppErrorLogs(BEAppErrorLog searchCriteria,
        //int pageIndex, int numberOfRecordsPerPage, string orderByQuery, out int totalCount)
        //	{
        //		BEAppErrorLogs appErrorLogs = null;
        //		DAAppErrorLog daAppErrorLog = new DAAppErrorLog();

        //		try
        //		{
        //			sqlHelper = new SQLHelper();
        //			appErrorLogs = daAppErrorLog.GetAppErrorLogs(sqlHelper, searchCriteria, pageIndex, numberOfRecordsPerPage, orderByQuery, out totalCount);
        //			sqlHelper.CommitTran();
        //		}
        //		catch(Exception ex)
        //		{
        //			if (sqlHelper != null) sqlHelper.Rollback();
        //			throw ex;
        //		}
        //		finally
        //		{
        //			sqlHelper = null;
        //		}
        //		 return appErrorLogs;
        //	}
    }

    #endregion
}