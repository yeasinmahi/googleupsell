﻿using System;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using DataAccess;
using SQLFactory;

namespace BusinessObject
{
    public class BOServiceUserRole
    {
        #region Destructor

        ~BOServiceUserRole()
        {
        }

        #endregion

        public async Task<BEServiceUserRoles> GetServiceUserRole()
        {
            BEServiceUserRoles serviceUser;


            var serviceUserRole = new DAServiceUserRole();
            SQLHelper sqlHelper = null;

            try
            {
                //sqlHelper = new SQLHelper("HVCConnection");
                sqlHelper = new SQLHelper("HVCDB");
                serviceUser = await serviceUserRole.GetServiceUserRole(sqlHelper);
                sqlHelper.CommitTran();
            }
            catch (Exception)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return serviceUser;
        }

        #region Constructor

        #endregion

        #region Declaration

        //private SQLHelper _sqlHelper = null;
        //private DAServiceUserRole _oDaServiceUserRole = new DAServiceUserRole();

        #endregion
    }
}