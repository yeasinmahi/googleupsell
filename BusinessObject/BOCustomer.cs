﻿using System;
using System.Threading.Tasks;
using BusinessEntity.APIHub2;
using DataAccess;
using SQLFactory;

namespace BusinessObject
{
    public class BOCustomer
    {
        //private SQLHelper sqlHelper;


        public async Task<bool> IsValidPassword(string MSISDN, string password)
        {
            //customer.CustomerID = "c84fae1dc73343709e82a0a6b9a9a48aa65e5b03";
            //customer.EmailAddress = "bdzahirul@gmail.com";
            //customer.Password = "D0D64932DB655FEEABADD14C9C74E3E19BAC2EAD";
            var daCustomer = new DACustomer();
            //bool isValidCustomer = false;

            SQLHelper sqlHelper = null;
            bool isValidCustomer;
            try
            {
                sqlHelper = new SQLHelper("HVCDB");
                isValidCustomer = await daCustomer.IsValidPassword(sqlHelper, MSISDN, password);
                sqlHelper.CommitTran();
            }
            catch (Exception)
            {
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }


            return isValidCustomer;
        }

        public async Task<bool> IsValidOTP(string msisdnNo, int otpValidityDuration, string OTP, int pageType,
            int otpWrongAttempLimit)
        {
            bool isValidOTP;
            SQLHelper sqlHelper = null;
            try
            {
                var daOTP = new DAOTP();
                sqlHelper = new SQLHelper();
                isValidOTP = await daOTP.IsValidOTP(sqlHelper, msisdnNo, otpValidityDuration, OTP, pageType,
                    otpWrongAttempLimit);
                sqlHelper.CommitTran();
            }
            catch (Exception)
            {
                //   Utility.SaveErrorLog(this.GetType().ToString(), "", ex);
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return isValidOTP;

            // return otp;
        }

        public async Task<bool> IsValidCustomerSatus(string MSISDN)
        {
            bool isValidCustomer;
            SQLHelper sqlHelper = null;
            var daCustomer = new DACustomer();
            try
            {
                sqlHelper = new SQLHelper();
                isValidCustomer = await daCustomer.IsValidCustomerSatus(sqlHelper, MSISDN);
                sqlHelper.CommitTran();
            }
            catch (Exception)
            {
                //HVC.Common.Utility.SaveErrorLog(this.GetType().ToString(), "", ex);
                //TODO: error log
                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }

            return isValidCustomer;
        }

        public async Task<BECustomer> GetCustomer1(string MSISDN, bool unregisteredCustomer)
        {
            SQLHelper sqlHelper = null;
            BECustomer customer = null;
            var daCustomer = new DACustomer();
            try
            {
                sqlHelper = new SQLHelper();
                customer = await daCustomer.GetCustomer(sqlHelper, MSISDN, unregisteredCustomer);
                sqlHelper.CommitTran();
            }
            catch (Exception ex)
            {
                //HVC.Common.Utility.SaveErrorLog(this.GetType().ToString(), "", ex);

                sqlHelper?.Rollback();
                throw;
            }
            finally
            {
                //sqlHelper?.Dispose();
            }
            return customer;
        }
    }
}