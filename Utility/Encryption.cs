﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.Crypto.Digests;

namespace Utility
{
    public class Encryption
    {
        public async Task<string> EncryptBase64(string value)
        {
            var byt = Encoding.UTF8.GetBytes(value);
            return Convert.ToBase64String(byt);
        }

        public async Task<string> DecryptBase64(string value)
        {
            var byt = Convert.FromBase64String(value);
            return Encoding.UTF8.GetString(byt);
        }

        public async Task<string> EncryptAes(string clearText, string secret)
        {
            var iv = new byte[16];
            byte[] array;

            using (var aes = Aes.Create())
            {
                aes.KeySize = 128;
                aes.Key = Encoding.UTF8.GetBytes(secret);
                aes.IV = iv;
                aes.Padding = PaddingMode.PKCS7;
                aes.Mode = CipherMode.CBC;
                var encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (var memoryStream = new MemoryStream())
                {
                    using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (var streamWriter = new StreamWriter(cryptoStream))
                        {
                            await streamWriter.WriteAsync(clearText);
                        }

                        array = memoryStream.ToArray();
                    }
                }
            }

            return Convert.ToBase64String(array);
        }

        public async Task<string> DecryptAes(string cipherText, string secret)
        {
            var iv = new byte[16];
            var buffer = Convert.FromBase64String(cipherText);

            using (var aes = Aes.Create())
            {
                aes.KeySize = 128;
                aes.Key = Encoding.UTF8.GetBytes(secret);
                aes.IV = iv;
                aes.Padding = PaddingMode.PKCS7;
                //aes.Mode = CipherMode.ECB;
                aes.Mode = CipherMode.CBC;
                var decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                using (var memoryStream = new MemoryStream(buffer))
                {
                    using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (var streamReader = new StreamReader(cryptoStream))
                        {
                            var plainText = await streamReader.ReadToEndAsync();
                            return plainText;
                        }
                    }
                }
            }
        }
        public async Task<string> DecryptAesECBMode(string cipherText, string secret)
        {
            var iv = new byte[16];
            var buffer = Convert.FromBase64String(cipherText);

            using (var aes = Aes.Create())
            {
                aes.KeySize = 128;
                aes.Key = Encoding.UTF8.GetBytes(secret);
                aes.IV = iv;
                aes.Padding = PaddingMode.PKCS7;
                //aes.Mode = CipherMode.ECB;
                aes.Mode = CipherMode.ECB;
                var decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                using (var memoryStream = new MemoryStream(buffer))
                {
                    using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (var streamReader = new StreamReader(cryptoStream))
                        {
                            var plainText = await streamReader.ReadToEndAsync();
                            return plainText;
                        }
                    }
                }
            }
        }

        public async Task<string> EncryptSha1(string clearText)
        {
            using (var sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(clearText));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (var b in hash)
                    // can be "x2" if you want lowercase
                    sb.Append(b.ToString("x2"));

                return sb.ToString();
            }
        }

        public async Task<string> EncryptSha3(string clearText)
        {
            var hashAlgorithm = new Sha3Digest(512);

            // Choose correct encoding based on your usecase
            var input = Encoding.ASCII.GetBytes(clearText);

            hashAlgorithm.BlockUpdate(input, 0, input.Length);

            var result = new byte[64]; // 512 / 8 = 64
            hashAlgorithm.DoFinal(result, 0);

            var hashString = BitConverter.ToString(result);
            hashString = hashString.Replace("-", "").ToLowerInvariant();
            return hashString;
        }

        public async Task<bool> IsSHA1HashEqual(string hashinput, string plainText)
        {
            return string.Equals(hashinput, await EncryptSha1(plainText), StringComparison.OrdinalIgnoreCase);
        }

        public async Task<bool> IsSHA3HashEqual(string hashinput, string plainText)
        {
            return string.Equals(hashinput, await EncryptSha3(plainText), StringComparison.OrdinalIgnoreCase);
        }
    }
}