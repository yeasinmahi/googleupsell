﻿using System;
using System.Data;
using BusinessEntity.APIHub2;

namespace Utility
{
    public class CustomObject
    {
        public BEBase MapItem(BEBase obj, IDataReader reader)
        {
            var properties = obj.GetType().GetProperties();

            foreach (var property in properties)
                if (ReaderContainsColumn(reader, property.Name))
                {
                    if (property.PropertyType == typeof(string))
                        property.SetValue(obj,
                            reader[property.Name] == DBNull.Value ? "" : Convert.ToString(reader[property.Name]), null);
                    else if (property.PropertyType == typeof(int))
                        property.SetValue(obj,
                            reader[property.Name] == DBNull.Value ? 0 : Convert.ToInt32(reader[property.Name]), null);
                    else if (property.PropertyType == typeof(long))
                        property.SetValue(obj,
                            reader[property.Name] == DBNull.Value ? 0 : Convert.ToInt64(reader[property.Name]), null);
                    else if (property.PropertyType == typeof(double))
                        property.SetValue(obj,
                            reader[property.Name] == DBNull.Value ? 0 : Convert.ToDouble(reader[property.Name]), null);
                    else if (property.PropertyType == typeof(decimal))
                        property.SetValue(obj,
                            reader[property.Name] == DBNull.Value ? 0 : Convert.ToDecimal(reader[property.Name]), null);
                    else if (property.PropertyType == typeof(bool))
                        property.SetValue(obj,
                            reader[property.Name] == DBNull.Value ? false : Convert.ToBoolean(reader[property.Name]),
                            null);
                    else if (property.PropertyType == typeof(DateTime))
                        property.SetValue(obj,
                            reader[property.Name] == DBNull.Value
                                ? DateTime.MinValue
                                : Convert.ToDateTime(reader[property.Name]), null);
                    else if (property.PropertyType.ToString().Contains("Enum"))
                        property.SetValue(obj,
                            reader[property.Name] == DBNull.Value ? 0 : Convert.ToInt32(reader[property.Name]), null);
                }

            obj.IsNew = false;
            return obj;
        }

        private bool ReaderContainsColumn(IDataReader reader, string name)
        {
            for (var i = 0; i < reader.FieldCount; i++)
                if (reader.GetName(i).Equals(name, StringComparison.CurrentCultureIgnoreCase))
                    return true;
            return false;
        }
    }
}