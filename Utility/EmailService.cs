﻿using System;
using System.Net.Mail;

namespace Utility
{
    public class EmailService
    {
        public void SendMail(string toAddr, string subj, string msgBody)
        {
            try
            {
                var mail = new MailMessage();
                var toAddress = new MailAddress(toAddr);
                mail.To.Add(toAddress);
                mail.Subject = subj;
                mail.Body = msgBody;
                mail.IsBodyHtml = true;
                var smtp = new SmtpClient();
                smtp.Send(mail);

                //HVC.Common.Utility.SaveErrorLog("Common", "SendMail(string toAddr, string subj, string msgBody)", "Send Mail Success", "toAddr:" + toAddr + "subj:" + subj + "msgBody:" + msgBody);
            }
            catch (Exception)
            {
                //HVC.Common.Utility.SaveErrorLog("Common", "SendMail(string toAddr, string subj, string msgBody)", ex);
            }
        }
    }
}