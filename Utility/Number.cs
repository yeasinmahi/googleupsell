﻿using System;

namespace Utility
{
    public class Number
    {
        public string GenerateRandomNo(int length)
        {
            var randomNoGeneratingString = "1234567890";
            var chars = randomNoGeneratingString.ToCharArray();
            var randomNo = string.Empty;
            var random = new Random();

            while (randomNo.Length < length)
            {
                var randomIndex = random.Next(1, chars.Length);
                var randomChar = Convert.ToChar(chars.GetValue(randomIndex));

                if (!randomNo.Contains(randomChar)) randomNo += chars.GetValue(randomIndex);
            }

            return randomNo;
        }
    }
}