﻿namespace Utility.Extensions
{
    public static class StringExtensions
    {
        //public static string MySubString(this string str,int length)
        //{

        //    return MySubString(str,length,true);
        //}
        public static string MySubString(this string str, int length,bool isFromBegening=true)
        {
            if (string.IsNullOrWhiteSpace(str))
                return str;
            
            if (str.Length > length)
            { 
                if (isFromBegening)
                {
                    return str.Substring(0, length);
                }
                else
                {
                    return str.Substring(str.Length-length);
                }
            
            }
                
            return str;
        }

    }
}
