﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Utility.Extensions
{
    public static class ExceptionExtensions
    {
        public static string DBBSErrorLogPath = string.Empty;

        public static string RequestLogPath
        {
            get
            {
                var startupPath = string.Empty;

                try
                {
                    //if (!string.IsNullOrEmpty(AppSettings.DBBSErrorLogPath))
                    //{
                    //    startupPath = AppSettings.DBBSErrorLogPath;
                    //}
                    if (!string.IsNullOrEmpty(DBBSErrorLogPath))
                        startupPath = DBBSErrorLogPath;
                    else
                        startupPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Log");

                    //if (HttpRuntime.AppDomainAppId != null)
                    //{
                    //    //is web app
                    //    startupPath = HttpContext.Current.Server.MapPath(string.Format("~/{0}/", "Log"));
                    //}
                    //else
                    //{
                    ////is windows app

                    //}
                }
                catch (Exception)
                {
                    startupPath = @"C\Log";
                }

                Directory.CreateDirectory(startupPath);
                return startupPath;
            }
        }


        public static string MSISDN { get; set; }


        public static string ToFormattedError(this Exception exception)
        {
            var message = string.Empty;
            var exceptions = exception.GetAllExceptions();

            foreach (var ex in exceptions)
            {
                var st = new StackTrace(ex, true);

                var frame = st.GetFrame(st.FrameCount - 1);
                message += Path.GetFileName(frame.GetFileName()) + ":: " + frame.GetMethod().Name + "::" +
                           frame.GetFileLineNumber() + " :: " + ex.Message + Environment.NewLine;
            }

            return message;
        }


        public static IEnumerable<Exception> GetAllExceptions(this Exception exception)
        {
            yield return exception;

            if (exception is AggregateException)
            {
                var aggrEx = exception as AggregateException;
                foreach (var innerEx in aggrEx.InnerExceptions.SelectMany(e => e.GetAllExceptions()))
                    yield return innerEx;
            }
            else if (exception.InnerException != null)
            {
                foreach (var innerEx in exception.InnerException.GetAllExceptions()) yield return innerEx;
            }
        }


        public static string ToFormattedErrorMessage(this Exception exception)
        {
            var messages = exception
                .GetAllExceptions()
                .Where(e => !string.IsNullOrWhiteSpace(e.Message))
                .Select(exceptionPart => exceptionPart.Message.Trim() + "\r\n" +
                                         (exceptionPart.StackTrace != null ? exceptionPart.StackTrace.Trim() : ""));
            var flattened = string.Join("\r\n\r\n", messages); // <-- the separator here
            return flattened;
        }

        public static void ToTextFileLog(this Exception exception, string msisdn,
            string requestMethod, string requestURL = "", string requester = "BOSGW", string response = "",
            string request = "", string folderName = "Request Log",
            string fileName = "ErrorLog", string fileType = "txt")
        {
            var message = string.Empty;
            var exceptions = exception.GetAllExceptions();
            //var startupPath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/{0}/", folderName));
            var startupPath = RequestLogPath;
            var filePath = Path.Combine(startupPath,
                fileName + "_" + DateTime.Now.ToString("yyyy-MM-dd") + "." + fileType);
            if (!Directory.Exists(filePath))
                message += "Date|Time|MSISDN|FileName|MethodName|LineNumber|Message|StackTrace" + Environment.NewLine;


            foreach (var ex in exceptions)
            {
                var msg = string.Empty;
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(st.FrameCount - 1);

                if (st.FrameCount == 0 || frame == null) continue;
                msg = DateTime.Now.ToString("yyyy-MM-dd") + "|"
                                                          + DateTime.Now.ToString("hh:mm:ss") + "|"
                                                          + msisdn + "|"
                                                          + Path.GetFileName(frame.GetFileName()) + "|"
                                                          + frame.GetMethod().Name + "|"
                                                          + frame.GetFileLineNumber() + "|"
                                                          + ex.Message + "|" + "; requestMethod: " + requestMethod +
                                                          "; requestURL: " + requestURL + "; requester: " + requester +
                                                          "; response " + response + "; request:" + request
                                                          + ex.StackTrace;

                msg += Environment.NewLine;
                message += msg;
            }

            File.AppendAllText(filePath, message);
        }

        public static void ToWriteMessageLog(this string message, string methodName = "",
            string folderName = "Request Log", string fileName = "MessageLog", string fileType = "txt")
        {
            var startupPath = RequestLogPath;
            //var startupPath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/{0}/", folderName));
            var filePath = Path.Combine(startupPath,
                fileName + "_" + DateTime.Now.ToString("yyyy-MM-dd") + "." + fileType);

            var msg = "Date: " + DateTime.Now.ToString("yyyy-MM-dd") + Environment.NewLine
                      + "Time: " + DateTime.Now.ToString("hh:mm:ss") + Environment.NewLine;

            if (!string.IsNullOrWhiteSpace(methodName)) msg += "Function : " + methodName + Environment.NewLine;
            msg += "Message : " + message + Environment.NewLine + Environment.NewLine;

            File.AppendAllText(filePath, msg);
        }

        public static void ToRequestResponseLog(this string request, string response, string methodName,
            string folderName = "Request Log", string fileName = "RequestResponseLog", string fileType = "txt")
        {
            //var startupPath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/{0}/", folderName));
            var startupPath = RequestLogPath;
            var filePath = Path.Combine(startupPath,
                fileName + "_" + DateTime.Now.ToString("yyyy-MM-dd") + "." + fileType);

            var msg = string.Format("==========REQUEST START=======({0})===={1}======",
                DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"), methodName) + Environment.NewLine;
            msg += "==========Request===========" + Environment.NewLine + request + Environment.NewLine;
            msg += "==========Response==========" + Environment.NewLine + response + Environment.NewLine;
            msg += "==========REQUEST END==========" + Environment.NewLine;

            File.AppendAllText(filePath, msg);
        }


        public static string TruncateLongString(this string str, int maxLength)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            return str.Substring(0, Math.Min(str.Length, maxLength));
        }
    }
}