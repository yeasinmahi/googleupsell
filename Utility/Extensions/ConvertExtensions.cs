﻿using System;
using System.Text;

namespace Utility.Extensions
{
    public static class ConvertExtensions
    {
        public static byte[] ToEncodding(this string input)
        {
            var b = Encoding.UTF8.GetBytes(input);
            return b;
        }

        public static string ToEncodding(this byte[] input)
        {
            var s = Encoding.UTF8.GetString(input);
            return s;
        }

        public static byte[] ToBase64(this string input)
        {
            var b = Convert.FromBase64String(input);
            return b;
        }

        public static string ToBase64(this byte[] input)
        {
            var s = Convert.ToBase64String(input);
            return s;
        }

        public static string ConvertToString(this Enum eff)
        {
            return Enum.GetName(eff.GetType(), eff);
        }

        public static EnumType ConverToEnum<EnumType>(this string enumValue)
        {
            return (EnumType) Enum.Parse(typeof(EnumType), enumValue);
        }
    }
}